# Naamgeving van versieinformatie

Als versieinformatie meegegeven moet worden in een uitwisselpakket dan wordt dat ondergebracht in een aparte component, [Versie-informatie](uitwisselpakket_versieinformatie.md). De naamgeving voor die component volgt de [naamgevingsconventie](naamgevingsconventie.md) van niet-tekstuele informatie.
Voor versieinformatie wordt alleen het work-id gebruikt, want de versie ervan wordt nu juist in de module `Momentopname` (onderdeel van deze component) beschreven in plaats van met een expression-id.

Voor versieinformatie wordt in de identificatie "**versie**" gebruikt:

**Work**: `"/join/id/versie/" <overheid> "/" <datum_work> "/" <overig>`

met:

|code       |betekenis|
|---        |---      |
|"/join   |	Afkorting van **Juridische Object Identificatie Naming convention**
|/id | `id` om aan te geven dat het identificaties betreft|
|/versie/"  | `versie` om aan te geven dat het om versieinformatie gaat. |
|overheid   | Code van het verantwoordelijke bevoegde gezag volgens één van de [waardelijsten](imop_waardelijsten.md#overheid)
|datum_work | Datum waarop de versieinformatie voor het eerst is aangemaakt. Dit mag een volledige datum zijn (YYYY-MM-DD conform ISO 8601) maar het mag ook alleen een jaartal zijn. |
|overig     | Een extra kenmerk om het doel uniek te maken. Het is altijd toegestaan om hiervoor een UUID te gebruiken.| 

Voorbeelden:
- `/join/id/versie/gm9999/2021-07-21/320f0afdae494f3ab443de20ea3929fa`
- `/join/id/versie/gm9999/2021/dossier3929938`

XML voorbeeld van de component versieinformatie in een pakbon.xml van een uitwisselpakket:

```xml
<Component>
  <FRBRWork>/join/id/versie/gm9999/2021/4D7A3BBC6CCC4F828608E3A2CA0E258D</FRBRWork>
	<soortWork>/join/id/stop/work_024</soortWork>
  <heeftModule>
    <Module>
      <localName>Momentopname</localName>
      <namespace>https://standaarden.overheid.nl/stop/imop/data/</namespace>
      <bestandsnaam>Momentopname.xml</bestandsnaam>
      <mediatype>application/xml</mediatype>
      <schemaversie>1.2.0</schemaversie>
    </Module>
  </heeftModule>
</Component>
```

