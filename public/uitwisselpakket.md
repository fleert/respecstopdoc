# Uitwisselpakket

## Toepassing

IMOP schrijft slechts voor hoe losse STOP-modules gecodeerd moeten worden. Een STOP-verwerkend systeem, zoals de LVBB, kan zelf bepalen hoe de verschillende STOP-modules in samenhang uitgewisseld moeten worden en/of welke informatie daarbij aanwezig moet zijn. Echter, voor een aantal toepassingen is het wenselijk de uitwisseling van een verzameling STOP-modules in samenhang te standaardiseren. Een voorbeeld hiervan is een pakket dat de complete versie van de geldende regelgeving (tekst, informatieobjecten, metadata, annotaties) samenvoegt om uit te wisselen. In die gevallen kan het **IMOP-uitwisselpakket** gebruikt worden.

## Implementatie

Het uitwisselpakket is een gecomprimeerd (zip) bestand met als extensie `.stop` in plaats van `.zip`. De inhoud van het pakket bestaat uit:

- Een bestand met de vaste naam `pakbon.xml` waarin de inhoud van het pakket beschreven wordt.
- Een of meer XML-bestanden. Elk uit te wisselen STOP-module is opgenomen in een apart XML-bestand.
- Bestanden die door de STOP-modules gebruikt worden, zoals plaatjes of informatieobject-bestanden.
- Bestanden met gerelateerde informatie als beschreven in andere standaarden (zoals IMOW).

Zie [implementatie](uitwisselpakket_implementatie.md) voor een gedetailleerde beschrijving van de opbouw en inhoud van het pakket.

## Importeren van een uitwisselpakket

Bij overgang naar een nieuwe versie van STOP kunnen enige tijd twee versies van de standaard in gebruik zijn. Ook bij het uitwisselen van historische versies van een regeling kan het voorkomen dat voor de informatie een andere versie van de standaard is gebruikt dan de versie waarop de software is gebaseerd die het uitwisselpakket importeert.

De pakbon in het uitwisselpakket is versieneutraal. De beschrijving van de pakbon is in combinatie met de IMOP-versieinformatie in veel gevallen voldoende om importerende software in staat te stellen STOP modules van andere (nog ondersteunde) versies te importeren. Zie daarvoor de beschrijving van het [importeren](uitwisselpakket_import.md) van een uitwisselpakket.

## Versieinformatie in een uitwisselpakket

Een instrumentversie (bijv. een regelingversie) bevat juridische en niet-juridische informatie. De niet-juridische informatie (zoals de metadata en informatie uit andere standaarden als IMOW) kent geen zelfstandige versie-identificatie. Een landelijke voorziening als LVBB of DSO-LV staat het toe dat (niet-juridische) informatie wordt gecorrigeerd of aangevuld zonder dat dit leidt tot een nieuwe expression voor een regeling- of informatieobjectversie. Welke versie van de niet-juridische informatie is opgenomen in een uitwisselpakket, moet dus apart gespecificeerd worden.

Om in een uitwisselpakket met een gewijzigde versie te kunnen achterhalen welke aanpassingen zijn aangebracht, is het noodzakelijk om de basisversie te identificeren waarop de wijzigingen zijn aangebracht. 

De [versieinformatie in een uitwisselpakket](uitwisselpakket_versieinformatie.md) voorziet in beide situaties.

## Gebruik bij het maken van nieuwe regelgeving

Het uitwisselpakket ondersteunt de uitwisseling van informatie in verschillende [scenario's](uitwisselpakket_scenarios.md) bij het opstellen van nieuwe (of gewijzigde) regelgeving:

1. Het ophalen van de geldigheidsinformatie (toestanden) van regelgeving uit de LVBB
2. Het ophalen van een (nu of in de toekomst) geldende versie van regelgeving uit de LVBB
3. Het doorgeven van een gewijzigde versie van de regelgeving bij de voorbereiding van een besluit
4. Het uitwisselen van een besluit waarin de gewijzigde versie van de regelgeving is beschreven
