# Status van een besluit

Als tegen een besluit beroep is ingesteld, geeft het BG dit met een [procedurestap](besluitproces_rechterlijkemacht_procedurele_status.md) aan. De LVBB leidt uit de procedurestappen de herroepelijkheid van het besluit af. Bij de uitlevering wordt de status van het besluit aangegeven met de datum `onherroepelijkVanaf`. Ligt deze datum in de toekomst, of is deze datum onbepaald, dan is het besluit *niet onherroepelijk*. Ligt deze datum in het heden of verleden dan is het besluit *onherroepelijk* vanaf betreffende datum. 

Naast de datum `onherroepelijkVanaf` is er ook een *vlag* waarmee aangegeven wordt of een besluit is *geschorst* of niet.

Welke stap tot welke herroepelijkheid leidt, wordt hieronder toegelicht.

## Procedurestappen en onherroepelijkVanaf 

In onderstaande tabel staat aangegeven wat het effect is van het aanleveren van een procedurestap door het BG op de uitlevering door de LVBB van de status van het besluit, uitgedrukt in de datum `onherroepelijkVanaf` en de geschorst-vlag.

Onderstaande procedurstappen hebben invloed op de status van het besluit. Niet genoemde procedurestappen veranderen niets aan de status van het besluit. 

Of een besluit direct onherroepelijk is, hangt af van het wel of niet aanwezig zijn van de stap *Einde beroepstermijn*. Aangezien een besluit onherroepelijk is tenzij anders aangegeven, leidt het ontbreken van een *Einde beroepstermijn*-stap tot `OnherroepelijkVanaf` de datum ondertekening. Dit kan door de LVBB geïmplementeerd worden door uit de verplichte stap *Ondertekening* de datum als datum `onherroepelijkVanaf` vast te leggen,  totdat het BG de stap *Einde beroepstermijn* verzendt.

Een "-" in onderstaande tabel betekent geen wijziging. 

| **Aangeleverde stap**            | Gevolgen voor  `onherroepelijkVanaf`                                 | Geschorst? |
| --------------------------------- |  --------------------------------------------------------- | :-------: |
| **Ondertekening**   | Bepaald door datum ondertekening.     |    Nee.    |
| **Einde beroepstermijn**    | Bepaald door datum einde beroepstermijn.  |    Nee.    |
| **Beroep ingesteld**   | Wordt gewist/onbepaald.  |    Nee.    |
| **Schorsing**  |   -     |    Ja.     |
| **Schorsing opgeheven**     | -                                                         |    Nee.    |
| **Beroep(en) definitief afgedaan** | Bepaald door:<ul><li>datum uitspraak,</li><li>datum einde (hoger) beroepstermijn.</li></ul>  |    Nee.    |


### Einde beroepstermijn

Volgt er na een besluit geen kennisgeving met *Einde beroepstermijn*, dan is de status van het besluit "Onherroepelijk" per datum ondertekening: onherroepelijkVanaf = datum ondertekening. 

Wordt wel een kennisgeving met een *Einde beroepstermijn* gepubliceerd, dan is de status van het besluit "Niet onherroepelijk" totdat de einddatum van de beroepstermijn wordt bereikt (datum onherroepelijkVanaf = datum einde beroepstermijn). Is deze datum verstreken, dan wordt de status "Onherroepelijk" tenzij inmiddels een *Beroep(en) ingesteld*-stap is aangeleverd. 

### Beroep(en) ingesteld

Indien aan het einde van de beroepstermijn een of meer beroepen zijn ingesteld, levert het BG de procedurestap *Beroep(en) ingesteld*. Hierdoor wordt de datum *onherroepelijkVanaf* gewist en wordt het besluit pas "Onherroepelijk" als het BG de stap *Beroep(en) definitief afgedaan* aanlevert.

### Schorsing

Besluit de rechter in een tussenuitspraak het besluit geheel of gedeeltelijk te schorsen, dan levert het BG de stap *Geschorst*. Hierdoor wordt de vlag "Geschorst" op "true" gezet bij het besluit.

### Schorsing opgeheven

Heft de rechter na verloop van tijd de schorsing op, dan levert het BG de stap *Schorsing opgeheven* en wordt de "Geschorst"-vlag  weer op "false" (=default waarde) gezet.

### Beroep(en) definitief afgedaan

Verklaart de rechter een beroep niet ontvankelijk en zijn er geen andere beroepen ingesteld, dan kan het BG ervoor kiezen om dit nog voor het einde van de beroepstermijn door te geven d.m.v. de procedurestap *Beroep(en) definitief afgedaan*. De datum `onherroepelijkVanaf` wordt weer gelijk gemaakt aan einde beroepstermijn. Mocht er voor het einde van de beroepstermijn opnieuw beroep worden aangetekend, levert het BG opnieuw de stap *Beroep(en) ingesteld*.

Is de beroepsprocedure definitief afgerond, geen hoger beroep meer mogelijk, of de termijn om in hoger beroep te gaan is ongebruikt verstreken, dan levert het BG de stap *Beroep(en) definitief afgedaan*.

De status van het besluit wordt onherroepelijk per aangeleverde datum. Een eventuele *schorsing*-vlag wordt automatisch opgeheven.


## Zie ook
* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).