# Informatieobject

## Inhoud deze sectie
In dit deel wordt uiteengezet wat een informatieobject (IO) is, welke onderdelen het heeft, welke soorten er zijn en hoe u kunt werken met een IO. 

## Wat is een informatieobject?
Een [informatieobject](begrippenlijst_informatieobject.dita#informatieobject) is een op zichzelf staand object voor het opslaan en via internet ontsluiten van informatie die niet op een mens-leesbare manier in de tekst van het besluit kan worden opgenomen. Een informatieobject bevat informatie die onderdeel is van de regels uit een regeling of besluit, maar die niet is vermeld in of bij de tekst. Dit object moet worden gebruikt als de informatie niet volgens STOP kan worden gecodeerd en/of niet-tekstueel / mens-leesbaar kan worden getoond. Informatieobjecten zijn bijvoorbeeld geen afbeeldingen, grafieken of tabellen die wél in een tekst(document) kunnen worden opgenomen.


## Vormen

Een IO wordt in sommige gevallen net als een regeling geconsolideerd en heeft daardoor twee verschijningsvormen:
* een informatieobject als onderdeel van een besluit.
* een geconsolideerd informatieobject. Geconsolideerde IO's zijn juridisch zelfstandig en kennen een eigen levenscyclus. Het informatieobject kent dan meerdere versies waarbij het consolidatieproces de periode van geldigheid bepaalt. Voor meer informatie zie [Consolidatie van een instrument](EA_AFAF97EA251247c9B2CC7B9922384BF8.dita#Pkg).

Het [geografisch informatieobject](gio-intro.md) (GIO) is een voorbeeld van een IO dat wordt geconsolideerd.
Een [PDF document](IO_Document.md) zal meestal onderdeel zijn van het besluit.


## Kenmerken

* Een IO volgt het [FRBR-model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg/29F89190FCE1470289A3566A3FC13663): een IO is een [work](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg/AB6FDC32D3F3474eBBE7438305EB0E26) dat door de tijd heen bestaat, maar op een moment in de tijd altijd tot uitdrukking komt in een [expression](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg/F689C5A0EC9B4b3491F3558906741DB8)  (specifieke versie).
* Een IO-wijziging wordt aangeleverd als volledige nieuwe versie, met een verwijzing naar de vorige versie.
* Het IO heeft een [unieke identificatie](io-expressionidentificatie.md) (JOIN-ID).




## Zie ook

* Overzicht van [bedrijfsregels voor metadata van informatieobjectversies](businessrules_data_InformatieObjectVersieMetadata.dita#Bedrijfsregels).
* Overzicht van [bedrijfsregels voor metadata van informatieobjecten](businessrules_data_InformatieObjectMetadata.dita#Bedrijfsregels).
* Overzicht van [bedrijfsregels voor GIO's](businessrules_geo_GeoInformatieObjectVersie.dita#Bedrijfsregels).
* [Hoe het juridisch zit met GIO's](vaststellen-wijzigen-gio.md).
* Uitleg over [GIO's](gio-intro.md).

* [Voorbeelden van GIO's met verschillende typen geometrische data](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Coderingen/GIO/GIO-varianten/index.md).
* [Hoe het juridisch zit met bijlagen](stop-bijlage.md).