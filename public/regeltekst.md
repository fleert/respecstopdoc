# Tekststructuur en -inhoud

De STOP-tekstmodellen van de juridische [instrumenten](regeltekst_instrument.md), zoals een besluit, regeling, kennisgeving, et cetera, zijn allemaal op dezelfde manier ontworpen. De opbouw van het STOP tekstmodel is het beste uit te leggen aan de hand van de tekst van een regeling.

Een lezer ziet de tekst als een doorlopend verhaal (lichtgrijs in de illustratie) dat is onderverdeeld in secties die herkenbaar zijn aan een kop of label (donkergrijs). Binnen het verhaal kan tekst een speciale opmaak hebben, bijvoorbeeld in de vorm van een lijst of een link naar andere teksten. Daarnaast kunnen multimediale elementen (zoals figuren) onderdeel zijn van de tekst.

![Structuur versus inhoud van tekst](img/regeltekst.png)

De tekstmodellen van STOP kijken op een andere manier naar de tekst. Als het gaat om het vastleggen van de tekst hanteert STOP de indeling van de tekst als leidraad. Als het gaat om het annoteren van teksten gaat STOP uit van de inhoud.

## Tekst in STOP

Een tekst is in STOP opgebouwd uit verschillende categorieën elementen: 

[Instrument](regeltekst_instrument.md)-elementen
: De 'Instrument' elementen zijn de rootelementen waarmee een juridisch instrument in STOP gecodeerd wordt. 

[Structuur](regeltekst_structuur.md)-elementen
: De 'Structuur' tekstelementen structureren de tekst van het instrument. 

[Koppen](regeltekst_koppen.md)
: De 'Koppen' categorie bevat elementen voor de naamgeving van de structuur. 

[Inhoud](regeltekst_inhoud.md)-elementen
:  De 'Inhoud' tekstelementen zijn de elementen 'onderaan' de structurele hiërarchie die gevormd wordt door de structuurelementen. 

[Bloktekst](regeltekst_bloktekst.md)-elementen
: De inhoud is opgebouwd uit 'Bloktekst' elementen met broodtekst. Elk bloktekst element voorziet de broodtekst van specifieke formattering. Het bloktekstelement [tabel](regeltekst_tabel.md) wordt apart toegelicht.

[Inline](regeltekst_inline.md)-elementen
: Waar de bloktekst elementen de volledige inhoud van formattering voorzien, kan met een 'Inline'-element een specifiek deel van de tekst geformatteerd worden, of kan een verwijzing in de tekst worden opgenomen. 

[Aantekening](regeltekst_aantekening.md)-elementen
: Specifieke situaties in de geconsolideerde regeling worden aangeduid met aantekening elementen.

[Renvooi](regeltekst_renvooi.md)-elementen
: Door middel van renvooi-elementen kunnen wijzigingen gecodeerd worden.

[Publicatieblad](regeltekst_publicatieblad.md)-elementen
: De 'Publicatieblad' tekstelementen zijn elementen die uitsluitend gebruikt worden in juridische publicatiebladen.

[Tabeltechnische](regeltekst_tabeltech.md)-elementen
: Enkele elementen die geen tekst bevatten, maar gebruikt worden om tabelstructuren te coderen.

### XML codering geeft de structuur

Teksten in STOP worden gecodeerd in XML. De codering in XML volgt de structuur van de tekst. Zo staan de XML elementen die de tekstblokken coderen in dezelfde volgorde in de XML als de tekstblokken in de tekst voorkomen. De structuur-, inhoud-, bloktekst- en aantekening-elementen worden via _nesting_ aan elkaar gerelateerd. De bloktekst-elementen uit een tekstblok zijn opgenomen in het inhoud-element dat het tekstblok codeert, en de inhoud-elementen zijn weer opgenomen in het structuurelement waarvan de kop voorafgaand aan het tekstblok getoond wordt. Structuurelementen die hiërarchisch onder een ander structuurelement vallen zijn in XML opgenomen binnen het bovenliggende element:

![](img/regeltekst_nesting.png)

Op het niveau van broodtekst is er geen strikt hiërarchisch XML model meer. Tekst en inline-elementen komen naast elkaar voor als inhoud van de bloktekst-elementen.

### Instrumentmodel: specificatie van structuur- en inhoud-elementen

STOP definieert voor verschillende toepassingen (instrumenten) tekstmodellen. Zo zijn er modellen voor de tekst van een regeling, maar ook voor besluiten of officiële publicaties.

De specificatie van een instrumentmodel geeft aan welke structuur- en inhoud-elementen aanwezig mogen of moeten zijn en in welke volgorde ze voorkomen. Op het niveau van het instrumentmodel worden geen beperking opgelegd aan welke bloktekst-elementen binnen een inhoud-element kunnen voorkomen.

Dat betekent niet dat elk element dat binnen een tekstblok kan voorkomen ook binnen elk inhoudelement van elk tekstmodel kan voorkomen. Sommige elementen hebben een specifieke functie die alleen binnen een bepaalde toepassing of aard van de tekst kan voorkomen. Zo is de aantekening `Vervallen` specifiek voor een regeling, en kan het inline-element `ExtIoRef` element niet in het bijschrift van een figuur staan. 

## Juridische inhoud van de tekst

Een tekst is ook op een andere manier op te delen: in juridisch relevante tekst en in tekst/opmaak die juridisch geen betekenis heeft. De juridische tekst zelf is verder op te delen in juridische inhoud en overige tekst.

### Juridische tekst

Met juridische tekst worden de tekstelementen aangeduid die juridische betekenis hebben. De juridische tekst van de regeling bestaat uit:

- de koppen van de structuurelementen;
- de inhoud-elementen met weglating van de niet-juridische inline-elementen.

In de documentatie van de [inline-elementen](regeltekst_inline.md) is aangegeven welke inline-elementen juridisch zijn en welke niet. De aantekening-elementen zijn nooit onderdeel van de juridische tekst. 

#### Aanpassen juridische tekst

De juridische tekst van een regeling kan alleen rechtsgeldig vastgesteld en gewijzigd worden indien voldaan wordt aan [wettelijke eisen](https://wetten.overheid.nl/jci1.3:c:BWBR0004287&z=2021-07-01&g=2021-07-01). Een van de eisen is dat een besluit van het bevoegd gezag of uitspraak van de rechter op de juiste manier wordt gepubliceerd. Pas na publicatie (officiële bekendmaking op http://www.officielebekendmaking.nl)is juridische tekst ook juridisch bindend.

### Juridische inhoud: regeltekst

In een deel van de juridische tekst is de regeling inhoudelijk uitgewerkt. Daarin zijn bijvoorbeeld de voorschriften of het beleid beschreven. De overige tekst bevat bijvoorbeeld de toelichting op de regeling. Het kleinste zelfstandig leesbare tekstblok waarin de regeling inhoudelijk is beschreven wordt regeltekst genoemd. Er zijn in STOP vier elementen die regeltekst kunnen bevatten:

- In het *Lichaam* van de regeling:
  - een [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel) zonder leden;
  - een [Lid](tekst_xsd_Element_tekst_Lid.dita#Lid) van een artikel;
  - een [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst).
- In een *Bijlage* van de regeling:
  - een [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst).

De begrenzing in de tekst van een artikel of lid zal aan de opmaak van de tekst te zien zijn. Dat geldt niet voor een `Divisietekst`: dat is bedoeld om doorlopende tekst te groeperen in zelfstandig leesbare eenheden. Wat _zelfstandig leesbaar_ is, is een inhoudelijke afweging die niet (altijd) van de indeling van de tekst is af te leiden.

#### Annotaties alleen bij regeltekst

STOP kent een mechanisme van [annotaties](annotaties.md) bij een regeling die onder andere gebruikt kunnen worden om de inhoud van een regeling te interpreteren of nader te duiden. Als daarbij verwezen moet worden naar de tekst van de regeling, dan zijn dat de _regeltekst_ elementen. Dit stelt software in staat om bij de interpretatie of duiding een zelfstandig leesbare tekst te tonen. Het wordt afgeraden om te verwijzen naar een element binnen de regeltekst (bijvoorbeeld een lijst-item) omdat de tekst daarvan in het algemeen niet te begrijpen is zonder de omliggende tekst te kennen. Het wordt ook afgeraden om te verwijzen naar structuurelementen in plaats van naar de individuele _regeltekst_ elementen als het voor kan komen dat de annotatie voor een van de _regeltekst_ elementen binnen het structuurelement kan afwijken.
