# Architectuur en ontwerpkeuzes

Deze leeswijzers beschrijven de structuur van de STOP-standaard, en geven een beeld van de eisen en uitgangspunten die zijn gehanteerd tijdens het ontwerp. 
Doelgroep van deze leeswijzers zijn architecten en ontwerpers van informatiemodellen.

[Modulaire structuur](modulaire_structuur.md)
: Beschrijft hoe de informatie die met de STOP-standaard uitgewisseld kan worden is ingedeeld in modules, en dat die modules voor het daadwerkelijk uitwisselen van informatie in API's verwerkt moeten worden.

[Tekststructuur en -inhoud](regeltekst.md)
: Beschrijft de verschillende modellen om dezelfde (juridische) tekst te beschrijven.

[Functioneel presenteren](functioneelpresenteren.md)
: Beschrijft het principe van functioneel verbeelden: alleen als het een functie heeft om de presentatie voor te schrijven is het onderdeel van de standaard.