# Tekstmodellen van Regelingen



De lopende (juridische) tekst van een regeling wordt vastgelegd in een van de onderstaande vier STOP-XML modules voor [tekstmodellen van regelingen](DITA_OnbekendeLink.dita#Oeps), waarvan drie een artikelstructuur hebben en één een vrije tekststructuur kent. De keuze voor het tekstmodel en het besluitmodel moeten congruent zijn (zie [Besluitmodellen](quickstart_2_Besluiten.md))



## **Artikelstructuur** 

Een artikelstructuur kent een hiërarchische indeling van artikelen (minimaal 1) over verschillende structuurelementen zoals [Afdelingen](tekst_xsd_Element_tekst_Afdeling.dita#Afdeling), [Hoofdstukken](tekst_xsd_Element_tekst_Hoofdstuk.dita#Hoofdstuk) en [Paragrafen](tekst_xsd_Element_tekst_Paragraaf.dita#Paragraaf). Deze tekststructuur is gangbaar in de meeste omgevingswetinstrumenten zoals het het omgevingsplan, de waterschapsverordening en de omgevingsverordening.



### 1 - [tekst:RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek)

Het [klassieke tekstmodel voor regelingen](regeling-klassiek.md) volgt de lijn van regelingen, besluiten en bekendmakingen zoals die gebruikelijk was alvorens de implementatie van STOP. Dit betekent onder andere dat elementen die tot het besluit horen (zoals [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef) en [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)) ook in de tekst van de regeling worden opgenomen.


### 2 - [tekst:RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact)

Het [compacte tekstmodel](DITA_OnbekendeLink.dita#Oeps) maakt een duidelijke scheiding tussen de actie van het besluiten en de regelinginhoud waar over besloten is. In een compacte regeling zitten dus geen teksten of elementen die tot het besluit behoren. 

[Voorbeeld RegelingCompact](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/01-BG-Regeling-v1-Tekst.xml)

### 3 - [tekst:RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel)

Dit tekstmodel wordt gebruikt om bij een voorbereidingsbesluit het overgangsrecht/parallelle geldigheid te faciliteren. 


## **Vrije tekststructuur**

Binnen een [vrije tekststructuur](begrippenlijst_vrijetekststructuur.dita#vrijetekststructuur) kan het bevoegd met behulp van [Divisies](tekst_xsd_Element_tekst_Divisie.dita#Divisie) zelf invulling geven aan de structuur van het [Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam). Deze tekststructuur is gangbaar in omgevingsvisies. 

### 4 - [tekst: RegelingVrijetekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) 






