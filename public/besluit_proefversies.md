# Proefversies

Een besluit bevat een juridisch accurate beschrijving van (de wijziging van) regelingen en informatieobjecten. Zeker als het om wijzigingen gaat kan het lastig zijn te doorgronden hoe het besluit doorwerkt in de regeling. Het is inzichtelijker om een complete versie van een regeling te tonen, zodat het effect van het besluit in context gelezen kan worden. Bij de complete versie van een regeling kunnen ook de annotaties gebruikt worden die de inhoud van de regeling duiden of interpreteren, wat niet mogelijk is als de regelingtekst nog onderdeel is van de tekst van het besluit.

Voor dit doel kent STOP de **proefversie**: de regelingversie (of informatieobjectversie) die volgt uit het besluit. Dit is de versie die is aangegeven in de `BeoogdeRegelgeving` in de [consolidatie-informatie](consolidatie-informatie.md) bij het besluit. De proefversie houdt geen rekening met andere besluiten: het is geen consolidatie maar een andere weergave van wat in het besluit is opgenomen.

Maken van de proefversies
: De regelingtekst voor de proefversie kan uit het besluit worden afgeleid door [reconstructie](renvooitekst_muteren.md) van de tekst op basis van renvooitekst. De volledige regelingtekst kan zowel met als zonder de renvooimarkeringen getoond worden. Voor informatieobjecten is de proefversie gelijk aan de versie die [opgenomen](besluit_informatieobject.md) is in het besluit.

[Modellering van de proefversies](besluit_proefversies_model.md)
: STOP kent een beschrijving van de proefversies voor een besluit die uitgewisseld kan worden om aan te geven welke proefversies er zijn. De beschrijving is het functionele equivalent van de beschrijving van [toestanden](consolideren_toestanden.md) voor de geconsolideerde regeling.

[Annotaties voor een proefversie](besluit_proefversies_annotaties.md)
: Als (STOP of non-STOP) annotaties voor een regeling of informatieobject niet bij elke versie worden meegeleverd, dan moet van de versiebeheer-informatie gebruikt gemaakt worden om de juiste versie van de annotatie bij de proefversie te vinden.