# Standaard tekstmodel

Het standaard model voor een regeling maakt onderscheid tussen een hoofdregeling en tijdelijk regelingdelen. In het algemeen bestaat een regeling alleen uit een hoofdregeling. Er zijn een [aantal situaties](regeling_artikelsgewijs_regelingdelen.md) waarbij het wijzigen en consolideren van de hoofdregeling technisch complex is. In dat geval wordt de regeling technisch gesplitst terwijl het juridisch nog steeds één regeling is. Gezien de samenhang tussen hoofdregeling en tijdelijk regelingdeel worden de modellen in samenhang beschreven.

STOP kent een standaard tekstmodel dat voor alle artikelsgewijze regelingen gebruikt wordt. Voor sommige Rijksregelingen wordt voor de hoofdregeling een afwijkend [klassiek model](regeling-klassiek.md) gebruikt. Het tekstmodel is ontleend aan de Aanwijzing voor de regelgeving (aanwijzingen [3.54](http://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=3&paragraaf=3.5&aanwijzing=3.54), [3.56](http://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=3&paragraaf=3.5&aanwijzing=3.56), [3.57](http://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=3&paragraaf=3.5&aanwijzing=3.57), [3.58](http://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=3&paragraaf=3.5&aanwijzing=3.58), [3.59](http://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=3&paragraaf=3.5&aanwijzing=3.59)) met enige nadere specificaties en toevoegingen.

![Tekstmodel voor artikelsgewijze regeling](img/Regeling-Artikelsgewijs.png)

De **hoofdregeling** bestaat uit:

[tekst:RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift)
: De officiële titel van de regeling, zoals (voor Rijksregelingen) in [paragraaf 4.5](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=4paragraaf=4.5) van de Aanwijzingen voor de regelgeving wordt beschreven. Bijvoorbeeld: [Mededingingswet](https://wetten.overheid.nl/BWBR0008691).

[tekst:Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam)
: Het lichaam bevat de voorschriften van de regeling, vastgelegd in artikelen([tekst:Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel)). Bij omvangrijke regelingen kan de tekst leesbaarder gemaakt worden door de artikelen in secties bij elkaar te brengen, en secties in secties te groeperen. Voor elk type sectie kent het tekstmodel een eigen element, met als hiërarchie: [tekst:Boek](tekst_xsd_Element_tekst_Boek.dita#Boek), [tekst:Deel](tekst_xsd_Element_tekst_Deel.dita#Deel), [tekst:Hoofdstuk](tekst_xsd_Element_tekst_Hoofdstuk.dita#Hoofdstuk), [tekst:Titel](tekst_xsd_Element_tekst_Titel.dita#Titel), [tekst:Afdeling](tekst_xsd_Element_tekst_Afdeling.dita#Afdeling), [tekst:Paragraaf](tekst_xsd_Element_tekst_Paragraaf.dita#Paragraaf), [tekst:Subparagraaf](tekst_xsd_Element_tekst_Subparagraaf.dita#Subparagraaf), [tekst:Subsubparagraaf](tekst_xsd_Element_tekst_Subsubparagraaf.dita#Subsubparagraaf). Welke elementen gebruikt mogen worden in een bepaald type regeling staat in het [toepassingsprofiel](toepassingsprofielen.md) voor de regeling. In een sectie mogen alleen andere secties voorkomen of alleen artikelen, maar geen mix van beiden. Zo kan een hoofdstuk paragrafen bevatten of artikelen, maar niet een aantal paragrafen en een aantal artikelen.

[tekst:Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)
: Een bijlage wordt gebruikt als de inhoud ervan niet goed is op te nemen in het lichaam. De redenen voor het opnemen van informatie in een bijlage zijn grofweg:  

* _vormgeving_: Bijvoorbeeld complexe tabellen behoeven vaak afwijkende formattering.
* _leesbaarheid_: Bijvoorbeeld lange lijsten zouden de tekstdoorloop teveel verstoren.
* _vindplaats_: Een bijlage kan bestaan uit een verwijzing naar een elders gepubliceerde informatie.
* _technisch_: Een bijlage kan uit niet-tekstuele informatie bestaan, zoals een gebiedsbegrenzing, audiobestand of configuratie voor een rekenprogramma. In STOP wordt dit type bijlage niet als bijlage bij de tekst opgenomen maar wordt als een [Informatieobject](io-intro.md) gemodelleerd.

    De inhoud van de bijlage is beschreven in [tekst:Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) elementen. Deze kunnen in een hiërarchie van secties ondergebracht worden, waarbij voor een sectie een [tekst:Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie) gebruikt wordt. In `Divisie` mogen andere `Divisie`s voorkomen, `Divisietekst` elementen of beiden.

Toelichting
: Voor een regeling kan een toelichting opgesteld worden die steeds wordt bijgewerkt als de regeling wijzigt. De toelichting wordt [apart](regeling_toelichting.md) beschreven.

Een **tijdelijk regelingdeel** bestaat uit onderdelen die ook in de hoofdregeling terugkomen, en daarnaast uit:

[tekst:Conditie](tekst_xsd_Element_tekst_Conditie.dita#Conditie)
: Een tijdelijk regelingdeel begint met een `Conditie` met daarin één of meer artikelen([tekst:Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel)) die aangeven hoe het tijdelijk regelingdeel zich verhoudt tot de hoofdregeling. In de conditie kan bijvoorbeeld staan dat het tijdelijk regelingdeel juridisch prioriteit heeft boven de inhoud van de hoofdregeling.

Technisch is het tijdelijk regelingdeel een apart work met een eigen identificatie. Het tijdelijke regelingdeel wijst altijd naar zijn hoofdregeling; deze verwijzing is onderdeel van de identificatie-STOP-module. Afgezien van het tekstmodel ziet een tijdelijk regelingdeel er hetzelfde uit als een gewone regeling: het wordt zelfstandig geconsolideerd, en alle STOP-modules die bij een gewone regeling toegestaan zijn, zijn ook bij een tijdelijk regelingdeel toegestaan.