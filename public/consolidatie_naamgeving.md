# Identificatie van geconsolideerde regelgeving

Voor de consolidatie van een regeling of informatieobject hanteert STOP een eigen naamgeving. De consolidatie krijgt twee identificaties:

* één voor de geconsolideerde regeling/informatieobject als work en
* één voor de versie (een expression) die in dit geval ook _toestand_ genoemd wordt.

STOP gebruikt een [invulling](naamgevingsconventie.md) van de Akoma Ntoso naamgevingsconventie (AKN NC) als basis voor de identificatie van een geconsolideerde regeling, en een invulling van de JOIN identificatie voor geconsolideerde informatieobjecten. Deze naamgevingsconventie wordt toegepast bij de ontsluiting van geconsolideerde regelgeving door de LVBB.

Voor zowel een regeling als een informatieobject zijn twee toestanden (expressions) verschillend (en hebben ze een andere expression-identificatie) als een van de condities geldt:
* De start van de inwerkingtreding (eerste [cons:juridischWerkendVanaf](cons_xsd_Element_cons_juridischWerkendVanaf.dita#juridischWerkendVanaf)) verschilt.
* De start van de geldigheid (eerste [cons:geldigVanaf](cons_xsd_Element_cons_geldigVanaf.dita#geldigVanaf)) verschilt.
* De lijst met doelen die bijdragen aan de inhoud van de toestand verschilt ([cons:gerealiseerdDoel](cons_xsd_Element_cons_gerealiseerdDoel.dita#gerealiseerdDoel) of [cons:inwerkingtredingsdoel](cons_xsd_Element_cons_inwerkingtredingsdoel.dita#inwerkingtredingsdoel) plus [cons:basisversieDoel](cons_xsd_Element_cons_basisversiedoel.dita#basisversiedoel)en).

Een toestand krijgt dus geen nieuwe expression-identificatie als de tekst van de regeling wijzigt als gevolg van het publiceren van een nieuwe regelingversie (zie [rectificatie, revisie](rectificatie_en_revisie.md)) voor een doel dat al onderdeel is van de toestand.

## Geconsolideerde regeling { #cons_reg }

Voor de geconsolideerde regeling wordt in de identificatie "**act**" gehanteerd:

**Work**: `"/akn/" <land> "/act/" <subtype> "/" <datum_work> "/" <regcode>`

**Expression**: `<work> "/" <taal> "@" <datum_g> [ ";" <datum_iwt> ]  [";" <versie> ]`

met

|code      |betekenis |
|---       |---       |
|land |Een 2-letter code van een land volgens ISO 3166-1 of van een code voor een subdivisie volgens ISO 3166-2. In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nl` ondersteund. |
|subtype   | Een nadere indeling van de regelingen; zie onderstaande tabel.|
|datum_work| Het jaar waarin de eerste toestand van de regeling in werking is getreden.|
|regcode    | De unieke code van de regeling volgens de nummeringsmethode ingesteld door het publicatieplatform voor (collecties van) de regelingen|
|taal | Taalcode volgens [ISO 639-2 alpha-3](https://www.iso.org/iso-639-language-codes.html). In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nld` ondersteund. |
|datum_g   | De datum van de eerste geldigheid van een toestand.|
|datum_iwt | De optionele datum van de inwerkingtreding van (het deel van) het besluit dat aan de toestand ten grondslag ligt. Deze datum wordt alleen vermeld als `<datum_iwt>` afwijkt van `<datum_g>`.|
|versie    | Een optioneel volgnummer (2, 3, ...) dat de versie aangeeft van de toestand. Volgnummer 1 wordt niet gebruikt.|

De code voor het subtype is:

|code|betekenis|
|--- |---      |
|internationaal|Verdragen |
|koninkrijk    |Regelgeving voor het koninkrijk |
|land          |Regelgeving voor een land |
|provincie     |Regelgeving voor een provincie |
|gemeente      |Regelgeving voor een gemeente |
|waterschap    |Regelgeving voor een waterschap |
|zbo           |Regelgeving voor een zbo |
|pbo           |Regelgeving voor een pbo |
|gr            |Regelgeving voor een gemeenschappelijke regeling |
|caropl        |Regelgeving voor Bonaire, Sint Eustasius of Saba: de Caraïbische openbare lichamen |

Uit de identificatie van de toestand is niet af te leiden welke besluiten er aan ten grondslag liggen; die informatie is onderdeel van de metadata van de toestand.

Voorbeelden
* `/akn/nl/act/land/1991/BWBR0005251`: Work "Wet op de accijns"
* `/akn/nl/act/land/1991/BWBR0005251/nld@2019-01-01`: De versie van de Wet op de accijns die geldig is vanaf 1 januari 2019.
* `/akn/nl/act/gemeente/2019/CVDR2020190/nld@2019-09-01;2`: De tweede versie die geldig is vanaf 1 september 2019 van de Toestand met nummer CVDR2020190 gepubliceerd in de collectie "gemeente"  

# Geconsolideerd informatieobject
Geconsolideerde informatieobjecten krijgen als identificatie:

**Work**: `"/join/id/regdata/consolidatie/" <datum_work> "/" <iocode>`

**Expression**: `<work> "/" <taal> "@"  <datum_g> [";"  <datum_iwt>]  [ ";" <versie> ]` 

De betekenis van de parameters is gelijk als voor de geconsolideerde regelingen, met als extra parameter

|code|betenis|
|--- |---    |
|iocode| De unieke code van het informatieobject volgens de nummeringsmethode ingesteld door het publicatieplatform. Deze code is opgebouwd volgens regex `CIO[0-9]{7}`: een prefix "CIO" (van ge**C**onsolideerde **I**nformatie**O**bject) gevolgd door 7 cijfers.|


Voorbeelden:
* `/join/id/regdata/consolidatie/2020/CIO0032561`: het work van een geconsolideerde IO met nummer "CIO0032561"
* `/join/id/regdata/consolidatie/2020/CIO0032561/nld@2021-06-01;2` : versie 2 van bovenstaande IO die geldig is vanaf 1 juni 2021.