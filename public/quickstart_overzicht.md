# Snelstartgids STOP-XML 

## Overzicht STOP-XML Modules

Deze leeswijzer beschrijft de invulling van de diverse STOP-XML modules per stap binnen het [proces van bekendmaken en consolideren](proces_overzicht.md). Het onderstaande figuur geeft een globaal overzicht van de STOP-XML modules per processtap.

<img src="img/quickstart_overzicht.png" alt="Over" style="zoom:70%;" />

De invulling van omgevingswetspecifieke informatie valt onder het Informatiemodel Omgevingswet (IMOW)  en is hier niet beschreven. Raadpleeg hiervoor de documentatie van Geonovum: <https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD>



# Stap 1: Opstellen nieuwe regelingversie en GIOversie met OW data

Het opstellen van een nieuwe versie van een regeling vindt plaats in de plansoftware van het bevoegd gezag. Het kan hierbij gaan om:

* een eerste versie van een nog niet bestaande regeling, ook wel initiële regeling genoemd.
* een bestaande regeling die door het bevoegd gezag wordt gewijzigd. Het wijzigen van een bestaande regeling stelt eisen aan het beheer van RegelingVersies in de plansoftware. 

Een regelingversie gaat gepaard met een STOP-XML modules 'data:ExpressionIdentificatie', 'data:RegelingMetadata' en 'data:RegelingVersieMetadata'. Deze STOP-modules evenals eventuele gerelateerde GIOversie(s) en OW data hebben een gemeenschappelijk ['doel'](data_xsd_Element_data_doel.dita#doel); een Identificatie van de introductie van nieuwe of aangepaste regelgeving met één moment van inwerkingtreding, één moment waarop de regelgeving geldig wordt en eventueel één moment waarop de geldigheid van de regelgeving eindigt. Zie ook: [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md) (bij het Besluit). Het is van belang dat reeds in stap 1 de samenhang (tussen RegelingVersie, GIOversie en OW-data) geborgd is met een doel in de plansoftware. 

**STOP-XML Modules  nieuwe regelingversie**

* [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieRegeling.md) van de Regeling
* [data:RegelingMetadata](quickstart_1_RegelingMetadata.md)
* [data:RegelingVersieMetadata](quickstart_1_RegelingMetadata.md)
* [Regeling](quickstart_1_Regelingen.md) met hierbinnen de keuze uit vier tekstmodellen/STOP-XML modules

**STOP-XML Modules Nieuwe GIOVersie**

* [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieGIO.md) van een GIO
* [data:InformatieObjectMetadata](quickstart_1_InformatieObjectMetadata.md)
* [data:InformatieObjectVersieMetadata](quickstart_1_InformatieObjectMetadata.md)
* [geo:GeoInformatieObjectVaststelling](quickstart_1_GeoInformatieObjectVaststelling.md)

# Stap 2: Aanleveren Besluit 

Op een bepaald moment legt het bevoegd gezag de nieuwe Regelingversie en/of GIOversie vast in een bekend te maken besluit in haar plansoftware.  Een Besluit kan:

- een nieuwe versie van een Regeling bekendmaken (via een initieel besluit); of
- de wijzigingen op een bestaande versie van een regeling bekendmaken (via een wijzigingsbesluit).

Een besluit dient aangeleverd te worden met de set samenhangende STOP-modules die hieronder beschreven zijn. In de aanlevering aan de LVBB wordt daarnaast ook informatie van de wordtVersie van de Regeling (de [ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieRegeling.md) en [RegelingMetadata](quickstart_1_RegelingMetadata.md)), GIOversie(s) en OW-data meegeleverd (zie ook het [bronhouderkoppelvlakschema](@@@BHKV_URL@@@)).

**STOP-XML Modules Besluit**

* [data:ExpressionIdentificatie](quickstart_2_ExpressionIdentificatieBesluit.md) van het Besluit
* [data:BesluitMetadata](quickstart_2_BesluitMetadata.md) (met onderscheid hierbinnen tussen definitieve en ontwerpbesluiten)
* [data:ProcedureVerloop](quickstart_2_ProcedureVerloop.md) 
* [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md)
* [Besluit](quickstart_2_Besluiten.md) met hierbinnen de keuze uit twee modellen/STOP-XML modules

# Stap 2a: Optioneel: Kennisgeving bij Besluit

Bij een besluit kan een [kennisgeving](begrippenlijst_kennisgeving.dita#kennisgeving) worden geschreven. Dit is een eigen instrument dat naar een besluit wijst. Net als een besluit bestaat een kennisgeving uit een set samenhangende STOP-modules.

* [Introductie, model en voorbeeld van de Kennisgeving](quickstart_2a_Kennisgeving.md)
* [data:ExpressionIdentificatie](quickstart_2a_ExpressionIdentificatieKennisgeving.md) van de kennisgeving
* [data:KennisgevingMetadata](quickstart_2a_KennisgevingMetadata.md) van de kennisgeving

# Stap 3: Consolideren na bekendmaking

Na bekendmaking van een definitief besluit met een juridische inwerkingtredingsdatum kan de LVBB de Toestanden berekenen van de regeling en de GIO(s) die uit het aangeleverde besluit volgen. Ontwerpbesluiten treden niet in werking. O.b.v. een ontwerpbesluit kan een proefversie van een instrument worden samengesteld. 

**STOP-XML Modules Consolidatie Regeling n.a.v. definitief Besluit**

* [cons:ConsolidatieIdentificatie](quickstart_3_ConsolidatieIdentificatie.md)
* [cons:Toestanden](quickstart_3_Toestanden.md)

**STOP-XML Module Proefversie n.a.v. ontwerpbesluit**

* [cons:Proefversies](quickstart_3_Proefversies.md)


## Voorbeelden

* [Voorbeeld volledige proces o.b.v. een definitief compact besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/index.md)
* [Voorbeeld van een ontwerpbesluit met een kennisgeving](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/index.md)
