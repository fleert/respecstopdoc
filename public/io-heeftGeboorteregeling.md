# HeeftGeboorteregeling

Het element [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) wordt **alleen** gebruikt **voor consolideerbare informatieobjecten**, zoals een [GIO](gio-intro.md). Het geeft aan samen met welk regeling-*work* de IO-versie is bekendgemaakt en dus juridisch geldig is geworden.

Bij het maken van een consolideerbaar informatieobject is de geboorteregeling altijd bekend. Elke consolideerbare informatieobjectversie heeft een geboorteregeling. De geboorteregeling drukt de juridische relatie uit tussen de regeling en het informatieobject: ze vormen juridisch een samenhangend geheel. Als de geboorteregeling wordt ingetrokken, dan komt het informatieobject ook te vervallen.

Het is mogelijk dat een IO een andere geboorteregeling krijgt, ofwel: dat het informatieobject door een andere regeling wordt "geadopteerd". De versies ná de "geboorteregeling-wijziging" wijzen dan naar de andere regeling.

## Uitgangspunten

1. De `heeftGeboorteregeling` bevat de JOIN-ID van het *work* van de geboorteregeling. 
1. De [`soortWork`](data_xsd_Element_data_soortWork.dita#soortWork) van de JOIN-ID is altijd `/join/id/stop/work_019` (Regeling).
1. Het IO met `heeftGeboorteregeling` is een GIO. Het IO heeft dus als `soortWork` `/join/id/stop/work_010` (informatieobject) èn als `formaatInformatieobject`  `/join/id/stop/informatieobject/gio_002` (geografisch informatieobject).
1. IO-versies van hetzelfde IO hoeven niet dezelfde `heeftGeboorteregeling` te hebben.

## Voorbeeld 
```
<data:heeftGeboorteregeling>/akn/nl/act/gm555/2020/REG0001</data:heeftGeboorteregeling>
```