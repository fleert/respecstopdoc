# Overzicht van complete toestanden

## Relatie met actuele toestanden
Naast een overzicht van [actuele toestanden](consolideren_toestanden.md) kent STOP ook een overzicht van toestanden waarin alle informatie verwerkt is voor tijdreizen over [vier tijdassen](regelgeving_in_de_tijd.md). Een toestand wordt beschreven door hetzelfde model als bij de actuele toestanden, waarbij in plaats van _Juridisch werkend op_ de tijdvakken _Juridisch werkend op_ + _Geldig op_ zijn opgenomen volgens het [bi-temporele-model](tijdreizen-bi-temporeel-model.md).

De toestanden in het complete model worden hebben een andere modellering, maar gebruiken dezelfde [naamgeving](consolidatie_naamgeving.md) als de actuele toestanden en ook dezelfde criteria om te bepalen of een toestand identiek is aan een andere toestand. De actuele toestanden komen (met een andere modellering van de geldigheid) terug in het overzicht van complete toestanden, maar niet alle complete toestanden corresponderen met een actuele toestand. Dat wordt geillustreerd in het voorbeeld: 

![Relatie complete en actuele toestanden](img/consolideren_compleet_actueel.png)

In dit voorbeeld worden voor doel 0, A, B, C, D en E besluiten gepubliceerd die opeenvolgend in werking treden, behalve C en D die tegelijk in werking treden. De regelingversie in besluit C werkt terug (wordt geldig) op een datum vóór de inwerkingtreding van het besluit voor doel A. In het voorbeeld zijn de verschillende toestanden met steeds een andere kleur weergegeven. De stippellijntjes geven de opdeling van de toestand in perioden van _Juridisch werkend op_ + _Geldig op_ weer. De constructie van het bi-temporele model wordt [later](#voorbeeld_toestanden) toegelicht.

De actuele toestanden corresponderen met de complete toestanden die liggen in het gebied _Juridisch werkend op_ ≤ _Geldig op_ (rechter helft van het bi-temporele diagram) waarvoor de _Juridisch werkend op_ samenvalt. Dit zijn ook de toestanden die doorsneden worden door de lijn waarvoor _Juridisch werkend op_ = _Geldig op_. De overige toestanden, met _Juridisch werkend op_ > _Geldig op_, zullen op geen enkel moment geldig zijn. In het voorbeeld zijn dat de toestanden:

* (0) C: inwerkingtredingsdoel C, basisversie-doelen: 0
* (0,A) C: inwerkingtredingsdoel C, basisversie-doelen: 0, A

Omdat het bevoegd gezag daarvoor geen consolidatieplicht heeft, zal de inhoud van de toestand onbekend zijn.

## Samenstellen complete toestanden

Het samenstellen van het overzicht van complete toestanden wordt hier beschreven alsof er nog niets over toestanden bekend is, voor alle informatie die tot en met een bepaalde datum _Bekend op_ publiek is geworden en die tot en met een bepaalde datum _Ontvangen op_. In de praktijk zal het interne datamodel van een STOP-gebruikende applicatie bijgewerkt worden als een nieuwe publicatie (of beschikbaarstelling) gedaan wordt, en zal het overzicht daaruit af te leiden zijn. Zonder kennis van het interne datamodel van software is het bijwerken niet te beschrijven. Vandaar dat hier beschreven wordt hoe het overzicht uit de uitgewisselde informatie afgeleid moet worden.

De afleiding wordt beschreven voor een regeling, maar werkt op dezelfde manier voor een te consolideren informatieobject.

### 1. Selectie van doelen
Dit is gelijk aan de werkwijze voor het [samenstellen van actuele toestanden](consolideren_toestanden_bepalen.md) (rekening houdend met wat op de gegeven _Bekend Op_/_Ontvangen op_ datums aan informatie beschikbaar is).

### 2. Maken van toestanden en datum juridisch uitgewerkt { #maken_toestanden }
Voor elk doel wordt gerekend met de laatst uitgewisselde waarde voor de tijdstempels (rekening houdend met wat op de gegeven _Bekend Op_/_Ontvangen op_ datums aan informatie beschikbaar is).

1. De doelen worden gesorteerd op volgorde van _juridisch werkend vanaf_ datum, en daarna op _geldig vanaf_ datum.
2. In deze volgorde, voor elke combinatie van datums jwv = _juridisch werkend vanaf_ en gv = _geldig vanaf_:
    a. In het bi-temporatele model wordt een nieuwe gebied afgebakend met _juridisch werkend vanaf_ ≥ jwv en _geldig vanaf_ ≥ gv
    b. Waar dat gebied geen andere toestanden overlapt is de geldigheid van een nieuwe toestand met:
        i. Inwerkingtredingsdoelen: alle doelen waarvoor _juridisch werkend vanaf_ = jvw en _geldig vanaf_ ≤ gv
        ii. Basisversie-doelen: geen.
    c. Waar dat gebied een andere toestand overlapt die voor dezelfde voor dezelfde jwv is aangemaakt: de overlapping is de geldigheid van een nieuwe toestand met:
        i. Inwerkingtredingsdoelen: alle doelen waarvoor _juridisch werkend vanaf_ = jvw en _geldig vanaf_ ≤ gv
        ii. Basisversie-doelen: de basisversie-doelen van de andere toestand
    d. Waar dat gebied een andere toestand overlapt die voor een eerdere jvw is gemaakt: de overlapping is de geldigheid van een toestand met:
        i. Inwerkingtredingsdoelen: alle doelen waarvoor _juridisch werkend vanaf_ = jvw en _geldig vanaf_ ≤ gv
        ii. Basisversie-doelen: de basisversie-doelen plus de inwerkingtredingsdoelen van de andere toestand.
3. Als een doelen een _Intrekking = ja_ heeft, dan wordt de datum _juridisch uitgewerkt_ van de module op de datum jvm = _juridisch werkend vanaf_ van het doel gezet. Alle toestanden die doorsneden worden door de lijn _juridisch werkend op_ = jvw krijgen in _juridisch werkend op_ een einddatum jvm voor de juridische werking. Alle toestanden die doorsneden worden door de lijn _geldg op_ = jvw krijgen in _geldig op_ de einddatum jvm voor de geldigheid. Toestanden voor doelen met een inwerkingtreding na jvm komen te vervallen.

Stap 2 is voor het voorbeeld [uitgewerkt](#voorbeeld_toestanden).

Een toestand krijgt een expression identificatie volgens de [naamgevingsconventie](consolidatie_naamgeving.md). Bij het toekennen van het versienummer moet rekening gehouden worden met de inwerkingtredingsdoelen en basisversie-doelen. Als er in het verleden al eens een actuele toestand met dezelfde inwerkingtreding, geldig vanaf en doelen is geweest, dan moet het versienummer daarvan opnieuw gebruikt worden. Zo niet, dan moet een nieuw versienummer gekozen worden. De complete toestanden en de overeenkomstige actuele toestanden krijgen op deze manier dezelfde identificatie.

### 3. Bepalen van de inhoud van een toestand
Dit is gelijk aan de werkwijze voor het [samenstellen van actuele toestanden](consolideren_toestanden_bepalen.md) (rekening houdend met wat op de gegeven _Bekend Op_/_Ontvangen op_ datums aan informatie beschikbaar is).


### 4. Bepalen materieel uitgewerkt
Dit is gelijk aan de werkwijze voor het [samenstellen van actuele toestanden](consolideren_toestanden_bepalen.md) (rekening houdend met wat op de gegeven _Bekend Op_/_Ontvangen op_ datums aan informatie beschikbaar is).

### 5. Bepalen van annotatieversies

Dit is gelijk aan de werkwijze voor het [consolideren van annotaties](consolideren_annotaties.md) (rekening houdend met wat op de gegeven _Bekend Op_/_Ontvangen op_ datums aan informatie beschikbaar is). Waar gesproken wordt over een _toestand die direct daarvoor in werking is getreden_ (voorgaande toestand) moet voldaan worden aan het criterium dat de optelsom van _Inwerkingtredingsdoelen_ en _Basisversie-doelen_ van de voorgaande toestand is gelijk aan de _Basisversie-doelen_ van de opvolgende toestand.

Net als bij de actuele toestanden kan het voorkomen dat er voor een toestand geen versie van een regeling/informatieobject is uitgewisseld, maar dat wel bepaald kan worden welke versie van een annotatie van toepassing is.

## Voorbeeld: maken van toestanden { #voorbeeld_toestanden }
Als toelichting op het maken van de (complete) toestanden wordt het bi-temporele model voor het voorbeeld aan het begin van dit hoofdstuk stap voor stap opgebouwd. De nummers verwijzen naar het [stappenplan](#maken_toestanden). 

| Bi-temporeel model | Beschrijving van de verandering |
| ----- | ----- |
| ![Toestanden voor doel 0](img/consolideren_compleet_0.png) | Er ontstaat één toestand T1 voor doel 0 door toepassing van stap 2b met inwerkingtredingsdoel 0. T1 komt overeen met de actuele toestand voor doel 0.  |
| ![Toestanden voor doel A](img/consolideren_compleet_A.png) | Er ontstaat één toestand T2 voor doel A door toepassing van stap 2d met T1 als andere toestand. De basisversie-doelen zijn 0 en inwerkingtredingsdoel is A. T2 komt overeen met de actuele toestand voor doel A. De geldigheidsbeschrijving van de toestand T1 krijgt daardoor twee _juridisch op_ perioden: één voor direct na de inwerkingtreding van T2 met oneindige geldigheid, één vanaf de inwerkingtreding van T2, met geldigheid tot de start geldigheid van T2.  |
| ![Toestanden voor doel B](img/consolideren_compleet_B.png) | Er ontstaat één toestand T3 voor doel B door toepassing van stap 2d met T2 als andere toestand. De basisversie-doelen zijn 0,A en inwerkingtredingsdoel is B. T3 komt overeen met de actuele toestand voor doel B. De geldigheidsbeschrijving van de toestand T2 krijgt daardoor twee _juridisch op_ perioden: één voor direct na de inwerkingtreding van T3 met oneindige geldigheid, één vanaf de inwerkingtreding van T3, met geldigheid tot de start geldigheid van T3.  |
| ![Toestanden voor doel C](img/consolideren_compleet_C.png) | Er ontstaan meerdere toestanden voor doel C omdat het gebied uit stap 2a overlapt met meerdere toestanden. De nieuwe toestanden ontstaan vanwege stap 2d en hebben alle inwerkingtredingsdoel C. Toestand T4 heeft T1 als andere toestand: basisversie-doel is 0. Toestand T5 heeft T2 als andere toestand: basisversie-doelen zijn 0,A. Toestand T6 heeft T3 als andere toestand: basisversie-doelen zijn 0,A,B. De geldigheidsbeschrijving van de toestand T1 krijgt daardoor drie _juridisch op_ perioden: één voor direct na de inwerkingtreding van T1 met oneindige geldigheid, één tussen de inwerkingtreding van T2 en T4 met geldigheid tot de start van de geldigheid van T2, één vanaf de inwerkingtreding van T4, met geldigheid tot de start geldigheid van T4.  |
| ![Toestanden voor doel D](img/consolideren_compleet_D.png) | Er ontstaat één toestand T7 voor doel D door toepassing van stap 2c met T6 als andere toestand. De basisversie-doelen zijn 0,A,B en inwerkingtredingsdoelen C,D. Deze toestand komt overeen met de actuele toestand voor doelen C en D.  |
| ![Toestanden voor doel E](img/consolideren_compleet_E.png) | Er ontstaat één toestand T8 voor doel E door toepassing van stap 2d met T7 als andere toestand. De basisversie-doelen zijn 0,A,B,C,D en inwerkingtredingsdoel is E. T8 komt overeen met de actuele toestand voor doel E. De geldigheidsbeschrijving van de toestand T7 krijgt daardoor twee _juridisch op_ perioden: één voor direct na de inwerkingtreding van T8 met oneindige geldigheid, één vanaf de inwerkingtreding van T8, met geldigheid tot de start geldigheid van T8.  |

### Zie ook:

In onderstaande XML voorbeelden is een voorbeeldmodule complete toestanden opgenomen:

| Voorbeeld         | XML voorbeeld                                                | Complete toestanden                                          |
| ----------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Initiële regeling | [Nieuwe regeling met iwt](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt) | [04-LVBB-CompleteToestanden-Regeling-v1.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/04-LVBB-CompleteToestanden-Regeling-v1.xml) |
| TWK wijziging     | [Terugwerkende kracht](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/10%20Terugwerkende%20kracht) | [24-LVBB-CompleteToestanden-Regeling-v3.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/10%20Terugwerkende%20kracht/24-LVBB-CompleteToestanden-Regeling-v3.xml) |