# Tekstmodel officiele publicatie

Elke [officiële publicatie](tekst_xsd_Element_tekst_OfficielePublicatie.dita#OfficielePublicatie) betreft één publicatieblad. En elk blad publiceert één document.

STOP kent de volgende publicatiebladen: de staatscourant, het staatsblad, het Gemeenteblad, het Provinciaalblad, het Waterschapsblad en het blad gemeenschappelijke regeling.

De tekstmodellen van alle publicatiebladen lijken heel erg op elkaar. Ze bestaan uit:

* een [Bladaanduiding](tekst_xsd_Element_tekst_Bladaanduiding.dita#Bladaanduiding) met de naam van het blad en de naam van het bevoegd gezag,
* de inhoud van het te publiceren document (een besluit, rectificatie, kennisgeving of mededeling).

Net als alle andere STOP-tekstmodellen beperkt het tekstmodel van de officiele publicatie zich tot de [structuur en inhoud](regeltekst.html). De opmaak van de tekst is geen onderdeel van het tekstmodel. Dit wordt overgelaten aan het portaal dat de publicatie verzorgd.



**Zie ook**

* Het [informatiemodel](EA_E0EFE05F9FBB45c6AB2493B9FF9037EC.dita#Pkg) van de officiële publicatie.
* [XML schema](tekst_xsd_Element_tekst_OfficielePublicatie.dita#OfficielePublicatie) van de officiële publicatie.
* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/03-LVBB-OffPub-Besluit-Tekst.xml) van een officiële publicatie van een besluit in het Provinciaal blad van Noord-Brabant.