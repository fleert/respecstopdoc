# Wat is een regeling?



Een regeling in STOP is het instrument waarmee regelgeving en beleid wordt vastgelegd. Een regeling bestaat uit gestructureerde tekst, eventueel aangevuld met annotaties.

De regeling in STOP is een informatiekundig model voor een deel van de tekst die uiteindelijk in het [besluit](besluit_modellering.md) terecht komt. Het besluit wordt bekendgemaakt en heeft juridische status. De regeling is dat deel van het besluit dat daarna gebruikt wordt om de voorschiften of beleidsregels die uit het besluit volgen, voor eenieder inzichtelijk te maken. De verdeling in tekst tussen een (STOP) regeling en besluit is noodzakelijk om een [geautomatiseerd creatieproces](automatiseerbaar-creatieproces.md) mogelijk te maken. Het modelmatig verdelen van 

[Tekststructuur en -inhoud](regeltekst.md)
: Het principe achter de tekstmodellen van STOP. Een lezer ziet de regeling als een doorlopende tekst. In STOP wordt de tekst gecodeerd met elementen die de aard van de tekst aanduiden. Dit biedt software de mogelijkheid op verschillende manieren door de tekst te navigeren of de tekst in fragmenten te presenteren.




**TODO**

[Annotaties](annotaties.md)
: Een [annotatie](begrippenlijst_annotatie.dita#annotatie) bevat een interpretatie of duiding van de (juridische) inhoud van een regeling of informatieobject die voor software te begrijpen is. De annotatie bevat dus geen nieuwe juridische informatie; die is immers voor een mens af te leiden door de regeling of informatieobject te bestuderen. De informatie is alleen op een andere manier gemodelleerd, zodat software er mee kan werken, en kan verwijzingen bevatten naar informatie die niet in STOP gemodelleerd is. STOP kent ook mogelijkheden om non-STOP annotaties synchroon te houden met juridische informatie, mits die annotaties aan bepaalde eisen voldoen.


<!-- Een regeling bestaande uit tekst en annotaties, informatieobjecten als niet-tekstueel onderdeel van regels (Bkw art 7 lid 1). Informatie over tekstmodellering, incl RegelingKlassiek in geconsolideerde vorm (placeholders voor wijzigartikelen en nog geen iwt-artikelen). Koppelen van niet-STOP informatie (bijv IMOW) via verwijzing naar regeling-work + doel (branch), naar analogie van de STOP-annotaties. -->
Een regeling is het juridische instrument waarmee regelgeving en beleid wordt vastgelegd. Een regeling wordt uitgedrukt in tekst. 

[Tekststructuur en -inhoud](regeltekst.md)
: Het principe achter de tekstmodellen van STOP. Een lezer ziet de regeling als een doorlopende tekst. In STOP wordt de tekst gecodeerd met elementen die de aard van de tekst aanduiden. Dit biedt software de mogelijkheid op verschillende manieren door de tekst te navigeren of de tekst in fragmenten te presenteren.

[Structuurelementen, inhoud en inline elementen](regeltekst_elementen.md)
: De elementen waarmee een tekst is opgebouwd kunnen verdeeld worden in drie categorie�n: structuur, inhoud en inline. 

[Elementen zijn adresseerbaar](regeltekst_adresseerbaar.md)
: Elk tekstelementen wordt voorzien van twee identifiers: ��n waarmee de plaats in de structuur van de tekst van de huidige versie wordt aangegeven en ��n waarmee aangeduid wordt dat een tekstelement dezelfde inhoud heeft als in een andere versie van de tekst.

[Verwijzingen naar tekstelementen](regeltekst_verwijzen.md)
: Tekstverwijzingen worden machine leesbaar gemaakt door gebruik te maken van de identifiers van de tekstelementen.

[Juridische en niet-juridische tekst](regeltekst_juridisch.md)
: Een regeling bevat vaak naast de juridisch geldende tekst ook niet-juridische tekst. 

[Presentatie van tekst](tekstpresentatie_2.md)
: Tekstelementen in STOP bevatten uitsluitend informatie voor functionele presentatie. Een tekst kan bijvoorbeeld als kop aangeduid worden, maar daarbij kan niet aangegeven worden in welk lettertype of welke font grootte de kop getoond moet worden. Dat bepaald het medium dat tekst aan de lezer presenteert.



TODO hoofdlijn terug laten komen:

- Als mens zie je een doorlopende tekst.
- Als machine wil je     begrijpen hoe de tekst in elkaar zit, om bijvoorbeeld functionaliteit om     (delen uit) de tekst te presenteren of op te zoeken te realiseren. Daarom     heeft STOP een uitgebreid tekstmodel en is het gebruik van ongestructureerde     tekst (zoals in een PDF bestand) niet voldoende.
- De eerste indeling van     de tekst gaat over structuur: lichaam, bijlagen, toelichting,     hoofdstukken, paragrafen. STOP kent structuurelementen die specifiek zijn     voor het type tekst (type regeling, maar ook besluit, rectificatie, etc).
- Voor regelingen wordt de     rest van de tekst opgedeeld in zelfstandig leesbare delen: regeltekst     eenheden. Dat is een stukje tekst dat apart getoond kan worden en dan nog     steeds begrijpelijk is � (volgens mij staat �zelfstandig leesbaar� ergens     beschreven). In STOP zijn dat artikel (zonder leden), lid en divisietekst
- De duiding of     interpretatie van tekst wordt in STOP gemodelleerd als annotatie. Ook     andere standaarden kunnen annotaties beschrijven. Annotaties verwijzen in     principe naar een regeltekst en niet naar tekst binnen een regeltekst. De     STOP tekstmodellen zitten zo in elkaar dat de verwijzing naar regeltekst     ongewijzigd blijft ook al wordt de tekst gewijzigd (dwz de verwijzing     blijft werken). Dat is niet het geval voor tekst binnen een regeltekst.



