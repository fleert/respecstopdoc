# C2. Regelingopschrift en ondertekening

Om het [regelingopschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) en/of de [ondertekening](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting) van een gepubliceerde besluit te corrigeren moet de rectificatie, correcties in de tekst van het besluit aanleveren. 

Voor het corrigeren van het regelingopschrift moet ook de besluitmetadata aangepast worden.



## Gecorrigeerde regeling

De regeling zelf wijzigt niet, dus voor een rectificatie van het regelingopschrift en/of de ondertekening worden geen STOP modules die betrekking hebben op de regeling meegeleverd.



## Gecorrigeerde besluitmodules

De correctie(s) hebben gevolgen voor de met het besluit meegeleverde informatie. Om het besluit te corrigeren is nodig:
- [ProcedureverloopMutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie): Geen aanpassingen, niet meeleveren.

- [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie): Geen aanpassingen, niet meeleveren.

- [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata): Bij alleen een correctie van de ondertekening, wordt de besluitMetadata niet aangepast en niet meegeleverd. 
  Bij een correctie van het regelingopschrift wel: het regelingopschrift moet als [officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel) opgenomen worden in de besluitmetadata. De gecorrigeerde besluitmetadata moet ook de ongewijzigde elementen bevatten.
  
  

## De rectificatie zelf

De rectificatie zelf bestaat uit:

* [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van de rectificatie zelf. De rectificatie van een besluit is een zelfstandig Work en elke rectificatie bij hetzelfde besluit is een nieuwe expressie van het rectificatie Work;

* [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata): Net als bij een besluit moet er bij een rectificatie altijd metadata aangeleverd worden. De RectificatieMetadata lijkt erg op de BesluitMetadata, maar heeft een extra veld [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert);

* [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie): De rectificatie bestaat uit:
  * Een [regeling opschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift), bijv. 'Rectificatie van de Verordening Toeristenbelasting 2017'

  * Het [lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met één of meerdere:
    * [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)(en) met 
      * een [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop);
      * een of meer toelichtende [alinea](tekst_xsd_Element_tekst_Al.dita#Al)'s;
      * een [Besluitmutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie).
      
        De besluitmutatie lijkt erg op een regelingmutatie, maar wijzigt de besluittekst. De *was* van de mutatie is het originele besluit. De *wordt* is het besluit wat *zou* ontstaan na verwerking van de wijzigingen uit de rectificatie. *Zou* omdat de besluittekst zelf niet geconsolideerd wordt. LET OP: er worden voor de rectificatie wel afwijkende standaard zinnen gebruikt om de [*wat*](tekst_xsd_Element_tekst_Wat.dita#Wat) aan te duiden.

Zie voor een voorbeeld de [STOP-codering](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/C2-Besluitmutatie-Tekst.xml) van de _'Rectificatie van de Wet van 27 juni 2008 tot wijziging van de Rijkswet op het Nederlanderschap ter invoering van een verklaring van verbondenheid, en tot aanpassing van de regeling van de verkrijging van het Nederlanderschap na erkenning'_ uit de [voorbeelden uit de staande praktijk](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/index.md).