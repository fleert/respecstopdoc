# Introductie

Dit is de documentatie van versie @@@IMOPVERSIE@@@ van de Standaard Officiële Publicaties die is gepubliceerd op @@@DATUM@@@. 

## Documentatie
De documentatie bestaat uit:

- de [Standaard Officiële Publicaties (STOP)](stop_index.md) met de uitgangspunten en de standaard zelf in tekst en als informatiemodel; en 
- het [Implementatiemodel Officiële Publicaties (IMOP)](imop_index.md), de implementatie van de standaard in XML. 

Naast de documentatie op deze site omvat de standaard ook een [Gitlab](https://gitlab.com/koop/STOP/standaard/-/tree/MeestRecent) repository met de XML schema's, schematrons en voorbeeldbestanden. Ook is er een [begrippenlijst](begrippenlijst.md). 

### Wijzigingsoverzicht

Zowel [STOP](stop_wijzigingsoverzicht.md) als [IMOP](imop_wijzigingsoverzicht.md) hebben een eigen wijzigingsoverzicht.



## Relatie met LVBB

STOP wordt gebruikt door de Landelijke Voorziening Bekendmaken en Beschikbaarstellen (LVBB). Het [bronhouderkoppelvlak](@@@BHKV_URL@@@) van de LVBB heeft eigen documentatie. Het bronhouderkoppelvlak houdt ook rekening met domein-specifieke afspraken en informatie die gebruikt maakt van STOP maar die in andere standaarden is vastgelegd:

- [Toepassingsprofielen Omgevingsdocumenten(TPOD)](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD#TPOD) beschrijven de toepassing van STOP voor Omgevingswet-besluiten en -regelingen.
- [Informatiemodel Omgevingswet](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD#TPOD) beschrijft de uitwisseling van domein-specifieke informatie uit de Toepassingsprofielen Omgevingsdocumenten ten behoeve van het [DSO-LV](https://aandeslagmetdeomgevingswet.nl/digitaal-stelsel/).



## Ondersteuning
Een beschrijving van de manier waarop de standaard ondersteund wordt is te vinden bij [Ondersteuning](ondersteuning.md).
