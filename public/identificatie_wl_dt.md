# Waardelijsten en datatypes in identificatie

## Datum
Datums worden geschreven volgens ISO 8601: YYYY-MM-DD zonder tijdsdeel. 

Enige uitzondering is [data:gemaaktOp](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp), deze wordt uitgedrukt als YYYY-MM-DD'T'HH:MM:SS'Z'. Waarbij T aangeeft dat een tijdsaanduiding volgt en Z staat voor de tijdzone UTC.

## Taal {#taal}
AKN NC schrijft het gebruik voor van een code volgens [ISO 639-2 alpha-3](https://www.iso.org/iso-639-language-codes.html). Voor STOP zijn de relevante codes:

|code|betekenis |
|--- |---       |
|nld |Nederlands|
|eng |Engels    |
|fry |West Fries|
|pap |Papiamento|
|mul |Meerdere talen in één versie|
|und |Taal is onbekend|

In STOP versie @@@IMOPVERSIE@@@ is `nld` de enig toegestane taal. In een latere versie worden andere talen en meertaligheid toegestaan.

## Land {#land}
AKN NC schrijft het gebruik voor van een 2-letter code van een land volgens ISO 3166-1 of van een code voor een subdivisie volgens ISO 3166-2. Voor STOP zijn de relevante codes:

|code|betekenis|
|--- |---      |
|nl  |Het land Nederland of het Koninkrijk de Nederlanden|
|aw  |Aruba|
|cw  |Curaçao|
|sx  |Sint Maarten|

## Documenttype {#doctype}
AKN NC kent een beperkte lijst met documenttypen. Voor STOP zijn relevant:

|code            |betekenis|
|---             |--- |
|act             |(Geconsolideerde) regeling|
|bill            |Besluit|
|doc             |Kennisgeving en Rectificatie|
|officialGazette |Officiële publicatie|

In STOP wordt act zowel gebruikt voor de geconsolideerde regeling die volgt uit de bekendgemaakte en in werking getreden besluiten, als voor documenten die de integrale regeling beschrijven in het besluitvormingsproces.

## Subtype {#subtype}
|code|betekenis|
|--- |---      |
|internationaal|Verdragen |
|koninkrijk    |Regelgeving voor het koninkrijk |
|land          |Regelgeving voor een land |
|provincie     |Regelgeving voor een provincie |
|gemeente      |Regelgeving voor een gemeente |
|waterschap    |Regelgeving voor een waterschap |
|zbo           |Regelgeving voor een zbo |
|pbo           |Regelgeving voor een pbo |
|gr            |Regelgeving voor een gemeenschappelijke regeling |
|caropl        |Regelgeving voor Bonaire, Sint Eustasius of Saba: de Caraïbische openbare lichamen |

## Blad {#blad}
|code  |betekenis|
|---  |---      |
|bgr  |Blad gemeenschappelijke regeling|
|gmb  |Gemeenteblad|
|prb  |Provinciaalblad|
|stb  |Staatsblad|
|stcrt|Staatscourant|
|trb  |Tractatenblad|
|wsb  |Waterschapsblad|

## Overheden {#overheid}
STOP maakt voor de overheden gebruik van de TOOI waardelijsten per bestuurslaag, zie [overzicht waardelijsten](imop_waardelijsten.md). Voor het aanduiden van een bevoegd gezag in een identificatie wordt de *code* uit de waardelijst gebruikt. 

Voorbeelden:

| Code     | uit waardelijst                                              | Voorbeeld in identificatie             |
| -------- | ------------------------------------------------------------ | -------------------------------------- |
| gm0638   | [https://identifier.overheid.nl/tooi/id/gemeente](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_gemeenten_compleet) | /akn/nl/bill/**gm0638**/2020/REG0001   |
| pv30     | [https://identifier.overheid.nl/tooi/id/provincie](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_provincies_compleet) | /akn/nl/doc/**pv30**/2020/kennisgeving |
| mnre1109 | [https://identifier.overheid.nl/tooi/id/ministerie](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_ministeries_compleet) | /akn/nl/act/**mnre1109**/2020/INST0021 |
| ws0663   | [https://identifier.overheid.nl/tooi/id/waterschap](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_waterschappen_compleet) | /akn/nl/act/**ws0663**/2020/REG0001    |





