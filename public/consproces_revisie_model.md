# STOP-modellering van een revisie

De [formele modellering van een revisie](EA_DBE8BF553C524b71BB84CFF11C7660CB.dita#Pkg) is vastgelegd in het STOP-informatiemodel. Deze pagina verwijst naar de elementen in de XML-implementatie van dat model.

## Tekstwijzigingen

Het element [tekst:Tekstrevisie](tekst_xsd_Element_tekst_Tekstrevisie.dita#Tekstrevisie) gebruikt hetzelfde mutatiemechanisme waarmee ook een STOP besluit wijzigingen in een regeling beschrijft: [tekst:RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie). Ook de revisie creëert dus net als een besluit door middel van een was- en een wordt-verwijzing een nieuwe versie (expressie) van het regeling-work. Het verschil is dat de door de revisie gecreëerde 'wordt' regeling-versie juridische gezien identiek moet zijn aan de 'was'-versie.

Een revisie mag immers alleen niet-juridische wijzigingen in de geconsolideerde regeling aanbrengen. Of een revisie juridische of niet-juridische wijzigingen bevat kan niet geautomatiseerd gecontroleerd worden. Het is dus aan het bevoegd gezag om het instrument revisie correct te gebruiken. Aangezien juridisch gezien alleen de bekend gemaakte besluiten 'geldig' zijn en de geconsolideerde regeling als 'service' informatie wordt beschouwd, kan altijd aan de hand van de besluiten gecontroleerd worden of de met een revisie aangebrachte wijzigingen juridisch correct zijn.

## Annotaties

Correcties, eventueel ten gevolge van de tekstwijzigingen, in de bij een regeling behorende informatie, zoals (versie)metadata of toelichtingsrelaties kunnen ook met een revisie worden aangeleverd. Annotaties worden niet door middel van een wijziging aangepast, maar worden altijd volledig opnieuw aangeleverd. Vandaar dat er geen speciale revisie-elementen bestaan voor annotaties. In de aanlevering worden de 'normale' modules gebruikt.

## Informatieobjecten

Ook voor een informatieobject kan een revisie aangeleverd worden. Omdat gewijzigde informatieobjecten altijd volledig worden aangeleverd, bestaat er ook voor de revisie van een informatieobject geen specifiek element.

## Consolidatie-informatie

Om een revisie correct te verwerken in de consolidatie, moet consolidatie informatie worden meegeleverd. In de consolidatie-informatie wordt aangegeven aan welke bestaande toestand de nieuwe regelingversie of de nieuwe informatieobject-versie gekoppeld moet worden.

Ook is het mogelijk om met een revisie uitsluitend consolidatie-informatie aan te leveren, als een regeling [materieel is uitgewerkt](data_xsd_Element_data_MaterieelUitgewerkt.dita#MaterieelUitgewerkt).



## Voorbeelden

* Nieuwe geconsolideerde regelingtekst om samenloop op te lossen: [Tekstrevisie](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/05%20Samenloop%20oplossen/01%20Verkeerde%20was/25-BG-Tekstrevisie-samenloop.xml) om [samenloop op te lossen](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/05%20Samenloop%20oplossen/01%20Verkeerde%20was).

* Annotaties los van een besluit aanleveren: [Annotaties achteraf](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/02%20Annotaties%20achteraf) 
* Zichtbaarheid beperken van een materieel uitgewerkte regeling: [Materieel uitgewerkt](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/07%20Beperking%20vindbaarheid)