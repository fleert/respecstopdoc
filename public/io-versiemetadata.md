# InformatieObjectVersieMetadata

De module [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) bevat de *expression*-metadata van een informatieobject.

## Overzicht onderdelen InformatieObjectVersieMetadata

| Element | Inhoud | Verplicht?   |
| ----------- | ----------- | -------------- |
| [`heeftBestanden`](data_xsd_Element_data_heeftBestanden.dita#heeftBestanden) | Container  | Nee. |
|[`heeftBestand`](data_xsd_Element_data_heeftBestand.dita#heeftBestand)|Kind van `heeftBestanden`. Container.| Ja, binnen ouder.|
|[`Bestand`](data_xsd_Element_data_Bestand.dita#Bestand)|Kind van `heeftBestand`. Container. |Ja, binnen ouder.|
| [`bestandsnaam`](data_xsd_Element_data_bestandsnaam.dita#bestandsnaam) | Kind van `Bestand`. Verwijst naar het bestand waarin de informatie van het informatieobject is geserialiseerd.  Bijvoorbeeld: `Centrumgebied.gml` | Ja, binnen ouder. |
| [`hash`](data_xsd_Element_data_hash.dita#hash)    | Kind van `Bestand`. Uniek SHA512-serienummer om de onveranderlijkheid van het bestand aan te tonen. | Ja, binnen ouder. |
| [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling)      | Alleen verplicht voor `TeConsolideren` IO's. De JOIN-ID van het regeling-*work* waarvoor het IO is opgesteld. Zie [de module `ExpressieIdentificatieRegeling`](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Nee. |
