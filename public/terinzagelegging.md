# Terinzagelegging

De [Algemene wet bestuursrecht](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.4&artikel=3:11&z=2021-04-01&g=2021-04-01) schrijft voor dat het ontwerp van een besluit in bepaalde gevallen ter inzage moeten worden gelegd. Dit kan bijvoorbeeld het geval zijn bij een (wijzigings)besluit over een verordening. Binnen een bepaalde periode, meestal zes weken, moeten belanghebbenden in staat worden gesteld kennis te nemen van het ontwerpbesluit. Deze termijn begint op de dag waarop het ontwerpbesluit ter inzage is gelegd. In het geval van de Omgevingswet zal het vrijwel altijd zijn dat een ieder het recht heeft inzage te krijgen in het ontwerpbesluit. 

Overheden kunnen hier op verschillende manier aan voldoen. Zij moeten in ieder geval faciliteren dat mensen het ontwerpbesluit kunnen inzien. Dit kunnen zij aanvullen met andere manieren om mensen te informeren over het besluit. Zo kunnen ze er ook voor kiezen om ook een (online) sessie te organiseren en hiervoor belanghebbenden voor uit te nodigen. In zo'n sessie kunnen experts het ontwerpbesluit toelichten en belanghebbenden vragen stellen.


**NB**: de publicatie van een ontwerpbesluit is niet automatisch het begin van de inzagetermijn.