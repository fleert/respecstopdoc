# Stap 1: Opstellen nieuwe regelingversie en GIOversie met OW data

Het opstellen van een nieuwe versie van een regeling vindt plaats in de plansoftware van het bevoegd gezag. Het kan hierbij gaan om:

* een eerste versie van een nog niet bestaande regeling, ook wel initiële regeling genoemd.
* een bestaande regeling die door het bevoegd gezag wordt gewijzigd. Het wijzigen van een bestaande regeling stelt eisen aan het beheer van RegelingVersies in de plansoftware. 

Een regelingversie gaat gepaard met een STOP-XML modules 'data:ExpressionIdentificatie', 'data:RegelingMetadata' en 'data:RegelingVersieMetadata'. Deze STOP-modules evenals eventuele gerelateerde GIOversie(s) en OW data hebben een gemeenschappelijk ['doel'](data_xsd_Element_data_doel.dita#doel); een Identificatie van de introductie van nieuwe of aangepaste regelgeving met één moment van inwerkingtreding, één moment waarop de regelgeving geldig wordt en eventueel één moment waarop de geldigheid van de regelgeving eindigt. Zie ook: [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md) (bij het Besluit). Het is van belang dat reeds in stap 1 de samenhang (tussen RegelingVersie, GIOversie en OW-data) geborgd is met een doel in de plansoftware. 

**STOP-XML Modules  nieuwe regelingversie**

* [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieRegeling.md) van de Regeling
* [data:RegelingMetadata](quickstart_1_RegelingMetadata.md)
* [data:RegelingVersieMetadata](quickstart_1_RegelingMetadata.md)
* [Regeling](quickstart_1_Regelingen.md) met hierbinnen de keuze uit vier tekstmodellen/STOP-XML modules

**STOP-XML Modules Nieuwe GIOVersie**

* [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieGIO.md) van een GIO
* [data:InformatieObjectMetadata](quickstart_1_InformatieObjectMetadata.md)
* [data:InformatieObjectVersieMetadata](quickstart_1_InformatieObjectMetadata.md)
* [geo:GeoInformatieObjectVaststelling](quickstart_1_GeoInformatieObjectVaststelling.md)