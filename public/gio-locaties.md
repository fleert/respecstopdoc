# Locatie(s) 
Een GIO bevat een of meerdere locaties. Elke locatie:
* heeft zijn eigen geometrische data. Deze data:
  * komen maar één keer voor in het GIO.
  * mogen in meerdere GIO's voorkomen.
  * heeft een `basisgeo:id` een unieke identificatie. Deze basisgeo:id blijft onveranderd als het gaat om dezelfde geometrische data in een andere GIO of in een volgende versie van dezelfde GIO.
* (optioneel) heeft een *naam*.
* (optioneel) heeft een *normwaarde* of (optioneel) een *GIO-deel-ID* (`groepID`)


## Basisgeometrie-GML met coördinaten  

De geometrische data van een locatie is versimpelde GML-data die conformeert aan de [basisgeometrie-standaard](https://docs.geostandaarden.nl/nen3610/basisgeometrie/) van Geonovum. Deze standaard is op zijn beurt een subset van de [GML-standaard](https://www.ogc.org/standards/gml). De basisgeometrie-GML bevat een van de volgende geometrische objecten:
* polygoon.
* multipolygoon.
* punt. 
* lijn. 
* puntengroep. 
* lijnengroep.

Volgens STOP-IMOP mogen deze geometrische objecten alleen uit rechte lijnsegmenten bestaan (dus geen cirkels of bogen). Dit is vastgelegd in de [geo schemadocumentatie](geo_xsd_Complex_Type_geo_LocatieType.dita#LocatieType_geometrie) van STOP. 

De GML-standaard bepaalt dat de geometrische objecten worden uitgedrukt in coördinaten. Een GIO kan daarom alleen geometrische data bevatten die zijn uitgedrukt in coördinaten. De toegestane ruimtelijke referentiesystemen hiervoor zijn:

- het [Rijksdriekhoekstelsel](https://nl.wikipedia.org/wiki/Rijksdriehoeksco%C3%B6rdinaten) of 
-  het [ETRS89-stelsel](https://en.wikipedia.org/wiki/European_Terrestrial_Reference_System_1989).


### Voorbeeld - XML-fragment van een locatie met coördinaten
```
      <geo:locaties xmlns:geo="https://standaarden.overheid.nl/stop/imop/geo/">
        <geo:Locatie>
          <geo:naam>Pyrodam centrum</geo:naam>
          <geo:geometrie>
            <basisgeo:Geometrie xmlns:basisgeo="http://www.geostandaarden.nl/basisgeometrie/1.0">
              <basisgeo:id>e80f9fdf-2df0-428f-8392-582990f55552</basisgeo:id>
              <basisgeo:geometrie>
                <gml:MultiSurface xmlns:gml="http://www.opengis.net/gml/3.2"
                                   srsName="urn:ogc:def:crs:EPSG::28992">
                  <gml:surfaceMember>
                    <gml:Polygon>
                      <gml:exterior>
                        <gml:LinearRing>
                          <gml:posList>230439.056 594375.806 230321.058 594344.121 230209.615 594308.066 230207.430 594266.548 230158.264 594266.548 230109.098 594254.530 230095.988 594294.955 230098.173 594379.083 229955.046 594345.214 229922.268 594468.675 229914.620 594524.396 229887.306 594653.319 229886.213 594682.819 229964.879 594795.354 230006.396 594831.409 230059.933 594836.872 230122.209 594800.817 230122.209 594800.817 230174.653 594803.002 230403.001 594827.039 230432.501 594618.357 230442.333 594562.636 230421.028 594551.983 230436.597 594503.364 230439.056 594375.806</gml:posList>
                        </gml:LinearRing>
                      </gml:exterior>
                    </gml:Polygon>
                  </gml:surfaceMember>
                </gml:MultiSurface>
              </basisgeo:geometrie>
            </basisgeo:Geometrie>
          </geo:geometrie>
        </geo:Locatie>
      </geo:locaties>
```



### Zie ook
[Voorbeelden van GIO's](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Coderingen/GIO/GIO-varianten) met verschillende typen geometrische data.


## Normwaarde(n) (optioneel)

Een **normwaarde** is van toepassing op een locatie en is optioneel. Een normwaarde is hierbij altijd gekoppeld aan de norm van de GIO. Er zijn vier manieren om normwaarden juridisch vast te leggen, zie de beschrijving daarvan in de sectie over [normen](gio-norm.md). Voor de regels voor normwaarden, zie [Uitgangspunten normen](gio-norm.md).

Er zijn twee soorten normwaarden:
1. Kwantitatieve normwaarden. 
2. Kwalitatieve normwaarden. 

### Kwalitatieve normwaarde
Een kwalitatieve normwaarde is een in tekst(string) uit te drukken waarde van een norm. Een kwalitatieve normwaarde met als inhoud een lege string ("") mag niet voorkomen.

### Kwantitatieve normwaarde 
Een kwantitatieve normwaarde is een in getallen (xs:double) uit te drukken waarde van een norm. Een kwantitatieve normwaarde kan 0 zijn, dus de waarde 0 hebben. Dat betekent niet dat de normwaarde onbepaald (null) is. Een onbepaalde waarde ondersteunt STOP niet.

## GIO-deel (optioneel)
Een locatie kan behoren tot een **GIO-deel** (genoemd in het groepenoverzicht van het GIO). De locatie heeft dan een *GIO-deel-ID* (`groepID`) om de locatie aan het GIO-deel te koppelen. Voor informatie over het hoe en waarom van GIO-delen, zie [uitgangspunten GIO-delen(-overzicht)](gio-delen.md).



