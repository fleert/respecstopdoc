# 1. Verwijs in de tekst naar het IO

In de tekst van een regeling wordt naar een informatieobject (IO) verwezen met een mens-leesbaar label.Ook kan de JOIN-ID van het IO in de tekst van de regeling worden opgenomen. Juridisch maakt het niet uit welke werkwijze wordt gekozen.

## Verwijzing naar informatief IO

Naar een informatief IO mag niet vanuit de besluit- of regelingtekst worden verwezen.

## Verwijzing naar Alleen bekend te maken IO
Voor `AlleenBekendteMaken` IO's geldt het volgende:
* De naam of titel van het IO (zoals een PDF-rapport) mag in de tekst van het *besluit* worden vermeld. 
* Deze naam of titel mag als ([`ExtRef`](tekst_xsd_Element_tekst_ExtRef.dita#ExtRef)) worden opgenomen in de tekst. Deze verwijzing heeft dan de JOIN-ID van de *expression* van het IO als doel, zodat er een hyperlink van kan worden gemaakt.
* Het is niet verplicht om de JOIN-ID in de tekst te zetten.

### Voorbeeld

Tekst van het besluit:

```xml
... gelezen het 
   <ExtRef 
     ref="/join/id/pubdata/mn002/2020/AkoestischOnderzoekN235Watergang_def/nld@2020-07-29"
     soort="JOIN">akoestisch onderzoek</ExtRef>; 
...
```
Dit kan er als volgt uitzien:


> Artikel 41. ... gelezen het [akoestisch onderzoek](/join/id/pubdata/mn002/2020/AkoestischOnderzoekN235Watergang_def/nld@2020-07-29)
> 

## Verwijzing naar te consolideren IO

### Verwijzing naar nieuwe IO(-versie)

* Naam/titel van het IO wordt opgenomen in de regelingtekst als [`IntIoRef`](tekst_xsd_Element_tekst_IntIoRef.dita#IntIoRef).
* Deze `IntIoRef` linkt naar het overzicht met informatieobjecten in de tekstbijlage. 



### Verwijzing naar bestaande IO bij andere regeling
Men kan ook verwijzen naar een IO dat voor een andere regeling is gemaakt. 

* Naam/titel van het IO wordt opgenomen in de regelingtekst als [`IntIoRef`](tekst_xsd_Element_tekst_IntIoRef.dita#IntIoRef).
* Deze `IntIoRef` linkt naar het overzicht met informatieobjecten in de tekstbijlage. 
* In dit overzicht staat een de naam/titel opnieuw vermeld, met daarbij de JOIN-ID van het IO. 
* Deze JOIN-ID is vormgegeven als link [`ExtIoRef`](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) naar het IO. Hier kan op twee manieren worden verwezen naar het IO. De keuze hangt af van het beoogde effect van de verwijzing. 

Twee manieren van verwijzing bij `ExtIoRef`:

1. **Dynamisch**: met de JOIN-ID van het *work* van een IO. Men verwijst naar de ‘actuele’ versie van het IO. Wordt deze andere IO gewijzigd, dan wijzigt juridisch het werkingsgebied van de regel waarin naar het IO wordt verwezen. De regel zelf hoeft dan niet meer te worden aangepast. 

2. **Statisch**: met de JOIN-ID van een specifieke *expression* (versie) van een IO. Men verwijst naar een specifieke versie op een bepaald moment in de tijd. Hierbij verandert het werkingsgebied van de regel waarin naar het IO wordt verwezen niet automatisch als er een nieuwe versie van het IO wordt bekendgemaakt.

### Verwijzing naar bestaande IO-versie bij dezelfde regeling
Dit gaat op dezelfde manier als [verwijzen naar een IO voor een andere regeling](#Verwijzing-naar-bestaande-IO-bij-andere-regeling).
## STOP-elementen voor tekstverwijzingen

- Het tekstlabel in de lopende tekst dat verwijst naar de JOIN-ID in de tekstbijlage wordt gecodeerd als een [`IntIoRef`](tekst_xsd_Element_tekst_IntIoRef.dita#IntIoRef). Deze interne verwijzing verwijst naar de [`wId`](begrippenlijst_wId.dita#wId) van de [`ExtIoRef`](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) van de JOIN-ID.
## Zie ook

* [Verwijzingen vanuit de tekst van regelingen naar specifieke onderdelen van een (G)IO](gio-tekstverwijzing.md), zoals [GIO-delen](geo_xsd_Element_geo_Groep.dita#Groep) of [normen](EA_1C752EC8163240cbA9644D1CD9266E58.dita#Pkg/A04E528498994d24B88888F7EE8331FE). 