# Soort tekstelement: Instrument

De 'Instrument'-tekstelementen identificeren de verschillende juridische instrumenten die STOP ondersteunt. 

## Instrument elementen

[BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact), [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek), [Kennisgeving](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving), [Mededeling](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling), [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie), [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact), [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek), [RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel), [RegelingVrijetekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst), [Tekstrevisie](tekst_xsd_Element_tekst_Tekstrevisie.dita#Tekstrevisie)