# Ondersteuning

## Helpdesk
KOOP beantwoordt vragen over STOP via 
[stopstandaard@koop.overheid.nl](https://localhost/mailto:stopstandaard@koop.overheid.nl).

## Bevindingen
Vermeende fouten in de technische hulpmiddelen of documentatie
en bevindingen over toepassing van de standaard waarvoor een
wijziging in de standaard nodig is, kunnen gemeld worden via de
[Gitlab issue tracker](https://gitlab.com/koop/STOP/standaard/issues).
Hier wordt ook de status van de bevindingen bijgehouden, en zijn
eerder gemelde bevindingen in te zien.

### Melden van een bevinding
* Ga naar de
[Gitlab issue tracker](https://gitlab.com/koop/STOP/standaard/issues)
* Zoek eerst of er al een bevinding is gerapporteerd over het hetzelfde onderwerp.
* Zo niet, maak een nieuwe bevinding aan. Daarvoor is alleen een gitlab account nodig.
* Kies voor het beschrijven van de bevinding een van de beschikbare templates.
![Nieuwe bevinding](img/bevinding_nieuw.png)

### In behandeling nemen van een bevinding
Zodra een bevinding wordt ontvangen, start een procedure om de bevinding te beoordelen.
Dit proces is te volgen via de blauwe labels die aan de bevinding worden toegevoegd,
en is te volgen via het bord [Acceptatie](https://gitlab.com/koop/STOP/standaard/-/boards/1353723):
* KOOP markeert de bevinding als *Te onderzoeken*
* KOOP kan contact opnemen als er vragen zijn over de inhoud van de bevinding.
* Als onderdeel van het onderzoek doet KOOP een voorstel voor het oppakken van de bevinding.
De gele labels (_schema_, _documentatie_, _voorbeelden_) geven aan welke onderdelen
van de standaard vooral geraakt worden. Tevens wordt via de groene labels (_correctie_, _uitbreiding_) 
aangegeven wat het effect van de voorgestelde oplossing op de standaard is.
* Na afronding van het onderzoek krijgt de bevinding het label _Te bespreken_.
Het bespreken gebeurt in een DSO afstemmingsoverleg. Daar wordt besloten of een bevinding
opgepakt gaat worden (_Geaccepteerd_) of niet (_Niet geaccepteerd_). In het laatste geval
zal de reden aangegeven worden bij de bevinding.

### Plannen van het oplossen van een bevinding
Als een bevinding _Geaccepteerd_ is wordt het verwerken van de oplossing in de standaard
gepland. Dat is te volgen op het bord [Planning](https://gitlab.com/koop/STOP/standaard/-/boards/1353727),
waarbij een _milestone_ aan de bevinding gekoppeld wordt:
* _Eerstvolgende versie_: het streven is om de oplossing van de bevinding in de 
eerstvolgende versie van de standaard mee te nemen.
* _Eerstvolgende of latere versie_: het streven is om de oplossing van de bevinding zo snel mogelijk 
in een oplevering van de standaard mee te nemen. Dat is bij voorkeur de eerstvolgende oplevering, 
maar het kan ook een van de volgende opleveringen zijn.
* _Toekomstige versie_: het is nog niet bekend in welke oplevering de oplossing van de bevinding meegenomenn wordt.

### Opgeloste bevindingen
Als een bevinding opgelost is, wordt de milestone gezet op de versie van de standaard 
waarvan de oplossing onderdeel uitmaakt. Via het [overzicht van milestones](https://gitlab.com/koop/STOP/standaard/-/milestones)
is per versie van de standaard na te gaan welke bevindingen zijn opgelost. De bevinding
krijgt als label _Gerealiseerd_ en wordt gesloten.

Als bij het oplossen van de bevinding blijkt dat de oplossing alsnog niet nodig of wenselijk is, 
krijgt de bevinding als label _Niet opgepakt_ en wordt gesloten. De reden voor het niet oplossen 
van de bevinding wordt bij de bevinding vermeld.