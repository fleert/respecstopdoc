# Nieuwe versie van een regeling of informatieobject

## Patroon
In het te publiceren juridisch document is aangegeven dat de regeling gewijzigd wordt en/of er wordt een nieuwe versie van een informatieobject in de te wijzigen regeling gebruikt. Dit patroon is ook van toepassing op een revisie.

## Uit te wisselen informatie
De nieuwe versie van de regeling of informatieobject moet uitgewisseld worden. De nieuwe versie bestaat uit:

* Voor een nieuwe versie van een [regeling](regeling.md): de complete tekst van een nieuwe, initiële regeling of een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) voor een wijziging van een bestaande versie. Afhankelijk van het type juridisch document is deze tekst onderdeel van de tekst van het document; dat is voor elk type juridisch document in STOP beschreven. Voor een revisie wordt de tekst als aparte module uitgewisseld.

* Voor een nieuwe versie van een [informatieobject](io-intro.md): de complete versie van het juridische deel van het informatieobject (bijvoorbeeld PDF-bestand of het GIO-GML-bestand). Als de soort informatieobject in STOP net als de tekst van de regeling gewijzigd kan worden, dan wordt voor een volgende versie de mutatie in plaats van de complete versie uitgewisseld.

* De modules die volgens het [overzicht](imop_modules.md) bij de regeling of het informatieobject horen. Het gaat om: 
    * Modules van het type _Instrumentversie-informatie_ moeten altijd meegeleverd worden.
    * Modules van het type _Annotatie_ moeten tenminste meegeleverd worden als ze veranderd zijn of het ontvangende systeem nog niet over de eerdere versie beschikt. Bij de implementatie van de uitwisseling kan ervoor gekozen worden om de annotaties altijd mee te leveren.
    
    De modules worden altijd als complete versie geleverd. Het is niet mogelijk alleen gewijzigde informatie aan te leveren. 

### ConsolidatieInformatie module

Een van de *instrumentversie-informatie* modules die altijd meegeleverd moeten worden is de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module. Deze module bevat: 

* Een [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) element als het om een versie van een regeling gaat.
* Een [BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject) element als het om een versie van een informatieobject gaat.

De inhoud van `BeoogdeRegeling`/`BeoogdInformatieobject` hangt af van de details van het patroon. In het algemeen:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat één doel, namelijk het doel waarvoor de versie gemaakt is.
* In [instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie) staat de expression-identificatie van de nieuwe versie.
* Voor het [eId](data_xsd_Element_data_eId.dita#eId) element geldt:
    * Voor een nieuwe versie die als revisie wordt aangeleverd wordt `eId` niet ingevuld.
    * Voor een nieuwe versie van een regeling verwijst het naar het `WijzigArtikel` in het juridisch document waarin staat dat de regeling gewijzigd wordt.
    * Voor een nieuwe versie van het informatieobject: zie [elders](io-opnemen-beoogdinformatieobject.md).

## Varianten
Er zijn vijf varianten van het patroon te onderscheiden voor het doorgeven van een nieuwe versie voor een doel (in de illustratie: doel A).

![Variaties](img/consolideren_patronen_1versie.png)

### Initiële versie
Als het gaat om de eerste versie van een nieuwe regeling of informatieobject, dan is de inhoud van `BeoogdeRegeling` / `BeoogdInformatieobject` zoals hierboven beschreven. Er hoeft geen rekening gehouden te worden met andere patronen.

### Nieuw doel
Als de publicatie een besluit is waarmee voor het eerst een versie voor een nieuw doel wordt beschreven, dan is in STOP [versiebeheer](versiebeheer_principes.md) sprake van een nieuwe branch. Van welke branch dit een aftakking is moet dan opgenomen worden in de ConsolidatieInformatie in het element [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan). Die branch (in de illustratie branch B) en het aftakpunt moet als [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) vermeld worden.

### Vervolgpublicatie of revisie

Deze variatie is van toepassing als een nieuwe versie wordt aangeleverd voor een bestaand doel dat geen historische toestand is. 

Als een nieuwe versie wordt uitgewisseld voor een doel waarvoor al eerder een publicatie is gedaan, dan is het een volgende versie op dezelfde branch. Als [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) moet de eerdere publicatie/revisie voor hetzelfde doel (A) vermeld worden.

### Historische toestand

Deze variatie is alleen van toepassing als een nieuwe versie nodig is voor een doel dat overeenkomt met een inwerkingtredingsdoel van een historische toestand die niet meer geldig is. Dit is bijvoorbeeld het geval als een beroepsorgaan een besluit gedeeltelijk vernietigt, waardoor een deel van de wijzigingen die voor het doel zijn aangebracht teruggedraaid moeten worden. 

Het bevoegd gezag heeft alleen een consolidatieplicht voor de geldende regelgeving en hoeft dus niet aan te geven hoe de geldende regeling (of informatieobject) voor de historische toestand na deze ingreep precies had moeten luiden. In dit geval stelt het bevoegd gezag dus alleen een nieuwe versie op voor de huidig geldende toestand (corresponderend met doel G in de illustratie). Voor het eigenlijke doel wordt wel aangegeven dat er een nieuwe versie is, maar er wordt inhoudelijk geen nieuwe versie uitgewisseld.  

De `ConsolidatieInformatie` moet nu twee keer een `BeoogdeRegeling`/`BeoogdInformatie` voor beide doelen:

1. Voor het eigenlijke doel (A in de illustratie): om aan te geven dat er een nieuwe versie is, maar dat deze versie inhoudelijk niet wordt samengesteld.

   **TODO**: hoe doe je dat? d.w.z. op een branch aanleveren dat er wel een versie zou moeten zijn, maar dat deze onbekend is. We hebben in de consolidatie informatie alleen 'instrumentVersie' om een specifieke versie aan te leveren. Als je een onbekende versie aan wil leveren kan dat nu (nog) niet. /TODO

2. Voor het huidige doel (G in de illustratie): om aan te geven dat er een nieuwe versie is ontstaan omdat een voorgaande versie gewijzigd is. Het element [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan) beschrijft dit door:

   - [`Basisversie`](data_xsd_Element_data_Basisversie.dita#Basisversie) te laten verwijzen naar de laatste publicatie/revisie voor de doel(en) van de huidige toestand (G in de illustratie);
   - [`VervlochtenVersie`](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) te laten verwijzen naar de onbekende nieuwe versie van het eigenlijke doel. Omdat de verwijzing bestaat uit een doel + datum/tijd, kan verwezen worden naar een niet bestaande versie. Merk op dat als datum/tijd in [gemaaktOp](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) de datum/tijd van de publicatie zelf gebruikt moet worden.

   Hiermee beschrijft de ConsolidatieInformatie dat de nieuwe versie gemaakt is door de wijzigingen uit de 'Vervlochtenversie' aan te brengen in de 'Basisversie'.

In beide elementen, voor zowel het eigenlijke doel als de doelen van de geldende toestand, wordt dezelfde `eId` ingevuld. De `eId` verwijst naar de tekst waarin staat dat het beroepsorgaan het besluit gedeeltelijk vernietigd heeft. Deze publicatie komt meermaals, voor alle doelen, voor in de [juridische verantwoording](juridische_verantwoording.md).

Het publiceren van de nieuwe versie kan effect hebben op toestanden tussen de historische toestand en de geldende toestand. Het bevoegd gezag hoeft daarvoor geen extra informatie te verstrekken (want daarvoor geldt geen consolidatieplicht); de geautomatiseerde consolidatie zal de inhoud van deze toestanden als onbekend markeren. Het kan ook effect hebben op in de toekomst geldende toestanden; daar moet het bevoegd gezag wel rekening mee houden en tijdig extra informatie voor verschaffen.
