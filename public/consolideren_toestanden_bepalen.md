# Samenstellen actuele toestanden

Het samenstellen van het overzicht van [actuele toestanden](consolideren_toestanden.md) wordt hier beschreven alsof er nog niets over toestanden bekend is. In de praktijk zal het overzicht bijgewerkt worden als een nieuwe publicatie (of beschikbaarstelling) gedaan wordt. Zonder kennis van het interne datamodel van software is het bijwerken niet te beschrijven. De maker van de STOP-gebruikende software moet deze beschrijving vertalen naar bijwerk-functionaliteit.

De bepaling wordt beschreven voor een regeling, maar werkt op dezelfde manier voor een te consolideren informatieobject. De legenda van de symbolen in de illustraties staat beschreven bij [de uitleg van consolidatiepatronen](consolideren_patronen.md).

## 1. Selectie van de doelen
Bij het bepalen van de toestanden wordt rekening gehouden met alle [branches/doelen](versiebeheer_principes.md) waarvoor een versie van de regeling (tekst of annotaties) wordt gepubliceerd of beschikbaar gesteld. Dit is elke branch/doel waarvoor in de verschillende [consolidatieinformatie-modules](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) voor de regeling is opgegeven:

* een of meer [`BeoogdeRegeling`](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling)-elementen zijn uitgewisseld die niet zijn gevolgd door een [`TerugtrekkingRegeling`](data_xsd_Element_data_TerugtrekkingRegeling.dita#TerugtrekkingRegeling), en/of:
* een [Intrekking](data_xsd_Element_data_Intrekking.dita#Intrekking) niet gevolgd door een [`TerugtrekkingIntrekking`](data_xsd_Element_data_TerugtrekkingIntrekking.dita#TerugtrekkingIntrekking);
* en: een of meer [Tijdstempel](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel) van de soort [`juridischWerkendVanaf`](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) die niet gevolgd is door een [terugtrekking](data_xsd_Element_data_TerugtrekkingTijdstempel.dita#TerugtrekkingTijdstempel) daarvan.

## 2. Maken van toestanden en datum juridisch uitgewerkt
Voor elk doel wordt gerekend met de laatst uitgewisselde waarde voor de tijdstempels. Voor het bepalen van de toestanden wordt alleen gekeken naar de doelen in de selectie met een `BeoogdeRegeling`.

Elke groep van doelen/branches die dezelfde datum voor _juridisch werkend vanaf_ heeft leidt tot een aparte toestand, met de doelen in de groep als inwerkingtredingsdoelen en de startdatum in _juridisch werkend op_ van de toestand gelijk aan _juridisch werkend vanaf_. De volgorde van doelen in de inwerkingtredingsdoelen is niet belangrijk.

De toestanden worden gerangschikt op volgorde van _juridisch werkend op_. De einddatum van _juridisch werkend op_ voor elke toestand wordt gezet op de begindatum van _juridisch werkend op_ van de volgende toestand.

Vanaf de tweede toestand worden de _basisversie-doelen_ gezet op de _basisversie-doelen_ plus _inwerkingtredingsdoelen_ van de voorgaande toestand. De volgorde van doelen in de basisversie-doelen is niet belangrijk.

De toestand krijgt een expression-identificatie volgens de [naamgevingsconventie](consolidatie_naamgeving.md). Daarbij wordt als _geldig vanaf_-datum de laatste/grootste datum gebruikt van de _Geldig vanaf_ voor de inwerkingtredingsdoelen. Bij het toekennen van het versienummer moet rekening gehouden worden met de inwerkingtredingsdoelen en basisversie-doelen. Als er in het verleden al eens een actuele toestand met dezelfde inwerkingtreding, geldig vanaf en doelen is geweest, dan moet het versienummer daarvan opnieuw gebruikt worden. Zo niet, dan moet een nieuw versienummer gekozen worden.

## 3. Bepalen van de inhoud van een toestand

### 3.1. Toekenning instrumentversie
Per toestand wordt vervolgens de inhoud bepaald. Dat is het eenvoudigste te beschrijven door aan te geven wat de inhoud in de _happy flow_ is, en daarna te beschrijven in welke gevallen géén sprake is van een happy flow en de inhoud alsnog niet bekend is.

Voor de happy flow geldt:
1. De _Instrumentversie_ van de toestand is in principe de versie die het laatst in aangeleverd voor het inwerkingtredingsdoel.
2. Als er meerdere inwerkingtredingsdoelen zijn, dan is dat de versie als bedoeld in (1) voor het eerste inwerkingtredingsdoel.
3. Als voor een van de inwerkingtredingsdoelen is aangegeven dat er wel een nieuwe versie is ontstaan maar die versie is niet aangeleverd, dan is de _Instrumentversie_ onbekend. Het is in dit geval niet nodig om na te gaan of de happy flow van toepassing is.

### 3.2 Controle meervoudige inwerkingtreding
Als er meerdere inwerkingtredingsdoelen zijn, dan moet de geselecteerde versie voor alle inwerkingtredingsdoelen als laatste versie zijn aangeleverd. Als dat niet het geval is, wordt voor elke laatst aangeleverde versie een van de doelen waarvoor de versie is aangeleverd in _Meerdere gelijktijdige versies_ opgenomen. In alle andere gevallen blijft _Meerdere gelijktijdige versies_ leeg.

| ![Gebruik van meerdere gelijktijdige versies](img/consolideren_meerderegelijktijdigeversies.png) |
| --- |
| Voorbeeld 3.2: controle op meerdere gelijktijdige versies |

### 3.3 Controle op voorgangers
Voor de verdere controle wordt gebruik gemaakt van de begrippen _voorganger_ en _bijgewerkt_:
* Doel 2 is een *voorganger* van doel 1 als de publicaties en revisies voor doel 2 volgens het versiebeheer bijgedragen hebben aan de versie voor doel 1.
* Doel 1 is *bijgewerkt* ten opzichte van een voorganger doel 2 als er geen publicaties/revisies voor doel 2 zijn die nog in de versie voor doel 1 verwerkt moeten worden.

Deze begippen kunnen meer formeel beschreven worden als:
* Doel 2 wordt _directe voorganger_ van doel 1 genoemd als:
    * doel 2 is genoemd in de [`Basisversie`](data_xsd_Element_data_Basisversie.dita#Basisversie) van een versie voor doel 1 (dus de branch/doel 1 is gemaakt als branch van branch/doel 2) en in een latere uitwisseling voor doel 1 is doel 2 niet genoemd in een [`OntvlochtenVersie`](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie), of:
    * doel 2 is genoemd in een [`VervlochtenVersie`](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) van een versie voor doel 1 (dus de branch/doel 1 is gemaakt als branch van branch/doel 2) en in een latere uitwisseling voor doel 1 is doel 2 niet genoemd in een [`OntvlochtenVersie`](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie), of:
    * er een versie is aangeleverd die voor zowel voor doel 1 als doel 2 van toepassing is en in een latere uitwisseling voor doel 1 is doel 2 niet genoemd in een [`OntvlochtenVersie`](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie).
* Als doel 2 een directe voorganger is van doel 1, dan is het _laatste bijwerkmoment_ de laatste [`gemaaktOp`](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) datum in een `Basisversie` / `VervlochtenVersie` / `OntvlochtenVersie` het _laatste bijwerkmoment_ en onderdeel van een uitwisseling voor doel 1, of van een versie voor zowel doel 1 als doel 2.
* Doel 2 is een _(indirecte) voorganger_ van doel 1 als:
    * doel 2 is een directe voorganger van doel 1, of:
    * doel 2 is een directe voorganger van een voorganger (zeg, doel 3) van doel 1 als alleen de uitwisseling voor doel 3 tot en met het _laatste bijwerkmoment_ wordt beschouwd.
* Van elke voorganger van doel 1 is bovendien aan te geven of doel 1 _bijgewerkt_ is, dus of alle publicaties en revisies voor de voorganger in de versie voor doel 1 verwerkt zijn. Dat is het geval als er geen uitwisseling voor de voorganger-branch meer heeft plaatsgevonden na het _laatste bijwerkmoment_ die gebruikt is bij de bepaling van die voorganger.

| ![Voorbeeld voorgangers](img/consolideren_voorganger.png) |
| --- |
| Voorbeeld 3.3: (Niet erg realistisch) voorbeeld van branches/doelen met onderlinge relaties |

In voorbeeld 3.3:
* A heeft geen voorgangers
* B heeft als voorgangers: A (bijgewerkt)
* C heeft als voorgangers: A (bijgewerkt)
* D heeft als voorgangers: E (bijgewerkt), A (bijgewerkt)
* E heeft als voorgangers: D (niet bijgewerkt), C (niet bijgewerkt), A (bijgewerkt)
* F heeft als voorgangers: C (niet bijgewerkt), B (niet bijgewerkt), A (bijgewerkt)

Nu wordt gevalideerd dat in de toegekende _instrumentversie_ alle publicaties en revisies verwerkt zijn:
1. Voor elk van de inwerkingtredingsdoelen wordt voor elke voorganger gecontroleerd of die voorkomt in de _basisversie-doelen_:
    a. Is dat niet het geval, dan wordt de voorganger toegevoegd aan _Nog te verwerken_ met indicatie _ontvlechten_. 
    b. Is dat wel het geval maar de voorganger is niet bijgewerkt, dan wordt de voorganger toegevoegd aan _Nog te verwerken_ met indicatie _verwerken_.
2. Elk doel in de _basisversie-doelen_ dat geen voorganger is van de inwerkingtredingsdoelen wordt toegevoegd aan _Nog te verwerken_ met indicatie _verwerken_.
3. Daarna wordt _Nog te verwerken_ opgeschoond: als zowel doel 1 als doel 2 erin voorkomen wordt doel 2 verwijderd als:
    a. doel 1 en 2 zijn opgenomen als _verwerken_ en doel 2 treedt eerder dan of tegelijk met doel 1 in werking.
    b. doel 1 en 2 zijn opgenimen als _ontvlechten_ en doel 2 is een voorganger van doel 1.

Als in voorbeeld 3.3 de doelen A, B, D, C, F en E in werking treden (in die volgorde) en besluit B is vernietigd, dan zijn de toestanden:

* Toestand met inwerkingtredingsdoel A heeft de versie van A als _instrumentversie_

* Toestand met inwerkingtredingsdoel D heeft als basisversie-doelen alleen A:
    * Doel E staat als _ontvlechten_ in _Nog te verwerken_ vanwege validatie 1a.

* Toestand met inwerkingtredingsdoel C heeft als basisversie-doelen A en D:
    * Doel D staat als _verwerken_ in _Nog te verwerken_ vanwege validatie 2.

* Toestand met inwerkingtredingsdoel F heeft als basisversie-doelen A, C en D:
    * Doel C staat als _verwerken_ in _Nog te verwerken_ vanwege validatie 1b.
    * Doel D zou als _verwerken_ in _Nog te verwerken_ staan vanwege validatie 2, maar wordt weggelaten vanwege validatie 3a.
    * Doel B staat als _ontvlechten_ in _Nog te verwerken_ vanwege validatie 1a.

* Toestand met inwerkingtredingsdoel E heeft als basisversie-doelen A, C, D en F:
    * Doel F staat als _verwerken_ in _Nog te verwerken_ vanwege validatie 2
    * Doelen C en D zouden als _verwerken_ in _Nog te verwerken_ staan vanwege validatie 1b, maar worden weggelaten vanwege validatie 3a.

### 3.3 Toepassen intrekking

Het is ongebruikelijk dat een regeling wordt ingetrokken terwijl er besluiten zijn die de regeling wijzigen en na de intrekking in werking treden. Bij informatieobjecten kan dat wel voorkomen: als een informatieobject als een GIO wordt ingetrokken omdat de inhoud ervan in een ander of nieuw informatieobject is ondergebracht, kunnen er andere besluiten later in werking treden die nog van het ingetrokken informatieobject uitgaan. Hoewel juridisch een wijziging na een intrekking mogelijk irrelevant is, wordt dat bij de bepaling van de toestanden niet zo gemodelleerd. Bevoegd gezag moet expliciet aangeven wat er moet gebeuren met de wijziging. Een wijziging na een intrekking wordt daarom ook als samenloop opgevat:

De volgende stap heeft betrekking op de intrekkingsdoelen, dus de doelen uit de selectie met een `Intrekking`.

* Voor elk intrekkingsdoel:
    * Als er toestanden zijn met _juridisch werkend op_ van toestand ≥ _juridisch werkend vanaf_ van de intrekking, dan wordt voor elk van die toestanden het doel van de intrekking toegevoegd aan _Nog te verwerken_.
* Als voor geen enkel intrekkingsdoel en geen enkele toestand het doel aan _Nog te verwerken_ is toegevoegd:
    * De datum intrekking is de _juridisch werkend vanaf_ van het enige intrekkingsdoel, of het minimum van de _juridisch werkend vanaf_ als er meerdere intrekkingsdoelen zijn.
    * De toestand met de grootste _juridisch werkend op_ krijgt in _juridisch werkend op_ een einddatum gelijk aan de intrekkingsdatum.
    * De datum _juridisch uitgewerkt_ van de module wordt op de intrekkingsdatum gezet.

### 3.4 Weghalen incomplete instrumentversie

Als _Nog te verwerken_ elementen bevat, dan is de toegekende _instrumentversie_ niet bruikbaar en moet die als onbekend gemarkeerd worden.

## 4. Bepalen materieel uitgewerkt
De datum voor _materieel uitgewerkt_ wordt uitgewisseld als [`MaterieelUitgewerkt`](data_xsd_Element_data_MaterieelUitgewerkt.dita#MaterieelUitgewerkt) zonder koppeling aan een doel. Als er niet op een later tijdstip een [`TerugtrekkingMaterieelUitgewerkt`](data_xsd_Element_data_TerugtrekkingMaterieelUitgewerkt.dita#TerugtrekkingMaterieelUitgewerkt) is uitgewisseld, wordt de doorgegeven datum als _materieelUitgewerkt_ opgenomen in de module met actuele toestanden.