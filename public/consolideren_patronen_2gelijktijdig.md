# Besluiten treden tegelijk in werking

## Patroon
Een publicatie vermeldt een datum inwerkingtreding voor (een deel van) een besluit, terwijl er al een of meerdere besluiten zijn die ook op die datum in werking treden (voor dezelfde regeling).

Bij dit patroon wordt ervan uitgegaan dat de besluiten juridisch verenigbaar zijn. Dat wil zeggen dat de besluiten elkaar juridisch gezien niet tegenspreken.

## Uit te wisselen informatie
Besluiten die tegelijk in werking treden moeten rekening met elkaar houden: de versie die inwerking treedt moet de wijzigingen uit alle besluiten bevatten. Omgekeerd moet elk doel/branche ook de wijzigingen uit de andere doelen/branches bevatten zodat elk doel/branch de versie bevat die in werking treedt.

De essentie van de aanpak van dit patroon is dus om bij elk volgend besluit de versie aan te leveren die voor alle doelen de goede versie is. De doelen treden immers gelijktijdig in werking. Gebeurt dit niet, dan moet achteraf alsnog de gecombineerde versie uitgewisseld worden.

## Varianten

![Gelijktijdige inwerkingtreding](img/consolideren_patronen_2gelijktijdig.png)

In de illustratie is het eerste besluit opgesteld voor doel A, opvolgende besluiten die gelijk inwerkingtreden hebben doel B en C.

## Zonder samenloop: een 'treintje'

In deze variant is er geen samenloop in de wijzigingen. De wijzigingen voor doel/branch B worden gebaseerd op de versie uit doel/branch A. De resulterende regelingversie wordt toegekend aan beide doelen/branches. 

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het tweede besluit staat de nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:

* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat doel A en B.

Vervolgens identiek voor de wijzigingen voor doel/branch C: deze worden gebaseerd op de laatste versie voor doel A èn B. Het resultaat is een regelingversie voor doel A, B en C.

**Zie ook**

* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/06%20Samenloop%20voorkomen/01%20Gelijktijdige%20inwerkingtreding) van twee besluiten die zonder samenloop gelijktijdig inwerking treden.

## Met samenloop: een 'waaier'

In deze variant is er wel samenloop in de wijzigingen. De wijzigingen voor de verschillende doelen/branches worden gebaseerd op dezelfde basisversie en houden geen rekening met elkaar. In dit geval wordt de publicatie uitgewisseld als ware er geen sprake van gelijktijdige inwerkingtreding. 

De geautomatiseerde consolidatie kan nu niet de regelingversie bepalen die in werking treedt: er zijn alleen regelingversies van de individuele branches, maar er is geen regelingversie met de gecombineerde wijzigingen. 

Om de samenloop op te lossen wordt een [revisie](consproces_revisie.md) uitgewisseld met de samengevoegde versie. In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van die revisie staat deze nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:

* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staan de doelen A, B en C.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch C.

**Zie ook**

* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/05%20Samenloop%20oplossen/02%20Gelijktijdige%20inwerkingtreding) van het oplossen van samenloop van twee besluiten die gelijktijdig in werking treden.

## Implementatiekeuzes
Als er sprake is van samenloop, dan is dat bekend bij de uitwisseling van de publicatie. STOP-gebruikende systemen kunnen ervoor kiezen om het mogelijk te maken de revisie voor het oplossen van samenloop tegelijk met de publicatie aan te leveren, of juist na elkaar te laten plaatsvinden.
