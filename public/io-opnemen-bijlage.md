# 2. Koppel in de bijlage label aan JOIN-ID

**N.B**.: Deze stap geldt alleen voor te consolideren IO's.

Om te weten wat er met het IO-label uit stap 1 wordt bedoeld, wordt in de regeling een bijlage opgenomen met een overzicht van de gebruikte labels en de JOIN-ID van de bijbehorende IO's. Dit zorgt ervoor dat de lopende tekst goed leesbaar blijft en de JOIN-ID (waarmee de inhoud van het IO altijd is te vinden) zichtbaar is.

## Voorbeeld-bijlage
Voor een praktijkvoorbeeld van een bijlage met JOIN-ID’s, zie [Regeling van de Minister voor Milieu en Wonen, de Staatssecretaris van Defensie, de Minister van Economische Zaken en Klimaat, de Minister van Infrastructuur en Waterstaat, de Minister van Landbouw, Natuur en Voedselkwaliteit en de Minister van Onderwijs, Cultuur en Wetenschap van 21 november 2019, houdende regels over het beschermen en benutten van de fysieke leefomgeving (Omgevingsregeling)](https://zoek.officielebekendmakingen.nl/stcrt-2019-56288.html#d17e23557). 
 
De bijlage waar de verwijzing naar de JOIN-ID staat kan er als volgt uitzien:

------

> **BIJLAGE A OVERZICHT INFORMATIEOBJECTEN** 
> In deze regeling worden de volgende informatieobjecten gebruikt.
>
> | Aanduiding               | Join                                                         |
> | ------------------------ | ------------------------------------------------------------ |
> | Militaire oefenterreinen | /join/id/regdata/mnre1034/2019/militaire_oefenterreinen/@2019-10-04 |

------

## JOIN-ID is ExtIORef
De JOIN-ID van de informatieobjecten-bijlage wordt vormgegeven als `ExtIORef`. De `ExtIORef` wordt daarbij gebruikt om vanuit de regeling te verwijzen naar het hele IO, een object dat extern is ten opzichte van de regeling. Hierbij staat de JOIN-ID zowel in de tekst als in het doel van de verwijzing.

## Verwijzing naar nieuwe IO(-versie)

* Een link met JOIN-ID van het IO wordt eventueel ook opgenomen in de renvooitekst van een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie).

## Gewijzigde IO: wijzig JOIN-ID

Bij een nieuwe IO-versie wijzig je in deze stap het JOIN-ID van het IO in de tekstbijlage.