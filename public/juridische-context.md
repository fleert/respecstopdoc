# Juridische context STOP

Deze sectie belicht de juridische context van STOP.

Tot de doelgroep van deze leeswijzer behoren:

 * Eindgebruikers van software die informatie volgens STOP uitwisselt, zoals beleidsmedewerkers die plannen opstellen en juristen.
 * Ontwikkelaars van software om plannen, besluiten en regelingen mee op te stellen.


Deze sectie is opgebouwd uit de volgende delen:

* [Juridische uitgangspunten](juridische-uitgangspunten.md)
   * [Algemene wet bestuursrecht (Awb)](awb.md)
   * [Omgevingswet](ow.md)
   * [Besluiten volgens de Awb](awb-besluit.md)
   * [Procedure voorafgaand aan een besluit](voorbereiding-besluit.md)
   * [Uniforme openbare voorbereidingsprocedure  ](uniforme-voorbereiding-besluit.md)
   * [Terinzagelegging](terinzagelegging.md)
   * [Bekendmaking besluit](bekendmaking-besluit.md)
   * [Elektronische bekendmaking besluit](elektronische-bekendmaking-besluit.md)
   * [Mededelen van een besluit](mededeling-besluit.md)
   * [Consolidatie](consolidatie-juridisch.md)
   * [Bezwaar](bezwaar.md)
   * [Uniforme openbare voorbereidingsprocedure in STOP](uniforme-voorbereiding-besluit-stop.md)
   * [Beroep](beroep.md)
*  [Opname juridische uitgangspunten in STOP](verwerking-juridische-uitgangspunten-STOP.md)  
   * [Belangrijkste wijzigingen van de Omgevingswet ](wijzigingen-ow.md)
   * [Besluit volgens STOP](stop-besluit.md)
   * [Regeling volgens STOP](stop-regeling.md)
   * [Bijlage volgens STOP](stop-bijlage.md)
   * [Werkingsgebieden van regels in  STOP](werkingsgebieden-stop.md)
   * [Vaststellen en wijzigen van GIO's](vaststellen-wijzigen-gio.md)
   * [Juridische verankering van  STOP](juridische-verankering-stop.md)
   * [Het maken van een wijzigingsbesluit in renvooiweergave](wijzigingsbesluit-renvooiweergave.md)
   * [Directe mutatie](directe-mutatie.md)
   * [Rectificatie](rectificatie.md)
   * [Effectgebieden](effectgebieden.md)
   * [Tijdelijk regelingdeel](tijdelijk-regelingdeel.md)
   * [Pons](pons.md)
   * [Verwerken van bezwaar tegen een STOP-besluit](verwerking-bezwaar.md)
   * [Verwerken van beroep tegen een STOP-besluit](verwerking-beroep.md)
   * [Authentieke bron](authentieke-bron.md)