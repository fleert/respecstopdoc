# Bedrijfsregels

De bedrijfsregels van STOP zijn machine-leesbaar als serviceproduct in [XML](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aservice/bedrijfsregels/bedrijfsregels.xml) beschikbaar. Als onderdeel van de standaard wordt ook het [schema](br_xsd_Main_schema_br_xsd.dita#br.xsd) uitgegeven, zodat anderen ook bedrijfsregels op deze manier kunnen documenteren. Het [bronhouderkoppelvlak](@@@BHKV_URL@@@) maakt daar gebruik van.

De bedrijfsregels worden op deze manier beschikbaar gesteld:

- Om te gebruiken in gecombineerde overzichten van validaties. DSO publiceert bijvoorbeeld een compleet overzicht van alle bedrijfsregels voor aanlevering aan het bronhouderkoppelvlak, waarbij naast de STOP-bedrijfsregels ook de LVBB en DSO-LV validaties worden meegenomen.

- Voor (leveranciers van) software die STOP-modules ontvangt om te weten welke validaties onderdeel van de software moeten zijn. Alle bedrijfsregels waarvoor [implementatieDoorEigenaar](br_xsd_Element_br_implementatieDoorEigenaar.dita#implementatieDoorEigenaar) *waar* is worden geheel geïmplementeerd door de STOP-schematrons. Als de software de STOP-schematrons gebruikt voor ingangscontrole zal informatie die zondigt tegen deze bedrijfsregels al gesignaleerd worden. De bedrijfsregels zonder _implementatieDoorEigenaar_ (of waar dat element *onwaar* is) moeten door de software zelf geïmplementeerd worden.

- Voor (leveranciers van) software die STOP-modules uitlevert als hulpmiddel bij het bepalen van de compleetheid van validaties en bij traceerbaarheid. Bedrijfsregels in STOP worden geïdentificeerd met een [code](br_xsd_Element_br_code.dita#code) en veranderen nooit. Als de bedrijfsregel zou wijzigingen, dan vervalt de bestaande code en komt er een nieuwe voor in de plaats. Als een bedrijfsregel in de STOP-schematrons is geïmplementeerd kan de implementatie wel wijzigen voor een bedrijfsregel, bijvoorbeeld om een bug te herstellen.

Zie [hier](businessrules.md) voor het overzicht van de STOP-bedrijfsregels.