# Er is nog een toestand na een intrekking

## Patroon
Er ontstaat samenloop doordat bij de publicatie van een intrekking blijkt dat er na de datum van intrekking nog een besluit in werking treedt die de inmiddels ingetrokken regeling/informatieobject wijzigt.

## Varianten

Er zijn drie situaties:

1. De intrekking is zo bedoeld en de wijziging kan genegeerd worden.
2. De intrekking was onbedoeld en moet ongedaan gemaakt worden. 
3. De intrekking is zo bedoeld en de wijziging moet plaatsvinden op de opvolger van de regeling/informatieobject.

![](img/consolideren_patronen_2intrekking.png)


### 1. Bedoelde intrekking, negeer de wijziging 

Deze variant is alleen van toepassing op de intrekking van een regeling of een intrekking van een informatieobject als die samenvalt met de intrekking van de geboorteregeling. Juridisch heeft de wijziging geen betekenis meer nadat de regeling is ingetrokken.

Er wordt een [revisie](consproces_revisie.md) uitgewisseld, want het is geen juridische wijziging. De revisie bevat geen nieuwe versie van de regeling/informatieobject. In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module staat:
* Een [TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) of [TerugtrekkingRegeling](data_xsd_Element_data_TerugtrekkingRegeling.dita#TerugtrekkingRegeling) met:
    * In [doelen](data_xsd_Element_data_doelen.dita#doelen) staan de inwerkingtredingsdoelen van de toestand corresponderend met de wijziging. (B in de illustratie)
    * In [instrument](data_xsd_Element_data_instrument.dita#instrument) staat het work-id van de regeling/informatieobject.
    * Er wordt geen [eId](data_xsd_Element_data_eId.dita#eId) opgenomen, want er is geen juridische tekst om naar te verwijzen.

### 2. Onbedoelde intrekking

Om samenloop in deze situatie op te lossen moet een nieuw besluit genomen worden om de intrekking ongedaan te maken. In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van dat besluit staat:
* Een [TerugtrekkingIntrekking](data_xsd_Element_data_TerugtrekkingIntrekking.dita#TerugtrekkingIntrekking) met:
    * In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat het doel van de intrekking.
    * In [instrument](data_xsd_Element_data_instrument.dita#instrument) staat het work-id van de regeling/informatieobject.
    * De [eId](data_xsd_Element_data_eId.dita#eId) verwijst naar het artikel in het besluit dat ziet op het ongedaan maken van de intrekking.

### 3. Bedoelde intrekking, wijzig de opvolger

Om samenloop in deze situatie op te lossen moet een nieuw besluit genomen worden om de wijziging op de opvolger toe te passen. Het voorgaande besluit had juridisch immers geen betekenis.

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het nieuwe besluit staat nu ook de [TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) of [TerugtrekkingRegeling](data_xsd_Element_data_TerugtrekkingRegeling.dita#TerugtrekkingRegeling) zoals beschreven in de variant 1 om de effecten van het juridisch betekenisloze besluit te verwijderen.
