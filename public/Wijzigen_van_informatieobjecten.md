# Wijzigen van informatieobjecten

Na de initiële totstandkoming, kan een (consolideerbaar) informatieobject veranderen door besluiten van het bevoegd gezag. Het bevoegd gezag moet daarvoor twee dingen doen:

- In een regeling naar de nieuwe versie van het informatieobject verwijzen;
- De nieuwe versie ([expressie](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg)) van het informatieobject met het besluit meeleveren.

De regeling die als eerste naar een versie van het informatieobject verwijst heet de [geboorteregeling](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling). De versie van het informatieobject krijgt juridische werking (treedt in werking) tegelijk met de geboorteregeling, en als de geboorteregeling wordt ingetrokken dan eindigt ook de juridische werking van de versie van informatieobject. In het besluit hoeft het bevoegd gezag alleen de datum van de inwerkingtreding (of intrekking) van de geboorteregeling op te nemen. Deze datum kan in het besluit zelf staan of in een ander besluit, zoals het inwerkingtredingsbesluit. 

## Twee manieren van muteren

Er zijn twee manieren waarop een besluit, een informatieobject kan wijzigen:

1. Gewijzigd vaststellen;
2. Opnieuw vaststellen.

Er bestaan twee manieren van wijzigen omdat het bij sommige typen besluiten mogelijk is om in beroep te gaan (deze besluiten noemt men appellabel). Welk deel van het informatieobject appellabel is, hangt af van de wijze van muteren.  

### Gewijzigd vaststellen

Een besluit stelt een informatieobject gewijzigd vast door aan te geven wat de vorige versie van dit informatieobject was. Hiermee beperkt het besluit zich uitsluitend tot de verschillen tussen deze en de aangeduide vorige versie. 

Wanneer een informatieobject gewijzigd vastgesteld wordt, impliceert dit dat het bevoegd gezag alleen dat gedeelte van het informatieobject dat daadwerkelijk gewijzigd is bekeken heeft. En dus is er geen besluit genomen over de niet-gewijzigde delen van het informatieobject. Hierdoor is slechts het gewijzigde gedeelte van het informatieobject appellabel. 

Om een informatieobject gewijzigd vast te stellen moet worden aangegeven wat de vorige versie van het informatieobject is. Daarmee wordt alleen besloten over de veranderingen in het nieuwe informatieobject ten opzichte van de aangeduide vorige versie.

### Opnieuw vaststellen

Een besluit bepaalt de nieuwe expressie van het informatieobject. Hetzelfde informatieobject wordt daarmee volledig nieuw vastgesteld. Er is geen verwijzing naar een vorige versie. De ongewijzigde [work](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg) aanduiding laat zien dat het om hetzelfde informatieobject gaat. 

Wanneer een informatieobject opnieuw vastgesteld wordt, geldt dat het bevoegd gezag het hele informatieobject heeft bekeken en over ieder onderdeel daarvan besloten heeft. Daardoor is het volledige informatieobject appellabel.

### Concrete uitwerking van mutaties

De paragraaf [Een GIO wijzigen](gio-wijzigen.md) geeft een concrete uitwerking van deze mutaties.