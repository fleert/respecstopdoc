# Principes van STOP versiebeheer

Voor het beheer van versies van een regeling of informatieobject en de samenhang tussen versies van verschillende regelingen en informatieobjecten maakt STOP gebruik van een model dat sterke overeenkomsten vertoont met versiebeheer volgens `git` zoals dat bij software-ontwikkeling veel wordt toegepast. STOP en `git` kennen diverse overeenkomsten:
* Het ontwerp van beide gaat uit van een creatieproces waarin door verschillende partijen onafhankelijk van elkaar gewerkt wordt zonder een centrale versiebeheervoorziening. 
* Tijdens het creatieproces is ook nog niet duidelijk hoe en in welke volgorde de verschillende versies samengevoegd gaan worden. 
* Specifieke versies zijn achteraf niet meer wijzigbaar; wijzigen worden gedaan door een nieuwe versie te maken. 

## Wat is een 'branch'?

Voordat we ingaan op de versiebeheer-principes, lichten we eerst het begrip *branch* (tak) toe. Branch is een van de kernbegrippen in `git`. Een branch kan alsvolgt gedefinieerd worden:

> A *branch* represents an independent line of software development *(Een branch representeert een onafhankelijke lijn van software-ontwikkeling)*

Voor het beschrijven van versiebeheer in STOP gebruiken we ook de term *branch*, in STOP is de definitie van een branch specifiek voor regelgeving:

> Een *branch* is een complete versie van een regeling (of regelingen), inclusief bijbehorende informatieobjecten en annotaties, met als doel om als één geheel op één moment in de tijd in werking te treden.

## Wat is een 'doel'?

In STOP wordt een branch geidentificeerd door een doel. In de STOP documentatie wordt vaak de term doel gebruikt in plaats van branch. 

## Werkwijze
Ter illustratie de typische werkwijze (van onder naar boven) bij `git` voor het aanpassen van software (links) en in STOP voor het wijzigen van regelgeving (rechts):

![](img/versiebeheer-git-stop.png)

1. In `git` wordt eerst de aan te passen versie opgezocht, meestal op de 'master' branch waar de laatste versie van software te vinden is. In STOP begint de aanpassing door selectie van de versie van een regeling die - voor zover op dat moment bekend - geldig zal zijn op het moment dat de aangepaste versie in werking treedt. Vaak zal dat de laatst bekendgemaakte versie zijn.  

1. In `git` wordt die versie in een nieuwe branch geplaatst, in STOP ook. STOP heeft geen uitwisselingsmodel voor een branch, maar benoemt wel de naam van de branch: [doel](begrippenlijst_doel.dita#doel). In `git` bevat de branch een complete versie van de software, in STOP de complete versie van de regeling, bijbehorende informatieobjecten en annotaties. Als er wijzigingen nodig zijn in andere regelingen die tegelijk in werking moeten treden, dan ook complete versie van die regelingen. In `git` bevat een branch op elk moment maar één versie van de software, een branch in STOP van elke regeling en elk informatieobject één versie.

1. In `git` wordt de software in de branch geïsoleerd aangepast, onafhankelijk van andere aanpassingstrajecten die er eventueel nog lopen. Dat is ook in STOP het geval. In `git` kan een aangepaste versie van de software onder versiebeheer bewaard worden; die versie (*commit*) blijft daarna onveranderlijk. In STOP kan een versie van een regeling (met alles wat erbij hoort) uitgewisseld worden; die versie (*momentopname*) wordt ook als onveranderlijk gezien.

1. Pas als het werk voor de software-aanpassing gereed is, worden in `git` eventuele wijzigingen uit andere aanpassingstrajecten overgenomen (via de operatie *merge*); `git` legt vast welke branches bij de *merge* betrokken zijn zodat de volledige wijzigingsgeschiedenis beschikbaar blijft.
   Als de aanpassing van regelgeving gereed is, wordt in STOP een besluit opgesteld waarin de nieuwe versie is opgenomen. Voorafgaand daaraan worden eventuele wijzigingen die sinds het aanmaken van de branch in de regeling zijn doorgevoerd, overgenomen in de regeling in de branch. Deze operatie heet in STOP *vervlechting* en moet ook vastgelegd worden.

1. Uiteindelijk wordt in `git` de eindversie in de branch overgenomen in de 'master', als volgende releasebare versie van de software. In STOP wordt de bekendgemaakte versie ingevoegd in de geldende regelgeving per datum van de inwerkingtreding. Waar `git` spreekt van de 'master'-branch voor een softwareproduct, kent STOP een equivalent voor een individuele regeling of informatieobject: dit heet de *consolidatie* van de regeling of informatieobject, ofwel de *geconsolideerde regeling* of het *geconsolideerd informatieobject*.

## Branches verdwijnen niet
Een branch in `git` is na de merge met de master nog wel beschikbaar, maar wordt niet meer gebruikt voor aanvullende wijzigingen. Mocht er op een later moment nog een wijziging nodig zijn op de inhoud die voor de branch is geïntroduceerd, dan kiest men er meestal dan voor om de wijziging in een nieuwe branch te doen (stap 3 in onderstaande illustratie) en deze dan weer mergen naar de master. Bij STOP is dat anders: de wijziging moet op de oorspronkelijke branch (B in de illustratie) worden gedaan. 

![](img/versiebeheer-doel-blijft-bestaan.png)

## Eén versie voor meerdere branches

Net als in `git` kan een nieuwe versie in STOP voor meer dan één doel van toepassing zijn. Dezelfde versie wordt dan aan meerdere branches gekoppeld in STOP. Maar omdat in STOP branches niet verdwijnen, moet bij een opvolgende versie dan ook verwezen worden naar *alle* branches:

![](img/versiebeheer-1-versie-meer-branches.png)


## Alleen in STOP: ontvlechting

STOP wijkt af van de gangbare `git`-praktijk waar het gaat om ongedaan maken van wijzigingen. Dit komt bijvoorbeeld voor als een rechter een besluit vernietigt en er voor dezelfde regeling inmiddels aanvullende besluiten bekendgemaakt zijn.

![](img/versiebeheer-ontvlechting.png)

In zowel STOP als `git` is het werk dat gedaan moet worden hetzelfde: de wijzigingen uit het vernietigde besluit moeten uit de huidige versie van de regelgeving teniet gedaan worden. In `git` wordt daarvoor in het algemeen hetzelfde mechanisme gebruikt als bij rectificatie: maak een nieuwe branch en verwijder de wijzigingen. Dit is vaak eenvoudiger dan de latere wijzigingen proberen over te zetten op de versie van voor het vernietigde besluit. Het is in de `git`-wijzigingshistorie niet te zien of de derde branch bedoeld was om een eerdere wijziging ongedaan te maken, of om een nieuwe wijziging aan te brengen. In STOP is dat onderscheid wel van belang: het stelt software in staat de verifiëren dat de wijzigingen van een vernietigd besluit geen onderdeel meer uitmaken van de geldende regelgeving. STOP kent daarom de operatie *ontvlechting* die het omgekeerde is van een *vervlechting*.

## Versiebeheer per regeling/informatieobject

Net als in `git` kan op een branch in STOP aan meer dan één regeling of informatieobject gewerkt worden. In `git` zijn de meeste operaties zoals `commit` of `merge` gericht op de collectie van alle bestanden in de branch. De administratie van het versiebeheer wordt in STOP per regeling of per informatieobject uitgevoerd. De branch (het doel) wordt gebruikt om aan te geven welke wijzigingen bij elkaar horen.

![](img/versiebeheer-per-instrument.png)

In het voorbeeld wordt een regeling in drie besluiten gewijzigd, en in twee ervan wordt ook een informatieobject gewijzigd. Het versiebeheer en het bepalen van de geldende versie wordt voor de regeling en voor het informatieobject apart gedaan. Zo kent het versiebeheer voor het informatieobject alleen de branches A en C omdat het voor branch B niet gewijzigd wordt. Waar zowel de regeling als het informatieobject gewijzigd worden maken beide gebruik van dezelfde aanduiding voor een positie op een branch, waardoor te herkennen is dat de wijzigingen samenhangen.

## Momentopname: positie op een branch via tijdstip

In `git` is een `commit` de bouwsteen voor versiebeheer. Als nieuwe versies voor bestanden worden toegevoegd aan het versiebeheer gebeurt dat via een `commit` die een globaal uniek ID heeft. Daarbij wordt ook een verwijzing naar de voorgaande commit bewaard. Omdat in `git` altijd alle commits uitgewisseld worden, zal na verloop van tijd elk systeem dat aan het versiebeheer deelneemt over alle versiebeheer-informatie beschikken. Uiteindelijk is ieder systeem in staat om alle versies van alle bestanden voor elke branch te reconstrueren. Dat is anders dan in STOP, waar bij uitwisseling niet de complete historie wordt gedeeld. Zowel in STOP als in `git` kan het voorkomen dat bij elke uitwisseling maar een stukje van de informatie wordt uitgewisseld, en dat een ontvangend systeem pas na enige tijd over alle informatie beschikt. Deze eigenschap wordt ook wel _eventual consistency_ genoemd.

![](img/versiebeheer-tijdstip.png)

In plaats van een `commit` kent STOP een [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname): een beschrijving van de versie van regelingen en informatieobjecten op de branch. Afhankelijk van de implementatiekeuzes van de uitwisseling bevat de beschrijving een versie van _alle_ regelingen en informatieobjecten, of (net als git) alleen de gewijzigde versies. Net als in `git` wordt verwezen naar de vorige momentopname, maar zoals hierboven is aangegeven verwijst _vorige_ naar de vorige uitwisseling van een versie voor een specifieke regeling of informatieobject. Waarbij *vorige* dus per regeling/informatieobject kan verschillen in één momentopname.

Een momentopname kent als identificatie de combinatie van doel plus tijdstip van het maken van de momentopname (in UTC op een seconde nauwkeurig). Dat is voldoende uniek voor opeenvolgende uitwisselingen tussen twee systemen. Het tijdstip wordt bepaald door het verzendende systeem, zodat de details van het verzenden en ontvangen daar geen invloed op hebben.

Als in de uitwisseling gekozen wordt voor het opnemen van alleen een versie van gewijzigde regelingen en informatieobjecten (zoals bij de uitwisseling met de LVBB), dan speelt het tijdstip de rol van "globaal unieke ID". Om een complete versiehistorie te verkrijgen moet van elke momentopname ook de basisversie uitgewisseld worden. De _eventual consistency_ bestaat er uit dat er in STOP rekening mee gehouden wordt dat er uitwisselingen kunnen zijn waardoor de geldende regelgeving niet bekend hoeft te zijn, en dat er aanvullende uitwisslingen (momentopnamen) nodig zijn om dat recht te zetten. De zogenaamde samenloop situaties.

Als in de uitwisseling gekozen wordt voor het opnemen van een versie voor alle regelingen en informatieobjecten, en het gaat bij de uitwisseling er alleen om een compleet beeld te krijgen van de regelgeving op een bepaald moment, dan speelt de waarde van het tijdstip wel een rol. Als een systeem via twee verschillende routes een momentopname voor dezelfde regeling/informatieobject en hetzelfde doel krijgt, dan is aan het tijdstip af te lezen wat de meest recente versie is, zelfs als niet alle tussenliggende momentopnamen uitgewisseld zijn.

## Push/pull via uitwisselingspakket en publicaties/revisies

Het `git` versiebeheer is ontworpen om tegelijkertijd op meerdere plaatsen gebruikt te worden. De aanpak is om op elke plaats een eigen geïsoleerd versiebeheer te voeren, en als dat opportuun is de versieinformatie te delen met andere plaatsen. Het delen gebeurt typisch via een centrale server zoals  github of gitlab.com. Het versturen naar zo'n server heet een `push` operatie, en ophalen van een server `pull`.

![Push/pull in git](img/versiebeheer-repositories-git.png)

Ook in het domein van de standaard moet het mogelijk zijn om zowel het geïsoleerd te werken aan regelgeving onder versiebeheer, en op de geëigende momenten de resultaten daarvan te delen.

![Push/pull in de STOP-keten](img/versiebeheer-repositories-stop.png)

STOP heeft twee mechanismen om de push/pull operaties mogelijk te maken. Algemeen bruikbaar is het [Uitwisselpakket](uitwisselpakket.md) waarin de clompete inhoud van een branch opgenomen kan worden samen met de [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) om het moment van de push/pull aan te geven. Maar de `push` van het bevoegd gezag naar de LVBB is anders ingericht: daar is de drager van de informatie een besluit, rectificatie, mededeling of revisie. De manier waarop de `push` vorm moet krijgen volgt uit de eisen die gesteld worden vanuit de wet- en regelgeving aan de instelling en wijziging van regelgeving. De inhoud van de branch wordt daarom in een specifiek type aanlevering aan de LVBB opgenomen, en de bijbehorende versiebeheer informatie is opgenomen in de [ConsolidatieInformatie](consolidatie-informatie.md) module.

Uitgangspunt voor de standaard is verder dat het bevoegd gezag over een versiebeheer/repository beschikt met daarin alle (toekomstig) geldende regelgeving en regelgeving waar nog aan gewerkt wordt. De LVBB beschikt over de (toekomstig) geldende regelgeving voor zover die gepubliceerd is, en heeft de functie om die aan eenieder beschikbaar te stellen. Voor andere partijen moet het mogelijk zijn om onder versiebeheer aan één (nieuwe) versie van regelgeving te werken zonder rekening te hoeven houden met alle andere versies die elders in de keten aanwezig zijn.