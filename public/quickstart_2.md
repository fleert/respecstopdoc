# Stap 2: Aanleveren Besluit 

Op een bepaald moment legt het bevoegd gezag de nieuwe Regelingversie en/of GIOversie vast in een bekend te maken besluit in haar plansoftware.  Een Besluit kan:

- een nieuwe versie van een Regeling bekendmaken (via een initieel besluit); of
- de wijzigingen op een bestaande versie van een regeling bekendmaken (via een wijzigingsbesluit).

Een besluit dient aangeleverd te worden met de set samenhangende STOP-modules die hieronder beschreven zijn. In de aanlevering aan de LVBB wordt daarnaast ook informatie van de wordtVersie van de Regeling (de [ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieRegeling.md) en [RegelingMetadata](quickstart_1_RegelingMetadata.md)), GIOversie(s) en OW-data meegeleverd (zie ook het [bronhouderkoppelvlakschema](@@@BHKV_URL@@@)).

**STOP-XML Modules Besluit**

* [data:ExpressionIdentificatie](quickstart_2_ExpressionIdentificatieBesluit.md) van het Besluit
* [data:BesluitMetadata](quickstart_2_BesluitMetadata.md) (met onderscheid hierbinnen tussen definitieve en ontwerpbesluiten)
* [data:ProcedureVerloop](quickstart_2_ProcedureVerloop.md) 
* [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md)
* [Besluit](quickstart_2_Besluiten.md) met hierbinnen de keuze uit twee modellen/STOP-XML modules

