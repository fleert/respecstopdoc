# XML-serialisatie 

IMOP schrijft voor dat STOP-informatie wordt uitgewisseld in de vorm van XML-documenten. In deze sectie wordt ingegaan op de wijze waarop dat plaats moet vinden.

[Modulaire structuur](xml_modulairestructuur.md)
: IMOP schrijft niet de XML-documenten als zodanig voor, maar IMOP-modules (XML-fragmenten) die in de documenten kunnen voorkomen. Hierdoor ontstaat er voor systemen die STOP implementeren ruimte om naar eigen inzicht een API te ontwerpen die van IMOP-modules gebruikt maakt.

[IMOP-modules in XML-documenten](xml_documenten.md)
: IMOP stelt wel eisen aan de manier waarop de IMOP-modules in XML-documenten zijn opgenomen.

[Verwijzing naar schema's](xml_schemalocation.md)
: In een XML-document kan verwezen worden naar de vindplaats van de schema's die gebruikt zijn om de XML te maken. In IMOP is dit optioneel. IMOP raadt een manier om XML-documenten te valideren. Als het gebruikt wordt, is het advies om naar de internet-locaties van de schema's te verwijzen.

[Witruimte in XML](xml_witruimte.md)
: IMOP gebruikt XML zowel voor het doorgeven van gegevens als voor tekst. Bij XML voor gegevens is witruimte tussen de XML elementen niet belangrijk: het wordt genegeerd. Bij XML voor tekst is dat anders: witruimte heeft daar wel degelijk een betekenis.

