# Beroep ongegrond


Het beroepsorgaan heeft uitspraak gedaan in de beroepsprocedure en heeft het beroep ongegrond verklaard. Dit heeft tot gevolg dat:
* Een eventuele schorsing wordt opgeheven. 
* Als er nog een hoger beroepstermijn ingaat, geeft het BG  alleen de opheffing van de schorsing door, want het besluit is dus nog niet onherroepelijk.  **N.B.**: deze mogelijkheid bestaat niet bij een beroep tegen een Omgevingsplan. 


## 1. Procedureverloopmutatie schorsing opgeheven
Was het besluit geschorst en zijn er in andere procedures ook geen schorsingen meer, maar is nog wel hoger beroep mogelijk, dan geeft het BG alvast door dat de schorsing is opgeheven. Door middel van een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) met een [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap). Deze bevat
* [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) is hierbij altijd  `/join/id/stop/procedure/stap_020` (schorsing opgeheven).
* [`voltooidOp`](data_xsd_Element_data_voltooidOp.dita#voltooidOp) bevat de datum waarop het beroep ongegrond is verklaard.
* (optioneel) [`meerInformatie`](data_xsd_Element_data_meerInformatie.dita#meerInformatie), link naar de BG-website met meer informatie over de juridische status van het besluit. Dit is niet de URL van de bekendmaking "Schorsing opgeheven".

Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).

### Voorbeeld procedureverloopmutatie schorsing opgeheven


```
<Procedureverloopmutatie>
  <bekendOp>2020-07-16</bekendOp>
  <voegStappenToe>
    <Procedurestap>
      <!-- Schorsing opgeheven  -->
      <soortStap>/join/id/stop/procedure/stap_020</soortStap>
      <voltooidOp>2020-07-14</voltooidOp>
    </Procedurestap>
  </voegStappenToe>
</Procedureverloopmutatie>
```

## 2. Verwerking definitief einde beroepsprocedure
Is met deze ongegrond verklaring een einde gekomen aan alle beroepsprocedures (eventueel na het ongebruikt verstrijken van een hogerberoepstermijn), dan geeft het BG via een directe mutatie een  [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) door met een [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap). Deze bevat:
* [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) is hierbij `/join/id/stop/procedure/stap_021` (beroep(en) definitief afgedaan).
* [`voltooidOp`](data_xsd_Element_data_voltooidOp.dita#voltooidOp) bevat de datum waarop het beroep definitief is afgedaan.

Lopen er nog andere beroepprocedures dan is er nog geen definitief einde en geeft het BG nu nog niets door.

## Zie ook

* Voor een volledig uitgewerkt scenario met voorbeelden, van opstelfase van het besluit waarvoor beroep is ingesteld tot en met het als onherroepelijk aanmerken van het besluit, zie [Uitleg voorbeelden scenario beroep ongegrond](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/01%20Beroep%20mogelijk/index.md). 
