# Uitgangspunten

<!--De huidige sectie *Over de standaard* is verplaatst naar hier als **Uitgangspunten**. Het bevat alle uitgangspunten, zowel qua scope, toepassingsgebied, juridisch, architectuur. Hierin komen ook alle pagina's terecht die niet gaan over een concrete toepassing, maar een thema beschrijven (zoals naamgevingsconventie)-->

Uitgangspunten en ontwerpkeuzes voor de standaard.

_Toepassingsgebied_
: STOP is een **ST**andaard voor **O**fficiële **P**ublicaties. De standaard richt zich ook op de informatie die aan de basis ligt van die publicaties. Op dit moment ondersteund de standaard besluiten van algemene strekking en besluiten over beleidsvoornemens, inclusief de daaruit volgende regelgeving. Een van de belangrijkste uitgangspunten voor STOP is dat concept regelgeving geautomatiseerd omgezet kan worden in een besluit en dat uit het gepubliceerde besluit geautomatiseerd geconsolideerde regelgeving afgeleid kan worden. Zie het [toepassingsgebied van de STOP standaard](toepassingsgebied.md).

_Publicatieketen_
: In de keten van plan tot publicatie zijn vele partijen betrokken: bevoegd gezagen(BG), adviesbureau's, bronhouderkoppelvlak(BHKV), landelijke voorziening bekendmaken en beschikbaarstellen(LVBB), de KOOP publicatie portalen, digitaal stelsel omgevingswet-landelijke voorziening(DSO-LV) en andere afnemers. STOP ondersteund deze [publicatieketen](publicatieketen.md) door voor te schrijven hoe in deze keten besluiten en (concept) regelingen uitgewisseld worden. 

_Juridische context_
: De Algemene wet bestuursrecht (Awb), de Bekendmakingswet (Bkw) en de Omgevingswet (OW) vormen de [juridische context](juridische-context.md) van de STOP standaard. Deze wetten bevatten uitgangspunten waarop de STOP standaard gebaseerd is.

_Toepassingsprofielen_
: STOP biedt vaak meerdere modellen om hetzelfde te bereiken. Dat is nodig omdat niet elk besluit of alle regelgeving op dezelfde manier opgeschreven wordt. Ook het te volgen proces om te komen tot wettelijk geldende regelgeving en de publicaties die daarvoor gedaan moeten worden zijn afhankelijk van de soort besluit of regelgeving. Voor het toepassen van de standaard voor concrete besluiten moet een selectie gemaakt worden van de mogelijkheden die STOP biedt. Dat wordt beschreven in toepassingsprofielen die als handleiding voor het gebruik van de standaaard zelf geen onderdeel uitmaken van STOP.

_Regelgeving digitaal_
: De huidige digitalisering van het toepassingsgebied van de standaard is versnipperd: verschillende aspecten worden met aparte informatiemodellen beschreven die niet (of niet goed) op elkaar aansluiten. Dezelfde informatie kent verschillende verschijningsvormen en het omzetten van de ene naar de andere vorm is een handmatig proces dat soms meer gestoeld is op interpretatie dan conversie. STOP is ontworpen om digitale herbruikbaarheid van juridische informatie beter te faciliteren, en het proces van totstandkoming van juridisch geborgde informatie te automatiseren.

_Versiebeheer voor regelgeving_
: Om het proces van totstandkoming van regelgeving te ondersteunen moeten alle deelnemende partijen in staat zijn met verschillende versies van een regeling te werken, en te weten hoe die elkaar opvolgen. STOP kent daarvoor een model om informatie over versies en de onderlinge relaties vast te leggen. Daarbij is gekeken naar andere versiebeheersystemen, met name [`git`](https://nl.wikipedia.org/wiki/Git_(software)). De ontwerpkeuzes in de standaard en de vergelijking met `git` is beschreven in de [principes van STOP versiebeheer](versiebeheer_principes.md). Deze principes komen op verschillende plaatsen in de STOP modellering terug.

_Functioneel presenteren_
: STOP gaat primair over de informatie in de regelgeving. En zo min mogelijk over de wijze van presenteren van deze informatie. STOP hanteert het principe van [functioneel presenteren](functioneelpresenteren.md) en biedt daarmee zoveel mogelijk vrijheid om de presentatie aan te passen naar gelang de toepassing (DSO-LV, officielebekendmakingen.nl, etc.).

_Verschillen met de huidige praktijk_
: STOP volgt op hoofdlijnen de huidige praktijk waar het gaat om opstellen van regelgeving, besluitvorming en consolidatie. Op een aantal punten zijn er [verschillen met de huidige praktijk](huidige_praktijk.md) om de doelstellingen van STOP te kunnen realiseren.

_Relatie met andere standaarden_
: Waar mogelijk maakt STOP gebruik van bestaande standaarden (bijvoorbeeld voor naamgeving en geometrie). In [relatie met andere standaarden](andere_standaarden.md) wordt beschreven op welke standaarden STOP gebaseerd is en welke standaarden door STOP gebruikt worden. 
