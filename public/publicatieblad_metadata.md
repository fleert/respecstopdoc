# Metadata publicatieblad

## Officiële publicatie metadata

De metadata van een [officiële publicatie](data_xsd_Element_data_OfficielePublicatieMetadata.dita#OfficielePublicatieMetadata) in een publicatieblad bevat een hele reeks aan informatie die opgedeeld kan worden in drie categorieën:

1. informatie over de publicatie zelf zoals de jaargang en het nummer,
2. informatie over het gepubliceerde document zoals de titel, de maker, grondslagen en onderwerpen
3. informatie uit het procedureverloop zoals de datum ondertekening.



**Zie ook**

* Het informatiemodel met de metadata van een [besluit](EA_9E3070BB712E49abA6B12A4A13ACACFC.dita#Pkg/68552B3417F34f5a94A1CD7F49B6FC0F), [kennisgeving](EA_233F9A157B1F4d11A41DBE8FC3424244.dita#Pkg/3FCB55375A0148019974D3231C29BFBB), mededeling(ea:{6F75DDB0-FE72-41b4-975C-BDC8353442C9}) en [rectificatie](EA_CE0701FE9A054073B11E9531BE7681D4.dita#Pkg/0ED2C08965514c0cB1C8B479BFD165AE).
* [XML schema](data_xsd_Element_data_OfficielePublicatieMetadata.dita#OfficielePublicatieMetadata) van de metadata van een officiële publicatie.
* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/03-LVBB-OffPub-Besluit-Metadata.xml) van de metadata van een besluit.



## Officiële publicatie versiemetadata

De [versiemetadata van een officiele publicatie](data_xsd_Element_data_OfficielePublicatieVersieMetadata.dita#OfficielePublicatieVersieMetadata) bevat enkel de datum waarop deze versie van de publicatie is verschenen. Indien er bij het publiceren van het document iets mis gaat, kan het voorkomen dat dezelfde publicatie nogmaals wordt uitgebracht.



**Zie ook**

* Het [informatiemodel](EA_E8EDA73EF3B842a9AB27E3B3E23B1470.dita#Pkg/90BB00386ADA4cbfA663CC0B33CAAE4C) met de versiemetadata van een officiële publicatie.
* [XML schema](data_xsd_Element_data_OfficielePublicatieVersieMetadata.dita#OfficielePublicatieVersieMetadata) van de versiemetadata van een officiële publicatie
* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/03-LVBB-OffPub-Besluit-versieMetadata.xml) van de versiemetadata van een besluit.

