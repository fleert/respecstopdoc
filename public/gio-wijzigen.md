# Een GIO wijzigen

De Bekendmakingswet stelt dat van een niet-tekstueel besluit-onderdeel, zoals een GIO, bij wijziging een nieuwe (GIO-)versie in z’n geheel moet worden aangeleverd. Een GIO wordt daarom niet gemuteerd als een regeling. Dat wil zeggen dat men dus niet de GIO-onderdelen die wijzigen in een vorm van renvooi aanlevert (zoals alleen de gewijzigde geometrische data). Een GIO wordt dus in zijn geheel vervangen.  

## Soorten GIO-mutaties
Er zijn drie soorten GIO-mutaties met ieder een andere juridische lading:
1. **Gewijzigd vaststellen**: Hiermee beperkt men de appeleerbaarheid van het besluit tot de wijzigingen ten opzichte van de vorige GIO-versie.  
1. **Opnieuw vaststellen**: In de bijlage van de regeling wordt de JOIN-ID van het GIO aangepast en in het gewijzigde GIO wordt *geen* verwijzing naar de vorige versie van het GIO opgenomen. Het GIO wordt hierdoor volledig opnieuw vastgesteld. 
1. **Intrekken en vervangen**: In de bijlage van de regeling wordt de JOIN-ID van het GIO verwijderd en vervangen door de JOIN-ID van een andere, nieuwe GIO. Het originele GIO wordt hiermee ingetrokken en bij het nieuwe GIO wordt aangegeven dat het de opvolger is van het originele GIO.

## 1. Gewijzigd vaststellen

Wanneer een GIO gewijzigd vastgesteld wordt, heeft het bevoegd gezag alleen dat gedeelte van het GIO dat daadwerkelijk gewijzigd is bekeken en is er dus geen besluit genomen over de niet-gewijzigde delen van het GIO. Om een GIO gewijzigd vast te  stellen, moet worden aangegeven wat de vorige versie van deze GIO is en daarmee wordt impliciet alleen besloten over de veranderingen in het nieuwe GIO ten opzichte van de vorige versie van deze GIO. Met een gewijzigde vaststelling beperkt men de appeleerbaarheid van het besluit tot de wijzigingen ten opzichte van de vorige GIO-versie.  

Dit gebeurt als volgt:
1. Geef in de tekst van het besluit of de toelichting aan wat er in het GIO wijzigt. Ook kan men in de toelichting een illustratie toevoegen waarin het verschil met de eerdere versie van het GIO is aangegeven. Dit maakt de wijziging begrijpelijker voor belanghebbenden, die deze informatie nodig hebben om te bepalen welke regels voor hen gaan gelden en of ze mogelijk bezwaar willen maken, zienswijzen willen indienen of [beroep](beroep.md) willen instellen. Geeft men de wijzigingen van het GIO aan in de tekst, dan is bezwaar, zienswijzen en beroep alleen mogelijk voor de wijzigingen in het nieuwe GIO ten opzichte van de oude GIO. Zijn de GIO-wijzigingen niet in de tekst aangegeven, dan is de gehele versie van het nieuwe GIO appelleerbaar, inclusief de ongewijzigde delen.
1. In de regelingtekst van een besluit wordt verwezen naar het GIO. Men kan daarbij het label van het IO ongewijzigd laten, maar dat hoeft niet. Juridisch maakt het niet uit.
2. De JOIN-ID in de tekst-bijlage van de regeling wordt gewijzigd.
3. In het gewijzigde GIO wordt een `was`-verwijzing naar de vorige versie van het GIO opgenomen. Hiermee beperkt de wijziging zich uitsluitend tot de verschillen tussen de nieuwe en de vorige versie van het GIO ondanks dat het GIO volledig wordt aangeleverd.
3. De raadpleegdatum of de achtergrond in de geografische context wijzigt mogelijk.
4. Het GIO wordt opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit.
5. De nieuwe versie van het GIO wordt als informatieobject bijgevoegd bij het besluit over de regeling.


## 2. Opnieuw vaststellen

Wanneer een GIO opnieuw wordt vastgesteld, geldt dat het bevoegd gezag het hele GIO heeft bekeken en over ieder onderdeel daarvan heeft besloten. Bij het volledig opnieuw vaststellen van een nieuwe GIO-versie is de nieuwe GIO-versie volledig appelleerbaar. Met het wijzigen van een GIO ontstaat dus geen nieuw work, maar loopt de geldigheid van de oude (huidige) expressie af, waardoor de `Toestand` van de "oude" GIO dus een 'datum geldig tot' krijgt.


Het opnieuw vaststellen van een GIO gebeurt als volgt:
1.  In de regelingtekst van een besluit wordt verwezen naar het GIO.
2. De JOIN-ID in de tekst-bijlage van de regeling wordt gewijzigd.
3. In het gewijzigde GIO wordt **geen** `was`-verwijzing naar de vorige versie van het GIO opgenomen.
3. De raadpleegdatum of de achtergrond in de geografische context wijzigt mogelijk.
4. Het GIO wordt opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit.
5. De nieuwe versie van het GIO wordt als informatieobject bijgevoegd bij het besluit over de regeling.

## 3. Intrekken en vervangen

Intrekken en vervangen is het intrekken van de laatste GIO-versie en het vaststellen van een volledig nieuwe GIO, een nieuw GIO-`work`. Er ontstaat een nieuwe expressie van hetzelfde work (zie [FRBR model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg)). In bepaalde situaties, bijvoorbeeld bij het samenvoegen van twee GIO's, is het gewenste resultaat niet met een wijziging te bereiken. Het oude GIO wordt dan ingetrokken en een nieuwe GIO wordt ingesteld. Om dan toch aan te kunnen geven dat het nieuwe GIO een opvolger is van de ingetrokken GIO('s), kan bij het nieuwe GIO aangegeven worden dat het GIO een opvolger is van één of meer andere GIO's. De opvolgingsrelatie wordt uitgedrukt met: 


Het intrekken en vervangen van een GIO gebeurt als volgt:
1.  In de regelingtekst van een besluit wordt verwezen naar de nieuwe GIO en de verwijzing naar de oude GIO wordt verwijderd.
2. Het label en de JOIN-ID in de tekst-bijlage van de regeling wordt gewijzigd in die van de nieuwe GIO. Die van het oude GIO wordt daarmee verwijderd.
2. Het ingetrokken GIO wordt opgenomen worden in de consolidatie-informatie van het besluit. Deze intrekking betreft altijd een `Work`.
3. Het nieuwe GIO wordt opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit.
3. De opvolgingsrelatie wordt met de elementen `opvolging` en ["opvolgerVan"](EA_EC4DE25BFB1341e6B632337DAF9C2791.dita#Pkg/823304801C6D45108A8BB5E86A666808)  toegevoegd aan de `InformatieObjectMetadata` om aan te geven dat het nieuwe GIO de opvolger is van het ingetrokken GIO.
4. De nieuwe versie van het GIO wordt als informatieobject bijgevoegd bij het besluit over de regeling.
  


### Voorbeeld - aanpassing regelingtekst

Het is verboden consumentenvuurwerk te gebruiken in het ~~vuurwerkverbodsgebied~~ vuurwerkverbodsgebied 2021.

Uitgedrukt in XML:

```xml
<Inhoud>
  <Al>Het is verboden consumentenvuurwerk te gebruiken in het <VerwijderdeTekst>
    <IntIoRef eId="art_2__para_1__ref_o_1_inst2"
               ref="gm9999_2__cmp_A__content_o_1__list_1__item_1__ref_o_1"
               wId="gm9999_1__art_2__para_1__ref_o_1">vuurwerkverbodsgebied</IntIoRef> </VerwijderdeTekst>
    <NieuweTekst> <IntIoRef eId="art_2__para_1__ref_o_1"
                             ref="gm9999_3__cmp_A__content_o_1__list_1__item_1__ref_o_1"
                             wId="gm9999_3__art_2__para_1__ref_o_1">vuurwerkverbodsgebied 2021</IntIoRef> </NieuweTekst>. </Al>
</Inhoud>
```

### Voorbeeld - aanpassing informatieobjecten-bijlage

  <span class="doorhalen">vuurwerkverbodsgebied </span>   vuurwerkverbodsgebied 2021
  <span class="doorhalen">`/join/id/regdata/gm0307/2019/gio993859238/nld@2020‑06‑16;2`</span>   `/join/id/regdata/gm9999/2021/gio993859462/nld@2021‑03‑23;1`

Uitgedrukt in XML:

```xml
<Begrip>
  <Term>
    <VerwijderdeTekst>vuurwerkverbodsgebied</VerwijderdeTekst>
    <NieuweTekst>vuurwerkverbodsgebied 2021</NieuweTekst> </Term>
  <Definitie>
    <!-- oude definitie met verwijzing naar oude GIO. 
           eId met inst2 toevoeging om hem uniek te maken -->
    <Al wijzigactie="verwijder"> <ExtIoRef eId="cmp_A__content_o_1__list_1__item_1__ref_o_1_inst2"
                                            ref="/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2"
                                            wId="gm9999_2__cmp_A__content_o_1__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2</ExtIoRef> </Al>
    <!-- nieuwe definitie met verwijzing naar nieuwe GIO -->
    <Al wijzigactie="voegtoe"> <ExtIoRef eId="cmp_A__content_o_1__list_1__item_1__ref_o_1"
                                          ref="/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1"
                                          wId="gm9999_3__cmp_A__content_o_1__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1</ExtIoRef> </Al>
  </Definitie>
</Begrip>
```

### Voorbeeld - intrekking
De `eId` van het voorbeeld verwijst naar de plek in het besluit waar de JOIN-ID van het GIO wordt verwijderd. De JOIN-ID van het GIO-work komt in het element `instrument`.

```
<Intrekkingen>
  <Intrekking>
    <doelen>
      <doel>/join/id/proces/gm9999/2021/WijzigingKernenOmgevingsplan</doel>
    </doelen>
    <instrument>/join/id/regdata/gm9999/2019/gio993859238</instrument>
    <eId>!wijziging#cmp_A__content_o_1__list_1__item_1__ref_o_1_inst2</eId>
  </Intrekking>
</Intrekkingen>
```
### Voorbeeld - opvolgingsrelatie

```
<opvolging>
  <opvolgerVan>/join/id/regdata/gm9999/2019/gio993859238</opvolgerVan>
</opvolging>
```

## Wanneer welke wijziging?


| Wat wijzigt er? | GIO-handeling | Opmerking |
| ---|---|---|
|Norm-ID en normwaarden|Nieuwe GIO (intrekken & vervangen)| Als de betekenis van de normwaarden en dus het karakter van de norm van het GIO wijzigt. Bijvoorbeeld: eerst "maximale nokhoogte" en daarna "maximale goothoogte". |
|Norm wijzigt, norm-ID niet| GIO opnieuw of gewijzigd vaststellen | Als het karakter van het GIO niet wezenlijk verandert. |
|Norm toegevoegd aan GIO | GIO opnieuw of gewijzigd vaststellen|Hele GIO appelleerbaar resp. alleen norm appelleerbaar. |
|Norm verwijderd uit GIO | GIO opnieuw of gewijzigd vaststellen| GIO opnieuw of gewijzigd vaststellen|Hele GIO appelleerbaar resp. alleen verwijdering norm appelleerbaar.|
|Normwaarden|GIO opnieuw of gewijzigd vaststellen |Alle normwaarden appelleerbaar resp. gewijzigde normwaarden appelleerbaar.|
|Normwaarde verwijderd bij locatie. Locatie moet worden verwijderd uit GIO.| GIO opnieuw of gewijzigd vaststellen |Keuze beïnvloedt appelleerbaarheid. |
|GIO zonder GIO-delen krijgt GIO-delen| GIO opnieuw of gewijzigd vaststellen|Keuze beïnvloedt appelleerbaarheid. |
|GIO krijgt meer of minder GIO-delen|GIO opnieuw of gewijzigd vaststellen |Keuze beïnvloedt appelleerbaarheid.  |
|GIO-delen worden geheel uit GIO verwijderd|GIO opnieuw of gewijzigd vaststellen | Keuze beïnvloedt appelleerbaarheid. |
|label GIO-deel|GIO opnieuw of gewijzigd vaststellen | ID GIO-deel blijft hetzelfde. Type wijziging beïnvloedt appelleerbaarheid. |
|ID GIO-deel| GIO opnieuw of gewijzigd vaststellen| Oud GIO-deel wordt verwijderd, nieuw GIO-deel wordt toegevoegd. Type wijziging beïnvloedt appelleerbaarheid.|
|Toewijzing van locaties aan (nieuwe) GIO-delen| GIO opnieuw of gewijzigd vaststellen| Keuze beïnvloedt appelleerbaarheid.|
|Geometrische data van locatie | GIO opnieuw of gewijzigd vaststellen|Is gelijk aan verwijderen oude locatie en toevoegen nieuwe. Type wijziging beïnvloedt appelleerbaarheid.|

## Zie ook
* [Voorbeeldbestanden inzake het wijzigen van een GIO bij een regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/02%20Regeling%20met%20GIO-wijziging).
* [Voorbeeldbestanden intrekken en vervangen van een GIO bij een regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/03%20Intrekking%20GIO%20en%20instelling%20nieuwe).
* Voor het terugtrekken van GIO-versies (expressions), zie [terugtrekken GIO](besluitproces_rectificatie_scenario_D3.md).