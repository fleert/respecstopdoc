# Versieinformatie in een uitwisselpakket

Een uitwisselpakket bevat bij [uitwisseling tussen organisaties](samenwerken_modellering.md) altijd de complete nieuwe regelingversie.
Naast de juridische tekst zijn ook de informatieobjecten waarnaar verwezen wordt vanuit de regeling aanwezig, [annotaties](annotaties.md) en 
[statusinformatie](context_modules.md) voor regeling en informatieobjecten (voor zover van toepassing) en informatie beschreven door andere standaarden zoals IMOW.

Het uitwisselpakket bevat een extra component, Versieinformatie, bestaande uit tenminste de module Momentopname. Hierin wordt 
aangegeven welke versie van de regeling, informatieobjecten en niet-juridische informatie in het uitwisselpakket is opgenomen. Als het uitwisselpakket gebruikt wordt om 
een gewijzigde versie uit te wisselen, is in de module Momentopname ook aangegeven op basis van welke bekende versie de nieuwe versie is gemaakt, zodat uit een vergelijking
van de versie in het uitwisselpakket en de bekende (basis)versie te achterhalen is welke wijzigingen precies zijn aangebracht. In dat geval kan in de (optionele) module 
Motivering ook een tekstuele toelichting op de wijzigingen gegeven worden.

## Versieinformatie component
Voorbeeld van het component Versieinformatie in een *pakbon* met alleen de verplichte module Momentopname:

```xml
<Component xmlns="https://standaarden.overheid.nl/stop/imop/uitwisseling/">
  <FRBRWork>/join/id/versie/gm9999/2020/766A3269AF5E4FDE85A6EC6A8E9CA22A</FRBRWork>
  <soortWork>/join/id/stop/work_024</soortWork>
  <heeftModule>
    <Module>
      <localName>Momentopname</localName>
      <namespace>https://standaarden.overheid.nl/stop/imop/data/</namespace>
      <bestandsnaam>Momentopname.xml</bestandsnaam>
      <mediatype>application/xml</mediatype>
      <schemaversie>1.2.0</schemaversie>
    </Module>
  </heeftModule>
</Component>
```

De component heeft een eigen `soortWork` en een [JOIN-identificatie](versieinformatie_naamgeving.md) die de informatie in het uitwisselpakket representeert. STOP kent geen versiebeheer
waarin meerdere versies/expressions van het uitwisselpakket worden ondersteund, dus het is voldoende een unieke JOIN-identificatie op work-niveau te gebruiken.
Aangezien de identificatie alleen voor gebruik door software nodig is, kan een door software gegenereerde unieke identificatie (bijvoorbeeld een UUID) gebruikt 
worden voor het `overig`-deel van de JOIN identificatie.

## Module Momentopname 

De module [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) bevat:

* het [doel](data_xsd_Element_data_doel.dita#doel) dat de wijziging identificeert waar de regelingversies en informatieobjectversies in het uitwisselpakket bij horen.
* de [gemaaktOp](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) datum/tijd waarop de versie van de modules in dit uitwisselingspakket is gemaakt.
* optioneel [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan) met een verwijzing naar de [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie),
  de versie die gebruikt is om de voor het doel gewenste wijzigingen aan te brengen.

De [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) wordt geïdentificeerd door de het FRBRWork van de regeling, het doel waarvoor de basisversie van de regeling is gemaakt,
en de gemaaktOp-datum/tijd van de gebruikte modules van de basisversie.

Bij het identificeren van de (basis)versie is gekozen voor een datum in plaats van een versie-identificatie zoals een FRBRexpression. De reden is dat de niet-juridische
informatie (zoals de metadata en informatie uit andere standaarden als IMOW) geen zelfstandige versie-identificatie kennen. Een landelijke voorziening als LVBB of DSO-LV
staat het toe dat (niet-juridische) informatie wordt gecorrigeerd of aangevuld zonder dat dit leidt tot een nieuwe expression voor een regeling of informatieobject.
Software die [tijdreizen](regelgeving_in_de_tijd.md) ondersteunt kan op basis van het tijdstip wel achterhalen welke informatie op de `gemaaktOp` datum/tijd beschikbaar was.

Voorbeeld van de module Momentopname bij export vanuit de LVBB:

```xml
<Momentopname>
  <doel>/join/id/proces/gm9999/2021/wijziging37</doel> 
  <gemaaktOp>2021-03-17T11:22:33Z</gemaaktOp>
</Momentopname>
```

Voorbeeld van de module Momentopname van een gewijzigde versie met een verwijzing naar de basisversie:

```xml
<Momentopname>
  <doel>/join/id/doel/gm9999/2021/36afeb226c9f458ca04041c2a7f062d3</doel>
  <gemaaktOp>2021-07-21T12:34:56Z</gemaaktOp>
  <bevatWijzigingenVoor>
    <UitgewisseldInstrument>
      <FRBRWork>/akn/nl/act/gm9999/2020/REG0001</FRBRWork>
      <gemaaktOpBasisVan>
        <Basisversie>
          <doel>/join/id/doel/gm9999/2020/wijziging37</doel>
          <gemaaktOp>2021-03-17T11:22:33Z</gemaaktOp>
        </Basisversie>
      </gemaaktOpBasisVan>
    </UitgewisseldInstrument>
  </bevatWijzigingenVoor>
</Momentopname>
```

Voorbeeld van de module Momentopname van een wijziging op een wijziging: 
```xml
<Momentopname>
  <doel>/join/id/proces/gm9999/2021/wijziging37</doel> 
  <gemaaktOp>2021-03-21T09:09:09Z</gemaaktOp>
  <bevatWijzigingenVoor>
    <UitgewisseldInstrument>
        <FRBRWork>/akn/nl/act/gm9999/2020/REG0001</FRBRWork>
        <gemaaktOpBasisVan>
            <Basisversie>
                <doel>/join/id/proces/gm9999/2021/wijziging37</doel> 
                <gemaaktOp>2021-03-17T11:22:33Z</gemaaktOp>
            </Basisversie>
        </gemaaktOpBasisVan>
    </UitgewisseldInstrument>
  </bevatWijzigingenVoor>
</Momentopname>
```

Zie de [scenario 3](uitwisselpakket_scenarios.md#scenario3) voor een toelichting op de toepassing van deze module.

## Module Motivering 

Het is niet altijd voldoende om uitsluitend de gewijzigde versie van regeling en informatieobjecten uit te wisselen. 
De maker van de nieuwe versie zal soms willen toelichtingen *waarom* de wijzigingen zijn aangebracht. Het bevoegd gezag kan deze onderbouwing 
gebruiken bij het formuleren van het besluit om de nieuwe regelingversie vast te stellen.

De Versieinformatie component heeft in zo'n gevan een extra module [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering). Deze module bevat een tekst volgens exact hetzelfde tekstmodel
als de motivering van een besluit.

Voorbeeld van de module Motivering:

```xml
<Motivering>
  <Kop>
    <Opschrift>Toelichting bij aanpassing Omgevingsplan Pyrodam</Opschrift>
  </Kop>
  <Divisietekst eId="acc__content_1"
                 wId="gm9999_1__acc__content_1">
    <Inhoud>
      <Al>Aangezien na de instelling van het vuurwerkverbod de overlast 
        door carbietschieten enorm is toegenomen, is in Artikel 2 
        een nieuw lid 2 tussengevoegd dat een verbod op het schieten
        van carbiet in het vuurwerkverbodsgebied instelt. </Al>
    </Inhoud>
  </Divisietekst>
</Motivering>
```

Zie ook de [XML voorbeelden](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/3.%20Samenwerking) bij [scenario 3](uitwisselpakket_scenarios.md#scenario3) voor een toelichting op de toepassing van deze module.

