# Afbeelding in de tekst

## Bestandsformaten

Als bron van een afbeelding worden alleen de (bitmap-)formaten geaccepteerd:

* image/jpeg: standaard bestandsnaam-extensie .jpg of .jpeg
* image/png: standaard bestandsnaam-extensie .png. Het gebruik van een transparante achtergrond is niet toegestaan.

Andere formaten worden niet ondersteund vanwege:

* PDF/A-1, voorgeschreven door de Bekendmakingswet, sluit een aantal formaten uit.
* image/gif is een "proprietary" formaat.
* image/tiff zorgt voor groter bestandsformaten die makkelijk verkleind kunnen worden door aan te leveren als image/jpg (met loss) of image/png (lossless).


## Afmeting van de afbeelding

De tekst moet geschikt zijn voor [weergave](regeltekst_weergave.md) in een officiële publicatie, en dat geldt daarom ook voor de afbeelding in de tekst. De manier waarop in STOP de afmeting van een afbeelding wordt gespecificeerd is geoptimaliseerd voor de weergave in de publicatie. Daarbij spelen drie attributen een rol: 

* **breedte**: het aantal pixels in horizontale richting aanwezig in het afbeeldingsbestand.
* **hoogte**: het aantal pixels in verticale richting aanwezig in het afbeeldingsbestand.
* **dpi**: "dots per inch", de resolutie die gebruikt wordt bij weergave van de afbeelding in de publicatie. Een afbeelding die digitaal is gemaakt heeft tegenwoordig vaak als eigenschap de resolutie, uitgedrukt in _ppi_ of _pixel per inch_. Dit is de aanbevolen waarde voor de _dpi_ van de afbeelding.

Hiermee is de grootte van de afbeelding te berekenen, zowel in mm in de A4-pagina van de publicatie (als die afgedrukt zou worden) als in percentage van de hoogte of breedte van de bladspiegel.

![Berekening van de grootte](img/tekst_agInlineTekstAfbeelding.png)

Als een afbeelding op een ander medium dan PDF getoond wordt, dan moeten de relatieve groottes (% van zetspiegel) gebruikt worden om de afbeelding te schalen ten opzichte van de tekst. Als de afbeelding bijvoorbeeld getoond wordt op een webpagina waarbij de tekst een breedte heeft van 600px, en de afbeelding heeft een breedte van 37% van de zetspiegel, dan moet de afbeelding op de webpagina geschaald worden naar 37% van 600 = 222px.

Een voorbeeld van een afbeeldingsgrootte voor dpi = 300

| 300 dpi | pixel |	inch | mm | % van zetspiegel |
| ------- | ----: | ---: | ---: | -------------: |
| hoogte  | 1200 | 4 | 101,6 | 42% |
| breedte | 900  | 3 | 76,2  | 49% |

Als dpi = 400 wordt de afbeeldingsgrootte van dezelfde afbeeldingsbestand:

| 400 dpi | pixel |	inch | mm | % van zetspiegel |
| ------- | ----: | ---: | ---: | -------------: |
| hoogte  | 1200 | 3 | 76,2 | 32%
| breedte | 900 | 2,25 | 57,2 | 37%

## Kwaliteitseisen aan een afbeelding

Uitgangspunt voor een afbeelding is dat een afbeeldingsbestand met voldoende kwaliteit aangeleverd wordt zodat deze zonder verdere verwerking kan worden opgenomen in de PDF (volgens PDF/A-1a) die gemaakt wordt voor de officiële publicatie. De regels om dit te bereiken zijn:

* De _hoogte_ en _breedte_ in pixels die bij de afbeelding zijn opgegeven moeten exact overeenkomen met de afmeting in pixels in het afbeeldingsbestand.
* De waarde voor _dpi_ moet minimaal 300 en maximaal 600 zijn.
* De afbeelding moet binnen de zetspiegel passen, dus de maximale hoogte is 240mm en de maximale breedte is 155mm.

Daarnaast geldt de kwalitatieve eis dat als de officiële publicatie op A4 afgedrukt wordt, de afbeelding enerzijds voldoende detail heeft om er goed uit te zien, en anderzijds elk bedoeld detail ook zichtbaar is. Als de resolutie van de afbeelding (in _ppi_) overeenkomt met de waarde voor _dpi_ dan zal daaraan voldaan zijn.

De zichtbaarheidseis is er vooral om te voorkomen dat een afbeelding met weinig pixels wordt vergroot om de minimale _dpi_-waarde te halen waardoor lijnen er "blokkerig" gaan uitzien. En dat een tekening met een grote dichtheid aan lijnen, die normaal alleen op bijvoorbeeld A0 afgedrukt zou kuinnen worden, kleiner gemaakt wordt om als een afbeelding met maximale _dpi_ net op een A4 te kunnen passen.