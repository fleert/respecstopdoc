# Kennisgeving en mededeling

Publicaties zijn in twee soorten te verdelen:

* Een *mededeling* is een integrale publicatie waarbij de volledige inhoud opgenomen is in de publicatie.
* Een *kennisgeving* is een zakelijke weergave waarbij alleen de meest noodzakelijke informatie in de publicatie staat en de overige relevante informatie elders te vinden is.

Een voorbeeld van een mededeling is de bekendmaking van een [besluit](inspraak_en_vaststelling.md). Een kennisgeving kan bijvoorbeeld gaan over de terinzagelegging van het besluit: de gegevens over waar en wanneer het besluit ter inzage ligt staat in de kennisgeving, het besluit zelf niet. (Het beschikbaarstellen van de geconsolideerde [regeling](regelgeving.md) is geen officiële publicatie en is dus geen mededeling of kennisgeving.)

STOP volgt hiermee de beschrijving van een mededeling en kennisgeving die in de toelichting op [Bekendmakingswet artikel 12](https://wetten.overheid.nl/jci1.3:c:BWBR0004287&artikel=12&z=2022-05-01&g=2022-05-01) in het Memorie van Toelichting (4.6) bij de [Wet elektronische publicaties](https://zoek.officielebekendmakingen.nl/stb-2020-262.html) is gegeven. In dat artikel wordt bepaald waar de publicatie gedaan moet worden.

## Structuur

Tenzij STOP een specifiek model heet voor een publicatie (zoals voor de bekendmaking van een [besluit](besluit.md)) kan voor een kennisgeving of mededeling een generiek model gebruikt worden. De structuur van een kennisgeving en van een mededeling is in STOP vrijwel identiek. Ze bestaan uit:

- een [identificatie](publicatie_naamgeving.md);
- een model voor de tekstuele inhoud:
  - [Tekstmodel kennisgeving](publicatie_kennisgeving_tekst.md),
  - [Tekstmodel mededeling](publicatie_mededeling_tekst.md);
- [annotaties](publicatie_annotaties.md), voor een kennisgeving en mededeling bestaande uit:
    - [metadata](publicatie_metadata.md) bij de kennisgeving/mededeling;
    - eventueel een [mutatie van het procedureverloop](publicatie_procedureverloopmutatie.md) van het besluit waar de kennisgeving of mededeling betrekking op heeft.

