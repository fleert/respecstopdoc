# Document als informatieobject

Uitgangspunt voor STOP is dat elke tekst die onderdeel is van een officiële publicatie en van de te publiceren documenten (zoals een besluit of regeling) wordt uitgewisseld volgens het [Implementatiemodel Officiële Publicaties](imop_index.md). Het implementatiemodel en de landelijke voorzieningen die daar gebruik van maken zijn zo ontworpen, dat de teksten voldoen aan alle eisen die er vanuit wet- en regelgeving aan gesteld worden. Zo kunnen de teksten volgens de Bekendmakingswet gepubliceerd worden, en zorgen de voorzieningen ervoor dat de teksten volgens de richtlijnen van digitale toegankelijkheid ontsloten worden.

Het is echter niet altijd mogelijk een tekst in een van de tekstmodellen van STOP op te nemen zonder de tekst te moeten overtypen. STOP ondersteunt daarom het aanleveren van teksten in een andere vorm, als [PDF bestand](https://nl.wikipedia.org/wiki/Portable_document_format). Omdat een tekst in een PDF document voor de landelijke voorzieningen minder goed te hanteren is, wordt het gebruik van PDF documenten sterk afgeraden. Het gebruik van PDF documenten wordt alleen toegestaan als dat in de [toepassingsprofielen](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) voor de betreffende besluiten of regelingen is aangegeven.

In een toepassingsprofiel kan het gebruik van PDF documenten worden toegestaan voor twee situaties.

1. Een PDF document dat bij de bekendmaking van een besluit samen met het besluit gepubliceerd moet worden. Het document bevat zelf geen (beleids-)regels en is geen onderdeel van de geconsolideerde regeling. Een voorbeeld van zo'n tekst is een milieueffectrapportage(MER)-rapport dat is opgesteld om de effecten van het besluit te onderzoeken.
2. Een PDF document dat een deel van de (beleids-)regels van een regeling bevat. Het wordt gepubliceerd als onderdeel van de bekendmaking van het besluit dat de regeling vaststelt, en het wordt ook beschikbaar gesteld als onderdeel van de geconsolideerde regeling. Een voorbeeld van zo'n tekst zijn meetvoorschriften die in de tekstmodellen van STOP niet gecodeerd kunnen worden vanwege bijzondere eisen tussen illustraties en de beschrijvende tekst.

In beide situaties geldt:

* Het PDF document wordt als een [informatieobject](io-intro.md) gemodelleerd in STOP. 
* Het PDF document moet (bij aanlevering ter publicatie) voldoen aan de eisen van PDF/A-1a (ISO 19005-1:2005) of PDF/A-2a (ISO 19005-2:2011) vanwege de [Regeling elektronische publicaties, art 2.2](https://wetten.overheid.nl/jci1.3:c:BWBR0045086&hoofdstuk=2&artikel=2.2).
* Het PDF document bevat nooit de gehele tekst van een besluit of regeling, maar alleen een deel van de tekst dat als bijlage gezien wordt. De hoofdtekst (en dus het lichaam) moet conform een van de STOP-tekstmodellen worden opgesteld.

Voor het eerste type PDF document geldt verder:

* De [publicatieinstructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie) moet `AlleenBekendTeMaken` zijn.
* Het PDF document wordt gezien als een bijlage bij het *besluit*. Het is niet gebruikelijk om naar het document te verwijzen vanuit de tekst van het besluit. Mocht dat wel nodig zijn, dan is uitgelegd in het toepassingsprofiel op welke manier dat moet gebeuren.

Voor het tweede type PDF document geldt:

* De [publicatieinstructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie) moet `TeConsolideren` zijn.
* Het PDF document wordt gezien als een bijlage bij de *regeling*. Net als voor andere informatieobjecten kan met een naam binnen de tekst van de regeling naar het PDF document verwezen worden en moet de naam samen met de unieke identificatie (/join/id/regdata/... van de expression) in een bijlage zijn opgenomen.
* Het PDF document wordt (net als andere te consolideren informatieobjecten) gezien als een separaat work. Als het PDF document niet wijzigt bij een wijziging van de regeling hoeft het niet opnieuw gepubliceerd te worden. Als de PDF wel gewijzigd moet worden (via een besluit) dan moet een nieuwe versie (expression) voor hetzelfde work onderdeel zijn van het besluit en in de tekst van de gewijzigde regeling genoemd worden. Als een PDF document na wijziging van de regeling geen onderdeel meer uitmaakt van de regeling, dan moet het ingetrokken worden.
