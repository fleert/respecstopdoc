# Witruimte in XML

## Mixed content in XML
IMOP gebruikt XML zowel voor het doorgeven van gegevens als voor tekst. 
* Bij XML voor gegevens is witruimte tussen de XML-elementen niet relevant en  wordt daarom genegeerd.
* Bij XML voor tekst is dat anders: witruimte heeft daar wel degelijk een betekenis. 

De tekst-bevattende elementen zijn te herkennen in de XML-schema's en in de documentatie daarvan aan:

    `mixed = "true"`

Dat geeft aan dat de inhoud van het XML-element als *mixed content* beschouwd moet worden: tekst met daarbinnen weer ("inline") XML-tags. Zo kan bijvoorbeeld het [`Al`](tekst_xsd_Element_tekst_Al.dita#Al)) element naast tekst ook het [`i`](tekst_xsd_Element_tekst_i.dita#i)- en [`sub`](tekst_xsd_Element_tekst_sub.dita#sub)-element bevatten. 

De eigenschap dat een XML-element mixed content is, wordt expliciet aangegeven in een schema. Als het attribuut `mixed` ontbreekt, dan is er geen sprake van mixed content en is witruimte tussen de XML-elementen niet van belang.

In mixed content zijn (net als in het HTML DOM model) spaties en regeleinden betekenisvol. Dus

```
<i>V</i><b>H</b>
```

staat in mixed mode voor `VH`, en 

```
<i>V</i> <b>H</b>
```
(met spatie) voor `V H` (*met spatie tussen V en H*).

Bij gebruik van XML voor het uitwisselen van gegevens wordt er wel voor gekozen om de XML via "pretty-print" of met inspringen (*indent*) op te stellen zodat het voor mensen beter te lezen is. Voor de mixed content elementen mag de software daarbij geen relevante witruimte zoals spaties of regeleinden toevoegen, want dan verandert de inhoud van het element. Omdat alleen in het XML-schema te zien is welke elementen het mixed content model gebruiken, zal software die geen gebruik maakt van het schema bij pretty-print/indent niet in staat zijn om een dergelijke pretty-print uit te voeren. Bij gebruik van STOP-tekstmodules in een XML-document wordt daarom geadviseerd geen pretty-print of indent toe te passen. Tekstmodules zijn gecodeerd in de namespace `https://standaarden.overheid.nl/stop/imop/tekst/` gedefinieerd door [imop-tekst.xsd](tekst_xsd_Main_schema_tekst_xsd.dita#tekst.xsd).

## IMOP-elementen voor witruimte

Spaties en regeleinden in de XML resulteren in spaties in de tekst. Het maakt daarbij niet uit of er in de XML één of meerdere opeenvolgende spaties staan: deze worden bij weergave van de tekst getoond als een enkele spatie. Om witruimte aan te brengen tussen tekstonderdelen kent IMOP speciale elementen.

### Witruimte-verdeling in een [Inhoud](tekst_xsd_Element_tekst_Inhoud.dita#Inhoud)
Tussen Alinea's, Lijsten, afbeeldingen en tabellen is de verwachting dat een witregel wordt geplaatst wanneer de tekst in een element Inhoud is geplaatst.

Voorbeeld XML:
```
    <Inhoud>
        <Al>Eerste alinea</Al>
        <Al>Tweede alinea</Al>
        <Lijst>
            <Li>
                <Linummer>-</Linummer>
                <Al>Eerste lijst-item</Al>
            </Li>
            <Li>
                <Linummer>-</Linummer>
                <Al>Tweede lijst-item</Al>
            </Li>
        </Lijst>
    </Inhoud>
```

Eenvoudig voorbeeld van de weergave:
```
    Eerste alinea

    Tweede alinea

    - Eerste lijst-item
    - Tweede lijst-item
```

### Geen witruimte in een [Groep](tekst_xsd_Element_tekst_Groep.dita#Groep)
Wanneer de tekst wordt geplaatst binnen een element `Groep`, zal tussen de elementen geen extra wit worden gegeven en kan de tekst aaneengesloten worden getoond.

Voorbeeld XML:
```
    <Inhoud>
        <Groep>
            <Al>Eerste alinea</Al>
            <Al>Tweede alinea</Al>
            <Lijst>
                <Li>
                    <Linummer>-</Linummer>
                    <Al>Eerste lijst-item</Al>
                </Li>
                <Li>
                    <Linummer>-</Linummer>
                    <Al>Tweede lijst-item</Al>
                </Li>
            </Lijst>
        </Groep>
        <Al>Derde alinea</Al>
    </Inhoud>
```

Voorbeeld van de weergave:
```
    Eerste alinea
    Tweede alinea
    - Eerste lijst-item
    - Tweede lijst-item

    Derde alinea
```

### Regelovergang binnen een alinea
Wanneer binnen een alinea een tekst op een nieuwe regel moet beginnen kan een element [br](tekst_xsd_Element_tekst_br.dita#br) worden gebruikt om een deel van de tekst op een nieuwe regel te beginnen. Het achter elkaar plaatsen van meer dan 1 `br`-element om extra witruimte tussen tekstblokken is niet toegestaan (zie ook [tekstweergave](regeltekst_inline.md#inline_weergave)); het scheiden van tekstblokken met witruimte kan alleen met een alinea.

Voorbeeld XML:
```
    <Inhoud>
        <Al>De eerste zin.<br/>De tweede zin.</Al>
    <Inhoud>
```

Voorbeeld van de weergave:
```
    De eerste zin.
    De tweede zin.
```


