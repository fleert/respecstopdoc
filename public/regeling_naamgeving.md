# Naamgeving van een regeling

Conform het [FRBR model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg) krijgt een regeling twee identificaties:

* één voor de regeling als work en
* één voor de versie (een expression).

STOP gebruikt een [invulling](naamgevingsconventie.md) van de Akoma Ntoso naamgevingsconventie (AKN NC) als basis voor de identificatie. AKN NC schrijft de structuur van de identificatie voor, STOP vult dat nader in.

Voor regelingen die een bevoegd gezag opstelt wordt in de identificatie "**act**" gehanteerd:

**Work**: `"/akn/" <land> "/act/" <overheid> "/" <datum_work> "/" <overig> `

**Expression**: `<work> "/" <taal> "@" [ <datum_expr> ";" ] <versie> [ ";" <overig> ]`

De codes betekenen:

| code       | betekenis                                                    | verplicht?                                |
| ---------- | ------------------------------------------------------------ | ----------------------------------------- |
| land       | Een 2-letter code van een land volgens ISO 3166-1 of van een code voor een subdivisie volgens ISO 3166-2. In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nl` ondersteund. | Ja                                        |
| overheid   | Code van het bevoegde gezag volgens één van de [waardelijsten](imop_waardelijsten.md#overheid). Het gaat hierbij om het bevoegd gezag dat uiteindelijk verantwoordelijk is voor de informatie, het is geen aanduiding van de opsteller van de informatie. | Ja                                        |
| datum_work | Datum van het ontstaan van de eerste versie van het work. Dit mag een volledige datum zijn (YYYY-MM-DD conform ISO 8601) maar het mag ook alleen een jaartal(YYYY) zijn. | Ja                                        |
| taal       | Taalcode volgens [ISO 639-2 alpha-3](https://www.iso.org/iso-639-language-codes.html). In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nld` ondersteund. | Ja                                        |
| datum_expr | Optioneel. Datum van het ontstaan van deze versie van het expression. Dit mag een volledige datum zijn (YYYY-MM-DD conform ISO 8601) maar het mag ook alleen een jaartal(YYYY) zijn. Deze datum moet gelijk zijn of later liggen dan `<datum_work>` | Nee                                       |
| versie     | Een [data:versienummer](data_xsd_Element_data_versienummer.dita#versienummer) voor het document. | Ja                                        |
| overig     | Overige kenmerken om de Expression (optioneel) of het Work (verplicht) te onderscheiden, bijvoorbeeld een dossiernummer of een afkorting gebaseerd op de naam. De kenmerken moeten compact zijn, het is niet de bedoeling om hiervoor een lange titel te gebruiken. "`<overig>`" bestaat uit een combinatie van cijfers, boven- en onderkast-letters, "_" en "-", te beginnen met een cijfer of letter (regex `[a-zA-Z0-9][a-zA-Z0-9\_\-]*`) met een maximale lengte van 128 karakters. Het bevoegd gezag is zelf verantwoordelijk voor uniciteit van dit "overig" deel. Er is geen centrale service om de waarde voor "overig" te genereren. | Ja, bij een work. Nee, bij een expressie. |

Voorbeelden zijn:

* `/akn/nl/act/gm0503/2021/Omgevingsplan`: Het omgevingsplan van Gemeente Delft zoals dat gemaakt is in 2021.
* `/akn/nl/act/gm0503/2021/Omgevingsplan/nld@1-0a`:  Versie 1.0a van eerdergenoemd omgevingsplan. 
* `/akn/nl/act/pv30/2022/Omgevingsvisie/nld@518d67613862486c9121784868d047e6;concept`: Onderhanden versie van de omgevingsvisie van de provincie Noord Brabant. Voor het aanduiden van het [data:versienummer](data_xsd_Element_data_versienummer.dita#versienummer) is gebruikt gemaakt van een [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier).
* `/akn/nl/act/pv30/2022/Omgevingsvisie/nld@2022-03-15;ontwerp`: De versie van 15 maart van de omgevingsvisie van Noord Brabant die deel uitmaakt van het ontwerpbesluit. 

## Publicaties

In STOP wordt AKN NC ook toegepast voor de publicaties van juridische documenten. Hiervoor wordt de structuur voorgeschreven door AKN NC anders ingevuld dan bij documenten opgesteld door het bevoegd gezag. Deze toepassingen zijn elders beschreven:

Identificatie van een [publicatie](publicatieblad_naamgeving.md) in het publicatieblad
: AKN NC hanteert een ander documenttype (`officialGazette`) voor publicaties. Daarnaast kennen de publicatiebladen een eigen systematiek van nummering van uitgaven.

Identificatie van [toestanden](consolidatie_naamgeving.md) van geconsolideerde regelgeving
: AKN NC stelt andere eisen aan de datums die in de identificatie gebruikt moet worden voor juridische documenten die in werking getreden zijn. Dit is uitgewerkt voor de consolidatie van geldende regelgeving zoals die door de LVBB beschikbaar gesteld wordt.

## Identificatie van tekstelementen in een regeling

AKN NC schrijft ook voor hoe elementen in een tekst geïdentificeerd moeten worden. Deze conventie wordt in STOP overgenomen en kent ook een [STOP invulling](eid_wid.md).
