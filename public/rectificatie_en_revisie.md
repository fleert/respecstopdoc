# Rectificatie en revisie

Wat als niet het goede is gepubliceerd? Ook in publicaties van besluiten of geconsolideerde regelingen/informatieobjecten kunnen fouten gecorrigeerd moeten worden. 

[Rectificatie](besluitproces_rectificatie.md)

: Met het publiceren van een rectificatie kunnen juridische fouten in de publicatie van een besluit gecorrigeerd worden.

[Revisie](consproces_revisie.md)

: Om niet-juridische informatie, of niet-juridische delen van de tekst van de geconsolideerde regeling te corrigeren of aan te vullen moet het instrument revisie gebruikt worden. Met een revisie kunnen ook annotaties gewijzigd of achteraf toegevoegd aan een geconsolideerde regeling of informatieobject.