# Geografisch informatieobject

## Inhoud deze sectie
In dit deel wordt uiteengezet wat een Geografisch informatieobject (GIO) is, welke onderdelen en varianten een GIO heeft, en hoe u kunt werken met een GIO. 

## Wat is een GIO?

Een GIO is een geografisch informatieobject. Het wordt gebruikt om een [werkingsgebied](werkingsgebieden-stop.md) van een (onderdeel van een) regeling vast te stellen. Een werkingsgebied het gebied aan waarbinnen een of meerdere juridische regels in de tekst van een regeling geldig zijn. Met een GIO kan de exacte ligging van (een of meer locaties in) het werkingsgebied op een juridisch correcte manier worden vastgelegd. Vanuit de tekst van een regeling wordt dan voor het werkingsgebied naar de GIO verwezen. Het GIO zelf bevat geometrische data die de begrenzing van de locatie(s) aangeven. Optioneel kan een locatie normwaarden hebben of behoren tot een groep (een zogenoemd GIO-deel).

## Vormen

Een GIO kan net als een regeling geconsolideerd worden en heeft daardoor twee verschijningsvormen:
* een juridisch geldige GIO. Deze is onderdeel van een besluit.
* een geconsolideerde GIO. Deze behoort tot de service-informatie.

## Kenmerken

* Een GIO volgt het [FRBR-model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg/29F89190FCE1470289A3566A3FC13663): een GIO is een [work](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg/AB6FDC32D3F3474eBBE7438305EB0E26) dat door de tijd heen bestaat, maar op een moment in de tijd altijd tot uitdrukking komt in een [expression](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg/F689C5A0EC9B4b3491F3558906741DB8) (specifieke versie).
* Een GIO-wijziging wordt aangeleverd als volledige nieuwe versie, met een verwijzing naar de vorige versie.
* Het GIO en de GIO-delen hebben een [unieke identificatie](io-expressionidentificatie.md) (JOIN-ID).


## Zie ook

* [Werkingsgebieden in juridische zin](werkingsgebieden-stop.md).
* <a href="vaststellen-wijzigen-gio.md">Hoe het juridisch zit met GIO's</a>.
* Het overzicht van <a scope="external" href="brmodule:geo_GeoInformatieObjectVersie">geo-bedrijfsregels</a>.
* <a scope="external" href="git:voorbeelden/Coderingen/GIO/GIO-varianten">Voorbeelden van GIO's</a> met verschillende typen geometrische data.