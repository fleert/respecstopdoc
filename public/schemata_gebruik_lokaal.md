# IMOP lokaal gebruiken 

In IMOP wordt naar schema's en schematrons verwezen via URLs. Het is niet altijd wenselijk om voor ieder gebruik een schema of schematron van het internet te downloaden. Het is ook mogelijk alle gebruikte [schema's, schematrons](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema/stop/%40%40%40IMOPVERSIE%40%40%40) en [transformaties](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aservice/schematron_xslt) te downloaden. Naast de IMOP-schemata zijn op die plaats ook alle andere schema's verzameld waar IMOP gebruik van maakt.

IMOP gebruikt een [URL](schemata_gebruik_url.md) om een schema, schematron of transformatie te identificeren. Om een koppeling te leggen tussen de URL van schemata en de bestanden in de download levert IMOP een zogenaamde [OASIS catalog](https://www.oasis-open.org/committees/entity) mee: [catalog.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema/stop/%40%40%40IMOPVERSIE%40%40%40/stop-catalog.xml).

Het formaat van deze catalog is:
```xml
<catalog xmlns="urn:oasis:names:tc:entity:xmlns:xml:catalog">
  ...
  <uri name="https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-data.xsd" uri="stop/@@@IMOPVERSIE@@@/schema/imop-data.xsd"/>
  <uri name="https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-aknjoin.sch" uri="stop/@@@IMOPVERSIE@@@/schematron/imop-aknjoin.sch"/>
  <uri name="https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-aknjoin.xslt" uri="service/schematron-xslt/@@@IMOPVERSIE@@@/imop-aknjoin.xslt"/>
   ...
</catalog>
```

Elk `<uri>`-element koppelt de URL van een schema of schematron (in het `name`-attribuut) aan het bestand uit de download, waarbij het `uri`-attribuut het relatieve pad ten opzichte van de plaats van de catalogus vermeldt. Bij het uitpakken van het gedownloade gecomprimeerde bestand is het daarom noodzakelijk om de mappenstructuur te behouden.

Software die gebruik maakt van de download moet de relatie tussen de URLs en bestandsnamen in de catalog gebruiken. Bij een volgende versie van IMOP kan de indeling van de download veranderen. Dit wordt niet als een wijziging van IMOP beschouwd, omdat bij de nieuwe versie ook een nieuwe catalog geleverd wordt die de juiste relaties bevat. 
