# Annotaties voor publicaties

Ook voor publicaties en de bron ervan, zoals voor besluiten en rectificaties, kent STOP [annotaties](annotaties.md). Het versiebeheer van annotaties bij publicaties is eenvoudiger dan voor annotaties bij regelingen en informatieobjecten.

## Wijzigen van annotaties
Voor besluiten en publicaties wordt geen gebruik gemaakt van het STOP versiebeheer. Uitgangspunt is dat instrumentversies elkaar in de tijd opvolgen. Een annotatie wordt gemaakt voor een bepaalde versie en blijft geldig voor opvolgende versies totdat een nieuwe versie van de annotatie wordt gemaakt. 

![Annotatie van publicatie](img/Annotaties.png)

Uitgangspunt in STOP is dat het bevoegd gezag ook inderdaad een nieuwe versie van de annotatie opstelt als dat nodig is. Het is voor software meestal niet te detecteren of een versie van een annotatie al dan niet bij een versie van een instrument past omdat voor die beoordeling inhoudelijke kennis nodig is. De ontvangende partij kan dus niet signaleren dat een annotatie gewijzigd had moeten worden. De leverende partij moet dus zorgen voor een nieuwe versie van de annotatie als dat nodig is.

## Implementatiekeuze

Als STOP-gebruikende systemen annotaties uitwisselen kan ervoor gekozen worden om:

1. annotaties alleen uit te wisselen als de annotatie gewijzigd is, òf 
2. altijd alle annotaties uit te wisselen.

Als ervoor gekozen wordt om een annotatie alleen bij wijziging uit te wisselen, dan moet een ontvangend systeem ervan uit kunnen gaan dat bij de eerste uitwisseling de annotatie aanwezig is (indien beschikbaar in het verzendende systeem). Er zijn in STOP geen annotaties die vanaf een bepaalde versie niet meer van toepassing kunnen zijn.