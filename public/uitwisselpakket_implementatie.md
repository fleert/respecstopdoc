# Implementatie

## Opbouw van het pakket

Het uitwisselpakket is een gecomprimeerd (zip) bestand met als extensie `.stop` in plaats van `.zip`. De inhoud van het pakket bestaat uit:

- Een bestand met de vaste naam `pakbon.xml` waarin de inhoud van het pakket beschreven wordt. Het specifieke XML-formaat is gedefinieerd en beschreven in [imop-uitwisseling.xsd](uws_xsd_Main_schema_uws_xsd.dita#uws.xsd). 
- Een of meer XML-bestanden. Elk uit te wisselen STOP-module is opgenomen in een apart XML-bestand.
- Bestanden die door de STOP-modules gebruikt worden, zoals plaatjes of informatieobject-bestanden.
- Bestanden met gerelateerde informatie als beschreven in andere standaarden (zoals IMOW).

Bij de bestandsnamen in het zip-bestand is het gebruik van hoofd- en kleine letters van belang: `PAKBON.xml` is een ander bestand dan `pakbon.xml`. Uit oogpunt van interoperabiliteit wordt geëist dat er geen bestandsnamen gebruikt mogen worden die slechts in hoofd- of kleine letters van elkaar afwijken. Het gebruik van een mapstructuur in het zip-bestand is wel toegestaan. De bestandsnamen moeten per map uniek zijn. 

Een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/2.%20Geldige%20regeling) van de inhoud van een zip-bestand is:

| Bestand in zip | Inhoud |
| ------------ | ----------- |
| `pakbon.xml` |  XML-bestand met de beschrijving van de inhoud van het pakket |
|       | **Gegevens van een versie van de regeling** |
| `Regeling-Identificatie.xml` | XML-bestand van de STOP-module [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) die een versie van een (geconsolideerde) regeling identificeert |
| `Regeling-Tekst.xml` | XML-bestand met de STOP-module [tekst van de RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact) |
| `img/baksteen.png` | Plaatje dat in de tekst van de regeling gebruikt wordt |
| `Regeling-Metadata.xml` | XML bestand van de STOP-module [RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata) |
| `Regeling-VersieMetadata.xml` | XML-bestand van de STOP module [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata) |
| `MomentOpname.xml` | XML-bestand van de STOP-module [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) |
|       | **Annotaties bij de regeling volgens een andere standaard (IMOW)** |
| `manifest-ow.xml` | XML-bestand met de beschrijving van annotaties volgens een andere standaard dan STOP (in dit geval IMOW) |
| `owLocaties.xml` | XML-bestand met gegevens volgens een andere standaard dan STOP (in dit geval IMOW locaties) |
| `owRegeltekst.xml` | XML-bestand met gegevens volgens een andere standaard dan STOP (in dit geval IMOW objecten voor juridische regels) |
|       | **Gegevens van een geo-informatieobject dat door de regeling gebruikt wordt** |
| `Vuurwerkverbodsgebied-Identificatie.xml` | XML-bestand van de STOP-module [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) die een versie van een (geconsolideerd) geo-informatieobject identificeert |
| `Vuurwerkverbodsgebied.gml` | GML-bestand van de STOP-module [GeoInformatieObjectVersie](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie) die het (geconsolideerd) geo-informatieobject bevat als [GeoInformatieObjectVersie](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie) |
| `Vuurwerkverbodsgebied-Metadata.xml` | XML-bestand van de STOP-module [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata) |
| `Vuurwerkverbodsgebied-VersieMetadata.xml` | XML-bestand met de STOP-module [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) die het GML-bestand aanroept |

## De pakbon beschrijft de inhoud en samenhang

De inhoud van een uitwisselpakket wordt beschreven in `pakbon.xml`. Dit bestand vervult verschillende functies. In de pakbon is opgenomen:

- Welke bestanden met STOP-modules en aanvullende bestanden in het uitwisselpakket zitten.
- Welke informatie bij elkaar hoort
- Hoe een STOP-module (of een bestand volgens een andere standaard) gelezen en gevalideerd moet worden

### Samenhangende informatie staat in één component
In bovenstaand voorbeeld bevat het uitwisselpakket twee groepen informatie: voor een versie van een regeling en voor een geo-informatieobject. De bijbehorende [pakbon.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/2.%20Geldige%20regeling/pakbon.xml) heeft voor elk van deze groepen een `<Component>` met de STOP-identificatie van de instrumentversie en het soort work:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Pakbon schemaversie="@@@IMOPVERSIE@@@"
  xmlns="https://standaarden.overheid.nl/stop/imop/uitwisseling/"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="https://standaarden.overheid.nl/stop/imop/uitwisseling/
  https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-uitwisseling.xsd">
  
  <Component>
    <FRBRWork>/akn/nl/act/gm9999/2020/REG0001</FRBRWork>
    <FRBRExpression>/akn/nl/act/gm9999/2020/REG0001/nld@2020-01-20;1</FRBRExpression>
    <soortWork>/join/id/stop/work_019</soortWork>
    <heeftModule>
        ... Informatie over XML-bestanden voor de regelingversie ...
    </heeftModule>
    <heeftBestand>
        ... Informatie over de overige bestanden voor de regelingversie ...
    </heeftBestand>
  </Component>
  
  <Component>
    <FRBRWork>/join/id/regdata/gm9999/2019/gio993859238</FRBRWork>
    <FRBRExpression>/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1</FRBRExpression>
    <soortWork>/join/id/stop/work_010</soortWork>
    <heeftModule>
        ... Informatie over XML-bestanden voor het geo-informatieobject ...
    </heeftModule>
  </Component>
```

### XML bestanden worden als module beschreven

Elk bestand waarin de XML-codering voor een STOP-module is opgenomen wordt apart beschreven:
```xml
<Module>
  <localName>RegelingCompact</localName>
  <namespace>https://standaarden.overheid.nl/stop/imop/tekst/</namespace>
  <bestandsnaam>Regeling/Tekst.xml</bestandsnaam>
  <mediatype>application/xml</mediatype>
  <schemaversie>1.1.0</schemaversie>
</Module>
```
De `<localName>` en `<namespace>` identificeren de STOP-module (zie het [overzicht](imop_modules.md)), de `<schemaversie>` geeft aan welke IMOP-versie gebruikt is voor de XML-serialisatie.

Het systeem van modules is uitbreidbaar. Naast STOP-modules kunnen andere standaarden de inhoud van een XML-bestand voorschrijven die ook als
een `Module` in de pakbon opgenomen kan worden. In zo'n XML bestand kan informatie staan gecodeerd volgens de andere standaard, maar het is ook
toegestaan dat het XML-bestand een pakbon of manifest is waarin beschreven staat hoe de informatie uit de andere standaard in het zip bestand is
opgenomen. IMOW-modules kunnen op deze manier uitgewisseld worden. Voor meer informatie zie de [IMOW specificaties](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD).


### Overige bestanden worden gebruikt door XML modules
Bestanden die door een XML-module gebruikt worden maar die zelf niet als XML gecodeerd zijn, worden ook bij de component vermeld. Als bijvoorbeeld
de XML-tekst van de regeling een plaatje van vuurwerk gebruikt, dan wordt dat aangegeven als:
```xml
<Bestand>
  <bestandsnaam>Regeling/img/consumentenvuurwerk.jpg</bestandsnaam>
  <mediatype>image/jpeg</mediatype>
</Bestand>
```
Hiervoor gelden een aantal regels:

- Naar elk `Bestand` wordt vanuit tenminste één XML-module van de component verwezen.
- Elk bestand dat door een XML-module wordt gebruikt moet als `Bestand` voorkomen.
- De bestandsnaam van het bestand is gelijk aan de map waarin de XML-module staat, plus de bestandsnaam van het bestand als gebruikt in de XML-module. Als de tekst van de regeling bijvoorbeeld in `regeling/regelingtekst.xml` staat en daarin wordt een plaatje gebruikt dat ans naam `img/baksteen.png` heet, dan moet het plaatje met de bestandsnaam `regeling/img/baksteen.png` opgenomen worden in het zip bestand.

## Gebruik van Bestand/Module bij informatieobjecten
Voor een informatieobject moet tenminste de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) module (en meestal ook [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata)) onderdeel zijn van de component.
In die module staan de bestanden genoemd waaruit het informatieobject bestaat. Bijvoorbeeld:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<InformatieObjectVersieMetadata schemaversie="@@@IMOPVERSIE@@@" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
  xmlns="https://standaarden.overheid.nl/stop/imop/data/" xsi:schemaLocation="https://standaarden.overheid.nl/stop/imop/data/ https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-data.xsd">
  <heeftGeboorteregeling>/akn/nl/act/gm9999/2020/REG0001</heeftGeboorteregeling>
  <heeftBestanden>
    <heeftBestand>
      <Bestand>
        <bestandsnaam>Vuurwerkverbodsgebied.gml</bestandsnaam>
        <hash>ca2d23c1776b15cbc83bd9031c70e3acf745d2f1f0c12c78c3b5325c7b81b69d4a95dacdd30238721fdba73a02151110f005d144d27c0fc82e3f7cb8d415e09f</hash>
      </Bestand>
    </heeftBestand>
  </heeftBestanden>
</InformatieObjectVersieMetadata>
```

Hoe die bestanden in de pakbon vermeld worden hangt af van het formaat van het informatieobject.
Voor informatieobjecten waarvan de bestanden (volgens IMOP) als XML bestand worden gecodeerd staat het bestand als `Module` vermeld:
```xml
<Module>
  <localName>GeoInformatieObjectVersie</localName>
  <namespace>https://standaarden.overheid.nl/stop/imop/geo/</namespace>
  <bestandsnaam>GIO993859238/Vuurwerkverbodsgebied.gml</bestandsnaam>
  <mediatype>application/xml+gml</mediatype>
  <schemaversie>1.0.4</schemaversie>
</Module>
```
Als dat niet het geval is, zoals bij PDF-informatieobjecten, dan staat het als `Bestand` vermeld:
```xml
<Bestand>
  <bestandsnaam>MER.pdf</bestandsnaam>
  <mediatype>application/pdf</mediatype>
</Bestand>
```

## STOP schrijft niet alle beperkingen aan het uitwisselpakket als expliciete bedrijfsregels voor
Het uitwisselpakket is zo generiek mogelijk ingestoken, voor een zo breed mogelijk scala aan scenario's waarin STOP-informatie wordt uitgewisseld. Er is een klein aantal [bedrijfsregels](businessrules_STOP_1200.dita#Bedrijfsregels__br_STOP1200) voor de [Pakbon](uws_xsd_Element_uws_Pakbon.dita#Pakbon) geformuleerd met als doel: 
* Consistentie van de pakbon, de informatie per component en de bestanden in het uitwisselpakket;
* Aanwezigheid van de minimaal noodzakelijke informatie voor een component;
* Reductie van de implementatie-eisen bij import van een uitwisselpakket.

STOP stelt geen striktere eisen, hoewel dat voor sommige toepassingen denkbaar en werkbaar zou zijn. Zo kent STOP geen expliciete bedrijfsregels over welke Moduletypes in welke Componenten kunnen voorkomen. Deze toekenning is versieafhanklijk en kan beter uit een relatietabel als [versiecompleet.xml](schemata_gebruik_versionering.md) worden afgeleid. 

Ook stelt STOP geen regels over welke informatie wanneer kan en moet worden uitgewisseld. Een voorbeeld hiervan zou kunnen zijn: _"Een module mag niet worden uitgewisseld middels een uitwisselpakket als dezelfde module van hetzelfde instrument ongewijzigd is ten opzichte van het meest recente uitwisselpakket tussen dezelfde verzender en ontvanger."_ De verwachting is dat er dergelijke afspraken tussen zender en ontvanger zullen worden opgesteld, afhankelijk van het scenario.
