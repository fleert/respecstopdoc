# Rectificatie van een besluit

Met een rectificatie worden juridische correcties gepubliceerd op een gepubliceerd besluit.

[Beschrijving](besluitproces_rectificatie_beschrijving.md)
: Beschrijft wat een rectificatie is, wanneer het gebruikt moet worden, en wat het effect van een rectificatie is.

[STOP-modellering van een rectificatie](besluitproces_rectificatie_model.md)
: Overzicht van de elementen van een rectificatie zoals gemodelleerd in STOP, en verwijzingen naar de technische documentatie.

[Renvooiweergave voor rectificaties](besluitproces_rectificatie_renvooi.md)
: STOP gebruikt het tekstmutatiemechanisme om correcties in de tekst van een besluit weer te geven. Het maken van deze renvooiweergave is in grote lijnen hetzelfde als voor het opstellen van een "standaard" wijzigingsbesluit. De verschillen worden op deze pagina toegelicht.

[Annotaties/service-informatie bij rectificaties](besluitproces_rectificatie_annotaties.md)
: De rectificatie kan correcties in tekst of informatieobjecten beschrijven die ook doorgevoerd moeten worden in de annotaties/service-informatie bij het besluit of regelingen. 

[Rectificatiescenarios](besluitproces_rectificatie_scenarios.md)
: Richtlijnen voor het opstellen van een rectificatie, onderverdeeld naar de verschillende aspecten van een besluit die gecorrigeerd kunnen worden.

[Voorbeelden](besluitproces_rectificatie_XML_voorbeelden.md)
: Voorbeelden van rectificaties die in IMOP-XML gecodeerd zijn.
