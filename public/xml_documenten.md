# IMOP-modules in XML-documenten 

## Eisen aan XML-documenten

De STOP-informatie die tussen systemen uitgewisseld wordt bestaat uit één of meer XML-documenten die, afhankelijk van de manier van uitwisselen, bijvoorbeeld in losse bestanden, IMOP [uitwisselpakket](uitwisselpakket.md), HTTP-response of ebML bericht opgenomen worden. De exacte vorm van de XML-documenten wordt bepaald door de API van het ontvangende of verzendende systeem. IMOP stelt alleen eisen aan de XML-documenten:

- De XML-documenten bevatten één of meer STOP-modules
- Deze STOP-modules zijn geserialiseerd zoals door IMOP is voorgeschreven.
- De modules in een XML-document gebruiken daarvoor allen dezelfde [IMOP versie](#schemaversie).
- De IMOP-versie bepaalt [welke schema's en schematrons](schemata_gebruik_versionering.md) gebruikt moeten worden voor de validatie van het XML document.
- Een XML-document mag aangeven [welk schema](xml_schemalocation.md) gebruikt is, maar dat hoeft niet.
- Als een XML-document aangeeft welk schema gebruikt is, dan wordt aangeraden naar de [locatie op internet](schemata_gebruik_url.md) te verwijzen.

## IMOP-versie: attribuut schemaversie { #schemaversie }

De serialisatie van elke IMOP-module heeft een verplicht attribuut [`schemaversie`](tekst_xsd_Attribute_Group_tekst_agSchemaVersie.dita#agSchemaVersie) dat aangeeft welke versie van IMOP is gebruikt. Bijvoorbeeld:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<RegelingCompact xmlns="https://standaarden.overheid.nl/stop/imop/tekst/" 
 schemaversie="@@@IMOPVERSIE@@@">
  ...
</RegelingCompact> 
```

Als er in een XML document meerdere IMOP-modules zijn opgenomen, dan moeten ze alle dezelfde waarde voor `schemaversie` hebben:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Regeling xmlns="https://mijn.systeem.nl/api/">
  <RegelingCompact xmlns="https://standaarden.overheid.nl/stop/imop/tekst/" 
   schemaversie="@@@IMOPVERSIE@@@">
    ...
  </RegelingCompact> 
  <RegelingMetadata xmlns="https://standaarden.overheid.nl/stop/imop/data/" 
   schemaversie="@@@IMOPVERSIE@@@">
    ...
  </RegelingMetadata> 
</Regeling>
```