# Revisie van een geconsolideerde regeling

Met een revisie kunnen niet-juridische wijzigingen in de geconsolideerde regeling worden aangebracht.

[Beschrijving revisie](consproces_revisie_beschrijving.md)
: Beschrijft wat een revisie is, wanneer het gebruikt moet worden, en wat het effect van een revisie is.

[STOP-modellering van een revisie](consproces_revisie_model.md)
: Overzicht van de elementen van een revisie zoals gemodelleerd in STOP, en verwijzingen naar de technische documentatie.

