# Opname juridische uitgangspunten in STOP

Deze sectie gaat in op hoe de wet- en regelgeving omtrent het bekendmaken, publiceren en consolideren van besluiten en regelingen is verwerkt in STOP.

* [Belangrijkste wijzigingen van de Omgevingswet ](wijzigingen-ow.md)
* [Besluit volgens STOP](stop-besluit.md)
* [Regeling volgens STOP](stop-regeling.md)
* [Bijlage volgens STOP](stop-bijlage.md)
* [Werkingsgebieden van regels in STOP](werkingsgebieden-stop.md)
* [Vaststellen en wijzigen van GIO's](vaststellen-wijzigen-gio.md)
* [Juridische verankering van STOP](juridische-verankering-stop.md)
* [Het maken van een wijzigingsbesluit in renvooiweergave](wijzigingsbesluit-renvooiweergave.md)
* [Directe mutatie](directe-mutatie.md)
* [Rectificatie](rectificatie.md)
* [Effectgebieden](effectgebieden.md)
* [Tijdelijk regelingdeel](tijdelijk-regelingdeel.md)
* [Pons](pons.md)
* [Verwerken van bezwaar tegen een STOP-besluit](verwerking-bezwaar.md)
* [Verwerken van beroep tegen een STOP-besluit](verwerking-beroep.md)
* [Authentieke bron](authentieke-bron.md)

