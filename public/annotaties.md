# Annotaties

[Annotaties in STOP](annotaties_intro.md)
: Een [annotatie](begrippenlijst_annotatie.dita#annotatie) bevat een interpretatie of duiding van de (juridische) inhoud van een instrument, zoals een regeling, informatieobject, besluit of publicatie. De annotatie is voor software te begrijpen.

[Annotaties voor regelgeving](annotaties_regelgeving.md)
: Annotaties voor regelgeving ontstaan op branches in het STOP versiebeheer. Een implementatiekeuze voor STOP-gebruikende systemen is het wel of niet opnieuw aanleveren van ongewijzigde annotaties bij een opvolgende versie van een regeling of informatieobject. 

[Non-STOP annotaties](annotaties_non-stop.md)
: Het annotatiemechanisme van STOP kan ook gebruikt worden door andere standaarden (zoals IMOW) om informatie met een regeling of informatieobject synchroon te houden. Het ontwerp van de informatie en van de uitwisseling ervan moet aan bepaalde eisen voldoen.

Elders in de documentatie is beschreven:

[Selectie van annotaties](besluit_selectie_annotaties.md)
: Worden alleen gewijzigde annotaties uitgewisseld, dan is een specifieke aanpak nodig om bij uitwisseling van een regeling- of informatieobjectversie te bepalen welke annotatie van toepassing is.

[Consolideren van annotaties](consolideren_annotaties.md)
: Worden alleen gewijzigde annotaties uitgewisseld, dan is een specifieke aanpak nodig om bij toestanden van een geconsolideerde regeling of informatieobject te bepalen of, en zo ja, welke annotatie van toepassing is.

[Annotaties voor publicaties](publicatie_annotaties.md)
: Annotaties voor publicaties en de bron van publicaties (besluit, rectificatie) kennen een eenvoudiger versiebeheer. Een implementatiekeuze voor STOP-gebruikende systemen is het wel of niet opnieuw aanleveren van ongewijzigde annotaties bij een opvolgende versie van een besluit of regeling. 

