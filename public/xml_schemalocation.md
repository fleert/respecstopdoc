# Verwijzing naar schema's 

## Attribuut `xsi:schemaLocation` 

Het is aan te raden in een XML-document vast te leggen tegen welke XSD het document is gemaakt en valideert. Dat maakt het eenvoudiger om de XML te lezen en valideren door software die niet op de hoogte is van de schemaversies zoals IMOP die hanteert.

Dit kan door via het attribuut  [`schemaLocation`](https://www.w3.org/TR/xmlschema-1/#Instance_Document_Constructions)  in de `xsi`-namespace de relatie vast te leggen tussen de namespace waarin een module is gedefinieerd en de locatie van het schema. De waarde van het attribuut bestaat uit één of meer paren van een namespace gevolgd door de locatie van het schema voor die namespace. Het is gebruikelijk om dat attribuut aan het root element van het XML document toe te voegen:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<RegelingCompact 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://standaarden.overheid.nl/stop/imop/tekst/ 
                        https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-tekst.xsd"

 xmlns="https://standaarden.overheid.nl/stop/imop/tekst/" 
 schemaversie="@@@IMOPVERSIE@@@">
  ...
  </RegelingCompact> 
```

of als er meerdere namespaces gebruikt worden:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Regeling xmlns="https://mijn.systeem.nl/api/"

    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://standaarden.overheid.nl/stop/imop/tekst/ 
                        https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-tekst.xsd
                        https://standaarden.overheid.nl/stop/imop/data/ 
                        https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-data.xsd">

  <RegelingCompact xmlns="https://standaarden.overheid.nl/stop/imop/tekst/" 
   schemaversie="@@@IMOPVERSIE@@@">
    ...
  </RegelingCompact> 
  <RegelingMetadata xmlns="https://standaarden.overheid.nl/stop/imop/data/" 
   schemaversie="@@@IMOPVERSIE@@@">
    ...
  </RegelingMetadata> 
</Regeling>
```

## `xsi:schemaLocation` verwijst naar internet

De aanbevolen werkwijze is om de [URL](schemata_gebruik_url.md) van het schema uit de corresponderende IMOP-versie als `xsi:schemaLocation` op te nemen. Elke XML-software die in staat is schema-informatie van het internet te betrekken kan dan de XML interpreteren volgens het schema dat gebruikt is om de XML te maken.

## `xsi:schemaLocation` is optioneel

Software die IMOP implementeert mag niet vertrouwen op de aanwezigheid of correctheid van het `xsi:schemaLocation` attribuut. De software moet het `schemaversie` attribuut van een module lezen en aan de hand daarvan bepalen [welke schema's en schematrons](schemata_gebruik_versionering.md) gebruikt moeten worden voor de validatie van de XML.

Het `xsi:schemaLocation` mag daarom worden weggelaten. Als het wordt weggelaten dan zal generieke XML-software (zoals XML-viewers of editors) de XML kunnen laten zien, maar geen ondersteuning bieden waarvoor kennis van het XML schema vereist is.

Het gebruik van relatieve paden (zoals `../stop/imop-tekst.xsd`) of lokale paden (zoals `C:/STOP/xsd/@@@IMOPVERSIE@@@/imop-tekst.xsd`) wordt sterk afgeraden. Dit verlaagt de portabiliteit van de XML-bestanden.
