# Scenario's

Het uitwisselpakket ondersteunt de uitwisseling van informatie in verschillende scenario's bij het opstellen van nieuwe (of gewijzigde) regelgeving. Deze scenario's zijn hier beschreven. Ze zijn ook terug te vinden in de [voorbeelden van uitwisselpakketten](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket).

## 1: Uitwisselen van geldigheidsinformatie
De geldigheidsinformatie (toestanden) van regelgeving kan uit de LVBB opgehaald worden. De informatie in de LVBB is direct afgeleid van de informatie die bij officiële publicaties is verstrekt. De exacte inhoud van het uitwisselpakket is afhankelijk van de functionaliteit van de downloadservice en kan in de loop van de tijd wijzigen, maar het pakket bevat tenminste.

- De [ConsolidatieIdentificatie](cons_xsd_Element_cons_ConsolidatieIdentificatie.dita#ConsolidatieIdentificatie) van de geconsolideerde regeling of het geconsolideerde informatieobject. Hierin is ook de identificatie van de regeling/informatieobject terug te vinden zoals het bevoegd gezag die hanteert.

- De [toestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) van de geconsolideerde regeling of het geconsolideerde informatieobject. Dit geeft aan wanneer de geldigheid/juridische werking van de geconsolideerde regeling/informatieobject is gewijzigd, en welke versie daarbij hoort.

### Voorbeeld scenario 1
Voor een regeling is dit uitgewerkt in een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/1.%20Geldigheidsinformatie). 

Bij dit voorbeeld wordt aan een aanleverend systeem (zoals de LVBB) een vraag gesteld als "Geef mij alle bekende toestanden van de regeling met work-id `/akn/nl/act/gm9999/2020/REG0001`". Op basis van het antwoord kan de ontvanger bepalen welke regelingversie geldig en nodig is. Zie voor een uitwerking scenario 2. 

## 2: Uitwisselen van geldende regelgeving
Uit de geldigheidsinformatie blijkt welke versie van een regeling of informatieobject op een bepaald moment (nu of in de toekomst) geldig is. Deze versie kan uit de LVBB worden opgehaald of vanuit een ander systeem worden uitgewisseld.

De metadata, de niet-juridische informatie, bij een versie van een regeling (of informatieobject) kan wijzigen zonder dat de regeling zelf (juridisch) wijzigt.  Omdat de metadata zelf geen identificatie bevat, wordt aan de hand van een datum (en tijd) de versie van de metadata vastgelegd. In het uitwisselpakket wordt deze informatie opgenomen in het component [Versieinformatie](uitwisselpakket_versieinformatie.md).

Voor de [Versieinformatie](uitwisselpakket_versieinformatie.md) bevat het uitwisselpakket:

* De [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) met het [doel](begrippenlijst_doel.dita#doel) en datum waarop de regelingversie is opgehaald. 

Voor een versie van een regeling bevat het uitwisselpakket:
- De [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van de versie van de regeling
- De tekst van de regeling. De module hangt af van het tekstmodel van de regeling. Een voorbeeld van een tekstmodel is [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact).
- De [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata) bevat specifieke informatie over de versie van de regeling.
- Gerelateerde informatie beschreven door andere standaarden

Voor een versie van een informatieobject bevat het uitwisselpakket:
- De [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van de versie van het informatieobject
- De [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) bevat specifieke informatie over de versie van het informatieobject.
- De inhoud van het informatieobject, afhankelijk van het formaat van het informatieobject. Voor geo-informatieobjecten bestaat de inhoud uit een [GeoInformatieObjectVersie](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie).

Als het uitwisselpakket alle informatie moet bevatten om een nieuwe versie van de regeling of het informatieobject te maken, dan dienen alle [annotaties](imop_modules.md) behorend bij de versie dan de regeling of het informatieobject te worden meegegeven voor zover ze beschikbaar zijn. Ook gerelateerde informatie beschreven in andere standaarden (zoals IMOW) dient dan opgenomen te worden in het pakket.

Een versie van de regeling en de versies van de informatieobjecten waar de regeling naar verwijst kunnen in één uitwisselpakket worden opgenomen, maar elk van de versies kan ook in een apart pakket ondergebracht worden.

### Voorbeeld scenario 2
Hiervan is een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/2.%20Geldige%20regeling) uitgewerkt. Een vraag aan het systeem bij dit scenario kan geformuleerd worden als "Geef mij de geconsolideerde regelingversie `/akn/nl/act/gm9999/2020/REG0001/nld@2020-06-16;3` en geconsolideerde informatieobjectversie `/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1`". 

Deze expression-id's kunnen worden afgeleid uit de Toestandsinformatie uit scenario 1.   

## 3. Uitwisselen van concept-regelgeving voor een nieuw besluit {#scenario3} 

Het opstellen van een nieuwe versie van de regelgeving gebeurt niet altijd door (een organisatie van) het bevoegd gezag zelf; het wordt soms geheel of gedeeltelijk in opdracht gedaan door een externe partij. Ook komt het voor dat een initiatiefnemer met een voorstel voor aanpassing van de regelgeving komt. In beide gevallen wordt het uitwisselpakket gebruikt om regelingversies uit te wisselen. 

Dit scenario is opgesplitst in: 

a) het verkrijgen van de versie waarop de wijzigingen moeten worden aangebracht, de basisversie, dit kan op twee manieren:

1. Het BG geeft een planbureau de opdracht om een gewijzigde versie op te stellen,
2. Een planbureau krijgt van een initiatiefnemer de opdracht om een gewijzigde versie op te stellen.

b) het aan het BG aanleveren van de gewijzigde versie. 

Zie voor ook de toelichting [samenwerken aan regelgeving](samenwerken_aan_regelgeving.md), over de STOP-modellering van dit scenario, inclusief de beperkingen.

### 3a1: Opdracht vanuit bevoegd gezag 

Als het bevoegd gezag een externe organisatie opdracht geeft een regeling aan te passen, dan zal het bevoegd gezag bepalen hoe de nieuwe versie geïdentificeerd moet worden. Hiertoe levert het bevoegd gezag de "basis" regelingversie, met in de [versieinformatie](uitwisselpakket_versieinformatie.md) de gewenste doel-identificatie voor de nieuwe versie. Het pakket bevat dan:

* Versieinformatie met de gewenste doel-identificatie
* de regelingversie(s) die als basis voor de wijzigingen gebruikt moet worden
* bijbehorende informatieobjecten
* indien van toepassing: informatie beschreven door andere standaarden 

Van scenario 3a1 is een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/3.%20Samenwerking/3a.%20Opdracht) uitgewerkt.

### 3a2: Initiatiefnemer 

Als een initiatiefnemer (bijvoorbeeld een fabriek, projectontwikkelaar, pret- of vakantie-park, etc.) voor zijn plannen afhankelijk is van wijzigingen in de regelgeving, dan kan de initiatiefnemer een planbureau zelf de opdracht geven een concept op te stellen. In dit geval krijgt het planbureau dus geen opdracht en ook geen basisversie van het bevoegd gezag. De basisversie kan dan verkregen worden door gebruik te maken van de download functie van de LVBB, zoals hierboven beschreven in scenario 1 en 2.

In het Doel van de gewijzigde versie moet de initiatiefnemer een UUID gebruiken om de gewijzigde versie te identificeren. Zie [versieinformatie](uitwisselpakket_versieinformatie.md) voor een voorbeeld.

Van scenario 3a2 is geen apart voorbeeld opgenomen, omdat dit (vrijwel) identiek is aan voorbeeld 2.

### 3b: Terugleveren aan het BG 
Als het planbureau klaar is met zijn werk, levert deze een nieuwe regelingversie op. Dit is een complete regelingversie. Compleet in die zin dat ook ongewijzigde onderdelen van de regeling of bijbehorende informatieobjecten opgenomen worden in het uitwisselpakket. Het pakket bevat dan:
* Versieinformatie met een verwijzing naar de basisversie waarop de wijzigingen gebaseerd zijn en eventueel informatie over de wijziging (motivering), die het BG in het besluit over de nieuwe regelingversie op kan nemen.
* de nieuwe versie(s) van de regeling (zowel gewijzigde als ongewijzigde onderdelen)
* bijbehorende informatieobjecten (zowel gewijzigd als ongewijzigd)
* informatie beschreven door andere standaarden (zowel gewijzigd als ongewijzigd)

Van scenario 3b is een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/3.%20Samenwerking/3b.%20Leveren%20aan%20BG) uitgewerkt.

## 4. Uitwisselen van een (concept-)besluit

Het is ook mogelijk niet de concept-regeling uit te wisselen maar de wijziging in een concept-besluit te verwerken. Het (concept-)besluit kan via het uitwisselpakket gedeeld worden met andere bevoegd gezagen of betrokkenen. Als dat gebeurt als onderdeel van de besluitvormingsprocedure dan ligt de nadruk niet op het beschrijven van de resulterende regeling maar van de wijzigingen die doorgevoerd worden. Het pakket bevat dan alleen de tekst van het besluit en de versies van de informatieobjecten die voor het besluit nodig zijn:

- Het besluit met tenminste de [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie), [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) en de tekst. Indien van toepassing kunnen ook annotaties, context modules en overige informatie voor zover ze betrekking hebben op het besluit.

- De nieuwe versie(s) van informatieobject(en). Van elke versie tenminste de [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie), [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) en de inhoud van het informatieobject. Voor geo-informatieobjecten bestaat de inhoud uit een [GeoInformatieObjectVersie](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie); voor PDF documenten uit het PDF-bestand.

Informatie die voor of na consolidatie van toepassing is kan weggelaten worden. Het gaat dan om de consolidatieinformatie, de versies van regelingen, annotaties bij regelingen of informatie over regelingen beschreven door andere standaarden.

Het is mogelijk om zowel het concept-besluit als de concept-regelgeving met één uitwisselpakket te delen door de bestanden beschreven in de laatste twee secties te combineren.

### Voorbeeld
Van scenario 4 is een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/4.%20Concept-besluit) uitgewerkt.

