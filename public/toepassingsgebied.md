# Toepassingsgebied van STOP

## Officiële publicaties
De STandaard Officiële Publicaties (STOP) is primair een standaard die officiële publicaties beschrijft. Dat zijn in principe alle publicaties die op [www.officielebekendmakingen.nl](https://www.officielebekendmakingen.nl/) verschijnen. In versie @@@IMOPVERSIE@@@ wordt daarvan een deel ondersteund, namelijk:

- Bekendmaking van een besluit van algemene strekking
- Kennisgeving en publicatie van een ontwerp van een besluit van algemene strekking

Deze publicaties worden gedaan in een van de publicatiebladen; in versie @@@IMOPVERSIE@@@ zijn dat:

- [Staatsblad](https://www.officielebekendmakingen.nl/staatsblad) en [Staatscourant](https://www.officielebekendmakingen.nl/staatscourant) voor publicaties van de rijksoverheid;
- [Provinciaal blad](https://www.officielebekendmakingen.nl/provinciaalblad) voor publicaties van een provincie;
- [Gemeenteblad](https://www.officielebekendmakingen.nl/gemeenteblad) voor publicaties van een gemeente;
- [Waterschapsblad](https://www.officielebekendmakingen.nl/waterschapsblad) voor publicaties van een waterschap;
- [Blad gemeenschappelijke regeling](https://www.officielebekendmakingen.nl/bladgemeenschappelijkeregeling) voor publicaties van een gemeenschappelijke regeling

De officiële publicaties hebben een grond in de [Algemene wet bestuursrecht](https://wetten.overheid.nl/BWBR0005537/), de [Bekendmakingswet](https://wetten.overheid.nl/BWBR0004287/), het [Besluit bekendmaking en beschikbaarstelling regelgeving decentrale overheden](https://wetten.overheid.nl/BWBR0024917/) en de bijbehorende wet- en regelgeving. Dat leidt tot juridische eisen aan een publicatie en de manier waarop de benodigde informatie aangeleverd moet worden. STOP houdt rekening met die eisen in zowel de informatiemodellering als in voorschriften voor uitwisseling van informatie.

Een publicatie bestaat traditioneel uit tekst, eventueel met afbeeldingen. In STOP is het ook mogelijk niet-tekstuele informatie te publiceren via [informatieobjecten](begrippenlijst_informatieobject.dita#informatieobject).

De te publiceren informatie moet aangeboden worden aan het [bronhouderskoppelvlak ](begrippenlijst_bronhouderkoppelvlak.dita#bronhouderkoppelvlak) van de Landelijke Voorziening Bekendmaken en Beschikbaarstellen (LVBB). Niet alle publicaties die op basis van STOP mogelijk zijn, worden door het bronhouderkoppelvlak ondersteund. Zie verder de [documentatie](@@@BHKV_URL@@@) van het bronhouderkoppelvlak.

## Beschikbaarstellingen
STOP beschrijft ook bepaalde informatie die direct volgt uit de officiële publicaties. In versie @@@IMOPVERSIE@@@ betreft dat:

- Geconsolideerde regelgeving (uit bekendgemaakte besluiten)
- Proefversies van regelgeving (uit ontwerpbesluiten of nog niet in werking getreden besluiten)

Het informatiemodel van STOP bevat alles dat nodig is voor de (publieke) beschikbaarstelling hiervan. De beschikbaarstelling van geconsolideerde regelgeving en proefversies is geen officiële publicatie en hoeft daarom niet te voldoen aan de strikte juridische eisen die aan officiële publicaties van (ontwerp)besluiten gesteld worden.

STOP is zo ontworpen dat de beschikbaar te stellen informatie geautomatiseerd afgeleid kan worden uit de officiële publicaties die aangeboden zijn aan het bronhouderkoppelvlak van de LVBB. Het is voor de LVBB niet altijd mogelijk op basis van alleen de officiële publicaties alle informatie af te leiden. STOP beschrijft ook de aanvullende informatie die daarvoor nodig is.

## Geïntegreerd proces van bekendmaken en consolideren

TODO: herschrijven, minder nadruk op het proces, meer op het feit dat deze onderwerpen in STOP zitten omdat het de bron is voor zowel de publicaties als de beschikbaarstellingen.

Hoewel STOP primair bedoeld is ter ondersteuning van het publicatieproces, leiden andere ontwerpeisen aan STOP ertoe dat STOP al eerder in het (besluitvormings-)proces toegepast wordt. Het gaat daarbij om het ondersteunen van:

- geautomatiseerde afleiding van geconsolideerde regelgeving;
- synchroon houden van regelgeving-gerelateerde informatie (bijv. beslisbomen) die niet door STOP wordt beschreven met de geconsolideerde regelgeving die de juridische bron voor die informatie is.

De standaard beschrijft daartoe een [geïntegreerd proces van bekendmaken en consolideren](proces_overzicht.md), waarbij niet alleen de publicatie maar ook de te publiceren informatie gemodelleerd wordt. In STOP worden daartoe beschreven:

- [regeling](begrippenlijst_regeling.dita#regeling)
- [informatieobject](begrippenlijst_informatieobject.dita#informatieobject)
- [besluit](begrippenlijst_besluit.dita#besluit)
- [geconsolideerde](begrippenlijst_consolidatie.dita#consolidatie) regeling of informatieobject

De manier waarop het proces in STOP is vormgegeven garandeert dat er op elk moment niet meer dan één versie van een regeling of informatieobject juridisch geldig is, en dat regelgeving-gerelateerde informatie hiermee synchroon gehouden kan worden.

In STOP verwijzen *besluit* en *regeling* naar wat erdoor beschreven wordt. Dit [wijkt af](proces_besluit_en_regeling.md) van de juridische conventie voor het gebruik van de begrippen besluit en regeling.