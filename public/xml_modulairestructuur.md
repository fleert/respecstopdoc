# Modulaire structuur

## IMOP beschrijft modules
De informatie die op basis van STOP uitgewisseld kan worden is ingedeeld in [modules](EA_1A45BBE5EC4F4bb2BBD7F5E701CE0479.dita#Pkg). Een module is een set van bij elkaar horende informatie die steeds 
als één geheel uitgewisseld moet worden. Voor een overzicht van de verschillende modules: zie de specialisaties van de abstractie [Module](EA_1A45BBE5EC4F4bb2BBD7F5E701CE0479.dita#Pkg/B4649197FA68449e913F625FA43F27DF) in het informatiemodel. Het implementatiemodel (IMOP) beschrijft hoe elke module in XML geserialiseerd moet worden. 

## IMOP is geen API
IMOP schrijft *niet* voor hoe een STOP-gebruikend systeem de modules moet gebruiken.
Voor elk systeem kan een API ontworpen worden die voor de verschillende services
een eigen keuze maakt voor de uitwisseling van informatie, zolang die uitwisseling 
is opgebouwd uit de STOP-modules. Voor een API kan dus wel een keuze gemaakt worden
welke modules voor een service aangeleverd of uitgeleverd worden, en hoe meerdere 
modules in één pakket worden samengebracht. Maar het is niet toegestaan de definitie
van een module aan te passen en bijvoorbeeld maar de helft van de informatie in 
een API op te nemen.

![Relatie tussen STOP modules en API specificatie](img/modulaire_structuur_api.png) 

Om de XML samen te stellen die aangeleverd moet worden aan een systeem moet dus 
in eerste instantie naar de API-specificatie gekeken worden. Deze API valt voor de specificatie
van de modules terug op de STOP-specificaties. De XML die bijvoorbeeld de
metadata van een besluit beschrijft ziet er dus in elke API hetzelfde uit, maar het 
is een API-specifieke invulling of bijvoorbeeld die XML in een apart bestand staat of ingebed is in een XML-bestand met meer informatie over het besluit, oftewel het bestand via HTTPS gedownload of in een ZIP bestand via ebMS verstuurd wordt.

Voor software die gebruik maakt van de standaard betekent dit dat de STOP-informatie uit het 
informatiemodel Officiële Publicaties altijd op dezelfde manier geserialiseerd wordt,
ongeacht het systeem waaraan informatie geleverd of waaruit informatie betrokken wordt.
Maar de details over welke informatie precies uitgewisseld wordt en hoe die verpakt
wordt, is geen onderdeel van STOP.

## IMOP kent een uitwisselingsformaat voor regelgeving

Voor het uitwisselen van informatie voor regelgeving definieert IMOP een [uitwisselingsformaat](uitwisselpakket.md) waarmee de relevante STOP-modules uitgewisseld kunnen worden. Dit is de voorgeschreven manier om een complete versie van een regeling en/of informatieobjecten met bijbehorende informatie uit te wisselen. De manier waarop de uitwisseling plaatsvindt wordt door IMOP niet voorgeschreven. Ook hier is enige vrijheid voor de STOP-implementerende systemen om API-specifieke keuzes te maken, bijvoorbeeld door de regeling en informatieobjecten apart aan te leveren.