# Modellen voor besluiten 

Bevoegde gezagen hebben in het opstellen van een Besluit de keuze uit 2 modellen: het [compacte model](DITA_OnbekendeLink.dita#Oeps) of het [klassieke model](DITA_OnbekendeLink.dita#Oeps). De keuze voor een besluitmodel moet passen bij de keuze voor het [tekstmodel van de Regeling](quickstart_1_Regelingen.md)):
* [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek) kan enkel in combinatie met [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) gebruikt worden.
* [BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact) kan gebruikt worden in combinatie met 
  * [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact)
  * [RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel)
  * [RegelingVrijetekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) 



## [tekst:BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek)

Het [klassieke besluitmodel](besluit-klassiek.md) volgt de lijn van besluiten, die gebruikelijk was alvorens de implementatie van STOP. Bij een klassiek initieel besluit komt de besluittekst vrijwel overeen met de regeltekst.

Hieronder een overzicht van elementen binnen een klassiek besluit.

| Element | Vulling | Verplicht?  |
| ------- | ------- | ----------- |
| [RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift)             | Binnen de context van besluittekst wordt dit element met de officiële titel van het besluit gevuld. | N  |
| [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef)                                   | Zie [Aanwijzingen voor de regelgeving § 3.5](https://wetten.overheid.nl/BWBR0005730#Hoofdstuk3_Paragraaf3.5). | N |
| [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek)               | De RegelingKlassiek binnen een Besluittekst kan bestaan uit [WijzigArtikelen](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel) (met optioneel hierbinnen een of meer [WijzigLeden](tekst_xsd_Element_tekst_WijzigLid.dita#WijzigLid)) of [Artikelen](tekst_xsd_Element_tekst_Artikel.dita#Artikel). Het [WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel) kan direct een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) of [Wijziginstructies](tekst_xsd_Element_tekst_WijzigInstructies.dita#WijzigInstructies) bevatten.  | J |\
| [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie)   | Bevat de wijzigingen tussen twee RegelingVersies (@was en @wordt). De attributen @was en @wordt dienen gevuld te worden met de AKN FRBRExpression  IRI van de respectievelijke RegelingVersies (zie ook de module [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieRegeling.md) bij een RegelingVersie). Meer informatie over de vulling van RegelingMutatie is te vinden onder [tekstmuteren](tekstmuteren.md). | N |
| [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)                               | Bevat het slotformulier, de dagtekening en de ondertekening van een besluit tot een nieuwe RegelingVersie. | N |                                          |
| [Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)                                 | Bijlage bij besluit. Wordt niet geconsolideerd.              | N                                           |
| [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting)                         | Toelichtende tekst voor een besluit. | N                                           |
| [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) | Toelichtende tekst per artikel. Wordt niet geconsolideerd.   | N                                           |
| [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering)                           | De Motivering bevat de vaststellingsdocumenten en de motivering van het besluit. Wordt niet geconsolideerd. | N                                           |
| [Inhoudsopgave](tekst_xsd_Element_tekst_Inhoudsopgave.dita#Inhoudsopgave)                     | Zie [Aanwijzingen voor de regelgeving aanwijzing 4.30](https://wetten.overheid.nl/BWBR0005730#Hoofdstuk4). Wordt niet geconsolideerd. | N                                           |

## [tekst:BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact)

Het compact besluitmodel maakt een duidelijke scheiding tussen de actie van het besluiten en de (gewijzigde) regelinginhoud (de inhoud van [WijzigBijlage](tekst_xsd_Element_tekst_WijzigBijlage.dita#WijzigBijlage)) waarover besloten is. De besluittekst komt enkel in de bekendmaking terecht, de regelinginhoud in zowel de bekendmaking als in de latere consolidatie van de regeling. 


Hieronder een overzicht van elementen binnen een compact besluit.

| Element  | Vulling  | Verplicht? |
| -------- | -------- | ---------- |
| [RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift)             | Binnen de context van besluittekst wordt dit element met de officiële titel van het besluit gevuld. | J |
| [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef)                                   | Zie [Aanwijzingen voor de regelgeving § 3.5](https://wetten.overheid.nl/BWBR0005730#Hoofdstuk3_Paragraaf3.5). | N |
| [Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam)                                 | Het Lichaam in de Besluittekst heeft een andere invulling dan het Lichaam in de Regelingtekst. Het Lichaam van een Besluittekst dient minimaal te bestaan uit een [WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel) (met optioneel hierbinnen een [WijzigLid](tekst_xsd_Element_tekst_WijzigLid.dita#WijzigLid)) of een [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel). Het [WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel) (al dan niet in het [WijzigLid](tekst_xsd_Element_tekst_WijzigLid.dita#WijzigLid)), dient een [IntRef](tekst_xsd_Element_tekst_IntRef.dita#IntRef) te hebben naar een [WijzigBijlage](tekst_xsd_Element_tekst_WijzigBijlage.dita#WijzigBijlage). | J |
| [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)                               | Bevat het slotformulier, de dagtekening en de ondertekening van een besluit tot een nieuwe RegelingVersie. | J |
| [WijzigBijlage](tekst_xsd_Element_tekst_WijzigBijlage.dita#WijzigBijlage)     | Dit element bevat alle wijzigingen voor een versie van een regeling. Hierbinnen wordt of een initiële regeling aangeleverd of een bestaande regeling gewijzigd (via [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie)). | J |
| [WijzigBijlage/RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact), [WijzigBijlage/RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel), [WijzigBijlage/RegelingVrijetekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) | Hierbinnen wordt de tekst van de initiële regeling opgenomen. Bijlagen en Toelichtingen hierbinnen worden geconsolideerd. | N          |
| [WijzigBijlage/RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie)   | Bevat de wijzigingen tussen twee RegelingVersies (@was en @wordt). De attributen @was en @wordt dienen gevuld te worden met de AKN FRBRExpression  IRI van de respectievelijke RegelingVersies (zie ook de module [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieRegeling.md) bij een RegelingVersie). Meer informatie over de vulling van RegelingMutatie is te vinden onder [tekstmuteren](tekstmuteren.md). | N |
| [Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)                                 | Bijlage bij besluit. Wordt niet geconsolideerd.              | N |
| [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting)                         | Toelichtende tekst voor een besluit. Wordt niet geconsolideerd. | N |
| [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) | Toelichtende tekst per artikel. Wordt niet geconsolideerd.   | N |
| [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering)                           | De Motivering bevat de vaststellingsdocumenten en de motivering van het besluit. Wordt niet geconsolideerd. | N |
| [Inhoudsopgave](tekst_xsd_Element_tekst_Inhoudsopgave.dita#Inhoudsopgave)                     | Zie [Aanwijzingen voor de regelgeving aanwijzing 4.30](https://wetten.overheid.nl/BWBR0005730#Hoofdstuk4). Wordt niet geconsolideerd. | N |


### Voorbeelden

* [Voorbeeld compact initieel besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/02-BG-Besluit-Tekst.xml)
* [Voorbeeld compact wijzigingsbesluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/02%20Wijziging%20in%20regeltekst/12-BG-Besluit-Tekst.xml)

