# Versiebeheer voor samenwerken

Het [STOP versiebeheer](versiebeheer_principes.md) is ook van toepassing op het samenwerken aan regelgeving. Hier wordt beschreven hoe het STOP versiebeheer toegepast wordt in de twee meest voorkomende samenwerk-scenario's:

* Het bevoegd gezag verstrekt zelf de basisversie waarop het adviesbureau wijzigingen aanbrengt.
* Het adviesbureau werkt vanuit een geldende regelingversie uit de LVBB.

Het verschil tussen deze scenario's is dat in het eerste geval het adviesbureau ook een basisversie kan krijgen die nog niet bekendgemaakt is. In beide scenario's hoeft het adviesbureau geen rekening te houden met tussentijdse wijzigingen in de geldende regeling. Het is de taak van het bevoegd gezag om eventuele tussentijdse wijzigingen te verwerken in het resultaat van de samenwerking.

## Bevoegd gezag verstrekt basisversie

![Bevoegd gezag verstrekt basisversie](img/versiebeheer-samenwerken_bg_opdracht.png)

Het bevoegd gezag maakt een nieuwe [branch](begrippenlijst_branch.dita#branch) op basis van een branch waarvoor al dan niet een (ontwerp)besluit is gepubliceerd. De inhoud van de branch wordt aan het adviesbureau geleverd. Zowel bevoegd gezag als het adviesbureau werken op dezelfde branch (in het plaatje met doel B).

Als het adviesbureau de gevraagde wijzigingen heeft aangebracht, dan wordt het resultaat aan het bevoegd gezag geleverd. Het bevoegd gezag weet welke versie aan het adviesbureau is geleverd, of kan dat aflezen omdat in het resultaat verwezen wordt naar de eerder uitgewisselde basisversie. Het bevoegd gezag zorgt voor de vervlechting van de door het adviesbureau aangebrachte wijzigingen met eventuele andere wijzigingen die in de tussentijd in de geldende regelgeving zijn aangebracht en/of wijzigingen die het bevoegd gezag zelf nog in voorbereiding heeft (in het plaatje voor doel C).  Hierdoor ontstaat de 'wordt' versie voor het nieuwe (ontwerp)besluit.

Als het bij langdurige projecten nodig is dat de versie die het adviesbureau onderhanden heeft, bijgewerkt wordt met tussentijdse wijzigingen, dan worden stappen 1 en 2 herhaald: het adviesbureau levert een tussenresultaat aan het bevoegd gezag (stap 2), het bevoegd gezag vervlecht de versie met de benodigde andere wijzigingen, en levert de complete versie aan het adviesbureau (stap 1) dat op basis van die versie verder werkt.

## Adviesbureau haalt geldende versie uit LVBB

![Adviesbureau haalt geldende versie uit LVBB](img/versiebeheer-samenwerken_initiatiefnemer.png)

Een adviesbureau kan ook de (nu of in de toekomst) geldende regeling als basisversie nemen. Die kan opgehaald worden met de LVBB downloadservice. In dit scenario maakt niet het bevoegd gezag, maar het adviesbureau een nieuwe branch. De software van het bevoegd gezag ziet die branch pas bij uitlevering van het resultaat. Het bevoegd gezag kan de branch voortzetten of (als getekend in het plaatje) een eigen branch maken op basis van de dan geldende regelgeving en de resultaten van het adviesbureau daarmee vervlechten.

Mochten er vanwege de vervlechting aanvullende werkzaamheden door het adviesbureau nodig zijn, dan kan het eerste patroon (bevoegd gezag verstrekt basisversie) toegepast worden op de branch waar het bevoegd gezag op verder werkt.