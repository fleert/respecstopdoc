# Artikelsgewijze regeling

Regelingen met voorschriften van algemene strekking kennen een opbouw waarbij de voorschriften in de vorm van artikelen worden opgeschreven. In het standaard tekstmodel (het "compacte" model) vormen de artikelen samen het lichaam van een regeling.

[Standaard tekstmodel](regeling_artikelsgewijs_tekst.md)
: STOP kent een standaard tekstmodel (het "compacte" model) dat voor alle artikelsgewijze regelingen wordt gebruikt.

[Toelichting](regeling_toelichting.md)
: Bij een regeling kan een toelichting bevatten, die steeds samen met de artikelen wordt bijgewerkt. De standaard kent verschillende mogelijkheden om de toelichting te structureren. Ook is het mogelijk via een annotatie een relatie te leggen tussen de toelichting en wat er toegelicht wordt.

[Begrippen](regeling_begrippen.md)
: In een regeling kunnen begrippen gedefinieerd worden. Voor het ontdekken, beheren, en harmoniseren van begrippen wordt vaak een systematiek gebruikt die gebaseerd is op linked data. STOP kent een annotatie en een mechanisme voor artikelsgewijze regelingen om een brug te slaan tussen een begrip zoals dat in de tekst voorkomt en software die van linked data gebruik maakt.

[Klassiek tekstmodel](regeling-klassiek.md)
: Voor sommige Rijksbesluiten wordt een afwijkend tekstmodel voor de hoofdregeling gebruikt. In dit tekstmodel worden bepaalde onderdelen opgenomen in de regeling die afkomstig zijn uit het besluit waarmee de regeling wordt ingesteld.