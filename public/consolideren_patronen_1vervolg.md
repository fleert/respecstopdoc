# Vervolgpublicatie

## Patroon
Een juridisch document wordt gepubliceerd waarin aanpassingen op een eerdere publicatie gedaan worden voor hetzelfde doel. 

Een vervolgpublicatie kan een vervolgbesluit van het bevoegd gezag zijn, maar ook een [rectificatie](besluitproces_rectificatie.md) of mededeling van een uitspraak van een beroepsorgaan. 


## Uit te wisselen informatie
Zolang in de vervolgpublicatie alleen een gewijzigde of eerste versie gegeven wordt voor een regeling, informatieobject of tijdstempel, kan dit opgelost worden door deze aanpassingen op te nemen in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module zoals hiervoor beschreven in de patronen [1A t/m 1F](consolideren_patronen.md). 

### Verwijderen

Als de vervolgpublicatie daarentegen iets weghaalt uit de voorgaande publicatie, bijvoorbeeld een regeling, informatieobject of tijdstempel dan volstaan deze patronen niet. 

![Terugtrekking](img/consolideren_patronen_1vervolg.png)



* De instelling of wijziging van een regeling maakt geen deel meer uit van de beoogde wijzigingen voor een doel, dus er is geen nieuwe versie meer voor de regeling in de branch. Voeg [TerugtrekkingRegeling](data_xsd_Element_data_TerugtrekkingRegeling.dita#TerugtrekkingRegeling) toe aan de `ConsolidatieInformatie` met als `instrument` de work-identificatie van de regeling. De `eId` verwijst naar de tekst van de vervolgpublicatie waarin staat dat de regeling niet meer gewijzigd wordt.

* De instelling of wijziging van een informatieobject maakt geen deel meer uit van de beoogde wijzigingen voor een doel. Voeg [TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) toe aan de `ConsolidatieInformatie` met als `instrument` de work-identificatie van het informatieobject. Als dit het resultaat is van een wijziging van de geboorteregeling, dan wijst het `eId` naar de plaats in de regeling zoals voor de [intrekking](io-intrekken.md) van een informatieobject is beschreven. Anders verwijst `eId` naar de tekst van de vervolgpublicatie waarin staat dat de geboorteregeling niet meer gewijzigd wordt.

* De intrekking van een regeling of informatieobject maakt geen deel meer uit van de beoogde wijzigingen voor een doel. Voeg [TerugtrekkingIntrekking](data_xsd_Element_data_TerugtrekkingIntrekking.dita#TerugtrekkingIntrekking) toe aan de `ConsolidatieInformatie` met als `instrument` de work-identificatie van de regeling of het informatieobject. De `eId` verwijst naar de tekst van de vervolgpublicatie waarin staat dat de regeling niet meer gewijzigd wordt.

* De eerder vermelde inwerkingtreding wordt uitgesteld naar een onbekende datum en/of de eerder vermelde datum van geldigheid bij terugwerkende kracht wordt (voorlopig) geschrapt. Voeg [TerugtrekkingTijdstempel](data_xsd_Element_data_TerugtrekkingTijdstempel.dita#TerugtrekkingTijdstempel) toe aan de `ConsolidatieInformatie` met als `soortTijdstempel` de [tijdstempel](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) waar het om gaat. De `eId` verwijst naar de tekst van de vervolgpublicatie waarin staat dat de eerder gegeven datum komt te vervallen.

  
