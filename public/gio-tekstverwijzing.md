# 1. Verwijs in de tekst naar het GIO

De [generieke manier van verwijzen naar IOs vanuit een besluit](io-tekstverwijzen.md) geldt ook voor GIOs.

* In de tekst van een regeling wordt naar GIO's verwezen met een mens-leesbaar label, zoals bijvoorbeeld 'militaire oefenterreinen'. Ook kan de JOIN-ID in de tekst van de regeling worden opgenomen. Juridisch maakt het niet uit welke werkwijze wordt gekozen.

## Voorbeeld-verwijzing met label



> **Artikel 10**  
> 
> Militaire oefenterreinen zijn omheind door een hek.
> 

De term "Militaire oefenterreinen" is label.

In de bijlage wordt het label vervolgens gekoppeld aan het JOIN-ID. Zie daarvoor [stap 2](gio-opnemen-bijlage.md).


## Verwijzing naar een norm 

### Norm met kwantitatieve normwaarden 

De volgende manier van verwijzen wordt gebruikt als zowel normwaarden als werkingsgebied in een GIO worden opgenomen. 

> **Artikel a**  
> 
> In de gebieden aangegeven in de 'bouwhoogtekaart' geldt een maximale bouwhoogte.

### Norm met kwalitatieve normwaarden

De volgende manier van verwijzen wordt gebruikt als zowel normwaarden als werkingsgebied in een GIO worden opgenomen. 

> **Artikel b**  
> 
> In de wateren aangegeven in de 'kwaliteit zwemwaterkaart' wordt onder 'slecht' verstaan wateren die niet geschikt zijn om in te zwemmen, onder 'matig' wateren waarin het sterk is af te raden om te zwemmen en onder 'goed' wateren die geschikt zijn om in te zwemmen. 

### Normwaarde in de tekst

De volgende manier van verwijzen wordt gebruikt als de normwaarde in de tekst wordt opgenomen, en het werkingsgebied in een GIO. 

> **Artikel c**
> 
> In de gebieden aangegeven in de 'bouwhoogtekaart' geldt een maximale bouwhoogte van 10 meter. 

## Verwijzing naar een GIO-deel {#giodeel}

Een regeling-tekst kan op de volgende manier met een label verwijzen naar een GIO-deel. In de tekst van een regeling moet de naam van het GIO-deel zo worden gebruikt dat eenduidig wordt verwezen naar een deel van het GIO, zoals genoemd in de bijlage. Dit kan op een juridisch sluitende manier door de naam van het GIO-deel in combinatie met het GIO-label te vermelden. 

### Voorbeeld
> **Artikel p**
> 
> Op de gronden behorend bij 'zone 1' in 'beperkingengebied Schiphol' gelden de volgende regels.

*Zone 1* verwijst hierbij naar het GIO-deel en *beperkingengebied Schiphol* naar het GIO.  

De JOIN-ID's van de GIO-delen worden slechts gebruikt om in de service-informatie een GIO-deel aan te wijzen. Deze komen dus niet terug in de bijlage van de regeling. De JOIN-ID van het GIO wel, zoals hieronder toegelicht bij stap 2. 

## Maak van het label een link

### Link naar GIO
Het is verplicht om van het label van een GIO in de lopende tekst een link te maken naar de plaats in de bijlage waar het wordt gekoppeld aan het JOIN-ID. Het gaat hier dus om een interne link (`IntIoRef`) die verwijst naar de JOIN-ID.

In de bijlage moet er vervolgens een rechtstreekse link ([ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef)) naar het GIO worden opgenomen.

### Link naar GIO-deel
Naar GIO-onderdelen kan niet rechtstreeks worden gelinkt, behalve naar GIO-delen. Hiervoor wordt ook een [ExtRef](tekst_xsd_Element_tekst_ExtRef.dita#ExtRef) gebruikt, waarin de JOIN-ID van het GIO-deel wordt opgenomen. De link-tekst is gelijk aan het label van het GIO-deel. 

De JOIN-ID van een GIO-deel heeft de volgende syntax:

```
<JOIN-ID GIO>/!groepid=<groepID>
```

Bijvoorbeeld:

```
/join/id/regdata/mn002/2019/or_defensie_terreinen_objecten/nld@2019-11-01/!groepid=12345`
```

Het is voldoende om de naam van het GIO-deel tekstueel te verbinden (dus in de buurt plaatsen) met de label-met-IntIoRef voor het GIO. Er hoeft in de bijlage geen aparte naam en JOIN-ID opgenomen te worden voor een GIO-deel.


### Voorbeeldlinks naar GIO in bijlage en GIO-deel

Tekst in het lichaam van de regeling:

```xml
<Artikel><Kop>Artikel 42</Kop> 
<Lichaam><Al>In de buurt van 
  <IntIoRef ref="mn002_1_cmp_III_…_ref_1">militaire gebieden</IntIoRef>
    aangeduid met 
  <ExtRef ref="/join/id/regdata/mn002/2019/or_defensie_terreinen_objecten/nld@2019-11-01/!groepid=12345"
          soort="JOIN">luchthavens</ExtRef>
    is het vliegen met drones verboden.
</Lichaam></Artikel>
```

### Voorbeeld directe link in bijlage naar GIO 

```xml
<Bijlage><Kop>Bijlage III</Kop> 
<Begrippenlijst>
 <Begrip>
  <Term>Militaire gebieden:</Term>
  <Definitie><ExtIoRef
   ref="/join/id/regdata/mn002/2019/or_defensie_terreinen_objecten/nld@2019-11-01" 
   wId="mn002_1_cmp_III_…_ref_1"
   eId="cmp_III_…_ref_1">
    /join/id/regdata/mn002/2019/or_defensie_terreinen_objecten/nld@2019-11-01</ExtIoRef>
 </Defintie><Begrip></Begrippenlijst></Bijlage>  
```

### Link naar GIO bij ander besluit

Men kan ook verwijzen naar een GIO dat voor een andere regeling is gemaakt. Dit kan op twee manieren, waarbij de keuze afhangt van het beoogde effect van de verwijzing: 

1. **Dynamisch**: met de JOIN-ID van het *work* van een GIO. Men verwijst naar de ‘actuele’ versie van het GIO. Wordt deze andere GIO gewijzigd, dan wijzigt juridisch het werkingsgebied van de regel waarin naar het GIO wordt verwezen. De regel zelf hoeft dan niet meer te worden aangepast. 

2. **Statisch**: met de JOIN-ID van een specifieke *expression* (versie) van een GIO. Men verwijst naar een specifieke versie op een bepaald moment in de tijd. Hierbij verandert het werkingsgebied van de regel waarin naar het GIO wordt verwezen niet automatisch als er een nieuwe versie van het GIO wordt bekendgemaakt.
