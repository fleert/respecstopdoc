# Klassiek tekstmodel

Om een [RegelingKlassiek](#RegelingKlassiek) in te stellen of te wijzigen moet een [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek) gebruikt worden in plaats van `BesluitCompact` van het [standaard tekstmodel](besluit_compact.md). Een BesluitKlassiek wordt toegepast als de beschrijving van het besluit van het bevoegd gezag zelf ook als een STOP-regeling is op te vatten die geconsolideerd kan worden. Of een BesluitKlassiek ook daadwerkelijk geconsolideerd wordt hangt van de inhoud af. De twee klassieke STOP-modellen zijn gebaseerd op de [Aanwijzingen voor de Regelgeving](https://wetten.overheid.nl/BWBR0005730).

## Structuur

Een `BesluitKlassiek` is gemodelleerd als een hamburger: boven en onder staan onderdelen die specifiek zijn voor de vastlegging van het besluit en die alleen in het besluitvormingsproces een rol spelen, en in het midden is een `RegelingKlassiek` opgenomen die de basis voor de consolidatie is. 

![Structuur van een BesluitKlassiek](./img/BesluitKlassiek-Structuur.png)

De blauwgekleurde onderdelen zijn gerelateerd aan het besluit en veranderen na bekendmaking van het besluit in principe niet meer:

* [RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) van `BesluitKlassiek`
* [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef) van `RegelingKlassiek`
* [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting) van `RegelingKlassiek`
* [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) van `BesluitKlassiek`

Het roodgekleurde `RegelingOpschrift` van de `RegelingKlassiek` wordt niet getoond bij de publicatie van het besluit maar wel bij het beschikbaar stellen van de geconsolideerde regeling.

Het lichaam bevat zowel artikelen met zelfstandige bepalingen, wijzigingen van andere regelingen als inwerkingtredingsbepalingen.

## BesluitKlassiek voor een nieuwe STOP-regeling

Als het `BesluitKlassiek` gemaakt wordt voor de vaststelling van een nieuwe STOP-regeling, dan wordt voorafgaand aan het opstellen van het besluit de regeling als een [RegelingKlassiek](regeling-klassiek.md) geschreven. Het wordt [aangeraden](besluit_hoeveel.md) in deze regeling alleen de voorschriften van de nieuwe regeling op te nemen. Als toch tegelijk een andere STOP-regeling gewijzigd moeten worden kan er al een [WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel) voor opgenomen worden, als een container voor de beschrijving van de wijziging, dus nog zonder `RegelingMutatie`. Ook kunnen er al (voorlopige) inwerkingtredingsbepalingen zijn opgenomen.

![BesluitKlassiek voor STOP-regeling](./img/BesluitKlassiek-Voor-Regeling.png)

De `RegelingKlassiek` wordt als basis voor het `BesluitKlassiek` gebruikt. De WijzigArtikelen worden aangevuld met de beschrijving van de wijzigingen in andere STOP-regeling(en), de [InwerkingtredingArtikelen](tekst_xsd_Element_tekst_InwerkingtredingArtikel.dita#InwerkingtredingArtikel) worden toegevoegd of gefinaliseerd, de `Sluiting` toegevoegd. In de module [ConsolidatieInformatie](consolidatie-informatie.md) verwijst de `eId` van de [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) voor deze versie naar het lichaam van de `RegelingKlassiek` als onderdeel van het besluit.

Nadat het besluit is vastgesteld en bekendgemaakt is de aangevulde versie van de `RegelingKlassiek` de basisversie voor verdere wijzigingen. In het algemeen wordt alleen het tekstgedeelte met de artikelen en de bijlagen gewijzigd in volgende besluiten. De inhoud van een WijzigArtikel wordt niet meer gewijzigd; als de wijziging van de regeling in het WijzigArtikel aangepast moet worden, dan wordt dat in STOP geformuleerd als een wijziging op die regeling en niet als een wijziging van het WijzigArtikel. Bij weergave van de RegelingKlassiek kunnen de RegelingMutaties daarom weggelaten worden. Het is hoogst ongebruikelijk dat de `Aanhef`, het `RegelingOpschrift` of een `InwerkingtredingArtikel` wordt gewijzigd; de `Sluiting` wordt nooit meer gewijzigd. Deze onderdelen kunnen nog wel gewijzigd worden als het besluit [gerectificeerd](besluitproces_rectificatie.md) moet worden.

[Aanwijzing 4.4](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=4paragraaf=4.1&aanwijzing=4.4) stelt dat het [RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) van het `BesluitKlassiek` (wat in de aanwijzing "het opschrift van de *regeling* wordt genoemd) aan het eind de citeertitel van de regeling tussen haakjes vermeldt. Volgens [de modellenformulering van Aanwijzing 4.2](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=4paragraaf=4.1&aanwijzing=4.2) bevat het opschrift altijd de datum waarop het besluit is ondertekend. Bijvoorbeeld:

* [Wet van 23 maart 2016, houdende regels over het beschermen en benutten van de fysieke leefomgeving (Omgevingswet)](https://zoek.officielebekendmakingen.nl/stb-2016-156.html)
* [Wet van 22 mei 1997, houdende nieuwe regels omtrent de economische mededinging (Mededingingswet)](https://zoek.officielebekendmakingen.nl/stb-1997-242.html)

## BesluitKlassiek voor wijziging/inwerkingtreding van STOP-regelingen

Als een `BesluitKlassiek` wordt opgesteld om een of meer STOP-regelingen te wijzigen, dan wordt speciaal voor het besluit een `RegelingKlassiek` gemaakt.

![BesluitKlassiek voor wijziging STOP-regeling(en)](./img/BesluitKlassiek-Voor-Wijziging.png)

De `RegelingKlassiek` heeft geen onderdelen die na bekendmaking gewijzigd kunnen worden (afgezien van rectificaties) vandaar dat de geconsolideerde versie van de regeling niet beschikbaar wordt gesteld. Het is daarom niet nodig de `RegelingKlassiek` een `RegelingOpschrift` te geven en er is ook geen [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling)-element nodig in de consolidatie-informatie. Overgangsrecht en andere geldigheidsbepalingen voor STOP-regelingen zijn niet toegestaan in de `RegelingKlassiek`.

[Aanwijzing 4.2](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=4paragraaf=4.1&aanwijzing=4.2) stelt dat het [RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) van het `BesluitKlassiek` de datum bevat waarop het besluit is ondertekend. Ook [Aanwijzing 4.3](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=4&paragraaf=4.1&aanwijzing=4.3&z=2018-01-01&g=2018-01-01) is van toepassing. Bijvoorbeeld:

* [Wet van 28 juni 2007, houdende wijziging van de Mededingingswet als gevolg van de evaluatie van die wet](https://zoek.officielebekendmakingen.nl/stb-2007-284.html)
* [Wet van 10 december 2009 tot wijziging van de Mediawet 2008 en de Tabakswet ter implementatie van de richtlijn Audiovisuele mediadiensten](https://zoek.officielebekendmakingen.nl/stb-2009-552.html)

## Wijzigingen van STOP regelingen
Een wijziging van een STOP regeling moet in renvooi (dus als [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie)) in een `WijzigArtikel` worden opgenomen. Daarbij geldt de conventie:

1. **Wijzigartikelen zijn (meestal) Romeins genummerd**: Als een wijzigingsregeling alleen wijzigartikelen bevat, of een nieuwe regeling ook wijzigArtikelen bevat die een of meer andere regelingen wijzigt, dan worden deze wijzigartikelen volgens [Aanwijzing 6.6](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=6&paragraaf=6.1&aanwijzing=6.6) doorlopend met Romeinse cijfers (I, II, III, IV, ...) genummerd. Dit is geen "harde" regel, er zijn voorbeelden van met Romeinse cijfers genummerde zelfstandige bepalingen. Ook mag er in het geval van uitgebreide wijzigingen een andere nummermethode van artikelen en wijzigartikelen gekozen worden.
2. **Eén wijzigartikel bevat de wijzigingen voor één regeling**: Volgens [Aanwijzing 6.15](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=6&paragraaf=6.2&aanwijzing=6.15) beschrijft één wijzigartikel de wijzigingen van één regeling. Deze werkwijze past goed bij het proces dat STOP voorstaat.
3. **Een `WijzigArtikel` is opgesplitst in `WijzigLid`-elementen bij meer dan één set wijzigingen op dezelfde regeling waarvan elke set een andere inwerkingtredingsdatum wordt gegeven**: Wijzigingen aan dezelfde regeling met verschillende inwerkingtredingsdatums worden opgenomen in één wijzigartikel met per inwerkingtredingsdatum één [`WijzigLid`](tekst_xsd_Element_tekst_WijzigLid.dita#WijzigLid). Elk van de wijzigleden bevat een `RegelingMutatie`. Per wijziglid met eigen inwerkingtreding moet de regeling ook een aparte [inwerkingtredingsbepaling](proces_iwt.md) bevatten. In de consolidatie-informatie van het `BesluitKlassiek` dat de `RegelingKlassiek` bekend maakt moeten alle inwerkingtredingsbepalingen een corresponderend [doel](begrippenlijst_doel.dita#doel) hebben. De inwerkingtredingsdatum van de wijzigleden kan met aparte besluiten bekend gemaakt worden.
4. **Een `RegelingMutatie` (in een `WijzigArtikel` of `WijzigLid`) bevat alle wijzigingen voor een regelingversie**. Dit is een verschil met de vrijheid die er nu bestaat om wijzigingen die tegelijk in werking zullen treden in verschillende WijzigLeden onder te kunnen brengen. Het is wel in lijn met de Aanwijzingen; deze vormvereiste wordt door STOP afgedwongen.
5. **Wijzigingen worden beschreven in renvooi; elk genummerd onderdeel van een wijzigartikel geeft de wijziging van één te wijzigen artikel.** [Aanwijzing 6.15](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=6&paragraaf=6.2&aanwijzing=6.15) schrijft voor dat de opeenvolgende wijzigingen van de artikelen worden aangeduid met hoofdletters (A, B, C, ... Z, AA, BB ...). Deze alfabetisch genummerde onderdelen binnen een RegelingMutatie worden in STOP **mutatie-operatoren** genoemd: [`Vervang`](tekst_xsd_Element_tekst_Vervang.dita#Vervang), [`VoegToe`](tekst_xsd_Element_tekst_VoegToe.dita#VoegToe) en [`Verwijder`](tekst_xsd_Element_tekst_Vervang.dita#Vervang). Deze operatoren beschrijven de wijzigingen aan de verschillende artikelen van de RegelingKlassiek die is genoemd in de `@was` van de `RegelingMutatie`.
6. **Wijzigartikelen en wijzigleden worden bij voorkeur op volgorde van beoogde inwerkingtreding opgenomen in de wijzigingsregeling**: [Aanwijzing 6.5](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=6&paragraaf=6.1&aanwijzing=6.5) geeft veel vrijheid in de volgorde waarin de wijzigartikelen in de wijzigingsregeling worden opgenomen. De voorkeursvolgorde voor STOP is het opnemen van de wijzigingen in volgorde van beoogde inwerkingtredingsdatum.
7. **OpmerkingVersie**: als de `RegelingKlassiek` geconsolideerd moet worden, dan moet voor het `WijzigArtikel`/`WijzigLid` naast de `RegelingMutatie` ook [OpmerkingVersie](tekst_xsd_Element_tekst_OpmerkingVersie.dita#OpmerkingVersie) ingevuld worden met een tekst als _Dit artikel wijzigt de regeling ..._. Deze tekst wordt getoond in de geconsolideerde regeling of basisversie van de regeling.

## Wijzigingen en overgangsrecht van non-STOP regelingen
Wijzigingen van andere dan STOP-regelingen kunnen niet in renvooi worden beschreven. Hiervoor moeten wijziginstructies worden geschreven. In plaats van een `RegelingMutatie` moet een [WijzigInstructies](tekst_xsd_Element_tekst_WijzigInstructies.dita#WijzigInstructies) element gebruikt worden. De conventies voor plaatsing in een `WijzigArtikel` of `WijzigLid` en het invullen van de `OpmerkingVersie` zijn gelijk aan die voor een `RegelingMutatie` van een STOP-regeling.

Overgangsrecht en geldigheidsbepalingen voor non-STOP regelingen mogen in de `RegelingKlassiek` worden opgenomen in [Artikelen](tekst_xsd_Element_tekst_Artikel.dita#Artikel). In dat geval moet de `RegelingKlassiek` wel geconsolideerd worden, en moeten alle artikelen tegelijk in werking treden (voor hetzelfde doel). Er zijn twee situaties:

* Als de `RegelingKlassiek` is opgesteld als eerste versie van een STOP-regeling volgens een specifiek toepassingsprofiel, dan wordt de `RegelingKlassiek` al geconsolideerd.
* Als de `RegelingKlassiek` speciaal gemaakt is voor het wijzigingsbesluit, dan moet het `RegelingOpschrift` van de `RegelingKlassiek` gelijk zijn aan het `RegelingOpschrift` van het `BesluitKlassiek`. De `RegelingKlassiek` is technisch een STOP-regeling en kan via STOP gewijzigd worden. In de praktijk zal de regeling niet meer aangepast worden, en zal het als geconsolideerde regeling beschikbaar gesteld worden tot het na verloop van tijd als materieel uitgewerkt wordt aangemerkt.

De consolidatie van de regeling die via wijziginstructies wordt gewijzigd kan niet via STOP verlopen. De wijziginstructies moeten in een handmatig proces verwerkt worden.

## Consolideren van BesluitKlassiek / (re)constructie van de basisversie

In lijn met het uitgangspunt van het [automatiseerbaar creatieproces](automatiseerbaar-creatieproces.md) is aan het `BesluitKlassiek` te zien of de daarin besloten `RegelingKlassiek` de eerste regelingversie is van een STOP-regeling: Dat is het geval als in de `RegelingKlassiek` een `Artikel` voorkomt. Een `RegelingKlassiek` zonder `Artikel` maar met een of meer `WijzigArtikel` en/of `InwerkingtredingArtikel` elementen is een wijzigingsbesluit zonder overgangsrecht en hoeft niet geconsolideerd te worden. In dat geval hoeft de wijzigingsregeling ook niet als [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) in de consolidatie-informatie opgenomen te worden.

In de geconsolideerde regeling worden de `RegelingMutatie` en `WijzigInstructies` niet getoond. In plaats daarvan wordt de `OpmerkingVersie` getoond. Een `WijzigArtikel` of `WijzigLid` ziet er daarom verschillend uit voor, tijdens en na het besluitvormingsproces.

### Voorafgaand aan opname in BesluitKlassiek
Bij het opstellen van een `RegelingKlassiek` voor opname in een `BesluitKlassiek` is het `WijzigArtikel` of `WijzigLid` een container waar later de beschrijving van de wijziging in opgenomen wordt, bijvoorbeeld:

```
<WijzigArtikel>
    <Kop>...</Kop>
    <Wat>De regeling ... wordt gewijzigd als aangegeven:</Wat>
    <OpmerkingVersie>Wijzigt de regeling ...</OpmerkingVersie>
</WijzigArtikel>
```

Voor de te wijzigen regeling wordt een nieuwe regelingversie opgesteld (in geval van een STOP-regeling) of wordt in apart  document (bijvoorbeeld een Word document) een nieuwe versie van de tekst gemaakt.

### Opname in BesluitKlassiek
Bij opname in het BesluitKlassiek wordt de beschrijving van de wijziging toegevoegd. Voor een STOP regeling is dat een [gegenereerde](bepalen_wijzigingen_renvooi.md) `RegelingMutatie`, voor een non-STOP regeling worden handmatig `WijzigInstructies` opgesteld op basis van het Word document. Het WijzigArtikel wordt dan: 

```
<WijzigArtikel>
    <Kop>...</Kop>
    <Wat>De regeling ... wordt gewijzigd als aangegeven:</Wat>
    <OpmerkingVersie>Wijzigt de regeling ...</OpmerkingVersie>
    <RegelingMutatie componentnaam="..." was="..." wordt="...">
        ...
    </RegelingMutatie>
</WijzigArtikel>
```
of
```
<WijzigArtikel>
    <Kop>...</Kop>
    <Wat>De regeling ... wordt gewijzigd als aangegeven:</Wat>
    <OpmerkingVersie>Wijzigt de regeling ...</OpmerkingVersie>
    <WijzigInstructies>
        ...
    </WijzigInstructies>
</WijzigArtikel>
```

### In geconsolideerde regeling of basisversie
De basisversie die STOP-gebruikende software als eerste regelingversie hanteert om verdere wijzigingen op aan te brengen is de versie die ook voor de geconsolideerde regeling gebruikt wordt. De beschrijving van de wijziging is niet meer relevant, alleen de `OpmerkingVersie` wordt getoond. De `RegelingMutatie` en `WijzigInstructies` en `Wat` worden niet meer gebruikt en kunnen weggelaten worden: 

```
<WijzigArtikel>
    <Kop>...</Kop>
    <OpmerkingVersie>Wijzigt de regeling ...</OpmerkingVersie>
</WijzigArtikel>
```

## Implementatiekeuze
Bij het schrijven van de regeling kan de STOP-gebruikende software kiezen of en in hoeverre WijzigArtikelen/WijzigLeden zonder RegelingMutatie/WijzigInstructies zijn toegestaan. Het implementatiemodel van STOP vereist alleen de aanwezigheid van een kop in een `RegelingKlassiek`; validatie op volledig ingevulde WijzigArtikelen/WijzigLeden vindt alleen in de context van een `BesluitKlassiek` plaats.

In bovenstaande beschrijving zijn de `Aanhef` en `Sluiting` als onderdeel van de geconsolideerde regelingversie/basisversie opgenomen. Dit volgt de conventie die [wetten.nl](https://wetten.overheid.nl/) hanteert. De sluiting en mogelijk ook de aanhef passen niet bij een tweede of volgende regelingversie. STOP-gebruikende systemen kunnen er daarom voor kiezen de aanhef en sluiting geen onderdeel te laten zijn van de geconsolideerde regelingversie/basisversie.