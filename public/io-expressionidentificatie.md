# ExpressionIdentificatie IO

Deze module bevat de identificatie van een informatieobject (IO) en gebruikt daarbij de **J**uridische **O**bject **I**dentificatie **N**aming convention (JOIN). 

## Onderdelen

De `ExpressionIdentificatie` van een informatieobject bevat de volgende onderdelen:

| Element   | Inhoud | Verplicht? |
| ------------- | -------- | ---------- |
| [FRBRWork](data_xsd_Element_data_FRBRWork.dita#FRBRWork)  | JOIN-ID van het *work* van het IO.    | Ja    |
| [FRBRExpression](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression) | JOIN-ID van de expression van het GIO.  | Ja  |
| [soortWork](data_xsd_Element_data_soortWork.dita#soortWork) | Verplichte waarde is   `/join/id/stop/work_010` (Informatieobject). | Ja          |

## Syntax JOIN-ID

Voor een Nederlands bevoegd gezag moet de JOIN-ID van een informatieobject als volgt zijn opgebouwd:

### FRBRWork

Tussen < en > staan plaatshouders. 

```
//FRBRWork: "/join/id/" <xxxdata> "/" <overheid> "/" <datum_work> "/" <overig>
```


### FRBRExpression

Tussen < en > staan plaatshouders. [ ] geeft een optioneel deel aan.
```
//FRBRExpression: "<work>/" [ <taal> ] "@" <datum_expr> [ ";" <versie> ] [ ";" <overig> ]
```

| Plaatshouder | Inhoud | Verplicht? |
| ------------ | ------------- | ----- |
| **xxxdata** | `regdata`, `pubdata` of `infodata`. Zie [publicatie-instructie](io-publicatie-instructie.md) | Ja |
| **overheid**     | Code van het bevoegde gezag volgens één van de [waardelijsten](imop_waardelijsten.md#overheid). Het gaat hierbij om het bevoegd gezag dat uiteindelijk verantwoordelijk is voor het besluit, het is geen aanduiding van de opsteller van de informatie. | Ja |
| **datum_work**   | Datum van het ontstaan van de eerste work-versie. Een volledige datum (YYYY-MM-DD conform ISO 8601) of alleen een jaartal. | Ja |
| **taal**         | Taalcode volgens [ISO 639-2 alpha-3](https://www.iso.org/iso-639-language-codes.html). In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nld` ondersteund. | Nee |
| **datum_expr**   | Datum van het ontstaan van deze expressie van het IO. Volledige datum (YYYY-MM-DD conform ISO 8601) of een jaartal. De datum moet gelijk zijn of later liggen dan `datum_work` | Ja |
| **versie**       | [Versienummer](data_xsd_Element_data_versienummer.dita#versienummer) voor het IO | Nee |
| **overig**       | Overige kenmerken om de Expression of het Work te onderscheiden, zoals een dossiernummer of een afkorting gebaseerd op de naam. Kenmerken moeten compact zijn. Dus geen lange titels. Bestaat uit een combinatie van cijfers, boven- en onderkast-letters, "_" en "-", begint met een cijfer of letter (regex `[a-zA-Z0-9][a-zA-Z0-9_-]*`) en heeft maximaal 128 karakters. Het BG moet zorgen dat de waarde uniek is. Er is geen centrale service om de waarde voor "overig" te genereren. | Ja, bij een work. Nee, bij een expressie. |

Voorbeelden zijn:

* `/join/id/regdata/gm0439/2019/gio993859238`: Een te consolideren informatieobject (Work) van Gemeente Purmerend uit 2019 met als kenmerk gio993859238.
* `/join/id/regdata/gm0439/2019/gio993859238/nld@2019-12-20;1`: de expression van 20 december 2019 van dat informatieobject.

## Zie ook

* [Voorbeeld ExpressionIdentificatie - GIO](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/01-BG-Vuurwerkverbodsgebied-v1-Identificatie.xml).
* [Naamgevingsconventie](naamgevingsconventie.md).
