# Tabellen

Het model voor tabellen in STOP is gebaseerd op de [CALS standaard](https://www.oasis-open.org/specs/tm9901.htm) waarvan een groot deel van de element- en attribuut-namen overgenomen zijn. Niet alle mogelijkheden die beschreven worden in de CALS standaard zijn overgenomen in STOP. 

Ruwweg kan gesteld worden dat de elementen van een tabel de vorm specificeren en dat de attributen specificeren hoe de tabel weergegeven moet worden. Alleen de inhoud en de indeling van een tabel is van juridische betekenis, de vorm (belijning etc.) niet. 

Een voorbeeld van een eenvoudige [`table`](tekst_xsd_Element_tekst_table.dita#table): 

*XML voorbeeld tabel*

```xml
<table colsep="0"
       eId="table_o_1"
       frame="none"
       pgwide="0"
       rowsep="0"
       wId="table_o_1">
  <title>Voorbeeld</title>
  <tgroup align="left"
           cols="2">
    <colspec colname="col1"
              colnum="1"
              colwidth="41*"/>
    <colspec colname="col2"
              colnum="2"
              colwidth="490*"/>
    <thead valign="top">
      <row>
        <entry align="center"
                colname="col1">
          <Al>Eerste</Al>
        </entry>
        <entry align="center"
                colname="col2">
          <Al>Tweede</Al>
        </entry>
      </row>
    </thead>
    <tbody valign="top">
      <row rowsep="0">
        <entry align="right"
                colname="col1"
                colsep="0"
                valign="top">
          <Al>1.0000</Al>
        </entry>
        <entry align="left"
                colname="col2"
                colsep="0"
                valign="top">
          <Al>Hoeveelheid</Al>
        </entry>
      </row>
    </tbody>
  </tgroup>
  <Bron>STOP-documentatie</Bron>
</table>
```

*Voorbeeld weergave:*

> ![](img/VoorbeeldTabel.png)

Beperkingen:

- Een tabel van 1 cel is niet toegestaan. In plaats daarvan moet [`Kadertekst`](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst) worden gebruikt. Een tabel moet altijd minimaal 2 kolommen en 2 rijen hebben.
- Tabellen mogen niet worden gebruikt binnen een Aanhef (`Aanhef`, `Considerans`, `Afkondiging`), `Rectificatietekst`, `Definitie` en `Noot`

## Tabel-elementen

Het root-element van een tabel is [`table`](tekst_xsd_Element_tekst_table.dita#table). De tabel kan een titel([`title`](tekst_xsd_Element_tekst_title.dita#title)) en een bron([`Bron`](tekst_xsd_Element_tekst_Bron.dita#Bron)) bevatten die respectievelijk boven en onder de tabel getoond worden. De tabel zelf wordt samengesteld door het specificeren van één of meer groepen van rijen([`tgroup`](tekst_xsd_Element_tekst_tgroup.dita#tgroup)) die bestaan uit:

- voor elke kolom van de rijen in de `tgroup` een [`colspec`](tekst_xsd_Element_tekst_colspec.dita#colspec)
- optioneel een [`thead`](tekst_xsd_Element_tekst_thead.dita#thead) met één of meer koprijen([`row`](tekst_xsd_Element_tekst_row.dita#row))
- en een [`tbody`](tekst_xsd_Element_tekst_tbody.dita#tbody) met één of meer tabelrijen([`row`](tekst_xsd_Element_tekst_row.dita#row))

Elke `row` bevat weer één of meer cellen ([`entry`](tekst_xsd_Element_tekst_entry.dita#entry)). Een cel kan meerdere kolommen of meerdere rijen overspannen.

| ![](img/tabel-elementen.png) |
| :--------------------------: |
|   Elementen van een tabel    |

De [`title`](tekst_xsd_Element_tekst_title.dita#title) bevat koptekst en [`Bron`](tekst_xsd_Element_tekst_Bron.dita#Bron) broodtekst. Verder bevat het element [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) zelf weer bloktekst, met één of meer keer een `Al`inea (met broodtekst), `Figuur`, `Formule`, `Groep`, `Lijst` of `Tussenkop`.

## Tabel attributen

De weergave van de tabel wordt bepaald door de attributen van elk element, waarbij soms hetzelfde attribuut op meerdere elementen voor kan komen. Hierbij geldt dat een attribuutwaarde op een lager element de waarde van hetzelfde attribuut op een hoger element overschrijft. Bijvoorbeeld: als het `table`-attribuut `colsep` geen verticale binnenlijnen specificeert en de `colsep` van een `row` wel, dan wordt alleen betreffende rij met verticale binnenlijnen getoond.

| attribuut | betekenis                                                    | zie documentatie van de attributen van                       |
| --------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| frame     | buitenlijnen van de tabel: top/bottom/topbot/all(standaard)/sides/none | [`table`](tekst_xsd_Attribute_Group_tekst_agTable.dita#agTable)                                 |
| pgwide    | tabel tonen binnen de zetspiegel '0' (standaard), tabel beslaat hele pagina (bladspiegel) '1'. | [`table`](tekst_xsd_Attribute_Group_tekst_agTable.dita#agTable)                                 |
| tabstyle  | pas een voorgedefinieerde stijl toe bij de weergave van de tabel, stijl gedefinieerd door een viewer. Officiëlebekendmakingen.nl kent zebra-, xml- en tekst tabellen. Zie toelichting in onderstaande tabel. | [`table`](tekst_xsd_Attribute_Group_tekst_agTable.dita#agTable)                                 |
| orient    | oriëntatie van de tabel: 'port' (standaard) of 'land' (90 graden tegen de klok in gekanteld, binnen de zetspiegel, ongeacht de waarde van @pgwide) | [`table`](tekst_xsd_Attribute_Group_tekst_agTable.dita#agTable)                                 |
| cols      | aantal kolommen in deze groep rijen                          | [`tgroup`](tekst_xsd_Attribute_Group_tekst_agTableGroup.dita#agTableGroup)                           |
| colnum    | positie van de kolom van links naar rechts genummerd         | [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec)                             |
| colwidth  | kolombreedte in '\*', 'mm', 'cm,' 'pi', 'pt' of 'in'. Aanbevolen wordt om de breedte in '\*' te definiëren. Met \* wordt een proportionele maat aangeduid. Een kolom met 5\* is 5x zo breed als een kolom met '*'='1\*'. | [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec)                             |
| colname   | identifier van de kolom, bijv 'kolom1'                       | [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| colsep    | verticale binnenlijn aan de rechter kant '0'=geen '1'=wel(standaard) | [`table`](tekst_xsd_Attribute_Group_tekst_agTable.dita#agTable), [`tgroup`](tekst_xsd_Attribute_Group_tekst_agTableGroup.dita#agTableGroup), [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| rowsep    | horizontale binnenlijn aan de onderkant '0'=geen '1'=wel(standaard) | [`table`](tekst_xsd_Attribute_Group_tekst_agTable.dita#agTable), [`tgroup`](tekst_xsd_Attribute_Group_tekst_agTableGroup.dita#agTableGroup), [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec), [`row`](tekst_xsd_Attribute_Group_tekst_agTableRow.dita#agTableRow), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| valign    | verticaal uitlijnen van tekst: 'top'(standaard) / 'middle' / 'bottom' | [`thead`](tekst_xsd_Attribute_Group_tekst_agTableHead.dita#agTableHead), [`tbody`](tekst_xsd_Attribute_Group_tekst_agTableBody.dita#agTableBody), [`row`](tekst_xsd_Attribute_Group_tekst_agTableRow.dita#agTableRow), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| align     | horizontaal uitlijnen van tekst: 'left'(standaard) / 'right' / 'center' / 'justify' / 'char'. *Justify en char worden niet ondersteund door Officiëlebekendmakingen.nl* | [`tgroup`](tekst_xsd_Attribute_Group_tekst_agTableGroup.dita#agTableGroup), [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| char      | teken(s) dat/die gebruikt moet worden voor uitlijning van de tekst. *Wordt niet ondersteund door Officiëlebekendmakingen.nl* | [`tgroup`](tekst_xsd_Attribute_Group_tekst_agTableGroup.dita#agTableGroup), [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| charoff   | horizontale positie uitlijning op het 'char' teken(s) binnen de kolom. Uitgedrukt in %. *Wordt niet ondersteund door Officiëlebekendmakingen.nl* | [`tgroup`](tekst_xsd_Attribute_Group_tekst_agTableGroup.dita#agTableGroup), [`colspec`](tekst_xsd_Attribute_Group_tekst_agColspec.dita#agColspec), [`entry`](tekst_xsd_Element_tekst_entry.dita#entry) |
| morerows  | aantal rijen dat deze cel verticaal moet overspannen         | [`entry`](tekst_xsd_Element_tekst_entry.dita#entry)                                   |
| namest    | identifier van de eerste kolom die deze cel horizontaal moet overspannen | [`entry`](tekst_xsd_Element_tekst_entry.dita#entry)                                   |
| nameend   | identifier van de laatste kolom die deze cel horizontaal moet overspannen | [`entry`](tekst_xsd_Element_tekst_entry.dita#entry)                                   |
| rotate    | tekst wordt 90 graden 'counter-clockwise' gedraaid weergegeven '1' of niet '0' (standaard) | [`entry`](tekst_xsd_Element_tekst_entry.dita#entry)                                   |

### @tabstyle

| tabstyle                    | Effect op Officiëlebekendmakingen.nl                         |
| --------------------------- | ------------------------------------------------------------ |
| 'zebra1' 'zebra2'  'zebra3' | Zebra1 ([voorbeeld](https://zoek.officielebekendmakingen.nl/stcrt-2021-28102.html#main_reg0001_d3198e1_cmp_XXXA)) is de standaard opmaak voor tabellen. Hierbij zijn rijen om en om grijs en wit, met een blauwe header. Een hoger nummer leidt tot een kleiner font en regelhoogte, zodat de tabel compacter wordt. |
| 'xml1' 'xml2' 'xml3'        | De XML-tabel([voorbeeld](https://zoek.officielebekendmakingen.nl/stcrt-2021-28102.html#main_chp_2__art_2.9)) wordt bijvoorbeeld gebruikt wanneer er samengevoegde cellen in de tabel voorkomen. Het om-en-om karakter van de zebratabel zou dan verloren gaan. De XML-tabel is omlijnd en heeft een witte achtergrond.  Een hoger nummer leidt tot een kleiner font en regelhoogte, zodat de tabel compacter wordt. |
| 'tekst'                     | Een teksttabel wordt gebruik voor de positionering van standaardtekst. Met standaard font settings en zonder tabelbelijning. Een tabel van 1 cel is niet toegestaan. In plaats daarvan moet [`Kadertekst`](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst) worden gebruikt. Een tabel moet altijd minimaal 2 kolommen en 2 rijen hebben. |

## Overspanningen

Het is mogelijk om zowel horizontale als verticale overspanningen aan te brengen in een tabel. Een overspanning is de verticale en/of horizontale samenvoeging van twee of meer cellen tot één cel. Voor een horizontaal overspannende cel wordt op `entry` de attributen `namest` en `nameend` gebruikt. `namest` bevat dan de `colname` van de eerste kolom en `nameend` de `colname` van de laatste kolom. Voor een verticale overspanning wordt `morerows` gebruikt. Met de waarde 1 wordt de cel over 2 rijen getoond. Zie onderstaand voorbeeld.

![](img/TabelOverspanningen.png)

```
<table eId="table_o_1" wId="table_o_1" frame="all">
    <title>Voorbeeld overspanning</title>
    <tgroup cols="5"
             align="left"
             colsep="1"
             rowsep="1">
        <colspec colname="c1"/>
        <colspec colname="c2"/>
        <colspec colname="c3"/>
        <colspec colname="c4"/>
        <colspec colnum="5"
                  colname="c5"/>
        <thead valign="middle">
            <row>
                <entry namest="c1"
                        nameend="c2"
                        align="center"><Al>Horizontaal</Al></entry>
                <entry><Al>a3</Al></entry>
                <entry><Al>a4</Al></entry>
                <entry><Al>a5</Al></entry>
            </row>
        </thead>
        <tbody valign="middle">
            <row>
                <entry><Al>b1</Al></entry>
                <entry><Al>b2</Al></entry>
                <entry><Al>b3</Al></entry>
                <entry><Al>b4</Al></entry>
                <entry morerows="1"
                        valign="middle"><Al>Verticaal</Al></entry>
            </row>
            <row>
                <entry><Al>c1</Al></entry>
                <entry namest="c2"
                        nameend="c3"
                        align="center"
                        morerows="1"
                        valign="bottom"><Al>Beide</Al></entry>
                <entry><Al>c4</Al></entry>
            </row>
            <row>
                <entry><Al>d1</Al></entry>
                <entry><Al>d4</Al></entry>
                <entry><Al>d5</Al></entry>
            </row>
        </tbody>
    </tgroup>
    <Bron>Bron: <ExtRef ref="https://tdg.docbook.org/tdg/5.0/cals.table.html"
                         soort="URL">docbook</ExtRef></Bron>
</table>
```

<!--**TODO**

Voorstel documentatie:

Het is prima om naar CALS te verwijzen, maar die documentatie is niet echt toegankelijk dus een STOP-gebruiker kan daar weinig mee. Daarnaast moeten we uitleggen wat het effect van de verschillende instellingen is in de context van STOP.

* Uitleg eenvoudige tabel
* Uitleg van overspannen meerdere kolommen.
* Verwijzing nar CALS mag, maar dan graag naar het relevante deel een aangewen welke STOP elementen a la CALS werken.
* Welke attributen zijn juridisch (dwz wijzigen via besluit)
* Wat is pgwide en hoe toe te passen in website omgeving?
* Wat is orient en hoe toe te passen in website omgeving? Wanneer overstappen van portret (is dat de default?) naar landscape.
* Wat is tabstyle? Welke opties ondersteunt STOP (of OEP en dus STOP)?
* Wat zijn de verdere opmaakopties?

Ook eens door de schemadocumentatie lopen. Daar staan op de meest obscure plaatsen nuttige stukjes documentatie die niemand daar ooit terugvindt die ernaar op zoek is. Bijv dat Kadertekst gebruikt moet worden in plaats van een tabel te gebruiken met 1 rij en 1 kolom.

-->