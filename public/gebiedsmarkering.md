# Gebiedsmarkering

De [Regeling elektronische publicaties](https://wetten.overheid.nl/BWBR0045086/2022-01-01#Bijlage1) vereist dat publicaties over bepaalde besluiten worden voorzien van een gebiedsmarkering. De gebiedsmarkering wordt gebruikt om omwonenden en andere geïnteresseerden te attenderen op de publicatie. Met de gebiedsmarkering wordt het gebied aangeduid waar het besluit betrekking op heeft. 



## Attenderen op gebiedsmarkering 

Geïnteresseerden kunnen op [overheid.nl/berichten-over-uw-buurt](https://www.overheid.nl/berichten-over-uw-buurt) aan de hand van criteria instellen op welke publicaties zij geattendeerd willen worden. Als een gebiedsmarkering van een publicatie aan de opgegeven criteria voldoent, wordt de geïnteresseerde op die publicatie geattendeerd.

Een gebiedsmarkering behorend bij een publicatie kan uitgedrukt worden als één van de volgende opties:

1. het volledige ambtsgebied van het bevoegd gezag, of
2. een geometrische begrenzing van een gebied. 

**1. Ambtsgebied als gebiedsmarkering**

Indien een besluit betrekking heeft op (of geldt voor) het volledige ambtsgebied, dan wordt de gebiedsmarkering aangeduid door middel van de [BG-code](geo_xsd_Complex_Type_geo_AmbtsgebiedType.dita#AmbtsgebiedType_bevoegdGezag). 

**LET OP**: Het gebruik van de BG-code is verplicht in deze situatie. Het is *NIET* de bedoeling om in dit geval de geometrie van het ambtsgebied aan te leveren. 

Op besluiten van de rijksoverheid die algemeen geldend zijn, wordt niet geattendeerd. 

**2. Geometrie als gebiedsmarkering**

Indien een besluit betrekking heeft op een specifiek gebied (zoals een adres, perceel, straat, buurt, woonwijk, bedrijventerrein, etc. etc.) dan wordt dat aangegeven door middel van de geometrische afbakening: een of meer punten, lijnen of vlakken.”

## Modellering in STOP

De gebiedsmarkering is in STOP [gemodelleerd](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied) als een zelfstandige [module](imop_modules.md) die voor een publicatie van een besluit, kennisgeving of rectificatie meegeleverd kan worden. Zie ook het [informatiemodel](EA_E8EDA73EF3B842a9AB27E3B3E23B1470.dita#Pkg).

De gebiedsmarkering bestaat dus uit òf een [Ambtsgebied](geo_xsd_Element_geo_Ambtsgebied.dita#Ambtsgebied) òf één of meerdere [Gebied](geo_xsd_Complex_Type_geo_GebiedType.dita#GebiedType)-en. Het Ambtsgebied bevat een [BG-code](geo_xsd_Complex_Type_geo_AmbtsgebiedType.dita#AmbtsgebiedType_bevoegdGezag). De gebieden hebben optioneel een [label](geo_xsd_Complex_Type_geo_GebiedType.dita#GebiedType_label) en verplicht een [geometrie](geo_xsd_Complex_Type_geo_GebiedType.dita#GebiedType_geometrie). Voor de codering van de geometrie wordt, net als in het GIO, gebruik gemaakt van de [basisgeometrie-standaard](https://docs.geostandaarden.nl/nen3610/basisgeometrie/) van Geonovum. 

**Beperkingen**

Uit compatibiliteitoverwegingen zijn geen cirkels of bogen [toegestaan](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3040) in de geometrie. Bovendien [moeten](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3050) coördinaten uitgedrukt worden in het [Rijksdriekhoekstelsel](https://nl.wikipedia.org/wiki/Rijksdriehoeksco%C3%B6rdinaten).


**Voorbeeld**

* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/08%20Gebiedsmarkering) van gebiedsmarkeringen bij een besluit.

**Gerelateerd**

[Effectgebied](effectgebied.md) van een besluit.