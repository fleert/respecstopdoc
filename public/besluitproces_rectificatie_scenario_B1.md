# B1. Rectificatie Regelingmutatie RegelingCompact

In dit scenario wordt een `RegelingMutatie` gerectificeerd. De wijzigingen in de rectificatie zijn beschreven alsof het gerectificeerde besluit is geconsolideerd in een regelingversie. De zinssnedes die de locatie van de rectificatie in het besluit aangeven zijn als vrije tekst gecodeerd in `<Rectificatietekst>`; De gerectificeerde wijzigartikelen uit het besluit worden aangeduid als "onderdeel".

## Gecorrigeerde regeling

Het BG begint met het opstellen van de gecorrigeerde regeling of regelingen als het besluit meerdere regelingen betrof. Per STOP-module van de regeling wordt hieronder aangegeven of, en zo ja, wat er aangepast moet worden:

- [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): Een rectificatie leidt tot een nieuwe versie van de regeling en dus een nieuwe identificatiemodule met een aangepaste FRBRExpression.
- [RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata): Indien er correcties nodig zijn in bijv. de officiele titel, citeertitel etc., moet gecorrigeerde RegelingMetadata aangeleverd worden. Omdat de RegelingMetadata een annotatie is, hoeft de rectificatie geen nieuwe RegelingMetadata te bevatten als er hier geen correcties zijn. 
- [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata): Het versienummer van de gecorrigeerde regeling moet altijd meegegeven worden met de rectificatie.

- Regelingtekst: [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact), [RegelingVrijeTekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) of [RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel). De correctie(s) worden aangebracht in de regelingtekst. Aan de hand van de originele en gecorrigeerde regelingtekst worden de correctie(s) in [renvooi](bepalen_wijzigingen_renvooi.md) opgesteld, zodat deze straks in een [regelingmutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) opgenomen kunnen worden in de rectificatie.

- [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties): Indien er correcties nodig zijn in de Toelichtingsrelaties moet een gecorrigeerde Toelichtingsrelatie-module aangeleverd worden. Omdat de Toelichtingsrelaties annotaties zijn, hoeft de rectificatie geen nieuwe Toelichtingsrelatie-module te bevatten als er geen correcties zijn.



## Gecorrigeerde besluitmodules

De correctie(s) hebben gevolgen voor de met het besluit meegeleverde informatie. Om het besluit te corrigeren is nodig:

* [ProcedureverloopMutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie): Als de rectificatie alleen een correctie in de regeling betreft, bevat de rectificatie geen aanpassingen voor het procedureverloop.

* [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie): De rectificatie voegt een nieuwe regelingversie toe aan het [doel](doel.md) van het besluit. De consolidatieinformatie moet dus worden uitgebreid. De consolidatieInformatie moet een  [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) bevatten met 
  * hetzelfde doel als het besluit, 
  * de instrumentVersie van de gecorrigeerde regeling,

  * een eId verwijzing naar de voegtoe/vervang in de regelingmutatie uit de tekst van de rectifcatie.

    Zie voor een [voorbeeld de consolidatieinformatie](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03%20Rectificatie%20besluit/02-BG-Rectificatie-Besluit-Consolidatie-informatie.xml) uit het Pyrodam-rectificatievoorbeeld.

* [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata): Als de rectificatie alleen correcties in de regelingmutatie(s) betreft, bevat de rectificatie geen nieuwe besluitmetadata. Ook de BesluitMetadata is een annotatie en de informatie van de besluitmetadata van voorgaande versies blijft van toepassing zolang er geen nieuwe versie wordt aangeleverd. 


## De rectificatie zelf

De rectificatie zelf bestaat uit:

* [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van de rectificatie zelf. De rectificatie van een besluit is een zelfstandig Work en elke rectificatie bij hetzelfde besluit is een nieuwe expressie van het rectificatie Work;

* [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata): Net als bij een besluit moet er bij een rectificatie altijd metadata aangeleverd worden. De RectificatieMetadata lijkt erg op de BesluitMetadata, maar heeft een extra veld [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert);

* [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie): De rectificatie bestaat uit:
  * Een [regeling opschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift), bijv. 'Rectificatie van de Verordening Toeristenbelasting 2017'

  * Het [lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met één of meerdere:
    * [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)(en) met 
      * een [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop);
      * een of meer toelichtende [alinea](tekst_xsd_Element_tekst_Al.dita#Al)'s;_
      * de [regelingmutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie).
      
        De regelingmutatie is identiek aan een 'normale' regelingmutatie in [renvooi](bepalen_wijzigingen_renvooi.md). De *was* van de mutatie is de regelingversie die ontstaan is door het besluit. De *wordt* is de regelingversie die ontstaat uit de rectificatie. LET OP: er worden voor de rectificatie wel afwijkende standaard zinnen gebruikt om de [*wat*](tekst_xsd_Element_tekst_Wat.dita#Wat) aan te duiden. En er is precies één regelingmutatie gecorrigeerde regeling.


[Het voorbeeld van Pyrodam](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03%20Rectificatie%20besluit/index.md) bevat een rectificatie van een `RegelingMutatie`.