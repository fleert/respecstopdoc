# Gedeeltelijke vernietiging

Als een beroepsorgaan het beroep gegrond heeft verklaard en het besluit daarbij **gedeeltelijk** heeft **vernietigd**, moeten door het BG de volgende extra processtappen worden verricht:


1.  Het stelt een mededeling op om de gedeeltelijke vernietiging bekend te maken, conform [art. 8 lid 80 Awb ](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=8&titeldeel=8.2&afdeling=8.2.6&artikel=8:80&z=2021-10-01&g=2021-10-01).
2. Bij deze mededeling voegt het BG, als het een definitieve uitspraak betreft, een procedureverloopmutatie met de procedurestap "Beroep(en) definitief afgedaan".
3. Als (delen van) de uitspraak gelden voor het algemene publiek, verwerkt het de uitspraak in aangepaste geconsolideerde regeling- en IO-versies waarbij ook consolidatie-informatie wordt bijgevoegd om de aangepaste versies als de huidige geconsolideerde versies aan te merken. De aanpassingen in de geconsolideerde regeling en IO-versies worden meegeleverd met de mededeling.

## 1. Maak een mededeling gedeeltelijke vernietiging
Het BG maakt een mededeling met de volgende modules:

1. De modules zoals omschreven in [stappen](scenario-vernietiging-zonder-rechtsgevolgen.md#1.-Opstellen-mededeling-vernietiging) 1.1 t/m 1.3 van het scenario van de vernietiging met behoud van rechtsgevolgen.
2. Als de uitspraak definitief is (er zijn dus geen verdere beroepsmogelijkheden meer), levert het BG ook direct een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) met [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap) met [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) met de waarde `/join/id/stop/procedure/stap_021` (beroep(en) definitief afgedaan). Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).

## 2. Maak aangepaste regeling en IO-versies

Het BG moet naar aanleiding van de uitspraak van het beroepsorgaan nieuwe regeling- en IO-versie(s) leveren waarin de uitspraak van de rechter is verwerkt. De nieuwe geconsolideerde regeling wordt vervolgens zichtbaar vanaf de datum van de uitspraak. Als er een nieuwe IO-versie moet worden gemaakt, is het aan te bevelen om met stap 2a te beginnen. Als er alleen een regeling moet worden aangepast, kan deze stap worden overgeslagen.

### 2a. Maak een aangepaste IO-versie
Voer deze stap uit als er een nieuwe IO-versie moet worden gemaakt.
Zie evt. [onderdelen geconsolideerde IO](io-onderdelen.md#Overzicht-onderdelen-geconsolideerd-IO). Verder geldt:

1. Pas de inhoud van het IO aan conform de uitspraak van de rechter. Lever dan alleen de `GeoInformatieObjectVersie`, dus zonder vaststellingscontext, want het gaat om een aanpassing van het geconsolideerde GIO waarvoor BG geen besluit heeft genomen. 
2. Wijzig de [`ExpressionIdentificatie`](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) in die van de nieuwe IO-versie. Deze heeft als [`soortWork`](data_xsd_Element_data_soortWork.dita#soortWork) `/join/id/stop/work_005` (geconsolideerd IO).
3. De `InformatieObjectVersieMetadata` moet worden aangepast.

4. Als `InformatieobjectMetadata` van het IO wijzigt, dan wordt deze toegevoegd.

### 2b. Maak een aangepaste regeling-versie 

#### 2b.I. (Alleen bij IO) Verwijs naar het aangepaste IO vanuit de nieuwe regeling-versie

Deze stap is alleen noodzakelijk als er een (of meerdere) aangepaste geconsolideerde IO-versie (GIO, PDF-document) is/zijn gemaakt. 

#### 2b.II. Wijzig de regels in de regeling-versie

1. Pas de geconsolideerde regeling aan conform de uitspraak van de rechter. Stel hiervoor de module [`Tekstrevisie`](tekst_xsd_Element_tekst_Tekstrevisie.dita#Tekstrevisie) samen, met daarin een [`RegelingMutatie`](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) met de benodigde wijzigingen in [renvooi](bepalen_wijzigingen_renvooi.md).
2. Wijzig de [`ExpressionIdentificatie`](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) in die van de nieuwe regelingversie.
3. Wijzig de [`RegelingVersieMetadata`](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata).
4. Wijzig indien nodig ook de `RegelingMetadata` en de `Toelichtingsrelatie`. Zie evt. ook [Modules voor aanpassen van de geconsolideerde regeling](imop_modules.md) in het Overzicht IMOP-modules. De aanpassing van de module `ConsolidatieInformatie` wordt beschreven in stap 2.c.

### 2c. Pas de ConsolidatieInformatie aan
Bij de aangepaste regeling- en eventuele IO-versies wordt de module [`ConsolidatieInformatie`](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) gevoegd. Er is namelijk door een beroepsorgaan een beslissing genomen, maar deze is niet door een BG vastgesteld. Er wordt slechts een mededeling van opgesteld en bekendgemaakt. Met de module `ConsolidatieInformatie` worden de nieuwe geconsolideerde regeling- en IO-versies gekoppeld aan het [doel](doel.md) van het besluit waartegen beroep was ingesteld.

#### Noot
Als na een uitspraak hoger beroep mogelijk is, kunnen stappen 1 en 2 zich herhalen door een nieuwe uitspraak van een beroepsorgaan. 

#### Voorbeeld ConsolidatieInformatie regeling

```
<ConsolidatieInformatie>
  <gemaaktOp>2020-12-15T09:03:00Z</gemaaktOp>
  <BeoogdeRegelgeving>
    <BeoogdeRegeling>
      <bekendOp>2020-12-05</bekendOp>
      <!-- koppeling van de regelingversie van <RegelingMutatie> aan het doel-->  
      <doelen><doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel></doelen>
      <!-- De regelingversie met de gewijzigde GIO-expressie is nu de juiste regelingversie voor het doel-->
      <instrumentVersie>/akn/nl/act/gm9999/2020/REG0001/nld@2020-12-12;3</instrumentVersie>
      <!-- geen eId, want er is geen besluit dat deze regelingversie instelt -->
      <gemaaktOpBasisVan>
        <Basisversie>
            <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
            <gemaaktOp>2020-06-20T09:03:00Z</gemaaktOp>
        </Basisversie>
      </gemaaktOpBasisVan>
    </BeoogdeRegeling>
    <BeoogdInformatieobject>
      <bekendOp>2020-12-05</bekendOp>
      <doelen>
        <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
      </doelen>
      <!-- n.a.v. de uitspraak van de rechter aangepaste GIO -->
      <instrumentVersie>/join/id/regdata/gm9999/2019/gio993859238/nld@2020-12-12;3</instrumentVersie>
      <!-- geen eId, want er is geen besluit dat deze regelingversie instelt -->
      <gemaaktOpBasisVan>
        <Basisversie>
            <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
            <gemaaktOp>2020-06-20T09:03:00Z</gemaaktOp>
        </Basisversie>
      </gemaaktOpBasisVan>
    </BeoogdInformatieobject>
  </BeoogdeRegelgeving>
  <!-- geen Tijdstempels, BHKV weet al wanneer het doel in werking treed -->
</ConsolidatieInformatie>
```

## 3. Verwerking onherroepelijkheid besluit
Na het einde van alle beroepsprocedures en/of het verstrijken van de hogerberoepstermijn, moet het BG het besluit de status onherroepelijk geven. BG levert dan een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) met de [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap) met daarin een [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) met de waarde `/join/id/stop/procedure/stap_021` (beroep(en) definitief afgedaan). Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).
De Procedureverloopmutatie wordt aangeleverd met een directe mutatie, of wordt meegeleverd met de mededeling uit stap 1, als er geen hogerberoepstermijn is die afgewacht hoeft te worden.

Lopen er nog andere beroepprocedures dan is er nog geen definitief einde en geeft het BG nu nog niets door.


## Zie ook
* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).
* Voor een volledig uitgewerkt scenario met voorbeelden van onderdelen, van opstelfase van het vernietigde besluit tot en met consolidatie naar aanleiding van de vernietiging, zie [Uitleg voorbeelden scenario gedeeltelijke vernietiging](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/03%20Gedeeltelijke%20vernietiging/index.md). 
* Als een beroepsorgaan uitspraak doet en een besluit (gedeeltelijk) vernietigd waarvan de regeling inmiddels is gewijzigd door andere besluiten, is er sprake van samenloop. Dat wil zeggen: de vernietiging maakt de geconsolideerde regelingversie die voortvloeide uit het vernietigde besluit (gedeeltelijk) incorrect, alsmede de geconsolideerde regelingversies van opeenvolgende besluiten die op het vernietigde besluit waren gebaseerd. Zie [oplossen samenloop](proces_samenloop.md) over hoe het BG ervoor kan zorgen dat vernietiging van het besluit wordt verwerkt in de huidige geconsolideerde regeling.



