# Vernietiging met behoud van rechtsgevolgen
Een vernietiging met behoud van rechtsgevolgen houdt in dat het beroepsorgaan de onderbouwing in het besluit onvoldoende vindt, maar dat het BG het besluit tijdens de beroepsprocedure alsnog voldoende heeft onderbouwd. Voor STOP maakt het hierbij niet uit of deze vernietiging met behoud van rechtsgevolgen geheel of gedeeltelijk is.

Als een beroepsorgaan het beroep definitief gegrond heeft verklaard en het besluit daarbij heeft **vernietigd met behoud van rechtsgevolgen**, moeten door het BG de volgende extra processtappen worden verricht:


## 1. Opstellen mededeling vernietiging
Het BG stelt een mededeling op om de vernietiging bekend te maken. De mededeling bestaat uit de volgende modules:

1. de [`ExpressionIdentificatie`](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van de mededeling met: 
   * [`FRBRWork`](data_xsd_Element_data_FRBRWork.dita#FRBRWork),
   * [`FRBRExpression`](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression),
   * [`soortWork`](data_xsd_Element_data_soortWork.dita#soortWork). Voor een mededeling is dit `/join/id/stop/work_025`.
2. de [`Mededeling`](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling)-module met de tekst van de mededeling.
3. de [`MededelingMetadata`](data_xsd_Element_data_MededelingMetadata.dita#MededelingMetadata), met daarin: 
   * de [`eindverantwoordelijke`](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke),
   * de [`maker`](data_xsd_Element_data_maker.dita#maker),
   * de [`officieleTitel`](data_xsd_Element_data_officieleTitel.dita#officieleTitel),
   * de [`onderwerpen`](data_xsd_Element_data_onderwerpen.dita#onderwerpen),
   * de [`mededelingOver`](data_xsd_Element_data_mededelingOver.dita#mededelingOver).


## 2. Verwerking onherroepelijkheid besluit
Is de vernietiging met behoud van rechtsgevolgen het einde van alle beroepsprocedures (evt. na het ongebruikt verstrijken van de hogerberoepstermijn), dan moet het BG het besluit de status onherroepelijk geven. BG levert dan een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) met de [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap) met daarin een [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) met de waarde `/join/id/stop/procedure/stap_021` (beroep(en) definitief afgedaan). Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).
De Procedureverloopmutatie wordt aangeleverd met een directe mutatie, of wordt meegeleverd met de mededeling uit stap 1, als er geen hogerberoepstermijn is die afgewacht hoeft te worden.

Lopen er nog andere beroepprocedures dan is er nog geen definitief einde en geeft het BG nu nog niets door. 

### Voorbeeld proceduremutatie beroep(en) definitief afgedaan

```
<Procedureverloopmutatie>
  <bekendOp>2020-12-05</bekendOp>
  <voegStappenToe>
    <Procedurestap>
      <!-- Beroep(en) definitief afgedaan -->
      <soortStap>/join/id/stop/procedure/stap_021</soortStap>
      <voltooidOp>2020-12-15</voltooidOp>
    </Procedurestap>
  </voegStappenToe>
</Procedureverloopmutatie>
```