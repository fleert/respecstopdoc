# Renvooitekst

Een eerste versie van de tekst van een regeling of van een tijdelijk regelingdeel wordt integraal opgenomen in het besluit. Als een wijziging van een regeling wordt vastgesteld, dan wordt gebruik gemaakt van renvooitekst. Hierbij worden in de tekst speciale markeringen aangebracht zodat de wijziging zichtbaar wordt. De beschrijving van de renvooitekst volgt het proces van maken en gebruiken ervan:

![Maken en gebruiken van renvooitekst](img/renvooitekst-principe.png)

[Doel van renvooiering](renvooitekst_principe.md)
: Het doel van het aanbrengen van renvooimarkeringen in de tekst is het zichtbaar maken van de wijzigen. Het is een omkeerbaar proces: uit de renvooimarkeringen die in het besluit opgenomen worden is de nieuwe versie te reconstrueren door de  renvooimarkeringswijzigingen aan te brengen op de vorige tekst. 

[De regelingtekst in renvooi](renvooitekst_regeling.md)
: Het startpunt is om renvooitekst van de complete tekst van de regeling te maken. Die kan naast het besluit beschikbaar gesteld worden, zowel voorafgaand aan de vaststelling van het besluit als na bekendmaking van het besluit in de vorm van een [proefversie](besluit_proefversies.md), zodat de wijziging in de volledige context van de regeling te lezen is.

Renvooitekst als onderdeel van de besluittekst
: In het algemeen zal slechts een deel van de regeling wijzigen. De tekst van het besluit wordt erg onoverzichtelijk als de renvooitekst van de hele regeling wordt opgenomen. Daarom worden alleen die onderdelen opgenomen die daadwerkelijk wijzigen. Voor het wijzigen van een `RegelingKlassiek` wordt daarvoor een [werkwijze](renvooitekst_klassiek.md) gevolgd die lijkt op de manier waarop wijzigingsinstructies worden opgesteld. Er zijn andere manieren denkbaar, wat geïllustreerd wordt aan de hand van een [voorbeeld](renvooitekst_indikken.md).

[Reconstructie van de regelingtekst uit renvooitekst](renvooitekst_muteren.md)
: De renvooitekst is een handleiding voor software om de nieuwe regelingtekst af te leiden uit de oude die eerder gepubliceerd (of uitgewisseld) is.

## Implementatiekeuze
STOP standaardiseert alleen het renvooimechanisme: de markeringen die aangebracht kunnen worden en de manier waarop de nieuwe regelingtekst uit de oude afgeleid kan worden. Het maken van de renvooitekst is niet voorgeschreven, zolang de wijze waarop maar aansluit bij de doelstellingen van de renvooitekst. Specifiek is de manier waarop de complete renvooitekst opgeknipt wordt voor gebruik in een besluit geen onderdeel van de standaard.
