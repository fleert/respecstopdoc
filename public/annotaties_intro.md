# Annotaties in STOP

Een annotatie is een aanvulling op de juridische inhoud van een instrument (zoals een regeling, informatieobject, besluit of publicatie). Een annotatie bevat een interpretatie of duiding van de juridische inhoud die voor software te begrijpen is. 

De annotatie bevat geen nieuwe juridische informatie. De informatie in een annotatie is voor een mens af te leiden door het instrument te bestuderen. De informatie is alleen op een andere manier gemodelleerd, zodat software er mee kan werken. Een voorbeeld is de [Toelichtingsrelatie](regeling_toelichting.md) als annotatie bij een regeling. De annotatie beschrijft de relatie van een toelichting naar het toegelichte artikel of lid. Die is af te leiden door de tekst goed te lezen, maar de tekst is te ingewikkeld gestructureerd om dat geautomatiseerd door tekstanalyse te laten doen. Door de relaties voor software expliciet te maken in de vorm van een annotatie kan  software deze relaties wel automatisch verwerken.

Een annotatie kan ook verwijzingen bevatten naar informatie die niet in STOP gemodelleerd is. Een voorbeeld is de [JuridischeBorgingVan](gio-juridische-borging.md) als annotatie bij een [GIO](gio-intro.md). De annotatie legt een verband tussen de informatie in de GIO en de objecten (zoals IMOW objecten) die corresponderen met de inhoud van de GIO.

Een annotatie kan ook informatie bevatten om een regeling of informatieobject op een bepaalde manier te kunnen gebruiken. Voorbeelden zijn de metadata die voor elk type instrument is gedefinieerd, of de [FeatureTypeStyle](gio-symbolisatie.md) als annotatie bij een GIO die de symbolisatie bevat voor weergave van de GIO.

Annotaties in STOP hebben geen eigen versie-identificatie, maar horen bij een instrument. Een nieuwe versie van een annotatie ontstaat voor een versie van het instrument. Instrumentversies kunnen altijd inclusief alle annotaties uitgewisseld worden, of een annotatie blijft ook voor volgende instrumentversies van toepassing totdat een nieuwe versie van de annotatie ontstaat. Zie [annotaties voor regelgeving](annotaties_regelgeving.md#implementatiekeuze).

**Zie ook**

* Begripsomschrijving [annotatie](begrippenlijst_annotatie.dita#annotatie).
* Annotaties in het informatiemodel als onderdeel van [instrument en instrumentversies](EA_1A45BBE5EC4F4bb2BBD7F5E701CE0479.dita#Pkg). 
* De volledige lijst van alle STOP annotaties in het overzicht van [modules](imop_modules.md).