# Transformaties 

In het [versieoverzicht](schemata_gebruik_versionering.md) kunnen transformaties vermeld worden om een STOP module van de ene naar de andere versie te transformeren. Alle transformaties worden op dezelfde manier geïmplementeerd:

- Een transformatie is een XSLT-bestand. 
- Bij het uitvoeren ervan moeten parameters meegegeven worden.
- Het uitvoeren van de transformatie wordt afgebroken als de XML serialisatie niet te transformeren is.
- Het resultaat kan het attribuut `naarbestekunnen="true"` bevatten. Dit geeft aan dat er informatieverlies is opgetreden of dat bijvoorbeeld vanwege veranderde invulinstructies de conversie niet helemaal voldoet aan de standaard.

In deze versie van standaard zijn er nog geen transformaties beschikbaar. De eerste transformaties komen beschikbaar zodra er meerdere versies van STOP naast elkaar gebruikt gaan worden voor het doen van officiële publicaties, en die verschillende STOP-versies leiden tot verschillen in de geconsolideerde regelgeving (regelingen en informatieobjecten).