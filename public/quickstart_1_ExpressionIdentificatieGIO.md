# data:ExpressionIdentificatie (GIO)

Deze module voorziet in de identificatie van een Geografisch Informatieobject (GIO) en volgt hierin de Juridische Object Identificatie Naming convention (JOIN). 

|                                           | Vulling                                                      | Verplicht? |
| ----------------------------------------- | ------------------------------------------------------------ | ---------- |
| [FRBRWork](data_xsd_Element_data_FRBRWork.dita#FRBRWork)             | Zie 'JOIN IRI - GIO'                                         | J          |
| [FRBRExpression](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression) | Zie 'JOIN IRI - GIO'                                         | J          |
| [soortWork](data_xsd_Element_data_soortWork.dita#soortWork)           | Voor een aangeleverde GIO dient deze de waarde `'/join/id/stop/work_010'` "Informatieobject" te hebben. | J          |



## JOIN IRI - GIO

Voor de identificatie van het GIO.versie) dienen de JOIN IRIs als volgt te zijn opgebouwd (uitgaande van een Nederlands bevoegd gezag):

`//FRBRWork: "/join/id/regdata/" <overheid> "/" <datum_work> "/" <overig> `

`//FRBRExpression: <work> ["/" <taal>] "@" <datum_expr> [ ";" <versie> ] [ ";" <overig> ]`

[Toelichting](io-expressionidentificatie.md)



## Voorbeeld

[Voorbeeld ExpressionIdentificatie - GIO](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/01-BG-Vuurwerkverbodsgebied-v1-Identificatie.xml)

