# Klassiek tekstmodel

Voor sommige Rijksbesluiten wordt een afwijkend tekstmodel voor de hoofdregeling gebruikt. In dit klassieke regeling-tekstmodel worden bepaalde onderdelen meegenomen die afkomstig zijn uit het besluit waarmee de regeling wordt ingesteld. Dat besluit moet volgens het [klassiek besluitmodel](besluit-klassiek.md) zijn opgesteld. Deze twee klassieke STOP-modellen zijn gebaseerd op de [Aanwijzingen voor de Regelgeving](https://wetten.overheid.nl/BWBR0005730).

Vergelijking RegelingCompact met RegelingKlassiek:

![Vergelijking standaard en klassiek model](img/Regeling-Compact-Klassiek.png)

Het klassieke tekstmodel van een regeling bevat een aantal onderdelen die opgesteld worden voor het besluit en na publicatie van het besluit in principe niet meer wijzigen. In de illustratie hierboven de geel gekleurde delen. Dit zijn:

* De [tekst:Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef): een beschrijving van de considerans en de grondslagen van het besluit.
* De [tekst:Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting) na het lichaam van de regeling en optioneel in een bijlage.
* Eventuele [tekst:WijzigArtikelen](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel) die andere regelingen wijzigen. Zoals bij het [opstellen van het besluit](besluit_hoeveel.md) is beschreven wordt het opnemen van wijzigartikelen in de hoofdregeling ontraden. Bij weergave van het wijzigartikel in de (geconsolideerde) regeling wordt niet de inhoud van het wijzigartikel maar alleen de [tekst:OpmerkingVersie](tekst_xsd_Element_tekst_OpmerkingVersie.dita#OpmerkingVersie) getoond.
* De [tekst:InwerkingtredingArtikelen](tekst_xsd_Element_tekst_InwerkingtredingArtikel.dita#InwerkingtredingArtikel) met de [inwerkingtredingsbepalingen](besluit_iwtbepalingen.md).

Deze onderdelen worden traditioneel gezien als onderdeel van de geconsolideerde regeling, ook al kunnen ze hun betekenis verliezen in latere, sterk gewijzigde versies van de regeling.

Regelingen die volgens het klassiek tekstmodel worden opgesteld kennen geen toelichting. Als er een toelichting nodig is op (een wijziging van) de regeling, dan wordt dat opgenomen in de toelichting bij het besluit.



