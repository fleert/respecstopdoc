# Stap 2a: Optioneel: Kennisgeving bij Besluit

Bij een besluit kan een [kennisgeving](begrippenlijst_kennisgeving.dita#kennisgeving) worden geschreven. Dit is een eigen instrument dat naar een besluit wijst. Net als een besluit bestaat een kennisgeving uit een set samenhangende STOP-modules.

* [Introductie, model en voorbeeld van de Kennisgeving](quickstart_2a_Kennisgeving.md)
* [data:ExpressionIdentificatie](quickstart_2a_ExpressionIdentificatieKennisgeving.md) van de kennisgeving
* [data:KennisgevingMetadata](quickstart_2a_KennisgevingMetadata.md) van de kennisgeving