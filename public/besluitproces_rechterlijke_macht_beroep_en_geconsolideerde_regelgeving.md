# Beroep en geconsolideerde regeling

<div outputclass="mini-toc"><p>De (on)herroepelijkheid van een besluit moet zichtbaar zijn op <a href="https://officielebekendmakingen.nl)"> officielebekendmakingen.nl</a>. Ook in de geconsolideerde regeling moet (on)herroepelijkheid zichtbaar zijn. En ook moet duidelijk zijn of een regel ongeldig is geworden of is gewijzigd door een uitspraak van de rechter. Op welke manier dit in STOP is verwerkt, zal hier worden uitgelegd. De volgende onderwerpen komen hierbij aan bod:</p></div>
