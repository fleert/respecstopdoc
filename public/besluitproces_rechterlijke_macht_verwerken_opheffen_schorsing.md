# Verwerken schorsing opgeheven

Mocht de rechter na verloop van tijd de schorsing weer opheffen, dan geeft het BG dat door.

Een besluit kan meerdere keren worden geschorst. Bijvoorbeeld als een rechter het besluit schorst, de schorsing voorafgaand aan de uitspraak opheft, en in zijn uitspraak het beroep ongegrond verklaard. Een hogere rechter kan vervolgens in een hoger beroep zaak wederom tot een schorsing besluiten. In dergelijke situaties levert het BG telkens de corresponderende procedurestappen *Schorsing* / *Schorsing opgeheven* via een direct mutatie met een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie), zodat de wijzigingen van het besluit in de geconsolideerde regeling terecht voorzien worden van een vlag 'geschorst' of niet. 
Onderdelen Procedureverloopmutatie schorsing opgeheven:
* [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) is hierbij altijd  `/join/id/stop/procedure/stap_020` (Schorsing opgeheven).
* [`voltooidOp`](data_xsd_Element_data_voltooidOp.dita#voltooidOp) bevat de datum waarop de schorsing is opgeheven.
* (optioneel) [`meerInformatie`](data_xsd_Element_data_meerInformatie.dita#meerInformatie), link naar de BG-website met meer informatie over de gevolgen van een schorsing voor de juridische werking van het besluit. Dit is niet de URL van de bekendmaking.

Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).


## Voorbeeld schorsing opgeheven
```xml
<Procedureverloopmutatie>
  <bekendOp>2020-07-16</bekendOp>
  <voegStappenToe>
    <Procedurestap>
      <!-- Schorsing opgeheven  -->
      <soortStap>/join/id/stop/procedure/stap_020</soortStap>
      <voltooidOp>2020-07-14</voltooidOp>
    </Procedurestap>
  </voegStappenToe>
</Procedureverloopmutatie>
```

