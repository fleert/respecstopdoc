# Correcties in renvooi

Net als reguliere besluiten, maken rectificaties ook gebruik van [renvooi](bepalen_wijzigingen_renvooi.md) om wijzigingen aan te duiden. Er is echter één verschil: rectificaties bevatten correcties in plaats van wijzigingen.

Om dit goed tot uiting te laten komen, moeten afwijkende standaard zinnen gehanteerd worden in de [tekst:Wat](tekst_xsd_Element_tekst_Wat.dita#Wat) om de context van de correcties juist aan te kunnen geven.

Voor rectificaties wordt in principe de zinsnede "*De tekst van .... wordt gecorrigeerd.*" gebruikt. Zie onderstaande tabel.

## Beperkingen

Een rectificatie kan zowel correcties aanbrengen in de tekst van de regeling als in de tekst van het besluit zelf. Voor correcties in de tekst van het *besluit* zijn niet alle mutaties toegestaan: 

- Een besluit heeft altijd een [`RegelingOpschrift`](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) <sup>noot * </sup>. Dit betekent dat het opschrift van een besluit niet via een rectificatie kan worden toegevoegd of verwijderd.
- Fouten in het `tekst:RegelingOpschrift` kunnen wel worden verbeterd met een `Vervang` binnen een `<BesluitMutatie>`. In dit specifieke geval moet ook een nieuwe versie van `data:BesluitMetadata` worden gerectificeerd en aangeleverd - daarin wordt immers ook de te corrigeren titel van het te rectificeren besluit vastgelegd.  
- Een bestaande `tekst:Aanhef` of `tekst:Sluiting` kan niet worden verwijderd middels een `<BesluitMutatie>`, wel worden vervangen.


## Renvooizinnen voor rectificatie

Voorbeeldopzet van 'gestandaardiseerde' inleidende zinnen voor rectificaties:

| **Mutatieactie**                                             | **Invulling t.b.v. Rectificatie**                            | Voorbeeld                                                    |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| `tekst:VoegToe[@positie="volgtOp"]`                           | <p>*Enkelvoud:* `[#] Correctie: Na [voorgaande mutatie-eenheid] wordt een [eenheid] ingevoegd, luidende:`</p><p>*Meervoud:*`[#] Correctie: Na [voorgaande mutatie-eenheid] worden [aantal] [eenheden] ingevoegd, luidende:`</p> | A. Correctie: Na artikel 149a worden twee artikelen ingevoegd, luidende:Artikel 149a... |
| `tekst:VoegToe[@positie="komtVoor"]`                           | <p>*Enkelvoud:* `[#] Correctie: Voor[opvolgende mutatie-eenheid] wordt een [eenheid] ingevoegd, luidende:` </p><p>*Meervoud:*`[#] Correctie: Voor[opvolgende mutatie-eenheid] worden [aantal] [eenheden] ingevoegd, luidende:`</p> | B. Correctie: Voor artikel 2 wordt een artikel ingevoegd, luidende: Artikel 2... |
| `tekst:VervangKop` bij wijziging van opschrift en/of vernummering | `[#] De tekst van het opschrift van [mutatie-eenheid] wordt op de aangegeven wijze gecorrigeerd:` | C. De tekst van het opschrift van hoofdstuk VII wordt op de aangegeven wijze gecorrigeerd: |
| `tekst:Vervang` bij wijziging onderliggende inhoud (met eventuele vernummering of niet) | `[#] De tekst van [mutatie-eenheid] wordt op de aangegeven wijze gecorrigeerd:` | D. De tekst van Artikel 7 wordt op de aangegeven wijze gecorrigeerd: |
| <p>`tekst:Vervang` invulling van een eerder `[`Gereserveerd`]` artikel;</p><p>zowel de was-tekst als de wordt-tekst wordt getoond in de PDF/HTML bekendmaking</p> | `[#] De tekst van [mutatie-eenheid] wordt op de aangegeven wijze gecorrigeerd:` | <p>E. De tekst van Artikel 2.11 wordt op de aangegeven wijze gecorrigeerd:</p><p>Artikel 2.11 (geometrische begrenzing PKB-Waddenzee en Waddengebied)</p> <p><span class="doorhalen">`[`Gereserveerd`]`</span></p><p><u>Bij een aanvraag ...</u></p> |
| `tekst:Vervang` invulling van een eerder `[`Gereserveerd`]` mutatie-eenheid (anders dan artikel). (wordt-tekst zonder renvooiopmaak in de PDF/HTML) | `[#] Correctie: De tekst van [mutatie-eenheid] komt te luiden:` | F. Correctie: De tekst van Hoofdstuk 2 `komt te luiden:`     |
| <p>`tekst:Vervang` invulling van een eerder `[`Gereserveerd`]` mutatie-eenheid (anders dan artikel) die ook vernummerd wordt.</p><p>(wordt-tekst zonder renvooiopmaak in de PDF/HTML)</p> | `[#] Correctie: [mutatie-eenheid bestaand nr.] wordt vernummerd tot [mutatie-eenheid nieuw nr.] en de tekst komt te luiden:` | Correctie: Paragraaf 7.1.2 wordt vernummerd tot paragraaf 7.2.2 en de tekst komt te luiden: |
| `tekst:Vervang` met enkel een verplaatsing | `[#] Correctie: [verplaatste mutatie-eenheid] wordt verplaatst van [bestaande parent mutatie-eenheid] naar [nieuwe parent mutatie-eenheid].` | G. Correctie: Artikel 2.4 wordt verplaatst van paragraaf 2.3.2 naar paragraaf 2.3.3. |
| `tekst:Vervang` met verplaatsing en wijziging en/of vernummering | `[#] Correctie: [verplaatste mutatie-eenheid] wordt verplaatst van [bestaande parent mutatie-eenheid] naar [nieuwe parent mutatie-eenheid]. ``[#] De tekst van [mutatie-eenheid] wordt op de aangegeven wijze gecorrigeerd:` | H. Correctie: Artikel 2.4 wordt verplaatst van paragraaf 2.3.2 naar paragraaf 2.3.3. De tekst van Paragraaf 2.3.3 wordt op de aangegeven wijze gecorrigeerd: |
| `tekst:Vervang` met uitsluitend de vervanging van een tabel | `[#] Correctie: De tekst van [mutatie-eenheid] komt te luiden:` | I. Correctie: De tekst van Bijlage XII komt te luiden:       |
| <p>`tekst:Vervang` met een `//Artikel/Vervallen`</p><p>(zowel de was-tekst als de wordt-tekst wordt getoond in de PDF/HTML bekendmaking)</p> | `[#] De tekst van [mutatie-eenheid] wordt op de aangegeven wijze gecorrigeerd:` | <p>J. De tekst van Artikel 7.3 wordt op de aangegeven wijze gecorrigeerd:</p>  <p>Artikel 7.3 (exclusieve economische zone)</p><p><span class="doorhalen">Deze regeling is ook van toepassing in de exclusieve economische zone.</span></p><p>`[`Vervallen`]`</p> |
| <p>`tekst:Verwijder` heeft dezelfde verbeelding als `//Vervallen`.</p><p>**NB** `[`Vervallen`]` dient hier dan bij gegenereerd te worden in de HTML en PDF.</p> | `[#] De tekst van [mutatie-eenheid] wordt op de aangegeven wijze gecorrigeerd:` | <p>K. De tekst van Artikel 9 wordt op de aangegeven wijze gecorrigeerd:</p><p>Artikel 9 (exclusieve economische zone)</p><p><span class="doorhalen">Deze regeling is ook van toepassing in de exclusieve economische zone.</span></p><p>`[`Vervallen`]`</p> |
| `tekst:Vervang` van een Divisie of Divisietekst onder een RegelingVrijetekst | `[#] De tekst van de volgende sectie wordt op de aangegeven wijze gecorrigeerd:` | L. De tekst van de volgende sectie wordt op de aangegeven wijze gecorrigeerd: |
| `tekst:VervangKop` van het opschrift en/of vernummering van een Divisie of Divisietekst | `[#] De tekst van het volgende opschrift wordt op de aangegeven wijze gecorrigeerd:` | M. De tekst van het volgende opschrift wordt op de aangegeven wijze gecorrigeerd: |
| `tekst:Vervang` van een Divisie of Divisietekst binnen een Bijlage | `[#] Binnen [bijlage + Nummer] wordt de tekst van de volgende sectie op de aangegeven wijze gecorrigeerd:` | N. Binnen bijlage VIII wordt de tekst van de volgende sectie op de aangegeven wijze gecorrigeerd: |
| `tekst:VervangKop` van het opschrift en/of vernummering van een Divisie of Divisietekst binnen een Bijlage | `[#] Binnen [bijlage + Nummer] wordt de tekst van het volgende opschrift op de aangegeven wijze gecorrigeerd:` | O. Binnen bijlage VI wordt de tekst van het volgende opschrift op de aangegeven wijze gecorrigeerd: |

- `[#]` doornummering van mutatieacties conform de Aanwijzingen voor de Regelgeving: A., B., C., ..., Z, AA, BB, CC, ..., ZZ, AAA, BBB, CCC etc. Bij iedere `<WijzigBijlage>` opnieuw beginnen bij A.
- `[Mutatie-eenheid]` als `<Label> + <Nummer>`, m.u.v. `<Lichaam>`. Indien `<Lichaam>` de mutatie-eenheid is, dan komt er in de `<Wat>`: “Het lichaam van de regeling”


## Renvooizinnen in besluitmutaties
Om alle onderdelen van een besluittekst te rectificeren met een [besluitmutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie) kunnen er bij een rectificatie ook als `mutatie-eenheid` voorkomen:

- [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef) - bijv. De tekst van *de aanhef* wordt op de aangegeven wijze gecorrigeerd:
- [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)
  - bijv. De tekst van *de ondertekening* wordt op de aangegeven wijze gecorrigeerd:
  - bijv. De tekst van *de dagtekening* wordt op de aangegeven wijze gecorrigeerd:
  - bijv. De tekst van *het slotformulier* wordt op de aangegeven wijze gecorrigeerd:
  - Indien een rectificatie een correctie van de (groep van) alinea('s) in de sluiting bevat, zal situationeel een passende inleidende renvooi zin opgesteld moeten worden.
- [Regelingopschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) - bijv. De tekst van *het opschrift van de regeling* wordt op de aangegeven wijze gecorrigeerd:



__noot *__: Preciezer gezegd [BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact) heeft een verplicht kindelement `RegelingOpschrift`; [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact) heeft of een direct kindelement `RegelingOpschrift` of binnen een [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) een verplicht `RegelingOpschrift`.