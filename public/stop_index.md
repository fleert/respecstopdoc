# Standaard Officiële Publicaties

Dit is de documentatie van versie @@@IMOPVERSIE@@@ van de Standaard Officiële Publicaties (STOP) die is gepubliceerd op @@@DATUM@@@. De documentatie bestaat uit vijf delen:

[Wijzigingsoverzicht](stop_wijzigingsoverzicht.md)
: Een overzicht van de wijzigingen die in deze STOP-versie zijn aangebracht ten opzichte van eerdere STOP-versies, en wijzigingen in de bijbehorende documentatie zijn aangebracht ten opzichte van eerdere documentatie-versies.

[Uitgangspunten](stop_uitgangspunten.md)
: Een beschrijving van de uitgangspunten die een rol gespeeld hebben bij het opstellen van de standaard.
Het gaat daarbij zowel om de juridische context, functionele eisen en architectuuroverwegingen.

[Gebruik van de standaard](stop_gebruik.md)
: Een beschrijving per onderwerp waarvoor de standaard toegepast kan worden. De indeling van dit hoofdstuk is gebaseerd op de volgorde 
waarin de informatie tot stand komt die volgens de standaard uitgewisseld kan worden.

[Informatiemodel](EA_4D1D7FD517FE4d8dA9A2B905D202C92B.dita#Pkg)
: Een beschrijving van de informatie die onderdeel is van de standaard.
Een export van het model is te vinden op [Gitlab](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Auml).

[Bedrijfsregels](businessrules.md)
: Een opsomming van de bedrijfsregels die van toepassing zijn voor de informatie. Naast deze bedrijfsregels worden soms in de documentatie aanvullende conformiteitsregels beschreven. De bedrijfsregels uit de opsomming worden expliciet benoemd omdat ze te valideren zijn tijdens of na de uitwisseling van informatie.



