# Een IO intrekken

**N.B.**: Deze stap geldt alleen voor te consolideren IO's.  

Een regelingwijziging kan er toe leiden dat een IO niet langer nodig is, bijvoorbeeld omdat het artikel dat naar betreffende IO verwees, wordt geschrapt. De juridische werking van een IO wordt beëindigd door het IO in te trekken. Een intrekking moet tot uitdrukking komen in zowel de tekst als de [consolidatie-informatie](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) van het besluit. 


Dit doet men als volgt:

1. De verwijzing naar het IO in de regelingtekst wordt verwijderd.
2. De JOIN-ID en het label van het IO in de bijlage van de regeling worden verwijderd.
3. In de `ConsolidatieInformatie` van het besluit in `Intrekkingen` wordt aangegeven dat het IO wordt ingetrokken.
## Voorbeeld verwerking intrekking in ConsolidatieInformatie
De `eId` van het voorbeeld verwijst naar de plek in het besluit waar de JOIN-ID van het IO wordt verwijderd. De JOIN-ID van het IO-work komt in het element `instrument`.

```
<Intrekkingen>
  <Intrekking>
    <doelen>
      <doel>/join/id/proces/gm9999/2021/WijzigingKernenOmgevingsplan</doel>
    </doelen>
    <instrument>/join/id/regdata/gm9999/2019/gio993859238</instrument>
    <eId>!wijziging#cmp_A__content_o_1__list_1__item_1__ref_o_1_inst2</eId>
  </Intrekking>
</Intrekkingen>
```
