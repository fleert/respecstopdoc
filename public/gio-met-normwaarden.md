# GIO met normwaarden 

Men gebruikt deze GIO als men normwaarden en hun [werkingsgebied](begrippenlijst_werkingsgebied.dita#werkingsgebied) juridisch wil vastleggen door de normwaarden per locatie in de GIO op te nemen. 

## Onderdelen  
Een GIO met normwaarden dat onderdeel is van een besluit dat onderdeel is van een besluit heeft de GIO-specifieke onderdelen van een [juridisch geldige GIO](gio-onderdelen.md#GIO-specifieke-onderdelen), maar heeft:
* *geen* GIO-delen. 
* (verplicht) [symbolisatie](gio-symbolisatie.md). 

## Voorbeeld GIO met normwaarden
Een GIO waarin bouwhoogtes worden vastgesteld, bevat ~~van~~ de norm "maximale bouwhoogte" en elke locatie in deze GIO krijgt een normwaarde voor de maximale bouwhoogte. Zie onderstaande afbeelding van de GIO met normwaarden voor de maximale bouwhoogte. 

![Voorbeeld van een GIO met normwaarden voor maximale bouwhoogte](img/GIO-Bouwhoogtes.png "Voorbeeld van een GIO met normwaarden voor maximale bouwhoogte")

## Zie ook

[Voorbeeldbestand van een GIO met normwaarden](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Coderingen/GIO/GIO-symbolisatie/GIO-normwaarde)
