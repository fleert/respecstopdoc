# Onderdelen informatieobject

## Overzicht vaste onderdelen  

Een informatieobject heeft altijd de volgende onderdelen: 

**Metadata**

1. De module [ExpressionIdentificatie](io-expressionidentificatie.md), de identificatie van een versie van het informatieobject.
2. De module [InformatieObjectMetadata](io-metadata.md), de metadata voor het informatieobject. Deze bevat onder meer de [Publicatieinstructie](io-publicatie-instructie.md).
3. De module [InformatieObjectVersieMetadata](io-versiemetadata.md), de metadata voor de versie van een informatieobject. Deze bevat onder meer de [SHA512-hash](io-hash.md) een uniek serienummer berekent op basis van de inhoud van een bestand.

**Inhoud**

1. Een of meerdere inhouds-modules (varieert per [soort informatieobject](regeling-modellenIO.md)).




## Overzicht onderdelen geconsolideerd IO

Een geconsolideerd informatieobject heeft daarnaast de volgende, veelal afgeleide onderdelen:
1. De module [`cons:ConsolidatieIdentificatie`](cons_xsd_Element_cons_ConsolidatieIdentificatie.dita#ConsolidatieIdentificatie), de identificatie van een geconsolideerd informatieobject.
2. De module [`cons:GerechtelijkeProcedureStatus`](cons_xsd_Element_cons_GerechtelijkeProcedureStatus.dita#GerechtelijkeProcedureStatus), informatie over de status van het informatieobject naar aanleiding van nog openstaande mogelijkheden tot beroep of van lopende gerechtelijke procedures.
3. De module [`cons:JuridischeVerantwoording`](cons_xsd_Element_cons_JuridischeVerantwoording.dita#JuridischeVerantwoording), informatie over de juridische bronnen die gebruikt zijn om het geconsolideerd informatieobject samen te stellen
4. Een [toestand module](consolideren_toestanden.md) met informatie over de juridische geldigheid van een informatieobject.

Net als bij een geconsolideerde regeling geven de modules van een geconsolideerd informatieobject informatie over de geldigheid. Vanuit deze informatie wordt voor de inhoud verwezen naar versies van het informatieobject (zoals de versie van de PDF of de GeoInformatieObjectVersie) en de modules die daarbij horen.

## Zie ook

* Het [Informatiemodel]( ea:P:%7BED1B61D3-297C-4ae6-BD9A-4FDB6BDF83F9%7D) van een informatieobject.
