# data:BesluitMetadata

Deze module voorziet in de metadata van een besluit.

| Element                                                      | Vulling                                                      | Verplicht?           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------- |
| [afkortingen](data_xsd_Element_data_afkortingen.dita#afkortingen)                          | Afkortingen waarmee het besluit kan worden aangeduid.        | N                    |
| [alternatieveTitels](data_xsd_Element_data_alternatieveTitels.dita#alternatieveTitels)            | Een alternatieve titel voor het instrument (hier: Besluit), die niet officieel is vastgesteld en die in het gangbaar spraakgebruik gebruikt wordt om het instrument (Besluit) aan te duiden | N                    |
| [heeftCiteertitelInformatie](data_xsd_Element_data_heeftCiteertitelInformatie.dita#heeftCiteertitelInformatie) | Container                                                    | N                    |
| [heeftCiteertitelInformatie/CiteertitelInformatie/citeertitel](data_xsd_Element_data_citeertitel.dita#citeertitel) | De titel van het instrument (hier Besluit) die gebruikt wordt in aanhalingen | J (binnen container) |
| [heeftCiteertitelInformatie/CiteertitelInformatie/isOfficieel](data_xsd_Element_data_isOfficieel.dita#isOfficieel) | Boolean (true/false). Indien er een 'citeertitel' is, dan dient 'isOfficieel' ook gevuld te zijn | J (binnen container) |
| [eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke)      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J                    |
| [grondslagen](data_xsd_Element_data_grondslagen.dita#grondslagen)                          | Machineleesbare verwijzingen naar een juridische bron die de wettelijke grondslag voor het besluit geeft | N                    |
| [informatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs)        | JOIN Expression IRI (zie module [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieGIO.md)) van een [Juridisch informatieobject](begrippenlijst_informatieobject.dita#informatieobject) dat met de tekst van het besluit gepubliceerd moet worden, omdat: a) het informatieobject onderdeel is van de juridische inhoud van het besluit, of b) het aanvullende informatie geeft over het besluit | N                    |
| [maker](data_xsd_Element_data_maker.dita#maker)                                      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J                    |
| [officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel)                    | De officiële titel van het besluit                           | J                    |
| [onderwerpen](data_xsd_Element_data_onderwerpen.dita#onderwerpen)                          | Zie waardelijst [onderwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/onderwerp.xml) | J                    |
| [rechtsgebieden](data_xsd_Element_data_rechtsgebieden.dita#rechtsgebieden)                    | Zie waardelijst [rechtsgebied.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/rechtsgebied.xml) | J                    |
| [soortProcedure](data_xsd_Element_data_soortProcedure.dita#soortProcedure)                    | Zie waardelijst [soortprocedure.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortprocedure.xml). De waarde (ontwerp of definitief) van soortProcedure bepaalt de invulling van de [module data:ProcedureVerloop](quickstart_2_ProcedureVerloop.md). Onder de tabel is een korte toelichting op de classificatie van een definitief of een ontwerpbesluit opgenomen | J                    |
| [soortBestuursorgaan](data_xsd_Element_data_soortBestuursorgaan.dita#soortBestuursorgaan)          | Zie waardelijst [bestuursorgaan.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/bestuursorgaan.xml). Bij wijzigingsbesluiten hoeft soortBestuursorgaan niet identiek te zijn aan soortBestuursorgaan van de regeling.   | N                    |



## **Definitief besluit**

Een definitief besluit heeft de intentie om op een zeker moment te resulteren in een nieuwe Toestand (zie ook [data:Toestanden](quickstart_3_Toestanden.md)) van een regeling. De juridische inwerkingtredingsdatum van het besluit kan reeds met het aangeleverde besluit worden meegegeven (zie [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md)) of op een later moment worden bepaald (bijvoorbeeld via een Koninklijk Besluit). 



[Voorbeeld BesluitMetadata bij definitief besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/02-BG-Besluit-Metadata.xml)



## **Ontwerpbesluit**

Het bevoegd gezag kan een ontwerpbesluit nemen alvorens een definitief besluit te nemen om bijvoorbeeld rekening te houden met de zienswijzen van verschillende belanghebbenden. Een ontwerpbesluit leidt niet tot een nieuwe Toestand van een regeling. Met een ontwerpbesluit worden dus ook geen tijdstempels meegestuurd; zie ook [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md).









