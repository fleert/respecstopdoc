# De regelingtekst in renvooi

[TODO](DITA_OnbekendeLink.dita#Oeps)

Het startpunt is om renvooitekst van de complete tekst van de regeling te maken. De kan naast het besluit beschikbaar gesteld worden, zowel voorafgaand aan de vaststelling van het besluit als na bekendmaking van het besluit in de vorm van een [proefversie](besluit_proefversies.md), zodat de wijziging in de volledige context van de regeling te lezen is.