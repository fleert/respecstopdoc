# Opstellen van een besluit

De informatie die in STOP als [regeling](regeling.md) gemodelleerd wordt heeft geen juridische status als het niet formeel vastgesteld en bekendgemaakt wordt. Het vaststellen van een regeling of het wijzigen van een regeling is een besluit van het bevoegd gezag. Het 'besluit' in STOP is een vastlegging van dat besluit. Het wordt ambtelijk voorbereid, vastgesteld door het bevoegd gezag en daarna bekendgemaakt.

Relatie besluit - regeling
: Een besluit in STOP moet voldoen aan wettelijke eisen die gesteld worden aan de manier waarop een de instelling van een nieuwe regeling of de wijziging van een bestaande regeling vastgelegd en gepubliceerd moet worden. Maar het is onwenselijk als deze eisen ertoe leiden dat regelingen en informatieobjecten herschreven moeten worden voor een besluit. Het mag ook niet zo zijn dat er in het besluit (de juridische basis van een regeling) iets anders staat dan wat eenieder via de landelijke voorzieningen (zoals [wetten.nl](https://wetten.overheid.nl/)) kan raadplegen. In STOP wordt een besluit daarom gebaseerd op een omkeerbare transformatie van regeling- en informatieobjectversies. Dit is elders als [uitgangspunt](automatiseerbaar-creatieproces.md) beschreven.

[Eén of meer besluiten](besluit_hoeveel.md)
: Het vaststellen van nieuwe versies voor één of meer regelingen en één of meer doelen kan meestal met één besluit gedaan worden, maar dat is niet altijd toegestaan of wenselijk.

[Modellering van het besluit](besluit_modellering.md)
: In STOP wordt een besluit gemodelleerd als een "container" die de getransformeerde regeling- of informatieobjectversies en inwerkingtredingsbepalingen bevat. De "container" bestaat uit onderdelen die alleen voor het besluitvormingsproces nodig zijn.

[Renvooitekst](renvooitekst.md)
: De nieuwe tekst voor een regeling moet opgenomen worden in de tekst van het besluit. Als het gaat om de eerste versie van de regeling of van een tijdelijk regelingdeel dan wordt de tekst integraal overgenomen. Als het een wijziging betreft dan moet de tekst met renvooimarkeringen opgenomen worden. Het renvooimechanisme is zo ontworpen dat de renvooitekst (zowel door software als door de mens) gelezen kan worden als wijziginstructies waarmee de tekst van de regeling gereconstrueerd kan worden. Het toepassen van de wijziginstructies wordt *muteren* genoemd.

[Informatieobjecten bij het besluit](besluit_informatieobject.md)
: Net als de tekst van een regeling wordt (een wijziging van) de inhoud van een informatieobject formeel vastgesteld als onderdeel van het besluit. Er zijn informatieobjecten die gegevens bevatten die niet direct door een mens begrepen kunnen worden. Bijvoorbeeld een [GIO](gio-intro.md): niemand kan door de coördinaten van de geometrische begrenzingen te lezen vaststellen of de begrenzing correct is. Dat is wel mogelijk door de gegevens in een context (voor een GIO: samen met achtergrondkaarten) te tonen. Dit soort informatieobjecten wordt getransformeerd bij het besluit gevoegd, waarbij de transformatie bestaat uit het toevoegen van de vaststellingscontext aan het oorspronkelijke informatieobject.

[In- en uitwerkingtredingsbepalingen](besluit_iwtbepalingen.md)
: In de tekst van het besluit staat vaak beschreven wanneer (delen van) het besluit in werking treden. Die beschrijving moet overeenkomen met de tijdstempels die via het STOP versiebeheer gekoppeld zijn aan de doelen/branches van de regelingen en informatieobjecten die in werking treden. Een intrekking van een regeling kan ook als bepaling opgenomen worden.

[Toelichting en motivering](besluit_toelichting.md)
: Onderdeel van de tekst van een besluit is een motivering en/of een toelichting die het hoe en waarom van een besluit beschrijft. Afhankelijk van het soort besluit kunnen onderdelen hiervan wettelijk verplicht zijn.

[Naamgeving van een besluit](besluit_naamgeving.md)
: Voor de naamgeving van een besluit wordt de Akoma Ntoso naamgevingsconventie gebruikt.

[Speciale wijzigingsconstructies](besluit_speciaal.md)
: Er zijn een aantal juridische constructies waarbij in het dictum/lichaam van het besluit een beschrijving is opgenomen hoe een regeling gewijzigd moet worden, maar die wijziging is niet op de gebruikelijke wijze als renvooitekst opgenomen.

[Proefversies](besluit_proefversies.md)
: Zeker bij besluiten die een deel van een regeling wijzigen wordt maar een deel van de regeling in renvooi opgenomen in het besluit. Er kan behoefte zijn om de hele regelingversie in te zien zoals die resulteert uit het besluit. Dat wordt een proefversie genoemd, en STOP heeft modellen om een proefversie uit te wisselen.

Elders in de documentatie wordt beschreven:

[Besluitvormingsproces](inspraak_en_vaststelling.md)
: Beschrijving van de verschillende processtappen die een besluit kan doorlopen, van concept tot vastgesteld en bekendgemaakt.

[Attenderen](attenderen.md)
: Voor de bekendmaking van het besluit is extra informatie vereist om belanghebbenden en geïnteresseerden te attenderen op de publicatie van het besluit.

Proactief oplossen van samenloop
: Als er tegelijkertijd meerdere besluitvormingstrajecten lopen kan niet elk besluit voortbouwen op een voorgaand besluit. Het is vaak niet bekend uit welke van de trajecten het voorgaande besluit zal komen. Zolang besluiten juridisch onafhankelijk zijn van elkaar kan de samenloop die voortkomt uit het niet op elkaar voortbouwen, opgelost worden zonder een nieuw besluit te nemen. Als besluiten wel juridisch afhankelijk zijn, dan kan in het ene besluit al de oplossing opgenomen worden van de samenloop die ontstaat als het andere besluit in werking treedt. Dit is beschreven als [Conditionele wijziging](versiebeheer_gebruik.md#Conditoneel). 

