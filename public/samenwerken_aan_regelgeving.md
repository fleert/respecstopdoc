# Samenwerken aan regelgeving

Het maken van een nieuwe versie van een regeling gebeurt niet alleen binnen de organisatie van het bevoegd gezag dat verantwoordelijk is voor de regeling. Het bevoegd gezag kan een deel van het werk uitbesteden aan een adviesbureau en het resultaat gebruiken voor besluitvorming. Een adviesbureau kan ook worden ingeschakeld door een initiatiefnemer die een wijziging van de regeling laat uitwerken door een adviesbureau voordat deze aan de gemeente wordt aangeboden.

*Uitgangspunt: complete regelingversies uitwisselen*

Een adviesbureau zal uitgaan van een complete versie van de regeling met bijbehorende informatieobjecten en annotaties. Dit kan een (nu of in de toekomst) geldige versie van de regeling zijn die uit de LVBB wordt gehaald, of een (bekendgemaakte of nog onderhanden) versie afkomstig van het bevoegd gezag. Het adviesbureau zal wijzigingen aanbrengen, waarbij in het algemeen niet alleen de tekst van de regeling maar ook informatieobjecten en annotaties worden bijgewerkt. Na voltooiing zal het bevoegd gezag de complete nieuwe versie ontvangen, inclusief ongewijzigde onderdelen. Het bevoegd gezag zal de versie bijwerken met eventuele wijzigingen uit andere trajecten, er (ontwerp)besluiten van maken en zorgen voor de uiteindelijke vaststelling. Vaak zal het adviesbureau ook al een voorlopige versie van de motivering of toelichting op de aangebrachte wijzigingen maken die het bevoegd gezag kan gebruiken bij het opstellen van het besluit.

*Eisen aan de standaard*

Om de uitwisseling van regelingversies tussen partijen mogelijk te maken moeten verzender en ontvanger dezelfde methode gebruiken om versies aan te duiden en hetzelfde uitwisselformaat gebruiken. Vandaar dat STOP voorschriften bevat voor:

[Versiebeheer voor samenwerken](samenwerken_versiebeheer.md)
: Een adviesbureau zal onafhankelijk van het bevoegd gezag aan een nieuwe versie van een regeling werken. Het bevoegd gezag moet in staat zijn om het werk van het adviesbureau op te nemen in het eigen versiebeheer voor de regeling. De standaard biedt daarvoor mogelijkheden.

[Inhoud uitwisselpakket](samenwerken_modellering.md)
: De uitwisseling tussen adviesbureau en bevoegd gezag is gebaseerd op het gebruik van het [uitwisselpakket](uitwisselpakket.md). Daarvoor gelden aanvullende afspraken over de inhoud van het pakket. 

[Naamgeving van versieinformatie](versieinformatie_naamgeving.md)
: Het uitwisselpakket bevat versieinformatie. Dat is als een aparte component in STOP gemodelleerd met een eigen naamgeving. Hiervoor is de naamgevingsconventie voor niet-tekstuele informatie van toepassing.
