# Vernietiging

Als een beroepsorgaan het beroep gegrond heeft verklaard en het besluit daarbij heeft **vernietigd**, moeten door het BG de volgende extra processtappen worden verricht:

## 1. Maak een mededeling vernietiging

De mededeling bestaat uit de volgende modules:

1.  De modules zoals omschreven in stappen 1.1 t/m 1.3 van het scenario [vernietiging met behoud van rechtsgevolgen](scenario-vernietiging-zonder-rechtsgevolgen).  
5. De [`ConsolidatieInformatie`](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) om aan te geven dat de regeling- en eventuele `TeConsolideren` informatieobjecten van het vernietigde besluit niet meer bij het doel horen. Deze module bevat `TerugtrekkingRegeling` en `TerugtrekkingTijdstempel` waarmee de `juridischWerkendVanaf`-datum bij het doel wordt weggehaald. Eventueel wordt ook een `TerugtrekkingInformatieobject` toegevoegd.

### Voorbeeld consolidatie-informatie mededeling vernietiging

```
<ConsolidatieInformatie>
  <gemaaktOp>2020-11-15T09:03:00Z</gemaaktOp>
  <Terugtrekkingen>
    <TerugtrekkingRegeling>
      <bekendOp>2020-11-11</bekendOp>
      <!-- ontkoppeling van het doel aan de regelingversie van <RegelingMutatie>  -->
      <doelen>
        <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
      </doelen>
      <instrument>/akn/nl/act/gm9999/2020/REG0001/nld@2020-06-16;2</instrument>
      <eId>gm9999_1__content_1</eId>
      <gemaaktOpBasisVan>
        <Basisversie>
            <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
            <gemaaktOp>2020-06-20T09:03:00Z</gemaaktOp>
        </Basisversie>
      </gemaaktOpBasisVan>
    </TerugtrekkingRegeling>
    <TerugtrekkingInformatieobject>
      <bekendOp>2020-11-11</bekendOp>
      <doelen>
        <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
      </doelen>      
      <instrument>/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2</instrument>
      <eId>gm9999_1__content_1</eId>
      <gemaaktOpBasisVan>
        <Basisversie>
            <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
            <gemaaktOp>2020-06-20T09:03:00Z</gemaaktOp>
        </Basisversie>
      </gemaaktOpBasisVan>
    </TerugtrekkingInformatieobject>
    <TerugtrekkingTijdstempel>
        <bekendOp>2020-11-11</bekendOp>
        <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
        <soortTijdstempel>juridischWerkendVanaf</soortTijdstempel>
        <eId>gm9999_1__content_2</eId>
    </TerugtrekkingTijdstempel>
  </Terugtrekkingen>
</ConsolidatieInformatie>
```


## 2. Verwerk de onherroepelijkheid van het besluit
Komt met deze vernietiging een einde aan alle beroepsprocedures (evt. na het ongebruikt verstrijken van de hogerberoepstermijn), dan moet het BG het besluit de status onherroepelijk geven door het aanleveren van de procedurestap "Beroep(en) definitief afgedaan)". Dit gebeurt door een [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap) met daarin een [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) met de waarde `/join/id/stop/procedure/stap_021` (beroep(en) definitief afgedaan). Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).

De Procedureverloopmutatie wordt aangeleverd met een directe mutatie, of wordt meegeleverd met de mededeling uit stap 1, als er geen hogerberoepstermijn is die afgewacht hoeft te worden.

Lopen er nog andere beroepprocedures dan is er nog geen definitief einde en geeft het BG nu nog niets door.


## Zie ook

* Voor een volledig uitgewerkt scenario met voorbeelden van onderdelen, van opstelfase van het vernietigde besluit tot en met consolidatie naar aanleiding van de vernietiging, zie [Uitleg voorbeelden scenario vernietiging](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/02%20Vernietiging/index.md). 
* Als een beroepsorgaan uitspraak doet en een besluit (gedeeltelijk) vernietigd waarvan de regeling inmiddels is gewijzigd door andere besluiten, is er sprake van samenloop. Dat wil zeggen: de vernietiging maakt de geconsolideerde regelingversie die voortvloeide uit het vernietigde besluit (gedeeltelijk) incorrect, alsmede de geconsolideerde regelingversies van  opeenvolgende besluiten die op het vernietigde besluit waren gebaseerd. Zie [oplossen samenloop](proces_samenloop.md) over hoe het BG ervoor kan zorgen dat vernietiging van het besluit is verwerkt in de huidige geconsolideerde regeling.

