# Waardelijsten

IMOP schrijft voor diverse XML-elementen het gebruik van voorgedefinieerde waardes voor. 

Doelstelling is om zo min mogelijk of eigenlijk geen eigen STOP-waardelijsten te gebruiken, maar in plaats daarvan te verwijzen naar een linked-data-catalogus. De waardelijsten waar STOP gebruik van maakt zijn afkomstig uit de linked-data-catalogus [**Thesaurus en Ontologie Open overheidsInformatie** (TOOI)](https://tardis.overheid.nl/). 

Het voornemen is om in release B van het LVBB bronhouderkoppelvlak volledig over te gaan naar het gebruik van de TOOI-catalogus in plaats van de waardelijsten die met STOP worden meegeleverd. De STOP-waardelijsten zullen dan verdwijnen; ook de definitie daarvan, vastgelegd in [imop-rsc.xsd](rsc_xsd_Main_schema_rsc_xsd.dita#rsc.xsd) is daarmee overbodig.  

Vooruitlopend op de overstap naar TOOI zijn bij de waardelijsten in STOP verwijzingen opgenomen naar de corresponderende TOOI-waardelijsten, in het element [`Vindplaats`](rsc_xsd_Element_rsc_Vindplaats.dita#Vindplaats)). Generieke waardelijsten binnen STOP (zoals [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml) of [onderwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/onderwerp.xml)) bevatten identifiers die gecodeerd zijn conform de [TOOI-methodiek](https://tardis.overheid.nl/documentatie.html). 

Waardelijsten die specifiek voor STOP zijn, zijn te herkennen aan identifiers die beginnen met `/join/id/stop/`. Deze zijn nu ook zoveel mogelijk voorzien van een `Vindplaats` die verwijst naar de TOOI-tegenhanger. De `Vindplaats` is de waarde die gebruikt moet worden als het bronhouderkoppelvlak overschakelt op TOOI-identifiers. 

Bijvoorbeeld:

| Beschrijving | Waarde in STOP Release A | Waarde in STOP Release B |
| ---------------------------- | ---------------------------------------------------------- | ---------------------------------------------------------- |
| Ministerie van Algemene Zaken | `/tooi/id/ministerie/mnre1010` | `https://identifier.overheid.nl/tooi/id/ministerie/mnre1010` |
| Een generiek besluit | `/join/id/stop/work_003` | `https://identifier.overheid.nl/tooi/def/thes/kern/c_2307a3ff` |

**LET OP:** Aan de gebruikte identifiers in de waardelijsten kan dus *geen* betekenis toegekend worden. Dat wil zeggen: het is niet de bedoeling om in plaats van de relatie tussen STOP-identificatie en de TOOI-vindplaats te "ontdekken" hoe de relatie in elkaar zit en softwarematig de een naar de ander te vertalen. Deze vertaling zal voor een aantal identifiers afwijkend zijn; een deel van de identifiers zal in de toekomst wijzigen door de overgang naar TOOI. De STOP waardelijsten moeten gelezen worden als een relatietabel.



## Overzicht waardelijsten gebruikt door STOP

De tabel geeft voor elke waardelijst:

- element(en) die gebruik maken van betreffende waardelijst.

- de TOOI-identifier van de waardelijst

- xml-bestand met de nu nog bestaande STOP-waardelijst als link naar bestand in de [uitleveringsfolder](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten)

- corresponderende TOOI-waardelijst 

  

| Element(en) |<p>Identifier van de waardelijst, voorafgegaan door </p><p>`https://identifier.overheid.nl/tooi/set/`</p> | Waardelijstbestand(en) in uitleveringsfolder | TOOI-waardelijst |
| ------- | :-------- | :------ | ------- |
| [data:actor](data_xsd_Element_data_actor.dita#actor), [data:eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke), [data:maker](data_xsd_Element_data_maker.dita#maker), [data:uitgever](data_xsd_Element_data_uitgever.dita#uitgever), [geo:bevoegdGezag](geo_xsd_Complex_Type_geo_AmbtsgebiedType.dita#AmbtsgebiedType_bevoegdGezag) | `rwc_gemeenten_compleet` | [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml)               | [Register gemeenten compleet](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_gemeenten_compleet) |
| [data:actor](data_xsd_Element_data_actor.dita#actor), [data:eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke), [data:maker](data_xsd_Element_data_maker.dita#maker), [data:uitgever](data_xsd_Element_data_uitgever.dita#uitgever), [geo:bevoegdGezag](geo_xsd_Complex_Type_geo_AmbtsgebiedType.dita#AmbtsgebiedType_bevoegdGezag) | `rwc_provincies_compleet` | [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml)             | [Register provincies compleet](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_provincies_compleet) |
| [data:actor](data_xsd_Element_data_actor.dita#actor), [data:eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke), [data:maker](data_xsd_Element_data_maker.dita#maker), [data:uitgever](data_xsd_Element_data_uitgever.dita#uitgever), [geo:bevoegdGezag](geo_xsd_Complex_Type_geo_AmbtsgebiedType.dita#AmbtsgebiedType_bevoegdGezag) | `rwc_waterschappen_compleet` | [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml)           | [Register waterschappen compleet](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_waterschappen_compleet) |
| [data:actor](data_xsd_Element_data_actor.dita#actor), [data:eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke), [data:maker](data_xsd_Element_data_maker.dita#maker), [data:uitgever](data_xsd_Element_data_uitgever.dita#uitgever) | `rwc_ministeries_compleet` | [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml)           | [Register ministeries compleet](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_ministeries_compleet) |
| [data:formaatInformatieobject](data_xsd_Element_data_formaatInformatieobject.dita#formaatInformatieobject) | `scw_stop_formaten_informatieobjecten` | [formaatinformatieobject.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/formaatinformatieobject.xml) | [STOP formaten van informatieobjecten](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_stop_formaten_informatieobjecten) |
| [data:onderwerp](data_xsd_Element_data_onderwerp.dita#onderwerp)                         | `scw_toplijst`       | [onderwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/onderwerp.xml)             | [Thema-indeling voor Officiële Publicaties(TOP-lijst)](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_toplijst) |
| [data:overheidsdomein](data_xsd_Element_data_overheidsdomein.dita#overheidsdomein)             | `scw_bwb_themas`     | [overheidsthema.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/overheidsthema.xml)   | [Themas Basis wettenbestand](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_bwb_themas) |
| [data:publicatieblad](data_xsd_Element_data_publicatieblad.dita#publicatieblad)               | `scw_publicatiebladen` | [bekendmakingsblad.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/bekendmakingsblad.xml) | [Publicatiebladen](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_publicatiebladen) |
| [data:rechtsgebied](data_xsd_Element_data_rechtsgebied.dita#rechtsgebied)                   | `scw_bwb_rechtsgebieden` | [rechtsgebied.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/rechtsgebied.xml)       | [Rechtsgebieden Basis wettenbestand](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_bwb_rechtsgebieden) |
| [data:soortBestuursorgaan](data_xsd_Element_data_soortBestuursorgaan.dita#soortBestuursorgaan)     | `ccw_stop_bestuursorganen` | [bestuursorgaan.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/bestuursorgaan.xml)   | [STOP bestuursorganen](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fccw_stop_bestuursorganen) |
| [data:soortProcedure](data_xsd_Element_data_soortProcedure.dita#soortProcedure)               | `ccw_stop_proceduresoorten` | [soortprocedure.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortprocedure.xml)   | [STOP proceduresoorten](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fccw_stop_proceduresoorten) |
| [data:soortRegeling](data_xsd_Element_data_soortRegeling.dita#soortRegeling)                 | `ccw_stop_regelingsoorten_tpod` | [soortregeling.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortregeling.xml)     | [STOP regelingsoorten TPOD](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fccw_stop_regelingsoorten_tpod) |
| [data:soortStap](data_xsd_Element_data_soortStap.dita#soortStap)                         | `ccw_stop_stappen_besluitvorming_ontwerpbesluit` | [procedurestap_ontwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/procedurestap_ontwerp.xml) | [STOP stappen besluitvorming ontwerpbesluit](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fccw_stop_stappen_besluitvorming_ontwerpbesluit) |
| [data:soortStap](data_xsd_Element_data_soortStap.dita#soortStap)                         | `ccw_stop_stappen_besluitvorming_definitief_besluit` | [procedurestap_definitief.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/procedurestap_definitief.xml) | [STOP stappen besluitvorming definitief besluit](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fccw_stop_stappen_besluitvorming_definitief_besluit) |
| [data:soortPublicatie](data_xsd_Element_data_soortPublicatie.dita#soortPublicatie)             | `scw_publicatiesoorten` | [soortPublicatie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortpublicatie.xml) | [Publicatiesoorten](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_publicatiesoorten) |
| [data:soortWork](data_xsd_Element_data_soortWork.dita#soortWork)                         | `scw_stop_worktypes` | [soortWork.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortwork.xml)             | [STOP worktypes](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Fscw_stop_worktypes) |

De STOP-waardelijst `soortGepubliceerdwork` is niet omgezet naar een TOOI-waardelijst, maar in plaats daarvan omgezet naar een enumeratie in het [imop-consolidatie schema](cons_xsd_Element_cons_soortGepubliceerdWork.dita#soortGepubliceerdWork).

|Element |Identifier van de waardelijst | Waardelijstbestand(en) in uitleveringsfolder |
| ---- | :---- | :----- |
| [cons:soortGepubliceerdWork](cons_xsd_Element_cons_soortGepubliceerdWork.dita#soortGepubliceerdWork) | `/join/id/stop/soortgepubliceerdwork`| [soortgepubliceerdwork.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortgepubliceerdwork.xml) |

## Overheid code {#overheid}
STOP maakt in identificaties gebruik van een code voor een overheid in plaats van de JOIN identificatie of de TOOI URL. De code is het laatste onderdeel van de JOIN identificatie van de overheid. Zo is `/tooi/id/waterschap/ws0539` de identificatie van “waterschap De Dommel” en de bijbehorende code is **ws0539**. In de TOOI waardelijsten is de code als _Organisatiecode_ terug te vinden onder de TOOI URL van de overheid.

Voorbeelden van overheid codes:

| Code     | uit waardelijst                                              | Voorbeeld in identificatie             |
| -------- | ------------------------------------------------------------ | -------------------------------------- |
| gm0638   | [https://identifier.overheid.nl/tooi/id/gemeente](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_gemeenten_compleet) | /akn/nl/bill/**gm0638**/2020/REG0001   |
| pv30     | [https://identifier.overheid.nl/tooi/id/provincie](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_provincies_compleet) | /akn/nl/doc/**pv30**/2020/kennisgeving |
| mnre1109 | [https://identifier.overheid.nl/tooi/id/ministerie](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_ministeries_compleet) | /akn/nl/act/**mnre1109**/2020/INST0021 |
| ws0663   | [https://identifier.overheid.nl/tooi/id/waterschap](https://tardis.overheid.nl/waardelijst?work_uri=https%3A%2F%2Fidentifier.overheid.nl%2Ftooi%2Fset%2Frwc_waterschappen_compleet) | /akn/nl/act/**ws0663**/2020/REG0001    |
