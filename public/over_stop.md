# Over de standaard

Bij het opstellen van de Standaard Officiële Publicaties zijn verschillende (architectuur)uitgangspunten gehanteerd. In dit deel van de documentatie wordt daar op ingegaan.

- Het [toepassingsgebied](toepassingsgebied.md) beschrijft in het kort de domeinen waarvoor de standaard is opgesteld.
