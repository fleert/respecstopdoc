# C3. Besluitmutatie van Artikel

Om een (spel)fout in een artikel van een gepubliceerd besluit te corrigeren moet de artikel tekst aangepast worden. In deze beschrijving is de aanname dat de tekstuele wijziging(en) in het besluit geen gevolgen hebben voor eventuele regeling(en) die door het besluit ingesteld of gemuteerd worden en dat er verder ook geen impact is op de Besluit metadata, consolidatieinformatie of het procedureverloop van het besluit. De rectificatie bevat dan de volgende modules:



## Gecorrigeerde regeling

De aanname is dat de gecorrigeerde artikelen geen betrekking hebben op eventuele regeling(en) die door het besluit ingesteld of gemuteerd worden. Dus deze rectificatie levert geen STOP modules die betrekking hebben op de regeling mee.



## Gecorrigeerde besluitmodules

De correctie(s) hebben gevolgen voor de met het besluit meegeleverde informatie. Om het besluit te corrigeren is nodig:
- [ProcedureverloopMutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie): Geen aanpassingen, niet meeleveren.

- [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie): Geen aanpassingen, niet meeleveren.

- [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata): Geen aanpassingen, niet meeleveren.
  
  

## De rectificatie zelf

De rectificatie zelf bestaat uit:

* [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van de rectificatie zelf. De rectificatie van een besluit is een zelfstandig Work en elke rectificatie bij hetzelfde besluit is een nieuwe expressie van het rectificatie Work;

* [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata): Net als bij een besluit moet er bij een rectificatie altijd metadata aangeleverd worden. De RectificatieMetadata lijkt erg op de BesluitMetadata, maar heeft een extra veld [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert);

* [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie): De rectificatie bestaat uit:
  * Een [regeling opschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift), bijv. 'Rectificatie van de Verordening Toeristenbelasting 2017'

  * Het [lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met één of meerdere:
    * [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)(en) met 
      * een [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop);
      * een of meer toelichtende [alinea](tekst_xsd_Element_tekst_Al.dita#Al)'s;
      * een [Besluitmutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie).
      
        De besluitmutatie lijkt erg op een regelingmutatie, maar wijzigt de besluittekst. De *was* van de mutatie is het originele besluit. De *wordt* is het besluit wat *zou* ontstaan na verwerking van de wijzigingen uit de rectificatie. *Zou* omdat de besluittekst zelf niet geconsolideerd wordt. LET OP: er worden voor de rectificatie wel afwijkende standaard zinnen gebruikt om de [*wat*](tekst_xsd_Element_tekst_Wat.dita#Wat) aan te duiden.

Zie voor een voorbeeld de [STOP-codering](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/C3-Besluitmutatie-EnigArtikel.xml) van de _'Rectificatie van het Besluit van 20 november 2019 tot vaststelling van het tijdstip van inwerkingtreding van Wet afwikkeling massaschade in collectieve actie en het Besluit register collectieve acties'_ uit de [voorbeelden uit de staande praktijk](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/index.md).