# Beroep

Als een burger het niet eens is met de uitkomst van de [bezwaar](bezwaar.md)-procedure, kan hij in beroep. Dit kan ook als de beslissing op het bezwaarschrift niet binnen de geldende termijn is afgehandeld en het bestuursorgaan in gebreke is gesteld. Daarnaast is het soms niet mogelijk in bezwaar te gaan en staat beroep meteen open. 
Iemand kan in de meeste gevallen in beroep gaan bij de rechtbank. In sommige gevallen is er een andere instantie waar iemand beroep moet instellen, zoals de Afdeling bestuursrechtspraak van de Raad van State of het College van Beroep voor het bedrijfsleven. 
Voor het instellen van beroep moet iemand een beroepschrift indienen. In het beroepschrift wordt uitgelegd waarom iemand het niet eens bent met de beslissing van een bestuursorgaan. Ook moet er worden aangegeven wat de beslissing van de rechter zou moeten zijn. 
