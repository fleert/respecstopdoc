# Officiële publicaties

Op basis van de [Bekendmakingswet](https://wetten.overheid.nl/BWBR0004287/2021-07-01) is een bevoegd gezag verplicht om besluiten en ook bepaalde informatie over besluiten te publiceren. Deze officiële publicaties moet het bevoegd gezag in het eigen publicatieblad (zoals gemeenteblad of staatscourant) doen. De publicatiebladen worden digitaal ontsloten via [officielebekendmakingen.nl](https://www.officielebekendmakingen.nl/).  Er bestaan drie categorieën publicaties:

1. Bekendmakingen van [besluiten en regelingen](stop_gebruik.md) inclusief rectificaties,
2. Kennisgevingen van besluiten met informatie over de besluitprocedure,
3. Mededelingen, alle overige publicaties met 'eigen' inhoud.

STOP schrijft voor hoe de informatie die nodig is voor officiële publicaties gestructureerd moet worden om deze te kunnen publiceren. 

De benodigde informatie valt uiteen in:

Modellering van de inhoud (tekst + aanvullende informatie)
: Voor elke categorie bevat STOP een of meer modellen. Hier wordt ingegaan op de modellen voor [kennisgevingen en mededelingen](publicatie_generiek.md). Eerder zijn de modellen voor [besluiten en regelingen](stop_gebruik.md) toegelicht. 

Modellering van attenderingsinformatie
: Bij een deel van de publicaties is het bevoegd gezag verplicht om extra informatie mee te leveren ten behoeve van het [attenderen](attenderen.md) van betrokkenen en geïnteresseerden op de publicatie. 

Modellering van de publicatie
: STOP biedt ook een model voor de publicatie zoals die in het [publicatieblad](publicatieblad.md) verschijnt.



*Voorbeeld van een officiële publicatie in het Staatsblad:*

![](img/PublicatieOmgevingswet.png)