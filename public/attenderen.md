# Attenderen

De Regeling elektronische publicaties vereist (zie [artikel 4.1](https://wetten.overheid.nl/jci1.3:c:BWBR0045086&hoofdstuk=4&artikel=4.1&z=2022-01-01&g=2022-01-01)) dat iedereen van vijfentwintig jaar en ouder geattendeerd wordt op bekendmakingen, mededelingen en kennisgevingen die betrekking hebben op een locatie in de buurt. Deze attendering verloopt via de Berichtenbox van [mijn.overheid.nl](https://mijn.overheid.nl). Ook op [Overheid.nl/berichten-over-uw-buurt](https://www.overheid.nl/berichten-over-uw-buurt) kan op basis van een adres en een straal gezocht worden naar besluiten "in de buurt". 

[Gebiedsmarkering](gebiedsmarkering.md)
: Om te kunnen bepalen op welke locatie de inhoud van een publicatie betrekking heeft, moet de publicatie voorzien worden van een gebiedsmarkering. 

[Effectgebied](effectgebied.md)
: Besluiten kunnen soms gevolgen hebben die op grote afstand merkbaar zijn. In dergelijke situaties moet een publicatie over het besluit naast de gebiedsmarkering, ook voorzien worden van een effectgebied.

[Gebiedsmarkering berekenen](gebiedsmarkering_berekenen.md)
: De gebiedsmarkering die hoort bij een besluit kan afgeleid worden uit de werkingsgebieden van de regelingen die in het besluit gewijzigd of vastgesteld worden. 