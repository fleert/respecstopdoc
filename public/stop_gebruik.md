# Besluiten en regelingen

<!--De sectie **Gebruik** volgt het "geïntegreerde proces van bekendmaken en consolideren". Dat proces hoeft niet meer expliciet gemaakt te worden, want het volgt uit de indeling van de sectie. Het juridische deel ervan staat al in de juridische context. Alle onderwerpen in **Gebruik** moeten zelfstandig leesbaar zijn.-->

Hoe wordt de STOP standaard gebruikt voor het uitwisselen van informatie. Onderstaande onderwerpen staan op volgorde van het proces van de totstandkoming van nieuwe regelgeving.
<!--(FRO: ik denk dat het niet zinvol is om nog te proberen het complete proces
te tekenen. Het abstractieniveau is noodgedwongen vrij hoog omdat we in STOP
niet kunnen ingaan op de BG-specifieke processen - het rijk werkt heel anders dan een gemeente. Voor STOP is het waarschijnlijk genoeg om dit hoofdstuk als een stappenplan te presenteren en voor elk stapje uit te leggen waar het voor dient. BG's, -leveranciers en TP-schrijvers moeten het stappenplan omsmeden naar een proces. De belangrijkste boodschap is: het draait er steeds om om te komen tot één geldende versie van de regeling op elk moment, daarbij spelen verschillende zaken naast elkaar waar vaststelling van de besluiten er maar één is. Dat zit wel voldoende in het stappenplan en staat ook bij de uitgangspunten uitgelegd.)-->

_Start van een project_
: Voordat gestart kan worden met het schrijven van nieuwe regelgeving, moeten eerst enkele randvoorwaarden ingericht worden, zodat het versiebeheer direct vanaf de [start van het project](versiebeheer_in_stop.md) goed verloopt.

_Regeling_
: Een regeling bestaat uit tekst, annotaties en informatieobjecten (voor niet in tekst uit te drukken aspecten van de regeling). Wat is mogelijk en welke beperkingen gelden bij het opstellen van een [regeling](regeling.md)? 

_Samenwerken aan regelgeving_
: Vaak wordt het opstellen van regelgeving (deels) uitbesteed aan adviesbureaus en soms komt een initiatiefnemer zelf met een voorstel voor aanpassingen in de regelgeving. STOP bevat faciliteiten voor versiebeheer en het uitwisselen van concept regelingen, zodat [samenwerken aan regelgeving](samenwerken_aan_regelgeving.md) mogelijk is.

_Opstellen van een besluit_
: Nieuwe regelgeving kan alleen worden ingesteld door een besluit hiertoe van het bevoegd gezag. [Opstellen van een besluit](besluit.md) beschrijft hoe een besluit in STOP geformuleerd moet worden.

_Inspraak en vaststelling_
: De Algemene wet bestuursrecht ([Awb](juridische-uitgangspunten.md#algemene-wet-bestuursrecht-awb)) kent vaste procedures voor [inspraak en vaststelling](inspraak_en_vaststelling.md) van besluiten. Zo moeten bepaalde besluiten voorafgegaan worden door een ontwerpbesluit dat ter inzage moet worden gelegd. Publicatie van een kennisgeving van het voorgenomen besluit met de termijnen voor inzage en de wijze waarop zienswijze ingediend kunnen worden, is dan verplicht. In dat geval kan het besluit pas na het verstrijken van die termijnen en het verwerken van de ingediende zienswijzen definitief vastgesteld worden.

_Geldende regelgeving: consolidatie_
: Pas na publicatie wordt een besluit geldig en kan het in werking treden. Het bevoegd gezag wordt door de Bekendmakingswet ([artikel 19.1](https://wetten.overheid.nl/jci1.3:c:BWBR0004287&artikel=19&z=2021-07-01&g=2021-07-01)) verplicht om de geldende regelgeving te publiceren. Bepalen hoe de geldende regelgeving luidt of komt te luiden na inwerkingtreding van een besluit heet [consolideren](consolideren.md). 

_Samenhangende besluiten/regelingen_
: Als een bevoegd gezag over meerdere wijzigingen in één regeling, of over één wijziging met consequenties voor meerdere regelingen wil besluiten, kan hetzelfde resultaat vaak op verschillende juridische manieren tot stand komen. Welke manier heeft welke consequenties bij het nemen van [samenhangende besluiten](samenhangende_besluiten.md)?

_Meerdere BG_
: Een bevoegd gezag kan gerechtigd zijn om wijzigingen aan te brengen in een regeling van een ander bevoegd gezag. Op welke manieren kan dit? Hoe worden besluiten waar [meerdere BG](meerdere_BG.md) bij betrokken zijn in STOP gecodeerd?

_Rectificatie en revisie_
: Wat als niet het goede is gepubliceerd? Bij het aanpassen van juridische informatie spreekt men van [rectificatie en revisie](rectificatie_en_revisie.md) heet het aanpassen van niet-juridische informatie of niet-juridische delen van de tekst.

_Beroepsprocedures_
: Sommige besluiten zijn vatbaar voor beroep. Een besluit, en daarmee dus de wijziging van een regeling, kan naar aanleiding van een beroep bijvoorbeeld geschorst of vernietigd worden door een rechter. Om bij de geconsolideerde regeling weer te kunnen geven welke delen definitief zijn en welke nog onder de rechter, geeft het bevoegd gezag bepaalde informatie door over [beroepsprocedures](besluitproces_rechterlijkemacht.md).

_Bron van geldende regelgeving_
: Om te kunnen achterhalen hoe geconsolideerde regelgeving is ontstaan moet juridische verantwoording worden samengesteld. De juridische verantwoording laat zien uit welk besluit elk deel van de geconsolideerde regeling afkomstig is. De gerechtelijke procedurestatus toont daarnaast relevante informatie uit eventuele beroepsprocedures. Samen vormen zij de [bron van de geldende regelgeving](bron_regelgeving.md).

_Regelgeving in de loop van de tijd_
: Welke versie van een regeling was geldig op welk moment? Wanneer was de inwerkingtreding van een besluit en vanaf welke datum is het besluit onherroepelijk? Er zijn diverse tijdreisvragen te stellen. STOP kent verschillende tijdstempels waardoor de [regelgeving in de loop van de tijd](regelgeving_in_de_tijd.md) te reconstrueren is.