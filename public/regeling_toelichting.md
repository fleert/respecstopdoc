# Toelichting

Een regeling kan een toelichting bevatten, die steeds samen met de artikelen wordt bijgewerkt. De standaard kent verschillende mogelijkheden om de toelichting te structureren. Met behulp van een annotatie kan een relatie gelegd worden tussen de toelichting en wat er toegelicht wordt.

Een *regeling* volgens het [klassieke model](regeling-klassiek.md) kent zelf geen toelichting, een *besluit* volgens het klassieke model [wel](besluit_toelichting.md#klassiek). 

## Tekst

De tekst van de toelichting kent een van drie vormen:

![Modellering in STOP](img/Regeling_Toelichting.png)

* Als de toelichting alleen uit een algemene toelichting bestaat, dan wordt het element [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) gebruikt. Het element [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) dient alleen als container voor de algemene toelichting en eventuele bijlagen en is niet zichtbaar in de tekst.

* Als de toelichting alleen uit een toelichting bestaat die in detail ingaat op de formulering van (de wijziging van) regelingen of informatieobjecten, dan wordt het element [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) gebruikt.  Het element [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) dient alleen als container voor de artikelgewijze toelichting en eventuele bijlagen en is niet zichtbaar in de tekst.

* Als de toelichting uit zowel een algemene als gedetailleerde toelichting bestaat, dan worden beide elementen [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) en [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) gebruikt. Het element [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) dient als container voor deze elementen en eventuele bijlagen, en is zichtbaar als een [structuurelement](regeltekst.md) met een eigen kop. De elementen [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) en [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) staan als structuurelement een hiërarchisch niveau dieper.

## Toelichtingsrelaties

Als een regeling een [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) bevat, dan kan als [annotatie](annotaties.md) bij de regeling de module [Toelichtingsrelaties](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties) toegevoegd worden. De module bevat informatie om:

* bij de tekst van de regeling de relevante teksten uit de artikelsgewijze toelichting te tonen;
* bij de artikelsgewijze toelichting (verwijzingen naar) de tekst van de regeling te tonen en eventueel ook naar informatieobjecten die in de toelichting beschreven staan.

De module bevat dus een machine-leesbare representatie van het onderwerp van de toelichting: een deel van de regelgeving of een informatieobject. Die informatie kan niet verkregen worden door de tekst van de toelichting te scannen op [IntRef](tekst_xsd_Element_tekst_IntRef.dita#IntRef) of [ExtRef](tekst_xsd_Element_tekst_ExtRef.dita#ExtRef). Een verwijziging in de tekst kan immers gebruikt worden voor een "zie ook" die naar iets anders dan het onderwerp van de toelichting.

![Toelichtingrelaties voor een regeling](img/Regeling_Toelichtingrelaties.png)

De module [Toelichtingsrelaties](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties) bevat één of meer [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelatie.dita#Toelichtingsrelatie) elementen die een relatie leggen tussen een gedetailleerde toelichting en hetgeen toegelicht wordt. De ene kant van de relatie is een divisie of divisietekst in de artikelsgewijze toelichting, de andere kant een deel van de regelgeving:

* Een inhoudelement (artikel of lid) uit de tekst van de regeling.
* Een structuurelement (bijvoorbeeld hoofdstuk) uit de tekst van de regeling.
* Een informatieobject als geheel waarnaar verwezen wordt met een [ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) vanuit de tekst van de regeling
* Voor een GIO: een [GIO-deel](gio-met-delen.md).

De toelichtingrelaties werken als een filter. Als bijvoorbeeld een toelichting bij een fragment van de regeltekst getoond moet worden, dan wordt voor elk tekstelement uit het regeltekst-fragment nagegaan welke divisies en divisieteksten uit de toelichting via een toelichtingrelatie gerelateerd zijn. Daarmee wordt een deel van de volledige tekst van de toelichting geselecteerd, en die moet getoond worden. De toelichting op een structuurelement wordt bij de kop (of het begin) van het structuurelement getoond. In de illustratie:

| Fragment                   | Te tonen toelichting     |
| -------------------------- | ------------------------ |
| Regeling tekst #1          | Toelichting #1           |
| Regeling tekst #3          | Toelichting #4 en #5     |
| Regeling tekst #2 (kop)    | Toelichting #3           |
| Regeling tekst #2 (geheel) | Toelichting #3, #4 en #5 |

Als bij de toelichting (verwijzingen naar) de tekst en/of informatieobjecten getoond worden:

| Toelichting    | Onderwerp                           |
| -------------- | ----------------------------------- |
| Toelichting #1 | Regeling tekst #1                   |
| Toelichting #2 | -                                   |
| Toelichting #3 | Regeling tekst #2 (kop)             |
| Toelichting #4 | Regeling tekst #3, informatieobject |
| Toelichting #5 | Regeling tekst #3                   |

De toelichtingrelaties houden dus geen rekening met hiërarchie in de tekst of informatieobject. Als een structuurelement het onderwerp is van een toelichting en een inhoudelement staat in de tekst binnen het structuurelement, dan is het inhoudelement daarmee niet automatisch ook het onderwerp van de toelichting - als dat wel zo is, moet dat expliciet aangegeven worden. Als een GIO het onderwerp is van een toelichting en het GIO heeft GIO-delen, dan zijn de GIO-delen niet automatisch ook het onderwerp van de toelichting.

Bij het opstellen van de toelichtingrelaties moet wel rekening gehouden worden met de hiërarchie in de toelichting. Stel dat in de illustratie hieronder Toelichting #2 een hoofdstuk is dat de toelichting op de inhoud van Regeling tekst #2 bevat en dat begint met een toelichting (#3) op de inhoud van het hele structuurelement. Het lijkt voor de hand te liggen Toelichting #2 te relateren aan Regeling tekst #2.

![Toelichtingrelatie met onbedoeld effect](img/Regeling_Toelichtingrelaties_fout.png)

Maar dat heeft als effect dat bij de kop van het structuurelement Regeling tekst #2 de hele inhoud van Toelichting #2 wordt getoond. Dat is niet de bedoeling: alleen Toelichting #3 zou getoond moeten worden.

Hetzelfde speelt ook bij een toelichting op een GIO met GIO-delen. Als er een hoofdstuk is met een toelichting op het GIO, een tekst over het GIO als geheel en afzonderlijke teksten over de GIO-delen, dan moet de tekst over het GIO als geheel gerelateerd worden aan het GIO en niet het hele hoofdstuk.


## Implementatiekeuze

Het [schema](tekst_xsd_Main_schema_tekst_xsd.dita#tekst.xsd) voor de XML codering van toelichting en motivering kent om historische redenen meerdere manieren om de toelichting en motivering in XML op te nemen. In dit hoofdstuk is de voorkeurs-codering gebruikt die zeker in de volgende versies van de standaard ondersteund zal worden. De landelijke voorzieningen (LVBB, DSO-LV) ondersteunen ook de andere coderingen, maar in een toekomstige versie van de voorzieningen kan die ondersteuning beëindigd worden. 

Het gebruik van de informatie uit de _Toelichtingsrelaties_ module door een systeem dat deze informatie ontvangt is niet verplicht. Zie de documentatie van het betreffende systeem voor informatie over of en hoe de module gebruikt wordt.
