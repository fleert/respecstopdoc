# Gedeeltelijke inwerkingtreding

**Definitie**

Een regeling treedt gedeeltelijk in werking als een inwerkingtredingsbesluit niet de hele, maar slechts een deel van de regeling in werking stelt.

**Voorkom gedeeltelijke inwerkingtreding**

Het [uitgangspunt](versiebeheer_gebruik.md#Gefaseerd) in STOP is dat als van tevoren bekend is dat een regeling of wijziging op twee momenten in werking zal treden, er regelingversies voor twee doelen gemaakt worden. Een besluit als in dit [voorbeeld](https://zoek.officielebekendmakingen.nl/stb-2021-25.html) zal in STOP dus als een besluit met twee wijzigartikelen, elk voor een ander doel, opgesteld worden.

Niet op STOP gebaseerde regelingen worden nog wel eens als één regelingversie ingesteld, terwijl vanaf het begin bekend is dat op een later tijdstip over de inwerkingtreding van onderdelen wordt besloten. Dat is in STOP een veel moeizamere methode omdat het lastig is een versie (of wijziging) te splitsen. Niet voor de tekst van de regeling, maar wel voor de annotaties (in STOP en andere zoals IMOW), toepasbare regels, etc. Bij gedeeltelijke inwerkingtreding moet voor elke annotatie opnieuw bekeken worden waar de annotatie betrekking op heeft en hoe de annotatie geraakt wordt door de gedeeltelijke inwerkingtreding.

**Soms is gedeeltelijke inwerkingtreding niet te voorkomen**

Er zijn echter situaties waarbij vooraf niet duidelijk is welke delen wanneer in werking zullen treden. Ook kan een regeling die bedoeld was integraal in werking te treden achteraf toch gefaseerd ingevoerd moeten worden. Bijvoorbeeld omdat noodzakelijke organisatie- of ICT-aanpassingen gerealiseerd moeten zijn voordat een bepaald deel van de regeling ingevoerd kan worden. Het kan dus voorkomen dat een besluit met de integrale versie van een regeling wordt vastgesteld zonder inwerkingtredingsdatum (en dus niet meer aangepast kan worden op latere inzichten). Bij het formuleren van het inwerkingtredingsbesluit blijkt dan dat niet het hele besluit in werking kan treden. De ondersteuning van de gedeeltelijke inwerkingtreding in STOP is gericht op dit soort situaties.

**Extra informatie nodig**

Het is gebruikelijk om bij een gedeeltelijke inwerkingtreding in het inwerkingtredingsbesluit een opsomming te geven welke onderdelen (niet) in werking treden. Dit is bij gebruik van STOP nog steeds het geval. STOP kent geen "renvooi voor wel/niet inwerkingtreding" zoals er wel renvooi voor het aangeven van wijzigingen is. Nadeel van deze constructie is dat de juridische tekst die aangeeft wat wel of niet in werking treedt, niet op een voor software begrijpelijke manier gecodeerd kan worden.

Bij een besluit dat een gedeeltelijk inwerkingtreding regelt moet daarom, naast de juridische tekst van het besluit, ook de resulterende tekst van de regelingversie meegeleverd worden waarin is aangegeven welke delen nog niet in werking treden. Ook de annotaties die bij die versie horen moeten uitgewisseld worden. Als de resulterende tekst geen onderdeel van de tekst van het besluit is omdat er geen aanvullende wijzigingen nodig zijn, wordt het als [Tekstrevisie](tekst_xsd_Element_tekst_Tekstrevisie.dita#Tekstrevisie) onderdeel van het besluit.

Het is de verantwoordelijkheid van het bevoegd gezag, al dan niet ondersteund door software, om te zorgen dat de opsomming van de onderdelen die niet in werking treden en de regelingversie met het resultaat daarvan overeenkomen.

## Werkwijze gedeeltelijke inwerktreding

### De regelingversie die in werking treedt

Bij een gedeeltelijke inwerkingtreding stelt het BG eerst de regelingversie samen zoals deze in werking moet treden. Daarbij worden de delen van de regeling die nog niet in werking treden vervangen door [tekst:NogNietInWerking](tekst_xsd_Element_tekst_NogNietInWerking.dita#NogNietInWerking).

Als er aanvullende wijzigingen nodig zijn vanwege de gedeeltelijke inwerkingtreding, dan wordt de regelingversie vervolgens bijgewerkt. Er kunnen op deze manier dus geen wijzigingen aangebracht worden in de onderdelen die nog niet in werking treden. 

Van de regelingversie die in werking treedt wordt een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) gemaakt met de volledige regelingversie als basisversie/was-versie, zoals beschreven in **TODO** link naar andere besluit-pagina.

STOP ondersteunt geen gedeeltelijke inwerkingtreding van (een wijziging van) een informatieobject. Als dat vereist is, dan moet een nieuwe informatieobjectversie gemaakt worden en moet vanuit de tekst van de regeling naar die nieuwe verwezen worden.

### Opstellen besluit met gedeeltelijk inwerkingtreding

In het juridische deel van het besluit wordt een bepaling opgenomen die aangeeft welke delen van de regeling in werking treden. Er zijn nu twee mogelijkheden:

Als er geen aanvullende wijzigingen in de tekst aangebracht zijn, dan wordt de [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) in een [tekst:Tekstrevisie](tekst_xsd_Element_tekst_Tekstrevisie.dita#Tekstrevisie) module opgenomen en bij het besluit gevoegd. Deze tekst wordt niet getoond als onderdeel van de tekst van het besluit en is alleen bedoeld voor machinale verwerking.

Als er wel aanvullende wijzigingen in de tekst zijn aangebracht, dan wordt de [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) als een reguliere wijziging in de tekst van het besluit opgenomen worden. Bij de weergave van de RegelingMutatie in het besluit worden alleen de juridische wijzigingen getoond, bijvoorbeeld:

![](img/GedeeltelijkeIWTrenvooi-s3.png)

Het kan voorkomen dat het deel dat niet in werking treedt toch in de tekst te zien is. Bijvoorbeeld als lid 2 van artikel 2.2 nog niet in werking treedt en het artikel tevens gewijzigd wordt. Het niet in weking getreden deel heeft niet de markering van renvooi:

![](img/GedeeltelijkeIWTrenvooi-s4.png)

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het besluit moet een [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) element opgenomen worden voor de nieuwe regelingversie, en een [BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject) voor de nieuwe informatieobjecten.

Voor regelingen en informatieobjecten die in het oorspronkelijke besluit gewijzigd zouden worden maar door de gedeeltelijke inwerkingtreding ongewijzigd blijven, moet een [TerugtrekkingRegeling](data_xsd_Element_data_TerugtrekkingRegeling.dita#TerugtrekkingRegeling) resp. [TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) opgenomen worden.

### Inwerkingtreding resterende deel

Om het resterende deel in werking te laten treden, wordt een nieuwe branch gemaakt met als basisversie de volledige regeling. Zie ook de beschrijving van [gedeeltelijke inwerkingtreding](consolideren_patronen_1iwt.md#gedeeltelijkeiwt) bij consolidatie patronen.

Als er geen aanvullende wijziging nodig is, dan hoeft de volledige tekst van de regeling niet opnieuw uitgewisseld te worden. De tekst is immers al onderdeel van een eerder bekendgemaakt besluit. De regelingversie hoeft alleen in een [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) element in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module opgenomen te worden. Dat geldt ook voor eventuele informatieobjectversies en wijzigingen van andere regelingen die nog in werking moeten treden.

## STOP voorbeelden
* [regeling zonder inwerkingtreding](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/03%20Initiele%20regeling%20met%20gio%20zonder%20iwt)
* [inwerkingtreding volledige regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/04%20Inwerkingtreding)
* [gedeeltelijke inwerkingtreding](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/05%20Gedeeltelijke%20inwerkingtreding)
* [inwerkingtreding resterende delen](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/06%20Resterende%20inwerkingtreding)
* [gedeeltelijke inwerkingtreding gecombineerd met wijzigingen](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/07%20Gedeeltelijke%20inwerkingtreding%20met%20wijzigingen)



