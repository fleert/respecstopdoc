# Pons-GIO

Een GIO kan ook worden gebruikt als [pons](pons.md). Een pons-GIO is een werkingsgebied-GIO dat niet wordt gebruikt om het werkingsgebied van juridische regels in een _regeling_ te beschrijven, maar van een regeling als geheel. Met een pons wordt bijvoorbeeld aangegeven dat een bestemmingsplan dat onder de Wro tot stand is gekomen niet langer van toepassing is voor een bepaald gebied. De DSO-LV-viewer zal ter plaatse van de pons geen bestemmingsplannen tonen. Zie voor een toelichting op de bestaansreden van de pons ook de [juridische context over pons](pons.md).

## Wanneer de pons te gebruiken?
De pons wordt dus gebruikt als er delen van bestemmingsplannen vervallen. Per omgevingsplan is er één Pons. Deze Pons wordt in de loop van de tijd gemuteerd. Met iedere mutatie wordt de Pons uitgebreid, net zolang tot alle bestemmingsplannen van de gemeente zijn vervallen. Daarna kan het Pons-GIO worden ingetrokken en kan het OW-object Pons beëindigd worden. 

Als het *hele* bestemmingsplan vervalt, wordt *naast* de aanpassing van de pons, ook het bestemmingsplan uit het Wro-manifest van de gemeente verwijderd. 


## Verwijzen naar de pons
De pons wordt in STOP opgenomen als een GIO bij een besluit. In de besluittekst moet verwezen worden naar de pons, zoals bijvoorbeeld:

> **Artikel 5**. Door dit besluit vervallen de delen van de bestemmingsplannen Veluwe en Uddel die zijn aangegeven met de bij dit besluit behorende pons met de identificatie /join/id/regdata/gm1234/…

De pons wordt alleen in het besluit genoemd, in het omgevingsplan zelf dus *niet*.

## Pons als GIO bij het besluit
Het GIO dat wordt gebruikt als pons hoort net als andere GIO's bij het besluit, maar is geen onderdeel van het geconsolideerde omgevingsplan. De [geboorteregeling](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) van het pons-GIO is wel het omgevingsplan, omdat in de informatiehuishouding de pons iets zegt over non-STOP-regelingen (bestemmingsplannen) die van rechtswege deel uitmaken van het omgevingsplan.

Het pons-GIO heeft net als andere GIO's een landingspagina en deze GIO wordt ook geconsolideerd, zodat op elk moment duidelijk is welke pons van toepassing is.

## Onderdelen pons
Voor de onderdelen van een GIO-pons, zie [GIO als werkingsgebied](gio-werkingsgebied.md).

## Voorbeeld

Er is een [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/05%20Besluit%20met%20Pons) beschikbaar van een besluit met een pons.

