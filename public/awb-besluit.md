# Besluiten volgens de Awb

Onder besluit wordt verstaan: een schriftelijke beslissing van een bestuursorgaan, inhoudende een publiekrechtelijke rechtshandeling.
Een [besluit](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=1&titeldeel=1.1&artikel=1:3&z=2021-04-01&g=2021-04-01) is een juridisch kernbegrip uit de [Awb](https://wetten.overheid.nl/BWBR0005537/2021-04-01). Dat iets een besluit is, is relevant omdat dat betekent dat het een rechtsgevolg heeft voor een burger, bedrijf of andere overheid. Zo kan in sommige gevallen een burger die het niet eens is met een besluit hier tegen in bezwaar of beroep kan gaan.

Een besluit:
* moet altijd schriftelijk zijn,
* kan alleen worden genomen door een bestuursorgaan. Een bestuursorgaan is een orgaan van een rechtspersoon die krachtens publiekrecht is ingesteld of een ander persoon of college, met enig openbaar gezag bekleed, zie art. 1.1 Awb,
* creëert een recht of een plicht,
moet zijn gebaseerd op wet- of regelgeving die iets regelt tussen de overheid en de burger.

Besluiten kunnen algemeen of specifiek zijn. Een algemeen besluit is een 'besluit van algemene strekking'. Zo'n besluit is van toepassing op een niet bepaalde groep gevallen. Een voorbeeld hiervan is een omgevingsplan.

Een specifiek besluit heet een 'beschikking'. Een beschikking is op een of meer individuen of individuele situaties gericht. Een voorbeeld hiervan is een omgevingsvergunning. In de STOP-documentatie wordt er soms gesproken van een 'STOP-besluit'. Een [STOP-besluit](proces_besluit_en_regeling.md) is geen juridisch begrip uit de Algemene wet bestuursrecht.
