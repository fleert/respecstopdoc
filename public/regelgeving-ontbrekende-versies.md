# Ontbrekende inhoud voor een toestand

## Alternatieve beschrijving van de inhoud
Bij het samenstellen van [actuele toestanden](consolideren_toestanden_bepalen.md) en van de [complete toestanden](tijdreizen-toestanden.md) is geconcludeerd dat de toestanden van een geconsolideerde regeling of informatieobject altijd uit te rekenen zijn, maar dat niet altijd te bepalen is wat de inhoud van een toestand is: de tekst van de regeling of de data van het informatieobject. Dat wil zeggen: er is geen uitgewisselde regelingversie of informatieobjectversie aan te wijzen die de ingoud van de toestand weergeeft. de modellering van (een wijziging van) een regeling of informatieobject in STOP is niet zodanig dat de ontbrekende versie betrouwbaar geautomatiseerd bepaald kan worden; soms ontbreekt daar ook de benodigde informatie voor.

Voor documentgerichte STOP-gebruikende systemen is het wel mogelijk om een menselijke gebruiker te informeren wat de inhoud is van een toestand waarvoor geen regeling-/informatieobjectversie is uitgewisseld. De gebruiker krijgt dan een regeling-/informatieobjectversie te zien die wel is uitgewisseld met daarbij een lijst publicaties waarin beschreven staat welke wijzigingen nog niet in de getoonde versie zijn verwerkt. De gebruiker zal zelf het effect van de wijzigingen op de regeling moeten bepalen.

## Werkwijze

Uitgangspunt is de uitgewisselde informatie in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) modules. Bij [tijdreizen](regelgeving_in_de_tijd.md) betreft het de selectie uit de beschikbare informatie in overeenstemmoing met de tijdreis. Met elke element in de `ConsolidatieInformatie` waarvoor een `eId` is opgegeven corrspondeert een [bladwijzer](cons_xsd_Element_cons_Bladwijzer.dita#Bladwijzer) in de [juridische verantwoording](bron_regelgeving.md); meerdere elementen kunnen bij dezelfde officiële publicatie in het publicatieblad van het bevoegd gezag horen.

De werkwijze is het best uit te leggen aan de hand van een (niet helemaal realistisch) voorbeeld ([legenda](consolideren_patronen.md)):

![Voorbeeld van publicaties](img/consolideren_ontbrekende_versies.png)

De publicaties (elementen met een `Bladwijzer`) zijn aangeduid met een P-nummer, de overige met R-nummer. Publicatie P8 betreft de vernietiging van het besluit uit P5. Als de doelen A,B,C,E,F opeenvolgend in werking treden, dan hebben de toestanden met inwerkingtredingsdoel C, E en F geen inhoud.

Voor een toestand kan de laatst aangeleverde versie voor het inwerkingsdoel gebruikt worden plus nog te verwerken publicaties, maar ook de versie van een eerdere toestand plus alle publicaties van de overige doelen. Voor het bepalen van de ontbrekende publicaties voor de laatsts uitgewisselde versie voor een doel (of combinatie van doelen bij gelijktijdige inwerkingtreding) wordt gekeken welke publicaties ervoor zorgen dat een voorganger als niet-bijgewerkt gemarkeerd wordt. Revisies tellen niet mee. Het begrip voorganger is uitgelegd bij het [bepalen van toestanden](consolideren_toestanden_bepalen.md).

Voor de toestand met inwerkingtredingsdoel C: kan gekozen worden uit:

1.  Versie uit P4 voor doel C, nog te verwerken publicaties: P3
2.  Versie uit R1 voor doel B, nog te verwerken publicaties: P4
3.  Versie uit P3 voor doel A, nog te verwerken publicaties: P2, P4

De keuze voor de optie met zo min mogelijk nog te verwerken publicaties ligt voor de hand. Maar dat is niet altijd voldoende: zowel optie 1 als 2 voldoet. Er zijn geen objectieve criteria te geven om te kiezen tussen deze twee opties. Het is aan het STOP-gebruikend om met nadere criteria te komen.

Voor de toestand met inwerkingtredingsdoel E is wel te kiezen:

1.  Versie uit P8 voor doel E, nog te verwerken publicaties: P3
2.  Versie uit P4 voor doel C, nog te verwerken publicaties: P3, P5, P6, P8
3.  ... (opties met tenminste evenveel nog te verwerken publicaties)

Voor de toestand met inwerkingtredingsdoel F weer niet:

1.  Versie uit P7 voor doel F, nog te verwerken publicaties: P3, P8
2.  Versie uit P8 voor doel E, nog te verwerken publicaties: P3, P7
3.  ... (opties met meer nog te verwerken publicaties)

## Uitwisseling via STOP

In STOP is er geen module beschikbaar waarin de inhoud van een toestand beschreven wordt als combinatie van versie plus nog te verwerken publicaties. Dat komt omdat er geen objectieve criteria zijn om een eenduidige keuze te maken welke versie met ontbrekende publicatie gekozen moet worden. Het is bovendien nog niet duidelijk of deze werkwijze toegepast gaat worden, waardoor er ook geen inzicht is in de reikwijdte van aanvullende criteria.