# Tekstmuteren



Deze leeswijzer richt zich op tekstmuteren. Het beschrijft hoe wijzigingen van een was-versie t.o.v. een wordt-versie van een regeling binnen [tekst:RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) worden vastgelegd. Een [tekst:RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) wordt meegestuurd in een wijzigingsbesluit. Zie ook [Besluit](quickstart_2_Besluiten.md). 



## Structuur regeling

Een regeling (zie ook [Regeling](quickstart_1_Regelingen.md)) kan de volgende elementen kennen:

| Element                                                      | Muteerbaar |
| ------------------------------------------------------------ | ---------- |
| [tekst:RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift)       | Nee        |
| [tekst:Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef)                             | Nee        |
| [tekst:Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met daarbinnen: a) een hiërarchische indeling als Hoofdstuk, Afdeling, Artikel of b) een vrije tekststructuur met Divisie en Divisietekst | Ja         |
| [tekst:Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)                         | Nee        |
| [tekst:Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)                           | Ja         |
| [tekst:Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) me daarbinnen (optioneel) [tekst:AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) en/of [tekst:ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) | Ja         |



## Mutatie-eenheden

@@@IMOPVERSIE@@@ ondersteunt het wijzigen van de onderstaande elementen (mutatie-eenheden).

### Mutatie-eenheden Lichaam

Het [tekst:Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) zelf is een mutatie-eenheid. Indien er ingrijpende structuurwijzigingen zijn in een regeling, kan er in uitzonderlijke gevallen besloten worden om het gehele lichaam te vervangen.
NB. Voor een 'tekstplaatsing' wordt de volledige regeling als een nieuwe regeling aangeboden. 



Binnen het lichaam kan afhankelijk van het tekstmodel (zie ook [Regeling](quickstart_1_Regelingen.md)) een artikelsgewijze  of een vrije tekststructuur gevolgd worden.



#### Artikelsgewijs

- [tekst:Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel) (kleinste mutatie-eenheid)
- [tekst:Afdeling](tekst_xsd_Element_tekst_Afdeling.dita#Afdeling)
- [tekst:Boek](tekst_xsd_Element_tekst_Boek.dita#Boek)
- [tekst:Deel](tekst_xsd_Element_tekst_Deel.dita#Deel)
- [tekst:Hoofdstuk](tekst_xsd_Element_tekst_Hoofdstuk.dita#Hoofdstuk)
- [tekst:Paragraaf](tekst_xsd_Element_tekst_Paragraaf.dita#Paragraaf)
- [tekst:Subparagraaf](tekst_xsd_Element_tekst_Subparagraaf.dita#Subparagraaf)
- [tekst:Subsubparagraaf](tekst_xsd_Element_tekst_Subsubparagraaf.dita#Subsubparagraaf)
- [tekst:Titel](tekst_xsd_Element_tekst_Titel.dita#Titel)

De kleinste mutatie-eenheid binnen een regeltekststructuur is een Artikel. Dit betekent dat bij de aanlevering van een wijzigingsbepaling in het [Besluit](quickstart_2_Besluiten.md), het artikel de kleinste container is waarin wijzigingen op een was-versie van een regeling kunnen worden doorgegeven. Als bijvoorbeeld van de 5 leden binnen een Artikel, 1 lid verandert, dan dient het gehele Artikel met renvooimarkering voor het gewijzigde lid te worden aangeleverd via de mutatieactie [tekst:Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang). Zie ook [bepalen_wijzigingen_renvooi](bepalen_wijzigingen_renvooi.md). 



#### Vrije tekst

- [tekst:Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie) 

- [tekst:Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) (kleinste mutatie-eenheid)

  
### Overige Mutatie-eenheden (binnen een regeling)

- [tekst:Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)
- [tekst:Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting)
- [tekst:AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting)
- [tekst:ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting)

Deze bijlagen bij een regeling kennen een vrije tekstructuur. Ook binnen deze bijlage is de kleinste mutatie-eenheid dus [tekst:Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst)  



## Mutatieacties

Een STOP mutatie-actie beschrijft hoe een mutatie-eenheid wijzigt (de was-versie t.o.v. de wordt-versie). In imop-tekst zijn de onderstaande mutatieacties beschikbaar. Een aanvullende uitleg met een voorbeeldfragment is te vinden in de beschrijving van de betreffende elementen.

| Mutatieactie                             | Beschrijving                                                 |
| ---------------------------------------- | ------------------------------------------------------------ |
| [tekst:Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang)       | Dit element geeft aan welke mutatie-eenheid in een reeds bestaande regeling moet worden vervangen. |
| [tekst:VervangKop](tekst_xsd_Element_tekst_VervangKop.dita#VervangKop) | Dit element wordt gebruikt voor de wijziging van de tekstuele inhoud van de kop van een structuurelement. Deze mutatieactie wordt gebruikt, indien de inhoud van de mutatie-eenheid (buiten de kop) verder gelijk blijft. |
| [tekst:Verwijder](tekst_xsd_Element_tekst_Verwijder.dita#Verwijder)   | Dit element geeft aan welke mutatie-eenheid uit een reeds bestaande regeling moet worden verwijderd. |
| [tekst:VoegToe](tekst_xsd_Element_tekst_VoegToe.dita#VoegToe)       | Dit element wordt gebruikt voor het toevoegen van een mutatie-eenheid aan een reeds bestaande regeling. |

De bovenstaande mutatieacties kunnen op verschillende niveau's worden toegevoegd. Voor het bepalen van het juiste niveau dienen een aantal business rules gevolgd te worden die gedocumenteerd zijn in de leeswijzer [bepalen van wijzigingen en renvooi](bepalen_wijzigingen_renvooi.md).



## Voorbeelden

* [Voorbeeld wijzigingsproces o.b.v. een definitief compact besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/index.md)




