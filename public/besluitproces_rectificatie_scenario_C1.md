# C1. Rectificatie inwerkingstredingsdatum

Om een inwerkingstredingsdatum in een gepubliceerde besluit te corrigeren worden in de juridische praktijk twee methoden gebruikt:

1. In de rectificatie opnemen wat de correcte inwerkingstredingsdatum in het besluit had moeten zijn;
2. Met de rectificatie de tekst van het artikel in de besluittekst betreffende de inwerkingstredingsdatum corrigeren.

Hier wordt de eerste variant toegelicht. Hoe een foutief artikel in het besluit gecorrigeerd moeten worden, wordt beschreven in [scenario C3](besluitproces_rectificatie_scenario_C3.md): besluitmutatie artikel.  LET OP: In beide gevallen moet gecorrigeerde ConsolidatieInformatie meegeleverd worden, zie hieronder.



## Gecorrigeerde regeling

De regeling zelf wijzigt niet, dus voor een rectificatie van de inwerkingstredingsdatum worden geen STOP modules die betrekking hebben op de regeling meegeleverd.



## Gecorrigeerde besluitmodules

De correctie van de inwerkingstredingsdatum leidt tot nieuwe besluit modules:

- [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van het originele besluit dat door deze rectificatie wordt aangepast(FRBRExpression); 
- [ProcedureverloopMutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie):  De inwerkingstredingsdatum is geen stap in de besluitvormingsprocedure. Geen aanpassingen dus.
- [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie): De correctie van de inwerkingstredingsdatum moet verwerkt worden in de `juridischWerkendVanaf` [tijdstempel](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel) van de gecorrigeerde regelingversie in de consolidatie-informatie. De consolidatieInformatie van de rectificatie is een aanvulling op de consolidatieinformatie van het besluit. De consolidatieinformatie van de rectificatie van de inwerkingstredingsdatum bevat dus alleen nieuwe tijdstempel informatie met:
  - [Doel](data_xsd_Element_data_doel.dita#doel): wijzigt niet door de correctie van de inwerkingstredingsdatum;
  - [soortTijdstempel](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel): alleen de `juridischWerkendVanaf` tijdstempel moet aangepast worden als gevolg van de correctie van de inwerkingstredingsdatum;
  - [datum](data_xsd_Element_data_datum.dita#datum): hier moet de gecorrigeerde inwerkingstredinsdatum staan;
  - [eId](data_xsd_Element_data_eId.dita#eId): de verwijzing naar de plaats in de rectificatie waar de gecorrigeerde inwerkingstredingsdatum staat. Bijvoorbeeld: `!rectificatiebesluit1#art_II`.
- [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata): De correctie van de inwerkingstredingsdatum heeft geen impact op de BesluitMetadata. De BesluitMetadata is een annotatie en de informatie van de besluitmetadata van voorgaande versies blijft van toepassing zolang er geen nieuwe versie wordt aangeleverd. Daarom hoeft de rectificatie geen BesluitMetadata mee te leveren.



## De rectificatie zelf

De rectificatie zelf bestaat uit:

* [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van de rectificatie zelf. De rectificatie van een besluit is een zelfstandig Work en elke rectificatie bij hetzelfde besluit is een nieuwe expressie van het rectificatie Work;

* [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata): Net als bij een besluit moet er bij een rectificatie altijd metadata aangeleverd worden. De RectificatieMetadata lijkt erg op de BesluitMetadata, maar heeft een extra veld [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert);

* [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie): De rectificatie bestaat uit:
  * Een [regeling opschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift), bijv. 'Rectificatie van de Verordening Toeristenbelasting 2017'

  * Het [lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met één of meerdere:
    * [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)(en) met 
      * een [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop);
      * een of meer toelichtende [alinea](tekst_xsd_Element_tekst_Al.dita#Al)'s;
  In deze alinea's wordt beschreven wat de correcte inwerkingstredingsdatum moet zijn.

Zie voor een voorbeeld de [STOP-codering](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/C1-Iwt-datum.xml) van de _'Rectificatie bekendmaking publicatie verlenging van het Provinciaal Milieubeleidsplan Zeeland 2006–2012'_ uit de [voorbeelden uit de staande praktijk](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/index.md).