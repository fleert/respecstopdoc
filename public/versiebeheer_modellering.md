# Modellering in STOP

Als uitwisselingsstandaard beschrijft STOP niet zoals `git` hoe het versiebeheer binnen een organisatie uitgevoerd moet worden,
maar biedt STOP alleen een model om de uitwisseling van (informatie over) versies te faciliteren. De uitwisseling vindt op drie manieren plaats:

- Tijdens het [samenwerken aan nieuwe regelgeving](samenwerken_aan_regelgeving): via een uitwisselpakket met een complete versie van een regeling voor één doel, met een `Momentopname` als beschrijving van de versie.
- Als onderdeel van het [besluitvormingsproces](besluit.md): opgenomen in (een bekendmaking van) een besluit of een officiële publicatie, voor één of meer doelen, met ConsolidatieInformatie als beschrijving van de versies.
- Als onderdeel van een [revisie waarmee de geconsolideerde regeling](consolideren_patronen.md) in de LVBB wordt aangepast: voor één inwerkingtredingsdatum, met ConsolidatieInformatie als beschrijving van de versie.

De variatie in modellering is deels het gevolg van de juridische eisen die aan de besluitvorming gesteld worden, deels door de latere toevoeging van versiebeheer aan de standaard met behoud van de modellering die al aanwezig was.

## Vergelijking STOP en `git`
Voor vrijwel elk aspect van versiebeheer in STOP is een equivalent in `git` te benoemen.

| Aspect | STOP | `git` |
| ------ | ---- | ----- |
| Specificatie onveranderlijke versie | [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) of [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie)| *Commit* |
| Identificatie van onveranderlijke versie | [doel](data_xsd_Element_data_doel.dita#doel) + [gemaaktOp](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) | Hash van de commit  |
| Identificatie van een branch | [doel](data_xsd_Element_data_doel.dita#doel) | Naam |
| Verwijzing naar vorige versie | [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan)/[Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) | Verwijzing naar voorgaande commit |
| Wijzigingen overgenomen uit andere branch | [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan)/[VervlochtenVersie](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) | Verwijzing naar commit van andere branch |
| Wijzigingen uit andere branch verwijderd | [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan)/[OntvlochtenVersie](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie) | - |
| Uitwisseling van een versie | Via uitwisselpakket, via officiële publicatie of revisie | Push of pull |
| Producthistorie | [toestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) per regeling/informatieobject | Commits van de master |

## Wijzigingsoverzicht en historie in STOP en `git`
Een ander verschil met `git` is de manier waarop met historie wordt omgegaan. In `git` verwijst elke _commit_ naar een voorgaande _commit_; om te weten hoe de software er voor een bepaalde commit uitziet zoekt `git` voor elk bestand de laatst gewijzigde versie in de historie. Dat is mogelijk omdat in `git` uiteindelijk alle deelnemers inzage krijgen in elke _commit_. 

In het werkveld van STOP is dat niet het geval:  (tussen)versies worden slechts in beperkte kring uitgewisseld, en de LVBB
weet niets van versies uit creatieproces. Daarom is in STOP gekozen voor een absolute aanduiding (**Momentopname** = `doel + tijdstip`), waarbij het tijdstip de volgorde impliceert.

Een uitwisselpakket met de complete regeling voor `doel + tijdstip 1` kan daarom beschouwd worden als een oudere versie dan een vergelijkbaar uitwisselpakket voor hetzelfde `doel + tijdstip 2` als `tijdstip 1` vóór `tijdstip 2` ligt; het is daarbij niet relevant of eventuele tussenversies zijn uitgewisseld. Als er meerdere partijen tegelijk aan dezelfde branch werken is de Momentopname niet meer een unieke aanduiding van de inhoud van de branch. Die uniciteit kan wel bereikt worden door (in STOP-gebruikende software) de momentopname aan de partij te koppelen, of voor de uitwisseling tussen twee partijen een 
feature branch te gebruiken. Dit is beschreven bij [samenwerken aan regelgeving](samenwerken_aan_regelgeving).
