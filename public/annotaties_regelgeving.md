# Annotaties voor regelgeving

## Wijzigen van annotaties
In het [STOP versiebeheer](versiebeheer_in_stop.md) zal niet voor elke versie van een regeling of informatieobject een nieuwe versie van de annotatie ontstaan.  

![Annotatie in versiebeheer](img/annotaties_versiebeheer.png)

In de illustratie wordt de annotatie van, zeg, een regeling aangemaakt voor doel A. Voor doel B verandert de regeling wel, maar hoeft de annotatie niet aangepast te worden. Omdat doel B een branch is van doel A moet de annotatie overgenomen worden. De annotatie is dus van toepassing voor de toestanden die resulteren voor doel A en B. Voor doel C wijzigt de regeling zodanig dat de annotatie aangepast moet worden, voor doel D niet, dus is de C-versie van de annotatie van toepassing op de toestanden met inwerkingtredingsdoelen C en D.

Uitgangspunt in STOP is dat het bevoegd gezag ook inderdaad een nieuwe versie van de annotatie opstelt als dat nodig is. Het is voor software meestal niet te detecteren of een versie van een annotatie al dan niet bij een versie van de regeling/informatieobject past omdat voor die beoordeling inhoudelijke kennis nodig is. De ontvangende partij kan dus niet signaleren dat een annotatie gewijzigd had moeten worden. De leverende partij moet dus zorgen voor een nieuwe versie van de annotatie als dat nodig is.

## Implementatiekeuze {#implementatiekeuze}

Als STOP-gebruikende systemen annotaties uitwisselen kan ervoor gekozen worden om:

1. annotaties alleen uit te wisselen als de annotatie gewijzigd is, òf 
2. altijd alle annotaties uit te wisselen.

Als de systemen alle informatie over de geconsolideerde regelgeving delen (zoals de software van het bevoegd gezag en de LVBB) dan is optie 1 een bruikbare optimalisatie van het onderlinge dataverkeer. Als slechts incidenteel uitwisseling tussen systemen plaatsvindt (zoals de software van het bevoegd gezag en een adviesbureau), dan zal uitwisseling van regeling/informatieobject plus *alle* annotaties (gewijzigd of niet) voor de hand liggen.

Als ervoor gekozen wordt om een annotatie alleen bij wijziging uit te wisselen, dan moet er ook een manier zijn om aan te geven dat een annotatie niet langer relevant is. Een annotatie als [FeatureTypeStyle](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle) bijvoorbeeld is vereist als in een GIO GIO-delen gebruikt worden, maar zodra de GIO geen GIO-delen meer heeft dan is de `FeatureTypeStyle` niet meer relevant. De [API](xml_modulairestructuur.md) van de uitwisseling kan daar een faciliteit voor bieden. Als dat niet het geval is, moet voor een STOP annotatiemodule de inhoud (afgezien van het rootelement) leeggelaten worden.

**Zie ook**

* De volledige lijst van alle STOP annotaties in het overzicht van [modules](imop_modules.md).