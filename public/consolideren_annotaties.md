# Consolideren van annotaties

Een [annotatie](annotaties.md) is een machine-leesbare codering van bepaalde aspecten van de inhoud van de regeling of het informatieobject. In het algemeen zal een annotatie niet zo vaak wijzigen als de inhoud van de regeling/informatieobject. 

Als bij de [uitwisseling van annotaties](annotaties_regelgeving.md) gekozen wordt om alleen gewijzigde annotaties uit te wisselen, en bij ongewijzigde annotaties de annotatie van de voorgaande versie te gebruiken, kan aan de hand van onderstaande aanpak bepaald worden welke annotatie behoort bij een bepaalde toestand.

## Twee typen annotaties

Voor het correct consolideren van annotaties, het koppelen van versies van annotaties met toestanden van een regeling/informatieobject, is de volgende eigenschap van belang:

* _Type I_ annotaties zijn afgeleid van de inhoud van een deel van de tekst van de regeling (of data van het informatieobject) en veranderen alleen als de strekking van dat deel van de tekst/data wijzigt. Voorbeeld: de officiële titel in de annotatie _RegelingMetadata_ correspondeert met het regelingopschrift in de tekst van de regeling.
* _Type II_ annotaties zijn afgeleid van de structuur of vorm van de tekst (of van de data van het informatieobject). Een wijziging van de regeling/informatieobject die de strekking ervan niet wijzigt kan toch leiden tot een verandering van de annotatie. Voorbeeld: het aantal artikelen in de tekst van de regeling.

Het onderscheid tussen de typen zit in het gedrag bij het oplossen van samenloop. Als een versie van een annotatie van type I voor twee versies van een regeling/informatieobjeect bruikbaar is, dan is de annotatie ook bruikbaar voor de vervlechting of ontvlechting van die twee versies. Voor een annotatie van type II hoeft dat niet zo te zijn. Voor type I annotaties kan in bepaalde gevallen wel de juiste versie van de annotatie bepaald worden, ook al is de regeling- of informatieobjectversie niet bekend.

Alle annotaties die in STOP zijn gedefinieerd zijn van type I.

## Recept
Net als voor de [geautomatiseerde consolidatie](consolideren_toestanden_bepalen.md) wordt het recept beschreven alsof er nog niets over de relatie tussen annotaties en toestanden bekend is. In de praktijk zullen de relaties bijgewerkt worden als een nieuwe publicatie (of beschikbaarstelling) gedaan wordt. Zonder kennis van het interne datamodel van software is het bijwerken niet te beschrijven. De vertaling van deze beschrijving naar bijwerk-functionaliteit zal door de maker van de STOP-gebruikende software gedaan moeten worden.

De methodiek wordt beschreven voor één regeling met één annotatie.  De werking is hetzelfde voor annotaties bij een informatieobject. De illustraties gebruiken dezelfde symbolen als [elders](consolideren_patronen.md).

De methodiek bestaat uit vier stappen:

1. Selecteren laatst uitgewisselde versie per doel/branch;
2. Selecteren kandidaat-annotatieversies voor toestanden;
3. Controleren of de kandidaat-annotatiesversies toegewezen kunnen worden aan de toestanden;
4. Geldigheid overnemen.

De stappen worden hieronder toegelicht.

### 1. Selecteren laatst uitgewisselde versie van de annotatie

Voor de consolidatie zijn alleen de laatst aangeleverde versies voor elk doel relevant.

![Selectie van versies](img/consolideren_annotatie_versie.png)

In de illustratie is een reeks uitwisselingen weergegeven waarbij soms een versie van de annotatie is bijgesloten. Voor de consolidatie zijn van belang:

* _Niet_ versie 1 want er is een latere uitwisseling voor doel A.
* _Wel_ versie 2 voor doel A.
* _Niet_ versie 3 voor doel C, want er is een latere uitwisseling voor doel C
* _Wel_ versie 4 voor doelen C, D.

### 2. Selecteren kandidaat-annotatieversie voor toestanden

Vervolgens worden de [actuele toestanden](consolideren_toestanden.md) voor de regeling erbij genomen. De versies van de annotatie worden bij die toestand geplaatst waarvoor de inwerkingtredingsdoelen overeenkomen met de doelen waarvoor de annotatie is gemaakt. Dat zou er uit kunnen zien als:

![Kandidaat-annotatieversies voor toestanden](img/consolideren_annotatie_kandidaten.png)

In de illustratie is met een stippellijn aangegeven welke versie van de annotatie kandidaat is voor de versie die bij een toestand hoort. Deze lijnen zijn bepaald aan de hand van:

* Voor de toestanden met een inwerkingtreding vóór de inwerkingtreding van het doel van eerste uitwisseling van de annotatie is geen versie van de annotatie beschikbaar. Een annotatie geldt dus alleen voor het doel/doelen waarvoor de annotatie is uitgewisseld en voor doelen die daarna in werking treden. Nooit voor doelen die eerder in werking treden (let op: de inwerkingtredingsvolgorde kan afwijken van de volgorde waarin versies worden uitgewisseld).
* Als de doelen waarvoor de versie van de annotatie is uitgewisseld exact gelijk zijn aan de inwerkingtredingsdoelen van een van de toestanden, dan is die versie van de annotatie de kandidaat-versie voor die toestand.
* Alleen voor type I annotaties: als de doelen waarvoor de versie van de annotatie is uitgewisseld een deelverzameling is van de inwerkingtredingsdoelen van een van de toestanden, en geen van de inwerkingtredingsdoelen buiten deze deelverzameling maakt deel uit van de doelen waarvoor de andere annotatieversies zijn uitgewisseld, dan is die versie van de annotatie de kandidaat-versie voor die toestand. 
  Immers: zolang er alleen voor de deelverzameling van de inwerkingtredingsdoelen van de toestand een annotatie is aangeleverd moet dat ook de kandidaat-versie voor alle inwerkingtredingsdoelen van de toestand zijn, omdat als een van de andere inwerkingtredingsdoelen een tegenstrijdige wijziging had gehad dan zou voor dat doel ook een annotatie aangeleverd zijn. 
  (In de illustratie: als de annotatie voor doel H,K,L was uitgewisseld voor alleen doel H, alleen doel K of alleen voor doel L was het ook een kandidaatversie geweest. Als er een annotatie uitgewisseld was voor doel H en een voor doel K, dan is er geen kandidaat-versie aan te wijzen. Als er annotaties waren uitgewisseld voor doel H, voor doel K en doel H,K, dan is de H,K-versie de kandidaatversie.) 
* Als geen enkel doel van de inwerkingtredingsdoelen van een toestand voorkomt in de doelen waarvoor een versie van de annotatie is uitgewisseld, dan is de kandidaat-versie voor de toestand dezelfde als die van de toestand die direct daarvoor in werking is getreden.
* Er kunnen versies van een annotatie zijn die nu voor geen enkele toestand (meer) de kandidaat-versie zijn. Dat kan voorkomen als de annotatie bijvoorbeeld is uitgewisseld voor een doel waarvoor (nog) geen inwerkingtreding is doorgegeven, of als onderdeel van een besluit dat door de rechter vernietigd is. (In de illustratie: F)

De toestand die direct daarvoor in werking is getreden kan gevonden worden door de actuele toestanden op _juridisch werkend op_ te sorteren en dan de voorgaande in de rij te selecteren. Het kan ook door te kijken naar de _Basisversie-doelen_: de optelsom van _Inwerkingtredingsdoelen_ en _Basisversie-doelen_ van de voorgaande toestand is gelijk aan de _Basisversie-doelen_ van de opvolgende toestand.

### 3. Toewijzen van annotatieversies aan toestanden

De kandidaat-annotatieversie voor een toestand wordt al dan niet toegewezen aan een toestand op basis van een aantal criteria. Als de kandidaat-annotatieversie niet wordt toegewezen, dan is onbekend wat de inhoud van de annotatie voor de betreffende toestand is.

1. Voor de toestand waarvan de inwerkingtredingsdoelen gelijk zijn aan de doelen waarvoor de kandidaat-annotatieversie is aangeleverd geldt:

   a. Als de _Instrumentversie_ bekend is, dan wordt de kandidaatversie toegewezen aan deze toestand.

   b. Als de _Instrumentversie_ niet bekend is, dan wordt de kandidaatversie aan geen enkele toestand toegewezen waar het een kandidaat-versie voor is.

2. Voor andere toestanden:

   a. Als de _Instrumentversie_ bekend is, dan wordt de kandidaatversie toegewezen aan deze toestand (behalve als 1b van toepassing is).

   b. Als de _Instrumentversie_ niet bekend is en de annotatie is van type II, dan wordt de kandidaatversie niet toegewezen aan deze toestand.

   c. Als de _Instrumentversie_ niet bekend is en de annotatie is van type I:

   ​    i. Als _Nog te verwerken_ van de toestand niet is ingevuld, dan wordt de kandidaatversie niet toegewezen aan deze toestand.

   ​    ii. Als in _Nog te verwerken_ van de toestand een doel ter ontvlechting voorkomt waarvoor een annotatie-versie is aangeleverd, dan wordt de kandidaatversie niet toegewezen aan deze toestand.

   ​    iii. Als in _Nog te verwerken_ van de toestand een doel ter verwerking voorkomt en aan de toestand die dat doel als inwerkingtredingsdoel heeft is geen annotatieversie toegewezen, dan wordt de kandidaatversie niet toegewezen aan deze toestand. Met andere woorden als een vorige toestand geen bekende annotatieversie heeft en de wijzigingen voor een van de inwerkingtredingsdoelen van die toestand moeten nog verwerkt worden in deze toestand, dan kan de annotatieversie voor deze toestand ook nog niet bepaald worden.

   ​    iv. Als in _Nog te verwerken_ van de toestand een doel ter verwerking voorkomt en aan de toestand die dat doel als inwerkingtredingsdoel heeft is wel een annotatieversie toegewezen maar dat is een andere dan de kandidaat-annotatieversie voor deze toestand, dan wordt de kandidaatversie niet toegewezen aan deze toestand. Met andere woorden als een vorige toestand wel een bekende annotatie versie heeft en de wijzigingen moeten nog overgenomen worden en de huidige heeft ook een annotatie-versie, dan kan de annotatieversie voor deze toestand nog niet bepaald worden. 

   ​    v. In de andere gevallen wordt de kandidaat-annotatieversie toegewezen aan de toestand.

Deze criteria kunnen toegelicht worden voor een type I annotatie in het voorbeeld:

![Kandidaat-annotatieversies voor toestanden](img/consolideren_annotatie_type_I.png)

* Aan toestand A wordt geen annotatieversie toegewezen, want er is geen kandidaat-annotatieversie.
* Aan toestand B wordt de annotatieversie voor doel B toegewezen vanwege criterium 1a.
* Aan toestand C wordt de annotatieversie voor doel B toegewezen vanwege criterium 2a.
* Aan toestand D wordt de annotatieversie voor doel D toegewezen vanwege criterium 1a.
* Aan toestand E wordt de annotatieversie voor doel D toegewezen vanwege criterium 2c-v (want 2c-iii en 2c-iv zijn niet van toepassing). In dit geval is de regelingversie voor de toestand onbekend, maar is wel bekend wat de annotatieversie is.
* Aan toestand G wordt geen annotatieversie toegewezen vanwege criterium 2c-ii.
* Aan toestand H,K wordt geen annotatieversie toegewezen vanwege criterium 1b.
* Aan toestand L wordt geen annotatieversie toegewezen vanwege criterium 1b.

Als de annotatie van type II is zou het voorbeeld zijn:

![Kandidaat-annotatieversies voor toestanden](img/consolideren_annotatie_type_II.png)

* Aan toestand A wordt geen annotatieversie toegewezen, want er is geen kandidaat-annotatieversie.
* Aan toestand B wordt de annotatieversie voor doel B toegewezen vanwege criterium 1a.
* Aan toestand C wordt de annotatieversie voor doel B toegewezen vanwege criterium 2a.
* Aan toestand D wordt de annotatieversie voor doel D toegewezen vanwege criterium 1a.
* Aan toestand E wordt geen annotatieversie toegewezen vanwege criterium 2b.
* Aan toestand G wordt geen annotatieversie toegewezen vanwege criterium 2b.
* Aan toestand H,K wordt geen annotatieversie toegewezen vanwege criterium 1b.
* Aan toestand L wordt geen annotatieversie toegewezen vanwege criterium 1b.

### 4. Overnemen geldigheid

In stap 3 is voor elke toestand bepaald welke versie van de annotatie bij de toestand hoort. Soms is het handiger om niet de relatie met de toestand van de regeling op te slaan, maar de geldigheid van de toestanden over te nemen. Zoals uit de voorbeelden blijkt, kan één annotatieversie geldig zijn in verschillende _juridisch werkend op_-perioden.

Voor een ingetrokken regeling staat in het overzicht van de toestanden de datum [juridisch uitgewerkt](cons_xsd_Element_cons_juridischUitgewerktOp.dita#juridischUitgewerktOp). Deze datum is ook verwerkt in de geldigheid van de toestanden en hoeft niet apart verwerkt te worden in de geldigheid van de annotatieversies.

Als ook een datum [materieel uitgewerkt](cons_xsd_Element_cons_materieelUitgewerktOp.dita#materieelUitgewerktOp) is opgegeven dan moet die wel overgenomen worden voor de annotatieversies omdat die datum niet in de geldigheid van de toestanden is verwerkt.