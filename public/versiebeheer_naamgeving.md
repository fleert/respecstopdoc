# Naamgeving van doel

De naamgeving van het doel volgt de [naamgevingsconventie](naamgevingsconventie.md) van niet-tekstuele informatie.
Voor doel wordt alleen het work-id gebruikt, want het aangeven van de inhoud van de branch wordt via een `Momentopname` gedaan zoals beschreven bij de [modellering](versiebeheer_modellering.md).

Voor een doel wordt in de JOIN-identificatie **`proces`** gebruikt om aan te geven dat het om het doel in het creatieproces gaat:

**Work**: `"/join/id/proces/" <overheid> "/" <datum_work> "/" <overig>`

De codes betekenen:

| code       | betekenis                                                    |
| ---------- | ------------------------------------------------------------ |
| /join      | Afkorting van **Juridische Object Identificatie Naming convention** |
| /id        | `id` om aan te geven dat het identificaties betreft          |
| /proces    | geeft aan dat het om het *doel* in het creatieproces gaat      |
| /&lt;overheid&gt; | Code van het verantwoordelijke bevoegde gezag volgens één van de [waardelijsten](imop_waardelijsten.md#overheid) |
| /&lt;datum_work&gt; | Datum van het aanmaken van de branch. Dit mag een volledige datum zijn (YYYY-MM-DD conform ISO 8601) maar het mag ook alleen een jaartal zijn. |
| /&lt;overig&gt;     | Een extra kenmerk om het doel uniek te maken, bestaande uit maximaal 128 cijfers, boven- en onderkast-letters, "_" en "-", beginnend met een cijfer of letter (regex `[a-zA-Z0-9][a-zA-Z0-9_-]*`).  Het is altijd toegestaan om hiervoor een [UUID](https://nl.wikipedia.org/wiki/Universally_unique_identifier) te gebruiken. Als het bevoegd gezag de branch voor eigen regelgeving aanmaakt dan kan een betekenisvolle identificatie gebruikt worden. Software die informatie ontvangt waarin een doel voorkomt moet er altijd rekening mee houden dat het `overig` gedeelte op een andere manier is samengesteld dan volgens de conventie die het bevoegd gezag hanteert. |

Voorbeelden:
- `/join/id/proces/gm9999/2021-07-21/a1c2aa83ddaa4b3eb37a97c18f1907dd`
- `/join/id/proces/ws0651/2021/dossier-3929938`

