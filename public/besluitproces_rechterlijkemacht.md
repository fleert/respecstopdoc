# Beroep

TODO verifieren:

Onderwerp: [rechterlijke macht](besluitproces_rechterlijkemacht.md), schorsing e.d., gezien vanuit BG. Wat moet aangeleverd worden, wanneer.
  Dus zowel procedureverloopmutatie bij besluit, als uitspraak rechter plus mutatie van regeling via kennisgeving.Ook uitleggen
  hoet het [muteren van procedureverloop](muteren_procedureverloop.md) werkt.



Een deel van de besluiten van het bevoegd gezag (BG) zijn vatbaar voor ([bezwaar](bezwaar.md) en) [beroep](beroep.md). De (bezwaar- en) beroep-status van een besluit moet zichtbaar zijn op [officielebekendmakingen.nl](https://officielebekendmakingen.nl). De status van het besluit werkt door in de geconsolideerde regeling. Ook in de geconsolideerde regeling moet een en ander zichtbaar zijn, zoals de (on)herroepelijkheid van (wijzigingen in) de geconsolideerde regeling en of een regeling is gewijzigd door een uitspraak van de rechter. Zowel de bestuursrechtelijke beroepsprocedure als hoe BG's volgens STOP een beroep correct moeten verwerken zal hier worden toegelicht, waarbij onder andere de verschillende uitspraak-scenario's zullen worden behandeld. 

STOP faciliteert de uitwisseling van informatie over de beroepsprocedure voor zover het van invloed is op:
* de status van de nieuwe of gewijzigde regelingen en/of informatieobjecten, zoals beschreven in [status in de geconsolideerde regeling](besluitproces_rechterlijkemacht_geconsolideerde_regeling.md). STOP is niet ontworpen om een goed beeld te delen van de voortgang en details van de beroepsprocedure zelf.
* de inhoud van de geconsolideerde regelingen en informatieobjecten.

## [Over beroep](besluitproces_rechterlijkemacht_achtergrond.md)

Korte beschrijving van het beroepsproces.

## [Herroepelijkheid](besluitproces_rechterlijkemacht_herroepelijkheidbesluit.md)


Beschrijving van de status die aangeeft of er (nog) beroep mogelijk is voor een besluit. 

## [Beroep en geconsolideerde regelgeving](besluitproces_rechterlijke_macht_beroep_en_geconsolideerde_regelgeving.md)

Beschrijving van de procedurele statussen van een besluit, de bijbehorende procesflow
en de afleiding van de status van de geconsolideerde regelgeving uit de procedurele besluitstatus.


## [Uitspraken beroep](besluitproces_rechterlijke_macht_omgaan_met_beroep.md)
Beschrijving van de verschillende uitspraakscenario's die mogelijk zijn in een beroepsprocedure, met alle bijbehorende stappen die noodzakelijk zijn om ervoor te zorgen dat de inhoud van geconsolideerde regeling juist is en de juiste status heeft.



