# Pons in STOP {#concept_rcv_jzn_gqb}

Niet elk besluit dat op grond van de Wet ruimtelijke ordening tot stand is gekomen zal bij inwerkingtreding van de Omgevingswet al zijn omgezet in STOP-xml. Gemeenten hebben tijdens de overgangsfase voor bestemmingsplannen - die vooralsnog duurt tot 2029 - de tijd om alle oude bestemmingsplannen om te zetten in (delen van) het omgevingsplan. De op [Ruimtelijkeplannen.nl](http://ruimtelijkeplannen.nl/) gepubliceerde ruimtelijke plannen, die van rechtswege onderdeel zijn van een omgevingsplan, blijven via een overbruggingsfunctie in DSO-LV raadpleegbaar.

Voor de gebruiker van DSO-LV is het van belang om te weten welke regels gelden: die uit het omgevingsplan van rechtswege, die uit het omgevingsplan conform Omgevingswet of allebei. Om die duidelijkheid te kunnen verschaffen kan het bevoegd gezag bij het aanleveren van een besluit tot vaststelling of wijziging van het omgevingsplan een stukje extra informatie aanleveren waarmee het aangeeft dat dat besluit een deel van de oude content uit [Ruimtelijkeplannen.nl](http://ruimtelijkeplannen.nl/) vervangt. Met andere woorden: aangegeven kan worden of door het besluit één of meer in de overbruggingsfunctie aanwezige bestemmingsplannen of één of meer delen daarvan vervallen. Als dat het geval is wordt met het besluit een geometrie meegeleverd van het vervallen deel. Dit deel wordt dan in DSO-LV niet meer getoond. Voor het aanleveren van de extra informatie wordt het OW-object Pons gebruikt. Als door het besluit één of meer bestemmingsplannen volledig vervallen levert het bevoegd gezag een Pons aan én het verwijdert het bestemmingsplan uit het Wro-manifest van de gemeente.  Het bestemmingsplan wordt langs die weg ook uit de overbruggingsfunctie verwijderd.

De bijbehorende geometrische data moet bij het desbetreffende besluit worden aangeleverd in de vorm van een [pons-GIO](gio-pons.md). Het is daarnaast de bedoeling dat in het besluitdeel een artikel wordt opgenomen, waarin uitdrukkelijk bepaald wordt dat de delen van de daarin genoemde bestemmingsplannen vervallen als gevolg van de pons, waarbij verwezen wordt naar het JOIN-ID.

Per omgevingsplan is er één Pons. Deze Pons wordt in de loop van de tijd gemuteerd. Met iedere mutatie wordt de Pons uitgebreid, net zolang tot alle bestemmingsplannen van de gemeente zijn vervallen. Daarna kan het Pons-GIO worden ingetrokken en kan het OW-object Pons beëindigd worden.

## Voorbeeld {#section_ers_hyf_3qb .section}

Artikel 5.

Door dit besluit vervallen de delen van de bestemmingsplannen Veluwe en Uddel die zijn aangegeven met de bij dit besluit behorende pons met de identificatie `/join/id/regdata/gm1234/…`

**Opmerking:** De pons wordt *niet* alleen gebruikt als er delen van bestemmingsplannen vervallen. Als een bestemmingsplan in zijn geheel vervalt wordt naast de pons *ook* het bestemmingsplan uit het Wro-manifest van de gemeente verwijderd. 

