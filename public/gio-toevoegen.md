# Een GIO toevoegen

Om een eerste versie van een GIO vast te stellen moet deze worden opgenomen in een regeling en via een besluit worden vastgesteld. Dit gebeurt als volgt:



1. In de regelingtekst van een besluit wordt verwezen naar het GIO. Zie [Verwijzen naar een GIO](gio-tekstverwijzing.md).
2. In de tekst-bijlage van de regeling worden het label (GIO-naam, zoals *'Militaire oefenterreinen'*) gekoppeld aan het JOIN-ID van het GIO. Zie [GIO opnemen in bijlage](gio-opnemen-bijlage.md).
3. Het GIO wordt opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit. Zie [GIO opnemen als beoogd informatieobject](gio-opnemen-als-beoogdinformatieobject.md).
4. Het GIO  wordt als informatieobject toegevoegd aan het besluit over de regeling. Zie [GIO-bestand toevoegen](gio-bestand-toevoegen.md).


## Zie ook
* Voor informatie over de (verplichte) onderdelen van een GIO, zie de sectie [Onderdelen GIO](gio-onderdelen.md).
* Voor de (verplichte) onderdelen en meer van de verschillende soorten GIO's, zie de sectie [Soorten GIO's](gio-soorten.md).
* Voor informatie over verwijzen naar GIO's vanuit juridische teksten, zie [Verwijs in de tekst naar het GIO](gio-tekstverwijzing.md). 
* Voor informatie over verwijzen vanuit de tekst naar informatieobjecten in het algemeen, zie [verwijzen naar informatieobjecten](io-tekstverwijzen.md).
* [Voorbeeld van een nieuwe regeling met  GIO](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO).
* De [handreiking voor het werken met GIO's](handreiking-optimale-geometrieen-omgevingswet-ow-locaties-versie-0-6a-20210617.pdf) voor het werken met GIO's van het DSO.