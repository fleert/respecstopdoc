# Eén of meer besluiten

Als het bevoegd gezag meerdere samenhangende regelingversies (voor meerdere regelingen en/of voor meerdere doelen) tegelijk opstelt, dan is er vaak keuze met hoeveel besluiten de versies worden vastgesteld: één besluit voor alle versies, één besluit per versie of iets er tussen in. Dit is grotendeels een beslissing die los staat van de standaard. Het bevoegd gezag kan wel een aantal overwegingen meenemen die het gebruik van de standaard raken. De overwegingen hangen samen met het [tekstmodel](besluit_modellering.md) dat voor het besluit gebruikt wordt.

## Alle modellen
Ongeacht het tekstmodel staat de standaard toe één besluit te maken met daarin een combinatie van:

* Maximaal één instelling van een [(hoofd)regeling](DITA_OnbekendeLink.dita#Oeps).
* Nul of meer instellingen van een [tijdelijk regelingdeel](DITA_OnbekendeLink.dita#Oeps).
* Nul of meer wijzigingen van een (hoofd)regeling of van een tijdelijk regelingdeel.
* Nul of meer intrekkingen van een regeling.

Het maakt daarbij niet uit of de nieuwe regelingversies allemaal voor één doel zijn gespecificeerd, elk voor een ander doel, of iets daartussen. Ook is het mogelijk meer dan één wijziging (of een instelling plus één of meer wijzigingen) op te nemen voor hetzelfde work, dus voor dezelfde (hoofd)regeling of tijdelijk regelingdeel.

Er zijn geen beperkingen aan het aantal informatieobjecten dat via het besluit ingesteld, gewijzigd of ingetrokken kan worden. Een besluit kan niet uitsluitend een informatieobject instellen, wijzigen of intrekken: omdat het ook een wijziging in de geboorteregeling van het informatieobject vereist.

Voor de LVBB en achterliggende systemen kunnen implementatiekeuzes en limieten van toepassing zijn waardoor toch niet alle mogelijkheden toegestaan zijn. Dit is beschreven in de documentatie van het [bronhouderkoppelvlak](https://koop.gitlab.io/lvbb/bronhouderkoppelvlak/).

## Standaard model

Bij gebruik van het [standaard tekstmodel](besluit_compact.md), `BesluitCompact`, is het ook mogelijk meer dan één hoofdregeling in te stellen. De ingestelde of te wijzigen (hoofd)regeling mag geen `RegelingKlassiek` zijn.

Alle overwegingen hebben te maken met welke besluitprocedure gevolgd dient te worden, welk bestuursorgaan een regelingversie moet vaststellen, hoe het proces van vaststelling binnen een bevoegd gezag is geregeld, en wat het effect van een eventuele beroepsprocedure is.

## Klassiek model
Bij gebruik van een [klassieke tekstmodel](besluit-klassiek.md), `BesluitKlassiek`, moet de in te stellen of te wijzigen (hoofd)regeling altijd van het type `RegelingKlassiek` zijn. De [Aanwijzingen op de regelgeving 6.1](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=6&paragraaf=6.1&aanwijzing=6.1) stelt dat "wijziging (...) van een regeling geschiedt door een regeling van gelijke orde". Een wet kan alleen bij wet worden gewijzigd, een AMvB alleen door een AMvB, enzovoort. De in te stellen, te wijzigen of in te trekken (hoofd)regelingen en de hoofdregelingen behorend bij de tijdelijk regelingdelen moeten dus van gelijke orde zijn.

De STOP standaard is nadrukkelijk bedoeld om de geldende regelgeving beschikbaar te maken voor breder gebruik dan in juridisch georiënteerde regelingenbanken. In voorzieningen als het DSO-LV hebben artikelen met bijvoorbeeld inwerkingtredingsbepalingen geen functie. Ze kunnen zelfs verwarrend werken omdat een gebruiker van zo'n voorziening ervan uitgaat dat de voorziening alleen geldende regelgeving laat zien terwijl de tekst suggereert dat de gebruiker zelf nog moet nagaan in hoeverre de inhoud van het artikel in de voorziening is verwerkt.

Het is daarom aan te bevelen om in tegenstelling tot wat in de huidige praktijk soms gebruikelijk is, de instelling van een `RegelingKlassiek` over meerdere besluiten te verspreiden

![Instelling van een RegelingKlassiek](img/BesluitKlassiek-Hoeveel.png)


STOP adviseert om de Aanwijzing 6.3 "*Scheid de wijzigingsbepalingen af in een aparte eigen regeling, los van de initiële regeling*" strak te volgen. De instelling van de regeling wordt opgenomen in een instellingsbesluit, wijzigingen van andere regelingen (voor hetzelfde doel) en overige bepalingen in een of meer apart invoerings- en/of aanpassingsregeling. Dus om in een <u>apart</u> invoerings-, aanpassings- of inwerkingtredingsbesluit op te nemen:

* Wijzigingen van andere regelingen.
* Instelling van tijdelijk regelingdelen.
* Inwerkingtredingsbepalingen.
* Als bekend is dat enige tijd na inwerkingtreding de regeling verder aangepast wordt (dus er is bij inwerkingtreding al een tweede regelingversie voor een ander doel): de volgende regelingversie als wijziging op de initiële regeling.
* Zelfstandige bepalingen die zogenaamde "zelfbindende" [evaluatiebepalingen](https://wetten.overheid.nl/jci1.3:c:BWBR0005730&hoofdstuk=5&paragraaf=5.13) beschrijven. Zo kan een regeling bepalingen bevatten waarin de wetgever zichzelf een verplichting oplegt om het effect van de regeling na een bepaalde periode aan bijvoorbeeld het parlement te rapporteren, zoals ["Onze Minister zendt binnen vijf jaar na de inwerkingtreding van deze wet aan de Staten-Generaal een verslag over de doeltreffendheid en de effecten van deze wet in de praktijk"](https://wetten.overheid.nl/jci1.3:c:BWBR0044249&artikel=Ia).

Hiermee worden mutaties in een natuurlijke vorm gescheiden van de algemeen geldende voorschriften. De wijzigingsbepalingen worden in dat geval niet tijdens de hele levensloop van de (geconsolideerde) regeling "meegesleept". Met andere woorden: Schrijf voor 100% initiële regeling of 100% wijzigingsregeling-zonder-zelfstandige-bepalingen. De wijzigingsbepalingen zijn bij een initiële regeling vaak invoeringsbepalingen; die kunnen dan in een aparte invoeringsregeling komen die de vorm van een wijzigingsregeling heeft. Deze keuze is bijvoorbeeld expliciet zo gemaakt bij de [Omgevingswet](https://zoek.officielebekendmakingen.nl/stb-2016-156.html), de [Invoeringsregeling](https://zoek.officielebekendmakingen.nl/stcrt-2020-64380.htm) daarvan en de Aanvullingsregelingen ([geluid](https://zoek.officielebekendmakingen.nl/stcrt-2021-15868.html), [grondeigendom](https://zoek.officielebekendmakingen.nl/stcrt-2021-34636.html) en [bodem](https://zoek.officielebekendmakingen.nl/stcrt-2021-28102.html)) daarop.