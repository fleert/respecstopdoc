# Verschillen met de huidige praktijk

TODO

STOP volgt op hoofdlijnen de praktijk waar het gaat om opstellen van regelgeving, besluitvorming en consolidatie.
  Maar STOP wijkt op een aantal onderwerpen af om de doelstellingen te kunnen realiseren. Onderwerpen: renvooi ipv wijzigingsinstructies,
  [juncto](juncto.md), geo onder Bkw dus ontworpen voor officiële publicaties en niet zozeer gebruik in domein-specifieke LVs,
  strikte scheiding tussen regeling(wijziging) en besluit ivm automatisering van consolidatie,
  overgangsrecht en geldigheidsbepalingen in regeling ipv besluit vanwege één geldende versie van de regeling.

Dit zou vooral een opsomming moeten worden, met verwijzing naar de plaatsen in de STOP documentatie waar de STOP features beschreven staan.