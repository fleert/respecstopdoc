# D3. Terugtrekken Geografisch informatieobject(GIO)

Als een GIO per abuis is meegeleverd met een besluit, terwijl dat niet de bedoeling was, kan een informatieobject teruggetrokken worden. De rectificatie moet daartoe:

- de regeling corrigeren, waarbij in de informatieobjecten bijlage de verwijzing naar het GIO wordt verwijderd;
- gecorrigeerde BesluitMetadata zonder verwijzing naar de GIO-versie;
- ConsolidatieInformatie met als: 
  - BeoogdeRegeling de gecorrigeerde regeling;
  - Terugtrekking de GIO-versie;
- de rectificatie zelf opstellen.



## Gecorrigeerde regeling

Het BG begint met het opstellen van de gecorrigeerde regeling of regelingen als het besluit meerdere regelingen betrof. Per STOP module van de regeling wordt hieronder aangegeven of, en zo ja, wat er aangepast moet worden:

- [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): Een rectificatie leidt tot een nieuwe versie van de regeling en dus een nieuwe identificatiemodule met een aangepaste FRBR-Expression.
- [RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata): Het aanpassen van de (versie van) meegeleverde GIO's heeft geen invloed op de RegelingMetadata. Omdat de RegelingMetadata een annotatie is, en de metadata dus geldig blijft totdat een nieuwe versie wordt aangeleverd, bevat deze rectificatie geen nieuwe RegelingMetadata. 
- [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata): Het versienummer van de gecorrigeerde regeling moet altijd meegegeven worden met de rectificatie.

- Regelingtekst: [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact), [RegelingVrijeTekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) of [RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel). De correcties worden aangebracht in het GIO verwijzing(en) in de informatieobjecten-bijlage van de regelingtekst. Aan de hand van de originele en gecorrigeerde regelingtekst worden de correcties in [renvooi](bepalen_wijzigingen_renvooi.md) opgesteld, zodat deze straks als regelingmutatie opgenomen kunnen worden in de rectificatie.

- [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties): Indien er correcties nodig zijn in de Toelichtingsrelaties moet een gecorrigeerde Toelichtingsrelatie-module aangeleverd worden. Omdat de Toelichtingsrelaties annotaties zijn, hoeft de rectificatie geen nieuwe Toelichtingsrelatie-module te bevatten als er geen correcties zijn.



## Gecorrigeerde besluitmodules

De correctie(s) hebben gevolgen voor de met het besluit meegeleverde informatie. Om het besluit te corrigeren is nodig:

- [ProcedureverloopMutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie): Geen aanpassingen, niet meeleveren.

- [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie): Omdat met het terugtrekken van het GIO het beoogd informatieobject verdwijnt en de beoogde regeling wijzigt, moet de consolidatieinformatie worden aangepast met een  nieuwe versie van de [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) en een [Terugtrekking van het BeoogdInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject):
  *BeoogdeRegeling*:

  - [doel](data_xsd_Element_data_doel.dita#doel): identiek aan het doel in het besluit;
  - [instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie): nieuwe regelingversie die ontstaat door de rectificatie;
  - [eId](data_xsd_Element_data_eId.dita#eId): van de rectificatie waar de regelingmutatie wordt aangekondigd, bijv. `content_o_4`;

  *Terugtrekking:*

  * [doel](data_xsd_Element_data_doel.dita#doel): identiek aan het doel in het besluit;
  * [instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie): de informatieobject-versie die is ontstaat door de rectificatie;
  * [eId](data_xsd_Element_data_eId.dita#eId): van de verwijder-actie in de regelingmutatie in de rectificatietekst bijv. `!rectificatiergeling#cmp_A__content_o_4__list_1__item_1__ref_o_1`;

- [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata): Er hoort nu een GIO minder bij het besluit, dus de [informatieobjectRef](data_xsd_Element_data_informatieobjectRef.dita#informatieobjectRef) van deze GIO in de BesluitMetadata moet verwijderd worden.



## De rectificatie zelf

De rectificatie zelf bestaat uit:

* [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van de rectificatie zelf. De rectificatie van een besluit is een zelfstandig Work en elke rectificatie bij hetzelfde besluit is een nieuwe expressie van het rectificatie Work;

* [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata): Net als bij een besluit moet er bij een rectificatie altijd metadata aangeleverd worden. De RectificatieMetadata lijkt erg op de BesluitMetadata, maar heeft een extra veld [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert);

* [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie): De rectificatie bestaat uit:
  * Een [regeling opschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift), bijv. 'Rectificatie van de Verordening Toeristenbelasting 2017'

  * Het [lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met één of meerdere:
    * [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)(en) met 
      * een [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop);
      * een of meer toelichtende [alinea](tekst_xsd_Element_tekst_Al.dita#Al)'s;
      * de [regelingmutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie).
      
        De regelingmutatie is identiek aan een 'normale' regelingmutatie in [renvooi](bepalen_wijzigingen_renvooi.md). De *was* van de mutatie is de regelingversie die ontstaan is door het besluit. De *wordt* is de regelingversie die ontstaat uit de rectificatie. LET OP: er worden voor de rectificatie wel afwijkende standaard zinnen gebruikt om de [*wat*](tekst_xsd_Element_tekst_Wat.dita#Wat) aan te duiden. En er is precies één regelingmutatie gecorrigeerde regeling.

  Zie voor een uitgebreid rectificatie-voorbeeld met een GIO correctie de [Pyrodam rectificatie](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03%20Rectificatie%20besluit/index.md). In dit voorbeeld wordt geen GIO teruggetrokken.

