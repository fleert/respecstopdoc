# data:ExpressionIdentificatie (kennisgeving)

Deze module voorziet in de identificatie van de kennisgeving en volgt hierin de De [Akoma Ntoso](https://docs.oasis-open.org/legaldocml/akn-nc/v1.0/os/akn-nc-v1.0-os.html) [naamgevingsconventie](naamgevingsconventie.md) (AKN NC). 

| Element | Vulling   | Verplicht? |
| ----- | ------ | ---------- |
| [FRBRWork](data_xsd_Element_data_FRBRWork.dita#FRBRWork)             | Zie 'AKN IRI - Kennisgeving'    | N   |
| [FRBRExpression](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression) | Zie 'AKN IRI - Kennisgeving'    | J   |
| [soortWork](data_xsd_Element_data_soortWork.dita#soortWork)           | Voor een aangeleverde kennisgeving dient deze de waarde `'/join/id/stop/work_023'` te hebben | J |

## AKN IRI - kennisgeving

Voor de identificatie van het aangeleverde kennisgeving dienen de AKN IRIs als volgt te zijn opgebouwd (uitgaande van een Nederlands bevoegd gezag). Kennisgeving gebruiken het documenttype "`doc`" 

`//FRBRWork: "/akn/nl/doc/" <overheid> "/" <datum_work> "/" <overig> `

`//FRBRExpression: <FRBRWork> "/" <taal> "@" <datum_expr> [ ";" <versie> ] [ ";" <overig> ]`

[Toelichting](publicatie_naamgeving.md)


## Voorbeeld
[Voorbeeld ExpressionIdentificatie (Kennisgeving)](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-Identificatie.xml)