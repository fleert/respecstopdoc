# Directe mutatie

STOP/ [TPOD](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) biedt in bepaalde gevallen de mogelijkheid om een directe mutatie aan te leveren. Dat kan alleen in situaties waarin er geen sprake is van rechtsgevolg. Directe mutaties kunnen gebruikt worden om wijzigingen in de service-informatie bij het omgevingsdocument of in de opmaak door te voeren. Met de term service-informatie (ook wel [Regelgeving-gerelateerde informatie](begrippenlijst_regelgevingGerelateerdeInformatie.dita#regelgevingGerelateerdeInformatie)genoemd) wordt gedoeld op de annotaties met Ow-objecten, die dienen om het omgevingswetdocument goed te kunnen ontsluiten in DSO-LV. Een voorbeeld hiervan zijn de annotaties op activiteit of thema. Deze zitten in het Ow-deel van het omgevingsdocument.

## Directe mutatie van service-informatie  

Dit wil zeggen dat er een wijziging wordt aangebracht in de service-informatie, oftewel de annotaties met OW-objecten, zonder dat de tekst van de regels c.q. het beleid dan wel het gebied waarin deze van toepassing zijn wijzigt. Hiervan is bijvoorbeeld sprake wanneer een provincie besluit om aan een reeds gepubliceerde omgevingsverordening alsnog OW-annotaties als thema en activiteit toe te voegen, of deze te wijzigen. Een directe mutatie van de service-informatie heeft geen rechtsgevolg, en mag daarom worden uitgevoerd zonder dat hiervoor een [besluit in de zin van de Algemene wet bestuursrecht](awb-besluit.md) wordt genomen. De mutatie hoeft daarom ook niet bekend te worden gemaakt in een elektronisch publicatieblad. Deze mutaties moeten wel worden aangeleverd via de LVBB, maar worden rechtstreeks doorgeleverd aan DSO-LV zonder bekendmaking. Let wel: dit geldt zolang alleen service-informatie gewijzigd wordt! Zodra er ook iets in de juridische informatie moet veranderen, zoals een aanpassing in de tekst van de regels of het beleid, dan mag dit niet door middel van een directe mutatie worden uitgevoerd. Er moet dan een wijzigingsbesluit of een geheel nieuw besluit worden genomen. 

## Directe mutatie van opmaak  

Ook de opmaak van een regeling mag door middel van een directe mutatie gewijzgd worden. Bijvoorbeeld tekst vet of schuingedrukt maken en het toevoegen van links naar andere plaatsen binnen en buiten de tekst. 



