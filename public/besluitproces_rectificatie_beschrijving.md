# Inleiding op rectificaties

## Wat is een rectificatie?

Een rectificatie is een correctie van een publicatie van een besluit. De rectificatie wordt in hetzelfde publicatieblad geplaatst als de oorspronkelijke publicatie.

## Wanneer wordt een rectificatie gebruikt?

Het kan voorkomen dat de bekendmaking of publicatie van een besluit niet de correcte inhoud van dat besluit weergeeft. Een voorbeeld hiervan is het abusievelijk niet verwerken van op het laatste moment aangenomen amendementen in de vaststelling van een besluit; deze amendementen zullen dus per abuis ontbreken in de gepubliceerde versie van dat besluit.

Het is niet nodig om de gehele (correcte) versie van het besluit opnieuw te publiceren. In plaats daarvan wordt een rectificatie gepubliceerd. In de rectificatie wordt aangegeven welke onderdelen van de oorspronkelijke publicatie verkeerd zijn, en hoe ze hadden moeten luiden. Als er daarna nogmaals verschillen aan het licht komen tussen de publicatie van het besluit (inclusief rectificatie) en het besluit zoals het is vastgesteld, dan worden die verschillen gepubliceerd in een volgende rectificatie.

## Wanneer wordt géén rectificatie gebruikt?

Een rectificatie betreft correcties van de *gepubliceerde* *juridische* inhoud van het besluit, zoals correcties van de tekst of van de informatieobjecten.

Als er een niet-juridische fout ontdekt wordt in de geconsolideerde regeling of een informatieobject dat door de inwerkingtreding van het besluit ontstaan is, kan dat hersteld worden door een [revisie](consproces_revisie.md). Denk bijvoorbeeld aan het toevoegen van een toelichting of een verwijzing/link in de tekst van de geconsolideerde regeling. 

Als het BG een fout ontdekt in een besluit vóórdat dit is gepubliceerd, dan kan het BG de publicatieopdracht [*afbreken*](https://koop.gitlab.io/lvbb/bronhouderkoppelvlak/Afbreken_Publicatieopdracht.html); vervolgens kan het BG alsnog het correcte besluit aanleveren. Als het besluit wel al is gepubliceerd, dan kunnen fouten in de juridische inhoud alleen met een rectificatie worden hersteld.

## Wat doet een rectificatie?

Indien het door het BG *genomen* besluit niet correct wordt weergegeven in het officieel *gepubliceerde* besluit, past een rectificatie de juridische werking van het *gepubliceerde* besluit aan. Een rectificatie is dus zelf geen besluit, maar zorgt ervoor dat het genomen besluit alsnog de juiste juridische werking krijgt door het gepubliceerde besluit te corrigeren. 

## Wat is het juridische effect van een rectificatie?

De publicatie van de rectificatie geeft aan dat de eerder gepubliceerde versie van het besluit incorrect is. Informatie die afgeleid is van de eerder gepubliceerde versie moet daarom opnieuw bepaald worden. Bijvoorbeeld:

* Correctie van een inwerkingtredingsdatum zal leiden tot een veranderde geldigheid van [toestanden](begrippenlijst_toestand.dita#toestand) en mogelijk zelfs tot andere toestanden.

* Correctie van de tekst van een regeling of de inhoud van een informatieobject kan leiden tot een inhoudelijke aanpassing van de geconsolideerde regeling. Dit kan doorwerken in een aanpassing van de service-informatie behorend bij de toestanden.

Deze gevolgen kunnen via [de geautomatiseerde consolidatie](EA_81311E5C8AD34b35B153B21B50080288.dita#Pkg) bepaald worden.

Een rectificatie kan ook leiden tot een gecorrigeerde versie van de tekst van het besluit dat geen onderdeel is van een regeling, bijvoorbeeld het besluitopschrift of de toelichting op het besluit.

## Wat is het effect van een rectificatie op de publicatie van het besluit?

De publicatie die het besluit op een incorrecte manier weergeeft wordt niet gewijzigd. Bij de rectificatie wordt wel het verband met de te rectificeren besluitversie vastgelegd via de relatie [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert). Deze relatie wordt bij de officiële publicatie van de rectificatie vertaald in een relatie naar de eerdere publicatie van het besluit.

De correcties beschreven in de rectificatie kunnen worden toegepast op de eerder gepubliceerde (incorrecte) versie van het besluit om te komen tot een nieuwe (correcte) versie van het besluit. De modellering van de rectificatie in STOP is niet ontworpen om dit (geautomatiseerd) te kunnen uitvoeren.

