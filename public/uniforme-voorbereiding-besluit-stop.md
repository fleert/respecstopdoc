# **Uniforme openbare voorbereidingsprocedure in STOP** {#concept_bg1_hd4_gqb}

Op de meeste besluiten die via de LVBB door STOP zullen worden bekendgemaakt is de [Uniforme Openbare Voorbereidingsprocedure (afd. 3.4 Awb)](uniforme-voorbereiding-besluit.md) van toepassing.

Dit betekent dat een besluit de volgende stappen doorloopt:

1.  **Bekendmaking van een ontwerpbesluit**, dat wil zeggen een besluit met de status ‘ontwerp’. Dit is bijvoorbeeld een ontwerp-omgevingsverordening van een provincie. Deze kan via de Viewer Regels op de Kaart in DSO-LV ter beschikking worden gesteld, dit is voldoende om te voldoen aan de verplichting tot ter inzagelegging op grond van de Algemene wet bestuursrecht.
2.  **Verwerking van zienswijzen**. Belanghebbenden kunnen zienswijzen indienen bij het bestuursorgaan op de door het bestuursorgaan aangegeven wijze. Het bestuursorgaan moet deze zienswijzen verwerken in de definitieve versie van het besluit. Dit is een intern proces van het bestuursorgaan en leidt niet tot handelingen die door STOP worden ondersteund.
3.  **Vaststelling en bekendmaking definitief besluit**. Het bestuursorgaan stelt vervolgens een definitieve versie van het besluit vast, in dit voorbeeld dus de definitieve omgevingsverordening, en maakt deze bekend via de LVBB. In de toelichting bij het definitieve besluit moet worden ingegaan op hoe is omgegaan met de binnengekomen zienswijzen. De definitieve versie van het besluit behelst vaststelling van een nieuwe versie van de regeling met de status ‘definitief’. Hierbij hoeft niet door middel van mutaties precies te worden aangegeven wat er gewijzigd is ten opzichte van het ontwerp. Dat is ook niet nodig, want de hele definitieve versie staat open voor beroep.

