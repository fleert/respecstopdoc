# Tekstweergave

## Geschikt voor weergave op A4

De STOP tekstmodellen zijn zo ontworpen dat ze gebruikt kunnen worden voor weergave in uiteenlopende media, zoals websites of PDF documenten. Alle teksten die in STOP worden gecodeerd zullen uiteindelijk in een of andere vorm deel uitmaken van een officiële publicatie. De publicatie wordt opgemaakt volgens de voorschriften van het publicatieblad en moet dan goed leesbaar zijn, zelfs als er een [papieren afschrift](https://wetten.overheid.nl/jci1.3:c:BWBR0004287&artikel=18) van gemaakt wordt.

De consequentie is dat voor afbeeldingen en grote tabellen rekening gehouden moet worden met de zetspiegel van een pagina (155mm breed en 240mm hoog) in een officiële publicatie.

![Zetspiegel](img/Zetspiegel.png)

Tabellen mogen niet zodanig groot zijn dat ze bij gebruik van een normaal lettertype niet op een pagina passen zonder aan leesbaarheid in te boeten. [Afbeeldingen](regeltekst_afbeelding.md) met een detailniveau dat op een papieren afschrift niet voor eenieder te onderscheiden zijn, mogen daarom geen deel uitmaken van de tekst van de regeling.

## Tekstmodel als basis voor weergave

De standaard kent geen mechanisme om de opmaak van een tekst onafhankelijk van de inhoud van de tekst vast te leggen. De weergave van de tekst moet en kan afgeleid worden van het type tekstelement waarmee een bepaalde tekst in STOP is gecodeerd. STOP-gebruikende software dient een strategie te hebben voor de vertaling van het tekstelement naar de opmaak van de tekst.

Bij het ontwerp van de STOP tekstmodellen worden voor de verschillende [typen tekstelementen](regeltekst_structurering.md) de volgende uitgangspunten gehanteerd: 

* De software die de tekst weergeeft bepaalt het lettertype waarmee de tekst wordt weergegeven. STOP bevat geen mogelijkheid om een lettertype af te dwingen. Ook bevat STOP geen beperkingen voor de tekenset die in een tekst voor kan komen. STOP staat het gebruik van alle tekens toe waarvoor een UTF-code bestaat. Een viewer moet dan ook alle tekens weer kunnen geven.
* De broodtekst wordt in principe in de hele tekst met hetzelfde lettertype (font) en -grootte (corps) getoond, als doorlopende tekst. De weergave mag echter afhankelijk zijn van het type inhoudelement; zo kan bijvoorbeeld voor de aanhef en voor een artikel een ander lettertype gekozen worden.
* De [inline-elementen](regeltekst_inline.md) voor tekstweergave worden gehonoreerd.
* De weergave van een [bloktekst-element](regeltekst_bloktekst.md) wijkt af van de weergave van de broodtekst op een manier die recht doet aan de functie van het element.
* Een tussenkop of een kop die onderdeel is van een inhoudelement (bijvoorbeeld Artikel of Artikelsgewijze Toelichting) krijgt een opmaak die gebaseerd is op de weergave van de broodtekst maar die voldoende afwijkt om het verschil te zien. Bijvoorbeeld een iets grotere letter, extra witruimte, en/of een ander lettertype.
* De koppen van structuurelementen krijgen een opmaak die afhankelijk is van de hiërarchische structuur. Dus van de positie van het element in de tekst, en niet van het type element. De kop van een structuurelement dat in een ander structuurelement voorkomt wordt bijvoorbeeld met een kleinere letter opgemaakt dan de kop van het andere structuurelement, en de letter van de kop geassocieerd met het laagste hiërarchische niveau is nog juist groter dan een tussenkop.

## Implementatiekeuze

De standaard stelt als enige eis dat de weergave van de tekst voldoet aan bovenstaande uitgangspunten. STOP-gebruikende software heeft de vrijheid om in aanvulling daarop eigen keuzes te maken. Dat geldt in het bijzonder voor de niet-juridische elementen of eigenschappen. Bijvoorbeeld:

* [Links en hints](regeltekst_inline.md#Linkhints) mogen weggelaten worden.
* Hoe een link weergegeven wordt en waar een hyperlink uitkomt valt onder de implementatievrijheid.
* De oriëntatie van een tabel mag genegeerd worden.

