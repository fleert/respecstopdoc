# Tekstmodel STOP 2.x 

Evaluatie van het STOP 1.x tekstmodel heeft geleid tot de onderstaande verbetervoorstellen.
Het gaat om een drietal noodzakelijk gebleken uitbreidingen en verder om het "verstrakken" van het model: verwijderen van overbodige elementen, vereenvoudigen van onnodig complexe structuren en beletten van ongeoorloofd gebruik van elementen in bepaalde tekstdelen.
Indien akkoord worden deze aanpassingen in de 2.0 versie van STOP en de TPOD's opgenomen. In STOP gebeurt dat ofwel door schemawijziging of door bedrijfsregels, die ook de mogelijkheid bieden gebruik te ontraden i.p.v. af te keuren. 
Verwachting is dat geen van de te verwijderen mogelijkheden nu daadwerkelijk gebruikt wordt, behalve mogelijk in enkele _legacy_ Rijkspublicaties, waarvoor gebruik in toekomstige publicaties dan ontraden zal worden. 
Ter voorbereiding zullen werkafspraken worden gemaakt. 





## Uitbreidingen

| Nr | Omschrijving | Toelichting | Akkoord |
|-----|-----|-----|-----|
|1.| [LidNummer](tekst_xsd_Element_tekst_LidNummer.dita#LidNummer): Superieur (`<sup>`) en inferieur (`<sub>`) toevoegen | Nodig voor lidnummers als 1.<sup>quater</sup> (Rijksscenario)| |
|2.| [Slotformulering](tekst_xsd_Element_tekst_Slotformulering.dita#Slotformulering): [`Figuur`](tekst_xsd_Element_tekst_Figuur.dita#Figuur) toevoegen | Nodig voor bijvoorbeeld een gescande handtekening |  |
|3.| [IntRef](tekst_xsd_Element_tekst_IntRef.dita#IntRef)/[ExtRef](tekst_xsd_Element_tekst_ExtRef.dita#ExtRef): Cursivering [`<i>`](tekst_xsd_Element_tekst_i.dita#i) toevoegen | Maakt consistente weergave van links in gecursiveerde tekst mogelijk  |  |

## Opschonen tekstmodellen Instrumenten (gebruik structuurelementen)

| Nr | Omschrijving |  Toelichting | Actie |
|-----|-----|-----|-----|
|1.| [InleidendeTekst](tekst_xsd_Element_tekst_InleidendeTekst.dita#InleidendeTekst) niet meer gebruiken | Element [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) volstaat in alle gevallen | Weghalen/Ontraden |
|2.| [Inhoudsopgave](tekst_xsd_Element_tekst_Inhoudsopgave.dita#Inhoudsopgave) niet meer gebruiken. | Niet meer relevant bij digitale publicatie. Is in TPOD's al verboden. | Weghalen |
|3.| [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) niet meer toestaan in [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) | Rijksregelgeving kent geen te consolideren toelichtingen | Weghalen/Ontraden (Afhankelijk van Natura2000 legacy) |
|4.| [InwerkingtredingArtikel](tekst_xsd_Element_tekst_InwerkingtredingArtikel.dita#InwerkingtredingArtikel) alleen toestaan in RegelingKlassiek | Element is alleen vereist voor de consolidatie van RegelingKlassiek | Weghalen |
|5.| [ArtikelsgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) buiten container [`Toelichting`](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) niet meer gebruiken | Huidig toelichtingsmodel is onnodig complex | Al akkoord (Ontraden)|
|6.| [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef) en [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting) niet meer toestaan in [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek) | Redundant: beide elementen worden alleen gebruikt in [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) | Weghalen  |
|7.| [Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage) ontraden in [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek) | Wordt alleen gebruikt bij Pons (vermelding GIO) | Ontraden |

## Aanscherpen gebruik STOP tekstsoorten

### Inhoudselementen

| Nr | Omschrijving | Toelichting | Actie |
|-----|-----|-----|-----|
|1.| [Slotformulering](tekst_xsd_Element_tekst_Slotformulering.dita#Slotformulering): geen [`Lijst`](tekst_xsd_Element_tekst_Lijst.dita#Lijst) en [`Groep`](tekst_xsd_Element_tekst_Groep.dita#Groep) gebruiken | Voorschreven model is een enkelvoudige zin. Zie [AvdR §4.7](https://wetten.overheid.nl/BWBR0005730/2022-04-01#Hoofdstuk4_Paragraaf4.7) |  |
|2.| [ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) moet een (klein-)kindelement van [Inhoud](tekst_xsd_Element_tekst_Inhoud.dita#Inhoud) zijn | ExtIoRefs mogen alleen gebruikt worden in Artikel, Lid of Divisietekst | Weghalen |

### Bloktekst

Zie de beschrijving bij [soort tekstelement: Bloktekst](regeltekst_bloktekst.md).

| Nr | Omschrijving | Toelichting | Actie |
|-----|-----|-----|-----|
|1.| [ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) (binnen `Inhoud`) ook niet toestaan in de tekstelementen in [Figuur](tekst_xsd_Element_tekst_Figuur.dita#Figuur) en [Formule](tekst_xsd_Element_tekst_Formule.dita#Formule) (`Titel`, `Bijschrift` en `Bron`) en in de [Lijst](tekst_xsd_Element_tekst_Lijst.dita#Lijst)-elementen `Lijstaanhef` en `Lijstsluiting` | Geen passende plaatsen voor het vermelden van een informatieobject | Weghalen |
|2.| [Begrippenlijst](tekst_xsd_Element_tekst_Begrippenlijst.dita#Begrippenlijst) niet meer toestaan in [Citaat](tekst_xsd_Element_tekst_Citaat.dita#Citaat), [Kadertekst](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst) en [Li](tekst_xsd_Element_tekst_Li.dita#Li) | Geen geëigende context voor Begrippenlijst, mogelijk conflicterende presentatie | Weghalen |
|3.| [Lijst](tekst_xsd_Element_tekst_Lijst.dita#Lijst)-elementen binnen [Noot](tekst_xsd_Element_tekst_Noot.dita#Noot): gebruik van `Figuur`, `Formule`, `Begrippenlijst` verbieden? | Gebruik van uitgebreide noten in (digitale) teksten ontmoedigen | Ontraden|
|4.| `Figuur/Formule/Begrippenlijst/table` mogen geen (klein)kindelement zijn van [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef) (incl. `Considerans` en `Afkondiging`), [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst), [Definitie](tekst_xsd_Element_tekst_Definitie.dita#Definitie) en evt. [Noot](tekst_xsd_Element_tekst_Noot.dita#Noot) (zie vraag hieronder) | Eerstgenoemde elementen passen niet bij de functie van genoemde tekstblokken | Weghalen |
|5.| Wordt [`table`](tekst_xsd_Element_tekst_table.dita#table) binnen een lijstitem ([Li](tekst_xsd_Element_tekst_Li.dita#Li)) gebruikt? | Ongebruikelijk, complicerend voor opmaak | Ontraden (legacy Rijk?)|
|6.| Tabel moet min. 2 kolommen en 2 rijen moet hebben | Weergave is anders niet voorspelbaar. [Kadertekst](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst) is het alternatief voor een tabel met één cel | Ontraden |

### Koptekst

Zie de beschrijving bij [soort tekstelement: Koppen](regeltekst_koppen.md).

| Nr | Omschrijving | Toelichting | Actie |
|-----|-----|-----|-----|
|1.| Nadrukelementen m.u.v. `i` niet toestaan in kop- en titelelementen. | Nadruk anders dan `<i>` kan conflicteren met Kopopmaak | Weghalen |
|2.| In [Tussenkop](tekst_xsd_Element_tekst_Tussenkop.dita#Tussenkop) zijn geen nadrukelementen toegestaan, ook `i` niet. | Nadruk kan conflicteren met Kopopmaak | Weghalen |
|3.| [Links](regeltekst_inline.md#Linkhints) niet toestaan in Koppen | Niet goed verenigbaar met functie en opmaak van koppen | Weghalen |

### Inlines

Zie de beschrijving bij [soort tekstelement: Inline](regeltekst_inline.md).

| Nr | Omschrijving | Toelichting | Actie |
|-----|-----|-----|-----|
|1.| Nadrukelementen niet recursief gebruiken | Opmaaksoftware kan niet omgaan met bijv. dubbel `<b>`. Combinaties zijn wel toegestaan. | Niet toestaan |
|2.| Geen links in links toestaan; ook niet via [NieuweTekst](tekst_xsd_Element_tekst_NieuweTekst.dita#NieuweTekst)/[VerwijderdeTekst](tekst_xsd_Element_tekst_VerwijderdeTekst.dita#VerwijderdeTekst). Zie [Inline tekstelementen](regeltekst_inline.md#Linkhints) | Nesting van links resulteert in conflicterend browsergedrag | Niet toestaan |
|3.| Geen inline-elementen toestaan in [ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef)  | Controle op correcte syntax JOIN-ID | ook in schema onmogelijk maken, nu al via [STOP0012](https://koop.gitlab.io/STOP/standaard/1.3.0/businessrules_STOP_0.html#Bedrijfsregels__br_STOP0012) |
|4.| Geen harde regelovergang (`<br>`) in links toestaan | multi-line link is niet gewenst | Ontraden |
|5.| Moet het gebruik van element [abbr](tekst_xsd_Element_tekst_abbr.dita#abbr) ontraden worden? | Wordt niet gebruikt in de Officiële Publicatie. Doet de viewer er iets mee? | Weghalen/ontraden? |
|6.| Afkorting ([abbr](tekst_xsd_Element_tekst_abbr.dita#abbr)) heeft verplicht een omschrijving (attribuut `@title`) | Indien `abbr` behouden blijft. Zonder omschrijving is de afkorting zinloos | Verplicht maken |
|7.| [Noot](tekst_xsd_Element_tekst_Noot.dita#Noot) binnen `Noot` (recursief) is niet toegestaan | Gebruik is niet zinvol | Ontraden |
|8.| [Noot](tekst_xsd_Element_tekst_Noot.dita#Noot): attribuut `@type` verwijderen | Eindnoot en margenoot worden niet ondersteund. Alle noten zijn impliciet voetnoten, behalve in een tabel (tabelnoten). | Weghalen |
|9.| [InlineTekstAfbeelding](tekst_xsd_Element_tekst_InlineTekstAfbeelding.dita#InlineTekstAfbeelding) bij voorkeur niet gebruiken | Elk UTF-8 symbool kan gebruikt worden in STOP. Een InlineTekstAfbeelding schaalt niet mee met het weergave font. Zie [inline elementen](regeltekst_inline.md#InlineTekstAfbeelding) Voor afbeeldingen Figuur en voor formules Formule gebruiken.	| Ontraden |

### Aantekeningen

Zie de beschrijving bij [soort tekstelement: Aantekening](regeltekst_aantekening.md).

| Nr | Omschrijving | Toelichting | Actie |
|-----|-----|-----|-----|
|1.| [Redactioneel](tekst_xsd_Element_tekst_Redactioneel.dita#Redactioneel) niet meer gebruiken | Niet relevant in STOP, voor redactionele consolidatie. | Ontraden (mogelijk wel relevant t.b.v. Rijksscenario's) |
