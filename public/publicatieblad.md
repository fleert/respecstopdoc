# Publicatieblad

In STOP wordt onderscheid gemaakt tussen een te publiceren document (een besluit, kennisgeving of mededeling) zoals aangeleverd door het bevoegd gezag aan het Bronhouder Koppelvlak ([BHKV](https://koop.gitlab.io/lvbb/bronhouderkoppelvlak/)) en de publicatie van dat document door de Landelijke Voorziening Bekendmaken en Beschikbaarstellen ([LVBB](https://iplo.nl/digitaal-stelsel/introductie/landelijke-voorziening/lvbb/)). 

Dit onderscheid komt tot uiting in de modellering van een officiële publicatie. De publicatie in het publicatieblad komt grotendeels overeen met de feitelijke inhoud van de publicatie, maar de publicatie wijkt op een aantal punten af:

Naamgeving van een publicatie
: De publicatie heeft een eigen identificatie. STOP beschrijft de toepassing van de Akoma Ntoso naamgevingsconventie voor [publicaties](publicatieblad_naamgeving.md).

Tekstmodel van een officiële publicatie 
: Het [tekstmodel van een officiële publicatie](publicatieblad_tekstmodel.md) dat het publicatieblad met de inhoud van het te publiceren document bevat. 

Daarnaast heeft een publicatie:

Metadata en VersieMetadata
: De [metadata en versiemetadata van een publicatieblad](publicatieblad_metadata.md) bevat zowel informatie over de publicatie zelf (blad, jaargang,  etc.) als informatie die is overgenomen van het te publiceren document (titel, eindverantwoordelijke, etc.) alsook informatie uit het bijbehorende procedureverloop (Ondertekening, termijnen).

