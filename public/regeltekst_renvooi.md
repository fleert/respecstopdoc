# Soort tekstelement: Renvooi

De 'Renvooi'-tekstelementen zijn elementen die in een besluit gebruikt worden om wijzigingen in een regeling aan te brengen. Zie voor een volledige beschrijving van de werking: [renvooitekst](renvooitekst.md).

## Renvooi elementen

[BesluitMutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie), [NieuweTekst](tekst_xsd_Element_tekst_NieuweTekst.dita#NieuweTekst), [OpmerkingVersie](tekst_xsd_Element_tekst_OpmerkingVersie.dita#OpmerkingVersie), [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie), [Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang), [VervangKop](tekst_xsd_Element_tekst_VervangKop.dita#VervangKop), [VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling), [Verwijder](tekst_xsd_Element_tekst_Verwijder.dita#Verwijder), [VerwijderdeTekst](tekst_xsd_Element_tekst_VerwijderdeTekst.dita#VerwijderdeTekst), [VoegToe](tekst_xsd_Element_tekst_VoegToe.dita#VoegToe), [Wat](tekst_xsd_Element_tekst_Wat.dita#Wat)

De indeling van `OpmerkingVersie` als renvooi-element is arbitrair. Zou ook als een [Aantekening](regeltekst_aantekening.md)-element gezien kunnen worden. De tekst in `OpmerkingVersie` wordt opgesteld door een wetgevingsjurist of consolidatieredacteur, wordt niet getoond in de bekendmaking, maar komt terug in de consolidatie.
