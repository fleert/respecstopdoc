# Functioneel presenteren

STOP hanteert het principe van functioneel presenteren: alleen als het een functie heeft om de presentatie voor te schrijven is het onderdeel van de standaard. In alle andere gevallen is het aan de applicatie die de standaard gebruikt om de presentatie te bepalen. Dus lettertype, fontgrootte, kleur, et cetera. zijn alleen onderdeel van de standaard als daar betekenis aan gehecht wordt. 

Het is bijvoorbeeld irrelevant of de tekst van een besluit in zwarte of blauwe inkt is weergegeven, omdat alleen de tekst drager van de informatie is. De kleur van de tekst kan daarom niet in STOP opgegeven worden. Het aangeven van een accent aan tekst, bijvoorbeeld door de tekst vetgedrukt weer te geven, of het maken van een onderscheid tussen gewone tekst en een kopje is wel functioneel. Dat kan daarom in het tekstmodel van STOP wel aangegeven worden. Welke lettertype, fontgrootte, enzovoorts gebruikt moet worden voor het weergeven van een koptekst is dan weer geen onderdeel van STOP omdat dit (mede)afhankelijk is van het medium dat gebruikt wordt voor het weergeven van de tekst.

Kleurgebruik kan in andere omstandigheden wel functioneel zijn. Zo kunnen de kleuren die gebruikt zijn om verschillende geluidzones te verbeelden bij de vaststelling van een [geografisch informatieobject](gio-intro.md) wel relevant zijn om misinterpretatie te voorkomen, bijvoorbeeld rood=hoge geluidsbelasting, groen=lage geluidsbelasting.


