# Over beroep

Rechtsbescherming voor besluiten is geregeld in de [Algemene wet bestuursrecht (Awb)](https://wetten.overheid.nl/BWBR0005537). Beroep is een van de middelen waarmee burgers, bedrijven en anderen besluiten kunnen aanvechten. Niet tegen elk besluit kan beroep worden ingesteld. Wanneer dat wel of niet mogelijk is wordt geregeld door de Awb. Voor de besluiten van de Omgevingswet is op de [Aan de Slag-site](https://aandeslagmetdeomgevingswet.nl/regelgeving/procedures/inspraak-rechtsbescherming-omgevingswet/) een overzicht te vinden.

## Beroepsprocedure
Een deel van de besluiten van het BG staat open voor [beroep](beroep.md). De bestuursrechtelijke beroepsprocedure van voor beroep vatbare besluiten bestaat uit de volgende stappen:  
1. Besluiten waartegen beroep mogelijk is, moeten door het BG worden voorzien van een [**kennisgeving**](begrippenlijst_kennisgeving.dita#kennisgeving) **met de beroepstermijn** die van toepassing is. Dat is een wettelijke verplichting. De einddatum van de beroepstermijn van het besluit staat vermeld in de kennisgeving en wordt ook als metadata bij de kennisgeving meegeleverd. Zie [herroepelijkheid](besluitproces_rechterlijkemacht_herroepelijkheidbesluit.md) voor een toelichting hoe het BG dit doorgeeft.
1. Tijdens de **beroepstermijn** wordt het **beroep aangetekend** bij bijvoorbeeld de rechtbank. Het BG hoeft hiervan geen kennisgeving te doen, maar moet dit wel doorgeven aan de LVBB (zie [herroepelijkheid](besluitproces_rechterlijkemacht_herroepelijkheidbesluit.md)).
1. **Beroepsprocedure**: de procedure in de rechtbank of een andere instelling die optreedt als beroepsorgaan.
1. **Uitspraak beroepsorgaan**: de beroepsprocedure heeft een uitspraak (en eventueel een tussenuitspraak) tot gevolg die oordeelt of het beroep (gedeeltelijk) gegrond of ongegrond is. Als het beroep (gedeeltelijk) gegrond is, dan heeft dit mogelijk gevolgen voor de geldigheid van het besluit en moet het BG van de uitspraak een [kennisgeving](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving) doen. Zie [Vier soorten uitspraken](#Vier-soorten-uitspraken).
1. **Hoger beroep**: in deze procedurestap kan een eerder besluit van een beroepsorgaan worden aangevochten. Hoger beroep is niet bij alle besluiten mogelijk. Het kan bijvoorbeeld worden aangetekend bij de Afdeling bestuursrechtspraak van de Raad van State. 
1. Een besluit wordt pas [**onherroepelijk**](besluitproces_rechterlijkemacht_herroepelijkheidbesluit.md) als alle beroepsmogelijkheden zijn benut en er een definitieve uitspraak is. Na afloop van alle beroepsprocedures moet BG het einde daarvan melden aan de LVBB (zie [Procedurestappen](besluitproces_rechterlijkemacht_procedurele_status.md)); een kennisgeving daarvan is niet nodig.


## Vier soorten uitspraken

Een beroepsorgaan kan vier soorten uitspraken doen:

1. Beroep is **ongegrond**. Uitspraak heeft geen gevolgen voor de geldigheid of inhoud van de geconsolideerde regelgeving. In dit scenario heeft het BG de keuze om: 
   1.  Pas na het verstrijken van de beroepstermijn te bekijken of er lopende beroepen zijn. 
   1. Tijdens de beroepstermijn al door te geven dat een beroep (èn eventuele andere) beroepen ongegrond zijn verklaard.
1. Beroep is **gegrond** en wordt **vernietigd met behoud van rechtsgevolgen**. Dit houdt in dat het beroepsorgaan de onderbouwing in het besluit onvoldoende vindt, maar dat het BG het besluit tijdens de beroepsprocedure alsnog voldoende heeft onderbouwd. In dit geval hoeft het bevoegd gezag de geconsolideerde regelgeving niet aan te passen, maar moet het wel [kennisgeving](begrippenlijst_kennisgeving.dita#kennisgeving) doen van de uitspraak.
1. Beroep is **gegrond** en wordt **gedeeltelijk vernietigd**.  Dit houdt in dat het beroepsorgaan een gedeelte van het besluit ongeldig verklaart. Het bevoegd gezag moet de geconsolideerde regelgeving dan naar aanleiding van de uitspraak aanpassen, tenzij de uitspraak alleen betrekking heeft op de personen die het beroep ingesteld hebben. Ook moet het BG [kennisgeving](begrippenlijst_kennisgeving.dita#kennisgeving) doen van de uitspraak.
1. Beroep is **gegrond** en wordt **geheel vernietigd**. Dit houdt in dat het beroepsorgaan het besluit ongeldig verklaart. Het besluit heeft juridisch dan nooit bestaan. Het bevoegd gezag dat bronhouder is, moet de geconsolideerde regelgeving dan naar aanleiding van de uitspraak aanpassen. Ook moet het BG [kennisgeving](begrippenlijst_kennisgeving.dita#kennisgeving) doen van de uitspraak.






![Tijdslijnen bezwaar- en beroepprocedure(s)](img/BBtijdlijn.png "Tijdslijnen bezwaar- en beroepprocedure(s)")


## Zie ook
* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).
