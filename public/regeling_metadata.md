# Metadata van een regeling

Gegevens over een regeling, zoals de maker, onderwerpen, etc., de zogenaamde metadata wordt opgeslagen in twee modules:

1.  [data:RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata) 
2.  [data:RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata) 

De [data:RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata) bevat informatie die specifiek is voor een versie van de regeling: het [data:versienummer](data_xsd_Element_data_versienummer.dita#versienummer). 

De [data:RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata) bevat generieke metadata voor de regeling. Dit is een [annotatie](annotaties.md) die van toepassing kan zijn voor meerdere versies van de regeling. Deze metadata bestaat uit verschillende groepen informatie:

**Naam van de regeling**
Er kunnen verschillende manieren zijn om met een naam naar een regeling te verwijzen. Deze moeten in de metadata opgegeven worden om de regeling beter vindbaar te maken.

* De [data:officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel) is de titel van de regeling die in het [tekst:RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) in de tekst staat.
* Als de officiële titel erg lang is krijgt een regeling ook wel een [data:citeertitel](data_xsd_Element_data_citeertitel.dita#citeertitel). Deze kan in de tekst zijn aangegeven([data:isOfficieel](data_xsd_Element_data_isOfficieel.dita#isOfficieel)) of later gebruikelijk geworden zijn.
* Een regeling kan later "in de volksmond" een benaming krijgen die bekender is dan de officiële titel of citeertitel. Bijvoorbeeld de "wet Mulder". Zo'n naam kan als  [data:alternatieveTitel](data_xsd_Element_data_alternatieveTitel.dita#alternatieveTitel) vastgelegd worden.
* De regeling kan onder een of meer [data:afkortingen](data_xsd_Element_data_afkortingen.dita#afkortingen) bekend zijn, zoals Awb voor de Algemene Wet Bestuursrecht, Bkw voor de Bekendmakingswet, Ow voor de Omgevingswet. Als er meerdere zijn, dan zal er één de [data:voorkeursafkorting](data_xsd_Element_data_voorkeursafkorting.dita#voorkeursafkorting) zijn.

**Opsteller van de regeling**
Er zijn drie waarden die aangeven welk openbaar lichaam verantwoordelijk is voor het opstellen van de regeling:

* De [data:eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke) draagt de wettelijke eindverantwoordelijkheid voor de inhoud (strekking) van de regeling.
* De [data:soortBestuursorgaan](data_xsd_Element_data_soortBestuursorgaan.dita#soortBestuursorgaan) is een typering van het bestuursorgaan binnen het eindverantwoordelijk openbaar lichaam dat de wettelijke eindverantwoordelijkheid draagt.
* De [data:maker](data_xsd_Element_data_maker.dita#maker) draagt de eindverantwoordelijkheid voor het creëren van de inhoud van de regeling.

**Inhoud van de regeling**
Voor het vinden van de regeling is een karakterisering van de inhoud van de regeling nodig. Dit wordt vastgelegd via:

* [data:Onderwerpen](data_xsd_Element_data_onderwerpen.dita#onderwerpen), uit een waardelijst die afkomstig is van de LVBB portalen.
* [data:Overheidsdomeinen](data_xsd_Element_data_overheidsdomeinen.dita#overheidsdomeinen), uit een waardelijst die afkomstig is van de LVBB portalen. {::comment}Bewust gekozen voor 'LVBB' i.p.v, 'KOOP' (mogelijk niet duurzaam) of 'overheid.nl' (wel duurzaam){:/comment}
* [data:Rechtsgebieden](data_xsd_Element_data_rechtsgebieden.dita#rechtsgebieden), uit een waardelijst die afkomstig is van de LVBB portalen.

**Samenhang met andere regelingen**
Er zijn twee kenmerken die deze regeling inhoudelijk relateren aan andere regelingen.

* Als deze regeling één of meer andere regelingen vervangt, dan wordt in [data:opvolging](data_xsd_Element_data_opvolging.dita#opvolging) vastgelegd welke regelingen vervangen worden. Bij de inwerkingtreding van deze regeling trekt het BG de andere regelingen in. Dit maakt de regeling herkenbaar als opvolger van de oude regelingen.
* [data:grondslagen](data_xsd_Element_data_grondslagen.dita#grondslagen) geeft aan wat de juridische grondslagen van de regeling zijn en waar ze in de tekst vermeld zijn.

**Machine-leesbare typering van de regeling**
Voor elk soort regeling is een [toepassingsprofiel](toepassingsprofielen.md) opgesteld dat beschrijft hoe STOP (en eventuele andere standaarden) gebruikt moeten worden. De waarde van [data:soortRegeling](data_xsd_Element_data_soortRegeling.dita#soortRegeling) correspondeert met het toepassingsprofiel.