# Inwerkingtreding van een besluit

## Patroon
In het te publiceren juridisch document (bijvoorbeeld een besluit of een mededeling) is een inwerkingtredingsbepaling opgenomen die aangeeft of en zo ja wanneer (een deel van) het besluit in werking treedt. Ook kan aangegeven zijn dat (een deel van) het besluit met terugwerkende kracht geldig wordt, dus geldig wordt vanaf een datum die vóór de datum van inwerkingtreding ligt. In elk besluit is een inwerkingtredingsbepaling aanwezig, tenzij een besluit van rechtswege in werking treedt. In dat geval is er wetgeving die zegt dat een bepaald type besluit altijd na een vaste periode na de bekendmaking in werking treedt.

STOP ondersteunt geen inwerkingtredingsbepalingen over andere criteria dan de datum. Bepalingen die de geldigheid bijvoorbeeld beperken tot een bepaalde doelgroep of bepaalde scenario's zijn alleen toegestaan als de bepaling opgenomen is in een [regelingversie](besluit_geldigheidsbepalingen.md).

## Uit te wisselen informatie

De inwerkingtredingsbepaling wordt vertaald naar [tijdstempels](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel) in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module bij het juridisch document. De tijdstempels zijn gekoppeld aan het doel/branch en gelden daardoor voor alle regeling- en informatieobject-versies die aan datzelfde doel gekoppeld zijn. Het maakt daarbij niet uit of die versies in hetzelfde document staan of eerder of later in andere documenten/besluiten zijn geformuleerd. Zo moet de inwerkingstredingsbepaling vertaald worden naar de [tijdstempels](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel):

* Als de inwerkingtredingsbepaling inhoudt dat het besluit op een later te bepalen datum in werking treedt, dan worden *geen* tijdstempels meegegeven.

* Als een besluit van rechtswege in werking treedt of er staat een datum van inwerkingtreding in het document, dan moet de datum van inwerkingtreding via de [juridischWerkendVanaf](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) tijdstempel worden meegegeven. De standaard voorziet *niet* in een faciliteit om regels te formuleren en toe te passen om inwerkingtreding van rechtswege te modelleren. Het bevoegd gezag moet die regels zelf toepassen en  bepalen welke datum als tijdstempel doorgegeven moet worden.

* Als er sprake is van terugwerkende kracht, dan moet de eerste datum waarop het besluit geldig is worden meegegeven als [geldigVanaf](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) tijdstempel. Deze tijdstempel mag pas worden meegegeven als er tevens (of eerder) voor hetzelfde doel een `juridischWerkendVanaf` tijdstempel is uitgewisseld.


## Varianten
Er zijn drie varianten van het patroon te onderscheiden voor het doorgeven van een nieuwe versie voor een doel (in de illustratie: doel A).

![Variaties](img/consolideren_patronen_1iwt.png)

### 1. Directe inwerkingtreding
In het besluit waarin een nieuwe versie van de regeling of informatieobject is opgenomen of een intrekking daarvan, staat ook de inwerkingtredingsbepaling ervan. De `ConsolidatieInformatie` bevat zowel de informatie over de nieuwe versie c.q. intrekking en de [tijdstempels](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel) met de inwerkingtredingsdatum.

### 2. Apart inwerkingtredingsbesluit
Er is voor hetzelfde doel al eerder een besluit bekendgemaakt met daarin een nieuwe versie van de regeling of informatieobject of een intrekking daarvan. In een apart besluit (het _inwerkingtredingsbesluit_) staat de datum van de inwerkingtreding (en eventuele terugwerkende kracht) van de eerder gepubliceerde versie of van het besluit tot intrekking.

De gegevens over de inwerkingtreding, de [tijdstempels](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel), moeten in de `ConsolidatieInformatie` van het inwerkingtredingsbesluit worden gekoppeld aan het doel van het voorgaande besluit. De informatie over de regelingversie of intrekking maakt geen deel uit van de `ConsolidatieInformatie` van het inwerkingtredingsbesluit; dat is al uitgewisseld in het voorgaande besluit. 

Door het gebruik van hetzelfde doel zorgt het inwerkingtredingsbesluit ervoor dat het eerder gepubliceerde besluit meegenomen wordt bij het bepalen van de toestanden van de geconsolideerde regeling. 

### 3. Gedeeltelijke inwerkingtreding of splitsing {#gedeeltelijkeiwt}
Als gekozen wordt voor een apart inwerkingtredingsbesluit kan bij het opstellen daarvan blijken dat niet de hele versie van de regeling of het informatieobject in werking kan treden. Bijvoorbeeld omdat niet aan bepaalde randvoorwaarden is voldaan, zoals de beschikbaarheid van ICT-systemen of de oprichting van een nieuw organisatieonderdeel. Er is dan sprake van uitstel en geen afstel van de inwerkingtreding van een deel van die versie. Dit wordt gedeeltelijke inwerkingtreding genoemd.

Nu moet het bevoegd gezag naast de gegevens over de inwerkingtreding ook de regelingversie (of informatieobjectversie) meeleveren waarin alleen dat deel staat dat *wel* in werking treedt. Hoe dat moet is [elders](gedeeltelijke-inwerkingtreding.md) uitgelegd. Informatie over deze nieuwe versie wordt op de [gebruikelijke manier](consolideren_patronen_1versie.md) aan de `ConsolidatieInformatie` toegevoegd. Zowel deze nieuwe versie als de inwerkingtreding worden aan het originele doel (in de illustratie: A) gekoppeld, zodat voor de regelingen en informatieobjecten die al eerder voor dat doel/branch zijn uitgewisseld, niet opnieuw (voor een ander doel) uitgewisseld hoeven te worden.

Als op een later tijdstip het overgebleven deel alsnog in werking zal treden, dan wordt daarvoor een nieuw inwerkingtredingsbesluit opgesteld. De gegevens over de inwerkingtreding worden aan een nieuw doel (in de illustratie: doel B) gekoppeld en op de [gebruikelijke manier](consolideren_patronen_1versie.md) wordt de eerder bekendgemaakte volledige versie van de regeling (of informatieobject) als beoogde versie voor het nieuwe doel/branch (B) opgenomen, met als `eId` een verwijzing naar het inwerkingtredingsartikel van het tweede inwerkingtredingsbesluit. Eventuele wijzigingen die later voor het originele doel/branch (A) zijn aangebracht moeten overgenomen worden. 

Om de geautomatiseerde consolidatie goed te laten verlopen moet tenminste worden aangegeven dat er een vervlechting (groene pijl in de illustratie) van de laatste versie voor het originele doel/branch (A) met de versie voor het nieuwe doel/branch (B) heeft plaatsgevonden, ook al is de laatste gelijk aan de versie waarvan maar een deel in werking is getreden. Voor het nieuwe doel wordt de inwerkingtreding van het resterende deel gespecificeerd.

De hier beschreven variant is een gedeeltelijke inwerkingtreding waar bij het in werking treden van het eerste gedeelte nog niet bekend is of en wanneer het restant in werking treedt. Als dat al wel bekend is, is er sprake van een splitsing waarbij in hetzelfde inwerkingtredingsbesluit de inwerkingtreding van beide delen beschreven wordt. De informatie die hierboven is beschreven voor het tweede inwerkingtredingsbesluit wordt bij een splitsing ook onderdeel van het enige inwerkingtredingsbesluit.

