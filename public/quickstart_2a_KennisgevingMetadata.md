# data:KennisgevingMetadata

Deze module voorziet in de [metadata van een kennisgeving](data_xsd_Element_data_KennisgevingMetadata.dita#KennisgevingMetadata).

| Element  | Vulling | Verplicht? |
| ---- | ---- | --- |
| [eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke)      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J |
| [maker](data_xsd_Element_data_maker.dita#maker)                                      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J |
| [officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel)                    | De officiële titel van de kennisgeving | J |
| [onderwerpen](data_xsd_Element_data_onderwerpen.dita#onderwerpen)                          | Zie waardelijst [onderwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/onderwerp.xml) | J |
| [mededelingOver](data_xsd_Element_data_mededelingOver.dita#mededelingOver)                    | De AKN-identifier van het besluit waarover de kennisgeving zakelijke mededelingen bevat | J |

## Voorbeeld
* [Voorbeeld van een module KennisgevingMetadata](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-Metadata.xml)