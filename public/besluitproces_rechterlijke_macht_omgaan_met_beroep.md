# Verwerken beroep

Tegen sommige besluiten is [beroep](juridische-uitgangspunten.html#beroep) mogelijk. Hiervoor geldt een [bestuursrechtelijke procedure](besluitproces_rechterlijkemacht_achtergrond.md). Deze procedure is verwerkt in STOP, zie [Beroep en geconsolideerde regeling](besluitproces_rechterlijke_macht_beroep_en_geconsolideerde_regelgeving.md). De informatie van het BG wordt [verwerkt door de LVBB](besluitproces_rechterlijkemacht_statusbesluitbepalen.md) in de geconsolideerde regeling.

Deze sectie gaat in op de stappen die een BG moet ondernemen bij besluiten waarvoor beroep mogelijk is die extra zijn ten opzichte van besluiten waarvoor geen beroep mogelijk is. 

De volgende onderwerpen zullen worden behandeld:
* [Verwerken mogelijkheid beroep](besluitproces_rechterlijke_macht_beroep_mogelijk.md)
* [verwerken beroep ingesteld](besluitproces_rechterlijke_macht_verwerken_beroep.md)
* [Verwerken schorsing](besluitproces_rechterlijke_macht_verwerken_schorsing.md)
* [Verwerken uitspraak beroep](besluitproces_rechterlijkemacht_scenarios.md)

