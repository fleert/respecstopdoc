# cons:ConsolidatieIdentificatie

Deze module voorziet in de identificatie van de consolidatie van een work (regeling of informatieobject). Iedere consolidatie van ieder work kent zijn eigen cons:ConsolidatieIdentificatie. 

| Element                                                      | Vulling                                                      | Verplicht? |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------- |
| [FRBRWork](cons_xsd_Element_cons_FRBRWork.dita#FRBRWork)                                | Zie 'AKN IRI - Geconsolideerde regeling' en 'JOIN IRI' - Geconsolideerd Informatieobject. Deze identificaties worden uitgegeven door de LVBB. | J          |
| [soortWork](cons_xsd_Element_cons_soortWork.dita#soortWork)                              | Zie waardenlijst [soortwork.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortwork.xml). Een geconsolideerde regeling dient hier de waarde `'/join/id/stop/work_006'` te hebben. Een geconsolideerd informatieobject dient hier de waarde `/join/id/stop/work_005` te hebben. | J          |
| [isConsolidatievan/WorkIdentificatie/FRBRWork](cons_xsd_Element_cons_FRBRWork.dita#FRBRWork) | De AKN Work IRI van de Regeling (zie module [ExpressionIdentificatie_regeling](quickstart_1_ExpressionIdentificatieRegeling.md) van de regeling) of de JOIN Work IRI van het informatieobject (zie module [ExpressionIdentificatie_informatieobject](quickstart_1_ExpressionIdentificatieGIO.md) ). | J          |
| [isConsolidatievan/WorkIdentificatie/soortWork](cons_xsd_Element_cons_soortWork.dita#soortWork) | Zie waardenlijst [soortwork.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortwork.xml). Voor een regeling: `/join/id/stop/work_019`. Voor een informatieobject: `/join/id/stop/work_007` | J          |



## AKN IRI - Geconsolideerde regeling

De LVBB genereert de identificatie van de geconsolideerde regeling conform de onderstaande opbouw (uitgaande van een Nederlands bevoegd gezag): 

`//FRBRWork:  "/akn/nl/act/" <subtype> "/" <datum_work> "/" <nummer> `

[Toelichting](consolidatie_naamgeving.md)



## JOIN IRI - Geconsolideerd Informatieobject

De LVBB genereert de identificatie van de geconsolideerde regeling conform de onderstaande opbouw (uitgaande van een Nederlands bevoegd gezag): 

`//FRBRWork: "/join/id/regdata/consolidatie/" <datum_work> "/" <nummer> `

[Toelichting](consolidatie_naamgeving.md)



## Voorbeelden

[Voorbeeld ConsolidatieIdentificatie van een Regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/04-LVBB-Consolidatie-Regeling-Identificatie.xml)

[Voorbeeld ConsolidatieIdentificatie van een GIO](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/04-LVBB-Consolidatie-GIO-Identificatie.xml)