# 3. Voeg IO toe aan besluitmetadata

In de [besluitmetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) wordt de JOIN-ID van de  [expression](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van het IO in de [InformatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs) vermeld. 

## Voorwaarden
* De informatieobject-versie (expressie) is nog nooit eerder aan een besluit toegevoegd. 
* Dit kunnen informatieve, alleen bekend te maken en te consolideren informatieobjecten zijn.  

## Achtergrond
Bepalend voor systemen die de besluiten verwerken is de lijst [informatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs) als onderdeel van de [metadata van het besluit](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata). In deze lijst staat de expression-identificatie opgesomd van elk bij het besluit gevoegde informatieobject. Software die het besluit aan een lezer in een interactieve omgeving presenteert kan vervolgens bijvoorbeeld de lijst  gebruiken in een navigatie-element zodat de lezer naar de presentatie van het informatieobject kan worden geleid.

Bij de bekendmaking van het besluit worden de informatieobjecten als bijlage bij het besluit gepubliceerd.

