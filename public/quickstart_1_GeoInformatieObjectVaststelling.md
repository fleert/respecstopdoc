# geo:GeoInformatieObjectVaststelling

Deze STOP-XML module schrijft voor hoe een versie van een geografisch informatieobject (GIO) aangeleverd dient te worden. Deze `geo' module importeert het 'gio' schema waarin contextgegevens dienen te worden vastgelegd. 

| Element                                                      | Vulling                                                      | Verplicht? |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------- |
| [context/gio:GeografischeContext/gio:achtergrondVerwijzing](gio_xsd_Complex_Type_gio_GeografischeContextType.dita#GeografischeContextType_achtergrondVerwijzing) | Verwijzing naar de achtergrondkaart bij het vaststellen van de GIO. Bijvoorbeeld: "TOP10NL" | J          |
| [context/gio:GeografischeContext/gio:achtergrondActualiteit](gio_xsd_Complex_Type_gio_GeografischeContextType.dita#GeografischeContextType_achtergrondActualiteit) | De actuatliteit van de gebruikte achtergrondkaart. Zie [technische documentatie](gio_xsd_Complex_Type_gio_GeografischeContextType.dita#GeografischeContextType_achtergrondActualiteit). | J          |
| [context/gio:GeografischeContext/gio:contextGIOs](gio_xsd_Complex_Type_gio_GeografischeContextType.dita#GeografischeContextType_contextGIOs) | JOIN Expression IRI(s) van de GIOs die gebruikt zijn als context bij de vaststelling. Zie ook: [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieGIO.md) | N          |
| [context/gio:GeografischeContext/gio:nauwkeurigheid](gio_xsd_Complex_Type_gio_GeografischeContextType.dita#GeografischeContextType_nauwkeurigheid) | Nauwkeurigheid van een berekende GIO in decimeters           | N          |
| [GeoInformatieObjectVersie/FRBRWork](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_FRBRWork) | JOIN Work IRI. Zie ook: [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieGIO.md) | J          |
| [GeoInformatieObjectVersie/FRBRExpression](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_FRBRExpression) | JOIN Expression IRI. Zie ook: [data:ExpressionIdentificatie](quickstart_1_ExpressionIdentificatieGIO.md) | J          |
| [GeoInformatieObjectVersie/eenheidlabel](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidlabel) | Eenheid van een kwantitatieve normwaarde bij de locaties in de GIO. Bijvoorbeeld: "meters boven maaiveld" |            |
| [GeoInformatieObjectVersie/eenheidID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidID) | Identificatie van de eenheid                                 | N          |
| [GeoInformatieObjectVersie/normlabel](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normlabel) | Het label van de norm. Bijvoorbeeld: "bouwhoogte"            | N          |
| [GeoInformatieObjectVersie/normID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normID) | Identificatie van de norm                                    | N          |
| [GeoInformatieObjectVersie/groepen/Groep/groepID](geo_xsd_Complex_Type_geo_GroepType.dita#GroepType_groepID) | Identificatie van de groep. Moet voldoen aan regex: `[A-Za-z0-9][A-Za-z0-9_-]*` | N          |
| [GeoInformatieObjectVersie/groepen/Groep/label](geo_xsd_Complex_Type_geo_GroepType.dita#GroepType_label) | Het label van de groep. Bijvoorbeeld "zone 1"                | N          |
| [GeoInformatieObjectVersie/locaties/Locatie/naam](geo_xsd_Complex_Type_geo_LocatieType.dita#LocatieType_naam) | Naam van de Locatie.                                         | N          |
| [GeoInformatieObjectVersie/locaties/Locatie/geometrie](geo_xsd_Complex_Type_geo_LocatieType.dita#LocatieType_geometrie) | Wordt beschreven binnen IMOW door [Geonovum](https://docs.geostandaarden.nl/nen3610/def-st-basisgeometrie-20200930/) | J          |
| [GeoInformatieObjectVersie/locaties/Locatie/groepID](geo_xsd_Complex_Type_geo_LocatieType.dita#LocatieType_groepID) | Zie hierboven. Op deze manier wordt een Locatie aan een groep gekoppeld. | N          |
| [GeoInformatieObjectVersie/locaties/Locatie/kwantitatieveNormwaarde](geo_xsd_Complex_Type_geo_LocatieType.dita#LocatieType_kwantitatieveNormwaarde) | Een kwantitatieve waarde (double). Wordt beschreven binnen IMOW door [Geonovum](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) | N          |
| [GeoInformatieObjectVersie/locaties/Locatie/kwalitatieveNormwaarde](geo_xsd_Complex_Type_geo_LocatieType.dita#LocatieType_kwalitatieveNormwaarde) | Een tekstuele classificatie. Wordt beschreven binnen IMOW door [Geonovum](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) | N          |



## Voorbeeld

[Voorbeeld GeoInformatieObjectVaststelling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/01-BG-Vuurwerkverbodsgebied-v1-Inhoud.gml)

