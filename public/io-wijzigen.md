# Een IO wijzigen

De Bekendmakingswet stelt dat van een niet-tekstueel besluit-onderdeel, zoals een IO, bij wijziging een nieuwe (IO-)versie in z’n geheel moet worden aangeleverd. Een IO wordt daarom niet gemuteerd als een regeling. Dat wil zeggen dat men dus niet de IO-onderdelen die wijzigen in een vorm van renvooi aanlevert (zoals alleen de gewijzigde geometrische data). Een IO wordt dus in zijn geheel vervangen.  

## Soorten IO-wijzigingen
Er zijn drie soorten IO-wijzigingen met ieder een andere juridische lading:
1. **Gewijzigd vaststellen**: Hiermee beperkt men de appeleerbaarheid van het besluit tot de wijzigingen ten opzichte van de vorige IO-versie.  
1. **Opnieuw vaststellen**: In de bijlage van de regeling wordt de JOIN-ID van het IO aangepast en in het gewijzigde IO wordt *geen* verwijzing naar de vorige versie van het IO opgenomen. Het IO wordt hierdoor volledig opnieuw vastgesteld. 
1. **Intrekken en vervangen**: In de bijlage van de regeling wordt de JOIN-ID van het IO verwijderd en vervangen door de JOIN-ID van een andere, nieuwe IO. Het originele IO wordt hiermee ingetrokken en bij het nieuwe IO wordt aangegeven dat het de opvolger is van het originele IO.

## 1. Gewijzigd vaststellen

Wanneer een IO gewijzigd vastgesteld wordt, heeft het bevoegd gezag alleen dat gedeelte van het IO dat daadwerkelijk gewijzigd is bekeken en is er dus geen besluit genomen over de niet-gewijzigde delen van het IO. Om een IO gewijzigd vast te  stellen, moet worden aangegeven wat de vorige versie van deze IO is en daarmee wordt impliciet alleen besloten over de veranderingen in het nieuwe IO ten opzichte van de vorige versie van deze IO. Met een gewijzigde vaststelling beperkt men de appelleerbaarheid van het besluit tot de wijzigingen ten opzichte van de vorige IO-versie.  

Dit gebeurt als volgt:
1. Geef in de tekst van het besluit of de toelichting aan wat er in het `TeConsolideren IO` wijzigt. Ook kan men in de toelichting een illustratie toevoegen waarin het verschil met de eerdere versie van het IO is aangegeven. Dit maakt de wijziging begrijpelijker voor belanghebbenden, die deze informatie nodig hebben om te bepalen welke regels voor hen gaan gelden en of ze mogelijk bezwaar willen maken, zienswijzen willen indienen of [beroep](beroep.md) willen instellen. Geeft men de wijzigingen van het GIO aan in de tekst, dan is bezwaar, zienswijzen en beroep alleen mogelijk voor de wijzigingen in het nieuwe GIO ten opzichte van de oude IO. Zijn de IO-wijzigingen niet in de tekst aangegeven, dan is de gehele versie van het nieuwe IO appelleerbaar, inclusief de ongewijzigde delen. 
1. In de regelingtekst van een besluit wordt verwezen naar het IO. Men kan daarbij het label van het IO ongewijzigd laten, maar dat hoeft niet. Juridisch maakt het niet uit.
2. Als het een `teConsolideren` IO is, wordt de JOIN-ID in de tekst-bijlage van de regeling gewijzigd.
3. In de [besluitmetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) wordt de [expression-ID](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van het IO in de [InformatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs) gewijzigd. Zie [IO toevoegen aan besluitmetadata](io-toevoegen-besluitmetadata.md).
4. In het gewijzigde IO wordt een `was`-verwijzing naar de vorige versie van het IO opgenomen. Hiermee beperkt de wijziging zich uitsluitend tot de verschillen tussen de nieuwe en de vorige versie van het IO ondanks dat het IO volledig wordt aangeleverd.
3. De raadpleegdatum of de achtergrond in de geografische context wijzigt mogelijk.
4. Als het een `teConsolideren` IO is, wordt IO opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit.
5. De modules van de versie van het IO worden als informatieobject bijgevoegd bij het besluit over de regeling.




## 2. Opnieuw vaststellen

Wanneer een IO opnieuw wordt vastgesteld, geldt dat het bevoegd gezag het hele IO heeft bekeken en over ieder onderdeel daarvan heeft besloten. Bij het volledig opnieuw vaststellen van een nieuwe IO-versie is de nieuwe IO-versie volledig appelleerbaar. Met het wijzigen van een IO ontstaat dus geen nieuw work, maar loopt de geldigheid van de oude (huidige) expressie af, waardoor de `Toestand` van de "oude" IO dus een 'datum geldig tot' krijgt.


Het opnieuw vaststellen van een IO gebeurt als volgt:
1.  In de regelingtekst van een besluit wordt verwezen naar het IO.
2. Als het een `teConsolideren` IO is, wordt de JOIN-ID in de tekst-bijlage van de regeling gewijzigd.
2. In de [besluitmetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) wordt de [expression-ID](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van het IO in de [InformatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs) gewijzigd. Zie [IO toevoegen aan besluitmetadata](io-toevoegen-besluitmetadata.md).
3. In het gewijzigde IO wordt **geen** `was`-verwijzing naar de vorige versie van het IO opgenomen.
3. De raadpleegdatum of de achtergrond in de geografische context wijzigt mogelijk.
4. Als het een `teConsolideren` IO is, wordt het IO opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit.
6. Als het een `TeConsolideren` IO is, wordt de JOIN-ID van het *work* van de regeling waarmee het de IO-versie is bekendgemaakt opgenomen in de [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) van de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata).  Zie [geboorteregeling opnemen in IO](io-geboorteregeling-opnemen.md).
7. De modules van het IO  worden als informatieobject toegevoegd aan het besluit over de regeling. Zie [IO-modules toevoegen](io-modules-toevoegen.md).

## 3. Intrekken en vervangen

Intrekken en vervangen is het intrekken van de laatste IO-versie en het vaststellen van een volledig nieuwe IO, een nieuw IO-`work`. Er ontstaat een nieuwe expressie van hetzelfde work (zie [FRBR model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg)). In bepaalde situaties, bijvoorbeeld bij het samenvoegen van twee IO's, is het gewenste resultaat niet met een wijziging te bereiken. Het oude IO wordt dan ingetrokken en een nieuwe IO wordt ingesteld. Om dan toch aan te kunnen geven dat het nieuwe IO een opvolger is van de ingetrokken IO('s), kan bij het nieuwe IO aangegeven worden dat het IO een opvolger is van één of meer andere IO's. De opvolgingsrelatie wordt uitgedrukt met: 


Het intrekken en vervangen van een IO gebeurt als volgt:
1.  In de regelingtekst van een besluit wordt verwezen naar de nieuwe IO en de verwijzing naar de oude IO wordt verwijderd.
2. Als het een `teConsolideren` IO is, worden het label en de JOIN-ID in de tekst-bijlage van de regeling gewijzigd in die van de nieuwe IO. Die van het oude IO wordt daarmee verwijderd.
3. In de [besluitmetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) wordt de [expression-ID](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van het IO in de [InformatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs) gewijzigd in die van het nieuwe IO. Zie [IO toevoegen aan besluitmetadata](io-toevoegen-besluitmetadata.md).
4. Als het een `teConsolideren` IO is, wordt het `Work` van het ingetrokken IO opgenomen in de consolidatie-informatie van het besluit. 
3. Als het een `teConsolideren` IO is, wordt het nieuwe IO  opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit.
3. Als het een `teConsolideren` IO is, moet in de `ConsolidatieInformatie` van het besluit in `Intrekkingen` worden aangegeven dat het oude IO wordt ingetrokken. Voor een voorbeeld, zie [IO intrekken](io-intrekken.md)

3. Als het een `TeConsolideren` IO is, wordt de JOIN-ID van het *work* van de regeling waarmee het nieuwe IO is bekendgemaakt opgenomen in de [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) van de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata).  Zie [geboorteregeling opnemen in IO](io-geboorteregeling-opnemen.md).
3. De opvolgingsrelatie wordt met de elementen `opvolging` en ["opvolgerVan"](EA_EC4DE25BFB1341e6B632337DAF9C2791.dita#Pkg/823304801C6D45108A8BB5E86A666808)  toegevoegd aan de `InformatieObjectMetadata` om aan te geven dat het nieuwe IO de opvolger is van het ingetrokken IO.
4. De nieuwe versie van het IO wordt als informatieobject bijgevoegd bij het besluit over de regeling.


### Voorbeeld - aanpassing regelingtekst

Het is verboden consumentenvuurwerk te gebruiken in het ~~vuurwerkverbodsgebied~~ vuurwerkverbodsgebied 2021.

Uitgedrukt in XML:

```xml
<Inhoud>
  <Al>Het is verboden consumentenvuurwerk te gebruiken in het <VerwijderdeTekst>
    <IntIoRef eId="art_2__para_1__ref_o_1_inst2"
               ref="gm9999_2__cmp_A__content_o_1__list_1__item_1__ref_o_1"
               wId="gm9999_1__art_2__para_1__ref_o_1">vuurwerkverbodsgebied</IntIoRef> </VerwijderdeTekst>
    <NieuweTekst> <IntIoRef eId="art_2__para_1__ref_o_1"
                             ref="gm9999_3__cmp_A__content_o_1__list_1__item_1__ref_o_1"
                             wId="gm9999_3__art_2__para_1__ref_o_1">vuurwerkverbodsgebied 2021</IntIoRef> </NieuweTekst>. </Al>
</Inhoud>
```

### Voorbeeld - aanpassing informatieobjecten-bijlage

  <span class="doorhalen">vuurwerkverbodsgebied </span>   vuurwerkverbodsgebied 2021
  <span class="doorhalen">`/join/id/regdata/gm0307/2019/gio993859238/nld@2020‑06‑16;2`</span>   `/join/id/regdata/gm9999/2021/gio993859462/nld@2021‑03‑23;1`

Uitgedrukt in XML:

```xml
<Begrip>
  <Term>
    <VerwijderdeTekst>vuurwerkverbodsgebied</VerwijderdeTekst>
    <NieuweTekst>vuurwerkverbodsgebied 2021</NieuweTekst> </Term>
  <Definitie>
    <!-- oude definitie met verwijzing naar oude GIO. 
           eId met inst2 toevoeging om hem uniek te maken -->
    <Al wijzigactie="verwijder"> <ExtIoRef eId="cmp_A__content_o_1__list_1__item_1__ref_o_1_inst2"
                                            ref="/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2"
                                            wId="gm9999_2__cmp_A__content_o_1__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2</ExtIoRef> </Al>
    <!-- nieuwe definitie met verwijzing naar nieuwe GIO -->
    <Al wijzigactie="voegtoe"> <ExtIoRef eId="cmp_A__content_o_1__list_1__item_1__ref_o_1"
                                          ref="/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1"
                                          wId="gm9999_3__cmp_A__content_o_1__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1</ExtIoRef> </Al>
  </Definitie>
</Begrip>
```

### Voorbeeld - opvolgingsrelatie

```
<opvolging>
  <opvolgerVan>/join/id/regdata/gm9999/2019/gio993859238</opvolgerVan>
</opvolging>
```

### 

## Wanneer welke wijziging?


| Wat wijzigt er? | IO-handeling | Opmerking |
| ---|---|---|
|**Label**|IO opnieuw of gewijzigd vaststellen | work-ID IO blijft hetzelfde. Type wijziging beïnvloedt appelleerbaarheid. |
|**Work-ID IO**| Nieuwe IO (intrekken & vervangen)| Oud IO wordt verwijderd, nieuw IO wordt toegevoegd. |


## Zie ook
* [Een GIO wijzigen](gio-wijzigen.md). 
* [Voorbeeldbestanden inzake het wijzigen van een GIO bij een regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/02%20Regeling%20met%20GIO-wijziging).
* Voor het terugtrekken van GIO-versies (expressions), zie [terugtrekken GIO](besluitproces_rectificatie_scenario_D3.md).