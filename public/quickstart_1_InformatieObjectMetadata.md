# data:InformatieObjectMetadata en data:InformatieObjectVersieMetadata

Deze modules voorzien in de metadata bij een InformatieObject(versie). 

**data:InformatieObjectMetadata**

| Element                                                      | Vulling                                                      | Verplicht? |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------- |
| [alternatieveTitels/alternatieveTitel](data_xsd_Element_data_alternatieveTitel.dita#alternatieveTitel) | Een alternatieve titel voor het instrument (hier: InformatieObject), die niet officieel is vastgesteld en die in het gangbaar spraakgebruik gebruikt wordt om het instrument (InformatieObject) aan te duiden | N          |
| [eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke)      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J          |
| [formaatInformatieobject](data_xsd_Element_data_formaatInformatieobject.dita#formaatInformatieobject)  | Zie waardelijst [formaatinformatieobject.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/formaatinformatieobject.xml) | J          |
| [maker](data_xsd_Element_data_maker.dita#maker)                                      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J          |
| [officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel)                    | De officiële titel van het informatieobject                  | J          |
| [publicatieinstructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie)        | Enumeratie (Informatief I AlleenBekendteMaken I TeConsolideren). 'TeConsolideren' wordt gebruikt voor een werkingsgebied van een juridische regel | J          |

 **data:InformatieObjectVersieMetadata**

| Element                                                      | Vulling                                                      | Verplicht?           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------- |
| [heeftBestanden](data_xsd_Element_data_heeftBestanden.dita#heeftBestanden)                    | Container                                                    | N                    |
| [heeftBestanden/heeftBestand/Bestand/bestandsnaam](data_xsd_Element_data_bestandsnaam.dita#bestandsnaam) | Een bestand waarin de informatie van het informatieobject is geserialiseerd. Van een bestand kan de onveranderlijkheid worden aangetoond middels een hash. Bijvoorbeeld: Centrumgebied.gml | J (binnen container) |
| [heeftBestanden/heeftBestand/Bestand/hash](data_xsd_Element_data_hash.dita#hash)    | Voor de berekening van de [hash](data_xsd_Element_data_hash.dita#hash) wordt gebruikt gemaakt van het SHA512 algoritme. | J (binnen container) |
| [heeftGeboorteregeling](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling)      | De AKN WORK IRI van de regeling waarvoor het (consolideerbaar) informatieobject is opgesteld. Zie ook: [STOP-XML module: ExpressieIdentificatie Regeling](quickstart_1_ExpressionIdentificatieRegeling.md) | N                    |



## Voorbeeld

[Voorbeeld InformatieObjectMetadata](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/01-BG-Vuurwerkverbodsgebied-v1-Metadata.xml)

[Voorbeeld InformatieObjectVersieMetadata](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/01-BG-Vuurwerkverbodsgebied-v1-versieMetadata.xml)