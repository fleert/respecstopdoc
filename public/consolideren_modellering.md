# Modellering van de consolidatie

In deze sectie van de documentatie is beschreven:

[Naamgeving](consolidatie_naamgeving.md)
: Een geconsolideerde regeling wordt in STOP beschreven als een apart work met eigen [naamgeving](consolidatie_naamgeving.md).

[Overzicht van toestanden](consolideren_toestanden.md)
: De geconsolideerde regeling kan verschillende versies hebben in de loop van de tijd. Zo'n versie noemen we een *Toestand*. De regelingversie voor een Toestand hoeft niet altijd bekend te zijn, bijvoorbeeld als twee tegenstrijdige wijzigingen tegelijk in werking treden. STOP kent twee manieren om te beschrijven welke [Toestanden](consolideren_toestanden.md) een regeling heeft.

[Samenstellen actuele toestanden](consolideren_toestanden_bepalen.md)
: De toestanden kunnen [bepaald worden](consolideren_toestanden_bepalen.md) op basis van publicaties en versiebeheerinformatie.

[Voorbeelden van actuele toestanden](consolideren_voorbeelden_actueel.md)
: Verwijzingen naar XML voorbeelden van actuele toestanden.
