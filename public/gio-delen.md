# Overzicht met GIO-delen (optioneel)
Een GIO kan een **GIO-delen-overzicht** hebben. Dit is een opsomming van de GIO-delen waaruit een GIO met GIO-delen is opgebouwd. Heeft een GIO een GIO-delen-overzicht, dan behoort elke locatie tot een GIO-deel.


Een **overzicht met GIO-delen** bevat een of meerdere `Groep`-elementen. Elke groep heeft een:
* een `groepID`, identificatie van het GIO-deel
* een `label`, naam van het GIO-deel. Wordt gebruikt in de tekst van de regeling en in de [afbeelding (symbolisatie)](gio-symbolisatie.md) van een GIO. 

## Uitgangspunten GIO-delen-overzicht en GIO-delen
Een GIO-delen-overzicht en GIO-delen worden uitsluitend gebruikt om juridische regels van een regeling te laten gelden voor een beperkt aantal locaties van de GIO. Hierbij gelden de volgende uitgangspunten: 
-  Een GIO-deel gaat over een deelonderwerp van het hoofdonderwerp van het GIO. Dit hoofdonderwerp is gelijk aan het hoofdonderwerp van de regels waar het GIO betrekking op heeft. Zijn de regels onder te verdelen in subsets (van bijvoorbeeld afwijkende of aanvullende regels), dan kan een GIO ten behoeve van een overzichtelijke presentatie worden opgedeeld in GIO-delen. 
- Een GIO kent hooguit één opdeling in GIO-delen. Het is dus niet mogelijk om in één GIO bijvoorbeeld zowel een opdeling op basis van woonwijken als op basis van postcodegebieden te maken. Ook is er geen gelaagdheid van GIO-delen mogelijk. Het GIO "Militaire gebieden" zou locaties kunnen bevatten die toebehoren aan de groep "Marineterreinen", maar deze locaties kunnen niet tegelijk ook toebehoren aan oefenterreinen, kazernes, etc. Hiervoor zouden alsnog afzonderlijke GIO's nodig zijn.
- Voor elk GIO-deel is er minimaal één locatie met bijbehorend `groepID`. 
- De geometrische data van locaties in verschillende GIO-delen mogen elkaar alleen overlappen indien de overlap juridisch irrelevant is. 
-  De afbeelding van een GIO met GIO-delen moet eenvoudig te begrijpen zijn. Een GIO is dus geen atlas van alle mogelijke werkingsgebieden. Dit komt de begrijpelijkheid van de afgebeelde GIO niet ten goede. 
- Is een GIO opgedeeld in GIO-delen, dan bevat elke locatie precies één `groepID`. In het bovengenoemde voorbeeld van het GIO "Militaire gebieden" met het GIO-deel "Marineterreinen" zullen de terreinen van de land- en luchtmacht ook in GIO-delen geplaatst moeten worden. Dit om te zorgen dat een tekstuele verwijzing zoals 'geldend voor GIO-delen van de "Militaire gebieden" niet zijnde "Marineterrein" ' eenduidig is. Als de betreffende GIO namelijk twee GIO-delen "Marineterrein" en "Landmacht-terrein" en locaties zonder `groepID` zou bevatten, is niet duidelijk welke locaties met de 'groepen niet zijnde "Marineterrein" ' worden aangeduid. 
- Een GIO met GIO-delen heeft altijd [symbolisatie](gio-symbolisatie.md). Bij GIO’s met GIO-delen wordt getoond welke geometrische data tot welk GIO-deel behoort, door de locaties per GIO-deel een verschillende kleur te geven, conform de verplicht meegeleverde symbolisatie. 
- Een GIO met GIO-delen mag geen normwaarden bevatten. Tot op heden is er geen situatie onderkent waar een GIO met zowel GIO-delen als normwaarden toegevoegde waarde zou bieden. Daarnaast zou het combineren van GIO-delen en normwaarden in één GIO de verbeelding van zo'n GIO complex maken. 
- GIO-delen zijn niet zelfstandig. De GIO-delen in een GIO kunnen dus niet bestaan zonder het GIO zelf. Vergelijk het met artikelen binnen een regeling. Die kunnen ook niet zonder regeling bestaan. 
- Een GIO-deel heeft daarom geen eigen levensloop.

## Zie ook
* [Overzicht van de GIO-bedrijfsregels](businessrules_geo_GeoInformatieObjectVersie.dita#Bedrijfsregels) voor het gebruik van GIO-delen.
* [Voorbeeldbestand regeling en GIO met GIO-delen](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01a%20Nieuwe%20regeling%20met%20GIO-delen).