# Verwerken mogelijkheid beroep



Om aan te geven dat er tegen een besluit beroep mogelijk is, doet het BG het volgende tijdens de opstelfase van het besluit:

## 1. Stel een kennisgeving op

Voor de kennisgeving worden de volgende [kennisgeving](begrippenlijst_kennisgeving.dita#kennisgeving)-modules toegevoegd:
1. de [`ExpressionIdentificatie`](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van de kennisgeving met:
   * [`FRBRWork`](data_xsd_Element_data_FRBRWork.dita#FRBRWork),
   * [`FRBRExpression`](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression),
   * [`soortWork`](data_xsd_Element_data_soortWork.dita#soortWork). Voor een kennisgeving is dit `/join/id/stop/work_023`.
2. de [`Kennisgeving`](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving)-module met de tekst van de kennisgeving.
3. de [`KennisgevingMetadata`](data_xsd_Element_data_KennisgevingMetadata.dita#KennisgevingMetadata), met daarin:
   * de [`eindverantwoordelijke`](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke),
   * de [`maker`](data_xsd_Element_data_maker.dita#maker),
   * de [`officieleTitel`](data_xsd_Element_data_officieleTitel.dita#officieleTitel),
   * de [`onderwerpen`](data_xsd_Element_data_onderwerpen.dita#onderwerpen),
   * de [`mededelingOver`](data_xsd_Element_data_mededelingOver.dita#mededelingOver).
4. de [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie). Hierin wordt de einddatum beroepstermijn als procedurestap opgegeven. Dit gebeurt door een [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap) met daarin een [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap)  `/join/id/stop/procedure/stap_016`. Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).  

Aan de hand van de procedurestap *Einde beroepstermijn* kunnen systemen afleiden dat de status van het besluit *niet onherroepelijk* is. Deze procedurestap wordt met de [kennisgeving (informatiemodel)](EA_233F9A157B1F4d11A41DBE8FC3424244.dita#Pkg) meegeleverd als procedureverloopmutatie.

### Voorbeeld procedureverloopmutatie met einde beroepstermijn

```
<Procedureverloopmutatie>
  <bekendOp>2020-01-21</bekendOp>
  <voegStappenToe>
    <Procedurestap>
      <!-- Einde beroepstermijn -->
      <soortStap>/join/id/stop/procedure/stap_016</soortStap>
      <voltooidOp>2020-03-04</voltooidOp>
    </Procedurestap>
  </voegStappenToe>
</Procedureverloopmutatie>
```



## Zie ook

* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).
