# Consolidatie

Consolidatie is het proces dat op basis van besluiten de geldende regelgeving beschrijft zodat het beschikbaar gesteld kan worden. STOP maakt het mogelijk om dit proces vergaand te automatiseren. In deze sectie van de documentatie is beschreven:

[Consolidatie in STOP](consolideren_introductie.md)
: Beschrijving van wat consolidatie inhoudt en hoe STOP het mogelijk maakt dat de consolidatie geautomatiseerd uitgevoerd kan worden.

[Geautomatiseerde consolidatie](consolideren_principe.md)
: Globale beschrijving van de geautomatiseerde consolidatie om te komen tot de (nu of in de toekomst) geldende regelgeving.

Versiebeheer en consolidatie
: Het samenstellen van de geconsolideerde regeling maakt gebruik van informatie conform het STOP versiebeheer dat als onderdeel van een besluit, rectificatie of andere publicatie wordt uitgewisseld in de module [consolidatie-informatie](consolidatie-informatie.md).

[Modellering van de consolidatie](consolideren_modellering.md)
: Een geconsolideerde regeling wordt in STOP beschreven als een apart work met eigen [naamgeving](consolidatie_naamgeving.md), waarvan de expressions als [toestanden](consolideren_toestanden.md) zijn gemodelleerd. De toestanden kunnen [bepaald worden](consolideren_toestanden_bepalen.md) op basis van publicaties en versiebeheerinformatie.

[Consolidatiepatronen](consolideren_patronen.md)
: Een overzicht van de verschillende patronen die gebruikt moeten worden om de geautomatiseerde consolidatie goed te laten functioneren. De software van het bevoegd gezag moet deze patronen herkennen en eventueel de extra informatie kunnen (laten) opstellen.


[Consolideren van annotaties](consolideren_annotaties.md)
: STOP onderkent annotaties bij een regeling of informatieobject. Deze zullen in het algemeen minder vaak veranderen dan de regeling of het informatieobject zelf. De standaard is erop ingericht dat de annotaties niet bij elke uitwisseling meegenomen hoeven te worden, maar alleen in die gevallen waar ze veranderen. Met behulp van de resultaten van de geautomatiseerde consolidatie is te bepalen welke versie van een annotatie bij welke toestanden hoort. Dit mechanisme kan ook worden toegepast voor informatie beschreven door andere standaarden. De annotatie van de externe standaard moet zich dan wel op dezelfde manier gedragen als een STOP-annotatie.

Elders is beschreven:

[Regelgeving in de loop van de tijd](regelgeving_in_de_tijd.md)
: De geautomatiseerde consolidatie kan uitgebreid worden en ook beschrijven welke regelgeving op een moment in de tijd geldig was voor meer complexe tijdreizen. Dat is in een apart hoofdstuk beschreven: [regelgeving in de loop van de tijd](regelgeving_in_de_tijd.md). Daarin is ook uitgelegd welke tijdstempels bij dit tijdreizen van belang zijn.
