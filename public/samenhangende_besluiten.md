# Samenhangende besluiten

TODO



Onderwerpen: de verschillende consolidatiescenario's beschrijven (PI22), gezien vanuit BG. Hier zitten vaak afwegingen in voor BG.
  Bijv: meerdere wijzigingsbesluiten met iwt versus meerdere opvolgende besluiten en daarna iwt-besluit;
  twee wijzigingen van dezelfde regeling in een besluit, of een versie en gedeeltelijke iwt;
  een besluit dat meerdere regelingen wijzigt, of gekoppelde aparte besluiten per regeling;
  overgangsrecht in regeling vs regels in besluit (dwz aparte regeling voor overgangsrecht)