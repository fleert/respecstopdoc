# STOP-modellering van een rectificatie

De [formele modellering van de rectificatie](EA_CE0701FE9A054073B11E9531BE7681D4.dita#Pkg) is vastgelegd in het STOP-informatiemodel. Deze pagina verwijst naar de elementen in de XML-implementatie van dat model, zodat de gebruikte namen in de tekst en de XML-voorbeelden overeenkomen.  

# Tekst van de rectificatie

Voor de tekst van een regeling kent STOP het mutatiemechanisme waarmee uit de beschrijving van de wijzigingen de nieuwe complete versie van de tekst afgeleid kan worden. Een rectificatie gebruikt dat mechanisme voor de correcties in  
* de tekst van regelingen 
* de tekst van alléén het besluit (de tekst die geen onderdeel is van een geconsolideerde regeling). 

Kleine correcties op de tekst van het besluit kunnen ook alleen tekstueel omschreven worden.

Deze mutaties worden in de tekst van een rectificatie binnen het [element `Rectificatietekst`](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst) gecodeerd. De rectificatie in zijn geheel bestaat uit een of meer rectificatieteksten.

Elke rectificatietekst beschrijft een correctie van een deel van het te rectificeren besluit. De rectificatietekst bestaat uit een optionele inleidende serie van alinea's en lijsten om de correctie in meer detail te beschrijven, gevolgd door één van onderstaande constructies

1. ofwel een aankondiging dat (de wijziging van) de tekst van een **regeling** gecorrigeerd moet worden, gevolgd door de specificatie van deze correctie in [renvooi](besluitproces_rectificatie_renvooi.md). De renvooiweergave is opgenomen in een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie). De `was`-versie van deze `RegelingMutatie` is de regelingversie die ontstaan is uit de publicatie van het gerectificeerde besluit; met andere woorden de `wordt`-versie van dat gerectificeerde besluit. In de consolidatie-informatie moet de nieuwe regelingversie via een [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) gekoppeld worden aan hetzelfde doel als de eerder gepubliceerde regelingversie. 
1. ofwel daarin de aankondiging dat de tekst van een **besluit** gecorrigeerd wordt, gevolgd door de specificatie van de correctie in [renvooi](besluitproces_rectificatie_renvooi.md). De renvooiweergave is opgenomen in een [BesluitMutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie). Een `BesluitMutatie` heeft geen was-versie omdat besluiten niet worden geconsolideerd. De te corrigeren tekst mag geen onderdeel uitmaken van de tekst van een regeling.

Als de correctie beschreven in de `BesluitMutatie` klein is, dan kan volstaan worden met uitsluitend een [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst) waarin de correctie tekstueel beschreven wordt. 

In de rectificatie zijn de versies waarop de `RegelingMutatie`(s) en de `BesluitMutatie`(s) zijn gebaseerd dus niet gelijk:
* De `RegelingMutatie` heeft als was-versie de geconsolideerde regeling die **ontstaan is uit** het toepassen van de `RegelingMutaties` uit **het besluit**. De RegelingMutaties in de rectificatie kunnen niet als was-versie het besluit zelf hebben, omdat de was-versie een complete regelingversie moet zijn en niet alleen een renvooi-mutatie uit het besluit.
* De `BesluitMutatie` bevat wijzigingen ten opzichte van **het besluit zelf**. 

Dit is de reden dat besluiten niet geconsolideerd kunnen worden: Het verschil in 'was-versie' maakt het onmogelijk de juiste besluitversie te reconstrueren. Daarbij komt ook dat er geen vraag is naar geconsolideerde besluiten; de verschilweergave van rectificaties is juridische voldoende. Het gevolg hiervan is dat een [`BesluitMutatie`](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie) ook geen attributen `@was`/`@wordt` heeft.  

Als er een tweede rectificatie op hetzelfde besluit nodig is, dan bevat de `BesluitMutatie` en/of `RegelingMutatie` wijzigingen ten opzichte van de versies die uit de eerdere publicatie van het besluit **plus** de correcties uit de eerste rectificatie zijn ontstaan.

# Informatieobjecten

Als met het te rectificeren besluit verkeerde versies van informatieobjecten meegeleverd zijn, dan moet het informatieobject volledig opnieuw worden aangeleverd met de rectificatie. Daarbij dient aangegeven te worden dat het informatieobject een wijziging betreft ten opzichte van het informatieobject dat eerder is gepubliceerd. Indien het een geografisch informatieobject (GIO) betreft gaat het om een [was-verwijzing](geo_xsd_Complex_Type_geo_GeoInformatieObjectVaststellingType.dita#GeoInformatieObjectVaststellingType_wasID) naar de eerder gepubliceerde GIO. De verwijzing naar de informatieobjectversie moet ook worden aangepast in de tekst van het besluit of de regeling. In de consolidatie-informatie moet de nieuwe informatieobjectversie via een [BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject) gekoppeld worden aan hetzelfde doel als de eerder gepubliceerde informatieobjectversie.

Het kan zijn dat de eerdere publicatie ten onrechte naar een nieuwe versie van een informatieobject verwijst. In dat geval hoeft alleen de verwijzing in de tekst aangepast te worden. In de consolidatie-informatie moet via een [TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) aangegeven worden dat de eerder gepubliceerde informatieobjectversie geen rol meer speelt bij de consolidatie.

# Service-informatie

Bij een rectificatie horen als annotatie:

- [Metadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata) van de rectificatie - Informatie over de rectificatie zelf (niet over het te rectificeren besluit).

- [Consolidatie-informatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie), waarin vastgelegd wordt wat de invloed is van de rectificatie op de consolidatie van regelingen en informatieobjecten die door het besluit (of in de eerdere publicatie ten onrechte) worden gecorrigeerd. 

De rectificatie kan ook aanleiding geven de [service-informatie bij het besluit](besluitproces_rectificatie_annotaties.md) te wijzigen.