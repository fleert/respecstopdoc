# Naamgevingsconventie

## Akoma Ntoso als basis
STOP hanteert een naamgevingsconventie voor het identificeren van documenten en data die is gebaseerd op de naamgevingsconventie van [Akoma Ntoso](https://docs.oasis-open.org/legaldocml/akn-nc/v1.0/os/akn-nc-v1.0-os.html) (of LegalDocML), afgekort AKN NC, een internationale standaard voor juridische teksten. AKN NC laat veel ruimte voor een nadere invulling. In STOP wordt die ruimte ingeperkt om uniciteit van identificaties te bereiken en om de bruikbaarheid van de identificatie in de keten te garanderen. 

Net als AKN hanteert STOP het [FRBR model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg) voor (versies van) documenten en data. Net als bij AKN NC wordt de identificatie opgebouwd uit:
* Een identificatie van het _work_; dit is een onveranderlijke identificatie die vindplaats-onafhankelijk is.
* De identificatie van een versie (_expression_) wordt afgeleid van de identificatie van het work.
* Elementen binnen de tekst van een versie (zoals een artikel) hebben een versie-afhankelijke identificatie ([eId](eid_wid.md)) die gebruikt wordt om naar een positie binnen een tekst te verwijzen.
* Elementen binnen de tekst hebben een versie-onafhankelijke identificatie ([wId](eid_wid.md)) die gebruikt wordt om hetzelfde element/object in de verschillende versies te herkennen.

Waar AKN NC soms een grote vrijheid kent in het opstellen van identificaties, staat STOP veel minder vrijheid toe:
* STOP schrijft een nadere invulling van de work en expression identificaties voor.
* STOP bevat aanvullende regels voor het toekennen van de [eId]/[wId].
* In AKN NC kan een versie van een tekst in componenten verdeeld worden; STOP ondersteunt dat slechts voor specifieke toepassingen
* STOP @@@IMOPVERSIE@@@ ondersteunt alleen Nederlandstalige expressions 

## Ontwerpkeuzes STOP vs AKN
STOP beperkt dus de ruime AKN NC. Deze beperkingen zorgen ervoor dat de volgende ontwerpkeuzes gelden:
* Als een identificatiestring een `@` bevat, is het altijd een **Expression**-identificatie.
* Als een identificatiestring een `.` bevat, is het altijd een **Manifestation**-identificatie

## AKN-identifiers zijn betekenisloos
De AKN-regels zoals STOP ze hanteert zijn alleen geschikt voor het *samenstellen* van identificaties. 
Voor alle AKN-identificaties (dus ook `wId` en `eId`) geldt: ze worden afgeleid uit betekenisvolle informatie, maar nadat ze samengesteld zijn moet software ze behandelen als betekenisloos. Het is uitdrukkelijk niet de bedoeling eenmaal gemaakte AKN-identifiers te interpreteren. Identifiers zijn geen informatiedragers. Zo kan bijvoorbeeld het versienummer niet afgeleid worden uit de identificatiestring. Dergelijke relevante informatie is beschikbaar als metadata van de regeling zelf.

**TODO: paradox oplossen. Bovenstaande alinea stelt dat ids betekenisloos zijn, en onderstaande alinea stelt dat "in sommige gevallen" de id wel geïnterpreteerd mag worden. Welke gevallen? Alles uitschrijven!** 

## AKN-identifiers worden gevalideerd
Sommige onderdelen van de AKN-identificatie hebben wel betekenis voor het lokaliseren van informatie, zoals de landcode (`/akn/nl/...`) of het documenttype. Daarnaast is de structuur betekenisvol. Software mag bijvoorbeeld ervan uitgaan dat het [context](eid_wid.md)-gedeelte van een een `eId` verwijst naar het container-element, en mag daar gebruik van maken om een verwijzing naar het element om te zetten naar een verwijzing naar het container-element.

STOP kent daarom een aantal bedrijfsregels om de correcte toepassing van de naamgevingsconventie voor AKN identifiers te valideren. Deze bedrijfsregels worden vermeld in de documentatie van de AKN-gerelateerde schema-elementen.

## AKN voor tekst, JOIN voor data {#join}
AKN NC is opgesteld voor documenten die een tekst bevatten. AKN NC bevat ook mogelijkheden om niet-tekstuele componenten te identificeren. Die mogelijkheden zijn voor toepassing in STOP te beperkt; zo kennen ze het onderscheid tussen work en expression niet. Daarom kent STOP een eigen identificatiemethodiek die qua opbouw gelijk is aan de AKN naamgevingsconventie: de **J**uridische **O**bject **I**dentificatie **N**aming convention. Deze "JOIN"-identificatie wordt gehanteerd voor data-works en -expressions, en voor identificaties van waarden en waardelijsten. Deze identificaties beginnen met `/join/` in plaats van `/akn/nl`. De JOIN-standaard is gebaseerd op best practices en lijkt qua structuur op de AKN NC.

## Work/expression identificaties in STOP
In STOP wordt AKN NC gebruikt voor:

* [Besluiten](besluit_naamgeving.md)
* [Regelingen](regeling_naamgeving.md)
* Overige [te publiceren documenten](publicatie_naamgeving.md), zoals kennisgevingen, mededelingen en rectificaties.
* [Publicaties](publicatieblad_naamgeving.md) in een publicatieblad
* [Geconsolideerde regeling](consolidatie_naamgeving.md).

De JOIN identificatie wordt gebruikt voor:

* [Informatieobjecten](io-expressionidentificatie.md).
* [Doelen](versiebeheer_naamgeving.md) als onderdeel van versiebeheer
* [Versie-informatie componenten](versieinformatie_naamgeving.md)
* [Geconsolideerde informatieobjecten](consolidatie_naamgeving.md).


## Tekstelementen in STOP
AKN NC schrijft een systematiek voor waarin elk tekstelement waarnaar potentieel verwezen kan worden een `eId`- én een `wId`-identifier krijgt. De `eId` is gerelateerd aan de positie in de tekst, de `wId` aan de inhoud.

De `eId` is bedoeld om vanuit een tekst naar een deel van een andere tekst te kunnen verwijzen: 

**Dynamisch**: `<work identificatie> "/" [ "!" <componentnaam> ] "#" eId`

**Statisch**: `<expression identificatie> "/" [ "!" <componentnaam> ] "#" eId`

Als vanuit data naar tekst verwezen wordt, dan worden de work/expression identificatie en de identificatie van een tekstelement apart gemodelleerd. Als vanuit data naar tekst verwezen wordt als de vindplaats van bepaalde informatie dan wordt het `wId` gebruikt. Dit type verwijzing is gebruikelijk voor annotaties die de inhoud van de tekst machine-leesbaar maken. Als in data een verwijzing naar de tekst staat die bedoeld is om in andere tekst te gebruiken, dan wordt in STOP `eId` gebruikt.

STOP stelt eisen aan de opbouw van de [eId/wId identifiers](eid_wid.md) die in alle tekstmodellen worden gebruikt. [Componentnamen](identificatie_tekstonderdelen.md) worden alleen in de tekst van een besluit gebruikt.

## Data-elementen in STOP
In het algemeen kent STOP geen systematiek om te verwijzen naar een element van een (meta)datamodule op een manier die vergelijkbaar is met het verwijzen naar tekstelementen. Dit kan wel in enkele speciale gevallen:

* Verwijzen naar een [GIO-deel](gio-tekstverwijzing.md#giodeel)
