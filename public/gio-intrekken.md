# Een GIO intrekken

Een regelingwijziging kan er toe leiden dat een GIO niet langer nodig is, bijvoorbeeld omdat het artikel dat naar betreffende GIO verwees, wordt geschrapt. De juridische werking van een GIO wordt beëindigd door het GIO in te trekken. Een intrekking moet tot uitdrukking komen in zowel de tekst als de [consolidatie-informatie](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) van het besluit. 


Dit doet men als volgt:

1. De verwijzing naar het GIO in de regelingtekst wordt verwijderd.
2. De JOIN-ID van het GIO in de bijlage van de regeling wordt verwijderd.
3. In de `ConsolidatieInformatie` van het besluit in `Intrekkingen` wordt aangegeven dat het GIO wordt ingetrokken.