# Opstellen regelgeving

In een project wordt gewerkt aan een nieuwe versie van regelgeving. Juridisch bestaat regelgeving uit:tekst, eventueel aangevuld met data: wat in de [Bekendmakingswet artikel 7 lid 1](https://wetten.overheid.nl/jci1.3:c:BWBR0004287&artikel=7&z=2021-07-01&g=2021-07-01) aangeduid wordt als _informatie die niet uit tekst bestaat_. Naast tekst en data kent STOP ook annotaties: aanvullende informatie over de (juridische) inhoud van een instrument die voor software te begrijpen is.

De tekst, data en annotaties worden in de standaard gebundeld tot twee type instrumenten:

[Regeling](regeling.md)
: Een regeling is het instrument waarmee regelgeving en beleid wordt vastgelegd. Een regeling bestaat uit gestructureerde tekst, eventueel aangevuld met annotaties.

[Informatieobject](io-intro.md)
: Met een informatieobject kan data, informatie die niet of moeilijk in tekst uit te drukken is, toch in regelgeving opgenomen worden. Naast de juridische data die onderdeel wordt van de regeling kan een informatieobject ook annotaties hebben. 

De standaard bevat mechanismen die voor beide instrumenten gelijk zijn:

[Verwijzingen naar regelgeving](verwijzingen.md)
: Op verschillende plaatsen komen verwijzingen voor vanuit een regeling naar een andere regeling of informatieobject. Sommige verwijzingen zijn informatiekundig en betekenen dat de regelingen waarnaar verwezen wordt bij elkaar horen, andere zijn juridisch en betekenen dat waarnaar verwezen wordt van toepassing is voor de regeling.

[Annotaties](annotatie-concept.md)
: Een annotatie bevat informatie over de (juridische) inhoud van een instrument die voor software te begrijpen is. Het kan gaan om een interpretatie of duiding van de inhoud, of bijvoorbeeld om aanwijzigingen voor de weergave ervan. Het annotatiemechanisme in STOP is uitbreidbaar: andere standaarden kunnen aanvullende annotaties definiëren die, als ze voldoen aan een aantal vereisten, synchroon gehouden kunnen worden met de geldende regelgeving.


**TODO**
<!-- Een regeling bestaande uit tekst en annotaties, informatieobjecten als niet-tekstueel onderdeel van regels (Bkw art 7 lid 1). Informatie over tekstmodellering, incl RegelingKlassiek in geconsolideerde vorm (placeholders voor wijzigartikelen en nog geen iwt-artikelen). Koppelen van niet-STOP informatie (bijv IMOW) via verwijzing naar regeling-work + doel (branch), naar analogie van de STOP-annotaties. -->

- Annotaties:
  - Algemeen - misschien iets als [deze](annotaties.md) eerste deel.
  - metadata - beschrijven we alleen in schema's?
  - [toelichtingrelaties](regeling_toelichting.md)
  - Afkomstig uit andere standaarden zoals IMOW: iets zeggen dat deze net als annotaties aan regelingversies
    gekoppeld is. Dat dit in STOP via work + doel gaat en niet met regelingversie.
- Informatie niet als STOP-tekst
  - Algemeen: aparte works: [Informatieobject](io-intro.md)
  - Enige voorbeeld tot nu toe: [GIO](gio-intro.md)
- Bij het schrijven van de regeling wordt ook al informatie verzameld om later het vaststellingsbesluit te onderbouwen.
  In deze tekst alleen noemen, details in het hoofdstuk over besluiten.
  - Motivatie uit [deze](samenwerken_modellering.md). In het klassieke model zou je al de BesluitKlassiek + 
    toelichting bij besluit gebruiken, maar uitwisseling daarvan zit niet STOP.
  - Rapporten, analyses e.d., in de vorm van PDF-informatieobjecten.
