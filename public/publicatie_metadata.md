# Metadata kennisgeving/mededeling

Om een kennisgeving of mededeling geautomatiseerd te kunnen verwerken, wordt extra informatie meegeleverd: de metadata.

De metadata van een [kennisgeving](data_xsd_Element_data_KennisgevingMetadata.dita#KennisgevingMetadata) of mededeling(TODO link) bestaat uit:

1. de bg-code (zie [waardelijsten](imop_waardelijsten.md)) van het [eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke) bevoegd gezag en van het bevoegd gezag dat het document heeft opgesteld([maker](data_xsd_Element_data_maker.dita#maker));
2. de [officiële titel](data_xsd_Element_data_officieleTitel.dita#officieleTitel) van het document;
3. aanduidingen voor het onderwerp/de [onderwerpen](data_xsd_Element_data_onderwerpen.dita#onderwerpen) van het document ten behoeve van rubricering van de publicatie;
4. indien het een kennisgeving van een besluit betreft, een verwijzing naar betreffend besluit([mededelingOver](data_xsd_Element_data_mededelingOver.dita#mededelingOver))
5. een aanduiding van het [soort Kennisgeving](data_xsd_Element_data_soortKennisgeving.dita#soortKennisgeving).



**Zie ook**

* Het [informatiemodel](EA_233F9A157B1F4d11A41DBE8FC3424244.dita#Pkg) van de kennisgeving.
* [XML schema](data_xsd_Element_data_KennisgevingMetadata.dita#KennisgevingMetadata) van de metadata van de kennisgeving.
* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-Metadata.xml) van de metadata van een kennisgeving.