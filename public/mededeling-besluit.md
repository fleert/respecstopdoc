# Mededelen van een besluit

Van een [besluit](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.6&artikel=3:43&z=2021-04-01&g=2021-04-01) moet mededeling worden gedaan aan alle belanghebbenden. Het mededelen van een besluit wordt niet in STOP geregeld. De mededeling van een besluit is geen vereiste voor de inwerkingtreding van het besluit. 
Wanneer een besluit tot bepaalde belanghebbenden is gericht, zoals het geval is bij besluiten op aanvraag als de omgevingsvergunning, wordt het besluit toegezonden of uitgereikt aan deze belanghebbenden. 
Bijvoorbeeld als iemand een omgevingsvergunning aanvraagt, wordt aan deze persoon het besluit tot wel of niet verstrekken van deze omgevingsvergunning verstuurd.
Bij een besluit van algemene strekking kan het zijn dat belanghebbenden eerder hun zienswijze over het ontwerpbesluit naar voren hebben gebracht, zoals in het geval van een [terinzagelegging](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.4&artikel=3:11&z=2021-04-01&g=2021-04-01
). In zo'n geval moet er ook bij besluiten van algemene strekking [mededeling](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.6&artikel=3:43&z=2021-04-01&g=2021-04-01
) worden gedaan aan de personen die eerder iets hebben ingebracht. 
Bij het mededelen van een besluit wordt altijd vermeld wanneer en hoe de bekendmaking ervan heeft plaatsgevonden. 
