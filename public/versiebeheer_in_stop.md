# Starten van een project

Als er aanleiding is om een nieuwe regeling ingesteld of een bestaande regeling gewijzigd moet worden, zal een bevoegd gezag (of een initiatiefnemer) dat meestal projectmatig oppakken. Dat wil zeggen: er wordt een traject in gang gezet waarbij eerst een (nieuwe of eerste) concept-versie van de regeling wordt opgesteld, gevolgd door een proces om uiteindelijk te komen tot een in een besluit vastgestelde versie van de regeling. Ook als het traject organisatorisch niet in de vorm van een project uitgevoergd wordt, dan wordt (in de ondersteunende software) al het werk ondergebracht in een project of in een projectmap geplaatst.

In het project wordt het werk gescheiden gehouden van eventuele andere projecten die tegelijkertijd lopen. Slechts op een aantal momenten zal bekeken (moeten) worden wat de invloed van die andere projecten is. Of en hoe de ondersteunende software functionaliteiten biedt om het tegelijk uitvoeren van projecten te vereenvoudigen is geen onderwerp voor STOP. Maar tijdens de uitvoering van een project kan wel informatie uitgewisseld worden, bijvoorbeeld tussen gemeente en adviesbureau of voor publicatie van ontwerpbesluiten. Het is dan van belang de informatie voor de verschillende projecten uit elkaar te kunnen houden.

STOP gaat uit van een werkwijze waarin de integrale versie van een regeling centraal staat: de tekst, bijbehorende informatieobjecten, metadata en andere annotaties die daarbij horen. Deze werkwijze is voor mensen beter te hanteren, omdat op elk moment aangebrachte wijzigingen in hun context te beoordelen zijn. Ook technisch is dit minder complex dan een standaard die gebaseerd is op uitwisseling van mutaties beschreven als een reeks van wijziginstructies, zoals [operational transformation](https://en.wikipedia.org/wiki/Operational_transformation) vereist. Het kennen van de integrale regelingversie is bovendien van belang voor het maken van annotaties zoals beschreven door de [IMOW-standaard](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD), maar zeker ook voor het maken van toepasbare regels volgens [STTR](https://iplo.nl/digitaal-stelsel/aansluiten/standaarden/sttr-imtr/).

STOP kent een vorm van versiebeheer om te kunnen herkennen in welke context integrale versies uitgewisseld worden. In deze sectie wordt besproken:

Principes van STOP versiebeheer
: De systematiek van het beheren van versies van regelingen is te vergelijken met versiebeheer zoals dat bij software-ontwikkeling wordt toegepast. De principes worden uitgelegd aan de hand van het veel gebruikte versiebeheerssysteem [`git`](https://nl.wikipedia.org/wiki/Git_(software)) en is [elders](versiebeheer_principes.md) gedocumenteerd. De termen die STOP gebruikt voor versiebeheer komen daar aan bod. 

[Eén of meer doelen/branches](versiebeheer_gebruik.md)
: De standaard laat het bevoegd gezag veel ruimte om naar eigen inzicht versiebeheer in te richten dat past bij de modellering in STOP. Toch zijn er een aantal richtlijnen te geven voor het gebruik van doelen/branches binnen een project. Het volgen van de richtlijnen bij de start van een project maakt het eenvoudiger om aan het eind ervan, als de resultaten van het project gaan bijdragen aan de geldende regelgeving, onverwachte situaties op te vangen.

[Modellering in STOP](versiebeheer_modellering.md)
: Een overzicht van de manier waarop het versiebeheer in STOP is opgenomen, wederom vergeleken met `git`-versiebeheer bij softwareontwikkeling. In dit overziocht worden de begrippen `doel`, `Momentopname`, `ConsolidatieInformatie` en `gemaaktOpBasisVan` geïntroduceerd. Vanuit deze sectie wordt verwezen naar de verschillende toepassingen van versiebeheer, waar deze begrippen in detail beschreven worden.

[Naamgeving van doel](versiebeheer_naamgeving.md)
: Voor de naamgeving van het doel is de naamgevingsconventie voor niet-tekstuele informatie van toepassing.

Op andere plaatsen in de documentatie wordt ingegaan op gerelateerde onderwerpen:

Kennisgeving van voornemen tot besluit
: Voor sommige type regelingen is het verplicht dat een bevoegd gezag het voornemen tot het nemen van een besluit publiceert. Dat gebeurt meestal voor (of rond) de tijd dat het project gestart wordt. In STOP is daarvoor een generieke [kennisgeving](publicatie_generiek.md) beschikbaar. De kennisgeving hoeft niet machine-leesbaar (bijvoorbeeld via metadata) in verband gebracht te worden met de regeling.

[Samenwerken aan regelgeving](samenwerken_aan_regelgeving.md)
: Het gebruik van versiebeheer in het scenario waarbij een adviesbureau en een bevoegd gezag samenwerken aan een nieuwe versie van regelgeving.

[Opstellen van een besluit](besluit.md)
: Het opstellen van een besluit waarin de instellingen of wijzigingen worden beschreven die resulteren uit het project.

[Consolidatie](consolideren.md)
: Het gebruik van versiebeheer om ervoor te zorgen dat de juiste tekst, informatieobjecten en annotaties behorend bij de geldige regelgeving beschikbaar komen.

