# data:ConsolidatieInformatie

Deze module voorziet in de samenhang tussen een `Doel` en een of meerdere Instrument(Versies); zoals Regeling(Versies) en GIO(versies). Eveneens kunnen er met het `Doel` verschillende tijdstempels worden meegegeven.

| Element                                                      | Vulling                                                      | Verplicht?           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------- |
| [BeoogdeRegelgeving](data_xsd_Element_data_BeoogdeRegelgeving.dita#BeoogdeRegelgeving)            | Container voor BeoogdeRegelingen                             | N                    |
| [Intrekkingen](data_xsd_Element_data_Intrekkingen.dita#Intrekkingen)                        | Container voor regelingen die met een doel worden ingetrokken. | N                    |
| [Tijdstempels](data_xsd_Element_data_Tijdstempels.dita#Tijdstempels)                        | Container voor tijd  tijdstempels voor de consolidatie. NB een ontwerpbesluit kent geen tijdstempels (zie ook: [data:ProcedureVerloop](quickstart_2_ProcedureVerloop.md)) | N                    |
| [BeoogdeRegelgeving/BeoogdeRegeling/doel](data_xsd_Element_data_doel.dita#doel), [BeoogdeRegelgeving/BeoogdInformatieobject/doel](data_xsd_Element_data_doel.dita#doel), [Intrekkingen/Intrekking/doel](data_xsd_Element_data_doel.dita#doel),  [Tijdstempels/Tijdstempel/doel](data_xsd_Element_data_doel.dita#doel) | Zie [JOIN IRI  - DOEL](#joiniri) hieronder                                   | J (binnen container) |
| [BeoogdeRegelgeving/BeoogdeRegeling/instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie) | De identificatie van de RegelingVersie geassocieerd met het doel. Zie module [data:FRBRExpression - Regeling](quickstart_1_ExpressionIdentificatieRegeling.md) | J (binnen container) |
| [BeoogdeRegelgeving/BeoogdInformatieobject/instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie) | De identificatie van de GIOVersie geassocieerd met het doel. Zie module [data:ExpressionIdentificatie - Informatieobject](quickstart_1_ExpressionIdentificatieGIO.md) | J (binnen container) |
| [BeoogdeRegelgeving/BeoogdeRegeling/eId](data_xsd_Element_data_eId.dita#eId), [BeoogdeRegelgeving/BeoogdInformatieobject/eId](data_xsd_Element_data_eId.dita#eId), [Intrekkingen/Intrekking/eId](data_xsd_Element_data_eId.dita#eId),  [Tijdstempels/Tijdstempel/eId](data_xsd_Element_data_eId.dita#eId) | Het eId van het Artikel in het Besluit waarin de datum vermeld staat waarop het gespecificeerde doel in werking treedt. Deze dient gevuld te zijn, indien de datum inwerkingtreding bekend is (zie soortTijdstempel) hieronder. | N                    |
| [Intrekkingen/Intrekking/Instrument](data_xsd_Element_data_instrument.dita#instrument)    | De identificatie van het instrument dat wordt ingetrokken. Bijvoorbeeld de AKN [data:FRBRWork](quickstart_1_ExpressionIdentificatieRegeling.md) van de Regelingversie of de JOIN FRBRWorkIdentification van de GIOversie | J (binnen container) |
| [Tijdstempels/Tijdstempel/soortTijdstempel](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) | Enumeratie <ul><li>juridischWerkendVanaf</li><li>geldigVanaf</li></ul> voor meer details zie [data:soortTijdstempel](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) | N                    |
| [Tijdstempels/Tijdstempel/datum](data_xsd_Element_data_datum.dita#datum)             | De datum waarop de tijdstempel van kracht is                 | N                    |



## JOIN IRI - DOEL { #joiniri }

Voor de identificatie van het het 'doel' wordt gebruikt gemaakt van de  Juridische Object Identificatie Naming convention (JOIN). Uitgaande van een Nederlands bevoegd gezag ziet de JOIN IRI van een besluit er als volgt uit: 

`//doel: "/join/id/proces/"<overheid> "/"<datum_work> "/" <overig>`

[Toelichting](versiebeheer_naamgeving.md)




## Voorbeeld

[Voorbeeld ConsolidatieInformatie bij Definitief Besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/02-BG-Besluit-Consolidatie-informatie.xml)

