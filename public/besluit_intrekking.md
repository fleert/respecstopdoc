# Intrekking van een wijzigingsbesluit

Nadat een wijzigingsbesluit is vastgesteld en bekendgemaakt kan het bevoegd gezag later tot het inzicht komen dat het besluit toch beter niet in werking kan gaan. Er zijn een aantal situaties te onderscheiden:

![](img/IntrekkingBesluit.png)

**1. Afzien van inwerkingtreding**

Als er nog geen inwerkingtreding voor het in te trekken besluit is gepubliceerd, dan kan BG af zien van het publiceren van een inwerkingtredingsdatum: door het besluit niet in werking te laten treden zal het besluit nooit meegenomen worden in de consolidatie. 

Hoeft alleen een deel in werking te treden, dan is er sprake van een [gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md). In beide situaties wordt aangeraden niet het mechanisme van de intrekking van een besluit te gebruiken.

**2. Doorhalen inwerkingtredingsdatum**

Als het besluit al wel een inwerkingtredingsdatum kent, maar is deze datum nog niet bereikt dan kan het BG besluiten de inwerkingtredingsdatum door te halen. Het BG besluit dan om de inwerkingstredingsdatum tot nader order uit te stellen.

Het besluit met de doorhaling bestaat dan uit:

* In het lichaam van het besluit staat dat de inwerkingtredingsdatum van het besluit wordt gewist.
* In de consolidatie-informatie bij het besluit wordt voor het doel/de doelen van het besluit een [TerugtrekkingTijdstempel](data_xsd_Element_data_TerugtrekkingTijdstempel.dita#TerugtrekkingTijdstempel) aangeleverd.

**3. Intrekkingsbesluit**

Juridisch is een intrekking van een besluit een nieuw wijzigingsbesluit, dat met de woorden "het besluit ... wordt ingetrokken" een wijziging beschrijft die het omgekeerde is van het oorspronkelijke besluit. De wijziging die in het besluit werd beschreven vervalt en de situatie voorafgaand aan het besluit herleeft. De intrekking heeft een eigen inwerkingtreding.

Het besluit met de intrekking bestaat dan uit:

* In het lichaam van het besluit waarin staat dat het besluit ingetrokken wordt.
* In de consolidatie-informatie bij het besluit wordt voor elke regeling en elk informatieobject uit het in te trekken besluit het [consolidatiepatroon intrekking besluit](consolideren_patronen_1intrekkingbesluit.md) toegepast.
* Er hoeft geen nieuwe versie van de regelingen/informatieobjecten meegeleverd te worden.

In de huidige praktijk wordt ook de gedeeltelijke intrekking toegepast. Daar is in STOP geen direct equivalent voor. In STOP moet in dit geval een wijzigingsbesluit opgesteld worden die het in te trekken deel verwijdert.