# Identificatie van een te publiceren document
Een te publiceren juridisch document krijgt twee identificaties:

* één voor het document als work en
* één voor de versie (een expression).

STOP gebruikt een [invulling](naamgevingsconventie.md) van de Akoma Ntoso naamgevingsconventie (AKN NC) als basis voor de identificatie. AKN NC schrijft de structuur van de identificatie voor, STOP vult dat nader in.

Voor de documenten (behalve [besluit](besluit_naamgeving.md) en [regeling](regeling_naamgeving.md)) die een bevoegd gezag opstelt als bron van een publicatie wordt in de identificatie "**doc**" gebruikt:

**Work**: `"/akn/" <land> "/doc/" <overheid> "/" <datum_work> "/" <overig> `

**Expression**: `<work> "/" <taal> "@"  <datum_expr> [ ";" <versie> ] [ ";" <overig> ]`

De codes betekenen:

| code         | betekenis                                                    | verplicht?                                          |
| ------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| land | Een 2-letter code van een land volgens ISO 3166-1 of van een code voor een subdivisie volgens ISO 3166-2. In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nl` ondersteund. | Ja |
| overheid     | Code van het bevoegde gezag volgens één van de [waardelijsten](imop_waardelijsten.md#overheid). Het gaat hierbij om het bevoegd gezag dat uiteindelijk verantwoordelijk is voor het document, het is geen aanduiding van de opsteller ervan. | Ja |
| datum_work   | Datum van het ontstaan van de eerste versie van het work. Dit mag een volledige datum zijn (YYYY-MM-DD conform ISO 8601) maar het mag ook alleen een jaartal zijn. | Ja |
|taal | Taalcode volgens [ISO 639-2 alpha-3](https://www.iso.org/iso-639-language-codes.html). In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nld` ondersteund. | Ja |
| datum_expr   | Datum van het ontstaan van deze versie van het expression. Dit mag een volledige datum zijn (YYYY-MM-DD conform ISO 8601) maar het mag ook alleen een jaartal zijn. Deze datum moet gelijk zijn of later liggen dan `<datum_work>` | Ja |
| versie       | Optioneel. Een [versienummer](data_xsd_Element_data_versienummer.dita#versienummer) voor het document. | Nee |
| overig       | Overige kenmerken om de Expression (optioneel) of het Work (verplicht) te onderscheiden, bijvoorbeeld een dossiernummer of een afkorting gebaseerd op de naam. De kenmerken moeten compact zijn, het is niet de bedoeling om hiervoor een lange titel te gebruiken. "`<overig>`" bestaat uit een combinatie van cijfers, boven- en onderkast-letters, "_" en "-", te beginnen met een cijfer of letter (regex `[a-zA-Z0-9][a-zA-Z0-9\_\-]*`) met een maximale lengte van 128 karakters. Het bevoegd gezag is zelf verantwoordelijk voor uniciteit van dit "overig" deel. Er is geen centrale service om de waarde voor "overig" te genereren. | Ja, bij een work. Nee, bij een expressie. |

Voorbeelden zijn:

* `/akn/nl/doc/gm0503/2018/BW-OW-123`: Een document (Work) van Gemeente Delft uit 2018 met kenmerk `BW-OW-123` 
* `/akn/nl/doc/gm0503/2018/BW-OW-123/nld@2018-12-02`: de expression van 2 december 2018 van dat document.
