# Wijzigingsoverzicht

## Wijzigingshistorie
Een aanpassing in de schema's, schematrons en/of transformaties leidt tot een nieuwe versie van het implementatiemodel en bijbehorende documentatie. De documentatie kan worden bijgewerkt zonder dat het versienummer van het implementatiemodel wijzigt.

Op deze pagina staan de wijzigingen beschreven voor IMOP @@@IMOPVERSIE@@@. Wijzigingen in eerdere versies van het informatiemodel staan beschreven in het eerder gepubliceerde wijzigingsoverzicht van [versie 1.3.0](https://koop.gitlab.io/STOP/standaard/1.3.0/imop_wijzigingsoverzicht.html). Wijzigingen in de standaard (STOP) zijn [apart](stop_wijzigingsoverzicht.md) beschreven.

## Implementatiemodel 

### Uitleverdatum @@@DATUM@@@

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* [data:bekendOp](data_xsd_Element_data_bekendOp.dita#bekendOp) is verplaatst in de [data:ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) omdat in bepaalde situaties de `bekendOp` datum verschillend kan zijn voor de verschillende onderdelen van de `ConsolidatieInformatie`.
* [data:BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) en [data:BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject) kunnen nu òf een [data:instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie) òf een [data:instrument](data_xsd_Element_data_instrument.dita#instrument) bevatten. Een `instrument` moet aangeleverd worden in de situatie dat door bijvoorbeeld een uitspraak van de rechter ook een historische versie aangepast zou moeten worden waarvoor geen consolidatieplicht bestaat. In dergelijke situaties wordt alleen de actuele versie bijgewerkt en worden eventuele de historische versies die ook geraakt worden door de uitspraak als "onbekend" aangeduid door het meegeven van een `instrument` in plaats van een `instrumentVersie`.

### Uitleverdatum 29-06-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* De `eId`-verwijzing bij [data:Intrekking](data_xsd_Element_data_Intrekking.dita#Intrekking), [data:TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) en [data:TerugtrekkingIntrekking](data_xsd_Element_data_TerugtrekkingIntrekking.dita#TerugtrekkingIntrekking) is nu optioneel in [data:ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie). De `eId`-verwijzing naar de te verwijderen [tekst:ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) is namelijk soms onmogelijk als gebruik gemaakt wordt van [tekst:VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling). Als gebruik gemaakt wordt van renvooi met een expliciete verwijderactie, blijft de verplichting bestaan om de `eId`-verwijzing naar de te verwijderen `ExtIoRef` op te nemen.

### Uitleverdatum 15-06-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Aan het imop-tekst schema zijn 'Facetten' toegevoegd die beperkingen opleggen aan de inhoud van de [eId](tekst_xsd_Simple_Type_tekst_dtEID.dita#dtEID) en [wId](tekst_xsd_Simple_Type_tekst_dtWID.dita#dtWID) attributen van de tekstelementen. Deze beperkingen komen voort uit de [voorgeschreven wijze](eid_wid.md) van samenstellen van deze attributen.
* [Issue 203](https://gitlab.com/koop/STOP/standaard/-/issues/203): - In het imop-tekst schema zijn voor tekst:BesluitKlassiek de elementen tekst:Aanhef en tekst:Sluiting verwijderd.

### Uitleverdatum 16-05-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Net zoals in [ActueleToestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) ook aan [cons:Proefversies](cons_xsd_Element_cons_Proefversies.dita#Proefversies) het element [cons:materieeluitgewerktOp](cons_xsd_Element_cons_materieelUitgewerktOp.dita#materieelUitgewerktOp) toegevoegd, zodat een ontwerpbesluit dat niet langer relevant is als materieel uitgewerkt aangeduid kan worden, waardoor de proefversie van het ontwerpbesluit niet langer getoond wordt in het DSO-LV. Zie ook het bijbehorende [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/02a%20Ontwerpbesluit%20materieel%20uitgewerkt).

### Uitleverdatum 02-05-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Bedrijfsregels STOP1039 en STOP1040 toegevoegd voor [data:Begripsrelaties](data_xsd_Element_data_Begripsrelaties.dita#Begripsrelaties).
* De verplichte elementen in [se:FeatureTypeStyle](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle) en [gio:JuridischeBorgingVan](gio_xsd_Element_gio_JuridischeBorgingVan.dita#JuridischeBorgingVan) zijn optioneel gemaakt, zodat het ook mogelijk is om een 'lege' module aan te leveren, waarmee aangeduid wordt dat de bestaande versie van de annotatie niet meer van toepassing is voor een nieuwe versie van de GIO.
* In [data:toelichtingOp](data_xsd_Element_data_toelichtingOp.dita#toelichtingOp) is [data:IO](data_xsd_Element_data_IO.dita#IO) toegevoegd om een toelichtingsrelatie naar een informatieobject (PDF-document) te kunnen leggen.

### Uitleverdatum 06-04-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* [Gebiedsmarkering](geo_xsd_Element_geo_Gebiedsmarkering.dita#Gebiedsmarkering) en [Effectgebied](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied) opgenomen in het [geo-schema](geo_xsd_Main_schema_geo_xsd.dita#geo.xsd), tevens enkele technische verbeteringen van het geo-schema aangebracht.
* Beëindiging zichtbaarheid geconsolideerde regeling toegevoegd:
  * [data:MaterieelUitgewerkt](data_xsd_Element_data_MaterieelUitgewerkt.dita#MaterieelUitgewerkt) in [data:ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) om met een revisie de zichtbaarheid van een regeling te kunnen beëindigen (ook [data:TerugtrekkingMaterieelUitgewerkt](data_xsd_Element_data_TerugtrekkingMaterieelUitgewerkt.dita#TerugtrekkingMaterieelUitgewerkt) t.b.v. foutherstel in een rectificatie).
  * [cons:materieelUitgewerktOp](cons_xsd_Element_cons_materieelUitgewerktOp.dita#materieelUitgewerktOp) in [toestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) van een geconsolideerde regeling waarmee aangegeven wordt dat de regeling materieel is uitgewerkt.
* [Gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md):
  * In het [imop-tekst.xsd](tekst_xsd_Main_schema_tekst_xsd.dita#tekst.xsd) het element [tekst:NogNietInWerking](tekst_xsd_Element_tekst_NogNietInWerking.dita#NogNietInWerking) toegevoegd.
  * Schema [tekst:Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) qua opbouw gelijk getrokken met [tekst:Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel).
* [tekst:Vervallen](tekst_xsd_Element_tekst_Vervallen.dita#Vervallen) enkele technische schema-verbeteringen.
* In imop-tekst.xsd [tekst:Mededeling](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling) - nieuw root-element toegevoegd.
* In imop-data.xsd [data:MededelingMetadata](data_xsd_Element_data_MededelingMetadata.dita#MededelingMetadata) - Metadata voor Mededeling toegevoegd.
* In imop-data.xsd zijn aan de elementen [data:BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling), [data:BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject), [data:TerugtrekkingRegeling](data_xsd_Element_data_TerugtrekkingRegeling.dita#TerugtrekkingRegeling), [data:TerugtrekkingInformatieobject](data_xsd_Element_data_TerugtrekkingInformatieobject.dita#TerugtrekkingInformatieobject) en [data:Intrekking](data_xsd_Element_data_Intrekking.dita#Intrekking) de elementen [data:bekendOp](data_xsd_Element_data_bekendOp.dita#bekendOp) en [data:gemaaktOp](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) toegevoegd.
* In imop-consolidatie.xsd zijn elementen cons:Toestand, cons:ToestandMetSamenloop en cons:BekendeToestand vervangen voor een nieuw model met de volgende elementen:
  * [cons:CompleteToestanden](cons_xsd_Element_cons_CompleteToestanden.dita#CompleteToestanden) met daarbinnen een of meer [cons:ToestandCompleet](cons_xsd_Element_cons_ToestandCompleet.dita#ToestandCompleet).
  * [cons:ActueleToestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) met daarbinnen een of meer [cons:ToestandActueel](cons_xsd_Element_cons_ToestandActueel.dita#ToestandActueel).
* In het [STOP-modules overzicht](imop_modules.md#mededeling) de modules van een Mededeling opgenomen.
* Annotatie voor [data:Begripsrelaties](data_xsd_Element_data_Begripsrelaties.dita#Begripsrelaties) toegevoegd aan de xsd imop-data.xsd.
* Technische verbetering in schema [imop-data.xsd](data_xsd_Main_schema_data_xsd.dita#data.xsd) voor documentatiedoeleinden - deze verbetering verandert de validatie van de XSD niet: enkele lokaal gedefinieerde enumeraties in  zijn nu als globaal `xs:simpleType` bij de elementen
  * [data:publicatieinstructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie)
  * [data:soortRef](data_xsd_Element_data_soortRef.dita#soortRef)
  * [data:soortKennisgeving](data_xsd_Element_data_soortKennisgeving.dita#soortKennisgeving)
  * [data:soortMededeling](data_xsd_Element_data_soortMededeling.dita#soortMededeling)

## Schemadocumentatie 

### Uitleverdatum @@@DATUM@@@

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Schemadocumentatie van [data:ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie), [data:BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) en [data:BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject) uitgebreid naar aanleiding van verplaatsing [data:bekendOp](data_xsd_Element_data_bekendOp.dita#bekendOp) en toevoegen [data:instrument](data_xsd_Element_data_instrument.dita#instrument).

### Uitleverdatum 29-06-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Uitzondering gedocumenteerd dat [data:eId](data_xsd_Element_data_eId.dita#eId) weggelaten mag worden in de Consolidatie-informatie als een informatieobject wordt ingetrokken of teruggetrokken met [tekst:VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling).

### Uitleverdatum 06-04-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

- Bij elementen waarvan de waarde uit een waardelijst komt een verwijzing naar [overzicht waardelijsten](imop_waardelijsten.md) opgenomen. 
- Bij de elementen [geo:eenheidID:GeoInformatieObjectVersieType](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidID) en [geo:normID:GeoInformatieObjectVersieType](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normID) verwijzingen naar de juiste waardelijst in de DSO stelselcatalogs opgenomen.
- Schemadocumentatie van [geo:Gebiedsmarkering](geo_xsd_Element_geo_Gebiedsmarkering.dita#Gebiedsmarkering) en [geo:Effectgebied](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied) toegevoegd.
- Schemadocumentatie van [tekst:NogNietInWerking](tekst_xsd_Element_tekst_NogNietInWerking.dita#NogNietInWerking) toegevoegd.
- Schemadocumentatie van [tekst:Mededeling](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling) en [data:MededelingMetadata](data_xsd_Element_data_MededelingMetadata.dita#MededelingMetadata) toegevoegd.

## Waardelijsten

### Uitleverdatum 04-07-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* [Issue 206](https://gitlab.com/koop/STOP/standaard/-/issues/206):
  * Code `/join/id/stop/regelingtype_014` voor 'Omgevingsplanregels uit projectbesluit'
  * Code `/join/id/stop/regelingtype_015` voor 'Voorbeschermingsregels Omgevingsplan'
  * Code `/join/id/stop/regelingtype_016` voor 'Voorbeschermingsregels Omgevingsverordening' toegevoegd in waardelijst [soortregeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortregeling.xml).  

### Uitleverdatum 06-04-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

- Code `/join/id/stop/procedure/stap_022`  voor 'Materieel uitgewerkt' toegevoegd in waardelijst [procedurestap_ontwerp](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/procedurestap_ontwerp.xml).
- Code `/join/id/stop/soortpublicatie_004` voor 'Mededeling' toegevoegd in waardelijst [soortpublicatie](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortpublicatie.xml).
- Code `/join/id/stop/work_025` voor 'Mededeling' toegevoegd in waardelijst [soortwork](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortwork.xml).

## Schematron

### Uitleverdatum @@@DATUM@@@

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Aanpassingen in schematron `imop-consolidatie.sch` om nu ook `Instrument` in plaats van alleen `InstrumentVersie` in `beoogdeRegeling` en `beoogdInformatieobject` af te handelen.

### Uitleverdatum 06-04-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:
- Validatie in imop-procedure.sch aangepast zodat ook procedurestap: /join/id/stop/procedure/stap_022 wordt gevalideerd.


## Voorbeelden

### Uitleverdatum 29-06-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:
* [Voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/11%20Gefaseerde%20inwerkingtreding) toegevoegd van een besluit met gefaseerde inwerkingtreding.

* [Voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/00%20Kennisgeving%20voorgenomen%20besluit) toegevoegd van een kennisgeving over een voorgenomen besluit.

### Uitleverdatum 16-05-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* [Voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/02a%20Ontwerpbesluit%20materieel%20uitgewerkt) toegevoegd van een materieel uitgewerkt ontwerpbesluit.
* [Voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/08%20Conditionele%20inwerkingtreding) van een besluit met een conditionele inwerkingtreding.

### Uitleverdatum 02-05-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* In alle voorbeelden zijn zowel de aanlever-module `ConsolidatieInformatie` als de uitlever-module `ActueleToestanden` aangepast naar aanleiding van de wijzigingen in deze modules in STOP @@@IMOPVERSIE@@@:
  * [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) van een *wijzigings*besluit bevat nu altijd [`gemaaktOpBasisVan`](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan)
  * Oude `Toestanden` modules vervangen door [ActueleToestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden)

* [Voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/10%20Terugwerkende%20kracht) van een wijziging met terugwerkende kracht (TWK) toegevoegd.

* Voorbeelden 'Voortbouwen op STOP' verwijderd. In de reguliere voorbeelden zijn betreffende situaties inmiddels ook terug te vinden.
* Voorbeeld van de module [Begripsrelaties](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Coderingen/Begripsrelaties) toegevoegd.

### Uitleverdatum 06-04-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Voorbeelden van [Gebiedsmarkering](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/08%20Gebiedsmarkering) en [Effectgebied](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/09%20Effectgebied) toegevoegd.
* Voorbeeld van de codering van een [Instructie](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/07%20Instructie) toegevoegd. 
* Voorbeeld van de codering van [materieel uitgewerkt](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/07%20Beperking%20vindbaarheid) van een geconsolideerde regeling. Hiermee wordt de vindbaarheid van een geconsolideerde regeling beperkt.
* Voorbeelden van [Regeling klassiek](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/index.md) verplaatst en uitgebreid met:
  * [Initiële regeling klassiek met gio zonder in werkingtreding bepaling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/03%20Initiele%20regeling%20met%20gio%20zonder%20iwt)
  * [Inwerkingtredingsregeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/04%20Inwerkingtreding)
  * [Gedeeltelijke inwerkingtredingsregeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/05%20Gedeeltelijke%20inwerkingtreding)
  * [Inwerkingtredingsregeling overige artikelen](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/06%20Resterende%20inwerkingtreding)
  * [Gedeeltelijke inwerkingtreding met wijzigingen](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/07%20Gedeeltelijke%20inwerkingtreding%20met%20wijzigingen)
  
* In de voorbeelden van de [Scenario's mbt gerechtelijke macht](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht) de kennisgeving van de uitspraak van de rechter gewijzigd in de juridisch correcte [Mededeling](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling) met de uitspraak van de rechter.
