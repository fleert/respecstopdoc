# Effectgebieden

Een effectgebied is het gebied waarin belanghebbenden naar verwachting gevolgen zullen ondervinden van een besluit. Het effectgebied moet op grond van de Regeling elektronische publicaties (REP [bijlage 1](https://wetten.overheid.nl/jci1.3:c:BWBR0045086&bijlage=1&z=2022-01-01&g=2022-01-01)) in bepaalde situaties worden meegeleverd met een besluit. Op basis van [artikel 4.1 van de REP](https://wetten.overheid.nl/BWBR0045086/2022-01-01/#Hoofdstuk4_Artikel4.1) krijgen alle burgers ouder dan 25 jaar berichten met betrekking tot bepaalde besluiten binnen een straal van hun *woonadres*, tenzij zij hebben aangegeven deze niet te willen ontvangen. Hiervoor wordt een standaard straal van 125 tot 500 meter vanaf het woonadres gehanteerd, afhankelijk van de adressendichtheid in de omgeving. Deze attendering verloopt via de Berichtenbox van [mijn.overheid.nl](https://mijn.overheid.nl). 

Daarnaast wordt naar aanleiding van de REP ook de [attenderingsservice van overheid.nl](https://www.overheid.nl/berichten-over-uw-buurt) aangepast. Hier kan elke burger zich laten attenderen op overheidsbesluiten op basis van een *willekeurig adres* plus een zelf gekozen straal van maximaal 3000 meter. 

Van sommige besluiten kunnen de gevolgen echter op grotere afstand merkbaar zijn. Bijvoorbeeld bij stank- of geluidsoverlast. Het bevoegd gezag kan in deze situaties door het leveren van een effectgebied een groter gebied aangeven waarbinnen de burgers via attenderingen op het besluit worden gewezen. *Alleen in deze situaties* levert het BG hiertoe een effectgebied-geometrie aan bij een besluit. 

Zie [attenderen](attenderen.md) voor de wijze waarop het effectgebied in STOP is verwerkt.
