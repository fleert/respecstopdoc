# Regelgeving digitaal

Ten tijde van het schrijven van de eerste versie van de standaard was het niet mogelijk informatie voor het toepassingsgebied van de standaard zonder handmatige conversies of interpretaties te transporteren van bron (opstellers van regelgeving) tot eindgebruikers (lezers van regelgeving of software die gebaseerd is op regelgeving). Voor deelgebieden waren er informatiemodellen, met name:

* OP-XML en BWB-XML voor het uitwisselen van juridische teksten (bekendmakingen en consolidaties).
* IMRO voor het uitwisselen van tekst en geo-informatie voor ruimtelijke plannen.

OP-XML is ontstaan als digitalisering van het publiceren van papieren juridische documenten en biedt geen goede ondersteuning voor een geautomatiseerd creatieproces van (versies van) regelgeving. IMRO is ontstaan als digitalisering van regelgeving die deels als (niet-tekstuele) data is beschreven, voor een type regelgeving die wettelijk uitgezonderd was van de juridische eisen die aan de overige regelgeving gesteld worden. IMOW is de opvolger van IMRO in het Omgevingswetdomein.

STOP is bedoeld om de werelden van OP-XML en IMRO bij elkaar te brengen en - voor zover dat van toepassing is op regelgeving uit andere domeinen -  domein-specifieke toepassingen mogelijk te maken, en te komen tot een automatiseerbaar creatieproces. Dat is terug te vinden in vier uitgangspunten:

Regelgeving bestaat uit tekst en data
: Traditioneel worden alle aspecten van regelgeving beschreven in tekst. Dat is niet altijd de beste manier om informatie vast te leggen. In STOP (en in de regelgeving rond bekendmaken) is daarom een voorziening gekomen, [informatieobjecten](io-intro.md), waarmee data onderdeel kan worden van de regelgeving op een manier die voldoet aan dezelfde juridische eisen als tekst wanneer de data onderdeel is van een [besluit](besluit.md). De eerste toepassing van dit mechanisme zijn de [GIO's](gio-intro.md).

Regelgeving is uitbreidbaar met domein-specifieke data
: Informatieobjecten zijn een generieke manier om data onderdeel te maken van de regelgeving. Voor specifieke domeinen kunnen informatiemodellen gedefinieerd worden die de tekst en data nader duiden (zoals IMOW voor het Omgevingswetdomein). De standaard kent zowel mechanismen om de duiding te relateren aan de regelgeving ([wId](eid_wid.md) in tekst, [JuridischeBorgingVan](gio_xsd_Element_gio_JuridischeBorgingVan.dita#JuridischeBorgingVan) voor informatieobjecten) als een manier om de juridische geldigheid van de informatie [af te leiden](consolideren_annotaties.md) van de geldigheid van de regelgeving, mits de domein-specifieke informatie aan een aantal [voorwaarden](annotaties_non-stop.md) voldoet.

[Creatieproces is automatiseerbaar](automatiseerbaar-creatieproces.md)
: Twee belangrijke toepassingen van de standaard zijn het beschikbaarstellen van geldende regelgeving en het publiceren van besluiten om die regelgeving te wijzigen. Daaraan liggen traditioneel twee verschillende processen ten grondslag met een lage automatiseringsgraad. Beide processen delen echter dezelfde bron. Uitgangspunt voor STOP is dat de modellen voor regelgeving en besluiten zo geconstrueerd worden dat het proces om te komen van bron tot geldende regelgeving in grote mate geautomatiseerd kan worden.

Maximaal één geldige regeling-/informatieobjectversie
: Vanwege de automatiseerbaarheid kent STOP op elk moment maar één geldige versie van een regeling of informatieobject. In besluiten is het juridisch toegestaan om te schrijven dat de ene versie van een regeling onder bepaalde condities geldt en een andere versie onder andere condities. Als software in staat moet zijn te begrijpen wanneer welke versie van toepassing is, moeten de condities op een machine-leesbare manier gecodeerd worden. Dat is complexe materie die niet door de standaard ondersteund wordt. 

Presentatie wordt bepaald door de toepassing: [functioneel presenteren](functioneelpresenteren.md)
: STOP is een standaard voor uitwisseling van te publiceren informatie en niet van de publicatie zelf. De standaard is daarom vooral gericht op het modelleren van informatie en niet op het coderen van de opmaak en visuele weergave ervan. De informatie moet geschikt zijn om weergegeven te worden in diversie toepassingen, van websites tot authentieke PDF-bekendmakingen in een publicatieblad, op mobiele apparaten of op PCs met grote schermen, documentgericht of gefilterd voor specifieke domeinen. Alleen als het een functie heeft om de presentatie voor te schrijven is het onderdeel van de standaard (bijvoorbeeld voor [GIO's](gio-symbolisatie.md)). In alle andere gevallen is het aan de applicatie die de standaard gebruikt om de presentatie te bepalen. Dus lettertype, fontgrootte, kleur, et cetera. zijn alleen onderdeel van de standaard als daar betekenis aan gehecht wordt.

