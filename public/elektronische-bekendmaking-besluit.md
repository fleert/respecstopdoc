# Elektronische bekendmaking besluit

Het is wettelijk verplicht de [bekendmaking van besluiten elektronisch](https://wetten.overheid.nl/BWBR0024913/2011-01-01) te laten verlopen. Voor een groot deel van de (Rijks)besluiten gold dit al. Voor het elektronisch publiceren van omgevingswetbesluiten is er een speciale landelijke voorziening bekendmaken en beschikbaar stellen (LVBB) ontwikkeld. Deze maakt het elektronisch bekendmaken van onder meer Omgevingswet-besluiten met zowel tekst als andere data (zoals geometrie) mogelijk, en levert deze door aan het DSO-LV. 
Het bronhouderskoppelvlak binnen de LVBB is in het leven geroepen zodat overheden maar via één kanaal een officiële publicatie en bijbehorende omgevingswetinformatie hoeven aan te leveren aan een centrale voorziening. Via deze centrale voorziening wordt het omgevingswetbesluit in het relevante elektronische publicatieblad bekend gemaakt en vervolgens doorgeleverd aan de landelijke voorziening van DSO, en andere systemen om daar getoond te kunnen worden in de viewer Regels op de Kaart.
Voor omgevingswetbesluiten bepaalt de [Regeling elektronische publicaties](https://wetten.overheid.nl/BWBR0045086) in welke gevallen het gebruik van de LVBB en dus STOP verplicht is voor de bekendmaking. 


Voor de [inwerkingtreding van een besluit](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.6) en dus de juridische geldigheid ervan is vereist dat besluit geldig door middel van de LVBB bekend is gemaakt. Hiervoor is niet vereist dat de bijbehorende regeling is doorgeleverd aan OZON of zichtbaar is in de viewer [Regels op de Kaart](https://preprod.viewer.dso.kadaster.nl) in het Omgevingsloket van DSO-LV: maatgevend is de bekendmaking. 

Bij een storing in DSO-LV is een besluit dus nog steeds geldig als:
* het omgevingswetbesluit **wel** bekend gemaakt wordt via de LVBB in het desbetreffende elektronische publicatieblad
* maar **niet** verder doorgeleverd kan worden aan de andere onderdelen van DSO-LV (OZON, Viewer Regels op de Kaart etc.)

Dit komt omdat bekendmaking vereist is voor de [inwerkingtreding](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.6&artikel=3:40&z=2021-04-01&g=2021-04-01) van het besluit. 
