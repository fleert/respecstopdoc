# Modellen voor informatieobjecten

STOP kent de volgende type informatieobjecten:

[Geografisch Informatieobject (GIO)](gio-intro.md)
: Een geografisch informatieobject bevat geometrische data die de begrenzing van een of meer locatie(s) aangeeft.

[Portable document format (PDF)](IO_Document.md)
: Een document dat in zijn oorspronkelijke vorm deel uit moet maken van een regeling of in die vorm bij een regeling moet worden gevoegd, wordt uitgewisseld in het pdf-formaat zodat het document in zijn oorspronkelijke vorm gereproduceerd kan worden. 

