# In- en uitwerkingtredingsbepalingen

In de tekst van een besluit staat vaak ook iets over de inwerkingtreding van (delen van) het besluit. In een besluit kan ook aangegeven worden dat een regeling ingetrokken wordt. Op de datum dat de intrekking in werking treedt, houdt de regeling op geldig te zijn, welke versie van de regeling op dat moment ook geldig is.

Zowel de inwerkingtreding als de intrekking worden in STOP met een datum aangegeven. Er kan daarom maar één geldige versie van een regeling (en van een informatieobject) per dag zijn.

Bepalingen over de inwerkingtreding en/of over de intrekking van een regeling moeten in een [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel) worden opgenomen (in het [klassieke model](besluit-klassiek.md): in een [InwerkingtredingArtikel](tekst_xsd_Element_tekst_InwerkingtredingArtikel.dita#InwerkingtredingArtikel)) in het lichaam van het besluit. Het lichaam van een besluit wordt ook wel 'dictum' genoemd. Besluiten die uitsluitend bedoeld zijn om een eerder bekendgemaakt besluit in werking te laten treden zullen dit als enig artikel hebben.

Juridisch telt wat in de tekst staat, maar om de [geautomatiseerde consolidatie](consolideren.md) te ondersteunen moet deze informatie ook machine-leesbaar in de [consolidatie-informatie](consolidatie-informatie.md) opgenomen worden. Op deze pagina staat hoe de consolidatie-informatie in verschillende scenario's samengesteld moet worden:

1. Inwerkingtreding bekend bij publicatie van het besluit
2. Geen inwerkingtreding bekend bij publicatie van het besluit
3. Afhankelijke inwerkingtreding
4. Conditionele inwerkingtreding
5. Einde geldigheid
6. Wijziging tijdstempels

In hetzelfde besluit kunnen versies opgenomen zijn die voor verschillende doelen opgesteld zijn. Op elk doel is dan een van de scenario's 1 t/m 4 van toepassing. Voor tijdelijke regelingen kan een bepaling volgens scenario 5 voorkomen in combinatie met 1 t/m 4. In deze gevallen kan het besluit meerdere inwerkingtredingsbepalingen kennen, maar de informatie kan ook in één inwerkingtredingsbepaling gecombineerd worden.

## 1. Inwerkingtreding bekend bij publicatie van het besluit

Vaak is bij publicatie van het besluit bekend wanneer het besluit (of delen ervan) in werking zullen treden.  Als het hele besluit in werking treedt, moet voor elk doel waarvoor een regelingversie door het besluit ingesteld of gewijzigd wordt de tijdstempel [juridischWerkendVanaf](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) opgenomen worden in de [consolidatie-informatie](consolidatie-informatie.md). Een inwerkingtredingsbepaling kan ook voor (of per) deel van een besluit worden opgegeven. In dat geval moet elk deel alle nieuwe regelingversies betreffen voor dezelfde doelen. Dit ligt besloten in de betekenis van een doel in [STOP versiebeheer](versiebeheer_in_stop.md).

Er zijn verschillende varianten te onderkennen:

De datum van de inwerkingtreding staat genoemd in een artikel.
: Deze datum in de tekst moet voor de tijdstempel gebruikt worden. Als de datum in de tekst omschreven staat, bijvoorbeeld "de dag na bekendmaking", dan moet dat vertaald worden naar een concrete datum.

Het besluit treedt van rechtswege in werking.
: Er kunnen wettelijk voorschriften zijn die stellen dat het besluit bijvoorbeeld een bepaalde vaste periode na bekendmaking in werking treedt. Het bevoegd gezag (c.q. haar software) moet de concrete datum volgens de wettelijke voorschriften berekenen en als tijdstempel toevoegen.

Het besluit treedt in werking met terugwerkende kracht.
: In sommige gevallen staat naast de inwerkingtredingsdatum vermeld "*en werkt terug tot*" met een eerdere datum. De eerste datum is de `juridischWerkendVanaf` tijdstempel, de tweede datum moet opgenomen worden als `geldigVanaf`tijdstempel voor hetzelfde doel. Zie [soortTijdstempel](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) en het [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/10%20Terugwerkende%20kracht) van een wijziging met terugwerkende kracht.

De `geldigVanaf` tijdstempel mag alleen worden opgenomen als daadwerkelijk sprake is van terugwerkende kracht.

## 2. Geen inwerkingtreding bekend bij publicatie van het besluit

Als bij de bekendmaking van het besluit nog niet bekend is wanneer het besluit in werking zal treden, dan wordt geen tijdstempel opgenomen in de [consolidatie-informatie](consolidatie-informatie.md). Er is dan in het algemeen wel een artikel met inwerkingtredingsbepaling waarin iets staat als:

"De artikelen van dit besluit treden in werking op een bij koninklijk besluit te bepalen tijdstip."

Soms wordt daar aan toegevoegd "_... dat voor de verschillende artikelen of onderdelen daarvan verschillend kan worden vastgesteld_". Dat is juridisch correct, maar STOP ondersteunt geen manier om in de [consolidatie-informatie](consolidatie-informatie.md) aan te geven dat een arbitrair deel van de artikelen in werking treedt - voor STOP zullen alle versies voor hetzelfde `doel` tegelijk in werking treden. Bij het opstellen van het besluit moet het bevoegd gezag al een [verwachting](versiebeheer_gebruik.md) hebben over welke instellingen of wijzigingen tegelijk in werking gaan treden. Mocht bij het opstellen van het inwerkingtredingsbesluit blijken dat de verwachting niet correct was en dat slechts een deel ervan in werking kan treden, dan moet voor dat besluit het recept voor [gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md) gevolgd worden.

## 3. Afhankelijke inwerkingtreding

Als voor één doel meerdere regelingen ingesteld of gewijzigd worden en de instellingen/wijzigingen worden gespreid over meerdere besluiten, dan zal één besluit een inwerkingtredingsbepaling hebben van een van bovenstaande typen. De andere besluiten hebben een inwerkingtredingsbepaling die verwijst naar dat besluit, bijvoorbeeld:

"Dit besluit treedt in werking als artikel 1 van de Omgevingswet in werking treedt."

Voor deze bepaling hoeft in STOP geen informatie in de consolidatie-informatie opgenomen te worden; de koppeling van alle regelingversies aan hetzelfde doel leidt ertoe dat zodra een inwerkingtredingsdatum aan het doel wordt gekoppeld alle regelingversies van dat doel tegelijk voorzien worden van die datum.

## 4. Conditionele inwerkingtreding

Eerder is het scenario beschreven van een [conditionele wijziging](versiebeheer_gebruik.md), waarbij in besluit 1 al rekening gehouden wordt met een conditionele, aanvullende wijziging zodra een ander besluit 2 in werking treedt. Voor die aanvullende wijziging wordt een apart doel (in dat scenario: doel C) aangemaakt.

In besluit 1 zal bij de aanvullende wijziging in het `WijzigArtikel` iets staan als "_Indien besluit 2 in werking treedt wordt de regeling aangepast als aangegeven..._". De inwerkingtredingsbepaling voor het andere besluit 2  zal niet refereren aan besluit 1. 

De daadwerkelijke datum van inwerkingtreding van doel C is het maximum van de inwerkingtreding van besluit 1 en 2. Immers: als besluit 1 met de conditionele wijziging in werking treedt vòòr het andere besluit 2 moet doel C gelijk met besluit 2 inwerking treden. En als het andere besluit 2 voorafgaand aan besluit 1 inwerking treedt, zal doel C met de conditionele wijziging gelijk met andere wijzigingen uit besluit 1 inwerking treden. 

STOP heeft geen mogelijkheid om dit aan te geven. Het bevoegd gezag (c.q. haar software) dient ervoor te zorgen dat op het moment dat in de consolidatie-informatie een tijdstempel aan het doel van besluit 1 of aan het doel van besluit 2 wordt gekoppeld, ook bezien wordt of de tijdstempel voor doel C bepaald kan worden. Is dat het geval, dan moet de nieuwe tijdstempel voor doel C in dezelfde module meegenomen worden. Vanzelfsprekend geldt bovenstaande ook als er een tijdstempel wordt [teruggetrokken](data_xsd_Element_data_TerugtrekkingTijdstempel.dita#TerugtrekkingTijdstempel) in plaats van toegevoegd. 

Zie ook het [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Regeling%20klassiek/08%20Conditionele%20inwerkingtreding) van een conditionele inwerkingtreding.

## 5. Einde geldigheid

Er is in STOP maar één manier om de geldigheid van een regeling te beëindigen: door de regeling in te trekken. In de [consolidatie-informatie](consolidatie-informatie.md) moet een [Intrekking](data_xsd_Element_data_Intrekking.dita#Intrekking) voor de betreffende regeling opgegeven worden. De intrekking wordt gekoppeld aan een doel, waarin via een inwerkingtredingsbepaling uiteindelijk een datum gekoppeld wordt waarop de intrekking juridische werking krijgt.

Ook informatieobjecten kunnen ingetrokken worden, maar het mechanisme daarvoor is anders. Zie de documentatie [elders](io-intrekken.md).

Er zijn drie scenario's waarin intrekking een rol speelt.

Expliciete vermelding in een uitwerkingtredingsbepaling
: De tekst "_De regeling ... wordt ingetrokken_" staat in een artikel in het lichaam van het besluit.

Einddatum van een regeling bij instelling
: Het bevoegd gezag kan in het besluit dat een regeling instelt meteen een einddatum voor de geldigheid van de regeling opgeven. STOP kent geen einddatum-tijdstempel. Voor de einddatum moet een apart `doel` gemaakt worden, en voor dat doel moet de intrekking van de regeling worden vermeld in de consolidatie-informatie.

Beëindiging van een regeling van rechtswege
: Er kunnen wettelijk voorschriften zijn die stellen dat de geldigheid van de regeling bijvoorbeeld na een bepaalde vaste periode beëindigd wordt. Dat is bijvoorbeeld het geval bij een voorbereidingsbesluit. Ook in dit geval moet voor de beëindiging een apart `doel` gemaakt worden, en voor dat doel moet de intrekking van de regeling worden vermeld. Het bevoegd gezag (c.q. haar software) moet de datum volgens de wettelijke voorschriften berekenen en als [juridischWerkendVanaf](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) tijdstempel voor dat doel toevoegen aan de consolidatie-informatie.

Een intrekking beëindigt de juridische geldigheid van een regeling. Ook zonder intrekking kan het voorkomen dat een regeling niet meer van toepassing is. De regeling kan dan gemarkeerd worden als [materieel uitgewerkt](consolideren_patronen_1materieel.md); dat hoeft niet via een besluit of andere publicatie uitgewisseld te worden.

## 6. Wijziging van tijdstempels

In een zeldzaam geval komt het voor dat de inwerkingtreding via een nieuw besluit gewijzigd wordt nadat al een datum daarvoor is gepubliceerd. De wijziging wordt ook als inwerkingtredingsbepaling in het besluit opgenomen. Als er een nieuwe datum wordt genoemd, dan kan die als waarde voor de tijdstempel worden doorgegeven. De laatst aangeleverde consolidatie-informatie overschrijft eerder aangeleverde consolidatie-informatie. Als de eerder opgegeven datum geschrapt wordt moet een [TerugtrekkingTijdstempel](data_xsd_Element_data_TerugtrekkingTijdstempel.dita#TerugtrekkingTijdstempel) opgenomen worden in de [consolidatie-informatie](consolidatie-informatie.md). 

De wijziging/terugtrekking van de [juridischWerkendVanaf](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) tijdstempel wijzigt niet automatisch de waarde voor de [geldigVanaf](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel) tijdstempel; beide moeten apart gespecificeerd worden.
