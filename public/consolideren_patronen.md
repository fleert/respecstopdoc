# Consolidatiepatronen

Dit hoofdstuk beschrijft de informatie die uitgewisseld moet worden bij een publicatie of [revisie](begrippenlijst_revisie.dita#revisie) om de geautomatiseerde consolidatie te ondersteunen. 'Uitwisselen' omdat niet alleen de LVBB maar ook het bevoegd gezag moet consolideren als zij van een externe partij als een adviesbureau een nieuwe versie ontvangen. 

Onderstaande opsomming van consolidatiepatronen kijkt voornamelijk naar de technisch benodigde informatie en gaat niet in op bijvoorbeeld de soort publicatie (besluit, rectificatie, etc.) waar het patroon typisch voorkomt. In andere onderdelen van de documentatie, bijvoorbeeld bij de beschrijving van de rectificatie, wordt aangegeven welke patronen van toepassing zijn.

Naast patronen die voor een enkele publicatie van belang zijn, worden patronen beschreven die voortkomen uit de interacties tussen publicaties. Die kunnen bij elke publicatie voorkomen. Het is aan de software van het bevoegd gezag om deze situaties te herkennen en extra informatie bij de publicatie of later als revisie beschikbaar te stellen om ook in die situaties de consolidatie correct te laten verlopen.

| ![Legenda](img/versiebeheer-legenda.png) |
| --- |
| Gebruikte symbolen in dit hoofdstuk |

De consolidatiepatronen worden beschreven als het effect van een publicatie of [revisie](consproces_revisie.md) op één regeling of informatieobject (als work) voor één doel/branch. Dat moet niet gelezen worden als een beperking voor een publicatie of uitwisseling van revisies. STOP staat toe dat een besluit een instelling of wijziging van verschillende regelingen en informatieobjecten beschrijft, en zelfs verschillende wijzigingen van dezelfde regeling/informatieobject voor verschillende doelen. Voor de bekendmaking van het besluit moet dan alle informatie als beschreven in dit hoofdstuk gecombineerd worden als betrof het individuele publicaties.

De geautomatiseerde consolidatie houdt alleen rekening met publicaties en revisies waarvoor een inwerkingtreding bekend is. Alle informatie die voor de consolidatie nodig is moet bij elke publicatie of revisie meegeleverd worden, ook al is de inwerkingtredingsdatum zelf nog niet bekend. TODO: snap ik niet

## 1. Individuele publicatie of revisie
Een juridisch document dat tot een publicatie leidt, zoals een [besluit](besluit.md), [rectificatie](besluitproces_rectificatie.md) of mededeling, kan beschrijven dat een regeling of informatieobject gewijzigd wordt en wanneer de wijzigingen geldig worden. De manier waarop wijzigingen in de tekst terugkomen wordt toegelicht bij de beschrijving van de juridische documenten. Ter ondersteuning van de geautomatiseerde consolidatie moet een vertaling van de wijzigingen zoals beschreven in de juridische tekst opgenomen worden in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module. 

[1A Nieuwe versie van een regeling of informatieobject](consolideren_patronen_1versie.md)
: Als het te publiceren juridisch document aangeeft dat de regeling wijzigt en/of dat een nieuwe versie van een informatieobject in de te wijzigen regeling wordt gebruikt dan moet voor elke nieuwe versie een `BeoogdeRegeling` of `BeoogdInformatieobject` meegegeven worden. Dit patroon is ook van toepassing op een [revisie](consproces_revisie.md), die immers bedoeld is om een nieuwe versie met niet-juridische wijzigingen uit te wisselen.

[1B Inwerkingtreding van een besluit](consolideren_patronen_1iwt.md)
: In een te publiceren juridisch document kan informatie gegeven worden over de inwerkingtreding van een besluit. Dat moet vertaald worden naar de inwerkingtreding van het doel en daarmee de versie van de  regeling(en) en eventueel informatieobjecten. In het geval van een apart inwerkingtredingsbesluit kan blijken dat de versies waarover al besloten is, niet dezelfde versies zijn die in werking moeten of kunnen treden.

[1C Samenvoeging van versies](consolideren_patronen_1samenvoeging.md)
: Een bevoegd gezag kan via besluiten voor twee doelen verschillende nieuwe versies van een regeling hebben gepubliceerd zonder inwerkingtreding. Op een later moment besluit het bevoegd gezag dat beide versies toch niet apart door het verdere besluitvormingsproces lopen, en publiceert een (vervolg)besluit waarin beide in één versie zijn samengevoegd. Het bevoegd gezag kiest er dus niet voor om beide versies apart te houden maar tegelijk in werking te laten treden (dit is beschreven als patroon 2B).

[1D Materieel uitgewerkte regeling](consolideren_patronen_1materieel.md)
: Als een regeling nog wel juridisch geldig is maar eigenlijk geen toepassing meer kent, dan wordt die als materieel uitgewerkt beschouwd. Dat kan via een revisie worden uitgewisseld. Materiële uitwerking heeft effect op de vindbaarheid van de regeling.

[1E Intrekking van een regeling (of informatieobject)](consolideren_patronen_1intrekking.md)
: Als een regeling ingrijpend gewijzigd moet worden, wordt vaak afgezien van het maken van een nieuwe versie voor de bestaande regeling en wordt een nieuwe regeling opgesteld. De oude regeling wordt dan ingetrokken. Als de bestaande regeling niet meer nodig is kan die ingetrokken worden zonder dat er een nieuwe regeling voor in de plaats komt. In wet- en regelgeving kan voor een type regeling bepaald zijn dat de regeling automatisch vervalt onder bepaalde condities.

[1F Intrekking van een besluit](consolideren_patronen_1intrekkingbesluit.md)
: Een bevoegd gezag kan besluiten een eerder genomen besluit in te trekken. Als dat besluit een nieuwe regeling instelt, dan komt dit overeen met het intrekken van een regeling zoals beschreven in patroon 1E. Als het besluit een wijzigingsbesluit is, dan wordt de intrekking van een besluit gezien als een volgend wijzigingsbesluit.

[1G Vervolgpublicatie](consolideren_patronen_1vervolg.md)
: Nadat een eerste versie voor een doel is gepubliceerd kan er een volgend juridisch document zijn waarin aanpassingen op de eerdere publicatie gedaan worden (voor hetzelfde doel). Dat kan een vervolgbesluit van het bevoegd gezag zijn, maar ook een rectificatie of mededeling van een uitspraak van een beroepsorgaan. Dit wordt zoveel mogelijk opgelost via de patronen 1A t/m 1F door een nieuwe versie te formuleren of een nieuwe waarde voor een tijdstempel op te nemen. Maar er zijn situaties dat die niet voldoen, bijvoorbeeld als er een inwerkingtredingsdatum is opgegeven die eigenlijk nog onbekend had moeten zijn. Voor situaties dat er eerst voor een branch wel een versie of waarde bekend was en na correctie niet meer, kent de standaard de *terugtrekkingen*.

## 2. Interactie met andere doelen
Een publicatie of revisie kan effect hebben op inhoud en geldigheid van andere toestanden. De geautomatiseerde consolidatie kan zonder extra informatie altijd de nieuwe geldigheid bepalen van alle toestanden. Maar het is niet altijd mogelijk om te bepalen welke regelingversie of informatieobjectversie de geldende versie (inhoud) is voor een toestand. De geautomatiseerde consolidatie zal bij de toestanden aangeven als de juiste inhoud niet beschikbaar is, maar het is voorspelbaar in welke situaties dit zal optreden en het bevoegd gezag kan dit dus voorkomen. 

Het bevoegd gezag zal in deze situaties extra informatie moeten geven om de geautomatiseerde consolidatie te laten werken. Of dat tegelijk met de publicatie gedaan kan worden of in een aparte revisie hangt af van de details van de systemen (API) die de informatie uitwisselen. Het bevoegd gezag heeft een consolidatieplicht en zal daarom de extra informatie samen met of meteen na de publicatie moeten verstrekken als het gaat om de inhoud van de nu geldende toestand. Extra informatie over de inhoud van in de toekomst geldende toestanden hoeft pas enige tijd voor de start van de juridische werking van de toestand bekend te zijn. Hoe lang "enige tijd" precies is wordt niet bepaald door wetgeving maar door ketenafspraken en staat vermeld in de toepassingsprofielen.

[2A Nieuwe versie sluit niet aan bij geldige versie](consolideren_patronen_2samenloop.md)
: De publicatie leidt tot een nieuwe toestand. Volgens het versiebeheer zijn voor de inhoud (regelingversie, informatieobjectversie) van die toestand niet alle wijzigingen meegenomen die hebben geleid tot de toestand die eraan voorafgaand in werking is getreden. Ontbrekende wijzigingen uit de voorgaande toestand moet nog overgenomen worden. Als het niet mogelijk is dit te doen voorafgaand aan de vaststelling van het besluit, dan kan het ook na publicatie rechtgetrokken worden.

[2B Besluiten treden tegelijk in werking](consolideren_patronen_2gelijktijdig.md)
: Als de versie uit de publicatie of revisie tegelijkertijd in werking treedt met de versie van een een ander doel, dan moet de versie de wijzigingen van beide doelen combineren. De gecombineerde versie is dan ook de versie die voor beide doelen geldt. Als dit niet bij publicatie of uitwisseling van de revisie is aangegeven, dan kan het later met een revisie rechtgetrokken worden.

2C Nieuwe versie heeft gevolgen voor toestanden met latere inwerkingtreding
: De publicatie leidt tot een nieuwe toestand, of een vervolgpublicatie of revisie voor hetzelfde doel leidt tot een andere inhoud van een bestaande toestand. Eerdere publicaties hebben geleid tot toestanden met een latere inwerkingtreding. De wijzigingen uit de publicatie of revisie moeten ook doorgevoerd worden in de latere toestanden. De aanpak hiervoor is gelijk als voor patroon 2A.

[2D Er zijn nog toestanden na het terugtrekken van een versie of inwerkingtreding](consolideren_patronen_2ontvlechten.md)
: Als voor een doel wordt doorgegeven dat eerdere wijziging voor de regeling/informatieobject ongedaan gemaakt moeten worden of als de inwerkingtreding wordt teruggetrokken of uitgesteld, dan zal de uitkomst van de geautomatiseerde consolidatie in het algemeen zijn dat er een toestand vervalt en de voorgaande toestand langer geldig is. Maar de inhoud van de toestanden volgend op de vervallen toestand is meestal een wijziging van de vervallen toestand en wordt daarmee onbekend. Dat moet gecorrigeerd worden met een ontvlechting van de eerstvolgende of de huidig geldende toestand. De ontvlechting veroorzaakt het patroon 2D voor volgende toestanden.

[2E Er zijn nog toestanden na een intrekking](consolideren_patronen_2intrekking.md)
: Als er wijziging van een regeling/informatieobject in werking treedt nadat de regeling/informatieobject is ingetrokken, wordt dat door de geautomatiseerde consolidatie als samenloop gezien. Het bevoegd gezag moet expliciet aangeven of de intrekking of wijziging prioriteit heeft. Hetzelfde geldt *niet* voor toestanden die in werking treden nadat een regeling als materieel uitgewerkt is gemarkeerd. Deze worden wel geconsolideerd.
