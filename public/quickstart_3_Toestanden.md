# cons:Toestanden

Deze module voorziet in de invulling van de toestanden van een instrument (bijv. Regeling of GIO) en beschrijft per work wanneer welke toestand hiervan geldig en werkend is. 

| Element                                                      | Vulling                                                      | Verplicht? |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ---------- |
| [bekendOp](cons_xsd_Element_cons_bekendOp.dita#bekendOp)                                | De datum waarop eenieder kennis kon hebben van bepaalde informatie. In de meeste gevallen is dit de bekendmakingsdatum van de officiële publicatie. | J          |
| [ontvangenOp](cons_xsd_Element_cons_ontvangenOp.dita#ontvangenOp)                          | De datum waarop het STOP-gebruikend systeem kennis heeft van bepaalde informatie en die informatie ook kan en mag verstrekken. | N          |
| [BekendeToestand](DITA_OnbekendeLink.dita#Oeps)                  | Een door de LVBB berekende toestand van een regeling (dit kunnen er 1 of meer zijn) | J          |
| [BekendeToestand/FRBRExpression](cons_xsd_Element_cons_FRBRExpression.dita#FRBRExpression)    | De [FRBRExpression](quickstart_1_ExpressionIdentificatieRegeling.md) van de Toestand. | J          |
| [BekendeToestand/gerealiseerdeDoelen/doel](cons_xsd_Element_cons_doel.dita#doel)    | Het doel wordt overgenomen uit het corresponderende besluit. Een BekendeToestand kan meerdere doelen realiseren. Zie: [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md) | J          |
| [BekendeToestand/geldigheid/Gelidigheidsperiode/juridischWerkendOp](DITA_OnbekendeLink.dita#Oeps) | Deze datum(s) worden afgeleid uit de corresponderende tijdstempel uit het [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md) | J          |
| [BekendeToestand/geldigheid/Gelidigheidsperiode/geldigOp](DITA_OnbekendeLink.dita#Oeps) | Deze datum(s) worden afgeleid uit de corresponderende tijdstempel uit het [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md) | J          |
| [BekendeToestand/instrumentVersie](cons_xsd_Element_cons_instrumentVersie.dita#instrumentVersie) | De [FRBRExpression](quickstart_1_ExpressionIdentificatieRegeling.md) van de RegelingVersie zoals opgesteld door het Bevoegd gezag. | J          |



[Voorbeeld Toestand van een Regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/04-LVBB-Toestanden-Regeling-v1.xml)

[Voorbeeld Toestand van een GIO](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/04-LVBB-Toestanden-GIO-v1.xml)



## AKN IRI - Toestand van een geconsolideerde regeling

De LVBB genereert de identificatie van de geconsolideerde regeling conform de onderstaande opbouw (uitgaande van een Nederlands bevoegd gezag): 

`//FRBRWork:  "/akn/nl/act/" <subtype> "/" <datum_work> "/" <nummer> `

`//Expression: <work> "/" <taal> "@" <datum_g> [";" <datum_iwt>] [ ";" <versie> ] `

[Toelichting](consolidatie_naamgeving.md)



## JOIN IRI - Toestand van een geconsolideerd Informatieobject

De LVBB genereert de identificatie van de toestand van de geconsolideerde regeling conform de onderstaande opbouw (uitgaande van een Nederlands bevoegd gezag): 

`//FRBRWork: "/join/id/regdata/consolidatie/" <datum_work> "/" <nummer> `

`//FRBRExpression: <work> "/" <taal> "@" <datum_g> [";" <datum_iwt>] [ ";" <versie> ] `

[Toelichting](consolidatie_naamgeving.md)

