# Consolidatie-informatie

In de module [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) wordt vastgelegd hoe de regelingen en informatieobjecten in het besluit (of rectificatie of een andere publicatie of revisie) passen binnen de geldende regeling, en wanneer deze juridische werking krijgen.

De module bevat voor elke consolidatie-gerelateerde aanwijzing in de tekst van het besluit een beschrijving in termen van het [STOP versiebeheer](versiebeheer_principes.md).

## Algemene informatie
- [gemaaktOp](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) is de datum/tijd waarop de consolidatie-informatie is samengesteld. Het is dezelfde datum die ook voor de [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) wordt gebruikt. Waar de momentopname één doel kent, worden de doelen in de consolidatie-informatie bij de vertaling van de consolidatie-aanwijzingen vermeld.

## Versiebeheer-informatie voor regelingen en informatieobjecten
De consolidatie-informatie module bestaat uit drie secties die aangeven of en hoe een regeling of informatieobject onderdeel is van de wijzigingen voor een doel:

| Onderdeel | Bevat |
| --------- | ----- |
| [BeoogdeRegelgeving](data_xsd_Element_data_BeoogdeRegelgeving.dita#BeoogdeRegelgeving) | Geeft aan dat een nieuwe versie voor een regeling of informatieobject is gespecificeerd. |
| [Intrekkingen](data_xsd_Element_data_Intrekkingen.dita#Intrekkingen) | Geeft aan dat er regelingen en/of informatieobjecten worden ingetrokken, dus ophouden geldig te zijn. |
| [Terugtrekkingen](data_xsd_Element_data_Terugtrekkingen.dita#Terugtrekkingen) | Geeft aan dat er regelingen en/of informatieobjecten zijn waarvoor eerder een nieuwe versie is opgegeven voor een doel, maar waarvoor nu blijkt dat ze niet wijzigen voor dat doel. |

De inhoud van de drie secties is vergelijkbaar:

* [bekendOp](data_xsd_Element_data_bekendOp.dita#bekendOp) bevat de datum waarop de inhoud van de publicatie oorspronkelijk publiek geworden is. Dit wordt alleen ingevuld als de publicatie een mededeling is van eerder gepubliceerde informatie, zoals de uitspraak van een beroepsorgaan of een niet via de LVBB gepubliceerde noodverordening.

* [doelen](data_xsd_Element_data_doelen.dita#doelen) bevat het doel of de doelen (bij gelijktijdige in werking treding) waar de wijziging betrekking op heeft.

* [instrumentVersie](data_xsd_Element_data_instrumentVersie.dita#instrumentVersie): de beoogde regelingversie of informatie-objectversie die moet ontstaan door deze publicatie (of revisie). Leidt de wijziging tot een aanpassing van een historische versie waarvoor geen consolidatieplicht bestaat, dan wordt het [instrument](data_xsd_Element_data_instrument.dita#instrument) opgenomen in plaats van de `instrumentVersie`. Ook bij Intrekkingen en Terugtrekkingen kan alleen een `instrument` opgegeven worden.

* [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan): Behalve als het gaat om de eerste versie van een regeling of informatieobject moet [gemaaktOpBasisVan](data_xsd_Element_data_gemaaktOpBasisVan.dita#gemaaktOpBasisVan) opgenomen worden, met dezelfde functie als het gelijknamige element in een [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname):

  - De [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) verwijst naar de vorig succesvol uitgewisselde consolidatie-informatie voor de regeling/informatieobject. Als het informatie over tegelijk in werking tredende doelen gaat kunnen er meerdere basisversies zijn.

  - De [VervlochtenVersie](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) en [OntvlochtenVersie](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie) zijn optioneel en zullen van de betrokken [consolidatiepatronen](consolideren_patronen.md) afhangen.


* [eId](data_xsd_Element_data_eId.dita#eId) De `eId` moet verwijzen naar de plaats waar de consolidatie-gerelateerde aanwijzing in de tekst staat:![eId wijst naar...](img/ConsolidatieInformatie-eId.png)

    - Voor een regeling is dat een tekst in het lichaam van de publicatie waarin staat dat de regeling (niet meer) gewijzigd of ingetrokken wordt. Voor een besluit is dat dus een `WijzigArtikel` of `Artikel` en niet de `WijzigBijlage`.


    - Voor een informatieobject is dat de vermelding of verwijdering van de expression-id in de tekst van de publicatie (door middel van [tekst:ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef)). Als de tekst onderdeel uitmaakt van de tekst van (de wijziging van) de regelingtekst zoals die in de publicatie is opgenomen, dan moet de [tekstcomponent](identificatie_tekstonderdelen.md) meegenomen worden in de verwijzing. Als een terugtrekking of intrekking van een informatieobject niet expliciet in de tekst staat maar het gevolg is van de terugtrekking van een regeling of een mutatie door vervanging van de regeltekst, dan wordt de eId-verwijzing achterwege gelaten.


## Informatie over tijdstempels
Voor de tijdstempels (inwerkingtreding, terugwerkend tot) gelden altijd de laatst gepubliceerde waarden. De tijdstempel wordt daarom alleen aan het doel gekoppeld waar het betrekking op heeft. Er zijn drie mogelijkheden:

- Als voor een nieuw doel nog nooit een tijdstempel doorgegeven is en de datum is nog niet bekend, dan wordt er niets over de tijdstempel in de consolidatie-informatie doorgegeven.
- Een [Tijdstempel](data_xsd_Element_data_Tijdstempel.dita#Tijdstempel) wordt gebruikt om een nieuwe waarde voor de tijdstempel door te geven. Als de datum volgt uit wettelijke voorschriften (bijvoorbeeld de dag na bekendmaking van het besluit) en niet expliciet vermeld is, dan moet de aanleverende software deze datum bepalen en opnemen in de consolidatie-informatie.
- Een [TerugtrekkingTijdstempel](data_xsd_Element_data_TerugtrekkingTijdstempel.dita#TerugtrekkingTijdstempel) wordt gebruikt om aan te geven dat er geen warde meer bekend is voor de tijdstempel.

Ook hierbij moet aangegeven worden waar (in het lichaam van de publicatie) de datum te vinden is. Als de datum volgt uit wettelijke voorschriften, dan moet naar het lichaam van de publicatie verwezen worden.

## Materieel uitgewerkt
Als een regeling of informatieobject juridisch nog wel geldig is, maar er geen situaties meer zijn waarop de regeling/informatieobject van toepassing is, dan is de regeling/informatieobject materieel uitgewerkt. Het [instellen](data_xsd_Element_data_MaterieelUitgewerkt.dita#MaterieelUitgewerkt) of [terugtrekken](data_xsd_Element_data_TerugtrekkingMaterieelUitgewerkt.dita#TerugtrekkingMaterieelUitgewerkt) van een datum materieel uitgewerkt is niet gekoppeld aan een doel; de laatst uitgewisselde aanwijzing is van toepassing. Zie ook het [voorbeeld Beperking vindbaarheid](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/04%20Omgaan%20met%20geconsolideerde%20regeling/07%20Beperking%20vindbaarheid) over het toevoegen van een datum materieel uitgewerkt.