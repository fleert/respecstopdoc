# data:RegelingMetadata en data:RegelingVersieMetadata

Deze modules voorzien in de metadata bij een RegelingVersie. De metadata in RegelingMetadata zijn doorgaans persistent over verschillende RegelingVersies heen, met uitzondering van `versienummer` dat in RegelingVersieMetadata is opgenomen.

**data:RegelingMetadata**

| Element                                                      | Vulling                                                      | Verplicht?           |
| ------------------------------------------------------------ | ------------------------------------------------------------ | -------------------- |
| [afkortingen](data_xsd_Element_data_afkortingen.dita#afkortingen)                          | Afkortingen waarmee de regeling kan worden aangeduid         | N                    |
| [alternatieveTitels](data_xsd_Element_data_alternatieveTitels.dita#alternatieveTitels)            | Een alternatieve titel voor het instrument (hier Regeling), die niet officieel is vastgesteld en die in het gangbaar spraakgebruik gebruikt wordt om het instrument (hier Regeling) aan te duiden | N                    |
| [heeftCiteertitelInformatie](data_xsd_Element_data_heeftCiteertitelInformatie.dita#heeftCiteertitelInformatie) | Container                                                    | N                    |
| [heeftCiteertitelInformatie/CiteertitelInformatie/citeertitel](data_xsd_Element_data_citeertitel.dita#citeertitel) | De titel van het instrument (hier Regeling) die gebruikt wordt in aanhalingen. | J (binnen container) |
| [heeftCiteertitelInformatie/CiteertitelInformatie/isOfficieel](data_xsd_Element_data_isOfficieel.dita#isOfficieel) | Boolean (true/false). Indien er een 'citeertitel' is, dan dient 'isOfficieel' ook gevuld te zijn | J (binnen container) |
| [eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke)      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | J                    |
| [grondslagen](data_xsd_Element_data_grondslagen.dita#grondslagen)                          | Machineleesbare verwijzingen naar een juridische bron die de wettelijke grondslag van de regeling geeft | N                    |
| [maker](data_xsd_Element_data_maker.dita#maker)                                      | Zie waardelijsten overheid: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | N                    |
| [officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel)                    | De officiële titel van de regeling                           | J                    |
| [onderwerpen](data_xsd_Element_data_onderwerpen.dita#onderwerpen)                          | Zie waardelijst [onderwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/onderwerp.xml) | J                    |
| [overheidsdomeinen](data_xsd_Element_data_overheidsdomeinen.dita#overheidsdomeinen)              | Zie waardelijst [overheidsthema.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/overheidsthema.xml) | J                    |
| [rechtsgebieden](data_xsd_Element_data_rechtsgebieden.dita#rechtsgebieden)                    | Zie waardelijst [rechtsgebied.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/rechtsgebied.xml) | N                    |
| [soortRegeling](data_xsd_Element_data_soortRegeling.dita#soortRegeling)                      | Zie waardelijst [soortregeling.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/soortregeling.xml) | J                    |
| [soortBestuursorgaan](data_xsd_Element_data_soortBestuursorgaan.dita#soortBestuursorgaan)          | Zie waardelijst [bestuursorgaan.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/bestuursorgaan.xml)                  | N                    |
| [voorkeursafkorting](data_xsd_Element_data_voorkeursafkorting.dita#voorkeursafkorting)            | De voorkeursafkorting waarmee de regeling kan worden  aangeduid. Hier kan er maar 1 van zijn | N                    |



**data:RegelingVersieMetadata**

| Element                               | Vulling                                                      | Verplicht? |
| ------------------------------------- | ------------------------------------------------------------ | ---------- |
| [versienummer](data_xsd_Element_data_versienummer.dita#versienummer) | Het versienummer van de regeling. NB deze  dient gelijk te zijn aan het versienummer in de [FRBRExpression](quickstart_1_ExpressionIdentificatieRegeling.md) van de Regeling | J          |



## Voorbeeld

[Voorbeeld RegelingMetadata](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/01-BG-Regeling-v1-Metadata.xml)

[Voorbeeld RegelingVersieMetadata](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/01-BG-Regeling-v1-versieMetadata.xml)

