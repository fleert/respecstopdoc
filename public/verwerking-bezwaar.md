# Verwerken van bezwaar tegen een STOP-besluit

Tegen een aantal besluiten die worden bekendgemaakt met behulp van STOP kan [bezwaar](bezwaar.md) worden ingesteld door belanghebbenden.   

**N.B.**: bij inwerkingtreding van de Omgevingswet is bezwaar  alleen mogelijk bij de vergunning voor buitenplanse omgevingsplanactiviteit (afwijkvergunning).   

Veruit de meeste besluiten die worden bekendgemaakt met STOP vallen onder de [uniforme openbare voorbereidingsprocedure](uniforme-voorbereiding-besluit.md) van afdeling 3.4 Algemene wet bestuursrecht. Dat betekent dat er verplicht een ontwerp moet worden gepubliceerd waartegen zienswijzen kunnen worden ingediend. Zie hiervoor [Uniforme openbare voorbereidingsprocedure in STOP](uniforme-voorbereiding-besluit-stop.md). Dit onderdeel gaat alleen over het afhandelen van bezwaar.
Het bestuursorgaan dat het besluit heeft vastgesteld is verantwoordelijk voor het nemen van een beslissing op de bezwaren en het verwerken daarvan in het besluit. Als er bezwaren gegrond worden verklaard, moet voor zover nodig het bestreden besluit worden herroepen en moet voor zover nodig in de plaats daarvan een nieuw besluit worden genomen. Voor besluiten die met behulp van STOP zijn bekendgemaakt kan dit betekenen:  

1. Dat het bevoegd gezag een wijzigingsbesluit neemt, en dus de eerder vastgestelde regeling aanpast door middel van mutaties conform STOP.
2. Dat er een geheel nieuwe versie van de regeling wordt vastgesteld in plaats van de oude versie, waartegen bezwaar was gemaakt. Dit kan technisch door middel van het mutatiescenario 'intrekken en vervangen' of door het laten vervallen van de oude regeling (deze krijgt dan een einddatum mee, waarna deze vervalt) en het publiceren van een nieuwe regeling. Het verdient bij gebruik van deze scenario's wel de aanbeveling om in het bijbehorende besluit in STOP en/of in de toelichting bij het nieuwe besluit duidelijk aan te geven wat er precies gewijzigd is ten opzichte van oude versie. Dit moet voor belanghebbenden afdoende duidelijk zijn, zodat ook duidelijk is waar zij beroep tegen kunnen instellen (en waartegen niet). Is dit onvoldoende duidelijk aangegeven, dan ontstaat het risico dat tegen de hele nieuwe regeling beroep kan worden ingesteld. 

