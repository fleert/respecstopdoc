# Informatieobjecten bij het besluit

## Informatieobjecten als onderdeel van het besluit
Net als de tekst van een regeling wordt nieuwe of gewijzigde inhoud van een informatieobject formeel vastgesteld als onderdeel van het besluit. In het algemeen:

* De nieuwe of gewijzigde informatieobjectversie die bij de voorbereiding van het besluit is opgesteld wordt als bijlage bij het besluit gevoegd en samen met het besluit bekendgemaakt.
* In de [metadata van het besluit](besluit_metadata.md) staat de expression-identificatie van de informatieobjectversie vermeld.
* Van elk juridisch relevant bestand van het informatieobject wordt een [hash](data_xsd_Element_data_hash.dita#hash) berekend van de versie die ter vaststelling is aangeboden; die hash wordt doorgegeven als onderdeel van de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata). 

De Bekendmakingswet regelt dat een nieuwe versie van een informatieobject als wijziging wordt gezien en alleen tegen de wijziging beroep openstaat, mits er een manier is om twee separate wijzigingen (en dus twee informatieobjectversies) samen te voegen tot één (derde) geconsolideerde regelingversies. Dat is het geval voor een [GIO](gio-intro.md). In andere gevallen betekent het meeleveren van een complete informatieobjectversie dat de versie als geheel wordt vastgesteld en dus als geheel voor beroep vatbaar is.

Het berekenen van een hash is een controlemechanisme om na de bekendmaking van het besluit te kunnen verifiëren dat de gepubliceerde versie van het informatieobject identiek is aan de versie die als bijlage bij de bekendmaking is te downloaden.

## Vaststellingscontext
Er zijn informatieobjecten die gegevens bevatten die niet direct door een mens begrepen kunnen worden. Bijvoorbeeld een GIO: niemand kan door de coördinaten van de geometrische begrenzingen te lezen vaststellen of de begrenzing correct is. Dat is wel mogelijk door de gegevens in een context (voor een GIO: samen met achtergrondkaarten) te tonen.

Dit soort informatieobjecten wordt getransformeerd bij het besluit gevoegd. Het informatieobject dat onderdeel is van het besluit bevat naast het oorspronkelijke informatieobject aanvullende informatie die (machine-leesbaar) de vaststellingscontext beschrijft. De vaststellingscontext kan gebruikt worden om het informatieobject op een later moment op dezelfde manier te tonen als bij de vaststelling van het besluit. De hash wordt over het getransformeerde informatieobject berekend. Het oorspronkelijke informatieobject is door extractie uit het getransformeerde informatieobject te verkrijgen.

![Informatieobject bij besluit](img/Besluit-IO.png)

STOP en de LVBB bieden geen faciliteiten om extra informatie uit te wisselen die nodig is voor het reconstrueren van de context (zoals bij de GIO: de achtergrondkaarten voor het tijdstip van vaststelling van het besluit). Het bevoegd gezag is zelf verantwoordelijk voor de archivering van deze extra informatie. De archivering kan overgelaten worden aan een landelijke voorziening, mits die in staat is de extra gegevens onveranderlijk gedurende langere tijd (als voorgeschreven door de archiefwet) te reproduceren. Zie ook de [juridische context](vaststellen-wijzigen-gio.md).

Zie ook:
* [Vaststellingscontext voor een GIO](gio-vaststellingscontext.md)
