# A2. Rectificatie RegelingKlassiek

Dit betreft een besluit dat de eerste versie van een regeling instelt. Als het gepubliceerde besluit niet de juiste regelingtekst blijkt te bevatten, dan moet de publicatie gecorrigeerd worden met een rectificatie. 

- In de tekst van de regeling wordt renvooi gebruikt om de correcties aan te geven;
- Tussen de renvooi wordt "vrije tekst" gebruikt om aan te geven dat het een rectificatie betreft en waarvan het een rectificatie is;



## Gecorrigeerde regeling

Het BG begint met het opstellen van de gecorrigeerde regeling of regelingen als het besluit meerdere regelingen betrof. Per STOP-module van de regeling wordt hieronder aangegeven of, en zo ja, wat er aangepast moet worden:

- [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): Een rectificatie leidt tot een nieuwe versie van de regeling en dus een nieuwe identificatiemodule met een aangepaste FRBRExpression.
- [RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata): Indien er correcties nodig zijn in bijv. de officiele titel, citeertitel etc., moet gecorrigeerde RegelingMetadata aangeleverd worden. Omdat de RegelingMetadata een annotatie is, hoeft de rectificatie geen nieuwe RegelingMetadata te bevatten als er hier geen correcties zijn. 
- [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata): Het versienummer van de gecorrigeerde regeling moet altijd meegegeven worden met de rectificatie.

- Regelingtekst: Door middel van [renvooi](bepalen_wijzigingen_renvooi.md) worden correcties in de regelingtekst aangeleverd. De was-versie is de met het besluit meegeleverde versie. De wordt-versie is deze gecorrigeerde versie. Een correcte versie van het deel van het besluit dat de regelingtekst bevat kan met [Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang), [VoegToe](tekst_xsd_Element_tekst_VoegToe.dita#VoegToe) of [Verwijder](tekst_xsd_Element_tekst_Verwijder.dita#Verwijder) samengesteld worden. 

- [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties): Indien er correcties nodig zijn in de Toelichtingsrelaties moet een gecorrigeerde Toelichtingsrelatie-module aangeleverd worden. Omdat de Toelichtingsrelaties annotaties zijn, hoeft de rectificatie geen nieuwe Toelichtingsrelatie-module te bevatten als er geen correcties zijn.



## Gecorrigeerde besluitmodules

De correctie(s) hebben gevolgen voor de met het besluit meegeleverde informatie. Om het besluit te corrigeren is nodig:

* [ProcedureverloopMutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie): Als de rectificatie alleen een correctie in de regeling betreft, bevat de rectificatie geen aanpassingen voor het procedureverloop.

* [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie): De rectificatie voegt een nieuwe regelingversie toe aan het [doel](doel.md) van het besluit. De consolidatieinformatie moet dus worden uitgebreid. De consolidatieInformatie moet een  [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) bevatten met 
  * hetzelfde doel als het besluit, 
  * de instrumentVersie van de gecorrigeerde regeling,

  * een eId verwijzing naar de voegtoe/vervang/verwijder in de besluitmutatie die betrekking heeft uit de tekst van de rectifcatie. 

* [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata): Als de rectificatie alleen correcties in de regeling betreffen, bevat de rectificatie geen nieuwe besluitmetadata. Ook de BesluitMetadata is een annotatie en de informatie van de besluitmetadata van voorgaande versies blijft van toepassing zolang er geen nieuwe versie wordt aangeleverd. 



## De rectificatie zelf

De rectificatie zelf bestaat uit:

* [Identificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie): De identificatiemodule van de rectificatie zelf. De rectificatie van een besluit is een zelfstandig Work en elke rectificatie bij hetzelfde besluit is een nieuwe expressie van het rectificatie Work;

* [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata): Net als bij een besluit moet er bij een rectificatie altijd metadata aangeleverd worden. De RectificatieMetadata lijkt erg op de BesluitMetadata, maar heeft een extra veld [rectificeert](data_xsd_Element_data_rectificeert.dita#rectificeert);

* [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie): De rectificatie bestaat uit:
  * Een [regeling opschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift), bijv. 'Rectificatie van de Verordening Toeristenbelasting 2017'

  * Het [lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) met één of meerdere:
    * [Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)(en) met 
      * een [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop);
      * een of meer toelichtende [alinea](tekst_xsd_Element_tekst_Al.dita#Al)'s;
      * een [Besluitmutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie).
      
        De besluitmutatie lijkt erg op een regelingmutatie, maar wijzigt de besluittekst. De *was* van de mutatie is het originele besluit. De *wordt* is het besluit wat *zou* ontstaan na verwerking van de wijzigingen uit de rectificatie. *Zou* omdat de besluittekst zelf niet geconsolideerd wordt. LET OP: er worden voor de rectificatie wel afwijkende standaard zinnen gebruikt om de [*wat*](tekst_xsd_Element_tekst_Wat.dita#Wat) aan te duiden.

Zie voor een voorbeeld de [STOP-codering](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/A2-RegelingKlassiek.xml) van de _'Rectificatie van het wijzigingsbesluit voor het Besluit nevenvestigings- en nevenzittingsplaatsen en enkele andere besluiten in verband met een betere benutting van de zittingscapaciteit'_ uit de [voorbeelden uit de staande praktijk](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/index.md).