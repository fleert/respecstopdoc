# Standaard tekstmodel

## Tekstmodel
STOP kent een standaard tekstmodel voor een besluit dat gebruikt kan worden om elk type regeling in te stellen of te wijzigen. Alleen voor het instellen en wijzigen van een [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) moet het [klassiek tekstmodel](besluit-klassiek.md) gebruikt worden. 

Het standaard tekstmodel, [BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact), heeft als structuur:

![Structuur van een `BesluitCompact`](img/BesluitCompact.png)

[RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift) en [Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef)
: De titel en de grondslagen van het besluit.

[Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) of dictum
: Hierin staat wat het bevoegd gezag instelt of wijzigt. In het dictum wordt de regeling of wijziging zelf niet beschreven. Er mag ook geen overgangsrecht in beschreven worden of condities waaronder de regeling of wijziging geldig is. Het lichaam bestaat uitsluitend uit:
    * Voor elke in te stellen regeling of wijziging van een regeling: een [WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel). Hierin staat aangegeven dat de regeling ingesteld of gewijzigd wordt. In de tekst moet ook een verwijzing ([IntRef](tekst_xsd_Element_tekst_IntRef.dita#IntRef)) naar de bijbehorende `WijzigBijlage` met (de wijziging van) de regelingtekst staan.
    * Alle andere onderwerpen die het bevoegd vaststelt of in het besluit wil opnemen moeten in een [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel) staan. Bijvoorbeeld een [inwerkingtredingsbepaling](besluit_iwtbepalingen.md).

[Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)
: De tekst ter afsluiting van het besluit, waar bijvoorbeeld de ondertekening is opgenomen.

[WijzigBijlage](tekst_xsd_Element_tekst_WijzigBijlage.dita#WijzigBijlage)
: De regeling die ingesteld wordt of de beschrijving van de wijziging van de regeling wordt in een `WijzigBijlage` geplaatst. Voor elke instelling of wijziging van een regeling is er één `WijzigBijlage` waarnaar verwezen wordt vanuit het bijbehorende `WijzigArtikel` (en vanuit andere plaatsen als dat door de schrijver van de tekst relevant gevonden wordt).

[Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)
: Een bijlage bij het besluit dat als STOP tekst is opgemaakt. Optioneel, mag meerdere keren voorkomen.

[Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) of [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering)
: Een toelichting op het hoe en wat van het besluit; zie de [beschrijving](besluit_toelichting.md).

## Opnemen regelingtekst

De tekst van (een wijziging van) een regeling moet opgenomen worden in een `WijzigBijlage`.

Eerste versie van een nieuwe regeling
: De eerste versie van een regeling wordt integraal overgenomen in het besluit. Dit geldt voor alle tekstmodellen: [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact), [RegelingVrijetekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) en [RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel). Voor een [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) moet het [klassieke model](besluit-klassiek.md) gebruikt worden.

Wijziging van een regeling in [renvooi](renvooitekst.md)
: Een wijziging van een regeling moet als renvooi opgenomen worden. In STOP wordt een tekst in renvooi als een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) gecodeerd.

Wijziging via vervanging van de regelingtekst
: Alleen voor bijzondere toepassingen kent STOP een mechanisme om een regeling te wijzigen door een volledige nieuwe versie van een regeling in het besluit op te nemen. Dit mechanisme mag alleen gebruikt worden als het expliciet wordt toegestaan in een [toepassingsprofiel](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD). In alle andere gevallen moet een wijziging in renvooi opgenomen worden. De integrale tekst van de gewijzigde regeling wordt dan in een [VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling) binnen [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) geplaatst. Dit mechanisme voldoet niet automatisch aan de juridische eis dat het besluit alleen de wijzigingen betreft. In het lichaam/dictum van het besluit moet daarom (in een [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel)) beschreven worden wat precies de wijziging is die in de regelingtekst is verwerkt.

## Implementatiekeuze

Bij het presenteren van het besluit moet er rekening mee gehouden worden (zeker bij een `RegelingCompact`) dat dezelfde type tekstelementen (zoals `RegelingOpschrift` en `Artikel`) zowel in de tekst van het besluit voorkomen als binnen een `WijzigBijlage`. Voor een goed begrip van de tekst van het besluit kan het noodzakelijk zijn dat de presentatie van de regelingtekst in een `WijzigBijlage` afwijkt van de weergave van de regelingtekst als zelfstandige tekst.

STOP schrijft niet voor hoe een duidelijke presentatie bereikt moet worden.
