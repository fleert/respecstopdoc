# Soort tekstelement: Inhoud

De tekst binnen structuurelementen wordt verdeeld in tekstblokken die door middel van inhoudelementen worden gecodeerd. In STOP zijn dit bijvoorbeeld `Artikel`, `Lid` of `Divisietekst`. In inhoudelementen is de inhoud van de regeling vastgelegd, of een toelichting daarop. Omdat een tekst waarin veel inhoudelementen elkaar opvolgen lastig te lezen is, kan tussen inhoudselementen een [`Tussenkop`](tekst_xsd_Element_tekst_Tussenkop.dita#Tussenkop) geplaatst worden. Het tussenkopje is geen onderdeel van de hiërarchische structuur van de tekst, en sluit qua opmaak (zoals grootte van het lettertype) aan bij de opmaak van het tekstblok. 

Inhoudelementen bevatten zelf alleen [bloktekstelementen](regeltekst_bloktekst.md), niet de [broodtekst](regeltekst_bloktekst.md) zelf. 

Uitzonderingen:

* Artikel kan fungeren als [structuurelement](regeltekst_structuur.md) indien een artikel leden bevat.
* Een Artikel/Lid kan, net als een structuurelement, een [aantekeningelement](regeltekst_aantekening.md) bevatten. 



## Inhoudelementen

[Afkondiging](tekst_xsd_Element_tekst_Afkondiging.dita#Afkondiging), [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel), [Considerans](tekst_xsd_Element_tekst_Considerans.dita#Considerans), [Dagtekening](tekst_xsd_Element_tekst_Dagtekening.dita#Dagtekening), [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst), [Inhoud](tekst_xsd_Element_tekst_Inhoud.dita#Inhoud), [Instructie](tekst_xsd_Element_tekst_Instructie.dita#Instructie), [InwerkingtredingArtikel](tekst_xsd_Element_tekst_InwerkingtredingArtikel.dita#InwerkingtredingArtikel), [Lid](tekst_xsd_Element_tekst_Lid.dita#Lid), [Ondertekening](tekst_xsd_Element_tekst_Ondertekening.dita#Ondertekening), [Slotformulering](tekst_xsd_Element_tekst_Slotformulering.dita#Slotformulering), [WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel), [WijzigInstructies](tekst_xsd_Element_tekst_WijzigInstructies.dita#WijzigInstructies), [WijzigLid](tekst_xsd_Element_tekst_WijzigLid.dita#WijzigLid)