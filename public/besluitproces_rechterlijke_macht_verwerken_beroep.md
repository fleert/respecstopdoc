# Verwerken beroep ingesteld

Aan het einde van de beroepstermijn (na publicatie en consolidatie van het besluit) stelt het BG vast dat er binnen de beroepstermijn beroep is ingesteld. Het BG moet dan via een [directe mutatie](directe_mutatie.md) een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) doorgeven waarbij de instelling van het beroep wordt toegevoegd als [`Procedurestap`](data_xsd_Element_data_Procedurestap.dita#Procedurestap):
* [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) is hierbij altijd  `/join/id/stop/procedure/stap_018` (Beroep(en) ingesteld).
* [`voltooidOp`](data_xsd_Element_data_voltooidOp.dita#voltooidOp) bevat de datum waarop de beroepstermijn afloopt.

De `Procedureverloopmutatie` bevat daarnaast een `bekendOp`: de datum waarop het eerste beroep is aangetekend, of - indien onbekend - de datum van de mutatie, waarbij de mutatiedatum voor de datum einde beroepstermijn moet liggen. Zie ook [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).

 
## Voorbeeld procedureverloopmutatie instelling beroep

```
<Procedureverloopmutatie>
  <bekendOp>2020-02-14</bekendOp>
  <voegStappenToe>
    <Procedurestap>
      <!-- Beroep(en) ingesteld  -->
      <soortStap>/join/id/stop/procedure/stap_018</soortStap>
      <voltooidOp>2020-02-14</voltooidOp>
    </Procedurestap>
  </voegStappenToe>
</Procedureverloopmutatie>
```  

## Zie ook

* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).