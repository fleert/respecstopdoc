# Rectificatie XML-voorbeelden

Op deze pagina worden voorbeelden van de [STOP-modellering van rectificaties](besluitproces_rectificatie_model.md) in meer detail beschreven. 

## De rectificatie

De rectificatie bestaat uit een of meer rectificatieteksten. Eén [rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst) bevat één of enkele Alinea's met een Kop waarin de rectificatie beschreven/toegelicht wordt en optioneel een [BesluitMutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie) òf een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie).

### BesluitMutatie

Voorbeeld van een Rectificatietekst met een BesluitMutatie:

```xml
    <Rectificatietekst eId="content_o_1"
                        wId="gm9999_1__content_o_1">
      <Kop>
        <Opschrift>Correctie aanhef</Opschrift>
      </Kop>
      <Al>In de bekendmaking van het Besluit tot het instellen van het Vuurwerkverbod, is in de
        Aanhef niet de juiste datum en een onjuist nummer van het voorstel genoemd waarop het
        besluit gebaseerd is.</Al>
      <BesluitMutatie componentnaam="rectifcatiebesluit"
                       was="/akn/nl/bill/gm9999/2020/B2020-00013/nld@2020-01-19;1"
                       wordt="/akn/nl/bill/gm9999/2020/B2020-00013/nld@2020-01-18;1a">
        <Vervang wat="formula_1">
          <Wat>De tekst van de Aanhef wordt op de aangegeven wijze gecorrigeerd:</Wat>
          <Aanhef eId="formula_1"
                   wId="formula_1">
            <Al>De raad van de gemeente Pyrodam</Al>
            <Al>gelezen het voorstel van burgemeester en wethouders van <VerwijderdeTekst>2 januari
              2020, nr. 2020-01</VerwijderdeTekst><NieuweTekst>6 januari 2020, nr.
              2020-03</NieuweTekst>;</Al>
            <Al>Gelet op <ExtRef ref="https://wetten.overheid.nl/jci1.3:c:BWBR0005416&amp;titeldeel=III&amp;hoofdstuk=IX&amp;artikel=149"
                                  soort="URL">Gemeentewet, art. 149</ExtRef>.</Al>
            <Al>besluit:</Al>
          </Aanhef>
        </Vervang>
      </BesluitMutatie>
    </Rectificatietekst>
```

De [BesluitMutatie](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie) bevat de wijzigingen ten opzichte van de originele besluittekst in renvooi. 

### RegelingMutatie

Voorbeeld van een Rectificatietekst met een RegelingMutatie:

```xml
    
    <Rectificatietekst eId="content_o_4"
                        wId="gm9999_1__content_o_4">
      <Kop>
        <Opschrift>Correctie bijlage</Opschrift>
      </Kop>
      <Al>Bovendien is in "Bijlage A Vaststelling Vuurwerkverordening" niet de juiste versie van het
        vuurwerkverbodsgebied opgenomen.</Al>
      <RegelingMutatie componentnaam="rectificatieregeling"
                        was="/akn/nl/act/gm9999/2020/REG0001/nld@2020-01-19;1"
                        wordt="/akn/nl/act/gm9999/2020/REG0001/nld@2020-01-20;1a">
        <!-- RENVOOI ten opzichte van de resulterende regeling van het te rectificeren besluit -->
        <Vervang wat="gm9999_1__cmp_A__content_o_4__list_1">
          <Nummer>A</Nummer>
          <Wat>De tekst van Bijlage I wordt als volgt gecorrigeerd</Wat>
          <Bijlage eId="cmp_A"
                    wId="gm9999_1__cmp_A">
            <Kop>
              <Label>Bijlage</Label>
              <Nummer>I</Nummer>
              <Opschrift>Overzicht Informatieobjecten</Opschrift>
            </Kop>
            <Divisietekst eId="cmp_A__content_o_4"
                           wId="gm9999_1__cmp_A__content_o_4">
              <Inhoud>
                <Begrippenlijst eId="cmp_A__content_o_4__list_1"
                                 wId="gm9999_1__cmp_A__content_o_4__list_1">
                  <Begrip eId="cmp_A__content_o_4__list_1__item_1"
                           wId="gm9999_1__cmp_A__content_o_4__list_1__item_1">
                    <Term>vuurwerkverbodsgebied</Term>
                    <Definitie>
                      <Al wijzigactie="verwijder"><ExtIoRef eId="cmp_A__content_o_4__list_1__item_1__ref_o_1_inst2"
                                                             ref="/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1"
                                                             wId="gm9999_1a__cmp_A__content_o_4__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1</ExtIoRef></Al>
                      <Al wijzigactie="voegtoe"><ExtIoRef eId="cmp_A__content_o_4__list_1__item_1__ref_o_1"
                                                           ref="/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1a"
                                                           wId="gm9999_1__cmp_A__content_o_4__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1a</ExtIoRef></Al>
                    </Definitie>
                  </Begrip>
                </Begrippenlijst>
              </Inhoud>
            </Divisietekst>
          </Bijlage>
        </Vervang>
      </RegelingMutatie>
    </Rectificatietekst>
  </Lichaam>
</Rectificatie>
```

De [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) bevat correcties op de regelingmutaties uit het besluit. Deze worden opgenomen als een nieuwe RegelingMutatie in Renvooi met als was-versie de door de publicatie van het besluit ontstane geconsolideerde regeling-versie. 

Een rectificatie bevat één of meerdere rectificatieteksten. Eén rectificatietekst bevat òf een besluitMutatie òf een RegelingMutatie. In de Regelingmutatie [moet](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0074) de @wordt uniek zijn, dus alle wijzigingen in één regelingversie worden in één regelingmutatie ondergebracht.

## Pyrodam

Zie voor verdere toelichting de beschrijving van het "Pyrodam" [rectificatie-voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03%20Rectificatie%20besluit/index.md).

## Staande praktijk

Om te tonen hoe rectificaties uit de staande praktijk in STOP aangeleverd zouden moeten worden, zijn als voorbeeld enkele rectificaties in een aantal [rectificatie-scenario's](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/03a%20Rectificaties%20staande%20praktijk/index.md) uitgewerkt. 

