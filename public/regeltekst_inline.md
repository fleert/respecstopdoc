# Soort tekstelement: Inline

Dit zijn elementen die gebruikt kunnen worden binnen de [broodtekst](regeltekst_bloktekst.md). Niet alle inline-elementen kunnen in elke tekst gebruikt worden. Afhankelijk van de context van een tekst zijn er beperkingen. Op basis hiervan onderscheiden we vier soorten tekst:

1. **Platte tekst**: tekst waar geen inline-elementen zijn toegestaan. Bijvoorbeeld binnen `sup` en `sub`.
2. **Koptekst**: tekst waarin gebruik van `i`, `sub`, `sup` en `Noot` mogelijk is. Gebruikt in [Koppen](regeltekst_koppen.md)-elementen.
3. **Broodtekst zonder verwijzingen**: idem aan koptekst zonder `Noot`,  maar met `b`, `strong`, `br`, `Nootref`, `InlinetekstAfbeelding`. Wordt gebruikt voor tekst binnen een verwijzing. 
4. **Broodtekst**: idem aan broodtekst-zonder-verwijzingen plus  `abbr`, `u`, `ExtRef`, `IntRef`, `ExtIoRef`, `IntIoRef`, `Noot`, `Contact`.

De inline-elementen zijn op te splitsen in: 

- Tekstweergave
- Noten
- Bijzondere symbolen
- Links en hints

Volg de link naar de schemadocumentatie van elk element voor nadere toelichting op het gebruik van betreffend element.
De weergaven op deze pagina betreffen voorbeelden. Afgezien van de beperkingen die volgen uit de juridisch vereiste [tekstweergave](regeltekst_weergave.md) is een viewer vrij in het kiezen van een toepasselijke weergave. Hoe een viewer de tekst binnen een inline exact weer moet geven wordt *niet gespecificeerd* door STOP, voor de beeldvorming worden op deze pagina uitsluitend *voorbeeld* weergaven getoond.

## Tekstweergave {#inline_weergave}

Inline-elementen die de weergave van de tekst beïnvloeden:

***Letterhoogte elementen***

| element                | voorbeeld           | voorbeeld weergave |
| ---------------------- | ------------------- | ------------------ |
| [`sub`](tekst_xsd_Element_tekst_sub.dita#sub) | `tek<sub>st</sub>`  | tek<sub>st</sub>   |
| [`sup`](tekst_xsd_Element_tekst_sup.dita#sup) | `tek<sup>stk</sup>` | tek<sup>st</sup>   |

Letterhoogte elementen bevatten zelf platte tekst, waardoor ze niet genest kunnen worden.

***Nadruk*** 

| element                         | voorbeeld                     | voorbeeld weergave          |
| ------------------------------- | ----------------------------- | --------------------------- |
| [`b`](tekst_xsd_Element_tekst_b.dita#b)              | `<b>broodtekst</b>`           | <b>broodtekst</b>           |
| [`i`](tekst_xsd_Element_tekst_i.dita#i)              | `<i>broodtekst</i>`           | <i>broodtekst</i>           |
| [`u`](tekst_xsd_Element_tekst_u.dita#u)              | `<u>broodtekst</u>`           | <u>broodtekst</u>           |
| [`strong`](tekst_xsd_Element_tekst_strong.dita#strong) *) | `<strong>broodtekst/<strong>` | <strong>broodtekst</strong> |

*) `<strong>` is een semantisch nadrukselement, de weergave ervan ligt niet vast; in de praktijk wordt `<strong>` meestal vet weergegeven. 
Nadrukelementen zijn verrijkingen die geen deel uitmaken van de juridische tekst.

Beperkingen:

- Nadruk kan niet recursief gebruikt worden, &lt;x&gt; binnen een &lt;x&gt; heeft geen effect. Ook recursief gebruik van `<strong>` wordt binnen STOP ontraden. Combinaties zijn wel mogelijk: <b><u><i>tekst</i></u></b>
- De tekst binnen een nadruk element is broodtekst en gebruik van andere inline elementen binnen een nadruk element is dus mogelijk. 

***Afbreekelement***

| element                     | voorbeeld           | voorbeeld weergave  |
| --------------------------- | ------------------- | ------------------- |
| <p>[`br`](tekst_xsd_Element_tekst_br.dita#br)</p> | <p>`tek<br/>st`</p> | <p>tek</p><p>st</p> |

Met `<br>` wordt een regelovergang afgedwongen. Het achter elkaar plaatsen van meer dan 1 `br`-element om extra witruimte tussen tekstblokken te creëren is niet toegestaan.



## Noten {#Noten}

Een noot is een opmerking die bij de tekst geplaatst wordt.

| element                                                      | voorbeeld                                                    | weergave                                                     |
| ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| <p>[`Noot`](tekst_xsd_Element_tekst_Noot.dita#Noot)</p> met <p> [`NootNummer`](tekst_xsd_Element_tekst_NootNummer.dita#NootNummer)</p> | <p>`Dit is een tekst met een noot <Noot type="voet" id="N1"><NootNummer>1</NootNummer><Al>Een informatieve toevoeging voor leken en professionals</Al></Noot>.`</p> | <p>Dit is een tekst met een noot<sup>1</sup>.  </p><p><sup>1 </sup>Een informatieve toevoeging voor leken  en professionals.</p> |
| <p>[`Nootref`](tekst_xsd_Element_tekst_Nootref.dita#Nootref)</p>                        | <p>`Een nootref<NootRef refid="N1"/> is een extra verwijzing naar een bestaande noot. `</p> | <p>Een nootref<sup>1</sup> is een extra verwijzing naar een bestaande noot.</p> |

Beperkingen:

- De tekst van de noot mag zelf geen noten bevatten (geen recursief gebruik). 
- Een nootref mag alleen gebruikt worden binnen een tabel waar ook de noot met hetzelfde `refid` voorkomt.
- In STOP worden alleen noten van het `type` "voet" en "tabel" (weergave direct onder de tabel) gebruikt. 



## Bijzondere symbolen {#InlineTekstAfbeelding}

De standaard biedt geen mogelijkheid om een lettertype (font) te selecteren voor een tekst. Ook stelt de standaard geen eisen aan de minimum tekenset die een lettertype moet ondersteunen: STOP laat het gebruik van alle tekens toe waarvoor een UTF-code bestaat. Als een symbool (of een combinatie van symbolen, denk aan een complexe formule in de lopende tekst) niet beschikbaar is als letterteken, dan kan het als [`InlineTekstAfbeelding`](tekst_xsd_Element_tekst_InlineTekstAfbeelding.dita#InlineTekstAfbeelding) gegeven worden. Omdat de InlineTekstAfbeelding een vaste hoogte heeft en het aantal dpi van de officiële publicatie PDF sterk verschilt met het aantal dpi van een webpagina, zal een `InlineTekstAfbeelding` in verschillende media variëren. Bij 300 dpi wordt geadviseerd om een hoogte van 35 (of minder) toe te passen zodat in de PDF het symbool zonder verstoring van het tekstbeeld getoond wordt. Zie ook [Afbeelding in de tekst](regeltekst_afbeelding.md) voor nadere toelichting.
Gebruik wordt ontmoedigd; gebruik indien mogelijk een UTF-8 karakter.

*XML voorbeeld*

```xml
<Al> De som van de geluidsniveaus als gevolg van wederzijds
 incoherente bronnen wordt aangeduid met het teken 
 <InlineTekstAfbeelding naam="stcrt-2020-64380-239.png"
                         hoogte="35"
                         breedte="35"
                         formaat="image/png"
                         dpi="300"/>
 in overeenstemming met de volgende definitie:</Al>
```

*Voorbeeld PDF weergave:*

> ![figuren](img/InlineTekstAfbeelding.png)

*Voorbeeld web weergave:*

> ![](img/InlineTekstAfbeelding-web.png)



## Links en hints {#Linkhints}

| element                  | voorbeeld                                                    | weergave                                                     |
| ------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [`abbr`](tekst_xsd_Element_tekst_abbr.dita#abbr) | `Met <abbr title="Abbreviation">abbr</abbr> kan de uitgeschreven tekst toegevoegd worden aan een afkorting.` | Met abbr kan de uitgeschreven tekst toegevoegd worden aan een afkorting. |

Afkorting met toelichting (hint), die gebruikt kan worden om een afkortingenlijst te genereren of bij weergave bijvoorbeeld als [mouse-over](https://nl.wikipedia.org/wiki/Mouseover) tekst getoond kan worden. In het `abbr` element zelf is alleen platte tekst mogelijk. 

| element                          | voorbeeld / weergave                                         |
| -------------------------------- | ------------------------------------------------------------ |
| [`Contact`](tekst_xsd_Element_tekst_Contact.dita#Contact)   | `Reacties uitsluitend per <tekst:Contact` `adres="info@gemeente.nl" soort="e-mail"` `>e-mail</tekst:Contact>.` |
|                                  | Reacties uitsluitend per [e-mail](https://localhost/mailto:info@gemeente.nl).  |
| [`ExtIoRef`](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) | `<ExtIoRef soort="JOIN"` `eId="cmp_III__table_o_1__ref_o_5"` `ref="/join/id/regdata/mn002/2019/or_kustfundament/nld@2019-11-01"`  `wId="mn002__1-0__cmp_III__table_o_1__ref_o_5"` `>/join/id/regdata/mn002/2019/or_kustfundament/nld@2019-11-01</ExtIoRef>` |
|                                  | [/join/id/regdata/mn002/2019/or_kustfundament/nld@2019-11-01](https://zoek.officielebekendmakingen.nl/dc-2019-110/1/html) |
| [`ExtRef`](tekst_xsd_Element_tekst_ExtRef.dita#ExtRef)     | `consumentenvuurwerk in de zin van het <ExtRef ref="jci1.3:c:BWBR0013360" soort="JCI">Vuurwerkbesluit</ExtRef>` |
|                                  | consumentenvuurwerk in de zin van het [Vuurwerkbesluit](https://wetten.overheid.nl/jci1.3:c:BWBR0013360&bijlage=1&z=2021-01-02&g=2021-01-02) |
| [`IntIoRef`](tekst_xsd_Element_tekst_IntIoRef.dita#IntIoRef) | `<IntIoRef ref="mn002__1-0__cmp_III__table_o_1__ref_o_5"` `eId="chp_2__subchp_2.2__subsec_2.2.4__art_2.7__ref_o_1"` `wId="mn002__1-0__chp_2__subchp_2.2__subsec_2.2.4__art_2.7__ref_o_1">kustfundament</IntIoRef>` |
|                                  | [kustfundament](https://zoek.officielebekendmakingen.nl/stcrt-2019-56288.html#d17e23557) |
| [`IntRef`](tekst_xsd_Element_tekst_IntRef.dita#IntRef)     | `in <IntRef ref="art_2" scope="Artikel">artikel 2</IntRef> genoemde publicatiebladen` |
|                                  | in [artikel 2](https://wetten.overheid.nl/BWBR0004287/2021-07-01#Artikel2) genoemde publicatiebladen |


Links en hints zijn verrijkingen die geen deel uitmaken van de juridisch tekst; het is dus niet verplicht ze weer te geven en ze kunnen zonder renvooi worden aangebracht en gewijzigd. De juridisch authentieke versie van elk besluit is in pdf formaat en bevat nooit links ([voorbeeld](https://zoek.officielebekendmakingen.nl/stb-2016-156.pdf)).

De elementen `Contact`, `ExtIoRef` en `IntIoRef` bevatten zelf alleen platte tekst. De elementen `ExtRef` en `IntRef` kunnen broodtekst zonder verwijzingen bevatten, waardoor het nesten van links voorkomen wordt.

Beperkingen: 

- In Links wordt het gebruik van `br` afgeraden. 