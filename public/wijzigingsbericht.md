# Wijzigingsbericht {.rss_feed}

Elke wijziging van STOP en bijbehorende documentatie en van het bijbehorende IMOP wordt gemeld via een bericht op de RSS feed. Er zijn verschillende mogelijkheden dit bericht te ontvangen:

* Een RSS feed lezer. Dat kan een gespecialiseeerde RSS lezer zijn, maar het is ook mogelijk een RSS feed te laten volgen via een extensie van een webbrowser of applicaties als Outlook. Een webbrowser die RSS feeds ondersteunt kan de STOP-RSS-feed al ontdekt hebben door een van de webpagina's te analyseren.

* Omzetten naar bijvoorbeeld een e-mail. Er zijn diverse diensten, zoals IFTTT, Zapier, Microsoft Flow, die een RSS feed kunnen monitoren. Als er een nieuw bericht op de RSS feed verschijnt kunnen ze dat bericht omzetten naar een e-mail of een andere vorm van notificatie.

Voor al deze diensten is het adres van de RSS Feed noodzakelijk: @@@DOCUMENTATIE_URL@@@rss.xml