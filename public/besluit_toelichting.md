# Toelichting en motivering


Zowel de toelichting als de motivering bevatten informatie over het hoe en waarom van de instelling en/of wijziging van regelingen in het besluit. Deze informatie speelt alleen een rol in het besluitvormingsproces dat eindigt met de bekendmaking van het besluit, en wordt niet getoond bij de geconsolideerde regeling of proefversie die volgt uit het besluit. STOP kent ook een toelichting op een regeling. Die kan gebruikt worden bij een regelingversie en is [elders](regeling_toelichting.md) beschreven.

## Standaard model
Het [standaard model](besluit_compact.md) kent zowel een toelichting als een motivering die om verschillende redenen aan het besluit toegevoegd worden.

![Modellering in STOP](img/Besluit_Toelichting_Compact.png)

De [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) heeft het karakter van een voorstel aan het bevoegd gezag om het besluit in deze vorm vast te stellen. De toelichting wordt bijvoorbeeld opgesteld door het dagelijks bestuur van gemeente, provincie of waterschap. Het geeft een onderbouwing voor het voorgestelde besluit dat zij aan het algemeen bestuur voorleggen. De toelichting is dus vooral bedoeld als extra informatie in het besluitvormingsproces.

De [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering) is de inhoudelijke onderbouwing van het besluit, waarmee het bevoegd gezag aan eenieder duidelijk maakt waarom het besluit in deze vorm is vastgesteld. Het bevoegd gezag is wettelijk verplicht een inhoudelijke onderbouwing te geven van het besluit als beschreven in [Awb afdeling 3.7](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.7), maar bij uitzondering is het toegestaan geen motivering op te nemen.

Het is aan de opsteller van het besluit om in samenspraak met het bevoegd gezag te bepalen of een motivering en/of toelichting noodzakelijk zijn. Omdat de toelichting bedoeld is voor gebruik in het besluitvormingsproces kan er ook voor gekozen worden de toeliching wel op te nemen in het besluit dat aan het bevoegd gezag ter vaststelling wordt aangeboden, maar niet in het bekend te maken besluit. Als de toelichting wel onderdeel is van het bekend te maken besluit, dan wordt het meegenomen in de publicatie.

## Klassiek model { #klassiek }
Bij het [klassiek model](besluit-klassiek.md) wordt de motivering traditioneel in de vorm van een toelichting opgenomen. De toelichting kent een van drie vormen:

![Modellering in STOP](img/Besluit_Toelichting_Klassiek.png)

* Als de toelichting alleen uit een algemene toelichting bestaat, dan wordt het element [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) gebruikt. Het element [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) dient alleen als container voor de algemene toelichting en eventuele bijlagen en is niet zichtbaar in de tekst.

* Als de toelichting alleen uit een toelichting bestaat die in detail ingaat op de formulering van (de wijziging van) regelingen of informatieobjecten, dan wordt het element [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) gebruikt.  Het element [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) dient alleen als container voor de artikelgewijze toelichting en eventuele bijlagen en is niet zichtbaar in de tekst.

* Als de toelichting uit zowel een algemene als gedetailleerde toelichting bestaat, dan worden beide elementen [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) en [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) gebruikt. Het element [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) dient als container voor deze elementen en eventuele bijlagen, en is zichtbaar als een [structuurelement](regeltekst.md) met een eigen kop. De elementen [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) en [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) staan als structuurelement een hiërarchisch niveau dieper.

## Toelichtingsrelaties

Als een besluit een [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) bevat of als in de [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering) een gedetailleerde beschrijving is opgenomen van de onderdelen van het besluit, dan kan als [annotatie](annotaties.md) bij het besluit de module [Toelichtingsrelaties](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties) toegevoegd worden.

![Toelichtingrelaties voor een besluit](img/Besluit_Toelichtingrelaties.png)

De elementen van de module leggen een relatie tussen de tekst van de artikelgewijze toelichting/motivering en wat er in het besluit vastgesteld wordt. Dat kan [regeltekst](regeltekst.md) zijn in een eerste versie van een regeling, of een wijziging ervan in renvooitekst. De relatie kan bijvoorbeeld gebruikt worden om de regeltekst(wijziging) of informatieobject en de toelichting erop naast elkaar weer te geven. Zie de [toelichting op een regeling](regeling_toelichting.md) voor een gedetailleerde beschrijving van de werking van de [Toelichtingsrelaties](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties).


## Implementatiekeuze

Het [schema](tekst_xsd_Main_schema_tekst_xsd.dita#tekst.xsd) voor de XML codering van toelichting en motivering kent om historische redenen meerdere manieren om de toelichting en motivering in XML op te nemen. In dit hoofdstuk is de voorkeurs-codering gebruikt die zeker in de volgende versies van de standaard ondersteund zal worden. De landelijke voorzieningen (LVBB, DSO-LV) ondersteunen ook de andere coderingen, maar in een toekomstige versie van de voorzieningen kan die ondersteuning beëindigd worden. 

Het gebruik van de informatie uit de _Toelichtingsrelaties_ module door een systeem dat deze informatie ontvangt is niet verplicht. Zie de documentatie van het betreffende systeen voor informatie over of en hoe de module gebruikt wordt.

