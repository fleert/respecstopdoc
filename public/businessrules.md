# Bedrijfsregels

In deze sectie van de documentatie zijn alle bedrijfsregels bijeengebracht geordend op 

- [meldingscode](businessrules_codes.dita) en 
- per [STOP module](businessrules_modules.dita).

De standaard stelt uiteenlopende randvoorwaarden waaraan de informatie moet voldoen die is opgenomen in een van de STOP-modules. Een deel van de randvoorwaarden is vastgelegd in de [schema's](schema_documentatie.md) van de implementatie van de standaard. De bedrijfsregels beschrijven de overige randvoorwaarden die aan de informatie gesteld worden. Het gaat bijvoorbeeld om bedrijfsregels die een vertaling zijn van invulinstructies, of die de consistentie borgen tussen verschillende elementen van de informatie. De bedrijfsregels zijn van toepassing voor elk systeem dat gebruik maakt van STOP.

Bedrijfsregels zijn van toepassing op één of meer STOP modules en worden aangeduid met een nummer, bijv. STOP1020. Een bedrijfsregel heeft een beschrijving, een ernst (fout of waarschuwing) en indien de bedrijfsregel binnen STOP gevalideerd kan worden, een melding. Daarnaast bevat de bedrijfsregel een verwijzing naar de schemadocumentatie van relevante elementen.

Een deel van de bedrijfsregels is geïmplementeerd als [schematron](https://www.schematron.com). Deze zijn te vinden bij de STOP-schema's op gitlab (de [.sch bestanden](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema/stop)). De overige bedrijfsregels (zie [validatiematrix](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD)) zijn niet op een dergelijke manier uit te drukken en zijn òf door het [Bronhouderkoppelvlak](@@@BHKV_URL@@@) geïmplementeerd, òf worden (in de toekomst) door de LVBB geïmplementeerd.

De STOP-bedrijfsregels zijn ook beschikbaar als [xml-file](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aservice/bedrijfsregels).

