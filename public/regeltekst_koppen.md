# Soort tekstelement: Koppen

De 'Koppen'-tekstelementen hebben een afwijkende opmaak waarmee de structurering van een tekst zichtbaar wordt. Kopelementen worden gebruikt door [structuur](regeltekst_structuur.md)elementen en [bloktekst](regeltekst_bloktekst.md)elementen. Binnen structuurelementen is de opmaak afhankelijk van het 'moeder'-element: een `label`, `nummer`, `opschrift` of `subtitel` van een hoofdstuk zal groter en vetter weergegeven worden dan die van een subparagraaf.

De koppen van de bloktekst-elementen table([`title`](tekst_xsd_Element_tekst_title.dita#title)) en figuur([Titel](tekst_xsd_Element_tekst_Figuur.dita#Figuur_Titel)) kennen een eigen opmaak.

Een [`Tussenkop`](tekst_xsd_Element_tekst_Tussenkop.dita#Tussenkop) is geen onderdeel van de hiërarchische structuur van de tekst, en sluit qua opmaak (zoals grootte van het lettertype) aan bij de opmaak van de [broodtekst](regeltekst_bloktekst.md). 

## Koppen-elementen

[Kop](tekst_xsd_Element_tekst_Kop.dita#Kop), [Opschrift](tekst_xsd_Element_tekst_Opschrift.dita#Opschrift), [Subtitel](tekst_xsd_Element_tekst_Subtitel.dita#Subtitel), [Nummer](tekst_xsd_Element_tekst_Nummer.dita#Nummer), [Label](tekst_xsd_Element_tekst_Label.dita#Label), [Tussenkop](tekst_xsd_Element_tekst_Tussenkop.dita#Tussenkop), [title](tekst_xsd_Element_tekst_title.dita#title), [Figuur/Titel](tekst_xsd_Element_tekst_Figuur.dita#Figuur_Titel)

