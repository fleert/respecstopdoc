# Publicatieproces

TODO: nog uitwerken.

STOP kent modellen voor de inhoud van een publicatie. Die inhoud kan als zodanig uitgewisseld worden in het creatieproces. Als de inhoud aangeboden wordt ter publicatie in het publicatieblad moet er informatie aan toegevoegd worden die specifiek is voor het publicatieproces. Dit betreft:

Metadata voor de publicatie in het blad
: Algemene publicatie metadata. Daarbij de opmerking dat die alleen voor gebruik bij generieke publicatie-tekstmodellen nodig is, want in de andere gevallen is het in de metadata van de inhoud-modellen verwerkt.

Attenderen op een publicatie
: Vast onderdeel van het publicatieproces is dat geïnteresseerden geínformeerd worden dat er een publicatie verschenen is. Om het attenderen goed te kunnen uitvoeren is [extra informatie](attenderen.md) nodig.

