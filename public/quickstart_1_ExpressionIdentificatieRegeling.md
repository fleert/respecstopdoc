# data:ExpressionIdentificatie (Regeling)

Deze module voorziet in de identificatie van de Regeling en volgt hierin de [Akoma Ntoso](https://docs.oasis-open.org/legaldocml/akn-nc/v1.0/os/akn-nc-v1.0-os.html) naamgevingsconventie (AKN NC). 

|                                           | Vulling                                                      | Verplicht? |
| ----------------------------------------- | ------------------------------------------------------------ | ---------- |
| [FRBRWork](data_xsd_Element_data_FRBRWork.dita#FRBRWork)             | Zie 'AKN IRI - Regeling'                                     | J          |
| [FRBRExpression](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression) | Zie 'AKN IRI - Regeling'                                     | J          |
| [soortWork](data_xsd_Element_data_soortWork.dita#soortWork)           | Voor een Regeling dient deze de waarde `'/join/id/stop/work_019'` te hebben. | J          |



## AKN IRI - Regeling

Voor de identificatie van de RegelingVersie dienen de AKN IRIs als volgt te zijn opgebouwd (uitgaande van een Nederlands bevoegd gezag):

`//FRBRWork: "/akn/nl/act/" <overheid> "/" <datum_work> "/" <overig> `

`//FRBRExpression: <FRBRWork> "/" <taal> "@" [ <datum_expr> ";" ] <versie> [ ";" <overig> ]`

[Toelichting](regeling_naamgeving.md)

## Voorbeeld

[Voorbeeld ExpressionIdentificatie - Regeling](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/01-BG-Regeling-v1-Identificatie.xml)

