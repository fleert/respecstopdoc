# Nieuwe versie sluit niet aan bij geldige versie

## Patroon
Een publicatie leidt tot een nieuwe toestand, maar volgens het [versiebeheer](versiebeheer_in_stop.md) zijn voor de inhoud (regelingversie, informatieobjectversie) van die toestand niet alle wijzigingen meegenomen uit voorgaande toestanden: besluiten hebben geen rekening met elkaar gehouden.

Dit patroon wordt ook wel samenloop genoemd: er zijn twee besluiten die elk de inhoud van een toestand bepalen, er zijn regeling/informatieobject versies die het resultaat van elk van die besluiten weergeven maar er is geen versie die de correcte inhoud van de toestand weergeeft omdat in het resultaat niet het effect van beide besluiten is opgenomen.

Dit patroon is alleen van toepassing als de inhoud van het besluit dat aanleiding geeft voor de nieuwe toestand niet meer aangepast kan worden. Als dat wel zo is, dan kunnen de wijzigingen die nog niet in het besluit verwerkt zijn, nog voor de publicatie ervan overgenomen worden waardoor dit patroon niet voorkomt. 

## Uit te wisselen informatie

In essentie is de oplossing de uitwisseling van een nieuwe versie van de regeling/informatieobject voor het inwerkingtredingsdoel van de nieuwe toestand, waarbij de wijzigingen uit de eerder geldende toestand zijn overgenomen via [vervlechting](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie). De vorm waarin dat moet gebeuren hangt af van de variant.

## Varianten
De varianten ontstaan doordat eerst nagegaan moet worden of het samenvoegen van de versies uit de twee besluiten wel juridisch in de haak is. Als de twee besluiten juridisch niet verenigbaar met elkaar zijn, dan moet het bevoegd gezag een nieuw besluit nemen om de samengevoegde versie vast te stellen. Dan zijn er drie varianten te onderscheiden:

1. Juridisch onafhankelijke besluiten: de juiste versie kan gemaakt worden door de wijzigingen uit beide besluiten te combineren;
2. Juridisch niet-verenigbare besluiten, korte besluitperiode: de wijzigingen spreken elkaar tegen, maar een oplossing is snel gevonden;
3. Juridisch niet-verenigbare besluiten, uitgebreid besluittraject: de wijzigingen spreken elkaar tegen en een oplossing is complex.

![Oplossen samenloop](img/consolideren_patronen_2samenloop.png)

In de illustratie is het inwerkingtredingsdoel van de nieuwe toestand B en van de eerdere toestand A. Het oplossen van de samenloop moet tijdig gebeuren, zodat de samenloop is opgelost zodra de datum van inwerkingtreding van doel B is aangebroken, of zoveel eerder als volgens implementatieafspraken vereist is.

### Juridisch onafhankelijke besluiten
Er is geen nieuw besluit nodig om te komen tot een samengevoegde versie. Er wordt een [revisie](consproces_revisie.md) voor doel B uitgewisseld. De revisie bevat de nieuwe versie van de regeling/informatieobject waarin naast de wijzigingen voor doel/branch B nu ook de wijzigingen uit doel/branch A zijn overgenomen.

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van de revisie staat elke nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat doel B.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch B.
* In [VervlochtenVersie](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch A.

### Juridisch niet-verenigbare besluiten, korte besluitperiode
Er is een nieuw besluit nodig om te komen tot een samengevoegde versie. Het bevoegd gezag is in staat dat besluit tijdig te nemen, en er is geen bezwaar of beroep mogelijk. Het nieuwe besluit bevat wijzigingen voor doel B waarmee de wijzigingen uit doel/branch A zijn overgenomen.

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het nieuwe besluit staat elke nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat doel B.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch B.
* In [VervlochtenVersie](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch A.

### Juridisch niet-verenigbare besluiten, uitgebreid besluittraject
Er is een nieuw besluit nodig om te komen tot een samengevoegde versie. Het bevoegd gezag is niet in staat dat besluit tijdig te nemen en/of er is bezwaar of beroep mogelijk. Het nieuwe besluit moet in STOP als een regulier wijzigingsbesluit gezien worden, voor een nieuw doel (in de illustratie C) omdat niet op voorhand duidelijk is of en wanneer het besluit in werking treedt, terwijl de besluiten voor doelen A en B al wel in werking kunnen zijn. De intentie zal zijn dat het nieuwe besluit tegelijk met het besluit voor doel B in werking treedt.

Het nieuwe besluit bevat wijzigingen voor doel C en beschrijft de nieuwe versie van de regeling(en)/informatieobject(en) waarin de wijzigingen uit doel/branch A en B zijn verwerkt. In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het nieuwe besluit staat elke nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staan doel B en C.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch B.
* In [VervlochtenVersie](data_xsd_Element_data_VervlochtenVersie.dita#VervlochtenVersie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch A.

