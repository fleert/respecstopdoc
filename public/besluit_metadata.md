# Metadata van een besluit

De [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) bevat deels dezelfde kenmerken als de metadata van een regeling; deze kenmerken zijn [elders](regeling_metadata.md) beschreven. De kenmerken in de metadata van het besluit zullen deels afgeleid zijn van de regeling(en) die door het besluit ingesteld of gewijzigd worden. Kenmerken als [soortBestuursorgaan](data_xsd_Element_data_soortBestuursorgaan.dita#soortBestuursorgaan) zijn wel specifiek voor het besluit: het bestuursorgaan kan een ander zijn dan degene die verantwoordelijk is voor de regelingen.

Er zijn twee kenmerken die wel in de metadata van het besluit, maar niet in de metadata van de regeling voorkomen.

[soortProcedure](data_xsd_Element_data_soortProcedure.dita#soortProcedure)
: Typering van het besluit. Geeft aan welke procedure van toepassing is op het besluit en bepaalt daarmee welke stappen kunnen voorkomen in het [Procedureverloop](data_xsd_Element_data_Procedureverloop.dita#Procedureverloop). Zie de beschrijving van het [besluitvormingsproces](inspraak_en_vaststelling.md) voor de ondersteunde proceduresoorten.

[informatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs)
: De expression identificaties van alle informatieobjecten die onderdeel zijn van het besluit. Dit zijn zowel de te consolideren [informatieobjecten](besluit_informatieobject.md) die via het besluit worden vastgesteld, als de [PDF documenten](IO_Document.md) ter onderbouwing van het besluit.