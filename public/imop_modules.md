# STOP-modules in IMOP

Dit overzicht beschrijft de XML-elementen die corresponderen met een STOP-module.

STOP-modules kunnen onderscheiden worden naar [type](EA_1A45BBE5EC4F4bb2BBD7F5E701CE0479.dita#Pkg):

Instrumentversie-informatie
: Bevat informatie voor een specifieke versie van een instrument. De informatie in deze module is onveranderlijk: voor een gegeven instrumentversie zal de inhoud van een module steeds hetzelfde zijn. Als deze informatie wijzigt dan ontstaat er een nieuwe versie van het instrument. 

Annotatie
: Een annotatie bevat een interpretatie of duiding van de (juridische) inhoud van een instrument die voor software te begrijpen is. De annotatie bevat dus geen nieuwe informatie, immers de informatie in een annotatie is af te leiden door de instrument-informatie te bestuderen. De informatie is alleen op een andere manier gemodelleerd, zodat software er mee kan werken. Een annotatie kan voor meer dan één versie van een instrument van toepassing zijn.

Contextinformatie
: Contextinformatie representeert informatie die gaat over een instrument of instrumentversie, maar die ontstaat of beïnvloed wordt door gebeurtenissen die de instrument(versie) niet zelf wijzigen.

## Modules voor een regeling

| XML-element | Type module | Inhoud |
|---|---| --- |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van een regelingversie |
| [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata) | Instrumentversie-informatie | Versie-specifieke metadata van een regeling |
| [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact)<p>[RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek)</p><p>[RegelingVrijeTekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst)</p><p>[RegelingTijdelijkdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel)</p> | Instrumentversie-informatie | De tekst van (een versie van) de regeling |
| [RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata) | Annotatie | Metadata van een regeling |
| [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelatie.dita#Toelichtingsrelatie) | Annotatie | Relatie tussen een tekst in de (artikelsgewijze) toelichting bij de regeling en de artikelen/informatieobjecten waar die betrekking op heeft |
| [Begripsrelaties](data_xsd_Element_data_Begripsrelaties.dita#Begripsrelaties) | Annotatie | Relatie tussen een begrip in de regeling en een begrip in een externe catalogus. |
| Voor de geconsolideerde regeling bovendien: |||
| [ConsolidatieIdentificatie](cons_xsd_Element_cons_ConsolidatieIdentificatie.dita#ConsolidatieIdentificatie) | Instrument-informatie | De identificatie van een geconsolideerde regeling |
| [GerechtelijkeProcedureStatus](cons_xsd_Element_cons_GerechtelijkeProcedureStatus.dita#GerechtelijkeProcedureStatus) | Contextinformatie | Informatie over de status van de regeling naar aanleiding van nog openstaande mogelijkheden tot beroep of van lopende gerechtelijke procedures |
| [JuridischeVerantwoording](cons_xsd_Element_cons_JuridischeVerantwoording.dita#JuridischeVerantwoording) | Contextinformatie | Informatie over de juridische bronnen die gebruikt zijn om de geconsolideerde regeling samen te stellen |
| [ActueleToestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) | Contextinformatie | Informatie over de juridische geldigheid van een regeling |
| [CompleteToestanden](cons_xsd_Element_cons_CompleteToestanden.dita#CompleteToestanden) | Contextinformatie | Informatie over de juridische geldigheid van een regeling |

## Modules voor een informatieobject

| XML-element | Type module | Inhoud |
|---|---| --- |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van een versie van het informatieobject |
| [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) | Instrumentversie-informatie | Versie-specifieke metadata van een informatieobject |
| [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata) | Annotatie | Metadata van een informatieobject |
| Specifiek voor een geo-informatieobject: |||
| [GeoInformatieObjectVaststelling](geo_xsd_Element_geo_GeoInformatieObjectVaststelling.dita#GeoInformatieObjectVaststelling) | Instrumentversie informatie | De geo-informatie van (een versie van) het informatieobject inclusief informatie over de vaststelling ervan, zoals dat samen met het besluit gepubliceerd wordt. |
| [GeoInformatieObjectVersie](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie) | Instrumentversie-informatie | De geo-informatie van (een versie van) het informatieobject exclusief informatie over de vaststelling ervan, zoals dat gebruikt wordt voor (geconsolideerde) informatieobjecten buiten de context van een besluit. |
| [FeatureTypeStyle](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle) | Annotatie | De symbolisatie die gebruikt moet worden om het geo-informatieobject te tonen.|
| [JuridischeBorgingVan](gio_xsd_Element_gio_JuridischeBorgingVan.dita#JuridischeBorgingVan) | Annotatie | Optionele module om de relatie vast te leggen tussen de geometrieën (en evt. aanvullende informatie zoals een norm) in de GIO en de bron-objecten waar de geometrieën van zijn afgeleid. Maakt *geen* deel uit van de consolidatie. |
| Voor een geconsolideerd informatieobject bovendien: |||
| [ConsolidatieIdentificatie](cons_xsd_Element_cons_ConsolidatieIdentificatie.dita#ConsolidatieIdentificatie) | Instrument-informatie | De identificatie van een geconsolideerd informatieobject |
| [GerechtelijkeProcedureStatus](cons_xsd_Element_cons_GerechtelijkeProcedureStatus.dita#GerechtelijkeProcedureStatus) | Contextinformatie | Informatie over de status van het informatieobject naar aanleiding van nog openstaande mogelijkheden tot beroep of van lopende gerechtelijke procedures |
| [JuridischeVerantwoording](cons_xsd_Element_cons_JuridischeVerantwoording.dita#JuridischeVerantwoording) | Contextinformatie | Informatie over de juridische bronnen die gebruikt zijn om het geconsolideerd informatieobject samen te stellen |
| [ActueleToestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden) | Contextinformatie | Informatie over de juridische geldigheid van een informatieobject |
| [CompleteToestanden](cons_xsd_Element_cons_CompleteToestanden.dita#CompleteToestanden) | Contextinformatie | Informatie over de juridische geldigheid van een informatieobject |



## Modules voor een besluit

| XML-element | Type module | Inhoud |
|---|---| --- |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie informatie | De identificatie van een besluitversie |
| [BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact)<p>[BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek)</p> | Instrumentversie informatie | De tekst van (een versie van) het besluit |
| [BesluitMetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) | Annotatie | Metadata van een besluit |
| [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) | Annotatie | Aanwijzing hoe de (wijzigingen) in regelgeving geconsolideerd moeten worden, hoe de (juridische) geldigheid wordt. |
| [Proefversies](cons_xsd_Element_cons_Proefversies.dita#Proefversies) | Annotatie | Een overzicht van de regeling- en informatieobjectversies die resulteren als de wijzigingen uit het besluit doorgevoerd worden. |
| [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelatie.dita#Toelichtingsrelatie) | Annotatie | Relatie tussen een tekst in de (artikelsgewijze) toelichting bij het besluit en de artikelen/informatieobjecten waar die betrekking op heeft |
| [Procedureverloop](data_xsd_Element_data_Procedureverloop.dita#Procedureverloop) | Context informatie | Verloop van het proces rondom het besluit, van vaststelling tot onherroepelijk worden |
| [Gebiedsmarkering](geo_xsd_Element_geo_Gebiedsmarkering.dita#Gebiedsmarkering) | Annotatie | Ten behoeve van de attendering op de bekendmaking/publicatie van het besluit: een aanduiding van het gebied waar het besluit betrekking op heeft. |
| [Effectgebied](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied) | Annotatie | Ten behoeve van de attendering op de bekendmaking/publicatie van het besluit: een aanduiding van het gebied waar de gevolgen van het besluit mogelijk merkbaar zullen zijn. |


## Modules voor een kennisgeving

| XML-element | Type module | Inhoud |
|---|---| --- |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van (een versie van) de kennisgeving |
| [Kennisgeving](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving) | Instrumentversie-informatie | De tekst van (een versie van) de kennisgeving |
| [KennisgevingMetadata](data_xsd_Element_data_KennisgevingMetadata.dita#KennisgevingMetadata) | Annotatie | Metadata van een kennisgeving |
| [Procedureverloopmutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) | Annotatie | Als een kennisgeving informatie over het procedureverloop van een besluit bevat, dan wordt dat in de vorm van een mutatie geannoteerd. |
| [Gebiedsmarkering](geo_xsd_Element_geo_Gebiedsmarkering.dita#Gebiedsmarkering) | Annotatie | Ten behoeve van de attendering op de publicatie: een aanduiding van het gebied waar het besluit betrekking op heeft. |
| [Effectgebied](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied)         | Annotatie | Ten behoeve van de attendering op de publicatie: een aanduiding van het gebied waar de gevolgen van het besluit mogelijk merkbaar zullen zijn. |

## Modules voor een mededeling {#mededeling}

| XML-element                                                 | Type module                 | Inhoud                                                       |
| ----------------------------------------------------------- | --------------------------- | ------------------------------------------------------------ |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van (een versie van) de mededeling          |
| [Mededeling](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling)                          | Instrumentversie-informatie | De tekst van (een versie van) de mededeling                  |
| [MededelingMetadata](data_xsd_Element_data_MededelingMetadata.dita#MededelingMetadata)           | Annotatie                   | Metadata van een mededeling                                  |
| [Procedureverloopmutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) | Annotatie                   | Als een mededeling informatie over het procedureverloop van een besluit bevat, dan wordt dat in de vorm van een mutatie geannoteerd. |
| [Gebiedsmarkering](geo_xsd_Element_geo_Gebiedsmarkering.dita#Gebiedsmarkering)                | Annotatie                   | Ten behoeve van de attendering op de publicatie: een aanduiding van het gebied waar de mededeling betrekking op heeft. |
| [Effectgebied](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied)                        | Annotatie                   | Ten behoeve van de attendering op de publicatie: een aanduiding van het gebied waar de gevolgen van de mededeling mogelijk merkbaar zullen zijn. |

## Modules voor een rectificatie

| XML-element | Type module | Inhoud |
|---|---| --- |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van (een versie van) de rectificatie |
| [Rectificatie](tekst_xsd_Element_tekst_Rectificatie.dita#Rectificatie) | Instrumentversie-informatie | De tekst van (een versie van) de rectificatie |
| [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) | Annotatie | Aanwijzing hoe de correcties doorwerken in de consolidatie informatie die met het besluit (of een eerdere rectificatie) is meegeleverd. |
| [Procedureverloopmutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) | Annotatie | Als de rectificatie informatie over het procedureverloop van een besluit corrigeert, dan wordt dat in de vorm van een mutatie geannoteerd. |
| [RectificatieMetadata](data_xsd_Element_data_RectificatieMetadata.dita#RectificatieMetadata) | Annotatie | Metadata van een rectificatie |
| [Gebiedsmarkering](geo_xsd_Element_geo_Gebiedsmarkering.dita#Gebiedsmarkering) | Annotatie | Ten behoeve van de attendering op de publicatie: een aanduiding van het gebied waar het besluit betrekking op heeft. |
| [Effectgebied](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied) | Annotatie | Ten behoeve van de attendering op de publicatie: een aanduiding van het gebied waar de gevolgen van het besluit mogelijk merkbaar zullen zijn. |

## Modules voor aanpassen van de geconsolideerde regeling 

| XML-element                                                 | Type module                 | Inhoud                                                       |
| ----------------------------------------------------------- | --------------------------- | ------------------------------------------------------------ |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van een gereviseerde tekst                  |
| [Tekstrevisie](tekst_xsd_Element_tekst_Tekstrevisie.dita#Tekstrevisie)                      | Instrumentversie-informatie | Mutaties om tot de gereviseerde tekst te komen.              |
| [RegelingVersieMetadata](data_xsd_Element_data_RegelingVersieMetadata.dita#RegelingVersieMetadata)   | Instrumentversie-informatie | Versie-specifieke metadata van een regeling                  |
| [RegelingMetadata](data_xsd_Element_data_RegelingMetadata.dita#RegelingMetadata)               | Annotatie                   | Metadata van een regeling                                    |
| [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelatie.dita#Toelichtingsrelatie)         | Annotatie                   | Relatie tussen een tekst in de (artikelsgewijze) toelichting bij de regeling en de artikelen/informatieobjecten waar die betrekking op heeft |
| [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie)   | Annotatie                   | Aanwijzing hoe de correcties doorwerken in de consolidatie informatie die eerder is aangeleverd. |

## Modules voor aanpassen van het geconsolideerde GIO

| XML-element                                                  | Type module                 | Inhoud                                                       |
| ------------------------------------------------------------ | --------------------------- | ------------------------------------------------------------ |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie)  | Instrumentversie-informatie | De identificatie van een versie van het informatieobject     |
| [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) | Instrumentversie-informatie | Versie-specifieke metadata van een informatieobject          |
| [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata) | Annotatie                   | Metadata van een informatieobject                            |
| [GeoInformatieObjectVersie](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie) | Instrumentversie-informatie | De geo-informatie van (een versie van) het informatieobject exclusief informatie over de vaststelling ervan, zoals dat gebruikt wordt voor (geconsolideerde) informatieobjecten buiten de context van een besluit. |
| [FeatureTypeStyle](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle)                  | Annotatie                   | De symbolisatie die gebruikt moet worden om het geo-informatieobject te tonen. |
| [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie)    | Annotatie                   | Aanwijzing hoe de correcties doorwerken in de consolidatie informatie die eerder is aangeleverd. |

## Modules voor een officiële publicatie

| XML-element | Type module | Inhoud |
|---|---| --- |
| [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) | Instrumentversie-informatie | De identificatie van (een versie van) de officiële publicatie |
| [OfficielePublicatie](tekst_xsd_Element_tekst_OfficielePublicatie.dita#OfficielePublicatie) | Instrumentversie-informatie | De tekst van (een versie van) de officiële publicatie zoals gepubliceerd in het publicatieblad |
| [OfficielePublicatieVersieMetadata](data_xsd_Element_data_OfficielePublicatieVersieMetadata.dita#OfficielePublicatieVersieMetadata) | Instrumentversie-informatie | Versie-specifieke metadata van een officiële publicatie |
| [OfficielePublicatieMetadata](data_xsd_Element_data_OfficielePublicatieMetadata.dita#OfficielePublicatieMetadata) | Annotatie | Metadata van een officiële publicatie |
| [Procedureverloop](data_xsd_Element_data_Procedureverloop.dita#Procedureverloop) | Annotatie | Als de publicatie over een besluit gaat, dan is uit het procedureverloop af te leiden welke gerelateerde publicaties er zijn |

## Modules voor versiebeheer

| XML-element | Type module | Inhoud |
|---|---| --- |
| [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) | Instrumentversie-informatie | Beschrijving van de versie van een regeling met bijbehorende informatieobjecten en overige (niet-juridische) informatie. |
| [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering) | Instrumentversie-informatie | Optioneel: tekstuele toelichting op het *waarom* van de wijzigingen die in deze versie zijn aangebracht. |

