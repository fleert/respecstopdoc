# Juridische uitgangspunten


* [Algemene wet bestuursrecht (Awb)](awb.md)
* [Omgevingswet](ow.md)
* [Besluiten volgens de Awb](awb-besluit.md)
* [Procedure voorafgaand aan een besluit](voorbereiding-besluit.md)
* [Uniforme openbare voorbereidingsprocedure  ](uniforme-voorbereiding-besluit.md)
* [Terinzagelegging](terinzagelegging.md)
* [Bekendmaking besluit](bekendmaking-besluit.md)
* [Elektronische bekendmaking besluit](elektronische-bekendmaking-besluit.md)
* [Mededelen van een besluit](mededeling-besluit.md)
* [Consolidatie](consolidatie-juridisch.md)
* [Bezwaar](bezwaar.md)
* [Uniforme openbare voorbereidingsprocedure in STOP](uniforme-voorbereiding-besluit-stop.md)
* [Beroep](beroep.md)