# Meerdere BG

TODO

Onderwerp: wat als BG #1 iets wil wijzigen in regeling van BG #2 (en dat volgens de wet ook mag). Verschillende
  scenario's: (1) BG#1 maakt regeling waarin precies staat wat BG#2 moet doen. BG#2 voert dat uit (besluit, geen revisie, toch?).
  Regeling van BG#1 materieel uitgewerkt. Voorbeeld: instructie. De regels van BG1 gelden pas nadat BG#2 de wijziging heeft doorgevoerd.
  (2) BG#1 wijzigt de regeling van BG#2 via tijdelijk regelingdeel, BG#2 zet inhoud van tijdelijk regelingdeel over naar
  hoofdregeling (revisie), tijdelijk regelingdeel vervalt en wordt (technisch) ingetrokken. De regels van BG1 gelden meteen.