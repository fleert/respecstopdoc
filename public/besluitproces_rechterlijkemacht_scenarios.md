# Verwerken uitspraak beroep

Deze sectie gaat in op de stappen die een BG moet ondernemen om de uitspraak van een beroepsorgaan correct door te geven  aan de LVBB, zodat de uitspraak correct in de inhoud van de geconsolideerde regelgeving is verwerkt.

Een uitspraak van een beroepsorgaan moet volgens de Awb en STOP op de volgende wijze worden verwerkt:
* De procedurele status van het besluit moet zo nodig worden doorgegeven.
* Ook als de beroepsprocedure nog niet is afgerond, moet het BG de uitspraak van een beroepsorgaan verwerken in de geconsolideerde regeling.
* Is de beroepstatus van het besluit onherroepelijk geworden, dan moeten de definitieve uitspraken in het betreffende besluit worden verwerkt door het BG. 

Hoe dat precies moet zal voor elk van de vier uitspraakscenario's hieronder nader worden toegelicht. De [standaardstappen](proces_overzicht.md) voor besluiten van algemene strekking worden hierbij achterwege gelaten. Slechts de extra (deel)stappen voor het BG worden besproken. 


## Zie ook
* [Voorbeeldbestanden](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/index.md) van voor beroep vatbaar besluiten. U treft hier voorbeelden van de vier mogelijke scenario's.
* Als een beroepsorgaan uitspraak doet en een besluit (gedeeltelijk) vernietigd waarvan de regeling inmiddels is gewijzigd door andere besluiten, is er sprake van samenloop. Dat wil zeggen: de vernietiging maakt de geconsolideerde regelingversie die voortvloeide uit het vernietigde besluit (gedeeltelijk) incorrect, alsmede de geconsolideerde regelingversies van  opeenvolgende besluiten die op het vernietigde besluit waren gebaseerd. Zie [oplossen samenloop](proces_samenloop.md) over hoe het BG ervoor kan zorgen dat vernietiging van het besluit is verwerkt in de huidige geconsolideerde regeling.
