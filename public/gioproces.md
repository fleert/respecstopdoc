# Proces met informatieobjecten

## Hoe kan ik data juridisch borgen? 

STOP biedt naast tekst ook de mogelijkheid om informatieobjecten juridisch te borgen. Een [informatieobject](def#informatieobject) bevat niet-tekstuele informatie die onderdeel uitmaakt van de voorschriften en regels uit de regeling, maar die niet voorkomt in de tekst van de regeling zelf. Informatieobjecten zijn dus geen illustraties, tabellen, etc., die wel onderdeel van de tekst zijn. Kortom informatie die niet in tekstuele vorm op een voor de mens leesbare manier weergegeven kan worden. Een voorbeeld van een informatieobject is de geografische afbakening van het werkingsgebied van een regeling: een geografisch informatieobject (GIO).

Aan de hand van een aantal voorbeelden wordt toegelicht hoe in STOP een besluit een nieuwe regeling met een GIO instelt, wijzigt of intrekt.

[Nieuwe regeling met GIO](gioproces_nieuwe_regeling_met_gio.md)
: Een besluit tot het instellen van een nieuwe regeling die geldt in een bepaald gebied.

[Regeling met GIO wijziging](gioproces_regeling_met_gio_wijziging.md)
: Besluit tot wijziging van het gebied waar de regeling van toepassing is.

[Intrekken GIO en instelling nieuwe GIO](gioproces_intrekking_gio_en_instelling_nieuwe.md)
: Een besluit tot aanwijzen van een ander gebied waar de regeling van toepassing is.

[Intrekken regeling met GIO](gioproces_intrekken_regeling_met_gio.md)
: Besluit om een bestaande regeling met GIO niet langer van toepassing te laten zijn.