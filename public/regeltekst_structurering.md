# Structurering van tekst

STOP kent verschillende categorieën tekstelementen, waarmee een juridische tekst in XML gecodeerd kan worden. Per categorie wordt een toelichting gegeven op een aparte pagina.  

[Instrument](regeltekst_instrument.md)-elementen
: De 'Instrument'-elementen zijn de rootelementen waarmee een juridisch instrument in STOP gecodeerd wordt. 

[Structuur](regeltekst_structuur.md)-elementen
: De 'Structuur'-tekstelementen geven structuur aan de tekst van het instrument. 

[Koppen](regeltekst_koppen.md)
: De 'Koppen' categorie bevat elementen voor de naamgeving van de structuur. 

[Inhoud](regeltekst_inhoud.md)-elementen
:  De 'Inhoud'-tekstelementen zijn de elementen 'onderaan' de structurele hiërarchie die gevormd wordt door de structuurelementen. 

[Bloktekst](regeltekst_bloktekst.md)-elementen
: De inhoud is opgebouwd uit 'Bloktekst'-elementen met 'broodtekst'. Elk bloktekst element voorziet de broodtekst van specifieke formattering. Daarnaast worden [tabellen](regeltekst_tabel.md) en [afbeeldingen](regeltekst_afbeelding.md) specifiek toegelicht.

[Inline](regeltekst_inline.md)-elementen
: Waar de bloktekst elementen een compleet tekstblok van formattering voorzien, kan met een 'Inline'-element een specifiek gedeelte van een tekst geformatteerd worden of van een verwijzing worden voorzien. 

[Aantekening](regeltekst_aantekening.md)-elementen
: Specifieke situaties in de geconsolideerde regeling worden aangeduid met aantekeningelementen.

[Renvooi](regeltekst_renvooi.md)-elementen
: Door middel van renvooi-elementen kunnen wijzigingen gecodeerd worden.

[Publicatieblad](regeltekst_publicatieblad.md)-elementen
: De 'Publicatieblad'-tekstelementen zijn elementen die uitsluitend gebruikt worden in officiële (juridische) publicatiebladen.

[Tabeltechnische](regeltekst_tabeltech.md)-elementen
: Enkele elementen die geen tekst bevatten, maar gebruikt worden om tabelstructuren te coderen.