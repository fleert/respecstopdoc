# Een GIO maken

Een GIO maken gebeurt als volgt:

1. Controleer of er een bestaande GIO kan worden hergebruikt.
1. Zo ja, gebruik dan de bestaande GIO.
1. Zo nee, stel dan de GIO samen en gebruik daarbij zo veel mogelijk bestaande locaties of Ow-objecten met geometrische data.
1. Geef ook de relatie tussen bron-object en Ow-objecten aan in de module `JuridischeBorgingVan`.

