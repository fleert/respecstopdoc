# Inhoud van het uitwisselpakket

## Algemene eisen
Voor de uitwisseling van een versie van de regeling tussen bevoegd gezag en adviesbureau wordt het 'uitwisselpakket' gebruikt. Een uitwisselpakket is een gestandaardiseerde manier om een verzameling STOP modules uit te wisselen. Hier wordt beschreven welke informatie het uitwisselpakket moet bevatten om een regelingversie tussen een adviesbureau en het bevoegd gezag uit te wisselen. De structuur van het [uitwisselpakket](uitwisselpakket.md) staat elders beschreven. 

Voor het uitwisselen van een regelingversie bevat het uitwisselpakket:

- Het bestand [pakbon.xml](uws_xsd_Element_uws_Pakbon.dita#Pakbon) dat de inhoud en samenhang van het uitwisselpakket beschrijft.
- De versie van een regeling voor één [branch](samenwerken_versiebeheer.md). Er mag een versie van meer dan één regeling aanwezig zijn, mits het noodzakelijk is dat beide regelingen tegelijk aangepast moeten worden. Bijvoorbeeld als een deel van de voorbeschermingsregels overgezet moeten worden naar het omgevingsplan.
- Voor elke regeling moeten aanwezig zijn:
  - Alle STOP-modules (tekst, metadata, annotaties) voor de versie van de regeling. Deze modules zijn opgenomen in aparte bestanden in het pakket.
  - In de pakbon: de vermelding van de regeling-versie als component.
  - Alle gerelateerde informatie zoals beschreven door andere standaarden (zoals IMOW).
* Voor elk [informatieobject](io-intro.md) waarnaar [statisch wordt verwezen](io-tekstverwijzen.md) vanuit de versie van de regeling(en) of die de regeling(en) als geboorteregeling hebben:
  - Alle STOP-modules (metadata, inhoud informatie-object, annotaties) voor de versie van het informatie-object.
  - Als het type informatie-object een [GIO](gio-intro.md) is: de module [`JuridischeBorgingVan`](gio-juridische-borging.md).
  - In de pakbon: de vermelding van de informatieobject-versie als component.
  - Alle gerelateerde informatie zoals beschreven door andere standaarden (zoals IMOW).
* [Versie-informatie](uitwisselpakket_versieinformatie.md) voor het uitwisselpakket: 
  * De STOP-module [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) met tenminste: 
    * Als identificatie van de branch het [`doel`](data_xsd_Element_data_doel.dita#doel) waartoe de regelingversies en informatieobject-versies in het uitwisselpakket behoren.
    * De datum [`gemaaktOp`](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) waarmee de versie van de modules in dit uitwisselingspakket wordt vastgelegd.
  * In de pakbon: een vermelding van de versie-informatie als component met als soortWork `/join/id/stop/work_024` en een eigen [identificatie](versieinformatie_naamgeving.md).

In het uitwisselpakket wordt ook elk informatie-object opgenomen waarnaar [statisch wordt verwezen](io-tekstverwijzen.md) vanuit de versie van de regeling. Het kan om een informatie-object uit een andere regeling gaan. Een informatie-object kan alleen worden gewijzigd als ook de geboorteregeling onderdeel is van het uitwisselpakket. 

Er zijn ook informatie-objecten die een te wijzigen regeling als geboorteregeling hebben maar waarnaar niet vanuit de regeling wordt verwezen (zoals de [pons](pons.md)). Deze moeten ook worden opgenomen in het uitwisselpakket.


## Uitwisselpakket LVBB downloadservice
De geldende regelgeving zoals beschikbaar via de LVBB is op te halen via een downloadservice. Deze splitst de inhoud van het uitleverpakket op in een uitleverpakket voor een regelingversie (tekst met bijbehorende modules en annotaties uit andere standaarden, zoals IMOW) en een uitleverpakket per informatieobject. Verder komt de inhoud van elk pakket overeen met wat bij de algemene eisen is weergegeven.

Voor GIO's zal de downloadservice alleen de module [`JuridischeBorgingVan`](gio-juridische-borging.md) meeleveren als die is aangeleverd bij de publicatie van het besluit waarmee de versie van de GIO is vastgesteld. Zolang aanlevering van de module bij een GIO-versie niet verplicht gesteld kan worden, kan de downloadservice de beschikbaarheid ervan niet garanderen.

## Van bevoegd gezag naar adviesbureau
Als het bevoegd gezag een adviesbureau opdracht geeft een nieuwe versie van de regeling te maken, dan zal het bevoegd gezag een uitwisselpakket maken met de basisversie waarmee het adviesbureau moet gaan werken. Dat pakket voldoet aan de algemene eisen. Een bevoegd gezag kan een versie van regeling(en) en informatieobjecten opnemen die nog niet bekendgemaakt zijn. Het `doel` in de momentopname zal in het algemeen specifiek zijn voor de opdracht (zie [versiebeheer](samenwerken_versiebeheer.md)).

Zie ook het [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/3.%20Samenwerking/3a.%20Opdracht) van een dergelijk uitwisselpakket.

## Van adviesbureau naar bevoegd gezag
Als een adviesbureau de nieuwe versie aan het bevoegd gezag levert, dan is de informatie in het uitwisselpakket uitgebreider dan in de algemene eisen is beschreven.

De [Versie-informatie](uitwisselpakket_versieinformatie.md) component bestaat dan uit:
* De STOP-module [Momentopname](data_xsd_Element_data_Momentopname.dita#Momentopname) met: 
    * Als identificatie van de branch het [`doel`](data_xsd_Element_data_doel.dita#doel) waartoe de regelingversies en informatieobject-versies in het uitwisselpakket behoren.
    * De datum [`gemaaktOp`](data_xsd_Element_data_gemaaktOp.dita#gemaaktOp) waarmee de versie van de modules in dit uitwisselingspakket wordt vastgelegd.
    * Binnen [`bevatWijzigingenVoor`](data_xsd_Element_data_bevatWijzigingenVoor.dita#bevatWijzigingenVoor) voor elke gewijzigde versie van een regeling of informatieobject: een [`UitgewisseldInstrument`](data_xsd_Element_data_UitgewisseldInstrument.dita#UitgewisseldInstrument) met een verwijzing naar de versie die als basisversie voor de gewijzigde modules is gebruikt.
* (Optioneel) een [Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering) met daarin een toelichting op de wijzigingen die in de regeling(en) in dit uitwisselingspakket zijn doorgevoerd.
* (Optioneel) PDF-documenten die zijn vereist als onderbouwing van de wijzigingen, met per PDF:
  - Alle STOP-modules die horen bij een alleen-te-publiceren PDF-informatieobjectversie
  - In de pakbon: de vermelding van de PDF-informatieobjectversie als component.

Zie ook het [voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Uitwisselpakket/3.%20Samenwerking/3b.%20Leveren%20aan%20BG) van een dergelijk uitwisselpakket.

Als het bevoegd gezag het Rijk is en de regeling volgens het [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) model is opgesteld, dan is niet `Motivering` onderdeel van de Versie-informatie component maar een incompleet [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek) die een toelichting bevat.