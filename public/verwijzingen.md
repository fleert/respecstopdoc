# Verwijzingen naar regelgeving

## Soorten verwijzingen

Op verschillende plaatsen in de standaard staat beschreven hoe verwijzingen vanuit (een annotatie bij) een regeling naar een andere regeling of informatieobject gelegd kunnen worden. Sommige verwijzingen zijn **informatiekundig** en betekenen dat de regeling en waarnaar verwezen wordt bij elkaar horen of op een andere manier in relatie tot elkaar staan. Zoals bijvoorbeeld bij een tijdelijk regelingdeel en de hoofdregeling. Maar er zijn ook verwijzingen die een juridisch karakter hebben, die aangeven dat de inhoud van de ene regeling gebruik maakt, voortbouwt, uitgewerkt wordt, of in de plaats komt van de inhoud van een andere regeling.

Juridische verwijzingen zijn er in twee soorten: _statisch_ en _dynamisch_. Bij een **statische** verwijzing wordt verwezen naar een specifieke versie van een regeling of informatieobject, bijvoorbeeld:

* Regeling: Artikel 1.1 van de Omgevingsregeling zoals gepubliceerd op 22-11-2019

  /akn/nl/bill/mn002/2019/r00001/nld@2019;1#chp_1__subchp_1.1__art_1.1

* Informatieobject: Geometrische begrenzing van oppervlaktewaterlichamen beheer van de waterkwaliteit zoals gepubliceerd op 22-11-2019

  /join/id/regdata/mn002/2019/or_waterkwaliteit/nld@2019-11-01

In dit soort verwijzing wordt de identificatie van de expression van de regeling of informatieobject gebruikt.

**Dynamische** verwijzingen wijzen naar de regeling of informatieobject als geheel. Hierbij wordt de identificatie van het work gebruikt, bijvoorbeeld:

* Regeling: Artikel 1.1 van de Omgevingsregeling

  /akn/nl/bill/mn002/2019/r00001#chp_1__subchp_1.1__art_1.1

  (Het artikel wordt met een [eId](eid_wid.md) aangeduid.)

* Informatieobject: Geometrische begrenzing van oppervlaktewaterlichamen beheer van de waterkwaliteit

  /join/id/regdata/mn002/2019/or_waterkwaliteit

## Uitwerken van dynamische verwijzingen

Om precies te bepalen naar welke versie van een regeling of informatieobject een dynamische verwijzing wijst, moet uitgegaan worden van de context waarin de regeling met de verwijzing getoond wordt. Als bijvoorbeeld de huidig geldende regeling getoond wordt, dan wordt verwezen naar de huidig geldende versie van de andere regeling/informatieobject. Als een specifieke [tijdreis](tijdreizen-toelichting.md) gebruikt is, dan moet dezelfde tijdreis gebruikt worden om de versie van de andere regeling/informatieobject te bepalen.

Het is niet ongebruikelijk dat een regeling wordt gewijzigd waardoor dynamische verwijzingen naar een artikel uit de regeling voor bepaalde tijdreizen niet meer bij een geldige versie van de regeling uitkomen waarin dat artikel nog voorkomt. Regelingen en informatieobjecten evolueren immers in het algemeen onafhankelijk van elkaar. Bijvoorbeeld als een gemeente in haar regeling verwijst naar een artikel in de regeling van een provincie. De provincie zal eerst haar regeling aanpassen, waardoor dynamische verwijzingen na de inwerkingtreding van de wijziging niet meer altijd hoeven te werken. In antwoord daarop zal de gemeente haar regeling wijzigen met dezelfde inwerkingtredingsdatum, waardoor de verwijzingen weer voor elke tijdreis kloppen.

Als een bevoegd gezag voor een dynamische verwijzing kiest, wordt geaccepteerd dat de verwijzing niet voor elke tijdreis zal kloppen. Een niet-kloppende verwijzing is daarom een juridisch probleem en geen informatiekundig probleem.

## Implementatiekeuze: statische en informatiekundige verwijzingen

Statische en informatiekundige verwijzingen die naar een regeling(versie) of informatieobject(versie) verwijzen kunnen onbruikbaar zijn als hetgeen waarnaar verwezen wordt onbekend is. Rekening houden met de onbekendheid ervan kan software erg complex maken. Het is daarom aan STOP-gebruikende systemen om te bepalen hoe met informatiekundige en statische verwijzingen omgegaan wordt. Het systeem kan bijvoorbeeld vereisen dat de regeling(versie) die verwijst en hetgeen waarnaar verwezen wordt altijd moeten bestaan. Dus dat alleen een verwijzing is toegestaan naar een al in het systeem bekende regeling of informatieobject. Of dat een regeling of informatieobject alleen ingetrokken mag worden als er geen informatiekundige verwijzingen naar zijn vanuit geldige regelingen.

Voor dynamische verwijzingen ligt dat anders. Een STOP-gebruikend systeem mag wel bepalen of een nieuwe dynamische verwijzing al dan niet naar een bekende regeling of informatieobject verwijst. Omdat de regeling die verwijst en hetgeen waarnaar verwezen wordt onafhankelijk van elkaar moeten kunnen evolueren mag de software niet eisen dat een dynamische verwijzing op elk moment uitgewerkt kan worden. De software moet er rekening mee houden dat de uitwerking van een dynamische verwijzing kan uitkomen bij een (onderdeel van een) regeling of informatieobject die niet meer geldig is.