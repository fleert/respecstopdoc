# Hoofdlijnen van het STOP-proces 

Deze leeswijzer geeft een introductie van het STOP proces, waarin uitzonderingen, speciale situaties, etc. achterwege zijn gelaten. Voor gedetailleerde beschrijving van het proces wordt verwezen naar de [volledige procesbeschrijving](proces_overzicht_compleet.md).

## Achtergrond

STOP is gericht op het bekend maken van besluiten van het bevoegd gezag (gemeentes, provincies, waterschappen en de landelijke overheid). Na de bekendmaking van een besluit wordt bepaald wat het effect van het besluit op de regelgeving is: introductie van een nieuwe regeling, en/of aanpassing van een of meer bestaande regelingen. 

Met de invoering van de Bekendmakingswet wordt het mogelijk om domein-specifieke "annotaties" toe te voegen aan de regelgeving. Deze "annotaties" bevatten een machine leesbare duiding van de regelgeving, waarmee een koppeling gerealiseerd kan worden tussen de regelgeving en op deze regelgeving gebaseerde automatisering. 

De Omgevingswet gaat hier als eerste gebruik van maken. Door een Omgevingswet besluit te voorzien van regeling gerelateerde informatie ("annotaties") staat de bekendmaking niet langer op zich, maar wordt deze een onderdeel van de [landelijke voorziening van het digitaal stelsel van de omgevingswet](https://aandeslagmetdeomgevingswet.nl/digitaal-stelsel/) (DSO-LV). In deze keten is het wenselijk om de regeling gedurende zijn gehele levensloop, synchroon te houden met de daaraan gerelateerde informatie. Denk bijvoorbeeld aan beslisbomen die op de regeling zijn gebaseerd. Bovendien is het door de opzet van STOP mogelijk om, na de publicatie van een besluit, geautomatiseerd de geldende regelgeving te kunnen bepalen. Iets wat tot nu toe nog gedeeltelijk handwerk is.

Hieronder wordt dit proces toegelicht, waarbij de volgende terminologie wordt gebruikt: 

- regeling: juridische voorschriften bestaande uit de regeling-tekst en informatieobjecten waarnaar verwezen wordt vanuit de regeling-tekst;
- regeling-tekst: een tekst die juridische voorschriften bevat;
- informatieobject: niet-tekstuele informatie die onderdeel is van de voorschriften van de regeling (bijv. geometrische gebiedsaanduiding);
- besluit: een tekst die een of meer regelingen vaststelt, wijzigt of intrekt. Een besluit kan ook de geldigheid van een regeling bepalen, inclusief de met de regeling samenhangende informatieobjecten. Een besluit kan zelf een informatieobject als bijlage bevatten, bijvoorbeeld een MER rapportage; 
- regelgeving: de verzameling van alle in werking getreden regelingen en informatieobjecten;
- regelgeving-gerelateerde informatie: gegevens en/of teksten die afgeleid zijn van, of samenhangen met, de regelgeving. Zoals "annotaties" en "beslisbomen".

Deze terminologie wijkt iets af van het gebruikelijke juridische jargon omdat het juridische jargon voor de uitleg van STOP niet altijd specifiek genoeg is. Zie voor een toelichting [Besluit en regeling](proces_besluit_en_regeling.md). 

## Proces op hoofdlijnen

Het STOP proces bestaat uit drie stappen:

1. Opstellen regeling

   Het opstellen van een nieuwe of een aangepaste regeling, met bijbehorende informatieobjecten en gerelateerde informatie;

2. Bekendmaken besluit

   De officiële publicatie van het besluit waarmee de regeling bekend wordt gemaakt en waarmee de inwerkingtreding wordt geregeld. 

3. Consolideren 

   Op basis van het inwerkingtreden van het besluit samenstellen van de geldende regelgeving op elk moment in de tijd.

### 1. Opstellen regeling

Het proces start als het bevoegd gezag constateert dat er ergens in de toekomst nieuwe regelgeving nodig is. Dit kan een geheel nieuwe regeling zijn of een aanpassing van een bestaande regeling. 

Om een regeling correct in werking te kunnen laten treden is het volgende nodig:

- De regeling-tekst;
- Bijbehorende informatieobjecten;
- Gerelateerde informatie, bijv. beslisbomen, annotaties, etc.

**Opstellen in samenhang**
Tijdens het opstellen van de regeling zullen de tekst van de regeling, bijbehorende informatieobjecten en de gerelateerde informatie, geleidelijk hun definitieve vorm krijgen. Het is belangrijk dat dit in samenhang gebeurt om te borgen dat de nieuwe regeling als geheel in werking kan treden. Omdat aan het begin van dit proces meestal niet bekend is wanneer de regeling in werking zal treden, is iets anders dan de inwerkingtredingsdatum nodig om de samenhang te borgen. 

**Doel**
STOP gebruikt het [doel](doel.md) om deze samenhang te borgen. Zodat bij het wijzigen van één onderdeel van de regeling (regeling-tekst, informatieobjecten en gerelateerde informatie) niet direct ook een nieuwe versie van de andere regelingen gemaakt hoeft te worden. Door bij elke versie van de onderdelen het doel aan te geven, is duidelijk welke onderdelen samen één regelingversie vormen. Waarbij alle onderdelen die gekoppeld zijn aan hetzelfde doel uiteindelijk op hetzelfde moment in werking zullen treden. 

**Integrale versie**
Om zoveel mogelijk aan te sluiten bij de besluitvormingsprocessen van decentrale overheden, waarbij gewerkt wordt met de complete regelgeving zoals die op een bepaald moment geldig zal zijn, is STOP voornamelijk gericht op het werken met integrale versies. Hierdoor is het mogelijk om gerelateerde informatie, die gebaseerd is op de volledige regelgeving, tegelijk met de regelgeving te ontwikkelen. Overigens ondersteund STOP ook de huidige juridische praktijk waarin uitsluitend gewerkt wordt met wijzigingen ten opzichte van bestaande regelgeving.   

**Tekstmodellen**
Vanuit de juridische praktijk worden er eisen gesteld aan de tekstuele structuur van een regeling. Binnen de standaard wordt hier invulling aan gegeven door twee [tekst modellen](regeling_artikelsgewijs.md) die voorschrijven hoe regelingen gestructureerd moeten worden. 

### 2. Bekendmaken besluit

Om een regeling zeggingskracht te geven, moet het bevoegd gezag een besluit nemen over de regeling: de regeling wordt opgenomen in een besluit. Vervolgens wordt het besluit publiek gemaakt zodat iedereen hiervan kennis kan nemen. Een besluit kan voorafgegaan worden door een ontwerpbesluit.  

**Regelgeving in het besluit**
Het besluit beschrijft dus het instellen van een nieuwe regeling of het wijzigen van een bestaande regeling inclusief bijbehorende informatieobjecten. Er bestaan een aantal varianten om het instellen, wijzigen of vervangen van [regelgeving op te nemen in een besluit](proces_besluit.md). Bijvoorbeeld om een nieuwe regeling gefaseerd in werking te laten treden.

**Renvooi**
Een nieuwe regeling en een nieuw of een vervangend informatieobject wordt geheel opgenomen in het besluit, een wijziging van een bestaande regeling wordt via het [STOP-renvooimechanisme](bepalen_wijzigingen_renvooi.md) in het besluit opgenomen. Waarbij het besluit de nieuwe versie bevat met expliciet de wijzigingen ten opzichte van een vorige versie van de regeling. 

**Inwerkingtreding**
Als een besluit wordt gepubliceerd, hoeft de regeling waar het besluit over gaat niet meteen in werking te treden. Het besluit kan bijvoorbeeld aangeven dat de regeling pas per 1 januari in zal gaan. Maar het is ook mogelijk dat er helemaal geen inwerkingtredingsdatum in het besluit staat en dat het besluit van rechtswegen automatisch na een bepaalde periode in werking treedt of dat een apart besluit later de inwerkingtreding regelt. 

De inwerkingtreding en andere geldigheidsinformatie is geassocieerd met het [doel](doel.md), en moet opgenomen worden in de tekst van het besluit. De standaard biedt ondersteuning voor vier verschillende [inwerkingtredingsbepalingen](proces_iwt.md), waaronder het regelen van de inwerkingtreding via een apart besluit.

**Bekendmaking**
Het bevoegd gezag levert het besluit ter bekendmaking aan het [bronhouderkoppelvlak](@@@BHKV_URL@@@) van de Landelijke Voorziening Bekendmaken en Beschikbaarstellen (LVBB). De LVBB zorgt ervoor dat het aangeleverde besluit gepubliceerd wordt op [Officielebekendmakingen.nl](https://www.officielebekendmakingen.nl/). 

Alleen het juridisch relevante deel van het besluit wordt bekend gemaakt. Naast het besluit wordt ook consolidatie-informatie op basis van het [doel](doel.md) meegeleverd en kan ook regeling-gerelateerde informatie worden meegeleverd, bijv. annotaties of beslisbomen. Deze informatie wordt niet bekendgemaakt, het is immers geen onderdeel van het juridische besluit. De regeling-gerelateerde informatie wordt, samen met de geconsolideerde versie van de regeling doorgeleverd naar andere landelijke voorzieningen zoals DSO-LV, de voorziening in het [digitaal stelsel van de omgevingswet](https://aandeslagmetdeomgevingswet.nl/digitaal-stelsel/) om omgevingsdocumenten te ontsluiten in het Omgevingsloket. 

### 3. Consolideren

[Consolidatie](consolideren_introductie) is het proces om uit de bekendgemaakte en in werking zijnde besluiten af te leiden wat op elk moment in de tijd de geldende regelgeving is. Als het besluit een nieuwe regeling betreft, bevat het besluit de volledige regeling. Alle vervolgbesluiten bevatten slechts wijzigingen. Het besluit betreft dan het aanpassen van een bestaande regeling. Indien een besluit voor bezwaar of beroep vatbaar is, kan dan alleen op de wijzigingen in de regeling bezwaar worden gemaakt. 

Omdat besluiten elkaar niet altijd netjes chronologisch opvolgen, één besluit meerdere regelingen kan wijzigen, besluiten conditioneel in werking kunnen treden, besluiten met terugwerkende kracht in werking kunnen treden en vanwege een [uitspraak van een rechter](besluitproces_rechterlijkemacht.md) --> voorgaande besluiten weer kan wijzigen is het ingewikkeld om te bepalen wanneer nu precies welke versie van een regeling geldig was/is. 

De standaard zorgt er in principe voor dat de geconsolideerde regelgeving geautomatiseerd bepaald kan worden. Helaas is dat niet in elke situatie mogelijk, bijvoorbeeld als twee besluiten tegelijk dezelfde regeling wijzigen. Dit wordt samenloop genoemd. Het bronhouderkoppelvlak zal in die situatie aan het bevoegd gezag melden dat de geautomatiseerde consolidatie niet uitgevoerd kan worden. Het bevoegd gezag moet dan aangeven wat de [oplossing van de samenloop](proces_samenloop.md) is.

Via het bronhouderkoppelvlak kan het bevoegd gezag de consolidatie van regelingen en informatieobjecten uit bekendgemaakte besluiten op halen, om zo weer de basis te vormen van volgende (wijzigings-)besluiten.



## Verder lezen

De gedetailleerde beschrijving van het STOP proces is te vinden in de [volledige procesbeschrijving](proces_overzicht_compleet.md) en onderliggende pagina's.

De eisen en uitgangspunten van het ontwerp en de structuur van STOP zijn te vinden in de leeswijzer [Architectuur en ontwerpkeuzes.](architectuur.md) 

De informatiekundige opbouw van de standaard is beschreven in [Software development met STOP](gebruik.md), hier is ook een [snelstartgids](quickstart_overzicht.md) te vinden die beschrijft hoe de STOP-XML modules gebruikt moeten worden in het STOP proces.

Een nadere toelichting op het gebruik van STOP als onderdeel van de DSO-keten wordt gegeven in [Voortbouwen op STOP](voortbouwen_op_STOP.md).