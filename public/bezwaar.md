# Bezwaar

Als een belanghebbende in zijn belang wordt geschaad door een besluit, kan deze [bezwaar](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.5&paragraaf=3.5.3&artikel=3:28
) maken bij het bestuursorgaan die dat besluit heeft genomen. Voor meer informatie over bezwaar, zie [Bezwaar en beroep tegen een beslissing van de overheid](https://www.rijksoverheid.nl/documenten/brochures/2015/04/14/bezwaar-en-beroep-tegen-een-beslissing-van-de-overheid).  

Een belanghebbende kan bezwaar maken als:
* Deze het niet eens is met een besluit dat is genomen naar aanvraag van de belanghebbende. Bijvoorbeeld wanneer iemand een bouwvergunning heeft aangevraagd bij de gemeente en de gemeente besluit deze niet te verlenen.
* Deze het niet eens is met een besluit dat is genomen naar aanleiding van een aanvraag van iemand anders, maar dat de belanghebbende rechtstreeks raakt. Bijvoorbeeld, een buurman kan bezwaar maken tegen een bouwvergunning van zijn buurman, omdat hierdoor zon wordt weggenomen.
* Deze nadelige gevolgen ondervindt van een besluit dat het bestuursorgaan uit zichzelf neemt.

Of en hoe er bezwaar kan worden gemaakt staat in de kennisgeving bij de bekendmaking van het besluit in het elektronische publicatieblad vermeld.
