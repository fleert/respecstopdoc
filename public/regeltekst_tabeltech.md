# Soort tekstelement: Tabeltechnisch

Naast de [bloktekstelementen](regeltekst_bloktekst.md) [table](tekst_xsd_Element_tekst_table.dita#table) en [entry](tekst_xsd_Element_tekst_entry.dita#entry) zijn er nog een aantal 'Tabeltechnische' tekstelementen waarmee tabelstructuren gecodeerd worden. Deze bevatten zelf geen tekst. Zie [hier](regeltekst_tabel.md) voor een toelichting op de toepassing van deze elementen.

## Tabeltechnische elementen

[colspec](tekst_xsd_Element_tekst_colspec.dita#colspec), [row](tekst_xsd_Element_tekst_row.dita#row), [tbody](tekst_xsd_Element_tekst_tbody.dita#tbody), [tgroup](tekst_xsd_Element_tekst_tgroup.dita#tgroup), [thead](tekst_xsd_Element_tekst_thead.dita#thead)

