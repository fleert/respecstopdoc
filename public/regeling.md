# Regeling

## Inhoud van de regeling
Een regeling in STOP is het instrument dat de tekst van de (geconsolideerde) regelgeving bevat zoals beschreven bij het [automatiseerbaar creatieproces](automatiseerbaar-creatieproces.md), eventueel aangevuld met [annotaties](annotaties.md). STOP stelt als eis dat alleen die tekst uit een besluit die voorschriften en (beleids-)regels beschrijft in een regeling wordt ondergebracht. Dit is het resultaat van een informatiekundige analyse van de inhoud van het besluit, dat per soort besluit in een [toepassingsprofiel](toepassingsprofielen.md) wordt beschreven.

[Geldigheidsbepalingen](regeling_geldigheid.md)
: In STOP kan er op elk moment in de tijd maar één versie van de regeling geldig zijn. Dat stelt eisen aan de manier waarop in de tekst wordt vastgelegd onder welke voorwaarden een voorschrift of (beleids-)regel geldig is.

## Tekst in STOP

Alle tekstmodellen voor regelingen zijn volgens dezelfde principes opgezet. Veel van deze principes zijn ook van toepassing op de tekstmodellen voor besluiten en officiële publicaties.

[Tekststructuur en tekstelementen](regeltekst.md)
: Het principe achter de tekstmodellen van STOP. Een lezer ziet de regeling als een doorlopende tekst. In STOP wordt de tekst gecodeerd met elementen die de aard van de tekst aanduiden. Dit biedt software de mogelijkheid op verschillende manieren door de tekst te navigeren of de tekst in fragmenten te presenteren.

[Categorieën tekst-elementen](regeltekst_structurering.md)
: De structuur van de tekst wordt gevormd door verschillende categorieën tekstelementen.  

[Identificatie van tekstelementen](eid_wid.md)
: Elk tekstelement heeft een eigen identificatie. Die bestaat uit twee identifiers: eId bedoeld voor verwijzen van tekst naar een bepaalde plaats in de tekst en wId om te verwijzen naar de inhoud van de tekst.

[Tekstweergave](regeltekst_weergave.md)
: De standaard schrijft niet in detail voor hoe tekst weergegeven moet worden. De tekst moet weer te geven zijn als onderdeel van een officiële publicatie, die op A4-formaat worden vormgegeven. Daaruit volgen wel eisen aan de indeling van de tekst en de grootte en kwaliteit van illustraties.

## Modellen voor een regeling { #modellen }

De beschikbare modellen voor een regeling zijn: 

[Artikelsgewijze regeling](regeling_artikelsgewijs.md)
: De tekst van regelingen met voorschriften van algemene strekking kent een opbouw waarbij de voorschriften in principe in het lichaam van de regeling als artikelen worden opgeschreven.

[Regeling met vrije tekst](regeling_vrijetekst.md)
: Voor de overige regelingen is een model beschikbaar waarbij voor het lichaam alleen de structuur wordt vastgelegd. Dit soort teksten is gebruikelijk bij regelingen met beleidsregels of regelingen met zelfbindende voorschriften.

Voor alle regeling modellen is van toepassing:

[Metadata van een regeling](regeling_metadata.md)
: Bij elke regeling hoort metadata die de globale kenmerken van de regeling bevat. 

[Naamgeving van een regeling](regeling_naamgeving.md)
: Voor de naamgeving van een regeling wordt de Akoma Ntoso naamgevingsconventie gebruikt.

## Tekstmodel STOP 2.x t.o.v. 1.3.0
[Voorstellen voor verbetering van het tekstmodel](voorstel_wijzigingen.md)
: Voor de volgende release van STOP zijn een aantal verbeteringen van de tekstmodellen voorzien.

