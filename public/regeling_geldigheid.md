# Geldigheidsbepalingen

Omdat in STOP op elk moment in de tijd maar één versie van de regeling geldig kan zijn, stelt STOP eisen aan de manier waarop in de tekst wordt vastgelegd onder welke voorwaarden een voorschrift of (beleids-)regel geldig is. Situaties waarin twee regelingen tegelijkertijd geldig zijn (zoals [deze](https://wetten.overheid.nl/jci1.3:c:BWBR0001827) en [deze](https://wetten.overheid.nl/BWBR0039872)) zijn met STOP niet mogelijk.

[In- en uitwerkingtredingsbepalingen](besluit_iwtbepalingen.md) zijn in STOP in principe onderdeel van het besluit (en dus niet van de regeling). Alleen als een regeling een uitzondering maakt voor *een bepaalde doelgroep*, bijvoorbeeld bij overgangsrecht, worden geldigheidsbepalingen met de condities waaronder en eventueel de periode waarvoor, opgenomen in de regeling. 

Een regeling met bepalingen om delen van een regeling vanaf een bepaald moment *voor iedereen* wel of niet van toepassing te laten zijn, moet in STOP opgesplitst worden in meerdere versies die op verschillende momenten in werking treden. 

**TODO** onderstaande verifiëren bij juristen.

## Geen geldigheidsperiode in de tekst van de regeling

Een versie van de geconsolideerde regeling in STOP bevat die voorschriften en (beleids-)regels die op een bepaald moment geldig zijn. Als op een ander moment andere voorschriften of (beleids-)regels gelden, dan moet daarvoor een nieuwe versie van de regeling opgesteld worden. Het moment van geldig worden van die versie wordt geregeld via de [inwerkingtredingsbepalingen](besluit_iwtbepalingen.md) in het besluit. Alleen op deze wijze kan geautomatiseerd bepaald worden welke regels geldig zijn.

Het is daarom niet de bedoeling om de geldigheid van regels op te nemen in de tekst van een regeling, ook al is dat juridisch wel mogelijk. In plaats van:

    Artikel 17.1
        1. Voor de toepassing van artikelen ... geldt bijlage XXXVI in plaats van bijlage V
        2. Dit artikel vervalt op 1-1-2024.
    ...
    Bijlage XXXVI ...

zou de eerste versie van de regeling bij voorkeur moeten luiden:

    Artikel 17.1
        1. Voor de toepassing van artikelen ... geldt bijlage XXXVI in plaats van bijlage V
    ...
    Bijlage XXXVI ...

en de tweede versie, met een inwerkingtredingsdatum van 1-1-2024:

    Artikel 17.1
        [Vervallen]
    ...
    Bijlage XXXVI ...
        [Vervallen]

Als het voor een goed begrip van overgangsrecht van belang is om in de eerste versie al wel het vervallen van het artikel te vermelden dan is dat mogelijk, maar dan moet alsnog de tweede versie gemaakt worden en in het besluit opgenomen worden.

Het is in ieder geval niet mogelijk om bij het noemen van een geldigheidsperiode in een regeling een relatieve periode te hanteren, bijvoorbeeld *na een jaar*. Juridisch gaat zo'n periode in op de inwerkingtredingsdatum van het besluit waarin (de wijziging van) de regeling is opgenomen. STOP is niet ontworpen om die datum bij de tekst van de geconsolideerde regeling te vermelden of om die datum gemakkelijk op te kunnen zoeken. De lezer van de geconsolideerde regeling zal in het algemeen niet kunnen weten wanneer de relatieve periode begint of ophoudt.

## Geldigheidsbepaling opnemen in de regeling

Inwerkingtredingsbepalingen uit het besluit komen niet terug in de geconsolideerde regeling. Een bepaling als "*de wijziging van de regeling als beschreven in bijlage A geldt alleen voor ...*" is in STOP niet toegestaan. Een dergelijke bepaling leidt tot het opsplitsen van de regeling in een regelingversie per doelgroep, waardoor er meerdere regelingversies gelijktijdig geldig zouden zijn. De [geautomatiseerde consolidatie](consolideren_principe.md) van STOP ondersteunt dit niet. 

Geldigheidsbepalingen voor bijvoorbeeld overgangsrecht die van toepassing zijn voor bepaalde doelgroepen moeten daarom niet in het besluit maar in de regeling opgenomen worden.

Bepalingen als

`Deze afdeling is niet van toepassing op degene die een windturbine in werking heeft.`

of 

`1. De regels over een windturbine, bedoeld in de paragrafen 4.30, 4.30a en 4.30b, zijn tot en met 30 juni 2025 van toepassing, als daarvoor uiterlijk op 30 juni 2021 een omgevingsvergunning is verleend.
`

leiden tot één regelingversie die de geldende regels voor alle doelgroepen bevat. Dit is in overeenstemming met [Aanwijzing 5.60 van de Aanwijzingen voor de regelgeving](https://wetten.overheid.nl/BWBR0005730/2022-04-01/#Hoofdstuk5_Paragraaf5.14_Artikel5.60).

## Overgangsrecht via tijdelijk regelingdeel

Als een regeling ingrijpend gewijzigd wordt en deze wijziging wordt geleidelijk ingevoerd, zoals bijvoorbeeld destijds bij de KEI-wetgeving waarbij digitaal procederen voor bepaalde zaken initieel alleen ingevoerd werd bij de rechtbanken Gelderland en Midden-Nederland, kunnen ingewikkelde inwerkingtredingsbepalingen noodzakelijk zijn ([voorbeeld](https://zoek.officielebekendmakingen.nl/stb-2017-16.html)). 

In dergelijke situaties kan overwogen worden om de 'oude' regels als [tijdelijk regelingdeel](regeling_artikelsgewijs_regelingdelen.md) aan de gewijzigde regeling toe te voegen. Waarbij de [conditie](tekst_xsd_Element_tekst_Conditie.dita#Conditie) van het tijdelijk regelingdeel de doelgroep specificeert waarvoor de oorspronkelijke regels van toepassing blijven. Juridisch ontstaat dan één regeling die technisch opgebouwd is uit twee afzonderlijke regelingen die onafhankelijk te muteren zijn. In de loop van de tijd kan de conditie aangepast worden en bij definitieve invoering kan het tijdelijk regelingdeel vervallen. 