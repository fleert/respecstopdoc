# Gebiedsmarkering berekenen

De gebiedsmarkering heeft als doel _bij benadering_ aan te geven op welk gebied een publicatie betrekking heeft. Op basis daarvan worden geïnteresseerden in de buurt van dat gebied geattendeerd op de publicatie. Het is daarom niet nodig om _exact_ uit te rekenen om welk gebied het gaat. Een exact berekend gebied heeft in de praktijk al snel een behoorlijke omvang (groot aantal coördinaat-paren). Een grote geometrie leidt tot een grotere belasting van de publicatiesystemen terwijl het niet bijdraagt aan het beoogde servicegerichte doel. De 
[regeling elektronische publicaties](https://wetten.overheid.nl/jci1.3:c:BWBR0045086&bijlage=1&z=2022-01-01&g=2022-01-01) stelt dat een nauwkeurigheid tot 50 meter voldoende is.

De gebiedsmarkering van een publicatie hangt af van het onderwerp van de publicatie:

- Voor de *bekendmaking van een (ontwerp)besluit*: gebied waar het besluit betrekking op heeft.
- Voor de publicatie van een *rectificatie*: het gebied waar het te rectificeren besluit betrekking op heeft, plus het gebied van het besluit zoals dat na rectificatie luidt.
- Voor een mededeling van een *rechterlijke uitspraak*: het gebied waar het bestreden besluit betrekking op heeft.
- Voor een *kennisgeving over een besluit*, bijvoorbeeld over de inzage-, bezwaar- of beroepstermijn: het gebied waar het besluit betrekking op heeft.
- Voor een *kennisgeving van een voornemen tot besluit*: het gebied waar het voorgenomen besluit naar verwachting betrekking op heeft. Als dat onduidelijk of onzeker is, dan is de gebiedsmarkering het ambtsgebied van het bevoegd gezag.

Hieronder wordt nader ingegaan op de bepaling van het gebied waar een besluit betrekking op heeft. 

## Gebied waar een besluit betrekking op heeft
Een besluit wijzigt één of meer regelingen of stelt die vast. Een goed stappenplan om het gebied te bepalen waar het besluit betrekking op heeft is:

1. Als in een tekst één onderdeel (artikel, divisietekst) toegevoegd, verwijderd of gewijzigd wordt waarvoor de werking via GIO's *niet* tot een specifiek gebied beperkt wordt, dan is het gebied het ambtsgebied van het bevoegd gezag.

2. Anders bestaat het gebied uit de optelsom van de gebieden:
	1. Bepaal om welke gebieden het gaat:
		- Als een tekst (artikel, divisietekst) toegevoegd of verwijderd wordt: het werkingsgebied van de tekst.
		- Als een tekst (artikel, divisietekst) gewijzigd wordt en het geen cosmetische/technische wijziging betreft (zoals hernummering): het deel van het werkingsgebied dat met de wijziging is geassocieerd.
		- Als een GIO vastgesteld of gewijzigd wordt: het gebied waar de wijziging/vaststelling van het GIO betrekking op heeft.
	2. Voeg de gebieden samen tot één gebied.
	3. Als het gebied vrijwel gelijk is aan het ambtsgebied, kies dan het hele ambtsgebied. Een goed criterium hiervoor is als de het gebied gelijk is aan het ambtsgebied met "gaten" die niet groter in doorsnede zijn dan 50 meter.
	4. Vereenvoudig de coördinaten van gebied, rekening houdend met een nauwkeurigheid van maximaal 50 meter. Er is geen "beste manier" om de coördinaten te vereenvoudigen. In verschillende GIS-pakketten en geoprocessing software zijn standaard routines beschikbaar voor de vereenvoudiging van polygonen (als de gebiedsmarkering uit gebieden bestaat) of lijnen. Deze zijn typisch gebaseerd op algoritmen als:
		- Wang-Müller of [Douglas-Peucker](https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm), die het verloop van lijnen vereenvoudigen; 
		- *Snap to grid*, die de punten verplaatsen naar punten op een vast grid en daardoor tot minder (hoek)punten leidt;
		- [*Polygon covering*](https://en.wikipedia.org/wiki/Polygon_covering) met bijvoorbeeld rechthoeken of vierkanten (o.a. gebruikt bij vector tiling); het gebied is dan de omhullende van de vierkanten.

![Simplify en polygon covering van polygoon](img/gebiedsmarkering_simplify.png)

Voorbeeld van [polygoon-simplificatie](https://docs.qgis.org/3.16/en/docs/user_manual/processing_algs/qgis/vectorgeometry.html#simplify) (links) van een gebied (midden), en een vereenvoudiging via *polygon covering* met vierkanten. De afwijking van de eenvoudiger versie ten opzichte van het oorspronkelijke gebied moet minder dan 50 meter zijn, maar mag meer dan 10 meter zijn.

Behalve bij de kennisgeving van het voornemen tot besluit dienen de gebieden gebaseerd te worden op geometrieën in de GIO's. Het is niet de bedoeling om gebieden te gebruiken die ontstaan door interpretatie van teksten maar die niet in een GIO zijn opgenomen. Als in de tekst bijvoorbeeld staat "in het centrumgebied" maar _centrumgebied_ is niet opgenomen in één van de GIO's, dan is het niet de bedoeling om een apart gebied te maken voor het centrumgebied.

## Gebied waar een GIO wijziging/vaststelling betrekking op heeft

Als een GIO in zijn geheel vastgesteld wordt, dan houdt dat in dat ook de gebieden die niet veranderd zijn onderdeel zijn van het besluit. Het hele gebied dat het GIO bestrijkt is dan onderdeel van de gebiedsmarkering.

Meestal wordt het GIO gewijzigd vastgesteld, ook al wordt het GIO om technische redenen in zijn geheel aangeleverd. Net als bij wijziging van tekst dragen een aantal cosmetische/technische wijzigingen niet bij aan de gebiedsmarkering:

- Wijziging van het label van een Locatie.
- Wijziging van de naam van een [GIO-deel](gio-met-delen.md), als de naam ook overal in de tekst is gewijzigd.
- Wijziging van de [norm](gio-met-normwaarden.md) (de identifier van de norm, of wijziging van de eenheid van bijvoorbeeld meter naar millimeter) zolang de nieuwe waarden equivalent zijn aan de oude.
- Samenvoegen van de (ongewijzigde) inhoud van twee GIOs in één GIO, en gelijktijdige aanpassing van de verwijzingen naar de GIO's in de tekst, zolang daardoor de werkingsgebieden van de tekstelementen niet wijzigen.

Wijzigingen die wel consequenties hebben voor de juridische inhoud van de regeling dragen wel bij aan de gebiedsmarkering:

- Als de normwaarde van een Locatie wijzigt, dan draagt de geometrie ervan bij aan de gebiedsmarkering.
- Als de verzameling geometrieën met dezelfde normwaarde wijzigt, dan is het verschil tussen de oude en nieuwe geometrieën onderdeel van de gebiedsmarkering.
- Als de verzameling geometrieën met dezelfde naam van het GIO-deel wijzigt, dan is het verschil tussen de oude en nieuwe geometrieën onderdeel van de gebiedsmarkering.
- Als voor een GIO zonder GIO-delen en zonder normwaarden de verzameling geometrieën wijzigt, dan is het verschil tussen de oude en nieuwe geometrieën onderdeel van de gebiedsmarkering.

Met het verschil tussen de oude en nieuwe geometrieën wordt bedoeld: het verschil tussen de optelsom van alle geometrieën in de voorgaande versie van het GIO (aangeduid met de eigenschap [wasID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVaststellingType.dita#GeoInformatieObjectVaststellingType_wasID)) en de optelsom van alle geometrieën in de nieuwe versie. Het (geautomatiseerd) bepalen van een verschil van geometrieën is lastig. Het grootste probleem is dat gebieden die eigenlijk net niet overlappen of die aan elkaar grenzen, door de inexacte representatie van de gebieden door polygoon-geometrieën toch een verschilgebied opleveren. Deze [_slivers_](https://en.wikipedia.org/wiki/Sliver_polygon) komen als dunne filamenten terug in de verschilgeometrie, maar hebben juridisch geen betekenis.

![Verschilbepaling](img/gebiedsmarkering_verschil.png)

In bovenstaand plaatje is een mogelijke manier toegelicht om het verschil zonder _slivers_ te bepalen. Linkst staat de voorgaande versie (oranje gebied) en nieuwe versie (blauw) die elkaar deels overlappen: er is dus links een gebied bijgekomen en rechtsonder afgegaan. De grenzen van het gebied dat gelijk blijft zijn niet helemaal hetzelfde, maar wijken niet genoeg af om juridisch relevant te zijn.

Een mogelijke manier om het verschilgebied geautomatiseerd te bepalen is om in plaats van de oude en nieuwe gebieden iets grotere/kleinere gebieden te gebruiken door een buffer om een geometrie heen te leggen ("buiten-buffer") of een buffer binnen het gebied te leggen ("binnen-buffer"). De breedte van de buffer hangt af van de nauwkeurigheid van de geometrieën in het GIO, het moet groot genoeg zijn om _slivers_ te vermijden, maar kleiner dan de juridisch relevante details. Voor percelen zou bijvoorbeeld 1 meter gebruikt kunnen worden. De bijdrage aan de gebiedsmarkering is dan:

	verschil (buiten-buffer nieuwe versie, binnen-buffer oude versie) 
		met weglating van buiten-buffer nieuwe versie
	
	plus
	
	verschil (buiten-buffer oude versie, binnen-buffer nieuwe versie) 
		met weglating van buiten-buffer oude versie

De eerste regel komt in het rechter plaatje overeen met het groene gebied rechtsonder, de tweede regel met het groene gebied links. De twee groene gebieden samen kunnen als bijdrage aan de gebiedsmarkering gezien worden. Weliswaar komen de grenzen ervan niet exact overeen met de grenzen van de oude en nieuwe gebieden, maar wel voldoende voor het doel van attendering.