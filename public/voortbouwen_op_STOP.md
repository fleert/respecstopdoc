# Voortbouwen op STOP

Deze leeswijzers beschrijven de eisen waaraan software moet voldoen bij het genereren van entiteiten uit het STOP informatiemodel op basis van andere entiteiten uit dat model.
Doelgroep van deze documentatie zijn architecten, ontwerpers van informatiemodellen en softwareontwikkelaars van het DSO en de LVBB die moeten aansluiten bij STOP in domeinen waar STOP geen invulling aan geeft.

[Juridische verantwoording](juridische_verantwoording.md)
: Beschrijft hoe de juridische verantwoording van een regeling bepaald kan worden op basis van alle besluiten die wijzigingen in de regeling hebben aangebracht.

[Muteren procedureverloop](muteren_procedureverloop.md)
: Beschrijft hoe het initieel, met een besluit, aangeleverde procedureverloop gewijzigd kan worden door kennisgeving(en) en directe mutaties.

Bepaling geconsolideerde regelgeving => is nu [consolidere](consolideren.md)
: Beschrijft in detail op welke manier een systeem besluiten, rectificaties, interventies etc. kan verwerken om tot de toestand van (proef)consolidaties te komen.

[Synchronisatie van regelgeving-gerelateerde informatie](consolideren_annotaties.md)
: Beschrijft hoe informatie die niet via STOP is gemodelleerd toch synchroon gehouden kan worden met de geconsolideerde regelgeving die de juridische bron vormt van die informatie of die eraan gerelateerd is.

Gebruik van niet door STOP ondersteunde besluittypen en procedures - niet meer ondersteunen
: Beschrijft hoe STOP toch gebruikt kan worden voor het uitwisselen van besluiten binnen een besluitvormingsproces dat niet door STOP zelf ondersteund wordt.