# Relatie met andere standaarden

STOP maakt waar mogelijk gebruik van andere standaarden in plaats van zelf het wiel uit te vinden. Op het gebied van informatiemodellering maakt STOP gebruik van:

Akoma Ntoso naamgevingsconventie
: De Akoma Ntoso (AKN) standaard, ook bekend als OASIS [LegalDocML](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=legaldocml), is de standaard die het dichtst komt bij de eisen van STOP waar het gaat om werken met juridische teksten. De AKN standaard bestaat uit twee delen:

  * Een [naamgevingsconventie](naamgevingsconventie.md) die in STOP is overgenomen en is ingevuld voor toepassing in Nederland en die als voorbeeld is gebruikt voor de [JOIN identificatie](naamgevingsconventie.md#join) voor niet-tekstuele informatie.
  * Een [vocabulaire](https://docs.oasis-open.org/legaldocml/akn-core/v1.0/akn-core-v1.0-part1-vocabulary.html) van XML elementen om teksten te coderen. Het bleek lastig om deze elementen zonder meer toe te passen voor Nederlandse juridische teksten. In plaats daarvan hanteert STOP eigen tekstmodellen waarvan de elementen corresponderen met AKN elementen met een vergelijkbare functie. De relatie is terug te vinden in de [element_ref](eid_wid.md#akn) component van de identificatie van tekstelementen.

GML (via basisgeometrie) voor geografie
: Voor het vastleggen van geografie is in STOP gekozen voor een algemeen gangbaar formaat dat bovendien duurzaam is. De duurzaamheid is een eis die voortkomt uit het gebruik van STOP voor officiële publicaties die ook over zeer lange tijd nog te lezen moeten zijn. OGC standaard [Geography Markup Language](https://www.ogc.org/standards/gml) (GML) is daarvoor de aangewezen standaard, die ook op de Nederlandse [pas-to-of-leg-uit lijst](https://www.forumstandaardisatie.nl/open-standaarden/verplicht) staat. STOP bouwt voort op een specifieke vorm daarvan ([Basisgeometrie](https://docs.geostandaarden.nl/nen3610/basisgeometrie/)) die gedeeld wordt met de [IMOW-standaard](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD).

Symbol Encoding (SE) voor symbolisatie
: Indien eigenschappen worden toegevoegd aan geometrie is het nuttig om deze eigenschappen altijd op dezelfde manier te verbeelden. Indien bijvoorbeeld een normwaarde van toepassing is op een perceel, dan is het prettig als alle percelen met dezelfde waarde, dezelfde kleur hebben. Voor het coderen van kleuren, lijndikte, etc. maakt STOP gebruik van een [subset](EA_77AF770705D4439c946D8E821BC18F18.dita#Pkg) van de [Symbol Encoding](https://www.ogc.org/standards/se) standaard.  

Formaten uit de Bekendmakingswet
: Voor vormen van informatie waar STOP geen eigen model biedt wordt de bekendmakingswet en bijbehorende regelgeving gevolgd. Die schrijft voor:

  * Documenten moeten in [PDF](https://wetten.overheid.nl/jci1.3:c:BWBR0045086&hoofdstuk=2&artikel=2.2&z=2022-01-01&g=2022-01-01) opgesteld zijn.
  * Afbeeldingen moeten in PNG of JPG aangeleverd worden (voor de verdere eisen zie [agInlineTekstAfbeelding](tekst_xsd_Attribute_Group_tekst_agInlineTekstAfbeelding.dita#agInlineTekstAfbeelding)).
