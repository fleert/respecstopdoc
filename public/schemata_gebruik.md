# Gebruik van IMOP

IMOP biedt technische hulpmiddelen aan om STOP-informatie als modules in XML te serialiseren en om XML serialisaties te valideren. Voor een specifieke versie van IMOP gaat het om:

- XML-schema's (XSD's) die de structuur van de IMOP modules beschrijven.
- Schematrons die de inhoud van de IMOP-modules daarbovenop kunnen valideren. Niet alle bedrijfsregels die STOP beschrijft kunnen in een schematron uitgedrukt worden.
- Transformaties om de XML-serialisatie van/naar een vorige versie van IMOP om te zetten. Transformaties worden alleen aangeboden als de standaard (informatiemodel en/of bedrijfsregels) gewijzigd is in deze versie van de standaard, en alleen voor een selectie van de modules.

Daarnaast is een cumulatief overzicht van versies van IMOP-modules beschikbaar dat aangeeft welke onderdelen van welke versie van IMOP van toepassing zijn voor een XML-module.

In deze sectie wordt het gebruik van de hulpmiddelen beschreven.

[IMOP op het internet](schemata_gebruik_url.md)
: Alle technische hulpmiddelen hebben een vast internetadres en kunnen zo door alle XML-verwerkende software gebruikt worden.

[IMOP downloaden](schemata_gebruik_lokaal.md)
: Het is ook mogelijk alle technische hulpmiddelen uit een git-repository te downloaden. IMOP volgt een XML-standaard om een overzicht te maken die het verband aangeeft tussen de locatie op internet en een bestand in de kloon/download van de git-repository te beschrijven.

[Versionering van modules](schemata_gebruik_versionering.md)
: Een XML-serialisatie van een IMOP-module is opgesteld voor een specifieke versie van IMOP. Er is een cumulatief overzicht van versies van IMOP-modules beschikbaar dat aangeeft welke onderdelen van welke versie van IMOP van toepassing zijn voor een XML module. Dit overzicht is bedoeld om te achterhalen hoe een XML-module gevalideerd moet worden of omgezet kan worden naar een andere versie van IMOP.

[Schematrons](schemata_gebruik_schematrons.md)
: IMOP biedt schematrons om de XML-serialisatie te valideren. Omdat niet alle XML-verwerkende software met schematrons overweg kan, wordt als service ook een alternatieve implementatie van de schematrons aangeboden. Als de schematrons worden uitgevoerd, dan bestaat de uitkomst uit nul of meer [JSON](https://www.json.org/)-objecten.

[Transformaties](schemata_gebruik_transformaties.md)
: Voor een selectie van de modules wordt een transformatie aangeboden die de XML-serialisatie kan converteren van/naar een eerdere versie van de standaard. In deze sectie wordt beschreven hoe de transformaties uitgevoerd moeten worden en wat het resultaat van een transformatie kan zijn.