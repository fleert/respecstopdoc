# Eén of meer doelen/branches

Een [doel](begrippenlijst_doel.dita#doel) markeert één moment in de tijd waarop nieuwe regelgeving inwerking zal treden. Een doel verbindt de regelingversie(s) en bijbehorende informatieobjectversies aan een inwerkingtredingsdatum voordat de daadwerkelijke inwerkingtredingsdatum bekend is. Zoals [toegelicht](versiebeheer_principes.md) correspondeert een doel met een bramch in versiebeheer.

De manier waarop een bevoegd gezag omgaat met doelen/branches in een project zal erg afhangen van het type bevoegd gezag, het type regeling en de aard van de wijziging waarvoor het project wordt ingericht. Een deel van de keuzes heeft te maken met de interne werkwijze, bijvoorbeeld hoeveel en welke organisatieonderdelen betrokken zijn. Maar in elke situatie moet bij het opstellen van nieuwe regelingversies al bij de start rekening gehouden worden met het verdere verloop van het project en (voor zover bekend) de volgorde van inwerkingtreding. 

Ter illustratie zijn hier een aantal praktijk voorbeelden gegeven van welke keuzes voor [doel](begrippenlijst_doel.dita#doel)/[branch](begrippenlijst_branch.dita#branch) gemaakt kunnen worden.

## Basisscenario: één project { #Basisscenario }
![Basisscenario](img/versiebeheer-doel-basis.png)

Het basisscenario is dat het bevoegd gezag een besluit voorbereidt dat leidt tot een nieuwe versie van een regeling gebaseerd op de voorgaande versie. De nieuwe versie wordt voor één doel (A) gemaakt, met als basisversie de [branch](begrippenlijst_branch.dita#branch) van de versie waarvan verwacht wordt dat geldig is voorafgaand aan datum 1. Dat mag een versie zijn die nog niet bekendgemaakt is, zolang het zeker is dat die versie ingesteld wordt door een besluit dat bekendgemaakt gaat worden voorafgaand aan dit besluit. Meestal zal de voorgaande versie de geldende of een bekendgemaakte toekomstig geldende versie zijn.

## Onafhankelijke projecten{ #Onafhankelijk }
![Onafhankelijke wijzigingstrajecten](img/versiebeheer-doel-onafhankelijk.png)

Als apart besloten zal worden over twee projecten die elk wijzigingen in een regeling aanbrengen die elkaar inhoudelijk niet raken gebruik dan aparte doelen/branches voor elk separaat wijzigingstraject. De volgorde van inwerkingtreding maakt dan niet uit. Kies ook voor twee aparte doelen/braches als de besluitvorming over beide wijzigingen waarschijnlijk op hetzelfde moment plaats zal vinden. Hiervoor geldt namelijk: in een later stadium [samenvoegen](consolideren_patronen_1samenvoeging.md) of [tegelijk in werking](consolideren_patronen_2gelijktijdig.md) laten treden van twee besluiten is eenvoudiger dan het later uit elkaar halen van wijzigingen die toch [niet](gedeeltelijke-inwerkingtreding.md) op hetzelfde moment in werking treden. 

## Afhankelijke projecten{ #Afhankelijk }
![Afhankelijke wijzigingstrajecten](img/versiebeheer-doel-afhankelijk.png)

Als apart besloten zal worden over twee projecten waarbij het ene besluit op het andere voortbouwt, worden ook weer aparte doelen/branches voor elke separaat wijzigingstraject gebruikt. Maar nu moet het ene project een branch zijn van de andere. Vlak voordat het tweede besluit geformuleerd wordt (doel B in de illustratie), worden de wijzigingen van doel A overgenomen. Deze situatie treedt bijvoorbeeld op bij  gebiedsontwikkeling (doel A) en een eerste project (doel B) in dat gebied.

## Conditionele wijziging { #Conditoneel }
![Conditionele wijziging](img/versiebeheer-doel-conditioneel.png)

In deze situatie zijn er twee projecten, waarbij van één project(besluit 2) al voldoende bekend is, waar in het andere project(besluit 1) al rekening mee gehouden moet worden. In project 1 worden dan twee versies ontwikkeld: één voordat project 2 inwerking treedt(doel A) en de andere(doel C) treedt tegelijk met project B in werking. 

Het ene besluit (besluit 2 in de illustratie) bevat de wijzigingen voor een van de doelen (in de illustratie B). Het andere besluit bevat twee wijzigingen: de wijzigingen voor doel A die onafhankelijk zijn van de wijzigingen voor doel B, en wijzigingen voor doel C die wel afhankelijk zijn van doel B. In het besluit wordt beschreven dat de wijzigingen voor doel C tegelijk met de wijzigingen voor doel B in werking treden (zie de [inwerkingtredingsbepalingen](besluit_iwtbepalingen.md)).

Dit scenario is alleen mogelijk als besluit 2 eerder bekendgemaakt kan worden dan besluit 1. Het wordt toegepast als bij het opstellen van besluit 1 nog niet bekend is welke van de besluiten eerder in werking zal treden.

## Eén project, twee inwerkingtredingsdatums { #Gefaseerd }
![Twee inwerkingtredingsdatums](img/versiebeheer-doel-gefaseerd.png)

Als van tevoren bekend of aannemelijk is dat een deel van de nieuwe versie/wijziging op een later moment in werking treedt, splits dan de wijziging en gebruik twee doelen/branches. Het deel dat als eerste in werking treedt (doel A) is dan de basisversie voor het tweede deel (doel B). Het is namelijk eenvoudiger om twee wijzigingen te combineren dan om een wijziging te splitsen. Dat zit niet zozeer in de benodigde aanpassingen in de tekst van een regeling en wellicht ook niet in de aanpassing van de informatieobjecten, maar in de afgeleide informatie zoals annotaties of toepasbare regels. Die is gebaseerd op een beoordeling van de integrale versie, en een wijziging van een enkel artikel kan een veel groter effect hebben dan alleen op de annotaties behorend bij dat artikel. Bij het splitsen van wijzigingen moet van elke verandering in annotaties nagegaan worden bij welk deel van de wijzigingen het hoort, wat een inhoudelijke analyse is. Bij samenvoegen hoeft dat niet, het is veel meer een technische operatie.

## Opvolgende zekere wijzigingen: 'treintje' { #Treintje }
![Opvolgende zekere wijzigingen](img/versiebeheer-doel-treintje.png)

Als binnen een project apart besloten moet worden op de instelling of wijziging omdat de besluiten door andere bestuursorganen of via andere procedures vastgesteld moeten worden, maar het is zeker dat alle besluiten tegelijk in werking zullen treden, gebruik dan één doel. Dit patroon komt ook voor als er eerst op hoofdlijnen besloten wordt en de nieuwe regelingversie later aangevuld wordt. Er mag geen beroep mogelijk zijn tegen de besluiten. Het gebruik van één doel heeft als voordeel dat volgende besluiten voortbouwen op eerdere versies, zodat al gedurende het proces inzicht ontstaat hoe de geconsolideerde regeling eruit gaat zien. Deze aanpak wordt ook wel een treintje genoemd: elke versie is gebaseerd op de vorige versie binnen hetzelfde doel.

## Opvolgende wijzigingen: 'waaier' { #Waaier }
![Opvolgende onzekere wijzigingen](img/versiebeheer-doel-waaier.png)

Als het binnen een project niet zeker is of een deel van de instelling of wijziging met succes door het besluitvormingsproces komen, gebruik dan meerdere besluiten en aparte doelen. Ook in dit geval geldt: gesplitst opstellen en later (bij inwerkingtreding) [samenvoegen](consolideren_patronen_1samenvoeging.md) is gemakkelijker dan achteraf splitsen. De versie met de minst controversiële onderdelen/wijzigingen kunnen als basisversie voor de aanvullingen genomen worden. Deze aanpak wordt ook wel een waaier genoemd: elke versie is gebaseerd op dezelfde basisversie.