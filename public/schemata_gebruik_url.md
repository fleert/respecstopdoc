# IMOP op het internet 

## URL { #url }
Elk schema, schematron of transformatie die onderdeel is van een specifieke versie van IMOP heeft een URL die voldoet aan het volgende patroon:
```
"https://standaarden.overheid.nl/stop/" <IMOP-versie> "/" <bestandsnaam> 
```

* `https://standaarden.overheid.nl/stop/` is het domein waar de schema's beschikbaar worden gesteld. Software die de URL gebruikt voor het downloaden van een bestand moet er rekening mee houden dat de hulpmiddelen op een andere locatie beschikbaar gesteld worden en dat bij het opvragen van deze URL een redirect naar de andere locatie kan plaatsvinden.
* `<IMOP-versie>` is het versienummer van IMOP.
* `<bestandsnaam>` is de bestandsnaam van het schema, schematron of transformatie. 

Bijvoorbeeld
* `https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-data.xsd` verwijst naar versie @@@IMOPVERSIE@@@ van het data-schema.
* `https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-aknjoin.sch` is de schematron waarin de bedrijfsregels rondom de naamgevingsconventie zijn opgenomen.  
* `https://standaarden.overheid.nl/stop/@@@IMOPVERSIE@@@/imop-aknjoin.xslt` is de naar Saxon-XSLT vertaalde schematron waarin de bedrijfsregels rondom de naamgevingsconventie zijn opgenomen.  

Schema's waarvan IMOP gebruik maakt maar die geen onderdeel zijn van STOP maar van een andere standaard worden niet via `https://standaarden.overheid.nl/stop/` beschikbaar gesteld maar via de URL zoals door de andere standaard is voorgeschreven. Een overzicht van de door STOP gebruikte schema's staat op [Schemadocumentatie](schema_documentatie.md).

## Schema's en bestandsnamen

Elke IMOP module wordt gedefinieerd binnen één namespace. Het volledige schema voor een IMOP-namespace is ondergebracht in één XML-Schemadocument. De relatie tussen namespace en de bestandsnaam van het XML-Schema document: 

|Namespace|Bestandsnaam|
|---|---|
|[https://standaarden.overheid.nl/stop/imop/businessrules/](br_xsd_Main_schema_br_xsd.dita#br.xsd)|imop-businessrule.xsd|
|[https://standaarden.overheid.nl/stop/imop/consolidatie/](cons_xsd_Main_schema_cons_xsd.dita#cons.xsd)|imop-cons.xsd|
|[https://standaarden.overheid.nl/stop/imop/data/](data_xsd_Main_schema_data_xsd.dita#data.xsd)|imop-data.xsd|
|[https://standaarden.overheid.nl/stop/imop/geo/](geo_xsd_Main_schema_geo_xsd.dita#geo.xsd)|imop-geo.xsd|
|[https://standaarden.overheid.nl/stop/imop/gio/](gio_xsd_Main_schema_gio_xsd.dita#gio.xsd)|imop-gio.xsd|
|[https://standaarden.overheid.nl/stop/imop/resources/](rsc_xsd_Main_schema_rsc_xsd.dita#rsc.xsd)|imop-resources.xsd|
|[https://standaarden.overheid.nl/stop/imop/schemata/](sv_xsd_Main_schema_sv_xsd.dita#sv.xsd)|imop-schemata.xsd|
|[https://standaarden.overheid.nl/stop/imop/tekst/](tekst_xsd_Main_schema_tekst_xsd.dita#tekst.xsd)|imop-tekst.xsd|
|http://www.opengis.net/se *| se-FeatureStyle-imop.xsd|
|http://www.opengis.net/ogc *| se-filter-imop.xsd|

* IMOP biedt voor twee OpenGIS namespaces alternatieve schema's aan. Deze schema's bevatten alleen de elementen die binnen IMOP zijn toegestaan. De schema's hebben als voordeel ten opzichte van de officiële OpenGIS schema's dat ze geen gebruik maken van de GML schema's. 
