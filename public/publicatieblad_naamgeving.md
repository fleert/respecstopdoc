# Identificatie van publicatie

Een publicatie krijgt twee identificaties:

* één voor de publicatie als work en
* één voor de gepubliceerde versie (een expression).

STOP gebruikt een [invulling](naamgevingsconventie.md) van de Akoma Ntoso naamgevingsconventie (AKN NC) als basis voor de identificatie. AKN NC schrijft de structuur van de identificatie voor, STOP vult dat nader in.

Voor publicaties wordt in de identifcatie "**officalGazette**" gebruikt:

**Work**: `"/akn/" <land> "/officialGazette/" <blad> "/" <datum> "/" <nummer>`

**Expression**: `<work> "/" <taal> "@" <datum_bekendmaking> [ ; <herdruk> ]`

met

|code               |betekenis|
|---                |---      |
|blad               | Afkorting van het publicatieblad, zie hieronder. |
|datum              | Het jaar waarin de publicatie is verschenen.|
|nummer             | Het nummer van de publicatie volgens de nummeringsmethode van het betreffende blad.|
|taal | Taalcode volgens [ISO 639-2 alpha-3](https://www.iso.org/iso-639-language-codes.html). In versie @@@IMOPVERSIE@@@ van de standaard wordt alleen `nld` ondersteund. |
|datum_bekendmaking | De dag van de bekendmaking van betreffende publicatie, in YYYY-MM-DD. Bij een herdruk is dit de _datum van "herdruk"_ |
|herdruk            | Een optioneel nummer dat aangeeft dat het om een herdruk van de originele publicatie gaat, volgens de nummeringsmethode van het betreffende blad.|

De afkorting van het publicatieblad wordt gegeven door:

|code  |betekenis|
|---  |---      |
|bgr  |Blad gemeenschappelijke regeling|
|gmb  |Gemeenteblad|
|prb  |Provinciaalblad|
|stb  |Staatsblad|
|stcrt|Staatscourant|
|trb  |Tractatenblad|
|wsb  |Waterschapsblad|

Voorbeelden

* `/akn/nl/officialGazette/stcrt/2018/48231/nld@2018-08-28`: Staatscourant 2018, 48231
* `/akn/nl/officialGazette/stcrt/2018/48231/nld@2018-08-31;n2`: tweede "herdruk" van bovengenoemde Staatscourant 

Uit de identificatie van de publicatie is niet af te leiden welke documenten (besluiten) gepubliceerd worden; die informatie is onderdeel van de metadata van de publicatie en/of over de gepubliceerde documenten.

