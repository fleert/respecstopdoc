# 2. Koppel in de bijlage label aan JOIN-ID

Om te weten wat er met het GIO-label uit stap 1 wordt bedoeld, wordt in de regeling een bijlage opgenomen met een overzicht van de gebruikte labels en de JOIN-ID van de bijbehorende GIO's. Dit zorgt ervoor dat de lopende tekst goed leesbaar blijft en de JOIN-ID (waarmee de inhoud van het GIO altijd is te vinden)  zichtbaar is.

## Voorbeeld-bijlage
Voor een praktijkvoorbeeld van een bijlage met JOIN-ID’s, zie [Regeling van de Minister voor Milieu en Wonen, de Staatssecretaris van Defensie, de Minister van Economische Zaken en Klimaat, de Minister van Infrastructuur en Waterstaat, de Minister van Landbouw, Natuur en Voedselkwaliteit en de Minister van Onderwijs, Cultuur en Wetenschap van 21 november 2019, houdende regels over het beschermen en benutten van de fysieke leefomgeving (Omgevingsregeling)](https://zoek.officielebekendmakingen.nl/stcrt-2019-56288.html#d17e23557). 
 
De bijlage waar de verwijzing naar de JOIN-ID staat kan er als volgt uitzien:

------

> **BIJLAGE A OVERZICHT INFORMATIEOBJECTEN** 
> In deze regeling worden de volgende informatieobjecten gebruikt.
>
> | Aanduiding               | Join                                                         |
> | ------------------------ | ------------------------------------------------------------ |
> | Militaire oefenterreinen | /join/id/regdata/mnre1034/2019/militaire_oefenterreinen/@2019-10-04 |

------

## Gewijzigde GIO: wijzig JOIN-ID

Bij een nieuwe GIO-versie wijzig je in deze stap het JOIN-ID van het GIO in de tekstbijlage.

## Geen GIO-delen in bijlage
GIO-delen worden **niet** vermeld in de bijlage van de regeling. Alleen het GIO als geheel staat in de bijlage.

Verwijzen naar een GIO-deel van een GIO werkt hetzelfde als verwijzen naar een artikel van een regeling.


## JOIN-ID is ExtIORef
De `ExtIORef` wordt gebruikt om vanuit de regeling te verwijzen naar het hele GIO, een object dat extern is ten opzichte van de regeling. De JOIN-ID van de informatieobjecten-bijlage wordt vormgegeven als `ExtIORef`.
