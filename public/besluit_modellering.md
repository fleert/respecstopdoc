# Modellering van het besluit

Zoals bij de [uitgangspunten](automatiseerbaar-creatieproces.md) is beschreven wordt een besluit gemodelleerd als een "container" die de getransformeerde regelingversies en eventueel bijbehorende informatieobjectversies plus de inwerkingtredingsbepalingen bevat. De "container" bestaat uit onderdelen die alleen voor het besluitvormingsproces nodig zijn.

![Model van het besluit](img/Besluit-Model.png)

De onderdelen zijn:

Tekst van het besluit ([standaard](besluit_compact.md) of [klassiek](besluit-klassiek.md))
: Het besluit wordt beschreven in tekst, op een manier die vergelijkbaar is als de [tekst van een regeling](regeltekst.md). Het tekstmodel is gebaseerd op artikelgewijze tekst. In de hoofdtekst staat beschreven wat het besluit inhoudt, inclusief (de wijziging van) de tekst van een regeling. Daarnaast kan een besluit bijlagen hebben. STOP kent een [standaard model](besluit_compact.md) voor de tekst van het besluit. Voor sommige Rijksbesluiten wordt het [klassieke model](besluit-klassiek.md) gebruikt; in een [toepassingsprofiel](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) staat aangegeven wanneer het klassieke model gebruikt moet worden.

(Wijziging van) de tekst van een regeling als [tekstcomponent](identificatie_tekstonderdelen.md)
: In de hoofdtekst van een besluit zijn containers opgenomen om de eerste versie van de tekst van een regeling of om een wijziging daarvan op te nemen. Deze containers worden gemodelleerd als componenten waarvoor de naamgevingsconventie van toepassing is. Dat maakt het mogelijk te verwijzen naar tekst binnen de component. Het beschrijven van een tekstwijziging in renvooi staat [elders](renvooitekst.md) beschreven. De nieuwe (`wordt`-)versie van de regeling moet vermeld worden als [BeoogdeRegeling](data_xsd_Element_data_BeoogdeRegeling.dita#BeoogdeRegeling) in de [consolidatie-informatie](consolidatie-informatie.md); bij een wijziging moet de renvooitekst zijn gebaseerd op een oude (`was`-)versie die overeenkomt met een van de `Basisversie` elementen.

Toelichting of motivering
: Onderdeel van de tekst van een besluit is een motivering of een toelichting die het hoe en waarom van een besluit beschrijft ([elders](besluit_toelichting.md) beschreven). Een toelichting kan vergezeld gaan van een artikelsgewijze toelichting (al dan niet met [Toelichtingsrelatie](data_xsd_Element_data_Toelichtingsrelatie.dita#Toelichtingsrelatie) annotatie) die de wijzigingen in een regeling in detail toelicht. De toelichting bij een besluit heeft dus een andere rol dan de toelichting bij een regeling die de inhoud van de regeling toelicht en onderdeel is van de regelingtekst/wijziging in renvooi.

Informatieobjecten
: Nieuwe versies van informatieobjecten zijn in getransformeerde vorm ([elders](besluit_informatieobject.md) beschreven) onderdeel van het besluit. De informatieobjecten moeten in de [metadata](besluit_metadata.md) worden vermeld. De nieuwe (`wordt`-)versie van het informatieobject moet vermeld worden als [BeoogdInformatieobject](data_xsd_Element_data_BeoogdInformatieobject.dita#BeoogdInformatieobject) in de [consolidatie-informatie](consolidatie-informatie.md); bij een wijziging moet de vorige (`was`-)versie (indien [aanwezig](besluit_informatieobject.md)) overeenkomen met een van de `Basisversie` elementen.

Onderzoeksrapporten en andere [documenten](IO_Document.md)
: Voor sommige type besluiten bestaan er wettelijke verplichtingen om bij de bekendmaking van een besluit documenten bij te sluiten ter onderbouwing van het besluit. Het gaat dan niet om een motivering of toelichting, want die is onderdeel van de tekst van het besluit en moet volgens het STOP tekstmodel worden gecodeerd. Wel om bijvoorbeeld onderzoeksrapporten (zoals een milieueffectrapportage) die vaak door derden worden opgesteld. Het is niet nodig deze rapporten om te zetten in een XML-gecodeerde tekst. De documenten kunnen als PDF-informatieobject worden bijgesloten. Deze informatieobjecten moeten ook in de [metadata](besluit_metadata.md) vermeld worden.

[Metadata](besluit_metadata.md) 
: Bij elk besluit hoort metadata die de globale kenmerken van het besluit bevat.

Consolidatie-informatie
: Als annotatie bij het besluit moet aangegeven worden hoe de nieuwe regeling- en informatieobjectversies passen binnen de geldende regeling, en wanneer deze juridische werking krijgen. Dit wordt vastgelegd in de (elders beschreven) [consolidatie-informatie](consolidatie-informatie.md) module.

Voor de publicatie van een besluit is aanvullend nodig (en elders beschreven):

Gebiedsmarkering/effectgebied
: Geeft aan in welk gebied het besluit relevant is. Dit gebied wordt gebruikt om belanghebbenden en geïnteresseerden te [attenderen](attenderen.md) op de publicatie van het besluit.