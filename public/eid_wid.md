# Identificatie van tekstelementen

## Principe

Elk element uit de STOP tekstmodellen heeft een eigen identificatie. Die bestaat uit twee identifiers, in navolging van de [Akoma Ntoso naamgevingsconventie](https://docs.oasis-open.org/legaldocml/akn-nc/v1.0/os/akn-nc-v1.0-os.html) zoals overgenomen in [STOP](naamgevingsconventie.md):

* een **eId** (_expression identifier_) die gerelateerd is aan de positie van het element binnen de tekst.
* een **wId** (_work identifier_) die afgeleid wordt van de initiële _eId_ maar die nooit verandert.

De identifiers dienen ieder een eigen doel.

### eId

De _eId_ is bedoeld om een verwijzing te kunnen maken naar een positie in de tekst vanuit een andere tekst. Een _eId_ bestaat uit twee delen: een _lokale_id_ die gebaseerd is op het type tekstelement en het nummer daarvan (bijvoorbeeld _art_1_ voor artikel 1), en een _prefix_ die gelijk is aan de _eId_ van het container-tekstelement waarin het tekstelement bevat is (bijvoorbeeld van de paragraaf waar het artikel in voorkomt). Dat is om een aantal redenen gedaan.

1. Als vanuit een juridische tekst naar "het vijfde artikel" in een andere tekst verwezen wordt, dan moet de verwijzing ook bij artikel 5. uitkomen. Het is dan niet van belang of dat hetzelfde artikel is waarnaar bij het opschrijven van de verwijzing verwezen is. Dus als een verwijziging naar "het vijfde artikel" opgeschreven wordt, en er komt in de andere tekst later een nieuw artikel 5 en het oude artikel 5 wordt hernummerd naar artikel 6, dan moet de verwijzing nog steeds bij artikel 5 uitkomen.

2. Een geoefend mens kan aan de _eId_ in een verwijzing aflezen waar in de tekst de verwijzing uit moet komen. Een verwijzing naar een _eId_ is daarom nog steeds bruikbaar, ook al zouden in de weergave van de tekst de _eId_ van de tekstelementen niet zichtbaar zijn, of als de weergevende software verwijzingen met _eId_ niet ondersteunt.

3. In juridische teksten komen vaak verwijzingen voor naar teksten van hogere wet- en regelgeving. Die kan in de loop van de tijd veranderen, waarbij niet meteen alle teksten bijgewerkt zullen zijn die ernaar verwijzen. De AKN specificaties voor een _eId_ zijn zo opgesteld dat de verwijzing in dat geval vaak alsnog op de juiste plaats in de tekst kan uitkomen. Als de software die de tekst weergeeft waarnaar verwezen wordt het element met de _eId_ niet meer kan vinden, dan kan in plaats daarvan de prefix van de _eId_ gebruikt worden. De verwijzing komt dan niet exact op de juiste plaats uit, maar waarschijnlijk wel in de buurt.

Om deze redenen is de _eId_ gerelateerd aan de plaats van het tekstelement in de tekst, en is het geen onveranderlijke eigenschap van het element. Zeker na structuurwijzigingen moet de _eId_ van een tekstelement voor elke volgende versie van de tekst opnieuw gegenereerd worden.

### wId

De _wId_ is bedoeld om een verwijzing te kunnen maken naar de tekst waarin een bepaalde inhoud te vinden is, bijvoorbeeld een specifiek voorschrift of beleidsregel. De _wId_ wordt daarom gebruikt in annotaties om een duiding of interpretatie van de inhoud van de tekst te kunnen relateren aan de tekst. Aan de _wId_ worden de eisen gesteld:

* Dezelfde inhoud moet via de _wId_ zijn terug te vinden in alle versies van de tekst.
* De tekst in het tekstelement mag in verschillende versies anders zijn; dit wordt gezien als een evolutie van dezelfde inhoud (voorschrift, beleidsregel) in de tijd.
* Pas als de inhoud van een tekstelement in een volgende versie echt niet meer overeenkomt (zoals: het voorschrift gaat over iets totaal anders) krijgt een tekstelement een ander _wId_.
* Als een bepaalde versie van de tekst geen element met dat _wId_ heeft, dan is de inhoud waarnaar verwezen wordt niet aanwezig in die versie.

Het is daarom belangrijk dat de _wId_ uniek is over alle versies van de tekst heen. Om dat te garanderen schrijft STOP het gebruik van een versie-specifieke prefix voor bij introductie van nieuwe elementen.

Om een _wId_ gelijk te houden in volgende versies van de tekst moet de ondersteunende software bijhouden hoe de verschillende tekstfragmenten door de schrijver worden verplaatst en hergebruikt, en wanneer de inhoud zodanig verandert dat een nieuwe _wId_ nodig is. Dat is geen eenvoudige functionaliteit. Daarom stelt STOP het gelijk houden van de _wId_ alleen verplicht voor [structuur- en inhoudelementen en regeltekst](regeltekst.md). Als software niet in staat is om voor de andere elementen, zoals inline- of aantekening-elementen, de _wId_ over versies heen constant te houden, dan mag de _wId_ <u>uitsluitend</u> voor die elementen elke versie opnieuw toegekend worden.

### Componenten
De regels voor *eId* en *wId* gelden binnen een AKN component. Conform AKN is de hoofdtekst van een regeling, besluit etc. een component. In sommige gevallen komen binnen de hoofdtekst onderdelen voor die in STOP als [afzonderlijke component](identificatie_tekstonderdelen.md) gezien worden. De tekst in die component wordt niet gezien als onderdeel van de hoofdtekst-component. De regels voor *eId* en *wId* (zoals uniciteit) gelden dan alleen binnen de hoofdtekst-zonder-componenten en afzonderlijk binnen elk van de componenten.

Het is mogelijk te verwijzen naar tekst in een component; dat is [elders](identificatie_tekstonderdelen.md#Verwijzen) beschreven.

## Elementen met vaste *eId* en *wId*

Een aantal elementen heeft een vaste, onveranderlijke *eId* en een daaraan identieke *wId* zodat naar deze elementen verwezen kan worden zonder de verdere details van de tekst te kennen:

| elementen | eId = wId |
|----     | ----      |
|[Aanhef](tekst_xsd_Element_tekst_Aanhef.dita#Aanhef)|formula_1|
|[AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting)|genrecital|
|[ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting)|artrecital|
|[Inhoudsopgave](tekst_xsd_Element_tekst_Inhoudsopgave.dita#Inhoudsopgave)|toc|
|[Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam)|body|
|[Motivering](tekst_xsd_Element_tekst_Motivering.dita#Motivering)|acc|
|[RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift)|longTitle|
|[Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting)|formula_2 *of* formula_2_inst[volgnummer]|
|[Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting)|recital|

==**NB**== Indien het element *Sluiting* vaker dan 1 keer voorkomt (in de hoofdtekst of in een [component](identificatie_tekstonderdelen.md)) dan wordt een "*_instX*" aan de waarde van zowel de eId als de wId toegevoegd als: "*_inst[volgnummer]*" waarbij volgnummer begint bij 2.

Voor de overige elementen is de *eId* afgeleid van de positie van het element binnen de tekst van een component. De *eId* wordt dan als volgt samengesteld: 

```
eId = ([prefix]__)[element_ref](_[nummer])
```

Hierbij geldt:

| onderdeel | betekenis |
|----       | ----      |
| `[prefix]`  | de prefix bevat de `eId` van het hoger liggende XML-element in de tekststructuur. Als het hoger liggende XML element het [`tekst:Lichaam`](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) is, dan wordt de prefix met de daarop volgende dubbele underscore (`__`) weggelaten. |
| `[element_ref]` | Een afkorting van de naam van het xml-element, zoals voorgeschreven door de standaard. Zie onderstaande tabel. |
| `[nummer]` | Een volgnummer voor het element, moet waar mogelijk afgeleid worden van het nummer van het element dat in de tekst voorkomt. Zie toelichting onder de tabel. |


### `[element_ref]` { #akn }

| element | afkorting |
|----     | ----      |
|[Afdeling](tekst_xsd_Element_tekst_Afdeling.dita#Afdeling)|subchp|
|[Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel)|art|
|[Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip)|item|
|[Begrippenlijst](tekst_xsd_Element_tekst_Begrippenlijst.dita#Begrippenlijst)|list|
|[Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)|cmp|
|[Boek](tekst_xsd_Element_tekst_Boek.dita#Boek)|book|
|[Citaat](tekst_xsd_Element_tekst_Citaat.dita#Citaat)|cit|
|[Deel](tekst_xsd_Element_tekst_Deel.dita#Deel)|part|
|[Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie)|div|
|[Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst)|content|
|[Deel](tekst_xsd_Element_tekst_Deel.dita#Deel)|part|
|[ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef)|ref|
|[Figuur](tekst_xsd_Element_tekst_Figuur.dita#Figuur)|img|
|[Formule](tekst_xsd_Element_tekst_Formule.dita#Formule)|math|
|[Hoofdstuk](tekst_xsd_Element_tekst_Hoofdstuk.dita#Hoofdstuk)|chp|
|[InleidendeTekst](tekst_xsd_Element_tekst_InleidendeTekst.dita#InleidendeTekst)|intro|
|[IntIoRef](tekst_xsd_Element_tekst_IntIoRef.dita#IntIoRef)|ref|
|[InwerkingtredingArtikel](tekst_xsd_Element_tekst_InwerkingtredingArtikel.dita#InwerkingtredingArtikel)|art|
|[Kadertekst](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst)|recital|
|[Li](tekst_xsd_Element_tekst_Li.dita#Li)|item|
|[Lid](tekst_xsd_Element_tekst_Lid.dita#Lid)|para|
|[Lijst](tekst_xsd_Element_tekst_Lijst.dita#Lijst)|list|
|[Paragraaf](tekst_xsd_Element_tekst_Paragraaf.dita#Paragraaf)|subsec|
|[Rectificatietekst](tekst_xsd_Element_tekst_Rectificatietekst.dita#Rectificatietekst)|content|
|[Subparagraaf](tekst_xsd_Element_tekst_Subparagraaf.dita#Subparagraaf)|subsec|
|[Subsubparagraaf](tekst_xsd_Element_tekst_Subsubparagraaf.dita#Subsubparagraaf)|subsec|
|[table](tekst_xsd_Element_tekst_table.dita#table)|table|
|[Titel](tekst_xsd_Element_tekst_Titel.dita#Titel)|title|
|[WijzigArtikel](tekst_xsd_Element_tekst_WijzigArtikel.dita#WijzigArtikel)|art|
|[WijzigBijlage](tekst_xsd_Element_tekst_WijzigBijlage.dita#WijzigBijlage)|cmp|

### `[nummer]`
* Voor een expliciet genummerd element wordt het nummer van het element (uit de tekst) als volgt overgenomen: 
    * Spaties in het nummer worden weggelaten.
    * Cijfers, letters, "-" en "." worden overgenomen. 
    * Andere tekens worden vervangen door ".". 
    * Als de voorgaande voorschriften leiden tot 1 of meerdere punten aan het eind van de eId, dan worden deze weggelaten.
* Als de voorgaande voorschriften niet leiden tot een unieke eId, dan wordt aan het eind van de eId  `_inst[volgnummer]` toegevoegd, waarbij volgnummer begint bij 2.
* Voor een element dat niet expliciet genummerd is, wordt het nummer: `o_[volgnummer]`, waarbij volgnummer begint bij 1.


## Samenstellen van een *wId*

Voor de elementen die geen vast *wId* hebben wordt de *wId* als volgt samengesteld: 

```
wId = [bevoegd_gezag]_[versienummer]__[eId]
```

Hierbij geldt:

| onderdeel | betekenis |
|----       | ----      |
| `[bevoegd_gezag]`| De identificatie van het bevoegd gezag dat het tekstelement introduceert. Dit is dezelfde identificatie voor overheden als ook in de naamgeving van documenten gebruikt wordt.|
| `[versienummer]` | <p>Een door het bevoegd gezag aan de regelingversie toegekend versienummer, waar het element voor het eerst in verschijnt. Zie [versienummer](data_xsd_Element_data_versienummer.dita#versienummer) voor een toelichting. </p><p>Als dit versienummer gebruikt wordt en het komt terug in de [expression identificatie](regeling_naamgeving.md) van de regelingversie, dan moet het versienummer in de `wId` gelijk zijn aan dat in de expression identificatie. </p><p>Een alternatief voor versienummer is een compacte weergave van de datum waarop de versie is ontstaan, bijvoorbeeld `2018-11-29`</p><p>De eisen aan het versienummer in STOP laten ruimte om een UUID als versienummer te gebruiken en de cijfers en letters daarvan in de prefix van de *wId* te gebruiken.</p> |
| `[eId]` | Het `eId` van het element in de versie van de tekst waar het element voor het eerst in verschijnt. |

In software waarmee teksten volgens een STOP model geschreven worden kan het nodig zijn om direct na toevoeging van een nieuw tekstelement de *eId* en *wId* te kunnen bepalen zonder dat al bekend is in welke versie van de regeling deze tekst opgenomen zal worden. De *eId* hangt af van de positie in de tekst waar het element wordt toegevoegd, deze kan dus altijd vastgesteld worden. Om de *wId* te bepalen moet daar een prefix met versienummer aan worden toegevoegd. Als het versienummer nog onbekend is kan in plaats van het versienummer de datum of de letters en cijfers van een UUID gebruikt worden. Net als bij een 'gewoon' versienummer moet ook bij deze versienummers de *wId* tussen de regelingversies constant blijven zolang de strekking van de inhoud van betreffende tekst niet wijzigt.


## Voorbeelden *eId* / *wId*

|nr  |Beschrijving|eId|wId|
|----|----       | ----      |---|
| 1|Artikel 1 in lichaam van de regeling, introductie door bevoegd gezag gm0503 in versie v0.1|`art_1`|`gm0503_v0-1__art_1`|
| 2|Een tweede artikel genummerd met "1" in lichaam van dezelfde regeling, introductie in versie v1.6. (Dit zou niet voor mogen komen; *eId* en *wId* van het eerste artikel blijven ongewijzgd)|`art_1_inst2`|`gm0503_v1-6__art_1_inst2` |
| 3|Vernummering van het tweede "Artikel 1" naar "1a" in versie v1.7|`art_1a` |`gm0503_v1-6__art_1_inst2`|
| 4|Een artikel 10:2 in hoofdstuk 10 van een regeling, introductie door bevoegd gezag mn002 in versie 2018-25-10 |`chp_10__art_10.2`|`mn002_2018-25-10__chp_10__art_10.2`|
| 5|Lid 1. in voorgaand artikel 10:2 |`chp_10__art_10.2__para_1`|`mn002_2018-25-10__chp_10__art_10.2__para_1`|
| 6|Na plaatsen van bestaande artikel 10:2 binnen een nieuwe paragraaf 10.1 in versie 2019-01-09|`chp_10__subsec_10-1__art_10.2`|`mn002_2018-25-10__chp_10__art_10.2__para_1` |
| 7|Aanhef, een van de elementen met een vast *eId* / *wId*|`formula_1`|`formula_1`|
| 8|Sluiting direct na het lichaam, een van de elementen met een vast *eId* / *wId*|`formula_2`|`formula_2`|
| 9|Sluiting in de daaropvolgende bijlage|`formula_2_inst2`|`formula_2_inst2`|
| 10|Het derde item in een ongenummerde lijst; binnen wId is het [versienummer](data_xsd_Element_data_versienummer.dita#versienummer) een UUID|`list_o_1__item_o_3`|<p>`mn000__`</p><p>`518d67613862486c9121784868d047e6`</p><p>`__list_o_1__item_o_3`</p>|
| 11|Toelichting, element met een vast *eId* / *wId*|`recital`|`recital`|
| 12 |Artikelgewijze Toelichting, al dan niet binnen een Toelichting, element met een vast *eId* / *wId*|`artrectial`|`artrecital`|
| 13 |Eerste divisietekst van een Artikelgewijze Toelichting die binnen de Toelichting staat|`artrecital__content_o_1`|`mn002__2018-25-10__artrecital__content_o_1`|
