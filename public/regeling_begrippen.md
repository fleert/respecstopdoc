# Begrippen

## In regeling en in catalogi
In een artikelsgewijze regeling is het gebruikelijk om begrippen die in de regeling gebruikt worden expliciet te definiëren zodat er geen misverstand over de betekenis van een begrip bestaat. De STOP tekstmodellen kennen daarvoor een speciale constructie, een [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip) in een [Begrippenlijst](tekst_xsd_Element_tekst_Begrippenlijst.dita#Begrippenlijst), zodat het begrip in de tekst te herkennen is.

De begrippen kunnen ontleend zijn aan standaarden of geharmoniseerde begrippenlijsten die worden bijgehouden in een centraal beschikbaar gestelde catalogus / ontologie / waardelijst. Door in verwante of vergelijkbare regelgeving van verschillende bevoegd gezagen gebruik te maken van dezelfde begripsdefinities wordt regelgeving toegankelijker en makkelijker te begrijpen. Hiertoe worden begrippen-catalogi aangelegd zoals bijvoorbeeld de [begrippen uit de Omgevingswet](https://stelselcatalogus.omgevingswet.overheid.nl/begrippen/overview) als onderdeel van de stelselcatalogus Omgevingswet.

Een verwijzing naar een begrip in een catalogus kan echter geen onderdeel zijn van een juridische besluit, omdat de catalogus niet voldoet aan de eisen die aan de bekendmakingen worden gesteld, zoals eeuwigdurende, onveranderlijke beschikbaarheid. Als in een regeling een begrip wordt gebruikt moet de definitie van dat begrip dan ook in de regeling zelf opgenomen worden. Bijvoorbeeld conform de [TPOD](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) omgevingsplan (paragraaf 8.2) in het artikel *Begripsbepalingen* in hoofdstuk 1.

## Relateren van vindplaatsen
De gebruikelijke aanpak voor het beschikbaarstellen en beheren van dergelijke begrippen is om een [linked-data](https://nl.wikipedia.org/wiki/Linked_data) standaard zoals [SKOS](https://www.w3.org/TR/skos-reference) te gebruiken. Dergelijke standaarden kennen een grote rijkdom om begrippen semantisch te beschrijven en onderling te relateren. Als onderdeel van het interpreteren en duiden van regelgeving biedt STOP de mogelijkheid om de begrippen in de tekst van de regeling te gebruiken in de wereld van de linked-data standaarden - het biedt geen meerwaarde om de linked-data rijkdom in STOP te repliceren.

STOP biedt twee mechanismen om de vindplaats van een begrip in de tekst van de regeling te relateren aan de linked-data representatie van een begrip dat in een catalogus opgenomen is:

* via de STOP-annotatie [Begripsrelaties](data_xsd_Element_data_Begripsrelaties.dita#Begripsrelaties) die als onderdeel van de regeling wordt bijgehouden;
* via linked-data die bijvoorbeeld in een catalogus wordt bijgehouden.

![Relatie tussen begrip in regeling en in catalogus](img/Begripsrelaties.png)

## Begrip in de regeling als linked-data
Elk [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip) in de tekst heeft een [wId](eid_wid.md). Dit kan gecombineerd worden met de [work-identificatie](regeling_naamgeving.md) van een regeling en de basis-URL van de AKN resolver (`https://identifier.overheid.nl/`) om een versie-onafhankelijke URL voor het begrip te munten. Voorbeeld:

    https://identifier.overheid.nl/akn/nl/act/gm0555/2022/omgevingsplan#gm0555_1__chp_1__list_1__item_42

Deze URL kan vervolgens gebruikt worden als URI voor het begrip in de tekst. Bij het begrip in de catalogus kan via een van de linked-data standaarden een relatie gelegd worden, bijvoorbeeld via een instantie van [skos:mappingRelation](https://www.w3.org/2009/08/skos-reference/skos.html#mappingRelation).

Bij het gebruik van deze aanpak moet rekening gehouden worden met de beperkingen:

* De AKN resolver kent alleen (nu of in de toekomst) geldige regelingen, en begrippen uit regelingversies die publiek beschikbaar zijn.
* Een [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip) is een inline-element waarvoor in STOP niet de verplichting bestaat om de `wId` in de verschillende versies van de regeling gelijk te houden. Het kan nodig zijn aanvullende (implementatie-)afspraken te maken om de `wId` gelijk te houden, zolang het conceptueel om hetzelfde Begrip gaat (ook als Term en Definitie gewijzigd zijn). Omgekeerd: wanneer in een nieuwe versie van de regeling de wijziging van Term en/of Definitie dusdanig is dat een nieuw Begrip ontstaat, moet ook een nieuw wId gehanteerd worden.
* De richtlijnen voor de implementatie van AKN resolvers schrijven voor dat de URL een redirect is naar een geschikte versie van de tekst. Over het retourneren van data formaten die meer geschikt zijn voor toepassingen binnen linked-data (bijvoorbeeld het volgens een linked-data standaard doorgeven van de term en definitie) moeten aanvullende implementatieafspraken gemaakt worden.

## Begripsrelaties in STOP

Een [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip) in de tekst kan via de [Begripsrelaties](data_xsd_Element_data_Begripsrelaties.dita#Begripsrelaties) annotatie bij de regeling gerelateerd worden aan een begrip in een catalogus. Voor elk begrip in de tekst waarvoor de relatie gelegd moet worden, moet een [Begripsrelatie](data_xsd_Element_data_Begripsrelatie.dita#Begripsrelatie) opgenomen worden met daarin:

* de [wId](data_xsd_Element_data_wId.dita#wId) van het [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip) in de tekst van de regeling;
* de [catalogusEndPoint](data_xsd_Element_data_catalogusEndPoint.dita#catalogusEndPoint) is de URL van het begrip in de catalogus;
* de [mappingRelation](data_xsd_Element_data_mappingRelation.dita#mappingRelation) geeft de relatie weer van het begrip in de tekst (in linked-data termen het _subject_) en het begrip in de catlogus (als _object_). De soort relatie is identiek aan [skos:mappingRelation](https://www.w3.org/2009/08/skos-reference/skos.html#mappingRelation):
    - **exactMatch** - Het begrip en de definitie komt exact overeen met het begrip in de tekst.
    - **closeMatch** - Het begrip komt niet exact overeen met het begrip in de/een catalogus, maar de definitie zoals in die catalogus is gegeven moet wèl correct zijn en overeenkomen met de bedoeling/definitie zoals in de gepubliceerde juridische tekst is bedoeld.

Net als bij de andere methode is de `wId` van het [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip) niet gegarandeerd onveranderlijk in opvolgende regelingversies. Dat leidt er hier toe dat de annotatie mogelijk voor elke regelingversie bijgewerkt moet worden.

Er is een voorbeeld van de [Begripsrelaties module](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Coderingen/Begripsrelaties) beschikbaar.

## Implementatiekeuze

Het gebruik van de informatie uit de _Begripsrelaties_ module door een systeem dat deze informatie ontvangt is niet verplicht. Zie de documentatie van het betreffende systeen voor informatie over of en hoe de module gebruikt wordt.

STOP schrijft niet voor hoe de informatie in de _Begripsrelaties_ samen met de begrippen in de tekst vertaald moeten worden naar informatie volgens linked-data standaarden of welke URI de begrippen moeten krijgen. Dit wordt overgelaten aan de STOP-implementerende systemen.


