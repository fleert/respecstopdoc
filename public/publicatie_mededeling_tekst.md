# Tekstmodel mededeling

Alle publicaties die geen bekendmakingen of kennisgevingen zijn, worden mededelingen genoemd. Een voorbeeld van een mededeling is bijvoorbeeld een bevoegd gezag dat mededeling doet van een uitspraak van de rechter over één van haar besluiten.

TODO Mededeling opnemen in STOP, voorbeeld rechter aanpassen

Er bestaan geen wettelijke voorschriften voor het structuren van de tekst van een mededeling. In STOP is een mededeling daarom opgebouwd uit een of meer [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst)-en.



**Zie ook**

* Het informatiemodel(TODO link) van een mededeling.
* XML schema(TODO link) van de tekstmodule van de mededeling.
* XML voorbeeld(TODO link) van de tekstmodule van een mededeling.