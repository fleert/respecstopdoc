# Tekstmodel kennisgeving 

Een kennisgeving ([art 3.12 AWB](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.4&artikel=3:12&z=2022-01-28&g=2022-01-28)) heeft als doel om betrokkenen te wijzen op een besluit. Een kennisgeving bevat dan ook:

* Een verwijzing naar het besluit, tenzij het een kennisgeving van een voorgenomen besluit is;

* Informatie over de terinzagelegging van het besluit en bijbehorende stukken [art 3:44 AWB](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.6&artikel=3:44&z=2022-01-28&g=2022-01-28);

* Eventuele bezwaar- en beroep-mogelijkheden en termijnen [art 3:45 AWB](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&hoofdstuk=3&afdeling=3.6&artikel=3:45&z=2022-01-28&g=2022-01-28).

Deze drie aspecten zullen zowel in de tekst als in de [metadata](publicatie_metadata.md) van de kennisgeving te vinden zijn. Er bestaan geen wettelijke voorschriften voor het structuren van de tekst van een kennisgeving. In STOP is een [Kennisgeving](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving) daarom opgebouwd uit een of meer [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst)-en.



**Zie ook**

* Het [informatiemodel](EA_233F9A157B1F4d11A41DBE8FC3424244.dita#Pkg) van de kennisgeving.
* [XML schema](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving) van de tekstmodule van de kennisgeving.
* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-Tekst.xml) van de tekstmodule van een kennisgeving.