# Schematrons

## Schematrons in IMOP

Een [schematron](https://schematron.com/) is een standaard manier om in het XML-domein bedrijfsregels vast te leggen. Uitgebreidere XML-georiënteerde software kan een schematron direct uitvoeren. In IMOP worden de bedrijfsregels die betrekking hebben op een enkele IMOP-module en die aan de hand van alleen de informatie in de module te controleren zijn, zoveel mogelijk in schematrons vastgelegd.

Niet alle XML-gerelateerde software kan schematrons uitvoeren. Als dat niet het geval is, dan kan een schematron worden omgezet in een [XSLT versie 2.0](https://www.w3.org/TR/xslt20/)-transformatie volgens een [standaard procedé](https://github.com/Schematron/schematron). Als service worden bij IMOP de [XSLT2-transformaties](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aservice/schematron_xslt) uitgeleverd die geschikt zijn voor de Saxon XSLT engine. De XSLT2-vertalingen zijn getest met 
* [Saxon Home Edition](https://www.saxonica.com/download/download_page.xml) (aldaar vermeld als de Saxon HE editie), een [open source](https://www.saxonica.com/support/opensource.xml) XSLT/XQuery-implementatie, en met 
* [MarkLogic](https://www.marklogic.com/product/marklogic-database-overview/).

De schematrons die gebruikt kunnen worden om de XML-serialisatie van een IMOP-module te valideren zijn vastgelegd in het [versieoverzicht](schemata_gebruik_versionering.md). Dat overzicht verwijst naar de schematrons met de juiste URL. De bestandsnaam voor een schematron is te herkennen aan de `.sch` extensie. De XSLT2-vertaling van de schematron is beschikbaar via een URL die uit de schematron-URL verkregen wordt door de `.sch` extensie te vervangen door `.xslt`.

## Uitkomst van een schematron

De schematrons moeten toegepast worden op elk (geheel) [XML-document](xml_documenten.md) dat de IMOP-module bevat. Dus niet alleen als het XML-element het rootelement van het XML-document is, maar ook als het XML-element op een andere positie in het document voorkomt. 

De validatie van een XML-document via schematrons kan pas gedaan worden nadat is vastgesteld dat het XML-document valide is ten opzichte van de XML-schema's. De foutmeldingen uit XML-schema-validatie zijn afhankelijk van de gebruikte XML-parser en worden niet door STOP voorgeschreven. 

Als de XML-serialisatie niet aan de bedrijfsregels voldoet, dan resulteert het uitvoeren van een schematron in één of meer JSON-objecten, gescheiden door komma's (en met een komma aan het einde). Door de laatste komma weg te halen en `[ ]` er omheen te zetten ontstaat een valide JSON object-array. Voorbeeld van de uitkomst van een schematron:

```json
  {
    "code": "STOP0005",
    "melding": "De alinea voor element Divisietekst met id content_o_1 bevat geen tekst. Verwijder de lege alinea",
    "ernst": "fout"
    "element": "Divisietekst",
    "eId": "content_o_1"
  },
  {
    "code": "STOP1000", 
    "melding": "De identifier /join/id/proces/pv30/2020/Instelling.Verordening bevat een punt. Dit is niet toegestaan. Verwijder de punt.",
    "ernst": "fout"
    "ID": "/join/id/proces/pv30/2020/Instelling.Verordening"
  },

```

Elk JSON-object in de validatieoutput heeft een veld `code`. Deze codes met de bijbehorend meldingen zijn opgesomd op het overzicht van de [bedrijfsregels](businessrules.md). De `ernst` geeft aan of het om een `fout`, `waarschuwing` of `informatief` gaat. De `melding` geeft een mensleesbare beschrijving die overeenkomt met de melding als beschreven in de bijbehorende bedrijfsregel en waarbij eventuele parameters zijn ingevuld. De overige attributen van het JSON-object betreft de parameters zoals ze in de melding verwerkt zijn.

## Meldingen met ernst = `ontraden`

De `ernst` kan ook `ontraden` zijn. Op deze manier waarschuwt IMOP dat een deel van de XML-serialisatie een model volgt dat in een volgende versie van de standaard zal komen te vervallen. Er zal binnen deze versie van STOP ook al een alternatief model geboden worden. Het is aan te raden bij het inbouwen van deze versie van IMOP in software al om te schakelen naar het alternatieve model.