# Geautomatiseerde consolidatie

## Principe
De geautomatiseerde consolidatie wordt per regeling en per (te consolideren) informatieobject uitgevoerd. De details van consolideren worden [als consolidatiepatronen op een aparte pagina](consolideren_patronen.md) beschreven. Daar is ook een legenda te vinden voor de symbolen in de diagrammen. Op deze pagina wordt de werking van de geautomatiseerde consolidatie voor een regeling  toegelicht aan de hand van het [versiebeheer van STOP](versiebeheer_principes.md). Het versiebeheer is ook van toepassing op een (te consolideren) informatieobject.

Bij de consolidatie hoeft alleen rekening gehouden te worden met de [doelen/branches](versiebeheer_principes.md) waarvoor een nieuwe versie van de regeling of informatieobject is gemaakt. Bovendien moet de nieuwe versie onderdeel zijn van een vastgesteld besluit en moet het moment van inwerkingtreding bekend zijn. 

| ![Ontstaan van een toestand](img/consolideren_principe_doel.png) |
|:--:|
| Voorbeeld 1: ontstaan van een toestand |


In voorbeeld 1 wordt eerst een ontwerpbesluit of een besluit zonder inwerkingtreding gepubliceerd voor een nieuwe versie die voor doel A is aangemaakt. Dat heeft geen effect op de geconsolideerde regeling. Pas bij de publicatie de inwerkingtredingsdatum (in het vastgestelde besluit of met een apart inwerkingtredingsbesluit) gaat de nieuwe versie meetellen. Er ontstaat een nieuwe toestand van de geconsolideerde regeling. De nieuwe toestand is bekend op de dag van publicatie van het tweede besluit, maar wordt pas geldig op de inwerkingtredingsdatum. Alle volgende publicaties die de datum van inwerkingtreding ongemoeid laten, zoals een rectificatie van de tekst van de regeling of een revisie van de annotaties, leiden tot een andere *inhoud van de toestand* (tekst, annotaties), maar niet tot een *nieuwe toestand zelf*. De inhoud van de toestand bestaat steeds uit de laatste aangeleverde versie van de tekst en annotaties van de regeling - dus de versie/expression aangeduid met `wordt` in de [regeling](tekst_xsd_Attribute_Group_tekst_agComponentNieuweRegeling.dita#agComponentNieuweRegeling) ~~of [mutatie](tekst_xsd_Attribute_Group_tekst_agComponentMutatie.dita#agComponentMutatie)) die gebruikt is voor het opstellen van het besluit en die uit een wijzigingsbesluit verkregen kan worden door de mutatie (renvooi) te verwerken~~. 

TODO *ThF hier stond "door de mutatie uit te werken" maar dat begreep ik niet zo goed. Wat bedoel je daarmee? Ik vraag me ook af of je hier in dit voorbeeld de mutatie erbij moet halen, maakt de tekst ingewikkelder. Vandaar de doorhaling * 

In de meest eenvoudige situatie wordt een versie van een regeling voor een nieuw doel gebaseerd op de versie van een ander doel die geldig is op het moment van inwerkingtreding.

| ![Consolideren van opeenvolgende besluiten](img/consolideren_principe_doelen.png) |
|:--:|
| Voorbeeld 2: consolideren van opeenvolgende besluiten |


In voorbeeld 2 zijn er bijvoorbeeld drie publicaties van vastgestelde besluiten met een nieuwe versie van de regeling die steeds op de voorgaande regelingversie zijn gebaseerd. Elk besluit heeft een eigen moment van inwerkingtreding die later is dan de inwerkingtreding van het eerdere besluit. Het resultaat is dat er drie toestanden van de geconsolideerde regeling ontstaan, elk met als inhoud de versie van de regeling die volgt uit het vastgestelde besluit.

## Karakterisering van de toestanden
Om te bepalen welke toestanden er zijn is het niet nodig om te weten wat de inhoud van regelingversie is. Alleen de datum van inwerkingtreding (gemodelleerd als [`juridischWerkendVanaf`](data_xsd_Element_data_soortTijdstempel.dita#soortTijdstempel)) is van belang, en de doelen/branches die eerder in werking zijn getreden:

* Toestand 1 heeft `juridischWerkendVanaf` = inwerkingtreding van besluit 1. De inhoud is de initiële versie die voor doel A is gemaakt en waarop alle wijzigingen zijn toegepast die later voor doel A zijn gepubliceerd.
* Toestand 2 heeft `juridischWerkendVanaf` = inwerkingtreding van besluit 2. De inhoud van de toestand is de inhoud van de eerder geldige toestand (1) waarop alle wijzigingen zijn toegepast die voor doel B zijn gepubliceerd, dus de initiële versie waarop alle wijzigingen zijn toegepast die voor doel A en B zijn gepubliceerd.
* Toestand 3 heeft `juridischWerkendVanaf` = inwerkingtreding van besluit 3. De inhoud van de toestand is de inhoud van de eerder geldige toestand (2) waarop alle wijzigingen zijn toegepast die voor doel C zijn gepubliceerd, dus de initiële versie waarop alle wijzigingen zijn toegepast die voor doel A, B en C zijn gepubliceerd.

Een vergelijkbare beschrijving kan gemaakt worden als twee besluiten, bijvoorbeeld besluit 2 en 3, tegelijk (op dezelfde dag) in werking treden. 

| ![Toestanden voor gelijktijdige inwerkingtreding](img/consolideren_gelijktijdige_iwt_fout.png) |
|:--:|
| Voorbeeld 3a: toestanden voor gelijktijdige inwerkingtreding |

De toestanden zijn nu:
* Toestand 1 als in het eerdere voorbeeld.
* Toestand 2 ontstaat na bekendmaking van besluit 2 en heeft `juridischWerkendVanaf` = inwerkingtreding van besluit 2. De inhoud van de toestand is de inhoud van de eerder geldige toestand (1) waarop alle wijzigingen zijn toegepast die voor doel B zijn gepubliceerd, dus de initiële versie waarop alle wijzigingen zijn toegepast die voor doel A en B zijn gepubliceerd.
* Toestand 2 heeft na bekendmaking van besluit 3 een andere inhoud: de inhoud van de eerder geldige toestand (1) waarop alle wijzigingen zijn toegepast die voor doel B en voor doel C zijn gepubliceerd, dus de initiële versie waarop alle wijzigingen zijn toegepast die voor doel A, B en C zijn gepubliceerd.

Een toestand van een geconsolideerde regeling kan daarom gekarakteriseerd worden met:

1. De datum van inwerkingtreding: `juridischWerkendVanaf`
2. Het doel of de doelen van alle wijzigingen die op deze datum in werking treden: de *inwerkingtredingsdoelen*.
3. De doelen van de initiële versie en alle wijzigingen die eerder in werking getreden zijn: de *basisversie-doelen*.
4. Inhoudelijk (tekst en annotaties) correspondeert de toestand met die regelingversie waarin alle wijzigingen voor alle doelen (zowel onder 2 als onder 3) zijn verwerkt.

De eerste drie karakteristieken hangen alleen af van de collectie doelen waarvoor de regeling ingesteld of gewijzigd wordt, en de `juridischWerkendVanaf`-datums. Voor de twee voorbeelden:

Voorbeeld 2:
* Toestand 1:
  * `juridischWerkendVanaf` = inwerkingtreding van besluit 1
  * inwerkingtredingsdoelen = A
  * basisversie-doelen: -
* Toestand 2:
  * `juridischWerkendVanaf` = inwerkingtreding van besluit 2
  * inwerkingtredingsdoelen = B
  * basisversie-doelen: A
* Toestand 3:
  * `juridischWerkendVanaf` = inwerkingtreding van besluit 3
  * inwerkingtredingsdoelen = C
  * basisversie-doelen: A, B

Voorbeeld 3a:
* Toestand 1:
  * `juridischWerkendVanaf` = inwerkingtreding van besluit 1
  * inwerkingtredingsdoelen = A
  * basisversie-doelen: -
* Toestand 2 na publicatie van besluit 2:
  * `juridischWerkendVanaf` = inwerkingtreding van besluit 2
  * inwerkingtredingsdoelen = B
  * basisversie-doelen: A
* Toestand 2 na publicatie van besluit 3:
  * `juridischWerkendVanaf` = inwerkingtreding van besluit 2 en 3
  * inwerkingtredingsdoelen = B, C
  * basisversie-doelen: A

## Inhoud van de toestand

De inhoud van een toestand is gelijk aan de initiële versie waarin alle wijzigingen verwerkt zijn die gepubliceerd zijn voor alle doelen/branches geassocieerd met de toestand. Dit criterium is ook anders te formuleren:

> De inhoud van een toestand is de laatst aangeleverde versie voor de inwerkingtredingsdoelen van de toestand, mits in die versie alle wijzigingen zijn verwerkt van de basisversie-doelen en geen wijzigingen van andere doelen. 

Als dit toegepast wordt op voorbeeld 2 met drie opeenvolgende besluiten:

* Toestand 1 heeft als inhoud de versie voor doel A (uit besluit 1)
* Toestand 2 heeft als inhoud de versie voor doel B (uit besluit 2), waarin de laatste wijzigingen voor doel A zijn verwerkt omdat de versie van doel B is gebaseerd op de laatste versie voor doel A.
* Toestand 3 heeft als inhoud de versie voor doel C (uit besluit 3), waarin de laatste wijzigingen voor doel A en B zijn verwerkt omdat de versie van doel C is gebaseerd op de laatste versie voor doel B, die weer is gebaseerd op de laatste versie van doel A.

Toegepast op voorbeeld 3a:
* Toestand 1 heeft als inhoud de versie voor doel A (uit besluit 1)
* Toestand 2 heeft na bekendmaking van besluit 2 als inhoud de versie voor doel B (uit besluit 2), waarin de laatste wijzigingen voor doel A zijn verwerkt omdat de versie van doel B is gebaseerd op de laatste versie voor doel A.
* Toestand 2 heeft na bekendmaking van besluit 3 als inhoud - wat is de inhoud van de toestand nu?

Als in voorbeeld 3a voor besluit 3 niet is meegegeven dat de versie voor doel C tevens de nieuwe versie voor doel B is, dan ontstaat er onduidelijkheid. Er ontstaat een vorm van samenloop: er zijn twee versies (voor doel B en C) voor één toestand. Hoewel de versie voor C is gebaseerd op die voor B, hoeft de C-versie niet altijd recht te doen aan besluit 2. In de C-versie kan een deel van besluit 2 onbedoeld overschreven worden. Als onderdeel van besluit 2 kan een norm bijvoorbeeld gewijzigd worden naar de ene waarde, en in besluit 3 naar een andere waarde. Door besluit 2 en 3 dezelfde inwerkingtredingsdatum te geven zegt BG dat op die datum de norm zowel in de ene als de andere waarde gewijzigd moet worden. De C-versie zal alleen de laatste waarde kennen. In STOP wordt deze samenloop uitgedrukt door:

* Toestand 2 heeft na bekendmaking van besluit 3 een onbekende inhoud, want de wijzigingen voor doel B en C treden tegelijk in werking maar er zijn meerdere versies beschikbaar.

Of de C-versie de correcte weergave van beide besluiten is kan alleen het bevoegd gezag beoordelen. Het is aan het bevoegd gezag te bevestigen dat de versie van doel C nu ook voor doel B geldt. Dat moet onderdeel zijn van besluit 3:

| ![Correcte publicatie van gelijktijdige inwerkingtreding](img/consolideren_gelijktijdige_iwt_goed.png) |
|:--:|
| Voorbeeld 3b: correcte publicatie van gelijktijdige inwerkingtreding |

Nu kan wel eenduidig vastgesteld worden:

* Toestand 2 heeft na bekendmaking van besluit 3 als inhoud de versie voor doel B en C (uit besluit 3), waarin de laatste wijzigingen voor doel A zijn verwerkt omdat de versie van zowel doel B als C is gebaseerd op de laatste versie voor doel A.

### Samenloop oplossen
Er zijn twee vormen van samenloop die niet altijd opgelost kunnen worden voordat een besluit gepubliceerd wordt.

#### Eerste samenloopvorm: latere inwerkingtreding van eerder besluit
De eerste kan het beste uitgelegd worden aan de hand van voorbeeld 4, waarbij besluit 3 eerder bekendgemaakt wordt dan besluit 2 maar later in werking treedt.

| ![Samenloop oplossen: vervlechten](img/consolideren_vervlechten.png)  |
|:--:|
| Voorbeeld 4: vervlechten: verwerken later gepubliceerde wijziging |

Na publicatie van besluit 2 (linkerkant van voorbeeld 4) kan van toestand 3 de inhoud niet meer bepaald worden. De beschrijving van die toestand is dan:

Toestand 3:
* `juridischWerkendVanaf` = inwerkingtreding van besluit 3
* inwerkingtredingsdoelen = C
* basisversie-doelen: A, B
* Inhoud onbekend, want de wijzigingen voor doel B zijn nog niet volledig verwerkt in de versie van het inwerkingtredingsdoel.

Het bevoegd gezag moet ervoor zorgen dat de wijzigingen voor doel B ook in de versie voor doel C worden overgenomen. Dat kan door een revisie van de versie voor doel C uit te wisselen (rechts in voorbeeld 4) en aan te geven dat de wijzigingen van doel B daarin vervlochten zijn. De geautomatiseerde consolidatie kan nu constateren dat de versie voor doel C alle wijzigingen van de basisversie-doelen bevat en dus de inhoud van toestand 3 is.

#### Tweede samenloopvorm: vernietiging van eerder besluit
De tweede vorm komt voor als een rechter een besluit vernietigt terwijl er al een volgend besluit is bekendgemaakt en in werking is getreden.

| ![Ontvlechten bij consolideren](img/consolideren_ontvlechten.png) |
|:--:|
| Voorbeeld 5: ontvlechten: verwijderen eerder gepubliceerde wijziging |

In het voorbeeld (linkerkant van voorbeeld 5) komt besluit 2 te vervallen nadat besluit 3 is gepubliceerd. Dit is aangegeven met `XXX`. Voor toestand 3 geldt dan:

Toestand 3:
* `juridischWerkendVanaf` = inwerkingtreding van besluit 3
* inwerkingtredingsdoelen = C
* basisversie-doelen: A
* Inhoud onbekend, want de wijzigingen voor doel B zijn nog onderdeel van de versie van het inwerkingtredingsdoel.

Het bevoegd gezag moet ervoor zorgen dat de wijzigingen voor doel B uit de versie voor doel C worden verwijderd. Dat kan door een revisie van de versie voor doel C uit te wisselen (rechterkant in voorbeeld 5) en aan te geven dat de wijzigingen van doel B daarin ontvlochten zijn. De geautomatiseerde consolidatie kan nu constateren dat de versie voor doel C alle wijzigingen van de basisversie-doelen (A) bevat en van géén andere doelen. Het is daarmee de inhoud van toestand 3.
