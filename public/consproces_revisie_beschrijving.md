# Beschrijving revisie

## **Wat is een revisie?**

Een revisie is een *niet-juridische* aanpassing van de *geconsolideerde* regelingtekst (of informatieobject). *Juridische* wijzigingen kunnen alleen met een [besluit](awb-besluit.md) (of met een [rectificatie](besluitproces_rectificatie.md)) aangebracht worden. Een revisie wordt in tegenstelling tot een besluit, niet gepubliceerd. Een revisie wordt alleen zichtbaar doordat voor de consolidatie een andere versie van de regelingtekst of het informatieobject wordt gebruikt. Of dat een annotatie of de consolidatie-informatie is gewijzigd. Zie ook de [definitie](begrippenlijst_revisie.dita#revisie) en het [informatiemodel](EA_DBE8BF553C524b71BB84CFF11C7660CB.dita#Pkg) van revisie.

## **Wanneer wordt een revisie gebruikt?**

Het instrument revisie wordt in een aantal situaties gebruikt:

* Met een revisie kunnen bijvoorbeeld verwijzingen (links) aangebracht worden in de tekst van de geconsolideerde regeling of andere niet-juridische correcties of aanvullingen in de tekst aan te brengen. 
* Annotaties bij een regeling kunnen met een revisie gewijzigd worden. Zo kan bijvoorbeeld in de metadata van de regeling een alternatieve titel of afkorting voor de regeling worden toegevoegd. 
* Ook na het ontstaan van [samenloop](proces_samenloop.md), dus als de geconsolideerde regelingtekst niet automatisch afgeleid kan worden, wordt het instrument revisie gebruikt om de geconsolideerde regelingtekst te corrigeren. Ook het verwerken van een gedeeltelijke vernietiging van een besluit door de rechter in de geconsolideerde regeling gebeurd met een revisie.
* Daarnaast kan een revisie worden gebruikt om de zichtbaarheid van de regeling te beperken. Als een regeling voorziet in situaties of personen die niet meer bestaan, denk bijvoorbeeld aan een regeling voor mensen geboren voor 1848, dan kan de zichtbaarheid beperkt worden door nieuwe consolidatie-informatie aan te leveren waarin de regeling als 'materieel uitgewerkt' aan wordt geduid. 

## **Wanneer wordt géén revisie gebruikt?**

Een revisie kan niet gebruikt worden om *juridische* wijzigingen in de geconsolideerde regeling aan te brengen. Als het gepubliceerde besluit niet de correcte weergave is van het door het BG genomen besluit, kan met een [rectificatie](besluitproces_rectificatie.md) het gepubliceerde besluit gecorrigeerd worden. 

## **Wat doet een revisie?**

Een revisie kan:

* niet-juridische wijzigingen aanbrengen in de geconsolideerde regelingtekst,
* niet-juridische wijzigingen in informatieobjecten aanbrengen,
* annotaties (zoals regeling(versie)metadata) aanpassen,
* consolidatie-informatie aanpassen.

Een revisie is dus geen besluit, maar past alleen de weergave of de bijbehorende informatie van de geconsolideerde regeling of van het geconsolideerde informatieobject aan.
