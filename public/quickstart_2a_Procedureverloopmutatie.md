# data:Procedureverloopmutatie

Deze module voorziet in *mutatie* van de procedurestappen in een [data:Procedureverloop](data_xsd_Element_data_Procedureverloop.dita#Procedureverloop) die doorlopen worden om een mijlpaal te realiseren.

Bij een definitief besluit kan een bezwaar- en/of beroepstermijn van toepassing zijn en bij een ontwerp besluit een inzagetermijn. Indien dergelijke termijnen van toepassing zijn, dient het BG, conform de AWB, een kennisgeving te publiceren over deze termijnen. Bij deze kennisgeving worden de relevante procedurestappen als procedureverloopmutatie opgenomen.  Zie bijv. de toelichting over de [rechterlijke macht](besluitproces_rechterlijkemacht.md) en met name de [wijze van aanlevering](besluitproces_rechterlijkemacht_procedurele_status.html#procedureverloop-en-procedureverloopmutatie).

## Voorbeeld

[Voorbeeld Procedureverloopmutatie van een ontwerpbesluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-ProcedureMutatie.xml)

[Voorbeeld Procedureverloopmutatie van een definitief besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/01%20Beroep%20mogelijk/02-BG-Kennisgeving-ProcedureMutatie.xml)
