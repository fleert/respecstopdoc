# Regelgeving in de loop van de tijd

## Tijdreizen
De [geautomatiseerde consolidatie](consolideren_principe.md) geeft een beeld van de regelgeving zoals die op verschillende momenten in de tijd geldig was, uitgaande van de meest recente kennis over besluiten en beroepsprocedures. Landelijke voorzieningen zoals de LVBB zijn ook geïnteresseerd in het verleden, zoals wat er geldig was op een bepaald moment met de kennis van toen. Er zijn een [vijftal tijdreizen](tijdreizen-toelichting.md) onderscheiden die voor landelijke voorzieningen (of tenminste de LVBB) interessant zijn en die iets over in het verleden geldende regelgeving zeggen.

Om de tijdreizen te ondersteunen moet het mogelijk zijn langs vier tijdassen te reizen:

* _Bekend op_: wat er op een bepaald moment publiek bekend is.
* _Ontvangen op_: waarover een STOP-gebruikend systeem op een bepaald moment aan informatie beschikt.
* _Juridisch werkend op_: welke (delen van) besluiten in werking zijn getreden en niet ingetrokken of door een beroepesorgaan vernietigd zijn.
* _Geldig op_: welke (delen van) besluiten (die juridisch in werking zijn) geldig zijn, rekening houdend met de mogelijkheid dat de start van de geldigheid voor de datum inwerkingtreding ligt (geldigheid met terugwerkende kracht).

De STOP-gebruikende systemen die willen tijdreizen zullen een datamodel moeten hanteren dat deze vier tijdassen ondersteunt. Er zijn veel manieren om zo'n datamodel in te richten; als implementatie-neutrale standaard voor de uitwisseling van informatie doet STOP daarover geen aannamen. Het efficiënt synchroniseren van tijdreisinformatie tussen twee voorzieningen vereist kennis van de datamodellen; daar is STOP niet op gericht. Wel biedt STOP mogelijkheden om informatie uit te wisselen voor doorsnedes van de vierdimensionale tijd-ruimte.

## Tijdassen in STOP

De vier tijdassen worden in STOP gemodelleerd als:

Modules met [contextinformatie](context_modules.md)
: De _Bekend op_ / _Ontvangen op_ tijdassen zijn niet alleen voor tijdreizen van belang, ze worden in STOP ook gebruikt om van tijdsafhankelijke (context) informatie aan te geven van wanneer die informatie dateert. In zo'n module wordt een overzicht gegeven van de informatie zoals die op een bepaald moment (combinatie van _Bekend op_ en _Ontvangen op_ datum) in het verzendende systeem aanwezig was.

[Bi-temporele model](tijdreizen-bi-temporeel-model.md). 
: De _Juridisch werkend op_ / _Geldig op_ worden als tijdvakken gemodelleerd bij de tijdsafhankelijke informatie. Daarvoor wordt het bi-temporele model gebruikt, waarbij de tijdvakken opgedeeld worden in een _Juridisch werkend op_ perioden en vervolgens in _Geldig op_ perioden. De informatie is ondergebracht in een context module, waardoor de afhankelijkheid van alle vier tijdassen gemodelleerd is.

## Regelgeving en tijdreizen

Voor de uitwisseling van een doorsnede van de geldende regelgeving kent STOP verschillende modellen, afhankelijk van het aantal tijdassen die van belang zijn. Elke doorsnede wordt gemaakt voor een enkele regeling/informatieobject (als _work_):

Eén tijdas: _Juridisch werkend op_
: Het meest recente overzicht van [actuele toestanden](consolideren_toestanden.md) bevat deze doorsnede. Dit is de doorsnede die voor bevoegd gezagen relevant is om na te gaan of aan de consolidatieplicht is voldaan, en die aangeeft wat in de toekomst geldt (op basis van de tot nu toe gepubliceerde besluiten).

Twee tijdassen: _Bekend op_ / _Juridisch werkend op_
: Deze doorsnede kan uitgewisseld worden door het overzicht van [actuele toestanden](consolideren_toestanden.md) op te stellen voor elke waarde van de _Bekend op_ datum. Dus voor elke datum dat er publicaties via de LVBB zijn gedaan, en elke datum **TODO: welke is dat?** dat een rechter een uitspraak heeft gedaan die doorwerkt in de geconsolideerde regeling/informatieobject. Deze overzichten worden gemaakt op basis van de meest recente uitgewisselde informatie. Het gaat om meerdere modules van hetzelfde type, waarvan sommige bij het ontvangen van nieuwe informatie kunnen wijzigen of komen te vervallen.

Drie tijdassen: _Bekend op_ / _Ontvangen op_ / _Juridisch werkend op_
: Deze doorsnede kan uitgewisseld worden door het overzicht van [actuele toestanden](consolideren_toestanden.md) op te stellen voor elke waarde van de _Bekend op_/_Ontvangen op_  combinatie. datum. Dus voor elke datum dat er publicaties via de LVBB zijn gedaan, en elke datum **TODO: welke is dat?** dat een rechter een uitspraak heeft gedaan die doorwerkt in de geconsolideerde regeling/informatieobject, in combinatie met elke datum dat er nieuwe informatie over een publicatie/uitspraak is ontvangen. Het gaat om meerdere modules van hetzelfde type, waar in de loop van de tijd steeds nieuwe aan toegevoegd worden.

Drie tijdassen: _Bekend op_ / _Juridisch werkend op_ / _Geldig op_
: Deze doorsnede kan uitgewisseld worden door het overzicht van [complete toestanden](tijdreizen-toestanden.md) op te stellen voor elke waarde van de _Bekend op_. datum. Dus voor elke datum dat er publicaties via de LVBB zijn gedaan, en elke datum **TODO: welke is dat?** dat een rechter een uitspraak heeft gedaan die doorwerkt in de geconsolideerde regeling/informatieobject. Deze overzichten worden gemaakt op basis van de meest recente uitgewisselde informatie. Het gaat om meerdere modules van hetzelfde type, waarvan sommige bij het ontvangen van nieuwe informatie kunnen wijzigen of komen te vervallen.

Vier tijdassen: _Bekend op_ / _Ontvangen op_ / _Juridisch werkend op_ / _Geldig op_
: De volledige informatie kan uitgewisseld worden door het overzicht van [complete toestanden](tijdreizen-toestanden.md) op te stellen voor elke waarde van de _Bekend op_. datum. Dus voor elke datum dat er publicaties via de LVBB zijn gedaan, en elke datum **TODO: welke is dat?** dat een rechter een uitspraak heeft gedaan die doorwerkt in de geconsolideerde regeling/informatieobject, in combinatie met elke datum dat er nieuwe informatie over een publicatie/uitspraak is ontvangen. Het gaat om meerdere modules van hetzelfde type, waar in de loop van de tijd steeds nieuwe aan toegevoegd worden.

## Ontbrekende versies van een regeling/informatieobject
De consolidatieplicht van een bevoegd gezag betreft alleen de geldende regelgeving en daarmee ook de regelgeving die binnenkort geldig wordt. Als bijvoorbeeld een rechter een besluit (gedeeltelijk) vernietigt dat in werking is getreden vóór de inwerkingtreding van de huidig geldende regelingversie, dan hoeft het bevoegd gezag alleen aan te geven wat het effect op de huidig geldende regeling is. Er zullen dan toestanden zijn die wel het resultaat zijn van een van de vijf typen tijdreizen naar het verleden, maar waarvoor geen versie van de regeling en/of informatieobjecten beschikbaar is.

Het is mogelijk om de informatie van de toestanden, het versiebeheer en de [juridische verantwoording](bron_regelgeving.md) te combineren en zo voor een menselijke lezer altijd [inzichtelijk te maken](regelgeving-ontbrekende-versies.md) hoe de versie bij een toestand afgeleid kan worden. De menselijke lezer moet de afleiding zelf uitvoeren.
