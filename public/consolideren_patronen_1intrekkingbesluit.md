# Intrekking van een besluit

## Patroon
Het bevoegd gezag heeft een (wijzigings-)besluit gepubliceerd maar komt op een later tijdstip tot de conclusie dat het geen goed besluit is. Het bevoegd gezag publiceert een nieuw besluit waarin staat dat het eerdere besluit (deels) ingetrokken wordt. Zie ook de beschrijving van dat [besluit](besluit_intrekking.md).

## Uit te wisselen informatie

De intrekking van een besluit is juridisch een nieuw besluit met een eigen inwerkingtreding, waartegen bezwaar en beroep mogelijk kunnen zijn. Het verschil met een regulier wijzigingsbesluit is dat de formulering van de nieuwe versies van een regeling of informatieobject anders is: in plaats van nieuwe versies als onderdeel van het besluit te presenteren, staat beschreven dat teruggegaan wordt naar de versies die voor de inwerkingtreding van het in te trekken besluit geldig zijn.

![Intrekking besluit](img/consolideren_patronen_1intrekkenbesluit.png)

In STOP is het intrekkingsbesluit daarom de eerste publicatie voor een nieuw doel/branch (in de illustratie: C). In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het intrekkingsbesluit staat:

* Voor alle regelingen en/of informatieobjecten die gewijzigd worden voor het in te trekken doel/branch (A):
    * De versie voor het doel (in de illustratie: B) die is opgegeven als [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) van de regeling/informatieobject in de eerste publicatie voor het in te trekken doel (A) wordt op de [gebruikelijke wijze](consolideren_patronen_1versie.md) opgenomen.
    * De `eId` verwijst naar het `WijzigArtikel` waarin de intrekking van het besluit staat.
    * Als [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) wordt de oorspronkelijke basisversie (van doel B) opgevoerd (in de illustratie v6).

* Voor de regelingen en/of informatieobjecten die ingesteld worden voor het in te trekken doel/branch (A) wordt niets in de `ConsolidatieInformatie` vermeld.

De versies van de regelingen en informatieobjecten hoeven niet meegeleverd te worden met het besluit want deze zijn al eerder gepubliceerd.



