# Wijzigingsoverzicht

<!--
Beschrijf globaal wat de aanpassing opgeleverd heeft voor de gebruiker, 
en evt. waarom dat gedaan is.
Verwijs ook direct naar de aangepaste pagina's in de documentatie, 
en ook naar gewijzigde bestanden in de uitgeleverde (test-)git repository.
Zie werkwijze op: 
https://confluence-koop.overheid.nl/display/STOP/Wijzigingsoverzicht+STOP
-->

<!--
AANVULLEND VOOR PUBLICATIE NAAR DE WITTE SITE

Zolang naar de witte site wordt gepubliceerd, worden de wijzigingen per uitleverdatum beschreven, maar nog wel t.o.v. de eerdere versie van de standaard. Bij de voorbereiding van een publicatie naar de publieke site worden de wijzigingen weer bij elkaar geveegd.
-->

<!--
TODO: wijzigingen per uitleverdatum in elkaar schuiven door wijzigingen van latere uitleverdatums onder die van eerdere te zetten.
-->

## Wijzigingshistorie
Een aanpassing in het informatiemodel en de bedrijfsregels leidt tot een nieuwe versie van de standaard en bijbehorende documentatie. De documentatie kan worden bijgewerkt zonder dat het versienummer van de standaard wijzigt.

Op deze pagina staan de wijzigingen beschreven voor STOP @@@STOPVERSIE@@@. Wijzigingen in eerdere versies van de standaard staan beschreven in het eerder gepubliceerde wijzigingsoverzicht van [versie 1.3.0](https://koop.gitlab.io/STOP/standaard/1.3.0/stop_wijzigingsoverzicht.html). Wijzigingen in het implementatiemodel (IMOP) zijn [apart](imop_wijzigingsoverzicht.md) beschreven.

## Standaard 

### Uitleverdatum @@@DATUM@@@

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0: 

* [Voorstellen](voorstel_wijzigingen.md) voor het eenduidiger en eenvoudiger maken van het tekstmodel.

### Uitleverdatum 06-04-2022

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0:

* De STOP standaard is uitgebreid met de modules [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md) ten behoeve van [attenderingen](attenderen.md). Ook is een methodiek beschreven om [gebiedsmarkeringen deels geautomatiseerd](gebiedsmarkering_berekenen.md) te bepalen.
* Met een [Revisie](https://koop.gitlab.io/STOP/team/test7/EA_DBE8BF553C524b71BB84CFF11C7660CB.html) kan nu ook de zichtbaarheid van een geconsolideerde regeling beëindigd worden door deze als [Materieel uitgewerkt](https://koop.gitlab.io/STOP/team/test7/EA_DBE8BF553C524b71BB84CFF11C7660CB.html#Pkg__247423A4AB6C416eB111B647DE5D2D5E) aan te merken.
* Issue xxx: een status toegevoegd aan de expression identificatie van een [regeling](regeling_naamgeving.md) en [informatieobject](io-expressionidentificatie.md). Deze status moet gebruikt worden om de expression identificaties van regelingversies en informatieobjectversies uniek te maken bij gebruik in ontwerpbesluiten.
* Introductie van versiebeheer in STOP ten behoeve van de geautomatiseerde consolidatie. Zie ook de wijzigingen in de documentatie. Dit heeft effect op de [ConsolidatieInformatie](consolidatie-informatie.md) module waarin een breaking change zit t.o.v. STOP 1.3.0. Introductie van een ander overzicht voor toestanden: [overzicht actuele toestanden](consolideren_toestanden.md). Dit overzicht sluit beter aan bij de interactie tussen bevoegd gezagen en de LVBB voor het oplossen van consolidatieproblemen.
* In STOP treden regelingversies zoveel mogelijk als geheel in werking. Mocht een gedeeltelijke inwerkingtreding noodzakelijk zijn, dan biedt STOP nu mogelijkheden om ook in STOP [gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md) in goede banen te leiden.
* [se:FeatureTypeStyle](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle) en [gio:JuridischeBorgingVan](gio_xsd_Element_gio_JuridischeBorgingVan.dita#JuridischeBorgingVan) kunnen nu ook leeg zijn, zodat het verwijderen van deze annotaties bij een nieuwe GIO versie mogelijk is.
* [tekst:Mededeling](tekst_xsd_Element_tekst_Mededeling.dita#Mededeling) geïntroduceerd om het juridische onderscheid tussen een Kennisgeving en een Mededeling ook in STOP te kunnen maken. Een Mededeling wordt vooralsnog in STOP alleen gebruikt voor het publiceren van een [uitspraak van de rechter](besluitproces_rechterlijkemacht_scenarios.md).
* Om aan te kunnen geven dat een Begrip in een regeling gerelateerd is aan een begrip in een externe catalogus is de module [Begripsrelaties](regeling_begrippen.md) geïntroduceerd. 
* Omdat een [tekst:Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) ook een toelichting bij een consolideerbaar informatieobject (PDF-document) kan bevatten, is in de module [data:Toelichtingsrelaties](data_xsd_Element_data_Toelichtingsrelaties.dita#Toelichtingsrelaties) de mogelijkheid toegevoegd om met [data:toelichtingOp](data_xsd_Element_data_toelichtingOp.dita#toelichtingOp) een relatie tussen een toelichting en een [data:IO](data_xsd_Element_data_IO.dita#IO) te leggen.

## Bedrijfsregels

### Uitleverdatum @@@DATUM@@@

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0:

* Formulering [STOP2022](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2022) en [STOP2056](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2056) aangepast en zoveel mogelijk gelijkluidend gemaakt. Aanpassing betreft de `eId`-verwijzing in de Consolidatieinformatie van een ingetrokken- of teruggetrokken-informatieobject. De `eId`-verwijzing is niet altijd mogelijk, namelijk als geen gebruik gemaakt wordt van renvooi maar van [tekst:VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling) om een [tekst:ExtIoRef](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) te verwijderen. Daarvoor is de toevoeging *indien mogelijk* opgenomen.
* [STOP0089](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0089), [STOP0090](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0090) aangepast zodat `formula_2_instX` geen onterechte foutmeldingen geeft.

* [STOP0092](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0092) valideert nu dat een [`tekst:ExtIoRef`](tekst_xsd_Element_tekst_ExtIoRef.dita#ExtIoRef) *alleen* gebruikt mag worden om naar een consolideerbaar informatieobject te verwijzen. Consolideerbare informatie objecten bevatten 'regdata' in de join-ID. Voor verwijzingen naar alleen-bekend-te-maken 'pubdata' informatieobjecten moet [`tekst:ExtRef`](tekst_xsd_Element_tekst_ExtRef.dita#ExtRef) gebruikt worden, waarbij de link-tekst af kan wijken van de join-ID (bijvoorbeeld de titel van een rapport), hetgeen [STOP0012](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0012) afdwingt voor `ExtIoRef`.

### Uitleverdatum 15-06-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

*  [STOP0089](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0089), [STOP0090](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0090) en [STOP0091](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0091) toegevoegd om de [voorgeschreven wijze](eid_wid.md) van samenstellen van deze eId en wId attributen te valideren.
* STOP0013/STOP0014/STOP0044 (*Een `@eId`/`@wId`/een onderdeel van een `@wId` MAG NIET eindigen met een punt '.'* ) verwijderd. Deze validaties zijn nu als 'Facetten' opgenomen in het schema, zie [dtEID](tekst_xsd_Simple_Type_tekst_dtEID.dita#dtEID) en [dtWID](tekst_xsd_Simple_Type_tekst_dtWID.dita#dtWID).
* Formulering van [STOP2030](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2030) veralgemeniseerd, zodat deze regel ook van toepassing is in de context van een uitwisseling tussen BG en adviesbureau en niet alleen bij een aanlevering aan het BHKV.
* [STOP2075](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2075) toegevoegd dat het vierde deel van de AKN-identifier van een besluit overeen moet komen met `data:eindverantwoordelijke`.
* [STOP2076](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2076) toegevoegd dat het vierde deel van de AKN-identifier van een initiële regeling overeen moet komen met `data:eindverantwoordelijke`.

### Uitleverdatum 16-05-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* STOP1029 *Een doel kan maar één datum inwerkingtreding hebben* vervangen door  [STOP1077](businessrules_STOP_1000.dita#Bedrijfsregels__br_STOP1077) en [STOP1078](businessrules_STOP_1000.dita#Bedrijfsregels__br_STOP1078) per `soortTijdstempel`. 
* STOP2065 opgesplitst in [STOP2072](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2072), [STOP2073](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2073) en [STOP2074](businessrules_STOP_2000.dita#Bedrijfsregels__br_STOP2074).

### Uitleverdatum 06-04-2022

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0:

* [Issue #199](https://gitlab.com/koop/STOP/standaard/-/issues/199): Bedrijfsregel [STOP3040](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3040) toegevoegd om de bestaande beperking op het gebruik van cirkels en bogen expliciet te maken.
* [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md):
  * [STOP3018](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3018),  [STOP3019](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3019) en [STOP3090](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3090) herschreven zodat ze niet alleen toepasbaar zijn voor een [GIO](gio-intro.md), maar ook voor [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md).
  * [STOP3050](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3050) toegevoegd dat alleen RD coördinaten gebruikt mogen worden voor [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md).
  * [STOP3060](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3060) toegevoegd dat de geometrie van een [Effectgebied](effectgebied.md) een vlak moet zijn.
* [Gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md): 
  * [STOP0060](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0060), [STOP0063](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0063) en [STOP0070](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0070)  aangepast t.b.v. [tekst:NogNietInWerking](tekst_xsd_Element_tekst_NogNietInWerking.dita#NogNietInWerking)
  * [STOP0087](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0087) toegevoegd t.b.v. [tekst:NogNietInWerking](tekst_xsd_Element_tekst_NogNietInWerking.dita#NogNietInWerking)
* [STOP3018](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3018),  [STOP3019](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3019) en [STOP3090](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3090) herschreven zodat ze niet alleen toepasbaar zijn voor een [GIO](gio-intro.md), maar ook voor [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md).
* [STOP3050](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3050) toegevoegd dat alleen RD coördinaten gebruikt mogen worden voor [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md).
* [STOP3060](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3060) toegevoegd dat de geometrie van een [Effectgebied](effectgebied.md) een vlak moet zijn.
* [STOP0088](businessrules_STOP_0.dita#Bedrijfsregels__br_STOP0088) toegevoegd dat het tekstmodel van een regeling niet mag wijzigen door een [tekst:VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling).
* [STOP3176](businessrules_STOP_3100.dita#Bedrijfsregels__br_STOP3176) toegevoegd die controleert of [se:FeatureTypeStyle](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle) leeg is òf de drie noodzakelijk elementen bevat. 
* Weergave van de bedrijfsregels per module gecorrigeerd voor een aantal bedrijfsregels.  
* STOP0061 verwijderd: door een aanpassing in het schema wordt het verbod op [tekst:Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie) binnen [tekst:Kennisgeving](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving) nu door het schema afgedwongen.
* [STOP1076](STOP1076) toegevoegd dat [data:mededelingOver](data_xsd_Element_data_mededelingOver.dita#mededelingOver) moet verwijzen naar een reeds gepubliceerd besluit.
* [STOP1322](businessrules_STOP_1300.dita#Bedrijfsregels__br_STOP1322) toegevoegd dat bij een mededeling alleen de procedurestap *Beroep(en) definitief afgedaan* voor kan komen.
* Voor de module [Begripsrelaties](regeling_begrippen.md) [STOP1039](businessrules_STOP_1000.dita#Bedrijfsregels__br_STOP1039), [STOP1040](businessrules_STOP_1000.dita#Bedrijfsregels__br_STOP1040) en [STOP1041](businessrules_STOP_1000.dita#Bedrijfsregels__br_STOP1041) toegevoegd.

## Documentatie 

### Uitleverdatum @@@DATUM@@@

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0:

* Toelichting op het gebruik van de `bekendOp` en `instrument` in de [Consolidatie-informatie](consolidatie-informatie.md) toegevoegd.

### Uitleverdatum 29-06-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* Toelichting op het gebruik van de `eId` in de [Consolidatie-informatie](consolidatie-informatie.md) uitgebreid.
* Opmerking toegevoegd op [Identificatie van tekstelementen](eid_wid.md) dat als `Sluiting` meer dan één keer voorkomt, "*_instX*" aan de eId en wId wordt toegevoegd om ze uniek te maken.

### Documentatiestructuur per 16-05-2022

Wijzigingen in @@@IMOPVERSIE@@@ t.o.v. 1.3.0:

* De documentatiestructuur van de standaard is vereenvoudigd. Voorheen [Leeswijzers](https://koop.gitlab.io/STOP/team/test10/leeswijzers.html) is nu opgesplitst in (datum geeft laatste wijziging aan):
  * [Uitgangspunten](stop_uitgangspunten.md): uitwerking van in de standaard gehanteerde uitgangspunten qua scope, toepassingsgebied, juridisch, architectuur, et cetera. Nieuw of uitgebreid zijn:
       * Relatie van STOP met [toepassingsprofielen](toepassingsprofielen.md) (**nieuw**, @@@DATUM@@@). 
       * [Uitgangspunten](regelgeving_digitaal.md) voor regelingen in STOP (**nieuw**, , @@@DATUM@@@).
       * Relatie met [andere standaarden](andere_standaarden.md) (**uitgebreid**, 06-04-2022)
  * [Besluiten en regelingen](stop_gebruik.md): in de volgorde van het proces van de totstandkoming van nieuwe regelgeving wordt toegelicht hoe STOP dit proces ondersteunt. Hierin is te vinden:
       * [Starten van een project](versiebeheer_in_stop.md) (**nieuw**, 06-04-2022) beschrijft de principes van versiebeheer in STOP en hoe dat toe te passen is bij het starten van een project.
       * [Opstellen van regelgeving](regelgeving.md) beschrijft (**nieuw**, @@@DATUM@@@) de tekstmodellering in STOP en de toepassing daarvan voor regelingen en (**TODO**: nog bijwerken) informatieobjecten, inclusief (**TODO**: nog bijwerken) annotaties.
       * [Samenwerken aan regelgeving](samenwerken_aan_regelgeving.md) (**bijgewerkt**, 06-04-2022) beschrijft de uitwisseling tbv o.a. adviesbureaus; inhoudelijk niet gewijzigd.
       * [Opstellen van een besluit](besluit.md) (**uitgebreid**, 06-04-2022) beschrijft hoe (wijzigingen van) regelingen en informatieobjecten in een besluit verwerkt moeten worden, en hoe de modellen voor een besluit in elkaar zitten. Beschrijft ook renvooi en proefversies. **TODO** nog te doen: gedetailleerde uitwerking renvooi en proefversies.
       * [Consolideren](consolideren.md) (**nieuw**, 06-04-2022) Beschrijving van de geautomatiseerde consolidatie en wat daar voor nodig is. Bevat [consolidatiepatronen](consolideren_patronen.md) waar het bevoegd gezag mee te maken kan krijgen. Voor de LV's zijn de overige onderdelen van belang. 
       * [Regelgeving in de loop van de tijd](regelgeving_in_de_tijd.md) (**uitgebreid**, 06-04-2022) gaat over tijdreizen conform DSO-architectuur.
  * [Officiële publicaties](stop_publicaties.md): uitwerking van die aspecten van de standaard die voor officiële publicaties nodig zijn, zoals de bekendmaking van besluiten, rectificaties, kennisgevingen en mededelingen. Hierin is te vinden:
      * Documentatie over [Gebiedsmarkering](gebiedsmarkering.md) en [Effectgebied](effectgebied.md) (**nieuw**, 06-04-2022), ook opgenomen in het [moduleoverzicht](imop_modules.md) en [informatiemodel](EA_E8EDA73EF3B842a9AB27E3B3E23B1470.dita#Pkg).


### Uitleverdatum 16-05-2022

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0:

* Documentatie toegevoegd over de relatie van STOP met [toepassingsprofielen](toepassingsprofielen.md). 
* Documentatie toegevoegd over de [uitgangspunten](regelgeving_digitaal.md) voor regelingen in STOP. 
* Beschrijving van [begrippen in een regeling](regeling_begrippen.md) in STOP opgenomen.


### Uitleverdatum 06-04-2022

Wijzigingen in @@@STOPVERSIE@@@ t.o.v. 1.3.0:

* Documentatie toegevoegd over toepassing en gebruik van het instrument [revisie](consproces_revisie.md). 
* De term proefconsolidatie vervangen door de term [proefversie](EA_354F82ADD7C2441cBF0FA7D361B63C75.dita#Pkg).
* Documentatie toegevoegd over toepassing en gebruik van het instrument [revisie](consproces_revisie.md). 
* In [Vaststellen en wijzigen van GIO's](vaststellen-wijzigen-gio.md) verwijzing naar de wettelijke grondslag van de GIO in de bekendmakingswet opgenomen.
  * Beschrijving van [gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md) in STOP opgenomen.