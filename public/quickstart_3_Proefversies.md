# cons:Proefversies

De instrumentversies (zoals RegelingVersie en GIOversie) die voortvloeien uit een ontwerpbesluit treden niet in werking en kennen dus geen toestanden. De LVBB kan op basis van het ontwerpbesluit wel de instrumentversie samenstellen in een Proefversie. 

| Element                                                   | Vulling                                                      | Verplicht? |
| --------------------------------------------------------- | ------------------------------------------------------------ | ---------- |
| [bekendOp](cons_xsd_Element_cons_bekendOp.dita#bekendOp)                             | De datum waarop eenieder kennis kon hebben van bepaalde informatie. In de meeste gevallen is dit de bekendmakingsdatum van de officiële publicatie. | J          |
| [ontvangenOp](cons_xsd_Element_cons_ontvangenOp.dita#ontvangenOp)                       | De datum waarop het STOP-gebruikend systeem kennis heeft van bepaalde informatie en die informatie ook kan en mag verstrekken. | N          |
| [Proefversie](cons_xsd_Element_cons_Proefversie.dita#Proefversie)                       | Een door de LVBB samengestelde proefversie van een besluit (dit kunnen er 1 of meer zijn) | J          |
| [Proefversie/gerealiseerdeDoelen/doel](cons_xsd_Element_cons_doel.dita#doel)     | Het doel wordt overgenomen uit het corresponderende ontwerpbesluit. Een Proefversie kan meerdere doelen realiseren. Zie: [data:ConsolidatieInformatie](quickstart_2_ConsolidatieInformatie.md) | J          |
| [Proefversie/instrumentVersie](cons_xsd_Element_cons_instrumentVersie.dita#instrumentVersie) | Zie [data:ExpressionIdentificatie - Regeling](quickstart_1_ExpressionIdentificatieRegeling.md) voor de FRBRExpression van een RegelingVersie. Zie [data:ExpressionIdentificatie - Informatieobject](quickstart_1_ExpressionIdentificatieGIO.md) voor de FRBRExpression van een informatieobjectversie. | J          |





