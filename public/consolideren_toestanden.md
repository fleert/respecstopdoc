# Overzicht van toestanden

Vanaf STOP versie 2.0.0 zijn er twee manieren om de toestanden van een geconsolideerde regeling of geconsolideerd informatieobject te beschrijven:

1. Op basis van alleen de datum inwerkingtreding: [cons:ActueleToestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden). Dit is het eenvoudigste model dat het beste de [geautomatiseerde consolidatie](consolideren_principe.md) ondersteunt.
1. Op basis van zowel inwerkingtreding als geldigheid: [cons:CompleteToestanden](cons_xsd_Element_cons_CompleteToestanden.dita#CompleteToestanden). Met dit model wordt alle informatie ontsloten die nodig is voor alle mogelijke [tijdreizen](regelgeving_in_de_tijd.md).

Het eerste model wordt hier beschreven; het tweede wordt [op de pagina over tijdreizen](tijdreizen-toestanden.md) toegelicht. De principes achter dit model zijn toegelicht in de beschrijving van de [principes van de geautomatiseerde consolidatie](consolideren_principe.md).

Het overzicht van de actuele toestanden is een module die beschrijft welke toestanden er op een bepaald moment in de tijd zijn voor de consolidatie van een enkele regeling of informatieobject. Als er iets wijzigt aan de toestanden dan ontstaat een nieuw overzicht dat het eerdere overzicht vervangt. De module is een [contextmodule](context_modules.md); een systeem dat deze module opstelt kan ervoor kiezen een selectie van toestanden op te nemen.

Het overzicht bevat een lijst met toestanden. Elke toestand heeft als informatie:

| Kenmerk | Betekenis |
| --------- | --------- |
| expression-identificatie | Identificatie van de toestand volgens de [naamgevingsconventie](consolidatie_naamgeving.md) |
| Juridisch werkend op | De periode waarin de toestand juridisch geldig is. Voor de toestanden in het overzicht geldt dat er op elk moment in de tijd hooguit één geldige toestand is. |
| Inwerkingtredingsdoelen | Het doel of de doelen van alle versies of wijzigingen die op deze datum in werking treden. |
| Basisversie-doelen | Een lijst met de inwerkingtredingsdoelen alle toestanden die voorafgaand aan deze toestand in werking getreden zijn. |
| Instrumentversie | De expression-identificatie van de versie van de regeling of het informatieobject die overeenkomt met de inhoud van de toestand. Het is niet gegarandeerd dat voor een toestand een instrumentversie bekend is. Als de instrumentversie onbekend is, dan geven de volgende kenmerken aan waarom dat is. |
| Meerdere gelijktijdige versies | Als voor een toestand met meerdere inwerkingtredingsdoelen niet één versie voor alle doelen is aangegeven, dan is dit de lijst van doelen waarvoor verschillende versies bestaan. Per versie wordt één doel vermeld. |
| TeVerwerken | Als voor een toestand geen regelingversie beschikbaar is die alle doelen van die toestand bevat, is dit de lijst van doelen die nog verwerkt moeten worden. |
| Ontvlechten | Als voor een toestand een regelingversie beschikbaar is die meer doelen bevat dan de toestand, is dit de lijst van doelen die nog ontvlochten moeten worden. |

Naast de lijst van toestanden heeft de module vier datums:
* [Bekend op](cons_xsd_Element_cons_bekendOp.dita#bekendOp): de datum van de meest recente publicatie die verwerkt is in dit overzicht. Uitwisseling van [revisies](begrippenlijst_revisie.dita#revisie) tellen hierin niet mee. Deze datum geeft aan waarop eenieder kennis kon hebben van de juridische informatie waarop dit overzicht is gebaseerd.
* [Ontvangen op](cons_xsd_Element_cons_ontvangenOp.dita#ontvangenOp): de datum van de meest recente uitwisseling (publicatie of revisie). Deze datum moet groter of gelijk zijn aan _Bekend op_. Als deze datum afwijkt van _Bekend op_ geeft dat aan dat het systeem dat de module samenstelt een periode heeft gekend waarin de juridische informatie al wel publiek bekend was maar nog niet in het systeem verwerkt was. Deze datum is optioneel; het systeem dat de module opstelt mag ervoor kiezen deze datum niet te verstrekken.
* [Juridisch uitgewerkt](cons_xsd_Element_cons_juridischUitgewerktOp.dita#juridischUitgewerktOp): de datum waarop de regeling of informatieobject is ingetrokken. Deze datum wordt ook in de einddatum van de laatst geldende toestand verwerkt. Als de regeling of informatieobject niet is ingetrokken wordt deze datum weggelaten.
* [Materieel uitgewerkt](cons_xsd_Element_cons_materieelUitgewerktOp.dita#materieelUitgewerktOp): de datum waarop de regeling (of informatieobject) geen relevantie meer heeft. De regeling is nog wel juridisch geldig maar kan gearchiveerd worden. Als de regeling of informatieobject nog niet materieel is uitgewerkt wordt deze datum weggelaten.