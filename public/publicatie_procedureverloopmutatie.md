# Procedureverloopmutatie 

Een kennisgeving of mededeling bevat vaak informatie over het verloop van de besluitprocedure. Een kennisgeving bevat indien van toepassing bijvoorbeeld de inzage-, bezwaar- en beroepstermijnen van een besluit. Een mededeling kan betrekking hebben op een rechterlijke uitspraak over een besluit. Dit zijn voorbeelden van stappen in de besluitvormingsproces en het bevoegd gezag levert deze stappen door middel van een procedureverloopmutatie bij een kennisgeving of mededeling. 

De [Procedureverloopmutatie](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) is generiek van opzet. En bestaat uit: 

- de datum [bekendOp](data_xsd_Element_data_bekendOp.dita#bekendOp) met de datum waarop deze informatie bekend is geworden.
- een of meer van de volgende acties:
  1. [voegStappenToe](data_xsd_Element_data_voegStappenToe.dita#voegStappenToe),
  2. [vervangStappen](data_xsd_Element_data_vervangStappen.dita#vervangStappen),
  3. [verwijderStappen](data_xsd_Element_data_verwijderStappen.dita#verwijderStappen).

Elke actie kan één of meer stappen betreffen. Door middel van [soortStap](data_xsd_Element_data_soortStap.dita#soortStap) wordt aangegeven welke stap het betreft en wanneer de stap is voltooid ([voltooidOp](data_xsd_Element_data_voltooidOp.dita#voltooidOp)). Eventueel ook nog wie de stap heeft uitgevoerd ([actor](data_xsd_Element_data_actor.dita#actor)) en een verwijzing naar [meerInformatie](data_xsd_Element_data_meerInformatie.dita#meerInformatie) over de stap of consequenties daarvan.

**Zie ook**

* Uitgebreide uitleg over het [muteren van het procedureverloop](muteren_procedureverloop.md).
* [Toelichting](besluitproces_rechterlijkemacht_procedurele_status.md) op de verschillende procedurestappen bij een rechterlijke procedure.
* Het [informatiemodel](EA_85DA3D0FDE4F4d4a9F848DDFAC3591B4.dita#Pkg/242F0CD5E62E407aB90DD26FB083443F) van het procedureverloop en de procedureverloopmutatie.
* [XML schema](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie) van de procedureverloopmutatie.
* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-ProcedureMutatie.xml) van een procedureverloopmutatie bij een kennisgeving.