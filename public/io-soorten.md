# Soorten informatieobjecten

## Op basis van juridische status

Op basis van de juridische status (zoals vermeld in de [publicatie-instructie](io-publicatie-instructie.md)) van een informatieobject zijn er drie soorten informatieobjecten te onderscheiden:
1. Het **informatieve informatieobject**. Dit object speelt juridisch gezien geen rol.
1. Het **alleen bekend te maken informatieobject**. Dit object hoort juridisch gezien bij de *besluitvorming*, maar is *geen onderdeel van de juridische regels/regeling* die wordt vastgesteld in het besluit.
1. Het **te consolideren informatieobject**.

Het BG kan de juridische status niet zelf bepalen. Deze wordt bepaald door de toepassingsprofielen, zoals TPOD’s.

## Vermelding in de tekst
Afhankelijk van de juridische status uitgedrukt in de [publicatieinstructie](io-publicatie-instructie.md) van het informatieobject kan een verwijzing naar het informatieobject ook terugkomen in de tekst van het besluit of de regeling:


### Informatief informatieobject
* Komt in de praktijk nog niet voor. Het betreft een reservering voor de toekomst.
* Alleen relevant binnen de context van het besluit. 
* Wordt niet geconsolideerd. Er bestaat altijd maar één geldige versie van het informatieobject.
* Er mag niet vanuit het besluit of de regeling naar het informatieobject worden verwezen, want het is geen juridisch onderdeel van het bekend te maken besluit.


### Alleen bekend te maken informatieobject

Meestal zijn `AlleenBekendteMaken` informatieobjecten onderzoeksverslagen die wettelijk verplicht toegevoegd moeten worden aan een besluit. Deze worden daarom zelden vermeld in de tekst van het besluit.

Voor een `AlleenBekendteMaken` informatieobject geldt het volgende:

* Het is alleen relevant binnen de scope van het besluit. 
* Het wordt niet geconsolideerd. Er bestaat altijd maar één geldige versie van het informatieobject.
* Er mag in de *regelingtekst* *niet* naar worden verwezen. Dit geldt zowel voor het klassieke als compacte informatiemodel.  Een verwijzing naar deze IO mag dus niet in de `WijzigBijlage` van een [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact), een  [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) of in de renvooitekst van een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie).
* Er mag vanuit de *besluittekst* in het `Lichaam` van een [BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact) of `BesluitKlassiek` naar het informatieobject worden verwezen, maar dat is niet verplicht. Dat is namelijk een tekst die niet in de geconsolideerde regeling belandt. 
* Een besluittekst-verwijzing betreft altijd een **statische** verwijzing. De JOIN-ID is dus van de *expression* van de IO.

### Te consolideren informatieobject { #TeConsolideren }
* Onderdeel van de juridische regels vanuit de (geboorte)regeling die naar het informatieobject verwijst. 
* Moet worden geconsolideerd.
* Is een zelfstandige juridische entiteit: het kent een consolidatie los van een regeling. 
* Verwijst naar de regeling-versie waarin deze versie van het informatieobject voor het eerst wordt genoemd. De [versiemetadata van de IO](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) bevat dan een verwijzing naar de regelingversie [geboorteregeling](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling), waarin het object voor het eerst wordt genoemd.
* *Moet* naar worden verwezen vanuit de *regelingtekst* die na consolidatie onderdeel is van de geconsolideerde regeling.
* Kan naar worden verwezen vanuit andere regelingen. Verwijst een besluit naar een IO uit een andere (geboorte)regeling, dan hoeft de inhoud van dat IO niet (opnieuw) toegevoegd aan het besluit van de regeling. Zo kan een regeling verwijzen naar een bestaande GIO als het werkingsgebied waar ook deze regeling van toepassing is.

## Op basis van bestandsformaat {#bestandsformaat}

Op basis van bestandsformaat worden de volgende informatieobjecten onderscheiden door STOP:
1. [Geografisch informatieobject](gio-intro.md) (GIO).
1. [PDF-document](IO_Document.md). In principe moet alle tekst opgemaakt worden conform een van de tekstmodellen die in STOP beschikbaar zijn. Er zijn scenario's waarbij dat niet mogelijk is, of waarbij dat onevenredig veel moeite kost. In dat geval kan gebruik gemaakt worden van PDF-document. Het gebruik wordt sterk afgeraden en is alleen toegestaan als dat in de toepassingsprofielen voor de betreffende besluiten of regelingen is aangegeven.

In de toekomst kan het mechanisme voor informatieobjecten gebruikt worden voor andere bestandsformaten, zoals geluidsfragmenten of videobeelden. Deze zijn niet in STOP @@@IMOPVERSIE@@@ opgenomen.

<figure>
  <img src="img/Presentatiemodel_Informatieobjecten.png" width="500px" alt="Theoretisch mogelijke formaten van informatieobjecten"/><figcaption>Theoretisch mogelijke formaten voor informatieobjecten</figcaption>
</figure>


