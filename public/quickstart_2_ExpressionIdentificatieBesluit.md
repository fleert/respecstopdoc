# data:ExpressionIdentificatie (Besluit)

Deze module voorziet in de identificatie van het Besluit en volgt hierin de De [Akoma Ntoso](https://docs.oasis-open.org/legaldocml/akn-nc/v1.0/os/akn-nc-v1.0-os.html) naamgevingsconventie (AKN NC). 

| Element                                   | Vulling                                                      | Verplicht? |
| ----------------------------------------- | ------------------------------------------------------------ | ---------- |
| [FRBRWork](data_xsd_Element_data_FRBRWork.dita#FRBRWork)             | Zie 'AKN IRI - Besluit'                                      | N          |
| [FRBRExpression](data_xsd_Element_data_FRBRExpression.dita#FRBRExpression) | Zie 'AKN IRI - Besluit'                                      | J          |
| [soortWork](data_xsd_Element_data_soortWork.dita#soortWork)           | Voor een aangeleverd Besluit dient deze de waarde `'/join/id/stop/work_003'` te hebben (NB dit geldt voor zowel een Definitief Besluit als een Ontwerpbesluit). | J          |



## AKN IRI - Besluit

Voor de identificatie van het aangeleverde Besluit dienen de AKN IRIs als volgt te zijn opgebouwd (uitgaande van een Nederlands bevoegd gezag):

`//FRBRWork: "/akn/nl/bill/" <overheid> "/" <datum_work> "/" <overig> `

`//FRBRExpression: <FRBRWork> "/" <taal> "@" <datum_expr> [ ";" <versie> ] [ ";" <overig> ]`

[Toelichting](besluit_naamgeving.md)



## Voorbeeld

[Voorbeeld ExpressionIdentificatie (Besluit)](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/02-BG-Besluit-Identificatie.xml)