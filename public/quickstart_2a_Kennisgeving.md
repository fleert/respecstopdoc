# Kennisgeving

Een kennisgeving zoals decentrale overheden die publiceren bestaat uit een korte tekst waarin staat dat:

* een ontwerpbesluit ter inzage ligt gedurende een bepaalde periode waarin op het ontwerpbesluit gereageerd kan worden,
* tegen een defintief besluit bezwaar en/of beroep aantekenen mogelijk binnen een bepaalde periode.

In STOP is de [kennisgeving](tekst_xsd_Element_tekst_Kennisgeving.dita#Kennisgeving) is een apart Work dat verwijst naar het besluit, en wordt opgebouwd volgens het [vrijetekstmodel](begrippenlijst_vrijetekststructuur.dita#vrijetekststructuur). 

De terinzagestelling kan digitaal zijn, bijvoorbeeld via officielebekendmakingen.nl, of fysiek, bijvoorbeeld door een afspraak te maken op het gemeentehuis. De bijbehorende inzagetermijn staat in de tekst van de kennisgeving en de data van de inzagetermijn worden als [Procedureverloopmutatie](quickstart_2a_Procedureverloopmutatie.md) aangeleverd in de stappen "begin inzagetermijn", "eind inzagetermijn", "einde bezwaartermijn", "einde beroepstermijn".

De wijze waarop bezwaar of beroep ingesteld kan worden wordt in de kennisgeving in tekst toegelicht.


## Voorbeelden
* [Voorbeeld tekst van een kennisgeving ontwerpbesluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/01%20Initieel%20ontwerpbesluit%20met%20kennisgeving/02-BG-Kennisgeving-Tekst.xml)

* [Voorbeeld tekst van een kennisgeving definitief besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/01%20Beroep%20mogelijk/02-BG-Kennisgeving-Tekst.xml)

  

