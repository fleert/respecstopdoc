# Begrippenlijst

**A**
: [Annotatie](begrippenlijst_annotatie.dita), [Artikelstructuur](begrippenlijst_artikelstructuur.dita), [Authentiek besluit](begrippenlijst_authentiekBesluit.dita)

**B**
: [Beschikking](begrippenlijst_beschikking.dita), [Besluit](begrippenlijst_besluit.dita), [Branch](begrippenlijst_branch.dita), [Bronhouderkoppelvlak](begrippenlijst_bronhouderkoppelvlak.dita)

**C**
: [Consolidatie](begrippenlijst_consolidatie.dita)

**D**
: [Doel](begrippenlijst_doel.dita)

**E**
: [eId](begrippenlijst_eId.dita)

**G**
: [Geografisch informatieobject](begrippenlijst_geografischInformatieobject.dita)

**I**
: [Informatieobject](begrippenlijst_informatieobject.dita), [Initieel besluit](begrippenlijst_initieelBesluit.dita), [Intrekkingsbesluit](begrippenlijst_intrekkingsbesluit.dita), [Inwerkingtredingsbesluit](begrippenlijst_inwerkingtredingsbesluit.dita)

**J**
: [Juridisch authentiek product](begrippenlijst_juridischAuthentiekProduct.dita)

**K**
: [Kenmerken](begrippenlijst_kenmerken.dita), [Kennisgeving](begrippenlijst_kennisgeving.dita)

**M**
: [Mededeling](begrippenlijst_mededeling.dita), [Mutatie-eenheid](begrippenlijst_mutatieeenheid.dita), [Muteren](begrippenlijst_muteren.dita)

**N**
: [Naamgevingsconventie](begrippenlijst_naamgevingsconventie.dita)

**O**
: [Officiële bekendmaking](begrippenlijst_officieleBekendmaking.dita)

**R**
: [Rectificatie](begrippenlijst_rectificatie.dita), [Regelgeving](begrippenlijst_regelgeving.dita), [Regelgeving-gerelateerde informatie](begrippenlijst_regelgevingGerelateerdeInformatie.dita), [Regeling](begrippenlijst_regeling.dita), [Regelingstructuur](begrippenlijst_regelingstructuur.dita), [Regeltekst](begrippenlijst_regeltekst.dita), [Revisie](begrippenlijst_revisie.dita)

**S**
: [Samenloop](begrippenlijst_samenloop.dita), [Service-informatie](begrippenlijst_serviceInformatie.dita), [Serviceproduct](begrippenlijst_serviceproduct.dita), [Structuurelementen](begrippenlijst_structuurelementen.dita)

**T**
: [Tijdreizen](begrippenlijst_tijdreizen.dita), [Toepassingsprofiel](begrippenlijst_toepassingsprofiel.dita), [Toestand](begrippenlijst_toestand.dita)

**V**
: [Verrijkt besluit](begrippenlijst_verrijktBesluit.dita), [Vervanging](begrippenlijst_vervanging.dita), [Vrijetekststructuur](begrippenlijst_vrijetekststructuur.dita)

**W**
: [Werkingsgebied](begrippenlijst_werkingsgebied.dita), [wId](begrippenlijst_wId.dita), [Wijzigingsbesluit](begrippenlijst_wijzigingsbesluit.dita)