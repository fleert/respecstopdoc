# Toepassingsprofielen

## "Handleiding voor STOP"
STOP biedt vaak meerdere modellen om hetzelfde te bereiken. Zo zijn er verschillende modellen voor [besluiten](besluit_modellering.md) en voor de [tekst](regeling.md#modellen) en [data](io-soorten.md) die samen de regelgeving vormt. Ook het te volgen proces om te komen tot wettelijk geldende regelgeving en de publicaties die daarvoor gedaan moeten worden zijn afhankelijk van de soort besluit of regelgeving. STOP biedt allerlei mogelijkheden, maar schrijft niet voor welke mogelijkheden gebruikt moeten worden in een specifieke situatie.

Voor het toepassen van de standaard voor een specifiek soort regelgeving moet een selectie gemaakt worden uit de mogelijkheden die STOP biedt. Dat wordt beschreven in een toepassingsprofiel. Een toepassingsprofiel is een handleiding voor het gebruik van STOP voor een specifiek soort regelgeving. Een toepassingsprofiel is zelf geen onderdeel van STOP.

![Toepassingsprofiel](img/Toepassingsprofiel.png)

## Opstellen van een toepassingsprofiel

Het opstellen van een nieuw toepassingsprofiel begint ermee te bepalen: als de eerste versie van de soort regelgeving wordt vastgesteld, wat moet er dan in het instellingsbesluit van het bevoegd gezag staan en hoe wordt dat opgeschreven? De uitkomst zal meestal afhangen van conventies; zo worden algemeen verbindende voorschriften in de vorm van artikelen opgeschreven. Maar er kunnen ook specifieke wettelijke vereisten zijn over welke informatie aanwezig moet zijn. Waar STOP alleen vormvereisten stelt aan een besluit, kan een toepassinggsprofiel ook iets over de stijl van de inhoud zeggen. Daarnaast kan een toepassingprofiel aangeven of (en welke) teksten niet volgens een STOP-tekstmodel gecodeerd hoeven te worden maar wel samen met het besluit gepubliceerd moeten worden, zoals onderzoeksrapporten.

Zodra bekend is wat er in het instellingsbesluit moet staan, worden de voorschriften en (beleids-)regels daaruit apart gezet. Uit de beschikbare modellen voor regelgeving in STOP wordt het passende model gekozen om deze voorschriften en regels mee te beschrijven. Voor de overige informatie in het instellingsbesluit wordt een passend model gekozen uit een van de beschikbare modellen voor een besluit. Als er geen passende modellen gevonden kunnen worden, kan dat aanleiding zijn nieuwe modellen in een volgende versie van STOP te introduceren.

Daarna wordt bepaald welke wettelijke vereisten er zijn om eenieder op de hoogte te stellen van de verschillende stappen in het besluitvormingsproces. STOP beschrijft al de algemene eisen voor bekendmaken van een besluit en het doen van kennisgevingen over het besluit, maar voor specifieke regelgeving kunnen aanvullende vereisten gelden. Het toepassingsprofiel beschrijft welke publicaties er gedaan moeten worden, wat daar in moet staan, en legt vast welke STOP modellen daarvoor gebruikt moeten worden.

## Vastleggen van een toepassingsprofiel

STOP kent geen mechanisme of model om een toepassingsprofiel op een machine-leesbare manier vast te leggen. De ervaring met het opstellen van [toepassingsprofielen voor Omgevingswetbesluiten](https://www.geonovum.nl/geo-standaarden/omgevingswet/STOPTPOD) is dat het grootste deel van een toepassingsprofiel een vertaling is van domein-specifieke onderwerpen naar de manier waarop daarover in een besluit of regelgeving geschreven wordt, en een stijlgids om dat duidelijk op te schrijven. Dat is niet op een generieke machine-leesbare manier vast te leggen.

STOP heeft wel een [voorziening](data_xsd_Element_data_soortRegeling.dita#soortRegeling) om bij een aantal modellen, zoals een regeling, vast te leggen om welk soort regeling het gaat. De opsomming van soorten is een waardelijst die geen onderdeel is van STOP. Dit biedt software die voor een bepaald domein is geschreven een haakje om het van toepassing zijnde toepassingsprofiel vast te leggen of te herkennen en op basis daarvan een implementatie van de richtlijnen te activeren.
