# 4. Voeg GIO-modules toe

Het toevoegen van de GIO-modules aan een besluit over een regeling, gebeurt als volgt: 
   
1. Zorg ervoor dat alle relevante STOP-modules van het GIO de juiste onderdelen bevatten. Het gaat om:
   1.  [`ExpressionIdentificatie`](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie). Zie de uitleg hierover in de sectie over [informatieobjecten](io-onderdelen.md)).
   1. [`InformatieObjectMetadata`](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata). Zie de uitleg hierover in de sectie over [informatieobjecten](io-onderdelen.md).
   1. [`InformatieObjectVersieMetadata`](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata). Zie de uitleg hierover in de sectie over [informatieobjecten](io-onderdelen.md)).
   1. [`GeoInformatieObjectVaststelling`](geo_xsd_Element_geo_GeoInformatieObjectVaststelling.dita#GeoInformatieObjectVaststelling). Zie de secties [Onderdelen GIO](gio-onderdelen.md) en [Soorten GIO's](gio-soorten.md).
1. Voeg de STOP-modules van het GIO bij de besluit-modules.


## Zie ook 
Voor voorbeelden van hoe GIO-modules zijn toegevoegd aan een besluit, zie [Voorbeelden van verschillende soorten besluiten met GIO's](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO) met verschillende typen geometrische data.
