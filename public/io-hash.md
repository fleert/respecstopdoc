# Hash

Zodra een `TeConsolideren` of `AlleenbekendTeMaken` informatieobject een bijlage wordt van een besluit dat ter vaststelling wordt aangeboden, moet het voorzien zijn van een unieke, door het BG gegenereerde [SHA512-hash](data_xsd_Element_data_hash.dita#hash) als cryptografische sleutel. Deze sleutel wordt opgeslagen in de [`InformatieObjectVersieMetadata`](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata), de metadata voor de versie van een informatieobject. Deze hash wordt gebruikt bij `AlleenBekendTeMaken` en `TeConsolideren` informatieobjecten omdat volgens de wet de onveranderlijkheid van deze informatieobjecten moet worden geborgd door het systeem (de LVBB) dat het informatieobject officieel bekendmaakt. De LVBB moet daarom kunnen aantonen dat de inhoud van het informatieobject tijdens de verwerking op geen enkele manier wordt gewijzigd. 

## BG verantwoordelijk tot en met aanlevering
Het bevoegd gezag is verantwoordelijk voor de onveranderlijkheid van de inhoud van het besluit. Dit is zij vanaf het moment dat het besluit is vastgesteld tot en met de aanlevering aan de LVBB, en (behoudens amendementen) vanaf het ter vaststelling aanbieden tot en met de vaststelling zelf. 

## LVBB verantwoordelijk van aanlevering tot bekendmaking
De LVBB zorgt ervoor dat de inhoud van het besluit onveranderlijk is tussen de aanlevering en de bekendmaking. Omdat het voor informatieobjecten in het algemeen moeilijk is aan te tonen dat de inhoud ongewijzigd is, stelt STOP de onveranderlijkheid verplicht van informatieobjecten (het bestand / de serialisatie) als onderdeel van het besluit.

## Zie ook
* [Cryptografische hash-functies](https://nl.wikipedia.org/wiki/SHA-familie).
* [Voorbeeld van  InformatieObjectVersieMetadata](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/01%20Nieuwe%20regeling%20met%20GIO/01-BG-Vuurwerkverbodsgebied-v1-versieMetadata.xml)
