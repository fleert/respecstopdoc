var respecConfig =
{
  //voor specStatus mogelijkheden zie https://github.com/Logius-standaarden/respec/wiki/specStatus
  specStatus: "WV",
  //voor specType mogelijkheden zie https://github.com/Logius-standaarden/respec/wiki/specType
  specType: "ST",
  pubDomain: "dk",
  shortName: "template",
  publishDate: "2022-07-13",
  publishVersion: "2.0.0-respec",
  previousPublishVersion: "2.0.0",
  //  previousPublishDate: "(none)",
  //  previousMaturity: "WV",
  editors:
    [
      {
        name: "Kennis- en Exploitatiecentrum voor Officiële Overheidspublicaties",
        company: "KOOP",
        companyURL: "https://www.koopoverheid.nl",
      }
    ],
  authors:
    [
      {
        name: "Team STOP",
        company: "KOOP",
        companyURL: "https://www.koopoverheid.nl/contact",
      }
    ],
  // Ik denk dat we gitlab zouden willen gebruiken.  
  //  github: "https://github.com/KOOP/TOOI",
  
 // Controls if linked "§" section markers are added to a document
 addSectionLinks: true,
 
    // this parameter will add the tag_name of the latest release to the document Title
  // only set this parameter when a release has been set
  nl_addReleaseTagTitle: true,

    // nl_organisationName is used for some company specific values in the header (and Sotd)
  // currently supported: Logius and Geonovum (default)  
  nl_organisationName: "KOOP",
  
  // this url points to the folder where organsation specific css files are stored
  // defaults to https://tools.geostandaarden.nl/respec/style/ if not set
//  nl_organisationStylesURL: "https://publicatie.centrumvoorstandaarden.nl/respec/style/",
  nl_organisationStylesURL: "./style/",

  // prefix for the names of company specific css, svg and ico prefixes
  // defaults to "GN-"  
  nl_organisationPrefix: "LS-",

  // if nl_markdownEmbedImageInFigure is set to true images in markdown generated content will be surrounded with <figures> element
  // so that figures can be linked are be a part of table of figures
  nl_markdownEmbedImageInFigure: true,

  // nl_organisationPublishURL points to organisation specifica publication page, used in header
  // defaults to  https://docs.geostandaarden.nl/"
  nl_organisationPublishURL: "https://publicatie.koop.nl/",

  // nl_logo refers to company logo
  // defaults to https://tools.geostandaarden.nl/respec/style/logos/Geonovum.svg
  nl_logo: {
    src: "https://publicatie.centrumvoorstandaarden.nl/respec/style/logos/figure-logius.svg",
    alt: "Logius",
    id: "Logius",
    height: 77,
    width: 44,
    url: "https://www.logius.nl/standaarden",
  },


  localBiblio: {
    "SemVer": {
      href: "https://semver.org",
      title: "Semantic Versioning 2.0.0",
      authors: ["T. Preston-Werner"],
      date: "June 2013"
    }
  },
};
