# Er is een toestand volgend op terugtrekking

## Patroon
Een publicatie leidt tot het vervallen van een toestand omdat een versie voor een doel/branch wordt teruggetrokken of omdat de inwerkingtreding wordt uitgesteld. Er was al een opvolgende toestand ontstaan door een publicatie die gebaseerd was op de ingetrokken of uitgestelde publicatie. De opvolgende toestand is nu niet meer valide. 

## Uit te wisselen informatie

In essentie is de oplossing de uitwisseling van een nieuwe versie van de regeling/informatieobject voor het inwerkingtredingsdoel van de vervangende toestand, waarbij de wijzigingen uit de eerder geldende toestand zijn verwijderd via [ontvlechting](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie). De vorm waarin dat moet gebeuren hangt af van de variant.

## Varianten
De varianten ontstaan doordat eerst nagegaan moet worden of het verwijderen van de wijzigingen zonder meer gedaan kunnen worden, dus of het (deel van het) besluit dat teruggetrokken wordt juridisch verenigbaar is met het besluit dat tot de opvolgende toestand leidt. Bijvoorbeeld: een wijziging van een artikel is alleen juridisch verenigbaar met een versie waarin dat artikel al bestond. 

Als de wijzigingen niet juridisch verenigbaar zijn, dan moet het bevoegd gezag een nieuw besluit nemen  over hoe de wijzigingen moeten luiden nu de basisversie gewijzigd is. Er zijn drie varianten te onderscheiden.

![Ontvlechten](img/consolideren_patronen_2ontvlechten.png)

In de illustratie is A de doel/branch van de teruggetrokken versie/inwerkingtreding en B van de opvolgende toestand. Het ontvlechten moet tijdig gebeuren, zodat de consolidatie beschikbaar is zodra de datum van inwerkingtreding van doel B is aangebroken, of zoveel eerder dan volgens implementatieafspraken vereist is.

### Juridisch onafhankelijke besluiten
Er is geen nieuw besluit nodig om te komen tot een ontvlochten versie. Er wordt een revisie voor doel B uitgewisseld. De revisie bevat de nieuwe versie van de regeling/informatieobject waaruit de wijzigingen uit doel/branch A zijn verwijderd.

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van de revisie staat elke nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat doel B.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch B.
* In [OntvlochtenVersie](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch A.

### Juridisch niet-verenigbare besluiten, korte besluitperiode
Er is een nieuw besluit nodig om te komen tot een ontvlochten versie. Het bevoegd gezag is in staat dat besluit tijdig te nemen, en er is geen bezwaar of beroep mogelijk. Het nieuwe besluit bevat wijzigingen voor doel B en beschrijft de nieuwe versie van de regeling(en)/informatieobject(en) waarin de wijzigingen uit doel/branch A zijn overgenomen.

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het nieuwe besluit staat elke nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat doel B.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch B.
* In [OntvlochtenVersie](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch A.

### Juridisch niet-verenigbare besluiten, uitgebreid besluittraject
Er is een nieuw besluit nodig om te komen tot een ontvlochten versie. Het bevoegd gezag is niet in staat dat besluit tijdig te nemen en/of er is bezwaar of beroep mogelijk. Het nieuwe besluit moet in STOP als een regulier wijzigingsbesluit gezien worden, voor een nieuw doel (in de illustratie C) omdat niet op voorhand duidelijk is of en wanneer het besluit in werking treedt, terwijl de besluiten voor doelen A en B al wel in werking kunnen zijn. De intentie zal zijn dat het nieuwe besluit tegelijk met het besluit voor doel B in werking treedt.

Het nieuwe besluit bevat wijzigingen voor doel C en beschrijft de nieuwe versie van de regeling(en)/informatieobject(en) waarin de wijzigingen uit doel/branch A en B zijn verwerkt. In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van het nieuwe besluit staat elke nieuwe versie op de [gebruikelijke manier](consolideren_patronen_1versie.md) vermeld, met:
* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staan doel B en C.
* In [Basisversie](data_xsd_Element_data_Basisversie.dita#Basisversie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch B.
* In [OntvlochtenVersie](data_xsd_Element_data_OntvlochtenVersie.dita#OntvlochtenVersie) staat een verwijzing naar de laatst gepubliceerde versie voor doel/branch A.

