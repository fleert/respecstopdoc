# Soorten GIO's


## Op basis van functie
Er zijn wat inhoud en/of gebruik betreft verschillende soorten GIO's: 

1. een **GIO als werkingsgebied**. Dit is een GIO met een of meerdere locaties die allemaal hun eigen geometrische data hebben. Voor informatie over het onderdeel locaties, zie de sectie [locaties](gio-locaties.md). Voor informatie over deze GIO's, zie de sectie [GIO als werkingsgebied](gio-werkingsgebied.md).  
1. een **GIO met normwaarden**. Dit is een GIO met een norm die per locatie een normwaarde heeft. Voor informatie over  het onderdeel normwaarde in een locatie, zie de betreffende paragraaf in de sectie [locaties](gio-locaties.md). Voor informatie over GIO's met normwaarden, zie de sectie [GIO met normwaarden](gio-met-normwaarden.md).
1. een **GIO met GIO-delen**. Dit is een GIO die is opgedeeld in groepen waarvoor afzonderlijke of aanvullende regels gelden. Voor informatie over het onderdeel GIO-delen-overzicht, zie de sectie [GIO-delen-overzicht](gio-delen.md). Voor informatie over GIO's met GIO-delen, zie de sectie [GIO-delen bij de sectie Locaties](gio-locaties.md).
1. een **pons-GIO**. Dit is een GIO als werkingsgebied die slechts wordt gebruikt om aan te geven dat andere regelingen niet langer van toepassing zijn voor een bepaald gebied. Dit wordt bijvoorbeeld gebruikt om de geldigheid te beperken van de bestemmingsplannen die van rechtswege deel uitmaken van een omgevingsplan. Voor informatie over de pons-GIO, zie de sectie [Pons-GIO](gio-pons.md).


## Op basis van totstandkoming
Op basis van de manier waarop een GIO tot stand is gekomen, zijn er twee vormen te onderscheiden. Bij beide vormen kan het gaan om een GIO als werkingsgebied, met normwaarden, met GIO-delen of een pons-GIO. 

De twee vormen zijn:
1. een **niet-berekende GIO**. Bij niet berekende GIO's  volgt de nauwkeurigheid uit de gebruikte achtergrond, zie [vaststellingscontext](gio-vaststellingscontext.md). Het opgeven van een nauwkeurigheid in een GIO impliceert dus dat de GIO berekend is.
1. een **berekende GIO**. Dit is een GIO die is berekend in plaats van "getekend" en waarbij de nauwkeurigheid van de geometrische data wordt aangegeven. Voor informatie over de berekende GIO, zie de sectie [berekende GIO](gio-berekend.md).

De soorten GIO's worden hieronder toegelicht.


