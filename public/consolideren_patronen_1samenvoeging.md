# Samenvoeging van versies

## Patroon
In een te publiceren besluit is één nieuwe versie van een regeling (en eventuele informatieobjecten) beschreven die een samenvoeging (en eventuele wijziging) is van twee versies die eerder (voor verschillende doelen) in aparte besluiten zijn gepubliceerd. 

Het bevoegd gezag kiest dus niet voor de [optie](consolideren_patronen_2gelijktijdig.md) om beide versies in het nieuwe besluit te noemen en beide versies dezelfde inwerkingtreding te geven. In dat geval moet de samenvoeging achteraf doormiddel van een revisie uitgevoerd worden en is, mocht het een voor beroep vatbaar besluit zijn, alleen tegen het gezamenlijke inwerkingtredingsbesluit beroep mogelijk. 

![](img/consolideren_patronen_1samenvoegenInleiding.png)

In dit patroon (1 in de illustratie) wordt de samenvoeging vòòr de publicatie uitgevoerd. Dit patroon zal daarom gebruikt worden als de samengevoegde versie verder gewijzigd moet worden voordat het resultaat in werking treedt. In dit patroon is ook het volledige samenvoegingsbesluit voor beroep vatbaar, mocht beroep tegen dit besluit mogelijk zijn.

## Uit te wisselen informatie

Als voor een doel/branch (verder: branch) eenmaal een publicatie is gedaan, blijft deze branch altijd bestaan. Een branch kan dan niet meer opgaan in een andere branch. Dus ook als de wijzigingen van  branch A inmiddels verwerkt zijn in branch B, moet branch A toch ook inwerking treden samen met branch B. Anders zal het in [bron van de geldende regelgeving](bron_regelgeving.md) lijken of de wijzigingen van branch A nooit in werking zijn getreden. 

Technisch bestaat de samenvoeging daarom uit het steeds voor twee doelen publiceren van dezelfde inwerkingtreding en regeling/informatieobjectversies. De uit te wisselen informatie hangt af van de eerder uitgewisselde versies.

![Samenvoeging](img/consolideren_patronen_1samenvoegen.png)

Als de eerdere versies aan elkaar gerelateerd zijn, B is bijvoorbeeld een 'uitbreiding' van A, dan vormt de versie met wijzigingen voor beide doelen/branches de basis voor de verdere aanpassing. In de illustratie is dat de versie voor branch B die gebaseerd is op de versie voor branch A.

Als dat niet zo is, A en B hebben bijvoorbeeld allebei een artikel 4 wat niet gelijkluidend is, dan moet eerst voor een van de branches (in de illustratie: branch B) de samengestelde versie als een revisie worden uitgewisseld. In het voorbeeld: één van de twee artikelen 4 moet een nieuw nummer krijgen, zodat beide reeds gepubliceerde artikelen deel uit blijven maken van de nieuwe gezamenlijke versie. 

Het besluit dat de samengestelde versie wijzigt beschrijft de wijziging ten opzichte van de laatst aangeleverde versie voor branch B. Voor alle regelingen en informatieobjecten die voor de twee doelen worden ingesteld of gewijzigd moet in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) een versie worden opgenomen. Dat is ofwel de laatst aangeleverde versie voor branch B, ofwel de in het besluit gewijzigde versie, met bij [doelen](data_xsd_Element_data_doelen.dita#doelen) ingevuld: beide doelen (A en B).

Als er daarna nog wijzigingen nodig zijn gedragen de doelen/branches A en B zich als versies die [tegelijk in werking treden](consolideren_patronen_2gelijktijdig.md) en waarvoor al een gezamenlijke versie is uitgewisseld. 

## Implementatie

Als er een revisie nodig is voordat de publicatie uitgewisseld kan worden met wijzigingen op de samenvoeging, dan kunnen de niet-juridische revisie mutaties en de juridische nieuwe wijzigingen gecombineerd worden in één [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie). Om er voor te zorgen dat in de publicatie van zo'n wijzigingsbesluit alleen de juridische wijzigingen getoond worden, moet gebruik gemaakt worden van het attribuut [`revisie=1`](tekst_xsd_Attribute_Group_tekst_agVervang.dita#agVervang) op [Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang). Hiermee wordt aangegeven dat het een niet-juridische wijziging betreft, die niet getoond wordt in het wijzigingsbesluit en die ook niet terugkomt in de [bron van de geldende regelgeving](bron_regelgeving.md). 

Omdat de kleinste eenheid van wijziging een [artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel) of [divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) is, kan het voorkomen dat het niet mogelijk is om onderscheid te maken tussen de juridische en niet-juridische wijzigingen. Bijvoorbeeld omdat in één artikel zowel een lid vernummert moet worden als een nieuw lid toegevoegd. In deze situaties wordt de mutatie als juridisch aangemerkt. 

