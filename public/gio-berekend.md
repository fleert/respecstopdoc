# Niet-berekende en berekende GIO's

## Niet-berekende GIO
De meeste GIO's bevatten geometrische data die tot stand komen door vergelijking van deze data met een achtergrondkaart. Bij deze GIO's  volgt de nauwkeurigheid uit de gebruikte achtergrond, zie [vaststellingscontext](gio-vaststellingscontext.md). 


## Berekende GIO
Sommige GIO's bevatten geometrische data die ontstaan door berekening. Daarbij is het voor de juiste juridische interpretatie van belang te weten en vast te leggen met welke nauwkeurigheid het GIO is berekend. Daarom kan in de [vaststellingscontext](gio-vaststellingscontext.md) van een GIO de nauwkeurigheid worden opgegeven in decimeters. Het opgeven van een nauwkeurigheid in een GIO betekent dat het GIO is berekend. Een berekende GIO kan wel of niet een achtergrondkaart hebben. Een voorbeeld van een berekende GIO is een geluidscontour. 

## Onderdelen berekende GIO

Een berekende GIO kan zijn:
* een GIO als werkingsgebied, 
* een GIO met normwaarden, 
* een GIO met GIO-delen, 
* pons-GIO. 


Een berekende GIO onderscheidt zich doordat de module [**vaststellingscontext**](gio_xsd_Element_gio_GeografischeContext.dita#GeografischeContext) altijd het element [**`nauwkeurigheid`**](gio_xsd_Complex_Type_gio_GeografischeContextType.dita#GeografischeContextType_nauwkeurigheid) bevat. Het betreft de nauwkeurigheid van de geometrische data van het object ten opzichte van de werkelijkheid (zoals in de achtergrond is weergegeven) uitgedrukt in decimeters.
   