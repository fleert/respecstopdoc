# GIO met GIO-delen

Men gebruikt een GIO met GIO-delen als een set regels over een bepaald thema regels bevat die zowel betrekking hebben op een aantal (niet-overlappende) deelgebieden als op het hele werkingsgebied. Men kan een GIO met GIO-delen ook gebruiken in het scenario dat normwaarden in de tekst worden vastgelegd, en de deelwerkingsgebieden in een GIO met GIO-delen. 

## Onderdelen
Een GIO met GIO-delen dat onderdeel is van een besluit heeft de GIO-specifieke onderdelen van een [juridisch geldige GIO](gio-onderdelen.md#GIO-specifieke-onderdelen), maar heeft:
* *geen* norm, 
* (verplicht) [symbolisatie](gio-symbolisatie.md). 

## Voorbeeld
Voor het "beperkingengebied Schiphol" gelden regels die voor het hele gebied gelden. Ook zijn er zones waarbinnen aanvullende of afwijkende regels gelden. Hiervoor is één GIO "beperkingengebied Schiphol" gebruikt. Deze bevat GIO-delen met zones die verschillende regels hebben. Bij elke locatie is aangegeven bij welke GIO-deel deze locatie hoort. Op de kaart van het GIO wordt elk GIO-deel getoond aan de hand van een legenda.

<img src="img/GIO-BeperkingengebiedSchiphol.png" alt="Afbeelding GIO met beperkingengebied Schiphol" width="500px" />
