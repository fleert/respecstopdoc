# InformatieObjectMetadata

De module [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata) bevat de *work*-metadata van een informatieobject.


## Overzicht onderdelen InformatieObjectMetadata

| Onderdeel | Inhoud | Verplicht? |
| ---------- | ---------- | ---------- |
| [alternatieveTitels](data_xsd_Element_data_alternatieveTitel.dita#alternatieveTitel)[alternatieveTitels](data_xsd_Element_data_alternatieveTitels.dita#alternatieveTitels) | Alternatieve titel(s) voor het informatieobject. Deze wordt in de volksmond gebruikt en is geen officiele benaming. | Nee          |
| [eindverantwoordelijke](data_xsd_Element_data_eindverantwoordelijke.dita#eindverantwoordelijke)      | Zie de waardenlijsten met overheden: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | Ja          |
| [formaatInformatieobject](data_xsd_Element_data_formaatInformatieobject.dita#formaatInformatieobject)  | Zie waardenlijst [formaatinformatieobject.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/formaatinformatieobject.xml) | Ja          |
| [maker](data_xsd_Element_data_maker.dita#maker)                                      | Zie waardenlijsten met overheden: [gemeente.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/gemeente.xml), [provincie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/provincie.xml), [waterschap.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/waterschap.xml), [ministerie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/ministerie.xml) | Ja          |
| [officieleTitel](data_xsd_Element_data_officieleTitel.dita#officieleTitel)                    | Officiële titel van het informatieobject. Dit is altijd de JOIN-ID van het IO-*work*, omdat de JOIN-ID de juridische identificatie van het IO is. | Ja          |
| [publicatie-instructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie)        | Juridische status van het informatieobject. | Ja          |

