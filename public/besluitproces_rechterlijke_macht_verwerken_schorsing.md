# Verwerken schorsing
Tijdens een beroepsprocedure kan een beroepsorgaan in een tussenuitspraak het betreffende besluit schorsen. Een schorsing geeft het BG altijd via een direct mutatie door met een [`Procedureverloopmutatie`](data_xsd_Element_data_Procedureverloopmutatie.dita#Procedureverloopmutatie), zodat de wijzigingen van het besluit in de geconsolideerde regeling voorzien worden van een vlag 'geschorst'. Onderdelen:
* [`soortStap`](data_xsd_Element_data_soortStap.dita#soortStap) is hierbij altijd  `/join/id/stop/procedure/stap_019` (Nieuwe schorsing).
* [`voltooidOp`](data_xsd_Element_data_voltooidOp.dita#voltooidOp) bevat de datum waarop het besluit is geschorst.
* (optioneel) [`meerInformatie`](data_xsd_Element_data_meerInformatie.dita#meerInformatie), link naar de BG-website met meer informatie over de gevolgen van een schorsing voor de juridische werking van het besluit. Dit is niet de URL van de bekendmaking.


Voor de betekenis van `BekendOp` bij deze procedurestap, zie [BekendOp](besluitproces_rechterlijkemacht_procedurele_status.md#BekendOp).

## Voorbeeld procedureverloopmutatie schorsing

```
<Procedureverloopmutatie>
  <bekendOp>2020-06-01</bekendOp>
  <voegStappenToe>
    <Procedurestap>
      <!-- Schorsing  -->
      <soortStap>/join/id/stop/procedure/stap_019</soortStap>
      <voltooidOp>2020-06-01</voltooidOp>
      <meerInformatie>https://www.pyrodam.nl/actueel/schorsing-87904</meerInformatie>
    </Procedurestap>
  </voegStappenToe>
</Procedureverloopmutatie>
```


## Zie ook
* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).