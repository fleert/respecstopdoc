# 3. Neem GIO op als beoogd informatieobject

Om een GIO toe te voegen aan een besluit, is het noodzakelijk om deze als beoogd informatieobject op te nemen in de consolidatie-informatie. Het GIO wordt bij deze stap geassocieerd met een doel binnen een [Beoogde regeling](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/DE42934181E24a2197045C71533BC1DC).

## Voorbeeld

```
<ConsolidatieInformatie xmlns="https://standaarden.overheid.nl/stop/imop/data/">
   <BeoogdeRegelgeving>
      <BeoogdeRegeling>...</BeoogdeRegeling>
      <BeoogdInformatieobject xmlns="https://standaarden.overheid.nl/stop/imop/data/">
         <doelen>
            <doel>/join/id/proces/gm0307/2020/InstellingVuurwerkVerordening</doel>
         </doelen>
         <instrumentVersie>/join/id/regdata/gm0307/2019/gio993859238/nld@2019-12-20;1</instrumentVersie>
         <eId>!regeling#cmp_A__content_o_1__list_1__item_1__ref_1</eId>
      </BeoogdInformatieobject>
  </BeoogdeRegelgeving>
  <Tijdstempels>...</Tijdstempels>
</ConsolidatieInformatie>
```

## Toelichting
* De `instrumentVersie` bevat de JOIN-ID van de GIO-expression.
* De `eId` van het `BeoogdInformatieobject` verwijst naar de plaats binnen de besluittekst waar de verwijzing (`ExtIoRef`) naar het informatieobject staat. Deze `ExtIoRef` heeft zijn eigen `@eId`. Omdat die (behalve bij de [pons](gio-pons.md)) meestal in de [tekstbijlage](gio-opnemen-bijlage.md) van een regeling staat die een component is binnen de besluittekst, moet ook de componentnaam worden opgenomen. 