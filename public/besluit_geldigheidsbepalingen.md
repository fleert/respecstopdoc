# Geldigheidsbepalingen { .LATER }

In de huidige praktijk is het niet ongebruikelijk dat in het besluit geldigheidsbepalingen staan anders dan de inwerkingtreding en eventuele terugwerkende kracht. De werking van het besluit wordt bijvoorbeeld beperkt tot een bepaalde doelgroep of bepaalde scenario's. Ook kan er sprake zijn van overgangsrecht, waarbij in het besluit is opgenomen dat onder bepaalde voorwaarden de bestaande regeling nog geldig is.

De standaard staat het gebruik van dergelijke geldigheidsbepalingen niet toe. Het effect van zo'n bepaling is dat er feitelijk twee (of meer) versies van een regeling geldig zijn: de bestaande versie van een regeling en de nieuwe versie uit het besluit. STOP kent geen mogelijkheden om dat aan te geven. Dat is een bewuste keuze: er is altijd maar één geldende versie van een regeling, en STOP-gebruikende systemen zijn daar op ingericht.

Het is in STOP wel mogelijk om uitgebreide geldigheidsbepalingen te handeren. Er zijn twee mogelijkheden:

* De bepalingen en nieuwe regels worden toegevoegd aan de bestaande regelingversie.
* De nieuwe regels worden ondergebracht in een tijdelijk regelingdeel en de bepalingen worden in de conditie opgenomen.
