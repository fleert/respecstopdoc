# Intrekking van een regeling

## Patroon
Een regeling is niet meer nodig; de juridische werking ervan wordt beëindigd. Een informatieobject wordt niet meer in een regeling gebruikt en heeft geen juridische werking meer.

Met een intrekking wordt de juridische geldigheid dus beëindigd. Het is *geen* intrekking in de zin dat de regeling met terugwerkende kracht ongedaan wordt gemaakt.

![](img/consolideren_patronen_1intrekkingregeling.png)

## Uit te wisselen informatie
De intrekking van een regeling moet vermeld staan in een juridisch document. De intrekking van een informatieobject [volgt](io-intrekken.md) uit de wijziging van de tekst van de geboorteregeling.

In de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module behorend bij het juridisch document wordt een [Intrekking](data_xsd_Element_data_Intrekking.dita#Intrekking) element opgenomen met:

* In [doelen](data_xsd_Element_data_doelen.dita#doelen) staat één doel, namelijk het doel waarvoor de regeling of het informatieobject wordt ingetrokken.
* In [instrument](data_xsd_Element_data_instrument.dita#instrument) staat de work-identificatie van de in te trekken regeling of informatieobject.
* Het [eId](data_xsd_Element_data_eId.dita#eId) element verwijst:
    * voor een regeling: naar het `WijzigArtikel` in het juridisch document waarin staat dat de regeling ingetrokken wordt.
    * Voor een informatieobject: zie [elders](io-intrekken.md).

Er hoeft geen rekening gehouden worden met andere patronen. Op de datum van inwerkingtreding zoals opgegeven voor het doel zal de juridische werking van de regeling beëindigd worden. Eventuele later in werking tredende wijzigingen van de regeling (of informatieobject) worden genegeerd.

## Varianten

### Intrekking en vervanging: opvolging
Voor een intrekking van een regeling of informatieobject kan gekozen worden omdat een ingrijpende wijziging nodig is. Het besluit met een wijziging van de bestaande regeling/informatieobject zou slecht te begrijpen zijn. In dat geval wordt een nieuwe regeling (of nieuw informatieobject) geïntroduceerd voor hetzelfde doel. In één besluit zal dan zowel de intrekking als de formulering van de nieuwe regeling/informatieobject staan.

Naast het op de [gebruikelijke wijze](consolideren_patronen_1versie.md) instellen van de nieuwe regeling/informatieobject moet in de metadata van de nieuwe regeling/informatieobject worden aangegeven dat het de [opvolger](data_xsd_Element_data_opvolging.dita#opvolging) is van de ingetrokken regeling/informatieobject.

### Intrekking van rechtswege 
In wet- en regelgeving kan voor een type regeling bepaald zijn dat de regeling automatisch vervalt onder bepaalde condities. Bijvoorbeeld een vaste periode na inwerkingtreding, zoals bij voorbeschermingsregels. De standaard voorziet niet in een faciliteit om regels te formuleren en toe te passen om intrekking van rechtswege te modelleren. Het bevoegd gezag dient de wet- en regelgeving zelf toe te passen en de resulterende datum door te geven.

## Implementatiekeuzes
Als een regeling ingetrokken wordt, zullen ook de works die daar juridisch onderdeel van zijn hun juridische werking verliezen, zoals de informatieobjecten die de regeling als [geboorteregeling](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) hebben en de tijdelijk regelingdelen die de regeling als [hoofdregeling](data_xsd_Element_data_isTijdelijkDeelVan.dita#isTijdelijkDeelVan) hebben. Het is een implementatiekeuze van de STOP-gebruikende systemen om deze informatieobjecten impliciet in te trekken (dus alleen de intrekking door te geven voor de geboorteregeling/hoofdregeling) of voor elk van de regelingen en informatieobjecten afzonderlijk. Een tijdelijk regelingdeel kan ingetrokken worden terwijl de hoofdregeling nog in werking blijft.

Bij een intrekking van rechtswege waarbij de datum van intrekking al bij de instelling van de regeling bekend is, is het een implementatiekeuze van de STOP-implementerende systemen of die intrekking al bij de instelling meegegeven kan worden in de `ConsolidatieInformatie`. (Het systeem van) het bevoegd gezag kan er ook zelf voor kiezen om de intrekking van rechtswege pas enige tijd voorafgaand aan de datum van intrekking uit te wisselen. In het toepassingsprofiel voor het type regeling kunnen hier nadere eisen aan gesteld worden.