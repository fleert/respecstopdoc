# Effectgebied

De [Regeling elektronische publicaties](https://wetten.overheid.nl/BWBR0045086/2022-01-01#Bijlage1) vereist dat publicaties over bepaalde typen besluiten voorzien worden van een effectgebied. Het effectgebied wordt gebruikt om inwoners en geïnteresseerden te [attenderen](https://www.overheid.nl/berichten-over-uw-buurt) op een besluit dat naar verwachting ook voor hen gevolgen zal hebben. Het effectgebied is anders dan de [gebiedsmarkering](gebiedsmarkering.md):

* **gebiedsmarkering** = gebied waar het besluit betrekking op heeft.
* **effectgebied** = gebied waar mogelijk gevolgen van het besluit merkbaar zijn, uitgedrukt in een cirkel met een straal van minimaal 3000 meter.

De straal van de cirkel is minimaal 3000 meter omdat de attenderingservice gebruikers tot maximaal 3 kilometer van hun adres attendeert op basis van de gebiedsmarkering van een besluit. Veel besluiten hebben geen effectgebied omdat de gevolgen van het besluit alleen in de buurt van de gebiedsmarkering merkbaar zullen zijn. Een voorbeeld van een besluit dat wel een effectgebied heeft, is een besluit over een meststoffen-verwerkende onderneming die geuroverlast kan veroorzaken in de wijde omgeving. 


## Modellering in STOP

Het effectgebied is in STOP [gemodelleerd](geo_xsd_Element_geo_Effectgebied.dita#Effectgebied) als een zelfstandige [module](imop_modules.md) dat met een besluit, kennisgeving of rectificatie meegeleverd kan worden. Zie ook het [informatiemodel](EA_E8EDA73EF3B842a9AB27E3B3E23B1470.dita#Pkg).

Het effectgebied bestaat uit één of meerdere [Gebied](geo_xsd_Complex_Type_geo_GebiedType.dita#GebiedType)-en die (optioneel) van een [label](geo_xsd_Complex_Type_geo_GebiedType.dita#GebiedType_label) en een [geometrie](geo_xsd_Complex_Type_geo_GebiedType.dita#GebiedType_geometrie) worden voorzien. Voor de codering van de geometrie wordt, net als in het GIO, gebruik gemaakt van de [basisgeometrie-standaard](https://docs.geostandaarden.nl/nen3610/basisgeometrie/) van Geonovum. 

**Beperkingen**

Uit compatibiliteitoverwegingen zijn geen cirkels of bogen [toegestaan](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3040) in de geometrie. Een cirkel moet via [verstroken](https://www.geonovum.nl/uploads/documents/20140801_praktijkrichtlijn_verstroken_cirkelbogen.pdf) opgebouwd worden uit rechte lijnsegmenten (een convex [regular polygon](https://en.wikipedia.org/wiki/Regular_polygon)). Dit hoeft niet ten koste te gaan van de nauwkeurigheid: berekend kan worden dat bij 64 lijnsegmenten het verschil in oppervlakte slechts 0,2% is.

Daarnaast [moet](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3060) het effectgebied een vlak zijn en [moeten](businessrules_STOP_3000.dita#Bedrijfsregels__br_STOP3050) de coördinaten uitgedrukt zijn in het [Rijksdriekhoekstelsel](https://nl.wikipedia.org/wiki/Rijksdriehoeksco%C3%B6rdinaten).


**Voorbeeld**

* [XML voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/09%20Effectgebied) van een effectgebied bij een besluit.
