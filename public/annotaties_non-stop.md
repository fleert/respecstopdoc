# Non-STOP annotaties

Het annotatiemechanisme van STOP kan ook toegepast worden voor informatie die volgens andere standaarden of afspraken is gecodeerd. Bij elke versie van de annotatie moet dan naast de inhoud uitgewisseld worden:

* Het doel (of doelen) waarvoor de annotatie is gemaakt.
* De work-identificatie van de regeling/informatieobject waarvoor de annotatie van toepassing is.

Bovendien moet bij de uitwisseling van de non-STOP annotatie rekening gehouden worden met:

1. Dezelfde versie van de annotatie kan meer dan eens uitgewisseld worden. Bij het vervlechten van versies bijvoorbeeld kan de versie van een van te vervlechten branches ook de versie voor de vervlochten versie zijn.
2. Als een annotatie een aspect van de regeling/informatieobject beschrijft dat in een volgende versie van de regeling/informatieobject afwezig kan zijn, dan moet er een manier zijn om in plaats van een nieuwe versie van de annotatie iets uit te wissel dat de "niet van toepassing"-status weergeeft. In STOP wordt dat gedaan door de inhoud van de module (afgezien van het rootelement) leeg te laten. In een latere versie van de regeling/informatieobject kan dan weer een versie van de annotatie uitgewisseld worden die de "niet van toepassing"-status opheft.
3. Alle overige geldigheidsinformatie, inclusief de datum waarop een regeling ingetrokken (juridisch uitgewerkt) of materieel uitgewerkt is, wordt niet in of met de annotatie uitgewisseld maar afgeleid uit de geldigheid van de overeenkomstige toestanden van de regeling/informatieobject.

Ook voor non-STOP annotaties geldt de eis dat een bevoegd gezag de verantwoordelijkheid heeft een versie van de annotatie uit te wisselen als dat vanwege de verandering van de regeling/informatieobject noodzakelijk is.

Een voorbeeld van een non-STOP annotatie is een OW-object zoals dat in de IMOW standaard beschreven is. (**TODO**: dat is in release A niet zo want IMOW voldoet niet aan (3) - ook niet aan (1) maar dat is niet erg want oplossen van samenloop zit nog niet in A.)  Dit is ook een type I annotatie.

**TODO**: welke eis moet gesteld worden om een annotatie te kunnen valideren?