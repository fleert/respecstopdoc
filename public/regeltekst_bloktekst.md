# Soort tekstelement: Bloktekst

De Bloktekstelementen bevatten de broodtekst of 'lopende' tekst van het instrument. Een of meer bloktekstelementen vormen samen de [inhoud](regeltekst_bloktekst.md) van een [structuur](regeltekst_structuur.md)element. De bloktekst bestaat uit aaneengesloten zinnen. 

> "[broodtekst](https://nl.wikipedia.org/wiki/Broodtekst)" of "lopende tekst" is de tekst met [inlines](regeltekst_inline.md), het lopend proza van de tekst, waarmee "de schrijver zijn brood verdient". In XML-terminologie wordt dit "*mixed content*" genoemd.

Met de bloktekstelementen wordt de broodtekst voorzien van specifieke formattering. Broodtekst is niet alleen de basis voor de tekstblokken, maar ook voor specifieke onderdelen daarin, zoals een bijschrift bij een figuur. Afhankelijk van de context zijn er beperkingen op het gebruik van [inline](regeltekst_bloktekst.md)elementen: *platte tekst*: is tekst zonder inlines, *koptekst*: tekst met beperkte formattering daarbinnen, *broodtekst zonder verwijzingen*: tekst met formattering, *broodtekst*: tekst met alle mogelijke inlines.

De term "broodtekst" wordt dus zowel in algemene zin gebruikt om alle lopende tekst van een regeling aan te duiden ongeacht beperkingen op de toegestane inlines, als meer specifiek: om een specifiek deel van de lopende tekst aan te duiden waar alle mogelijke inlines zijn toegestaan. 

Met de Bloktekstelementen biedt STOP verschillende mogelijkheden om een bepaalde tekstcategorie aan te duiden. Met het uitgangspunt van [functioneel presenteren](functioneelpresenteren.md) in het achterhoofd biedt STOP geen mogelijkheid om een lettertype voor de weergave van de tekst te specificeren. Afgezien van de beperkingen die volgen uit de juridisch vereiste [tekstweergave](regeltekst_weergave.md) is een viewer vrij in het kiezen van een toepasselijke weergave. Hoe een viewer elke categorie moet weergeven wordt *niet gespecificeerd* door STOP. Voor de beeldvorming worden op deze pagina enkel *voorbeeld*weergaven getoond.

## Bloktekstelementen

[Al](tekst_xsd_Element_tekst_Al.dita#Al), [Begrip](tekst_xsd_Element_tekst_Begrip.dita#Begrip), [Begrippenlijst](tekst_xsd_Element_tekst_Begrippenlijst.dita#Begrippenlijst), [Bijschrift](tekst_xsd_Element_tekst_Bijschrift.dita#Bijschrift), [Bron](tekst_xsd_Element_tekst_Bron.dita#Bron), [Citaat](tekst_xsd_Element_tekst_Citaat.dita#Citaat), [Definitie](tekst_xsd_Element_tekst_Definitie.dita#Definitie), [entry](tekst_xsd_Element_tekst_entry.dita#entry), [Figuur](tekst_xsd_Element_tekst_Figuur.dita#Figuur), [Formule](tekst_xsd_Element_tekst_Formule.dita#Formule), [Groep](tekst_xsd_Element_tekst_Groep.dita#Groep), [Illustratie](tekst_xsd_Element_tekst_Illustratie.dita#Illustratie), [Kadertekst](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst), [Li](tekst_xsd_Element_tekst_Li.dita#Li), [Lijst](tekst_xsd_Element_tekst_Lijst.dita#Lijst), [Lijstaanhef](tekst_xsd_Element_tekst_Lijstaanhef.dita#Lijstaanhef), [Lijstsluiting](tekst_xsd_Element_tekst_Lijstsluiting.dita#Lijstsluiting), [LiNummer](tekst_xsd_Element_tekst_LiNummer.dita#LiNummer), [LidNummer](tekst_xsd_Element_tekst_LidNummer.dita#LidNummer), [table](tekst_xsd_Element_tekst_table.dita#table), [Term](tekst_xsd_Element_tekst_Term.dita#Term)

### Al

Het element [`Al`](tekst_xsd_Element_tekst_Al.dita#Al) (van alinea) bevat broodtekst met alle mogelijke [inlines](regeltekst_inline.md). 

### Begrippenlijst

*XML voorbeeld* [`Begrippenlijst`](tekst_xsd_Element_tekst_Begrippenlijst.dita#Begrippenlijst)

```xml
<Begrippenlijst eId="list_o_1" wId="mn002_1-0__list_o_1">
    <Begrip eId="list_o_1__item_1"
             wId="mn002_1-0__list_o_1__item_1">
        <LiNummer>1.</LiNummer>
        <Term>Begrip</Term>
        <Definitie>
            <Al>het verstaan of begrijpen van iets.</Al>
        </Definitie>
    </Begrip>
</Begrippenlijst>
```

*Voorbeeld weergave:*

> `1.` *Begrip*: het verstaan of begrijpen van iets.

Een [`Begrip`](tekst_xsd_Element_tekst_Begrip.dita#Begrip) bevat de tekstelementen: [`LiNummer`](tekst_xsd_Element_tekst_LiNummer.dita#LiNummer) (optioneel) en [`Term`](tekst_xsd_Element_tekst_Term.dita#Term).  `LiNummer` bevat koptekstinhoud en `Term` broodtekst. De [`Definitie`](tekst_xsd_Element_tekst_Definitie.dita#Definitie) bevat zelf weer bloktekst, met een of meer `Al`inea's(met broodtekst) of `Lijst`en.

### Lijst

Een [`Lijst`](tekst_xsd_Element_tekst_Lijst.dita#Lijst) kan *expliciet* genummerd of *ongemarkeerd* zijn. Bij een ongemarkeerde lijst mag het element [`LiNummer`](tekst_xsd_Element_tekst_LiNummer.dita#LiNummer) niet gebruikt worden.

*XML voorbeeld expliciet genummerde lijst*

```xml
<Lijst eId="list_o_1" wId="mn002_1-0__list_o_1">
    <Lijstaanhef>Expliciet genummerde lijst:</Lijstaanhef>
    <Li eId="list_o_1__item_A"
         wId="mn002_1-0__list_o_1__item_A">
        <LiNummer>A.</LiNummer>
        <Al>Eerste item op de lijst.</Al>
    </Li>
    <Lijstsluiting>Net zoals de aanhef een optionele tekst.</Lijstsluiting>
</Lijst>
```

*Voorbeeld weergave:*

> Expliciet genummerde lijst:
>
> A. Eerste item op de lijst.
>
> Net zoals de aanhef een optionele tekst.

*XML voorbeeld ongemarkeerde lijst*

```xml
<Lijst eId="list_o_1" wId="mn002_1-0__list_o_1">
    <Lijstaanhef>Ongemarkeerde lijst:</Lijstaanhef>
    <Li eId="list_o_1__item_o_1"
         wId="mn002_1-0__list_o_1__item_o_1">        
        <Al>Item in een ongemarkeerde lijst.</Al>
    </Li>
</Lijst>
```

*Voorbeeld weergave:*

> Ongemarkeerde lijst:
>
> ​			Item in een ongemarkeerde lijst.

Een [`Lijst`](tekst_xsd_Element_tekst_Lijst.dita#Lijst) bevat de tekstelementen: [`Lijstaanhef`](tekst_xsd_Element_tekst_Lijstaanhef.dita#Lijstaanhef), [`Lijstsluiting`](tekst_xsd_Element_tekst_Lijstsluiting.dita#Lijstsluiting) en [`LiNummer`](tekst_xsd_Element_tekst_LiNummer.dita#LiNummer).  `LiNummer` bevat inhoud van de categorie koptekst en `Lijstaanhef` en `Lijstsluiting` broodtekst. [`Li`](tekst_xsd_Element_tekst_Li.dita#Li) bevat zelf weer bloktekst: een of meer keer een `Al`inea, `Figuur`, `Formule`, `table` of weer een `Lijst`.

### Figuur {#figuur}

Met het element [`Figuur`](tekst_xsd_Element_tekst_Figuur.dita#Figuur) kan een illustratie opgenomen worden in de tekst. 

*XML voorbeeld*

```xml
<Figuur eId="img_o_1" wId="mn002_1-0__img_o_1">
    <Titel>Figuren</Titel>
    <Illustratie naam="figuren.png"
                  breedte="600"
                  hoogte="300"
                  dpi="300"
                  formaat="image/png"
                  uitlijning="start"/>
    <Bijschrift locatie="onder">Wiskundige figuren</Bijschrift>
    <Bron>math4all</Bron>
</Figuur>
```

*Voorbeeld weergave:*

> ![figuren](img/figuren.png)
> *Wiskundige figuren*
> *Bron: math4all*

Een `Figuur` bevat optioneel drie elementen: `Titel`, `Bijschrift` en `Bron`. De tekst in de [`Titel`](tekst_xsd_Element_tekst_Titel.dita#Titel) is van het type [koptekst](regeltekst_koppen.md). Het [`Bijschrift`](tekst_xsd_Element_tekst_Bijschrift.dita#Bijschrift) en de [`Bron`](tekst_xsd_Element_tekst_Bijschrift.dita#Bijschrift) bevatten broodtekst waarin alle opmaak en verwijzingen mogelijk zijn. Zie ook de toelichting [Afbeelding in de tekst](regeltekst_afbeelding.md).

### Formule

STOP biedt *geen* ondersteuning voor het formatteren van wiskundige of wetenschappelijke formules zoals bijvoorbeeld [MathML](https://nl.wikipedia.org/wiki/Mathematical_Markup_Language) dat wel biedt. Als alternatief kan met het element [`Formule`](tekst_xsd_Element_tekst_Formule.dita#Formule) een wetenschappelijke formule als plaatje opgenomen worden in de tekst. 

*XML voorbeeld*

```xml
<Formule eId="math_o_1" wId="mn002_1-0__math_o_1">
    <Illustratie naam="abc-formule.png"
                  breedte="300"
                  hoogte="100"
                  dpi="150"
                  formaat="image/png"
                  uitlijning="start"/>
    <Bijschrift locatie="onder">De bekende wortelformule</Bijschrift>
    <Bron>wikipedia</Bron>
</Formule>
```

*Voorbeeld weergave:*

> ![figuren](img/abc-formule.png) 
> *De bekende wortelformule*
> *Bron: wikipedia*

Een `Formule` bevat optioneel twee tekst elementen: `Bijschrift` en `Bron`. Deze bevatten broodtekst zodat alle opmaak en verwijzingen mogelijk zijn. Zie ook de toelichting [Afbeelding in de tekst](regeltekst_afbeelding.md).

Beperking:

- `Figuur` en `Formule` mogen niet worden gebruikt binnen een Aanhef (`Aanhef`, `Considerans`, `Afkondiging`), `Rectificatietekst`, `Definitie` en `Noot`.


### Tabellen

De manier waarom tabellen opgenomen zijn in het STOP tekstmodel is afgeleid van de [CALS standaard](https://www.oasis-open.org/specs/tm9901.htm). Uitgebreide uitleg van de (on)mogelijkheden van STOP tabellen is te vinden op een [deze](regeltekst_tabel.md) pagina.

### Verticale witruimte

Om visueel duidelijk te maken dat twee of meer alinea's (of Lijsten/Figuren/Formules) bij elkaar horen, kunnen deze in een [`Groep`](tekst_xsd_Element_tekst_Groep.dita#Groep) geplaatst worden. De gebruikelijke witruimte tussen alinea's of andere tekstdelen wordt dan onderdrukt. 

***XML voorbeeld Groep*** 

```xml
<Inhoud>
  <Al>Dit is een eerste alinea, voor de groep.</Al>
  <Groep>
    <Al>De eerste alinea in een alineagroep wordt voorafgegaan door een witregel.</Al>
    <Al>Een alinea die niet de eerste of de laatste alinea in een alineagroep is krijgt geen witruimte
      ervoor of erachter.</Al>
    <Al>Een alineagroep zorgt er derhalve voor dat de alinea's erbinnen visueel bij elkaar worden
      gehouden. </Al>
    <Al>De laatste alinea in een alineagroep wordt gevolgd door een witregel.</Al>
  </Groep>
  <Al>Dit is een alinea na de groep.</Al>
</Inhoud>
```

*Voorbeeld weergave:*

> ![](img/Groep.png)

Een `Groep` bevat dus zelf weer bloktekst met `Al`inea(met broodtekst), `Figuur`, `Formule` of `Lijst`.

### Citaat

*XML voorbeeld Citaat*

```xml
<Citaat eId="cit_o_1"
        wId="bgX__1-0__cit_o_1"
        auteur="M. van der Ham"
        bron="Promotieonderzoek Limes and Gouda"
        url="https://www.volksuniversiteitgouda.nl/onderzoeker/Ham/Limes">
    <Al>De archeologische waarde van dit gebied is...</Al>
</Citaat>
```

*Voorbeeld weergave*

> *"De archeologische waarde van dit gebied is..."*

Een [`Citaat`](tekst_xsd_Element_tekst_Citaat.dita#Citaat) bevat zelf bloktekst: `Al`inea(met broodtekst), `Figuur`, `Formule`,  `Groep`, `Tussenkop` of `Lijst`.

### Kadertekst

*XML voorbeeld Kadertekst*

```xml
<Kadertekst eId="recital">
  <Kop>
    <Opschrift>Wat is een kadertekst</Opschrift>
  </Kop>
  <Al>Een korte tekst.</Al>
  <Tussenkop>Waarvoor?</Tussenkop>
  <Al>Voor interessante observaties die verband houden met de hoofdtekst.</Al>
  <Groep>
    <Al>Een kadertekst kan ook alineagroepen hebben</Al>
  </Groep>
</Kadertekst>
```

*Voorbeeld weergave*

![img/kadertekst.png](img/kadertekst.png)

Een [`Kadertekst`](tekst_xsd_Element_tekst_Kadertekst.dita#Kadertekst) bevat zelf bloktekst: `Al`inea(met broodtekst), `Citaat`,  `Figuur`, `Formule`, `Groep`, `Lijst`, `Tussenkop` of `table`.





