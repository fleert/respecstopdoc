# Werkwijze voor RegelingKlassiek

[TODO](DITA_OnbekendeLink.dita#Oeps)

Kleiner maken van de renvooitekst via [wat-zinnetjes](bepalen_wijzigingen_renvooi.md) zoals de renvooiservice nu doet. Ontworpen voor gebruik in een `RegelingKlassiek` en beschreven in (een toekomstige versie van) de AvdR.