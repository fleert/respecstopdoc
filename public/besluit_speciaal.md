# Speciale wijzigingsconstructies

Er zijn een aantal juridische constructies waarbij in het lichaam van het besluit een beschrijving is opgenomen hoe een regeling gewijzigd moet worden, maar die wijziging is niet op de gebruikelijke wijze als [renvooitekst](renvooitekst.md) opgenomen.

[Gedeeltelijke inwerkingtreding](gedeeltelijke-inwerkingtreding.md)
: Bij het opstellen van een inwerkingtredingsbesluit kan blijken dat niet alle wijzigingen in regelingen en/of informatieobjecten uit een al bekendgemaakt besluit in werking kunnen treden. In het besluit moet tekstueel aangegeven worden welk deel al wel in werking treedt en de wijziging moet als onderdeel van het besluit meegegeven worden.

[Intrekking van een wijzigingsbesluit](besluit_intrekking.md)
: Nadat een een wijzigingsbesluit is vastgesteld en bekendgemaakt kan het bevoegd gezag later tot het inzicht komen dat het besluit toch beter niet in werking kan gaan. Het besluit kan dan worden ingetrokken.