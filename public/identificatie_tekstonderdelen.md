# Tekstcomponenten

Lees in deze sectie hoe er in STOP gebruik gemaakt wordt van tekstonderdelen met `@componentnaam` en hoe hier naar verwezen kan worden.

## AKN en @componentnaam 

Als de tekst van een nieuwe of gewijzigde regeling in het besluit opgenomen wordt dan kunnen daarin [`@eId`'s en `@wId`'s](eid_wid.md) voorkomen die identiek kunnen zijn aan de `@eId`'s en `@wId`'s  van het besluit waar deze elementen onderdeel van zijn. Het is een eis van [Akoma Ntoso](naamgevingsconventie.md) dat alle `@eId`/`@wId` uniek zijn. Om aan deze eis te kunnen voldoen wordt de tekst van de regeling of de tekst van de wijziging van de regeling als AKN component gezien. Dat wordt aangegeven door een `@componentnaam` toe te voegen aan het bovenliggende element.

## STOP-elementen met @componentnaam

STOP beperkt het gebruik van componenten tot elementen met een `@componentnaam` attribuut. In STOP hebben de volgende elementen het attribuut `@componentnaam`:

-   [`RegelingMutatie`](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) (verplicht)
-   [`BesluitMutatie`](tekst_xsd_Element_tekst_BesluitMutatie.dita#BesluitMutatie) (verplicht)
-   [`RegelingKlassiek`](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) (alleen verplicht bij een nieuwe regeling in een besluit)
-   [`RegelingVrijetekst`](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) (alleen verplicht bij een nieuwe regeling in een besluit)
-   [`RegelingCompact`](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact) (alleen verplicht bij een nieuwe regeling in een besluit)
-   [`RegelingTijdelijkdeel`](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel) (alleen verplicht bij een nieuwe regeling in een besluit)

Bij de laatste vier elementen is de `@componentnaam` alleen verplicht als de regeling initieel wordt vastgesteld als onderdeel van een besluit. In een regeling als zelfstandige module is de `@componentnaam` overbodig.

## Eisen aan de naam van een component

De volgende regels gelden voor de waarde van `@componentnaam`:

1.  Gebruik uitsluitend kleine letters, hoofdletters, cijfers (0-9) en een underscore '_' als scheidingsteken.
2.  Gebruik betekenisvolle namen voorzien van eventueel een (volg)nummer voorafgegaan door een underscore.
3.  Zorg ervoor dat de namen niet enkel verschillen door een ander gebruik van kleine en hoofdletters.
4.  Componentnamen moeten uniek zijn binnen het geleverde besluit en altijd dezelfde schrijfwijze hebben, zonder variatie in gebruik van hoofdletters. 
5.  De componentnaam moet kort maar wel onderscheidend zijn. Echter: er wordt geen limiet opgelegd aan de lengte van de naam. In STOP wordt het gebruik van namen als volgt verwoord: “Kenmerken om de expression (optioneel) of het werk (verplicht) te onderscheiden, bijvoorbeeld een dossiernummer of een afkorting gebaseerd op de naam moeten compact zijn; het is niet de bedoeling om hiervoor een lange titel te gebruiken.”
6.  Niet toegestane karakters. De restrictie op het gebruik van tekens in de componentnaam impliceert dat ook de volgende karakters hiervoor niet kunnen worden gebruikt:
    1.  Spaties
    2.  Geaccentueerde tekens, zoals á, ö of é.

## Verwijzen naar een *@eId* in een component {#Verwijzen}

Om te verwijzen naar een `eId` binnen een component wordt de `eId` voorafgegaan door de componentnaam. Dit is in AKN Akoma Ntoso beschreven als [local component reference](https://docs.oasis-open.org/legaldocml/akn-nc/v1.0/os/akn-nc-v1.0-os.html#_Toc531692283). De verwijzing wordt alsvolgt opgebouwd:

**verwijzing:** ` "!" <@componentnaam> "#" <eid>` 

met

|code|betekenis|
|---------|------------------|
|@componentnaam|De naam van het component zoals opgenomen in het componentnaam attribuut.|
|eId| De eId van het tekstdeel waarnaar verwezen wordt             |

## Voorbeelden

Verwijzingen naar `eId`'s met een component aanduiding komen bijvoorbeeld op de volgende plaatsen voor:

1. vanuit de [`data:ConsolidatieInformatie`](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) verwijzen naar een informatieobjectverwijzing in een regelingmutatie.
2. Vanuit [`cons:Bladwijzer`](cons_xsd_Element_cons_Bladwijzer.dita#Bladwijzer) verwijzen naar een deel van de regeling in een besluit.

**Voorbeeld 1**: eId met component

```
<BeoogdInformatieobject xmlns="https://standaarden.overheid.nl/stop/imop/data/">
  <doelen>
    <doel>/join/id/proces/gm9999/2020/InstellingOmgevingsplan</doel>
  </doelen>
  <instrumentVersie>/join/id/regdata/gm9999/2019/gio993859238/nld@2019-12-20;1</instrumentVersie>
  <eId>!regeling#cmp_A__content_o_1__list_1__item_1__ref_o_1</eId>
</BeoogdInformatieobject>
```

**Voorbeeld 2**: IRI met eId met component

Verwijzing naar artikel 1 van de componentnaam `regeling` die in besluit `gmb-2020-87904` is gepubliceerd op officielebekendmakingen.nl:

```
<Bladwijzer xmlns="https://standaarden.overheid.nl/stop/imop/consolidatie/">
  <relevantVoor>inhoud</relevantVoor>
  <url>https://www.officielebekendmakingen.nl/gmb-2020-87904!regeling#art_1</url>
</Bladwijzer>
```

**Opmerking:** Voor verwijzingen naar [delen van geografische informatie objecten](https://koop.gitlab.io/STOP/team/test6/gio-tekstverwijzing.html#giodeel) wordt dezelfde syntax gebruikt. 
