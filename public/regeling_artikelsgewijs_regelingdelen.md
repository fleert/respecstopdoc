# Juridisch één, technisch meerdelig

De standaard maakt onderscheid tussen een hoofdregeling en tijdelijk regelingdelen. Juridisch bestaat dat onderscheid niet: de tekst van de hoofdregeling en tijdelijk regelingdelen vormen één geheel. Technisch (in STOP) zijn de hoofdregeling en tijdelijk regelingdelen aparte regelingen die via metadata met elkaar verbonden zijn. Het is een technische oplossing om regelingen informatiekundig eenvoudig te kunnen beheren terwijl toch juridisch complexe wijzigscenario's mogelijk gemaakt worden. Voorbeelden daarvan zijn:

* Regels waarvoor van rechtswege een afwijkende geldigheidsduur geldt. Door deze regels in een aparte STOP-regeling te plaatsen hoeft in de hoofdregeling niet op het niveau van artikelen de geldigheidsduur bijgehouden te worden, maar kan het standaard versiebeheer voor een STOP-regeling gebruikt worden.
* Meervoudig bronhouderschap, waarin het ene bevoegd gezag gerechtigd is om (via besluiten) een regeling van een ander bevoegd gezag aan te passen, rechtstreeks of door aanpassing van een besluit. Dit wordt meervoudig bronhouderschap genoemd. Dit speelt bijvoorbeeld vanwege de Omgevingswet bij voorbeschermingsregels, reactieve interventies en projectbesluiten.
* Complex [overgangsrecht](regeling_geldigheid.md), waarbij op hetzelfde moment meerdere versies van dezelfde artikelen voor verschillende situaties van toepassing zijn **TODO** nog verifiëren.
* Scenario's waarbij van rechtswege een bestaande regeling (of een deel ervan) onderdeel gaat uitmaken van een andere (hoofd)regeling. Bijvoorbeeld omgevingsplan bij gemeentelijke herindelingen. Door de bestaande regeling om te zetten naar tijdelijk regelingdeel wordt de juridische fictie van "onderdeel van rechtswege" zichtbaar gemaakt zonder dat de hoofdregeling ingrijpend gewijzigd moet worden.

Voor deze situaties is in STOP een oplossing gekozen waarbij de regeling technisch uit verschillende onderdelen bestaat maar juridisch één geheel vormt. De regeling bestaat dan uit een hoofdregeling die volgens een van de gebruikelijke modellen is opgebouwd, en een aanvulling waarvan de tekst als [Tijdelijk Regelingdeel](tekst_xsd_Element_tekst_RegelingTijdelijkdeel.dita#RegelingTijdelijkdeel) is vormgegeven. Een tijdelijk regelingdeel kent zijn eigen structuur: het lichaam van een tijdelijk regeling begint altijd met een beschrijving in tekst van de condities waaronder het regelingdeel geldig is. Daarin staat ook in woorden aangegeven dat het juridisch hoort bij de hoofdregeling, en dat (bijvoorbeeld bij een voorbereidingsbesluit of reactieve interventie) de regels in het tijdelijk deel boven die in de hoofdregeling gaan.

## Voorbeeld: voorbeschermingsregels
Een bevoegd gezag kan een voorbereidingsbesluit nemen met het oog op de voorbereiding van omgevingsplan, omgevingsverordening, projectbesluit, instructieregel of instructie. Het voorbereidingsbesluit wijzigt het omgevingsplan respectievelijk de omgevingsverordening met voorbeschermingsregels. Het doel van voorbeschermingsregels is te voorkomen dat een locatie minder geschikt wordt voor de verwezenlijking van het doel van de regels. 
Voorbeschermingsregels kunnen nieuwe regels (of delen daarvan) toevoegen of bestaande regels buiten toepassing verklaren. Voorbeschermingsregels vervallen door òf tijdsverloop òf doordat het besluit waarop het voorbereidingsbesluit was gericht in werking is getreden of vernietigd is.

![Schematische weergave van de onderdelen van het voorbereidingsbesluit](img/SchemaTijdelijkDeel.png)

De figuur hierboven toont het voorbereidingsbesluit dat uit twee onderdelen bestaat:
1. Het besluit, met in het WijzigArtikel de verwijzing naar de voorbeschermingsregels in de WijzigBijlage, het tijdelijk regelingdeel.
2. Het tijdelijk regelingdeel, met de conditie en de voorbeschermingsregels. De conditie bevat typisch een artikel met de beschrijving in woorden van de verhouding tussen dit tijdelijk regelingdeel en de hoofdregeling. De kop van het artikel heeft doorgaans alleen een opschrift en geen label en nummer; dit artikel wordt niet in een hoofdstuk ondergebracht. De voorbeschermingsregels staan in een of meer artikelen, die moeten zijn ondergebracht in een of meer hoofdstukken. Precieze vormvereisten zijn beschreven in het [toepassingsprofiel](toepassingsprofielen.md).



