# 5. Neem geboorteregeling op

**N.B**.: Deze stap geldt alleen voor te consolideren IO's.


Bij een `TeConsolideren` IO wordt de JOIN-ID van het *work* van de regeling waarmee het de IO-versie is bekendgemaakt opgenomen in de [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) van de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata).

Zie [heeftGeboorteregeling](io-heeftGeboorteregeling.md)
