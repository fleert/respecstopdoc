# Basis van STOP

Deze leeswijzers beschrijven de processen waarvoor STOP is ontworpen en de mogelijkheden die STOP biedt.
Doelgroep van deze leeswijzers zijn:

  - de eindgebruikers van software die informatie volgens STOP uitwisselt, waaronder planmakers en juristen
  - de softwareontwikkelaars van dergelijke software

De Basis van STOP bevat onderstaande leeswijzers:

[Juridische context STOP](juridische-context.md)

: Beschrijft de juridische context van STOP.

[Hoofdlijnen van het STOP-proces](proces_overzicht.md)
: Beschrijft het geïntegreerd proces van bekendmaken en consolideren dat is ontworpen voor het juridisch borgen van regelgeving en gerelateerde informatie op een manier die vergaand te automatiseren is.

