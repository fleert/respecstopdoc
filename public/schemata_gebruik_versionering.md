# Versionering van modules 

## Versies van STOP, IMOP en XML-serialisatie

In STOP worden versienummers volgens [SemVer](https://semver.org) gebruikt voor drie verschillende onderdelen:

- De versie van de standaard (STOP), bijvoorbeeld `1.1.0` of `1.1.0-preview`. Het derde nummer is altijd 0, en een label wordt alleen gebruikt voor "previews" en "release candidates".
- De versie van het implementatiemodel (IMOP). Als er een nieuwe versie van de standaard uitkomt heeft het bijbehorende IMOP hetzelfde versienummer als de standaard. Als er daarna uitbreidingen of bug fixes uitkomen voor IMOP terwijl de standaard gelijk blijft, dan wordt het derde nummer opgehoogd (van 1.1.0 naar 1.1.1 naar 1.1.2 etc).
- De versie van de XML-serialisatie van STOP informatie. Dit is gelijk aan de versie van IMOP die gebruikt is voor het opstellen van de XML serialisatie (vastgelegd in het [schemaversie](xml_documenten.md) attribuut).

Bij *versionering van modules* wordt de derde versie bedoeld: de versie van de XML-serialisatie van een module. In opeenvolgende versies van STOP zullen niet voor alle modules een ander informatiemodel en/of bedrijfsregels gelden; veel modules zullen juist gelijk blijven. Dat betekent ook dat de XML-serialisatie van de informatie in een specifieke module voor opeenvolgende versies van STOP gelijk zal zijn (afgezien van het `schemaversie` attribuut).

## Cumulatief versieoverzicht

Elke versie van IMOP bevat een cumulatief overzicht met voor elke module een beschrijving van alle bekende en nog ondersteunde versies. Het overzicht voor deze versie van IMOP is terug te vinden in [versie.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema/stop/%40%40%40IMOPVERSIE%40%40%40/versie.xml). Het overzicht voor alle gepubliceerde versies (exclusief previews en release candidates) is te vinden op [https://standaarden.overheid.nl/stop/versiescompleet.xml](https://standaarden.overheid.nl/stop/versiescompleet.xml).

In het versieoverzicht is per versie van een module vastgelegd:

- De versie van de standaard waarin de versie van de module is geïntroduceerd.
- De URL van het schema waarin de structuur van de XML-serialisatie is beschreven.
- De URL van de schematron(s) die de XML-serialisatie kunnen valideren
- De transformaties (indien beschikbaar) om de XML-serialisatie te converteren naar de XML-serialisatie van een vorige of volgende versie van de standaard.

Een voorbeeld van een versieoverzicht voor een module is:

```xml
<Module>
  <localName>ConsolidatieInformatie</localName>
  <namespace>https://standaarden.overheid.nl/stop/imop/data/</namespace>
  <implementatie>
    <Moduleversie>
      <introductieversie>1.0.0</introductieversie>
      <schema>https://standaarden.overheid.nl/stop/1.0.4/imop-data.xsd</schema>
      <schematron>https://standaarden.overheid.nl/stop/1.1.0/imop-aknjoin.sch</schematron>
      <schematron>https://standaarden.overheid.nl/stop/1.0.4/imop-consolidatie.sch</schematron>
      <heeftTransformatie>
        <Transformatie>
          <introductieversie>1.1.0</introductieversie>
          <locatie>https://standaarden.overheid.nl/stop/1.1.0/identiteitstransformatie.xslt</locatie>
        </Transformatie>
      </heeftTransformatie>
    </Moduleversie>
    <Moduleversie>
      <introductieversie>1.1.0</introductieversie>
      <schema>https://standaarden.overheid.nl/stop/1.1.0/imop-data.xsd</schema>
      <schematron>https://standaarden.overheid.nl/stop/1.1.0/imop-aknjoin.sch</schematron>
      <schematron>https://standaarden.overheid.nl/stop/1.1.0/imop-consolidatie.sch</schematron>
      <heeftTransformatie>
        <Transformatie>
          <introductieversie>1.0.0</introductieversie>
          <locatie>https://standaarden.overheid.nl/stop/1.1.0/consolidatieinformatie_110_naar_104.xslt</locatie>
        </Transformatie>
      </heeftTransformatie>
    </Moduleversie>
  </implementatie>
</Module>
```

Hierin is te lezen:
- De module `ConsolidatieInformatie` uit de IMOP-data-namespace kent twee versies. De eerste is geïntroduceerd in STOP `1.0.0`, de tweede in STOP `1.1.0`.
- De versie uit STOP `1.0.0` moet gevalideerd worden tegen een schema uit IMOP `1.0.4` en tegen een schematron uit IMOP `1.0.4` (`imop-consolidatie.sch`) en één uit IMOP `1.1.0` (`imop-aknjoin.sch`). De versie uit `1.1.0` moet gevalideerd worden tegen een schema en twee schematrons uit IMOP `1.1.0`.
- Er zijn transformaties beschikbaar om de XML-serialisatie voor versie 1.0.0 naar versie 1.1.0 om te zetten en omgekeerd. Het is [op voorhand](schemata_gebruik_transformaties.md) niet gegarandeerd dat beide transformaties slagen.

Voor een verdere uitleg zie het [versies.xml voorbeeld](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Architectuur/Versie-informatie).

## Gebruik van het versieoverzicht voor creatie

Voor het maken van de XML-serialisatie op basis van een specifieke IMOP-versie dient `versies.xml` gebruikt te worden voor die IMOP-versie. Deze is te vinden [in de IMOP-versie op internet](schemata_gebruik_url.md) en is onderdeel van de [download](schemata_gebruik_lokaal.md) van IMOP.

Voor elke module moet uit deze `versies.xml` de moduleversie met de grootste introductieversie gebruikt worden - dit is voor deze IMOP-versie de huidige moduleversie. Het schema dat bij die moduleversie vermeld staat is het schema uit deze IMOP-versie. Ook de schematrons die erbij vermeld staan zijn uit deze IMOP versie afkomstig.

De XML-serialisatie van de module moet gemaakt worden volgens het schema uit deze IMOP-versie en conform alle bedrijfsregels die STOP voor de module beschrijft. De software die de XML-serialisatie maakt wordt geacht te valideren dat de resulterende XML voldoet aan de bedrijfsregels die in de schematrons voor de module zijn vastgelegd. Als de XML-serialisatie onderdeel is van een XML-document dat meerdere (type) modules bevat, dan moet het XML-document worden [gevalideerd](schemata_gebruik_schematrons.md) met alle schematrons die in `versies.xml` voor de huidige moduleversie van de verschillende modules is vermeld.

## Gebruik van het versieoverzicht voor validatie

Als software een XML-document ontvangen heeft en deze wil valideren, dan moeten de volgende stappen gevolgd worden:

- Elke IMOP-module in het XML-document moet volgens [dezelfde IMOP-versie](xml_documenten.md) zijn opgesteld, te zien aan het `schemaversie` attribuut.
- Voor elke IMOP-module moet de bijbehorende moduleversie worden opgezocht in de huidige (meest recente) `versies.xml`. Software die op een bepaalde versie van IMOP is gebouwd zal `versies.xml` uit die IMOP-versie gebruiken.
- De bijbehorende moduleversie is die versie waarvoor de introductieversie kleiner dan of gelijk is aan de `schemaversie`; als er meerdere moduleversies aan dat criterium voldoen dan moet de moduleversie met de grootste introductieversie genomen worden.
- De XML-serialisatie van de module moet gevalideerd worden tegen het schema dat bij de moduleversie vermeld staat. Daarna moet de XML [gevalideerd](schemata_gebruik_schematrons.md) worden tegen de vermelde schematrons.
