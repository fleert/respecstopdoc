# Omgaan met de geconsolideerde regeling

## Hoe beheer ik de geconsolideerde regeling? 

Wijzigingen die besluiten aanbrengen in een regeling komen tot uiting in de geconsolideerd regeling. Een [annotatie](begrippenlijst_annotatie.html#annotatie) bevat een interpretatie of duiding van de (juridische) inhoud van een regeling die voor software te begrijpen is. De annotatie bevat dus geen nieuwe informatie, immers de informatie in een annotatie is af te leiden door de regeling te bestuderen. De informatie is alleen op een andere manier gemodelleerd, zodat software er mee kan werken.

Annotaties kunnen direct meegeleverd worden bij een besluit, maar kunnen ook los, zonder besluit achteraf aangeleverd worden. 

[Annotaties meegeven bij besluit/rectificatie](geconsolideerde_regeling_annotaties_met_besluit.md)
: Meestal worden annotaties direct met het besluit meegeleverd. 

[Annotatie los aanleveren](geconsolideerde_regeling_annotaties_los.md)
: Het is ook mogelijk om een annotatie achteraf, los van het besluit aan te leveren. 

[Annotaties corrigeren](geconsolideerde_regeling_annotaties_corrigeren.md)
: Mocht er een fout zijn geslopen in een annotatie, dan kan deze gecorrigeerd worden. 

[Revisie: rectificatie van de geconsolieerde regeling](geconsolideerde_regeling_revisie_regeling.md)
: Ook in de geconsolideerde regeling kunnen fouten sluipen. In bepaalde omstandigheden mag het bevoegd gezag evidente fouten herstellen in de geconsolideerde regeling.