# GerechtelijkeProcedureStatus

Het BG meldt relevante gebeurtenissen in het beroepsproces door het [doorgeven van een procedurestap](besluitproces_rechterlijkemacht_procedurele_status.md).  Hoe vanuit de aangeleverde procedurestappen de status van een besluit afgeleid wordt, staat beschreven in [status besluit bepalen](besluitproces_rechterlijkemacht_statusbesluitbepalen.md).

Bij de wijzigingen die het besluit aanbrengt in de geconsolideerde regeling, moet worden weergegeven dat de wijzigingen nog niet definitief zijn, indien het besluit nog *niet onherroepelijk* is. In deze sectie wordt beschreven hoe bij een geconsolideerde regeling de status wordt getoond van de besluit(en) die hebben geleid tot deze versie van de regeling. 


## Herroepelijkheid geconsolideerde regeling

Een geconsolideerde regeling wordt samengesteld uit het besluit dat de initiële versie van de regeling instelt en de besluiten die vervolgens wijzigingen aanbrengen in deze regeling. Is een besluit vatbaar voor beroep, dan is de instelling van de initiële regeling of zijn de wijzigingen die het besluit heeft aangebracht in de regeling, pas definitief als het besluit onherroepelijk is geworden.

De publicatie van de geconsolideerde regeling wacht niet totdat het besluit onherroepelijk is. De geconsolideerde regeling wordt direct gepubliceerd, met een "kanttekening" bij de delen van de regeling die nog niet onherroepelijk zijn, omdat het besluit dat deze delen heeft ingevoegd of gewijzigd, nog niet onherroepelijk is. 

Wordt een besluit onherroepelijk, dan worden de "kanttekeningen" bij die delen van de regeling die door betreffend besluit zijn aangebracht, weggehaald. 



## Over GerechtelijkeProcedureStatus 

Deze kanttekeningen zijn opgenomen in de [contextmodule](context_modules.md) `GerechtelijkeProcedureStatus`. Elke geconsolideerde regeling heeft deze module, zodat altijd te achterhalen is welke besluiten hebben geleid tot deze regelingversie, en wat de status van deze besluiten is. 

`GerechtelijkeProcedureStatus` is een contextmodule omdat de status van de geconsolideerde regeling kan wijzigen onafhankelijk van de regeling zelf, bijvoorbeeld doordat na afloop van de beroepstermijn een besluit onherroepelijk wordt. Het besluit verandert niet inhoudelijk, maar de status ervan wel: door het verstrijken van de tijd is de beroepstermijn verlopen en is het besluit onherroepelijk geworden. 


De [GerechtelijkeProcedureStatus](EA_C0439F1B47B549bf95F43D2318161D9B.dita#Pkg/2F904AAEEDB24d0c9D44B765150A6034) module heeft een `bekendOp` en `ontvangenOp` datum aan de hand waarvan [tijdreizen](regelgeving_in_de_tijd.md) mogelijk is. Daarnaast bevat `GerechtelijkeProcedureStatus` informatie over één of meer besluiten. Een regeling ontstaat immers uit één of meer besluiten. Per besluit wordt de status weergegeven en wordt aan de hand van de [wId](eid_wid.md)'s aangegeven welke delen van de regeling door dit besluit ontstaan of gewijzigd zijn. Dit zijn dus niet alle `wId`'s in de tekst, maar alleen de `wId`'s van de muteerbare eenheden in de tekst. Om als lezer zelf het betreffende besluit te kunnen raadplegen, is er een verwijzing naar het gepubliceerde besluit en eventueel ook een verwijzing naar de website van het BG waar een toelichting op de status gegeven kan worden. [GerechtelijkeProcedureStatus(schema)](cons_xsd_Element_cons_GerechtelijkeProcedureStatus.dita#GerechtelijkeProcedureStatus) 



## Voorbeelden

### Niet onherroepelijk

De `GerechtelijkeProcedureStatus` behorende bij een geconsolideerde regelingversie na de publicatie van een, voor beroep vatbaar besluit. Het betreft een besluit dat artikelen 1, 2 en 3 heeft toegevoegd/gewijzigd van de regeling.

```xml
<GerechtelijkeProcedureStatus>
    <bekendOp>2020-01-21</bekendOp>
    <ontvangenOp>2020-01-21</ontvangenOp>
    <BesluitStatus>
        <FRBRWork>/akn/nl/bill/gm9999/2020/B2020-00013</FRBRWork>
        <onherroepelijkVanaf>2020-03-04</onherroepelijkVanaf>
        <geschorst>false</geschorst>
        <gepubliceerdIn>
            <JuridischeBronpublicatie>
                <url>https://www.officielebekendmakingen.nl/gmb-2020-87904</url>
                <publicatieIdentifier>gmb-2020-87904</publicatieIdentifier>
                <publicatienaam>Gemeenteblad 2020, 87904</publicatienaam>
                <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_001</soortGepubliceerdWork>
                <ondertekendOp>2020-01-17</ondertekendOp>
                <gepubliceerdOp>2020-01-21</gepubliceerdOp>
            </JuridischeBronpublicatie>
        </gepubliceerdIn>
        <wId>gm9999_1__art_1</wId>
        <wId>gm9999_1__art_2</wId>
        <wId>gm9999_1__art_3</wId>
    </BesluitStatus> 
</GerechtelijkeProcedureStatus>
```

De gerechtelijkeprocedurestatus verwijst naar het besluit als Work, het gaat immers om het besluit zelf en niet om de publicatie van het besluit. 

Vandaar dat `gepubliceerdIn` een of meer `JuridischeBronpublicaties` kan bevatten. De publicatie van een besluit heeft soms een rectificatie(s) nodig om het besluit zelf volledig correct te publiceren. 

De `Besluitstatus` bevat de datum `OnherroepelijkVanaf`, de ingangsdatum van de beroepstermijn.

Een of meer `wId`'s duiden de onderdelen van de regeling die door dit besluit zijn toegevoegd of aangepast.  

### Beroep(en) ingesteld

Zijn er aan het einde van de beroepstermijn een of meerdere beroepen ingesteld, dan is de datum `onherroepelijkVanaf` niet meer correct. Het BG heeft de procedurestap "Beroep(en) ingesteld" aangeleverd en de `GerechtelijkeProcedureStatus` van dat besluit wordt aangepast.

```xml
<GerechtelijkeProcedureStatus>
    <bekendOp>2020-02-14</bekendOp>
    <ontvangenOp>2020-03-04</ontvangenOp>
    <BesluitStatus>
        <FRBRWork>/akn/nl/bill/gm9999/2020/B2020-00013</FRBRWork>
        <!-- onherroepelijkVanaf datum gewist -->
        <geschorst>false</geschorst>
        <gepubliceerdIn>
            <JuridischeBronpublicatie>
                <url>https://www.officielebekendmakingen.nl/gmb-2020-87904</url>
                <publicatieIdentifier>gmb-2020-87904</publicatieIdentifier>
                <publicatienaam>Gemeenteblad 2020, 87904</publicatienaam>
                <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_001</soortGepubliceerdWork>
                <ondertekendOp>2020-01-17</ondertekendOp>
                <gepubliceerdOp>2020-01-21</gepubliceerdOp>
            </JuridischeBronpublicatie>
        </gepubliceerdIn>
        <wId>gm9999_1__art_1</wId>
        <wId>gm9999_1__art_2</wId>
        <wId>gm9999_1__art_3</wId>
    </BesluitStatus> 
</GerechtelijkeProcedureStatus>
```

### Geschorst

Hieronder een voorbeeld nadat het BG met een procedurestap de tussenuitspraak van de rechter: besluit geschorst, heeft doorgegeven:

```xml
<GerechtelijkeProcedureStatus>
    <bekendOp>2020-06-01</bekendOp>
    <ontvangenOp>2020-06-03</ontvangenOp>
    <BesluitStatus>
        <FRBRWork>/akn/nl/bill/gm9999/2020/B2020-00013</FRBRWork>
        <geschorst>true</geschorst> <!-- schorsing ingesteld -->
        <gepubliceerdIn>
            <JuridischeBronpublicatie>
                <url>https://www.officielebekendmakingen.nl/gmb-2020-87904</url>
                <publicatieIdentifier>gmb-2020-87904</publicatieIdentifier>
                <publicatienaam>Gemeenteblad 2020, 87904</publicatienaam>
                <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_001</soortGepubliceerdWork>
                <ondertekendOp>2020-01-17</ondertekendOp>
                <gepubliceerdOp>2020-01-21</gepubliceerdOp>
            </JuridischeBronpublicatie>
        </gepubliceerdIn>
        <wId>gm9999_1__art_1</wId>
        <wId>gm9999_1__art_2</wId>
        <wId>gm9999_1__art_3</wId>
        <meerInformatie>https://www.pyrodam.nl/actueel/schorsing-87904</meerInformatie>
    </BesluitStatus> 
</GerechtelijkeProcedureStatus>
```

De boolean `Geschorst` is nu `true` en met `meerInformatie` geeft het BG een URL verwijzing naar de website van het BG. Het BG kan op haar website bijvoorbeeld de precieze consequenties uitleggen van het besluit van de rechter om een (gedeelte) van het besluit te schorsen. 

### Niet onherroepelijk

Als de rechter de schorsing opheft, maar nog geen definitieve uitspraak doet, wordt de schorsing-vlag verwijderd. Doordat de datum `onherroepelijkVanaf` ontbreekt, zijn de artikelen in de geconsolideerde regeling nog "niet onherroepelijk".

```xml
<GerechtelijkeProcedureStatus>
    <bekendOp>2020-07-14</bekendOp>
    <ontvangenOp>2020-07-16</ontvangenOp>
    <BesluitStatus>
        <FRBRWork>/akn/nl/bill/gm9999/2020/B2020-00013</FRBRWork>
        <geschorst>false</geschorst> <!-- schorsing opgeheven -->
        <gepubliceerdIn>
            <JuridischeBronpublicatie>
                <url>https://www.officielebekendmakingen.nl/gmb-2020-87904</url>
                <publicatieIdentifier>gmb-2020-87904</publicatieIdentifier>
                <publicatienaam>Gemeenteblad 2020, 87904</publicatienaam>
                <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_001</soortGepubliceerdWork>
                <ondertekendOp>2020-01-17</ondertekendOp>
                <gepubliceerdOp>2020-01-21</gepubliceerdOp>
            </JuridischeBronpublicatie>
        </gepubliceerdIn>
        <wId>gm9999_1__art_1</wId>
        <wId>gm9999_1__art_2</wId>
        <wId>gm9999_1__art_3</wId>
    </BesluitStatus> 
</GerechtelijkeProcedureStatus>
```

### Onherroepelijk

Als de definitieve uitspraak van de rechter de beroepen ongegrond verklaard, geeft het BG met een procedurestap "beroep(en) definitief afgedaan" door, dat het besluit onherroepelijk is geworden. De `GerechtelijkeProcedureStatus` bevat nu een datum `onherroepelijkVanaf` in het verleden en daarmee is het besluit  "onherroepelijk".

```xml
<GerechtelijkeProcedureStatus>
    <bekendOp>2020-07-14</bekendOp>
    <ontvangenOp>2020-08-31</ontvangenOp>
    <BesluitStatus>
        <FRBRWork>/akn/nl/bill/gm9999/2020/B2020-00013</FRBRWork>
        <onherroepelijkVanaf>2020-08-25</onherroepelijkVanaf> 
        <geschorst>false</geschorst> <!-- schorsing niet meer van toepassing -->
        <gepubliceerdIn>
            <JuridischeBronpublicatie>
                <url>https://www.officielebekendmakingen.nl/gmb-2020-87904</url>
                <publicatieIdentifier>gmb-2020-87904</publicatieIdentifier>
                <publicatienaam>Gemeenteblad 2020, 87904</publicatienaam>
                <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_001</soortGepubliceerdWork>
                <ondertekendOp>2020-01-17</ondertekendOp>
                <gepubliceerdOp>2020-01-21</gepubliceerdOp>
            </JuridischeBronpublicatie>
        </gepubliceerdIn>
        <wId>gm9999_1__art_1</wId>
        <wId>gm9999_1__art_2</wId>
        <wId>gm9999_1__art_3</wId>
    </BesluitStatus> 
</GerechtelijkeProcedureStatus>
```

### Vernietigd

Indien de rechter de beroepen gegrond verklaard, wordt een (gedeelte) van het besluit vernietigd. Bij een vernietiging publiceert het BG altijd een kennisgeving met de uitspraak van de rechter. Deze publicatie wordt opgenomen in de `GerechtelijkeProcedureStatus`. 

```xml
<BesluitStatus>
    <FRBRWork>/akn/nl/bill/gm9999/2020/B2020-00025</FRBRWork>
    <onherroepelijkVanaf>2020-12-05</onherroepelijkVanaf>
    <geschorst>false</geschorst>
    <gepubliceerdIn>
        <JuridischeBronpublicatie> <!-- Wijzigingsbesluit -->
            <url>https://www.officielebekendmakingen.nl/gmb-2020-89103</url>
            <publicatieIdentifier>gmb-2020-89103</publicatieIdentifier>
            <publicatienaam>Gemeenteblad 2020, 89123</publicatienaam>
            <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_001</soortGepubliceerdWork>
            <ondertekendOp>2020-06-19</ondertekendOp>
            <gepubliceerdOp>2020-06-20</gepubliceerdOp>
        </JuridischeBronpublicatie>
        <JuridischeBronpublicatie> <!-- Mededeling gedeeltelijke vernietiging -->
            <url>https://www.officielebekendmakingen.nl/gmb-2020-89456</url>
            <publicatieIdentifier>gmb-2020-89456</publicatieIdentifier>
            <publicatienaam>Gemeenteblad 2020, 89456</publicatienaam>
            <soortGepubliceerdWork>/join/id/stop/gepubliceerdwork_004</soortGepubliceerdWork>
            <ondertekendOp>2020-12-14</ondertekendOp>
            <gepubliceerdOp>2020-12-15</gepubliceerdOp>
        </JuridischeBronpublicatie>
    </gepubliceerdIn>
    <wId>gm9999_1__art_2</wId>
    <wId>gm9999_1__cmp_A__content_o_1</wId>
</BesluitStatus>
```

Is met de uitspraak de beroepsprocedure afgerond, is geen hoger beroep meer mogelijk of er is geen gebruik gemaakt van een hogere beroep mogelijkheid, dan geeft het BG met een procedurestap "beroep(en) definitief afgedaan" door, dat het besluit onherroepelijk is geworden. De `GerechtelijkeProcedureStatus` bevat dan een datum `onherroepelijkVanaf` in het verleden en daarmee is het besluit  "onherroepelijk", zoals in bovenstaand voorbeeld.

## Zie ook

* Juridische informatie over [beroep](beroep.md) en de [verwerking van beroep in STOP](verwerking-beroep.md).
* [Verwerken uitspraak beroep](besluitproces_rechterlijkemacht_scenarios.md) voor een compleet overzicht van alle stappen van het BG in een beroepsprocedure.
* Volledige [XML voorbeelden](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/03%20Proces%20rondom%20besluit/05%20Scenarios%20mbt%20gerechtelijke%20macht/index.md) van de verschillende beroepscenario's.