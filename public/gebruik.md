# Softwaredevelopment met STOP

Deze leeswijzers beschrijven hoe de functionaliteit van STOP informatiekundig gemodelleerd is en hoe deze technisch gerealiseerd moet worden.
Doelgroep van deze leeswijzers zijn software-ontwikkelaars van leveranciers van bevoegd gezagen. 

[Snelstartgids voor STOP-XML](quickstart_overzicht.md)
: Beschrijft de invulling van de STOP-XML modules per stap binnen het [proces van bekendmaken en consolideren](proces_overzicht.md). 

[Modellering van tekst](regeling.md)
: Beschrijft de tekstmodellen van STOP

[informatieobject](io-intro.md)
: Beschrijft wat een informatieobject is en wat voor elk informatieobject geldt.

[Geografisch informatieobject](gio-intro.md)
: Beschrijft de specifiek het geografische informatieobject

[Document als informatieobject](IO_Document.md)
: Beschrijft hoe niet-STOP gecodeerde tekst opgenomen kan worden in een besluit of regeling.

[Naamgevingsconventie](naamgevingsconventie.md)
: Beschrijft de achtergrond van de naamgevingsconventies voor de identificatie van de diverse entiteiten die door STOP gemodelleerd worden, en de plaats waar de specificaties van de naamgeving zijn opgenomen.

[Tijdsafhankelijke (context) informatie](context_modules.md)
: Beschrijft het gebruik en de implementatie van de modules met contextinformatie zoals die in het STOP informatiemodel voorkomen.

[Annotaties](annotaties.md)

: Beschrijft de werking van annotatie modules in het STOP informatiemodel.

[Attenderen](attenderen.md)

: Beschrijft hoe besluiten voorzien moeten worden van informatie op basis waarvan burgers geattendeerd worden op besluiten in hun omgeving.