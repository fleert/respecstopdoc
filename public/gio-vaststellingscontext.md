# Vaststellings- of geografische context
Als een versie van een GIO bij een besluit wordt opgenomen, dan is een vaststellingscontext verplicht. De vaststellingscontext wordt ook wel geografische context genoemd en wordt vastgelegd in [GeografischeContext](gio_xsd_Element_gio_GeografischeContext.dita#GeografischeContext). Dit is een [verwijzing naar de achtergrond van het GIO](EA_1C752EC8163240cbA9644D1CD9266E58.dita#Pkg/EA026472CF9A4feeA2198820BC0D6C10) die is gebruikt  bij zijn vaststelling. De vaststellingscontext bestaat uit:
1. de **achtergrond** (zoals de Basisregistratie Topografie, BRT), en 
1. eventuele **andere GIO’s** die het bevoegd gezag heeft gebruikt om het GIO vast te stellen. 

![GIO met een achtergrond als vaststellingscontext](img/GIO-StellingVanAmsterdam.png "GIO met een achtergrond als vaststellingscontext")

## Gebruiksrichtlijnen vaststellingscontext
* Het gebruik van de basisregistraties voor het maken van een achtergrondkaart wordt sterk aangeraden en kan in een toepassingsprofiel (zoals een TPOD) verplicht gesteld worden. 
* Bij schaalniveaus 1:500 tot 1:5.000 moet [(wet BGT art. 23 lid 3)](https://wetten.overheid.nl/BWBR0034026/2018-07-01#Hoofdstuk5_Paragraaf2_Artikel23) de [Basisregistratie Grootschalige Topografie (BGT)](https://www.geobasisregistraties.nl/basisregistraties/grootschalige-topografie) worden gebruikt. 
* Voor andere schaalniveaus is de [Basisregistratie Topografie](https://www.geobasisregistraties.nl/basisregistraties/topografie) beschikbaar met de kaarten `TOP10NL`, `TOP50NL`, `TOP100NL`, `TOP250NL`, `TOP500NL` en `TOP1000NL`. Zie ook het [nationaal georegister](https://www.nationaalgeoregister.nl/geonetwork/srv/dut/catalog.search#/home) voor verdere informatie over de TOPNL-kaarten.
* Het bevoegd gezag moet de achtergrond van het GIO bewaren als de kaart TOP10NL (van de BRT) niet is gebruikt. Deze kaart is de enige door het Kadaster geleverde achtergrond die tijdreis-bestendig is.  Daarom is het niet nodig dat het bestuursorgaan deze ook nog zelf bewaart. 
* Moet het bevoegd gezag de kaart zelf bewaren, dan moet dit gebeuren volgens de Archiefwet op zo'n manier dat  kan worden teruggehaald hoe besluiten tot stand gekomen zijn, inclusief de weergave van het GIO op het moment van vaststellen. Niet alle kaartlagen hoeven te worden bewaard, maar slechts die data die nodig is om het genomen besluit te begrijpen en op de kaart te kunnen zien wat er destijds precies is besloten. Kaartlagen die totaal geen informatie toevoegen voor het besluit (bijvoorbeeld: een kaartlaag met alle putdeksels erop in een besluit tot functiewijziging in het omgevingsplan) hoeven niet te worden bewaard. 


## Nauwkeurigheid vaststellingscontext

Met de keuze van de achtergrond wordt impliciet ook de nauwkeurigheid van het GIO bepaald. Voor de [BGT](https://www.geobasisregistraties.nl/basisregistraties/bgt/basisregistratie-grootschalige-topografie)- en de [TOPNL](https://zakelijk.kadaster.nl/-/topnl)-kaarten is per kaart gedefinieerd voor welk detailniveau ze mogen worden gebruikt. De keuze van de achtergrondkaart bepaalt dus het detailniveau waarop het GIO getoond mag worden. Dit om te voorkomen dat een geometrisch object dat bijvoorbeeld op een TOP100NL-achtergrondkaart is afgebeeld, bij een weergave op een TOP10NL-niveau opeens onjuiste begrenzingen toont (zoals dwars door woningen, terwijl de weg voor de woningen was beoogd).

De nauwkeurigheid van een GIO volgt dus uit de gebruikte achtergrond, tenzij sprake is van een [berekende GIO](gio-berekend.md). In dat geval moet de nauwkeurigheid worden opgegeven in het element `nauwkeurigheid` binnen de **vaststellingscontext**](xsd:gio:GeografischeContext).

### Andere GIO's als geografische context

Er zijn situaties waar het tonen van een GIO tegen alleen een achtergrondkaart onvoldoende houvast biedt, bijvoorbeeld bij een nieuwe stadsuitbreiding waar de locatie van het nieuwe station ten opzichte van het nog te realiseren winkelcentrum bepaald moet worden.

Het is daarom mogelijk om naast de achtergrondkaart ook een aantal andere GIO's als referentie te gebruiken. In dat geval moet de context-GIO worden opgegeven in het element `contextGIOs` binnen de [**vaststellingscontext**](gio_xsd_Element_gio_GeografischeContext.dita#GeografischeContext).


## Zie ook
* [Het informatiemodel van de vaststellingscontext](gio_xsd_Element_gio_GeografischeContext.dita#GeografischeContext).
* [Uitleg over informatieobjecten](io-intro.md).
* [Juridische informatie over GIO's](vaststellen-wijzigen-gio.md).

