# data:Procedureverloop

Deze module voorziet in de informatie over de procedurestappen die doorlopen worden om een mijlpaal te realiseren. Het procedureverloop is afhankelijk van het [soortProcedure](data_xsd_Element_data_soortProcedure.dita#soortProcedure) (definitief of ontwerp) waarvoor gekozen is binnen de [data:BesluitMetadata](quickstart_2_BesluitMetadata.md).



| Element                                       | Vulling                                                      | Verplicht? |
| --------------------------------------------- | ------------------------------------------------------------ | ---------- |
| [bekendOp](data_xsd_Element_data_bekendOp.dita#bekendOp)                 | De datum waarop eenieder kennis kon hebben van bepaalde informatie. In de meeste gevallen is dit de bekendmakingsdatum van de officiële publicatie. | J          |
| [ontvangenOp](data_xsd_Element_data_ontvangenOp.dita#ontvangenOp)           | Wordt door de het STOP gebruikend systeem (LVBB) gevuld. Element niet meesturen met Besluit. | N          |
| [procedurestappen](data_xsd_Element_data_procedurestappen.dita#procedurestappen) | Verzameling van stappen in de Procedure die het BG doorloopt in het opstellen van een BesluitVersie teneinde een bepaalde mijlpaal (eind van een Procedure) te bereiken. Indien `soortProcedure=Ontwerpbesluit`, dan dient voor de invulling van [data:soortStap](data_xsd_Element_data_soortStap.dita#soortStap) de waardenlijst [procedurestap_ontwerp.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/procedurestap_ontwerp.xml) gebruikt te worden. Indien `soortProcedure=Definitief besluit`, dan dient voor de invulling van [data:soortStap](data_xsd_Element_data_soortStap.dita#soortStap) de waardenlijst [procedurestap_definitief.xml](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten/procedurestap_definitief.xml) gebruikt te worden. | J          |



## Kennisgeving levert termijnen

Bij een definitief besluit kan een bezwaar- en/of beroepstermijn van toepassing zijn en bij een ontwerp besluit een inzagetermijn. Indien dergelijke termijnen van toepassing zijn, dient het BG, conform de AWB, een kennisgeving te publiceren over deze termijnen. Bij deze kennisgeving worden dan de relevante procedurestappen als [procedureverloopmutatie](quickstart_2a_Procedureverloopmutatie.md) opgenomen.  Afhankelijk van deze (en andere) procedurestap(pen) wordt de status van het besluit bepaald, die bij de geconsolideerde regeling wordt uitgeleverd. Zie de toelichting over de [rechterlijke macht](besluitproces_rechterlijkemacht.md) en met name de [wijze van aanlevering](besluitproces_rechterlijkemacht_procedurele_status.html#procedureverloop-en-procedureverloopmutatie).



## Voorbeeld

[Voorbeeld Procedureverloop Definitief besluit](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/01%20Proces%20van%20aanleveren%20tm%20consolidatie/01%20Nieuwe%20regeling%20met%20iwt/02-BG-Besluit-Procedure.xml)

