# Publicatie-instructie

Een [publicatie-instructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie) bevat de juridische status van het informatieobject. Aan de hand daarvan kan het publicatiesysteem het informatieobject op de juiste manier verwerken als het wordt aangeleverd bij een besluit. De publicatie-instructie is onderdeel van de work-metadata van het informatieobject, de zogenoemde  [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata).
Er zijn drie statussen die kunnen worden toegekend:
1. **`Informatief`**. Niet-juridisch geldig. Het informatieobject is geen onderdeel is van een besluit.  Achtergrond-informatie bij een besluit. Wordt niet geconsolideerd.
2. **`AlleenBekendTeMaken`**. Juridisch geldig. Het informatieobject is onderdeel van een besluit. Wordt niet geconsolideerd. Voorbeeld: milieu-effectrapportage (MER). 
3. **`TeConsolideren`**. Juridisch geldig. Het informatieobject is juridisch zelfstandig. Het bevat niet-tekstuele informatie bij een regeling die aspecten van regels vastlegt. Wordt geconsolideerd. Voorbeeld: [geografisch informatieobject](gio-intro.md) (GIO).



| Publicatie-instructie |  Collectie | Consolidatie | Borging onveranderlijkheid |
| ------------         | -----------| ---------    | ------ |
| `Informatief` (gereserveerd voor toekomstig gebruik)       |  `infodata` | Nee | Nee |
|`AlleenBekendTeMaken` |  `pubdata` | Nee | Ja, door LVBB |
|  `TeConsolideren`    | `regdata` | Ja | Ja, door LVBB |

## Opmerkingen
* De collectie (regdata, pubdata of infodata) wordt gebruikt in de JOIN-ID van het informatieobject. De collectie moet passen bij juridische status zoals uitgedrukt in de publicatie-instructie.
* Voor verdere uitleg over informatieve, alleen bekend te maken of te consolideren IO's, zie [Soorten IO's](io-soorten.md). 

## Zie ook
* Schema-documentatie van de [publicatie-instructie](data_xsd_Element_data_publicatieinstructie.dita#publicatieinstructie).
* Diagram van de [publicatie-instructie](EA_EC4DE25BFB1341e6B632337DAF9C2791.dita#Pkg/2985BA71453C4a4894E33BBF68CAE23A).