# Soort tekstelement: Publicatieblad

De 'Publicatieblad'-tekstelementen zijn elementen die uitsluitend gebruikt worden in juridische publicatiebladen.

## Publicatieblad-elementen

[Bladaanduiding](tekst_xsd_Element_tekst_Bladaanduiding.dita#Bladaanduiding), [BladGR](tekst_xsd_Element_tekst_BladGR.dita#BladGR), [Gemeenteblad](tekst_xsd_Element_tekst_Gemeenteblad.dita#Gemeenteblad), [OfficielePublicatie](tekst_xsd_Element_tekst_OfficielePublicatie.dita#OfficielePublicatie), [Provinciaalblad](tekst_xsd_Element_tekst_Provinciaalblad.dita#Provinciaalblad), [Staatsblad](tekst_xsd_Element_tekst_Staatsblad.dita#Staatsblad), [Staatscourant](tekst_xsd_Element_tekst_Staatscourant.dita#Staatscourant), [Titelregel](tekst_xsd_Element_tekst_Titelregel.dita#Titelregel), [Waterschapsblad](tekst_xsd_Element_tekst_Waterschapsblad.dita#Waterschapsblad).

