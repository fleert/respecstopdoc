# Automatiseerbaar creatieproces

## Creatieproces

STOP moet de publicatie van besluiten en geconsolidereerde regelingen faciliteren voor regelingen die zeer frequent gewijzigd worden, zoals een omgevingsplan van een gemeente. Dat is enigszins te vergelijken met het wijzigen van regelgeving van het Rijk, waar ook meerdere parallelle processen lopen om (dezelfde) regeling te wijzigen.

Het proces voor Rijksregelgeving bestaat traditioneel uit twee verschillende processen met een lage automatiseringsgraad. Dit is in onderstaande illustratie als klassiek proces voor een wijziging geschetst:
* De wijziging wordt voorbereid door een nieuwe versie van de regeling op te stellen
* Een wijzigingsregeling wordt opgesteld waarin (in tekst) beschreven staat hoe de nieuwe versie van de regeling uit de oude regeling verkregen wordt. Het gebruik van wijzigingsinstructies heeft een juridische achtergrond: bevoegd gezag neemt een besluit over de wijziging, niet over de gehele regeling.
* Het besluit wordt genomen door de wijzigingsregeling vast te stellen.
* Het besluit/wijzigingsregeling wordt bekendgemaakt.
* Zodra de inwerkingtredingsdatum van een wijziging bekend is, worden de wijzigingsinstructies door een redactie geïnterpreteerd.
* De redactie verwerkt de wijzigingen om zo te komen tot de geconsolideerde regeling die volgt uit de besluit.
* De geconsolideerde regeling wordt beschikbaar gesteld.

![Creatieproces klassiek en in STOP](img/creatieproces.png)

Uitgangspunt voor STOP is te komen tot een proces dat beter te automatiseren is, waarbij tenminste de bekendmaking en beschikbaarstelling volledig geautomatiseerd kunnen worden en geen handwerk meer vereisen. Daarnaast moet STOP faciliteren dat de formulering van de geconsolideerde regeling direct afgeleid is van de formulering van de besluiten zodat de kans dat de geconsolideerde regeling juridisch afwijkt van de besluiten zo klein mogelijk is. Dat leidt tot verschuivingen in het proces:
* De wijziging wordt nog steeds voorbereid door een nieuwe versie van de regeling op te stellen, maar daarbij wordt gebruik gemaakt van speciale ondersteunende software in verband met het benodige versiebeheer.
* Het controleren op samenloop en onderlinge samenhang gebeurt in het klassieke model ook al bij het voorbereiden van een besluit, maar bij gebruik van STOP wordt dat op een machine-leesbare manier in de ondersteunende software vastgelegd.
* De beschrijving van de wijzigingen wordt gegenereerd door de ondersteunende sofware om zeker te weten dat die correspondeert met de aanpassing van de regeling.
* Het besluit wordt genomen door de wijzigingsregeling vast te stellen. De ondersteunende software kan hierbij van nut zijn door niet alleen het besluit te tonen, maar ook de aangepaste regeling die resulteert uit het besluit.
* STOP moet het mogelijk maken alle informatie voor de bekendmaking van het besluit en voor het samenstellen van de geconsolideerde regeling vast te leggen. Het besluit/wijzigingsregeling wordt bekendgemaakt.
* De landelijke voorzieningen stellen de geconsolideerde regeling samen door de aangepaste versies van de regeling te reconstrueren uit de bekendgemaakte besluiten en door de aangeleverde informatie over de samenhang tussen de besluiten te gebruiken.
* De landelijke voorzieningen stellen de geldende regeling beschikbaar in verschillende vormen: zowel op de manier als voorgeschreven door de Bekendmakingswet, als in domein-specifieke toepassingen als DSO-LV.


## Vertaling in STOP
STOP is geschreven voor een proces dat op twee manieren bezien kan worden:

* Gezien vanuit een bevoegd gezag is de regeling het belangrijkste, want daarin staat waaraan eenieder zich moet houden of welk beleid er gevoerd gaat worden. De regeling heeft geen rechtskracht als daarover niet volgens juridische voorschriften is besloten. Besluiten zijn een noodzakelijke voorwaarde, maar het werkproces is gericht op het actueel houden van regelingen.

* Gezien vanuit een jurist zijn de besluiten het belangrijkste, omdat de besluiten de juridische basis vormen voor regelgeving. Een besluit moet voldoen aan wettelijke eisen die onder andere voorschrijven hoe nieuwe of gewijzigde regels en beleid vastgelegd en gepubliceerd moet worden. Door alle besluiten te combineren kan de geldende regeling worden afgeleid, maar die vorm van de regeling is niet meer dan een praktische manier om te weten wat in de besluiten staat. 

De standaard is zo geschreven dat beide benaderingen naast elkaar kunnen bestaan:

* Er zijn [modellen](regeling.md) om een regeling vast te leggen die erop gericht zijn dat het bevoegd gezag regels en beleid kan beschrijven zodat eenieder daar kennis van kan nemen.
* Er zijn [modellen](besluit.md) om een besluit uit te wisselen zodat aan alle wettelijke eisen rond vastleggen en publicatie van een besluit voldaan kan worden.

Regeling en besluit zijn geen onafhankelijke entiteiten: de een is de bron van de ander. Uitgangspunt bij het ontwerp van de standaard is dat het afleiden van de een uit de ander een geautomatiseerd proces moet kunnen zijn. Dit is de beste garantie dat een regeling beschrijft wat daadwerkelijk besloten is, en dat een besluit vastlegt hoe de regeling komt te luiden. Gezien het werkproces bij bevoegd gezagen is er in de standaard voor gekozen om de regeling als bron te kiezen.

![Relatie besluit-regeling](img/Besluit-Relatie-Regeling.png)

De STOP modellen voor een regeling- en informatieobjectversie zijn ontworpen voor gebruik bij de voorbereiding van een nieuw besluit en voor het beschikbaar stellen van de geconsolideerde regeling. Deze modellen zijn ontdaan van aspecten die alleen in het besluitvormingsproces nodig zijn. De geconsolideerde regelgeving is een informatieproduct, een afgeleide van de (juridische) besluiten. Het ontwerp en toepassing van de STOP modellen is daarom op informatiekundige overwegingen gebaseerd, niet op juridische voorschriften over de inhoud van besluiten.

De STOP modellen voor een besluit zijn zodanig ontworpen dat ze voldoen aan alle juridische eisen van het besluitvormingsproces, inclusief de publicatievereisten. In de tekst van het besluit is ruimte gereserveerd om een eerste versie van een regeling op te nemen of, bij wijziging, een beschrijving van de wijziging. De (nieuwe of gewijzigde) informatieobjecten zijn onderdeel van het besluitmodel in ongewijzigde of uitgebreide {::comment}Uitgebreide? Wat bedoel je hier precies mee?{:/comment}vorm. De standaard beschrijft transformaties om de tekst van een regeling of de data van een informatieobject in een besluitmodel op te nemen.

De transformaties zijn bedoeld om geautomatiseerd uitgevoerd te worden. Sterker nog: het is niet de bedoeling dat er nog handmatige aanpassingen op het resultaat van de transformaties plaatsvindt. De transformaties zijn omkeerbaar. Een systeem als de LVBB kan daarom uit de daadwerkelijk gepubliceerde besluiten de regeling- en informatieobjectversies afleiden.
