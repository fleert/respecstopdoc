# Intrekken en opvolging GIO



## Opvolging

Bij een GIO-wijziging ontstaat een nieuwe expressie van hetzelfde work (zie [FRBR model](EA_188A75C61FB54d108E7659F3FE5DE8F3.dita#Pkg)). In bepaalde situaties, bijvoorbeeld bij het samenvoegen van twee GIO's, is het gewenste resultaat niet met een wijziging te bereiken. Het oude GIO wordt dan ingetrokken en een nieuwe GIO wordt ingesteld. Om dan toch aan te kunnen geven dat het nieuwe GIO een opvolger is van de ingetrokken GIO('s), kan bij het nieuwe GIO aangegeven worden dat het GIO een opvolger is van één of meer andere GIO's. De opvolgingsrelatie wordt tot uitdrukking gebracht door: 

- In de InformatieObjectMetadata de opvolgingsrelatie ["opvolger van"](EA_EC4DE25BFB1341e6B632337DAF9C2791.dita#Pkg/823304801C6D45108A8BB5E86A666808) toe te voegen.

Bovenstaande wordt hieronder in detail toegelicht.


## Tekst

De tekst van de regeling wordt op twee plaatsen gewijzigd:

1. de verwijzing naar het GIO wordt verwijderd uit de regelingtekst (en in dit voorbeeld wordt ook een verwijzing naar een nieuwe GIO toegevoegd);
2. in de informatieobjecten-bijlage wordt de JOIN-identifier verwijderd (en in dit voorbeeld wordt ook een nieuwe toegevoegd).

**Aanpassing in de regelingtekst:**

  Het is verboden consumentenvuurwerk te gebruiken in het <span class="doorhalen">vuurwerkverbodsgebied</span> vuurwerkverbodsgebied 2021.

Uitgedrukt in XML:

```xml
<Inhoud>
  <Al>Het is verboden consumentenvuurwerk te gebruiken in het <VerwijderdeTekst>
    <IntIoRef eId="art_2__para_1__ref_o_1_inst2"
               ref="gm9999_2__cmp_A__content_o_1__list_1__item_1__ref_o_1"
               wId="gm9999_1__art_2__para_1__ref_o_1">vuurwerkverbodsgebied</IntIoRef> </VerwijderdeTekst>
    <NieuweTekst> <IntIoRef eId="art_2__para_1__ref_o_1"
                             ref="gm9999_3__cmp_A__content_o_1__list_1__item_1__ref_o_1"
                             wId="gm9999_3__art_2__para_1__ref_o_1">vuurwerkverbodsgebied 2021</IntIoRef> </NieuweTekst>. </Al>
</Inhoud>
```

**Aanpassing in de informatieobjecten-bijlage:**

  <span class="doorhalen">vuurwerkverbodsgebied </span>   vuurwerkverbodsgebied 2021
  <span class="doorhalen">`/join/id/regdata/gm0307/2019/gio993859238/nld@2020‑06‑16;2`</span>   `/join/id/regdata/gm9999/2021/gio993859462/nld@2021‑03‑23;1`

Uitgedrukt in XML:

```xml
<Begrip>
  <Term>
    <VerwijderdeTekst>vuurwerkverbodsgebied</VerwijderdeTekst>
    <NieuweTekst>vuurwerkverbodsgebied 2021</NieuweTekst> </Term>
  <Definitie>
    <!-- oude definitie met verwijzing naar oude GIO. 
           eId met inst2 toevoeging om hem uniek te maken -->
    <Al wijzigactie="verwijder"> <ExtIoRef eId="cmp_A__content_o_1__list_1__item_1__ref_o_1_inst2"
                                            ref="/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2"
                                            wId="gm9999_2__cmp_A__content_o_1__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2019/gio993859238/nld@2020-06-16;2</ExtIoRef> </Al>
    <!-- nieuwe definitie met verwijzing naar nieuwe GIO -->
    <Al wijzigactie="voegtoe"> <ExtIoRef eId="cmp_A__content_o_1__list_1__item_1__ref_o_1"
                                          ref="/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1"
                                          wId="gm9999_3__cmp_A__content_o_1__list_1__item_1__ref_o_1">/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1</ExtIoRef> </Al>
  </Definitie>
</Begrip>
```



## ConsolidatieInformatie: Intrekking

Naast de aanpassing in de regelingtekst moet het intrekken van het GIO ook opgenomen worden in de consolidatie-informatie die het BG meelevert met het besluit.  Een intrekking betreft altijd een heel Work *). Dit wordt aangegeven met `instrument`. De `eId` verwijst naar de plek in het besluit waar de JOIN-id van het GIO wordt verwijderd.

*) Versies kunnen alleen met een rectificatie teruggetrokken worden (zie [terugtrekken GIO](besluitproces_rectificatie_scenario_D3.md)).

```xml
<Intrekkingen>
  <Intrekking>
    <doelen>
      <doel>/join/id/proces/gm9999/2021/WijzigingKernenOmgevingsplan</doel>
    </doelen>
    <instrument>/join/id/regdata/gm9999/2019/gio993859238</instrument>
    <eId>!wijziging#cmp_A__content_o_1__list_1__item_1__ref_o_1_inst2</eId>
    <gemaaktOpBasisVan>
      <Basisversie>
          <doel>/join/id/proces/gm9999/2020/VergrotenVuurwerkverbodsgebied</doel>
          <gemaaktOp>2020-06-20T09:03:00Z</gemaaktOp>
      </Basisversie>
    </gemaaktOpBasisVan>
  </Intrekking>
</Intrekkingen>
```

Met de intrekking is er een einddatum voor de juridische werking en de geldigheid van het GIO gekomen. Dit wordt in het [bi-temporeel geldigheid-werkendop-diagram](tijdreizen-bi-temporeel-model.md) van deze GIO uitgedrukt met een doorgetrokken streep op de intrekkingsdatum:

![](img/GIO1toestanden-geldigheid.png)



## ConsolidatieInformatie: Instelling

In dit voorbeeld wordt ook een nieuwe GIO ingesteld. De instelling van dit nieuwe GIO wordt opgenomen in de consolidatieinformatie van het besluit als `BeoogdInformatieobject`.

```xml
<BeoogdInformatieobject>
  <doelen>
    <doel>/join/id/proces/gm9999/2021/WijzigingKernenOmgevingsplan</doel>
  </doelen>
  <instrumentVersie>/join/id/regdata/gm9999/2021/gio993859462/nld@2021-03-23;1</instrumentVersie>
  <eId>!wijziging#cmp_A__content_o_1__list_1__item_1__ref_o_1</eId>
</BeoogdInformatieobject>
```

Het [bi-temporeel geldigheid-werkendop-diagram](tijdreizen-bi-temporeel-model.md) van het nieuwe GIO geeft doormiddel van de stippellijntjes aan dat er geen einddatum geldigOp / werkendOp bekend is.

![](img/GIO2toestanden-geldigheid.png)


## InformatieObjectMetadata: opvolging

Afwijkend bij de instelling van deze GIO is dat bij deze GIO aangegeven moet worden dat dit de opvolger is van het ingetrokken GIO. De opvolging wordt met de elementen `opvolging`  en `opvolgerVan` in de informatieobjectmetadata aangegeven:

```xml
<opvolging>
  <opvolgerVan>/join/id/regdata/gm9999/2019/gio993859238</opvolgerVan>
</opvolging>
```


De volledige uitwerking van dit voorbeeld is [hier](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden/Besluiten%20en%20regelingen/02%20Proces%20met%20GIO/03%20Intrekking%20GIO%20en%20instelling%20nieuwe) te vinden.