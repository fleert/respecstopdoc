# Bekendgemaakte GIO
Het (gewijzigde) GIO is juridisch onderdeel van het besluit en wordt met het besluit bekendgemaakt in een elektronisch publicatieblad (Staatscourant, Gemeenteblad, Provincieblad, Waterschapsblad) op  [Zoeken in alle documenten | Overheid.nl > Officiële bekendmakingen](http://www.officielebekendmakingen.nl). De bekendmaking bevat de (juridisch) authentieke versie van het besluit in PDF en van het GIO in GML-formaat. 

## Landingspagina
Een bekendgemaakte GIO heeft een eigen landingspagina. Daar wordt in de publicatie van het besluit naar verwezen. Deze pagina bevat naast een titel met de naam van het GIO (label) een interactieve kaart waar het GIO zichtbaar is zodat de gebruiker de geometrische data kan interpreteren. Het bevat ook een mogelijkheid om de geometrische data van het object te kunnen downloaden. Deze geometrische data is wat er juridisch geldt, want dat is wat het bevoegd gezag formeel heeft vastgesteld. De visuele presentatie van de geometrische data in de viewer moet wel correct zijn, maar is service-informatie en geen formeel onderdeel van het besluit. 

![img/Presentatiemodel_TekstverwijzenGIO.png](img/Presentatiemodel_TekstverwijzenGIO.png "Besluittekst met mens-leesbaar label en verwijzingen naar een GIO in de bijlage.")

## Raadplegen GIO
Ook vanuit de authentieke PDF-versie van het bekendgemaakte besluit (die geen links bevat) is elke GIO te achterhalen door de JOIN-ID op [URI - Resolver](https://identifier.overheid.nl/) in te vullen. Dit is een service van de LVBB. Deze service werkt alleen voor GIO's die zijn bekendgemaakt. In onderstaande afbeelding wordt de JOIN-ID `/join/id/regdata/mn002/2019-11-01/or_defensie_terreinen_objecten/nld@2019-11-1` aan de resolver aangeboden.

<img src="img/GIO-JOINResolver.png" width="500px" />

De resolver verwijst door naar de [GIO-landingspagina](https://repository.officiele-overheidspublicaties.nl/datacollecties/2019/dc-2019-141/1/html/dc-2019-141.html) met de inhoud van het GIO.

## Openen GIO-bestand
Om een GIO te bekijken is GIS-software of een GML-viewer nodig. Hoewel de inhoud van het GIO geheel voldoet aan de GML-standaard, is een GIO niet altijd zonder meer in elke applicatie in te laden. Voor het gratis beschikbare open source softwarepakket [QGIS](www.qgis.org):

* is een [conversiescript](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aservice/GIO/GIO2Qgis.xsl) beschikbaar, waarmee een GIO omgezet kan worden in een voor QGIS te openen .gml file;
* kan de [configuratie van QGIS](https://github.com/Geonovum/xml_xslt/tree/master/GIO_gml) aangepast worden, zodat GIO's zonder conversie ingelezen kunnen worden;
* of kan een [plug-in](https://plugins.qgis.org/plugins/gio_import/) geïnstalleerd worden, waarmee GIO's ook gelezen kunnen worden.
