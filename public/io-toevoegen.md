# Een IO toevoegen


## Nieuw IO toevoegen aan een besluit

Om een initiële versie van een IO vast te stellen moet deze worden opgenomen in een regeling en via een besluit worden vastgesteld. Dit gebeurt als volgt:

1. In de regelingtekst van een besluit wordt verwezen naar het IO. Zie [Verwijzen naar een IO](io-tekstverwijzen.md).
2. Als het een `TeConsolideren` IO is, wordt in de tekst-bijlage van de regeling het label (IO-naam) en de JOIN-ID van het IO opgenomen. Zie [IO opnemen in bijlage](io-opnemen-bijlage.md).
3. In de [besluitmetadata](data_xsd_Element_data_BesluitMetadata.dita#BesluitMetadata) wordt de [expression-ID](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie) van het IO in de [InformatieobjectRefs](data_xsd_Element_data_informatieobjectRefs.dita#informatieobjectRefs) vermeld. Zie [IO toevoegen aan besluitmetadata](io-toevoegen-besluitmetadata.md).
3. Als het een `TeConsolideren` IO is, wordt het IO  opgenomen als [`BeoogdInformatieobject`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/707BA9EDC9454e66AC0D6F45EDBCA93E) in de [`ConsolidatieInformatie`](EA_5E740899BABE47d1B62053A402064B33.dita#Pkg/E480ACE31F414f8aB3CF2309232EB934) bij het besluit. Zie [IO opnemen als beoogd informatieobject](io-opnemen-beoogdinformatieobject.md).
3. Als het een `TeConsolideren` IO is, wordt de JOIN-ID van het *work* van de regeling waarmee het de IO-versie is bekendgemaakt opgenomen in de [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) van de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata).  Zie [geboorteregeling opnemen in IO](io-geboorteregeling-opnemen.md).
4. Het IO  wordt als informatieobject toegevoegd aan het besluit over de regeling. Zie [IO-bestand toevoegen](io-modules-toevoegen.md).

## Bestaand IO toevoegen aan een besluit
Om een bestaande IO dat is vastgesteld met een ander besluit toe te voegen aan een besluit, moet deze worden opgenomen in een regeling door te verwijzen naar het eerder bekendgemaakte IO. Dit gebeurt als volgt:

1. In de regelingtekst van een besluit wordt verwezen naar het IO. Zie [Verwijzen naar een IO](io-tekstverwijzen.md).
2. In de tekst-bijlage van de regeling worden het label (IO-naam). Zie [IO opnemen in bijlage](io-opnemen-bijlage.md).
3. Als het een `TeConsolideren` IO is, wordt de JOIN-ID van het *work* van de regeling waarmee het de IO-versie is bekendgemaakt opgenomen in de [`heeftGeboorteregeling`](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) van de [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata).


De daadwerkelijke inhoud van het IO wordt niet (opnieuw) toegevoegd aan het besluit. De IO wordt dus niet genoemd in de `InformatieobjectRefs` van `BesluitMetadata` en ook niet in `BeoogdInformatieobject` van de `Consolidatieinformatie`.


## Zie ook
* Voor informatie over de (verplichte) onderdelen van een IO, zie [Onderdelen IO](io-onderdelen.md).
* Voor de verschillende soorten IO's, zie [Soorten IO's](io-soorten.md).
* Voor informatie over verwijzen vanuit de tekst naar informatieobjecten, zie [Verwijzen naar informatieobjecten](io-tekstverwijzen.md).
* Voor informatie over GIO's, zie [GIO's](gio-intro.md).

