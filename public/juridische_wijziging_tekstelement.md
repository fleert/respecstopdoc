# Bepaling juridische tekstwijziging

## Juridische en niet-juridische wijzigingen
In de [juridische verantwoording](juridische_verantwoording.md) voor een element in de tekst van een regeling worden alleen die wijzigingen meegenomen waarin de juridische inhoud van de tekst wijzigt. Deze pagina beschrijft hoe dat bepaald kan worden.

Een noodzakelijke voorwaarde is dat de wijziging onderdeel uitmaakt van een juridisch instrument, zoals een wijzigingsbesluit. Het tekstelement zal dan gewijzigd worden als onderdeel van een [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie). Maar niet elke wijziging heeft ook een effect op de juridische inhoud van het element. Voorbeelden van niet-juridische wijzigingen zijn het toevoegen van een verwijzing ([IntRef](tekst_xsd_Element_tekst_IntRef.dita#IntRef)) voor weergave in een webpagina of het wijzigen van de [eId](begrippenlijst_eId.dita#eId) van het element vanwege een wijziging in de structuur van de tekst elders.

## Elementen waarvoor een juridische verantwoording bijgehouden wordt

De juridische verantwoording kan alleen worden bijgehouden voor een element uit een STOP tekstmodel dat te muteren is, dus dat zelfstandig als onderdeel van een [Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang) kan voorkomen, en dat zichtbaar is in de weergave van de tekst:

| Element | Toelichting |
| ------- | ----------- |
| **Artikelgewijze structuur** ||
| [Afdeling](tekst_xsd_Element_tekst_Afdeling.dita#Afdeling) | Een afdeling wordt als gewijzigd beschouwd als de [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop) wijzigt. Als een van de tekstelementen binnen de afdeling wijzigt, wordt de afdeling zelf niet als gewijzigd beschouwd. |
| [Boek](tekst_xsd_Element_tekst_Boek.dita#Boek) | Als afdeling  |
| [Deel](tekst_xsd_Element_tekst_Deel.dita#Deel) | Als afdeling  |
| [Hoofdstuk](tekst_xsd_Element_tekst_Hoofdstuk.dita#Hoofdstuk) | Als afdeling  |
| [Paragraaf](tekst_xsd_Element_tekst_Paragraaf.dita#Paragraaf) | Als afdeling  |
| [Subparagraaf](tekst_xsd_Element_tekst_Subparagraaf.dita#Subparagraaf) | Als afdeling  |
| [Titel](tekst_xsd_Element_tekst_Titel.dita#Titel) | Als afdeling  |
| [Artikel](tekst_xsd_Element_tekst_Artikel.dita#Artikel) | Kleinste eenheid waarvoor de juridische verantwoording wordt bijgehouden. |
| **Vrijetekststructuur** ||
| [Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie) | Een divisie wordt als gewijzigd beschouwd als de [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop) of [InleidendeTekst](tekst_xsd_Element_tekst_InleidendeTekst.dita#InleidendeTekst) wijzigt. Als een divisie of divisietekst binnen de divisie wijzigt, wordt de divisie zelf niet als gewijzigd beschouwd. |
| [Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) | Kleinste eenheid waarvoor de juridische verantwoording wordt bijgehouden. |
| **Overige** ||
| [Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam) | Hoewel Lichaam vervangen kan worden, wordt voor het lichaam geen juridische verantwoording bijgehouden omdat het lichaam niet zichtbaar is in de weergave van de tekst; het is een geoepering van elementen in het XML model van de tekst. |
| [Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage) | Een bijlage wordt als gewijzigd beschouwd als de [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop) wijzigt. In de huidige praktijk wordt de bijlage ook wel als één geheel gezien; STOP volgt die werkwijze niet. STOP rapporteert ook de wijzigingen binnen de bijlage. |
| [Toelichting](tekst_xsd_Element_tekst_Toelichting.dita#Toelichting) | Een toelichting wordt als gewijzigd beschouwd als de [Kop](tekst_xsd_Element_tekst_Kop.dita#Kop) of [Sluiting](tekst_xsd_Element_tekst_Sluiting.dita#Sluiting) wijzigt.  Een toelichting wordt niet als onderdeel van de regeling beschouwd, maar is wel van belang voor de interpretatie van de regeling. Daarom wordt in STOP een juridische verantwoording bijgehouden. |
| [AlgemeneToelichting](tekst_xsd_Element_tekst_AlgemeneToelichting.dita#AlgemeneToelichting) | Zie Toelichting |
| [ArtikelgewijzeToelichting](tekst_xsd_Element_tekst_ArtikelgewijzeToelichting.dita#ArtikelgewijzeToelichting) | Zie Toelichting |

## Detectie van een juridische wijziging

Met onderstaande regels wordt vastgesteld in welke gevallen er sprake is van een juridische wijziging van een tekstelement. Een bevestigend antwoord betekent dat de publicatie waarin de wijziging bekend gemaakt is vermeld wordt in de juridische verantwoording voor dat element.

Welke regels toegepast moeten worden hangt af van het (parent-)element waarin een tekstelement is opgenomen. Dit parent-element is in de eerste kolom van de tabel vermeld. Dit parent-element moet op zijn beurt weer zijn opgenomen in het top-level element van het instrument dat gepubliceerd wordt, zoals een [BesluitKlassiek](tekst_xsd_Element_tekst_BesluitKlassiek.dita#BesluitKlassiek) of een [BesluitCompact](tekst_xsd_Element_tekst_BesluitCompact.dita#BesluitCompact).

| Parent-element | Juridische wijziging | Toelichting |
| -------------- | -------------------- | ----------- |
| [RegelingKlassiek](tekst_xsd_Element_tekst_RegelingKlassiek.dita#RegelingKlassiek) | `Instelling` | Aanwezigheid van deze structuur impliceert de instelling van een nieuwe regeling. |
| [RegelingCompact](tekst_xsd_Element_tekst_RegelingCompact.dita#RegelingCompact) | `Instelling` | Aanwezigheid van deze structuur impliceert de instelling van een nieuwe regeling. |
| [RegelingMutatie](tekst_xsd_Element_tekst_RegelingMutatie.dita#RegelingMutatie) | Afhankelijk van mutatieactie | De regels voor de verschillende mutatieacties worden in de volgende tabel beschreven. |
| [WijzigInstructies](tekst_xsd_Element_tekst_WijzigInstructies.dita#WijzigInstructies) | Nee | Elementen die binnen een wijziginstructie zijn opgenomen wijzigen regelingen die niet in STOP zijn gemodelleerd. De juridische verantwoording daarvoor wordt niet via STOP gemodelleerd. |

Voor de mutatieacties gelden de regels:

| Mutatieactie | Juridische wijziging | Toelichting |
| ------------ | -------------------- | ----------- |
| [VervangRegeling](tekst_xsd_Element_tekst_VervangRegeling.dita#VervangRegeling) | Ja, zie toelichting | Dit is een bijzonder scenario dat alleen toegepast wordt als een tekst niet via andere mutatieacties aangepast kan worden. daarom wordt aangenomen dat de hele tekst gewijzigd is. De juridische wijziging is `Instelling` voor een element met een wId die in de te wijzigen versie van de regeling niet voorkomt maar in de nieuwe versie wel, `Beëindiging`voor een element met een wId die in de te wijzigen versie van de regeling wel voorkomt maar in de nieuwe versie niet, en `Wijziging` voor de elementen die in beide versies van de regeling voorkomen. |
| [VoegToe](tekst_xsd_Element_tekst_VoegToe.dita#VoegToe) | `Instelling` | Dit is altijd een juridische wijziging. Er is niet alleen sprake van een `Instelling` voor het element dat toegevoegd wordt, maar ook voor alle kindelementen waarvoor een juridische verantwoording wordt bijgehouden. |
| [Verwijder](tekst_xsd_Element_tekst_Verwijder.dita#Verwijder) | `Beëindiging` | Dit is altijd een juridische wijziging, behalve als de inhoud van het element [Vervallen](tekst_xsd_Element_tekst_Vervallen.dita#Vervallen) is. Er is niet alleen sprake van een `Beëindiging` voor het element dat verwijderd wordt, maar ook voor alle kindelementen waarvoor een juridische verantwoording wordt bijgehouden die verwijderd worden als gevolg van de verwijdering van dit tekstelement (voor zover ze als inhoud niet `Vervallen` hebben). |
| [VervangKop](tekst_xsd_Element_tekst_VervangKop.dita#VervangKop) | `Wijziging` | Dit is altijd een juridische wijziging. Het maakt niet uit of deze mutatie gedaan wordt omdat er een vernummering heeft plaatsgevonden en alleen het [Nummer](tekst_xsd_Element_tekst_Nummer.dita#Nummer) wordt gewijzigd, of dat de tekst in de kop aangepast wordt. |
| [Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang) met `@revisie = "1"` | Nee | Met het `revisie` attribuut kan aangegeven worden dat de inhoud van een `Vervang` geen juridische wijziging bevat. Het is aan de opsteller van de mutatie om te borgen dat daadwerkelijk alleen de niet-juridische inhoud van het tekstelement gewijzigd wordt. |
| [Vervang](tekst_xsd_Element_tekst_Vervang.dita#Vervang) (anders) | Afhankelijk van de inhoud van het tekstelement | Zie onderstaande beschrijving. |

Bij een `Vervang` die geen revisie is moet de inhoud van de `Vervang` operatie geanalyseerd worden. Daarbij wordt uitsluitend gekeken naar te tekstelementen waarvoor een juridische verantwoording wordt bijgehouden. Niet alleen het element dat vervangen wordt, maar ook de kind-elementen. Daarbij gelden de algemene regels:

* De elementen [Nummer](tekst_xsd_Element_tekst_Nummer.dita#Nummer) en [Wat](tekst_xsd_Element_tekst_Wat.dita#Wat) van `Vervang` moeten genegeerd worden; deze worden gebruikt om mutatie weer te geven.
* Het element [Redactioneel](tekst_xsd_Element_tekst_Redactioneel.dita#Redactioneel) moet genegeerd worden; de inhoud is nooit juridisch.
* Voor een tekstelement dat als inhoud [Vervallen](tekst_xsd_Element_tekst_Vervallen.dita#Vervallen) heeft in de `was`-versie wordt geen nieuwe juridische wijziging geregistreerd als volgens onderstaande regels de wijziging een `Beëindiging` is.

De regels uit de volgende tabel moeten op volgorde worden afgelopen totdat er een regel van toepassing is: 

| Inhoud tekstelement | Juridische wijziging | Toelichting |
| ------------------- | ------------------- | ----------- |
| [Vervallen](tekst_xsd_Element_tekst_Vervallen.dita#Vervallen) met [@wijzigactie](tekst_xsd_Attribute_Group_tekst_agWijzigactie.dita#agWijzigactie)` = "voegtoe"` | `Beëindiging` | Een tekstelement kan pas vervallen als ook alle kind-elementen waarvoor een juridische verantwoording wordt bijgehouden vervallen (of vervallen zijn). Het is de verantwoordelijkheid van bevoegd gezag om die kind-elementen ook als `Vervallen` te markeren. |
| [@wijzigactie](tekst_xsd_Attribute_Group_tekst_agWijzigactie.dita#agWijzigactie)` = "voegtoe"` | `Instelling` | Er is niet alleen sprake van een `Instelling` voor het tekstelement, maar ook voor alle kind-elementen van het tekstelement waarvoor een juridische verantwoording wordt bijgehouden. |
| [@wijzigactie](tekst_xsd_Attribute_Group_tekst_agWijzigactie.dita#agWijzigactie)` = "verwijder"` | `Beëindiging` | Er is niet alleen sprake van een `Beëindiging` voor dit tekstelement, maar ook voor alle kind-elementen waarvoor een juridische verantwoording wordt bijgehouden die verwijderd worden als gevolg van de verandering van dit tekstelement. |
| [@wijzigactie](tekst_xsd_Attribute_Group_tekst_agWijzigactie.dita#agWijzigactie)` = "nieuweContainer"` | `Instelling` | Als `@wijzigactie = "voegtoe"` |
| [@wijzigactie](tekst_xsd_Attribute_Group_tekst_agWijzigactie.dita#agWijzigactie)` = "verwijderContainer"` | `Beëindiging` | Als `@wijzigactie = "verwijder"` |
| [NieuweTekst](tekst_xsd_Element_tekst_NieuweTekst.dita#NieuweTekst) in de inhoud van het tekstelement | `Wijziging` | |
| [VerwijderdeTekst](tekst_xsd_Element_tekst_VerwijderdeTekst.dita#VerwijderdeTekst) in de inhoud van het tekstelement | `Wijziging` | |
| [@wijzigactie](tekst_xsd_Attribute_Group_tekst_agWijzigactie.dita#agWijzigactie)` in de inhoud van het tekstelement | - | Als een wijzigactie als attribuut voorkomt op een van de kind-elementen binnen de inhoud van het tekstelement, dan is er geen sprake van een juridische wijziging. De wijzigactie wordt gebruikt om elementen te muteren die tot de service-informatie gerekend worden. |

