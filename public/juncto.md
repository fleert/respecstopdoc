# Juncto's in STOP {#concept_y2f_53b_gqb}

Deze sectie beschrijft wat een zogeheten juncto is en hoe STOP daarmee omgaat.

## Juncto {#definitie-juncto .section}

Een juncto is een 'wijziging van de wijziging', een wijzigingsinstructie voor een eerder bekendgemaakte maar nog niet inwerking getreden wijzigingsinstructie. **Juncto** betekent letterlijk ''in verband met'' of ''in samenhang met''. Soms besluit de wetgever namelijk om vóór inwerkingtreding van reeds bekendgemaakte wijzigingen deze alsnog aan te passen. Men leest een juncto dan ook altijd in samenhang met de oorspronkelijke wijzigingsopdracht. Daarnaast treedt een juncto hiermee tegelijk in werking. Anders zou het een regulier wijzigingsbesluit zijn dat de regeling direct wijzigt.

**Voorbeeld van een juncto**

Artikel I.A van de [Wet van 8 maart 2017 tot wijziging van de Wet uitfasering pensioen in eigen beheer en overige fiscale pensioenmaatregelen](https://zoek.officielebekendmakingen.nl/stb-2017-116.html) wijzigt de wijziging in [Wet van 8 maart 2017 tot wijziging van enkele belastingwetten en enige andere wetten tot uitfasering van het pensioen in eigen beheer en het treffen van enkele fiscale maatregelen inzake oudedagsvoorzieningen \(Wet uitfasering pensioen in eigen beheer en overige fiscale pensioenmaatregelen\)](https://zoek.officielebekendmakingen.nl/stb-2017-115.html). De geconsolideerde versie ziet er [zo](https://wetten.overheid.nl/BWBR0039380/2017-04-01) uit.

## STOP-methode juncto {#stop-handling-juncto .section}

STOP wijzigt nooit een wijziging. Wijzigingen op regelgeving worden in STOP bekendgemaakt via wijzigingen in renvooi op een versie van de regeling. Technisch is namelijk een 'renvooi op renvooi' niet mogelijk. STOP heeft echter wel een uniforme methode voor het verwerken van een 'wijziging van de wijziging', ongeacht de juridische aanleiding. Deze aanleiding kan zowel een autonoom besluit zijn van het BG, als een rectificatie \(link\) \(een correctieve wijziging op een foutieve *bekendmaking* van het besluit\) of een besluit van de rechter, dat een eerder besluit van het BG wijzigt.

Deze uniforme STOP-methode houdt in dat voor de bekendmaking van een 'wijziging van een wijziging' de *wordt*-versie van de regeling na de oorspronkelijke wijziging nu de *was*-versie van de juncto wordt. De beoogde 'juncto'-wijziging wordt dus uitgedrukt door de renvooiweergave van de beoogde wijzigingen, echter niet op het besluit dat de eerder besloten wijzigingen bekendmaakte, maar op de versie zoals die zou volgen uit de eerder besloten maar nog niet in werking getreden wijziging\(en\).

De STOP-methode en verwerkingsmethode is dus anders dan bij een traditionele juncto, maar het resultaat is in juridische zin hetzelfde.

![De verwerking van een juncto schematisch weergegeven](img/verwerking-juncto.png "De verwerking van een juncto schematisch weergegeven")

**Uitleg STOP-verwerking**:

1.  **Wijzigingsbesluit 2** bevat een `RegelingMutatie` op **Regelingversie 1**- niet op Wijzigingsbesluit 1;
2.  Voor **Wijzigingsbesluit 3** bevat ook een `RegelingMutatie`. Voor deze mutatie geldt dat `was` = Regelingversie 2, `wordt` = Regelingversie 3. Dit is het STOP-equivalent van de traditionele juncto.
3.  **Regelingversie 2** en **Regelingversie 3** hebben dezelfde datum inwerkingtreding.

**Uitleg verwerking traditionele juncto**:

1.  Er is een `BesluitKlassiek` \(**Wijzigingsbesluit 2** in het onderste diagram\) met daarin een `RegelingKlassiek` die de Regelingversie 1 wijzigt en dus een `RegelingMutatie` voor deze regeling bevat \(`was` = Regelingversie 1, `wordt` = Regelingversie 2\)
2.  Er is een tweede `BesluitKlassiek` \(**Wijzigingsbesluit 3**\) met daarin een `RegelingKlassiek` \(B2\) die de `RegelingMutatie` wijzigt van **Wijzigingsbesluit 2**.
3.  **Wijzigingsbesluit 2** wordt bekendgemaakt na **Wijzigingsbesluit 1**, maar beiden wijzigingsbesluiten treden tegelijk in werking.

