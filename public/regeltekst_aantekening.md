# Soort tekstelement: Aantekening

Aantekeningelementen zijn elementen die te maken hebben met het proces van creatie of vaststelling of die iets zeggen over de geldigheid van de geconsolideerde regelingtekst:

| element                                          | voorbeeld                                                 | weergave                                                 |
| ------------------------------------------------ | --------------------------------------------------------- | -------------------------------------------------------- |
| [`Gereserveerd`](tekst_xsd_Element_tekst_Gereserveerd.dita#Gereserveerd)         | `<Gereserveerd/>`                                         | `[`Gereserveerd`]`                                       |
| [`NogNietInWerking`](tekst_xsd_Element_tekst_NogNietInWerking.dita#NogNietInWerking) | `<NogNietInWerking/>`                                     | `[`Treedt in werking op een nader te bepalen tijdstip`]` |
| [`Vervallen`](tekst_xsd_Element_tekst_Vervallen.dita#Vervallen)               | `<Vervallen/>`                                            | `[`Vervallen`]`                                          |
| [`Redactioneel`](xsd.tekst.Redactioneel) *)      | `<Redactioneel>` `<Al>Toelichting</Al>` `</Redactioneel>` | `[`Red. : Toelichting`]`                                 |

*) Redactioneel is uit compatibiliteitsoverwegingen overgenomen uit de voorlopers van het IMOP-tekstmodel. Gebruik wordt ontmoedigd.