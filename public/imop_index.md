# Implementatiemodel Officiële Publicaties

## Inleiding
Dit is de documentatie van versie @@@IMOPVERSIE@@@ van het Implementatiemodel Officiële Publicaties (IMOP) die is gepubliceerd op @@@DATUM@@@. Dit onderdeel beschrijft de XML-implementatie van STOP, vastgelegd middels **[XML Schema Documents (XSD)](https://www.w3.org/TR/xmlschema-1/)** en **[Schematrons](https://schematron.com/)**.  

De XML-implementatie is geen directe vertaling van het [informatiemodel van STOP](EA_4D1D7FD517FE4d8dA9A2B905D202C92B.dita#Pkg). Het informatiemodel is vormgegeven om duidelijk te maken welke informatie van belang is en hoe die met elkaar samenhangt. De implementatie is vormgegeven met bruikbaarheid in software als primaire doel. Daardoor zijn er andere modelleerkeuzes gemaakt. De implementatie is wel direct te relateren aan het informatiemodel. Bij zo veel mogelijk elementen uit de XML-schema's is aangegeven van welk(e) element(en) uit het informatiemodel het element een implementatie is.

## Documentatie
De documentatie van IMOP bestaat uit een aantal onderdelen:

[Wijzigingsoverzicht](imop_wijzigingsoverzicht.md)
: Hierin is beschreven welke wijzigingen in de IMOP zijn aangebracht als gevolg van wijzigingen in STOP en/of als verbetering of uitbreiding van eerdere IMOP-versies, en welke wijzigingen in de bijbehorende documentatie zijn aangebracht ten opzichte van eerdere documentatie-versies.

[XML-serialisatie](imop_xml.md)
: Beschrijft de wijze waarop STOP informatie in XML-documenten wordt uitgewisseld. 

[Uitwisselpakket](uitwisselpakket.md)
: IMOP kent ook een manier om IMOP-modules op een standaard manier te verpakken in een gecomprimeerd bestand. Dit uitwisselpakket kan gebruikt worden in API's. Het kan ook gebruikt worden om STOP-informatie via een enkel bestand uit te wisselen.

[Gebruik van IMOP](schemata_gebruik.md)
: Toelichting op het gebruik van de technische hulpmiddelen die als onderdeel van IMOP beschikbaar gesteld worden.

[Schemadocumentatie](schema_documentatie.md)
: De documentatie van de technische implementatie van de standaard in XML schema's.
De schema's zelf zijn te vinden op [Gitlab](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema).

Schematrondocumentatie
: De schematrons zijn niet apart gedocumenteerd. In het overzicht van de [bedrijfsregels](businessrules.md) is aangegeven welke bedrijfsregels als schematron beschikbaar zijn. Deze sectie bevat een [toelichting op het gebruik van bedrijfsregels](imop_bedrijfsregels.md).

Transformaties
: Een beschrijving van de transformaties die nodig zijn om een [IMOP-module](xml_modulairestructuur.md) te converteren van/naar een andere IMOP-versie.

## Technische hulpmiddelen

Schema's, schematrons en transformaties
: De technische hulpmiddelen zijn te vinden op [Gitlab](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema).

[Waardelijsten](imop_waardelijsten.md)
: IMOP schrijft voor diverse XML-elementen het gebruik van voorgedefinieerde waardes voor. De toegestane waardes zijn te vinden op [Gitlab](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Awaardelijsten).

Voorbeelden
: Voorbeelden van de toepassing van technische implementatie van de standaard zijn te vinden op [Gitlab](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Avoorbeelden). 



