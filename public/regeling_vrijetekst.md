# Regeling met vrije tekst

De regeling met vrije tekst heeft een lichaam dat niet is opgebouwd uit artikelen maar alleen d.m.v. een hiërarchische structuur geordend is. Dit soort teksten is gebruikelijk bij regelingen met beleidsregels of regelingen met zelfbindende voorschriften.

![Tekstmodel met vrije tekst](img/Regeling-Vrijetekst.png)

Een [tekst:RegelingVrijetekst](tekst_xsd_Element_tekst_RegelingVrijetekst.dita#RegelingVrijetekst) bestaat uit:

[tekst:RegelingOpschrift](tekst_xsd_Element_tekst_RegelingOpschrift.dita#RegelingOpschrift)
: De officiële titel van de regeling.

[tekst:Lichaam](tekst_xsd_Element_tekst_Lichaam.dita#Lichaam)
: Het lichaam bevat de voorschriften of (beleids-)regels van de regeling in [tekst:Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) elementen. Deze kunnen in een hiërarchie van secties ondergebracht worden, waarbij voor een sectie een [tekst:Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie) gebruikt wordt. In een sectie mogen andere secties voorkomen, `Divisietekst` elementen of beiden.

[tekst:Bijlage](tekst_xsd_Element_tekst_Bijlage.dita#Bijlage)
: Een bijlage wordt gebruikt als de inhoud ervan niet goed is op te nemen in het lichaam. De redenen voor het opnemen van informatie in een bijlage zijn grofweg:  

* _vormgeving_: Bijvoorbeeld complexe tabellen behoeven vaak afwijkende formattering.

* _leesbaarheid_: Bijvoorbeeld lange lijsten zouden de tekstdoorloop teveel verstoren.

* _vindplaats_: Een bijlage kan bestaan uit een verwijzing naar een elders gepubliceerde informatie.

* _technisch_: Een bijlage kan uit niet-tekstuele informatie bestaan, zoals een gebiedsbegrenzing, audiobestand of configuratie voor een rekenprogramma. In STOP wordt dit type bijlage niet als bijlage bij de tekst opgenomen maar wordt als een [Informatieobject](io-intro.md) gemodelleerd.

  De inhoud van de bijlage is beschreven in [tekst:Divisietekst](tekst_xsd_Element_tekst_Divisietekst.dita#Divisietekst) elementen. Deze kunnen in een hiërarchie van secties ondergebracht worden, waarbij voor een sectie een [tekst:Divisie](tekst_xsd_Element_tekst_Divisie.dita#Divisie) gebruikt wordt. In een sectie mogen andere secties voorkomen, `Divisietekst` elementen of beide.
