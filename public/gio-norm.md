# Norm (optioneel)
Een GIO kan een **norm** (maximaal één) bevatten. Een norm kan worden toegevoegd aan een GIO om normwaarden per locatie juridisch vast te leggen. Dit is één van de manieren om normwaarden juridisch vast te leggen. In totaal zijn er vier manieren:
1. **Normwaarde en werkingsgebied in de tekst**. Voor deze optie gebruikt men geen GIO. Worden normwaarden in de tekst van de regeling vermeld zonder GIO met werkingsgebied, dan gelden deze normwaarden automatisch voor het ambtsgebied.
1. **Normwaarde in de tekst, werkingsgebied in het GIO**. Voor deze optie gebruikt men een [GIO als werkingsgebied](gio-werkingsgebied.md).
1. **Normwaarde in de tekst, werkingsgebied in een GIO-deel**. Voor deze optie moet men een [GIO met GIO-delen](gio-met-delen.md) gebruiken. 
1. **Normwaarde en werkingsgebied in het GIO**. Voor deze optie gebruikt men een [GIO met normwaarden](gio-met-normwaarden.md).

## Uitgangspunten normen
* Heeft een GIO een norm, dan hebben de locaties van de GIO een [normwaarde](gio-met-normwaarden.md). 
* Een GIO met een norm en normwaarden heeft altijd [symbolisatie](gio-symbolisatie.md). 
* Bij bovengenoemde optie 4 (Normwaarde en werkingsgebied in het GIO) staan de waarden alleen in het GIO. Deze *mogen niet* ook in de tekst aanwezig zijn.
* Geldt voor een locatie in een GIO met norm geen normwaarde, dan is deze locatie niet in die GIO aanwezig.

## Normsoorten

Een norm kan kwantitatief of kwalitatief zijn: 

* Kwantitatieve normen hebben normwaarden die in een reëel getal uitgedrukt worden. 
* Voor kwalitatieve normen worden de normwaarden uitgedrukt in een string. Denk aan "goed", "matig" of "slecht" om bijvoorbeeld de kwaliteit van zwemwater aan te duiden. Hierbij is geen eenheid aanduiding toegestaan.

## Onderdelen norm
Een GIO met kwalitatieve normwaarden bevat: 
* een [normlabel](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normlabel), 
* een [normID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normID).

Een GIO met kwantitatieve normwaarden bevat ook:

* een [eenheidlabel](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidlabel) waarin de normwaarden (die voor locaties gelden) worden uitgedrukt. 
* een [eenheidID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidID). 
## Zie ook
* De [bedrijfsregels met betrekking tot normen en normwaarden](businessrules_geo_GeoInformatieObjectVersie.dita#Bedrijfsregels).