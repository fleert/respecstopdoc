# Onderdelen GIO

Wat onderdelen betreft wijken het juridisch geldige GIO en het geconsolideerde GIO iets van elkaar af. Welke vorm welke onderdelen heeft, wordt in deze sectie beschreven.


## Onderdelen van een GIO

### Vaste onderdelen informatieobject
Een juridisch geldige GIO heeft de onderdelen die een  informatieobject altijd heeft:
* [ExpressionIdentificatie](data_xsd_Element_data_ExpressionIdentificatie.dita#ExpressionIdentificatie)
* [InformatieObjectMetadata](data_xsd_Element_data_InformatieObjectMetadata.dita#InformatieObjectMetadata)
*  [InformatieObjectVersieMetadata](data_xsd_Element_data_InformatieObjectVersieMetadata.dita#InformatieObjectVersieMetadata) 


## GIO-specifieke onderdelen
Daarnaast heeft het de volgende GIO-specifieke onderdelen:
1. de module [`GeoInformatieObjectVersie`](geo_xsd_Element_geo_GeoInformatieObjectVersie.dita#GeoInformatieObjectVersie) met daarin:
    * (verplicht) **JOIN-ID**. Dit is een [unieke identificatie](io-expressionidentificatie.md) voor:
        * de [Work-ID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_FRBRWork) van het GIO.
        * de [Expression-ID](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_FRBRExpression) van het GIO.
    * (optioneel) een **norm**, te weten:
        * een [`normID`](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normID), een identificatie van de norm.
        * een [`normlabel`](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_normlabel), de naam van de norm.
        * een [`eenheidlabel`](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidlabel), verplicht bij kwantitatieve norm(waard)en.
        * een [`eenheidID`](geo_xsd_Complex_Type_geo_GeoInformatieObjectVersieType.dita#GeoInformatieObjectVersieType_eenheidID), verplicht bij kwantitatieve norm(waard)en.
    * (optioneel) een **overzicht met GIO-delen**, bevat een of meerdere `Groep`-elementen. Elke groep heeft een:
        * een `groepID`
        * een `label`, dat als naam kan worden gebruikt in de tekst van de regeling en in de [afbeelding (symbolisatie)](gio-symbolisatie.md) van een GIO. 
    * (verplicht) één of meerdere **locaties**. Deze locaties worden beschreven door geometrische data die de begrenzing van een gebied aangeeft. Elke locatie heeft: 
        * zijn eigen *geometrische data*.
        * (optioneel) een *naam*. Deze wordt als label getoond in de LVBB-viewer.
        * (optioneel) een *normwaarde*.  
        * (optioneel) een *GIO-deel-ID* (`groepID`) dat aangeeft tot welk GIO-deel een bepaalde locatie behoort.
2. (optioneel) de module [`JuridischeBorgingVan`](	xsd:gio:JuridischeBorgingVan) met de **juridische borging**.
3. (optioneel) de module [`FeatureTypeStyle`](se_xsd_Element_se_FeatureTypeStyle.dita#FeatureTypeStyle) met daarin de **GIO-symbolisatie**: informatie over hoe een GIO wordt afgebeeld op een kaart.

## Onderdelen geconsolideerde GIO

Naast de modules die de GIO vormen kent de geconsolideerde GIO de extra modules voor een geconsolideerd informatieobject:

* [ConsolidatieIdentificatie](cons_xsd_Element_cons_ConsolidatieIdentificatie.dita#ConsolidatieIdentificatie) in plaats van `ExpressionIdentificatie`,
* [toestanden](cons_xsd_Element_cons_ActueleToestanden.dita#ActueleToestanden),
*  [GerechtelijkeProcedureStatus](cons_xsd_Element_cons_GerechtelijkeProcedureStatus.dita#GerechtelijkeProcedureStatus),
*  [JuridischeVerantwoording](cons_xsd_Element_cons_JuridischeVerantwoording.dita#JuridischeVerantwoording).
