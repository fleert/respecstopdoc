# Schemadocumentatie

## Schema bestanden
De IMOP-schema-bestanden staan in de map [schema](https://localhost/@@@DOCUMENTATIE_URL@@@go/go.html?id=git%3Aschema) in de git repository. 
het gaat om de volgende bestanden:

|XSD|betekenis|
|---|---|
|[imop-tekst.xsd](tekst_xsd_Main_schema_tekst_xsd.dita#tekst.xsd)    |de "tekst"-onderdelen van een Besluit en een Regeling|
|[imop-gio.xsd](gio_xsd_Main_schema_gio_xsd.dita#gio.xsd)        |de context van de vaststelling van een GIO |
|[imop-geo.xsd](geo_xsd_Main_schema_geo_xsd.dita#geo.xsd)        |de vaststelling van een GIO|
|[imop-data.xsd](data_xsd_Main_schema_data_xsd.dita#data.xsd)      |de "data"-onderdelen uit het informatiemodel|
|[imop-cons.xsd](cons_xsd_Main_schema_cons_xsd.dita#cons.xsd)      |onderdelen rondom consolidatie|
|[imop-resources.xsd](rsc_xsd_Main_schema_rsc_xsd.dita#rsc.xsd)  |de "resources" (grofweg de "STOP-waardelijsten")|
|[imop-schemata.xsd](sv_xsd_Main_schema_sv_xsd.dita#sv.xsd)  |de beschrijving van (versies van) schema's, schematrons en transformaties|
|[imop-bedrijfsregels.xsd](br_xsd_Main_schema_br_xsd.dita#br.xsd)|de bedrijfsregels van STOP|
|[imop-uitwisseling.xsd](uws_xsd_Main_schema_uws_xsd.dita#uws.xsd)|het uitwisselpakket van STOP|

De introductiepagina per schema bevat een beschrijving van dat schema. De structuur van de schema-documentatie is beschreven in [Legenda schemadocumentatie](schema_documentatie_legenda.md).

## Externe schema's

De juiste versies van de externe schema's wordt als service [meegeleverd](schemata_gebruik_lokaal.md) bij IMOP. De normatieve externe schema's zijn te vinden van de websites hieronder. 

Voor de implementatie van het geo-informatieobject (GIO) wordt gebruik gemaakt van de externe schema's: 

- [Basisgeometrie v1.0](https://docs.geostandaarden.nl/nen3610/basisgeometrie), beheerd door Geonovum. 
- Basisgeometrie importeert de schema's voor [GML 3.2.2](http://www.ogc.org/standards/gml).
- Voor symbolisatie van geo-informatie schrijft STOP het gebruik van de ["OpenGIS® Symbology Encoding Implementation Specification](https://www.ogc.org/standards/symbol)" **v1.1.0** voor.

Zie voor toelichting de introductiepagina's van [imop-geo.xsd](geo_xsd_Main_schema_geo_xsd.dita#geo.xsd) en [se-FeatureStyle-imop.xsd](se_xsd_Main_schema_se_xsd.dita#se.xsd).

## Eigenschappen van alle XSDs
De targetNamespace van een XSD geldt voor alle elementen in dat schema. Attributen kennen geen namespace. Dit wordt in alle IMOP-XSDs vastgelegd middels de *default* waarden voor de *element* en *attributes form*:
* `elementFormDefault = "qualified"` : alle elementen zijn gedefinieerd in "targetNamespace" van de XSD
* `attributeFormDefault = "unqualified"` : alle attributen hebben geen namespace (met één uitzondering in imop-tekst; deze staat attributen in een "foreign" namespace toe op een aantal elementen. 

Zie ook [O'Reilly XML Schema](https://docstore.mik.ua/orelly/xml/schema/ch10_03.htm). 

