# Doel van renvooiering

[TODO](DITA_OnbekendeLink.dita#Oeps)

Het doel van het aanbrengen van renvooimarkeringen in de tekst is het zichtbaar maken van de wijzigen. 

Iets zeggen over juridische ranvoorwaarden:

- Wat vastgesteld wordt zijn de wijzigingen
- Als alleen de tekst gegeven wordt, dan zijn wijzigingen moeilijk te herkennen
- Renvooi helpt om de wijzigingen te herkennen
- Renvooi is niet de wijziging zelf, maar laat alleen zien waar de wijziging in de tekst staan
- Renvooi hoeft dus ook niet exact overeen te komen met de wijziging (bijv letters markeren bij correctie van een spelfout), maar het moet voldoende precies zijn
- Bijv als een tabel qua structuur niet wijzigt, dan de inhoud van de cellen als renvooi. Als de structuur wijzigt, dan is het acceptabel dat de tebel vervangen wordt als het bepalen van renvooi te ingewikkeld zou zijn.

Iets over besluit en regelingtekst in renvooi:
- Besluit bevat alleen wijzigingen, anders niet meer leesbaar
- Dat is lastig voor eenieder en voor betrokkenen bij het vaststellingsproces om effect van wijziging op regeling in te zien.
- Daarom ook de mogelijkheid de hele regelingtekst in renvooi te hebben. Dan is wel duidelijk hoe de regeling eruit komt te zien en wat het besluit inhoudt.
- Bovendien komt regeling-in-renvooi exact overeen met het besluit, mede vanwege automatisering.

Het is een omkeerbaar proces, kan het besluit ook dienen als basis voor de geautomatiseerde consolidatie.