# Regeling is materieel uitgewerkt

## Patroon
Een regeling of informatieobject is nog wel juridisch geldig maar kent eigenlijk geen toepassing meer. Bijvoorbeeld een regeling voor personen geboren vòòr 1 januari 1900. Ondanks dat de regeling nog wel geldig is, is er niemand meer waarvoor deze regeling van toepassing is, de regeling is 'materieel uitgewerkt'. 

![](img/consolideren_patronen_1materieeluitgewerkt.png)

## Uit te wisselen informatie
Om een regeling als materieel uitgewerkt aan te duiden wordt als onderdeel van een revisie de datum doorgegeven van de eerste dag waarop de regeling of informatieobject geen toepassing meer heeft: Dat staat in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van de revisie:

* Een [MaterieelUitgewerkt](data_xsd_Element_data_MaterieelUitgewerkt.dita#MaterieelUitgewerkt) element met de work-identificatie van de regeling of informatieobject als `instrument`. En als `datum` de datum dat het instrument materieel uitgewerkt is.

Dit is niet aan een doel gekoppeld en heeft (bij uitwisseling met de LVBB) meteen effect.

Als later blijkt dat de regeling toch niet als materieel uitgewerkt gemarkeerd had moeten worden, dan kan een revisie worden uitgewisseld met in de [ConsolidatieInformatie](data_xsd_Element_data_ConsolidatieInformatie.dita#ConsolidatieInformatie) module van de revisie:

* Een [TerugtrekkingMaterieelUitgewerkt](data_xsd_Element_data_TerugtrekkingMaterieelUitgewerkt.dita#TerugtrekkingMaterieelUitgewerkt) element met de work-identificatie van de regeling of informatieobject als `instrument`. 

## Implementatiekeuzes
Als een regeling materieel uitgewerkt is zal dat ook gelden voor de works die daar juridisch onderdeel van zijn, zoals de informatieobjecten die de regeling als [geboorteregeling](data_xsd_Element_data_heeftGeboorteregeling.dita#heeftGeboorteregeling) hebben en de tijdelijk regelingdelen die de regeling als [hoofdregeling](data_xsd_Element_data_isTijdelijkDeelVan.dita#isTijdelijkDeelVan) hebben. Het is een implementatiekeuze van de STOP-gebruikende systemen om deze informatieobjecten impliciet materieel uitgewerkt te maken (dus alleen de datum door te geven voor de geboorteregeling/hoofdregeling) of voor elk van de regelingen en informatieobjecten afzonderlijk. Een tijdelijk regelingdeel kan materieel uitgewerkt zijn zonder dat de hoofdregeling dat is.

Een STOP-gebruikend systeem kan zelf bepalen hoe met een materieel uitgewerkte regeling omgegaan wordt. Voor de LVBB gelden de [voorschriften](https://wetten.overheid.nl/jci1.3:c:BWBR0045086&hoofdstuk=3&artikel=3.1&z=2022-01-01&g=2022-01-01) vanuit de Bekendmakingswet: de regeling blijft beschikbaar maar de vindbaarheid wordt beperkt. Een STOP-gebruikend systeem mag er ook voor kiezen de regeling niet meer te tonen.
