# GIO als werkingsgebied

Een GIO die als [werkingsgebied](begrippenlijst_werkingsgebied.dita#werkingsgebied) voor juridische regels wordt gebruikt bestaat uit een of meerdere locaties met alleen geometrische data. 

## Onderdelen


Een GIO als werkingsgebied dat onderdeel is van een besluit heeft de GIO-specifieke onderdelen van een [juridisch geldige GIO](gio-onderdelen.md#GIO-specifieke-onderdelen), maar heeft:
* *geen* norm, 
* *geen* GIO-delen.

