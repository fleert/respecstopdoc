# Leeswijzers

De leeswijzers zijn onderverdeeld naar thema:

[Basis van STOP](toepassing.md)
: Deze leeswijzers beschrijven de processen waarvoor STOP is ontworpen en de mogelijkheden die STOP biedt.
: Doelgroep van deze leeswijzers zijn:
  - de eindgebruikers van software die informatie volgens STOP uitwisselt, waaronder planmakers en juristen
  - de softwareontwikkelaars van dergelijke software

[Architectuur en ontwerpkeuzes](architectuur.md)
: Deze leeswijzers beschrijven de structuur van de STOP-standaard, en geven een beeld van de eisen en uitgangspunten die zijn gehanteerd tijdens het ontwerp. 
: Doelgroep van deze leeswijzers zijn architecten en ontwerpers van informatiemodellen.

[Softwaredevelopment met STOP](gebruik.md)
: Deze leeswijzers beschrijven hoe de functionaliteit van STOP informatiekundig gemodelleerd is en hoe deze technisch gerealiseerd moet worden.
: Doelgroep van deze leeswijzers zijn software ontwikkelaars van leveranciers van bevoegd gezagen. 

[Voortbouwen op STOP](voortbouwen_op_STOP.md)
: Deze leeswijzers beschrijven de eisen waaraan software moet voldoen bij het genereren van entiteiten uit het STOP-informatiemodel op 
basis van andere entiteiten uit dat model.
: Doelgroep van deze documentatie zijn architecten, ontwerpers van informatiemodellen en softwareontwikkelaars van het DSO en de LVBB die moeten aansluiten bij STOP in domeinen waar de STOP-standaard geen invulling aan geeft.
