# Stap 3: Consolideren na bekendmaking

Na bekendmaking van een definitief besluit met een juridische inwerkingtredingsdatum kan de LVBB de Toestanden berekenen van de regeling en de GIO(s) die uit het aangeleverde besluit volgen. Ontwerpbesluiten treden niet in werking. O.b.v. een ontwerpbesluit kan een proefversie van een instrument worden samengesteld. 

**STOP-XML Modules Consolidatie Regeling n.a.v. definitief Besluit**

* [cons:ConsolidatieIdentificatie](quickstart_3_ConsolidatieIdentificatie.md)
* [cons:Toestanden](quickstart_3_Toestanden.md)

**STOP-XML Module Proefversie n.a.v. ontwerpbesluit**

* [cons:Proefversies](quickstart_3_Proefversies.md)